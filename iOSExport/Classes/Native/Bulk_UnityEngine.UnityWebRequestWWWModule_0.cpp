﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.WWW
struct WWW_t2919945039;
// System.String
struct String_t;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t1786092740;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t2821884557;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t3420491431;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t3552561393;
// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t3443159558;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1216180266;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t62501539;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1241853011;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t766225616;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t3616431661;

extern RuntimeClass* UnityWebRequest_t254341728_il2cpp_TypeInfo_var;
extern RuntimeClass* UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var;
extern RuntimeClass* DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m1234042134_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m648019172_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m540686856_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m551560767_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2794591321_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m882561911_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1596707798;
extern Il2CppCodeGenString* _stringLiteral782856060;
extern Il2CppCodeGenString* _stringLiteral730767724;
extern const uint32_t WWW__ctor_m2992943289_MetadataUsageId;
extern RuntimeClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern const uint32_t WWW_get_bytes_m3375427027_MetadataUsageId;
extern RuntimeClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2512070489;
extern const uint32_t WWW_get_error_m2450520467_MetadataUsageId;
extern const RuntimeMethod* Dictionary_2_set_Item_m65971941_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1794480464;
extern Il2CppCodeGenString* _stringLiteral2722398782;
extern const uint32_t WWW_get_responseHeaders_m1819170249_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t WWW_get_text_m3992458586_MetadataUsageId;
extern RuntimeClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2494949592;
extern Il2CppCodeGenString* _stringLiteral309755548;
extern const uint32_t WWW_WaitUntilDoneIfPossible_m2158502638_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2041362128;
extern Il2CppCodeGenString* _stringLiteral75521586;
extern Il2CppCodeGenString* _stringLiteral3831553393;
extern Il2CppCodeGenString* _stringLiteral1598235917;
extern Il2CppCodeGenString* _stringLiteral1042792716;
extern Il2CppCodeGenString* _stringLiteral3901348444;
extern Il2CppCodeGenString* _stringLiteral2135358226;
extern Il2CppCodeGenString* _stringLiteral982223462;
extern Il2CppCodeGenString* _stringLiteral2038575240;
extern Il2CppCodeGenString* _stringLiteral1554773766;
extern Il2CppCodeGenString* _stringLiteral3465782523;
extern Il2CppCodeGenString* _stringLiteral523764538;
extern Il2CppCodeGenString* _stringLiteral3551656767;
extern Il2CppCodeGenString* _stringLiteral2809809645;
extern Il2CppCodeGenString* _stringLiteral1283101652;
extern Il2CppCodeGenString* _stringLiteral3248504360;
extern Il2CppCodeGenString* _stringLiteral3161377847;
extern Il2CppCodeGenString* _stringLiteral1899870879;
extern Il2CppCodeGenString* _stringLiteral3643225343;
extern Il2CppCodeGenString* _stringLiteral62823554;
extern Il2CppCodeGenString* _stringLiteral663334555;
extern Il2CppCodeGenString* _stringLiteral3795604005;
extern Il2CppCodeGenString* _stringLiteral3921440208;
extern Il2CppCodeGenString* _stringLiteral4253801532;
extern Il2CppCodeGenString* _stringLiteral1414245911;
extern Il2CppCodeGenString* _stringLiteral788780341;
extern Il2CppCodeGenString* _stringLiteral146775263;
extern Il2CppCodeGenString* _stringLiteral922626835;
extern Il2CppCodeGenString* _stringLiteral3230308904;
extern Il2CppCodeGenString* _stringLiteral652983053;
extern Il2CppCodeGenString* _stringLiteral618349143;
extern Il2CppCodeGenString* _stringLiteral1214181781;
extern Il2CppCodeGenString* _stringLiteral2640244280;
extern Il2CppCodeGenString* _stringLiteral46458807;
extern Il2CppCodeGenString* _stringLiteral4093720035;
extern Il2CppCodeGenString* _stringLiteral2585140355;
extern Il2CppCodeGenString* _stringLiteral1232914839;
extern Il2CppCodeGenString* _stringLiteral2631285777;
extern const uint32_t WWW_GetStatusCodeName_m170717970_MetadataUsageId;
struct UnityWebRequest_t254341728_marshaled_com;

struct ByteU5BU5D_t3397334013;


#ifndef U3CMODULEU3E_T3783534231_H
#define U3CMODULEU3E_T3783534231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534231 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534231_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DICTIONARY_2_T3943999495_H
#define DICTIONARY_2_T3943999495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t3943999495  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t3030399641* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t62501539* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1642385972* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1642385972* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t228987430 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___table_4)); }
	inline Int32U5BU5D_t3030399641* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t3030399641* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___linkSlots_5)); }
	inline LinkU5BU5D_t62501539* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t62501539** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t62501539* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___keySlots_6)); }
	inline StringU5BU5D_t1642385972* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1642385972** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1642385972* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___valueSlots_7)); }
	inline StringU5BU5D_t1642385972* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1642385972** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1642385972* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___serialization_info_13)); }
	inline SerializationInfo_t228987430 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t228987430 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t228987430 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t3943999495_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t766225616 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t3943999495_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t766225616 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t766225616 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t766225616 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T3943999495_H
#ifndef YIELDINSTRUCTION_T3462875981_H
#define YIELDINSTRUCTION_T3462875981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3462875981  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3462875981_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef CUSTOMYIELDINSTRUCTION_T1786092740_H
#define CUSTOMYIELDINSTRUCTION_T1786092740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1786092740  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1786092740_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef KEYVALUEPAIR_2_T38854645_H
#define KEYVALUEPAIR_2_T38854645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t38854645 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t38854645, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t38854645, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T38854645_H
#ifndef UINT64_T2909196914_H
#define UINT64_T2909196914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t2909196914 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t2909196914, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T2909196914_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef INT64_T909078037_H
#define INT64_T909078037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t909078037 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t909078037, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T909078037_H
#ifndef BYTE_T3683104436_H
#define BYTE_T3683104436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t3683104436 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t3683104436, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T3683104436_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef KEYVALUEPAIR_2_T1701344717_H
#define KEYVALUEPAIR_2_T1701344717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct  KeyValuePair_2_t1701344717 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1701344717, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1701344717, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1701344717_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef WWW_T2919945039_H
#define WWW_T2919945039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWW
struct  WWW_t2919945039  : public CustomYieldInstruction_t1786092740
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.WWW::_uwr
	UnityWebRequest_t254341728 * ____uwr_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::_responseHeaders
	Dictionary_2_t3943999495 * ____responseHeaders_1;

public:
	inline static int32_t get_offset_of__uwr_0() { return static_cast<int32_t>(offsetof(WWW_t2919945039, ____uwr_0)); }
	inline UnityWebRequest_t254341728 * get__uwr_0() const { return ____uwr_0; }
	inline UnityWebRequest_t254341728 ** get_address_of__uwr_0() { return &____uwr_0; }
	inline void set__uwr_0(UnityWebRequest_t254341728 * value)
	{
		____uwr_0 = value;
		Il2CppCodeGenWriteBarrier((&____uwr_0), value);
	}

	inline static int32_t get_offset_of__responseHeaders_1() { return static_cast<int32_t>(offsetof(WWW_t2919945039, ____responseHeaders_1)); }
	inline Dictionary_2_t3943999495 * get__responseHeaders_1() const { return ____responseHeaders_1; }
	inline Dictionary_2_t3943999495 ** get_address_of__responseHeaders_1() { return &____responseHeaders_1; }
	inline void set__responseHeaders_1(Dictionary_2_t3943999495 * value)
	{
		____responseHeaders_1 = value;
		Il2CppCodeGenWriteBarrier((&____responseHeaders_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWW_T2919945039_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef STRINGCOMPARISON_T2376310518_H
#define STRINGCOMPARISON_T2376310518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringComparison
struct  StringComparison_t2376310518 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringComparison_t2376310518, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCOMPARISON_T2376310518_H
#ifndef ASYNCOPERATION_T3814632279_H
#define ASYNCOPERATION_T3814632279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t3814632279  : public YieldInstruction_t3462875981
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t3616431661 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t3814632279, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t3814632279, ___m_completeCallback_1)); }
	inline Action_1_t3616431661 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t3616431661 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t3616431661 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279_marshaled_pinvoke : public YieldInstruction_t3462875981_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279_marshaled_com : public YieldInstruction_t3462875981_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T3814632279_H
#ifndef ENUMERATOR_T969056901_H
#define ENUMERATOR_T969056901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>
struct  Enumerator_t969056901 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3943999495 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1701344717  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t969056901, ___dictionary_0)); }
	inline Dictionary_2_t3943999495 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3943999495 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3943999495 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t969056901, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t969056901, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t969056901, ___current_3)); }
	inline KeyValuePair_2_t1701344717  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1701344717 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1701344717  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T969056901_H
#ifndef DOWNLOADHANDLER_T1216180266_H
#define DOWNLOADHANDLER_T1216180266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_t1216180266  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_t1216180266, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1216180266_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1216180266_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // DOWNLOADHANDLER_T1216180266_H
#ifndef UPLOADHANDLER_T3552561393_H
#define UPLOADHANDLER_T3552561393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandler
struct  UploadHandler_t3552561393  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t3552561393, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t3552561393_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t3552561393_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // UPLOADHANDLER_T3552561393_H
#ifndef UNITYWEBREQUEST_T254341728_H
#define UNITYWEBREQUEST_T254341728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequest
struct  UnityWebRequest_t254341728  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_t254341728, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_t254341728, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_t254341728, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728_marshaled_com
{
	intptr_t ___m_Ptr_0;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2;
};
#endif // UNITYWEBREQUEST_T254341728_H
#ifndef ENUMERATOR_T3601534125_H
#define ENUMERATOR_T3601534125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t3601534125 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t2281509423 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t38854645  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___dictionary_0)); }
	inline Dictionary_2_t2281509423 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2281509423 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2281509423 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3601534125, ___current_3)); }
	inline KeyValuePair_2_t38854645  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t38854645 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t38854645  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3601534125_H
#ifndef DOWNLOADHANDLERBUFFER_T3443159558_H
#define DOWNLOADHANDLERBUFFER_T3443159558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandlerBuffer
struct  DownloadHandlerBuffer_t3443159558  : public DownloadHandler_t1216180266
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t3443159558_marshaled_pinvoke : public DownloadHandler_t1216180266_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t3443159558_marshaled_com : public DownloadHandler_t1216180266_marshaled_com
{
};
#endif // DOWNLOADHANDLERBUFFER_T3443159558_H
#ifndef UNITYWEBREQUESTASYNCOPERATION_T2821884557_H
#define UNITYWEBREQUESTASYNCOPERATION_T2821884557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct  UnityWebRequestAsyncOperation_t2821884557  : public AsyncOperation_t3814632279
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::m_webRequest
	UnityWebRequest_t254341728 * ___m_webRequest_2;

public:
	inline static int32_t get_offset_of_m_webRequest_2() { return static_cast<int32_t>(offsetof(UnityWebRequestAsyncOperation_t2821884557, ___m_webRequest_2)); }
	inline UnityWebRequest_t254341728 * get_m_webRequest_2() const { return ___m_webRequest_2; }
	inline UnityWebRequest_t254341728 ** get_address_of_m_webRequest_2() { return &___m_webRequest_2; }
	inline void set_m_webRequest_2(UnityWebRequest_t254341728 * value)
	{
		___m_webRequest_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_webRequest_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t2821884557_marshaled_pinvoke : public AsyncOperation_t3814632279_marshaled_pinvoke
{
	UnityWebRequest_t254341728_marshaled_pinvoke ___m_webRequest_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t2821884557_marshaled_com : public AsyncOperation_t3814632279_marshaled_com
{
	UnityWebRequest_t254341728_marshaled_com* ___m_webRequest_2;
};
#endif // UNITYWEBREQUESTASYNCOPERATION_T2821884557_H
#ifndef UPLOADHANDLERRAW_T3420491431_H
#define UPLOADHANDLERRAW_T3420491431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandlerRaw
struct  UploadHandlerRaw_t3420491431  : public UploadHandler_t3552561393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t3420491431_marshaled_pinvoke : public UploadHandler_t3552561393_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t3420491431_marshaled_com : public UploadHandler_t3552561393_marshaled_com
{
};
#endif // UPLOADHANDLERRAW_T3420491431_H
// System.Byte[]
struct ByteU5BU5D_t3397334013  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3601534125  Dictionary_2_GetEnumerator_m3077639147_gshared (Dictionary_2_t2281509423 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m1091361971_gshared (Enumerator_t3601534125 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m3385717033_gshared (KeyValuePair_2_t38854645 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  RuntimeObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3349738440_gshared (Enumerator_t3601534125 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1905011127_gshared (Enumerator_t3601534125 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(!0,!1)
extern "C"  void Dictionary_2_set_Item_m1004257024_gshared (Dictionary_2_t2281509423 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);

// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m1440815618 (CustomYieldInstruction_t1786092740 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Get(System.String)
extern "C"  UnityWebRequest_t254341728 * UnityWebRequest_Get_m2852300615 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest()
extern "C"  UnityWebRequestAsyncOperation_t2821884557 * UnityWebRequest_SendWebRequest_m3795216673 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UnityWebRequest::.ctor(System.String,System.String)
extern "C"  void UnityWebRequest__ctor_m3297950477 (UnityWebRequest_t254341728 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UploadHandlerRaw::.ctor(System.Byte[])
extern "C"  void UploadHandlerRaw__ctor_m30984343 (UploadHandlerRaw_t3420491431 * __this, ByteU5BU5D_t3397334013* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UploadHandler::set_contentType(System.String)
extern "C"  void UploadHandler_set_contentType_m1800831204 (UploadHandler_t3552561393 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UnityWebRequest::set_uploadHandler(UnityEngine.Networking.UploadHandler)
extern "C"  void UnityWebRequest_set_uploadHandler_m2350577399 (UnityWebRequest_t254341728 * __this, UploadHandler_t3552561393 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.DownloadHandlerBuffer::.ctor()
extern "C"  void DownloadHandlerBuffer__ctor_m2242528619 (DownloadHandlerBuffer_t3443159558 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UnityWebRequest::set_downloadHandler(UnityEngine.Networking.DownloadHandler)
extern "C"  void UnityWebRequest_set_downloadHandler_m3504922771 (UnityWebRequest_t254341728 * __this, DownloadHandler_t1216180266 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,System.String>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m1234042134(__this, method) ((  Enumerator_t969056901  (*) (Dictionary_2_t3943999495 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3077639147_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
#define Enumerator_get_Current_m648019172(__this, method) ((  KeyValuePair_2_t1701344717  (*) (Enumerator_t969056901 *, const RuntimeMethod*))Enumerator_get_Current_m1091361971_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m540686856(__this, method) ((  String_t* (*) (KeyValuePair_2_t1701344717 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m3385717033_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m551560767(__this, method) ((  String_t* (*) (KeyValuePair_2_t1701344717 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1251901674_gshared)(__this, method)
// System.Void UnityEngine.Networking.UnityWebRequest::SetRequestHeader(System.String,System.String)
extern "C"  void UnityWebRequest_SetRequestHeader_m1686880873 (UnityWebRequest_t254341728 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
#define Enumerator_MoveNext_m2794591321(__this, method) ((  bool (*) (Enumerator_t969056901 *, const RuntimeMethod*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
#define Enumerator_Dispose_m882561911(__this, method) ((  void (*) (Enumerator_t969056901 *, const RuntimeMethod*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
// System.Boolean UnityEngine.WWW::WaitUntilDoneIfPossible()
extern "C"  bool WWW_WaitUntilDoneIfPossible_m2158502638 (WWW_t2919945039 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
extern "C"  bool UnityWebRequest_get_isNetworkError_m1187301034 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
extern "C"  DownloadHandler_t1216180266 * UnityWebRequest_get_downloadHandler_m2452966308 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Networking.DownloadHandler::get_data()
extern "C"  ByteU5BU5D_t3397334013* DownloadHandler_get_data_m1774581504 (DownloadHandler_t1216180266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt64 UnityEngine.Networking.UnityWebRequest::get_downloadedBytes()
extern "C"  uint64_t UnityWebRequest_get_downloadedBytes_m2753884688 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isDone()
extern "C"  bool UnityWebRequest_get_isDone_m3430883570 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.UnityWebRequest::get_error()
extern "C"  String_t* UnityWebRequest_get_error_m714775961 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int64 UnityEngine.Networking.UnityWebRequest::get_responseCode()
extern "C"  int64_t UnityWebRequest_get_responseCode_m237434835 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::GetStatusCodeName(System.Int64)
extern "C"  String_t* WWW_GetStatusCodeName_m170717970 (WWW_t2919945039 * __this, int64_t ___statusCode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C"  String_t* String_Format_m1811873526 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WWW::get_isDone()
extern "C"  bool WWW_get_isDone_m720669976 (WWW_t2919945039 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.Networking.UnityWebRequest::GetResponseHeaders()
extern "C"  Dictionary_2_t3943999495 * UnityWebRequest_GetResponseHeaders_m2859494488 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1)
#define Dictionary_2_set_Item_m65971941(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3943999495 *, String_t*, String_t*, const RuntimeMethod*))Dictionary_2_set_Item_m1004257024_gshared)(__this, p0, p1, method)
// System.String UnityEngine.Networking.DownloadHandler::get_text()
extern "C"  String_t* DownloadHandler_get_text_m515133966 (DownloadHandler_t1216180266 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Networking.UnityWebRequest::get_uploadProgress()
extern "C"  float UnityWebRequest_get_uploadProgress_m1088271350 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Networking.UnityWebRequest::get_url()
extern "C"  String_t* UnityWebRequest_get_url_m575042514 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Networking.UnityWebRequest::Dispose()
extern "C"  void UnityWebRequest_Dispose_m2948426222 (UnityWebRequest_t254341728 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WWW::get_url()
extern "C"  String_t* WWW_get_url_m1322408312 (WWW_t2919945039 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::StartsWith(System.String,System.StringComparison)
extern "C"  bool String_StartsWith_m46695182 (String_t* __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3299155069 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.WWW::.ctor(System.String)
extern "C"  void WWW__ctor_m992904521 (WWW_t2919945039 * __this, String_t* ___url0, const RuntimeMethod* method)
{
	{
		CustomYieldInstruction__ctor_m1440815618(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___url0;
		UnityWebRequest_t254341728 * L_1 = UnityWebRequest_Get_m2852300615(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set__uwr_0(L_1);
		UnityWebRequest_t254341728 * L_2 = __this->get__uwr_0();
		NullCheck(L_2);
		UnityWebRequest_SendWebRequest_m3795216673(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WWW::.ctor(System.String,System.Byte[],System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void WWW__ctor_m2992943289 (WWW_t2919945039 * __this, String_t* ___url0, ByteU5BU5D_t3397334013* ___postData1, Dictionary_2_t3943999495 * ___headers2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW__ctor_m2992943289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	UploadHandler_t3552561393 * V_1 = NULL;
	KeyValuePair_2_t1701344717  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Enumerator_t969056901  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B3_0 = NULL;
	{
		CustomYieldInstruction__ctor_m1440815618(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_0 = ___postData1;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		G_B3_0 = _stringLiteral1596707798;
		goto IL_001c;
	}

IL_0017:
	{
		G_B3_0 = _stringLiteral782856060;
	}

IL_001c:
	{
		V_0 = G_B3_0;
		String_t* L_1 = ___url0;
		String_t* L_2 = V_0;
		UnityWebRequest_t254341728 * L_3 = (UnityWebRequest_t254341728 *)il2cpp_codegen_object_new(UnityWebRequest_t254341728_il2cpp_TypeInfo_var);
		UnityWebRequest__ctor_m3297950477(L_3, L_1, L_2, /*hidden argument*/NULL);
		__this->set__uwr_0(L_3);
		ByteU5BU5D_t3397334013* L_4 = ___postData1;
		UploadHandlerRaw_t3420491431 * L_5 = (UploadHandlerRaw_t3420491431 *)il2cpp_codegen_object_new(UploadHandlerRaw_t3420491431_il2cpp_TypeInfo_var);
		UploadHandlerRaw__ctor_m30984343(L_5, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		UploadHandler_t3552561393 * L_6 = V_1;
		NullCheck(L_6);
		UploadHandler_set_contentType_m1800831204(L_6, _stringLiteral730767724, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_7 = __this->get__uwr_0();
		UploadHandler_t3552561393 * L_8 = V_1;
		NullCheck(L_7);
		UnityWebRequest_set_uploadHandler_m2350577399(L_7, L_8, /*hidden argument*/NULL);
		UnityWebRequest_t254341728 * L_9 = __this->get__uwr_0();
		DownloadHandlerBuffer_t3443159558 * L_10 = (DownloadHandlerBuffer_t3443159558 *)il2cpp_codegen_object_new(DownloadHandlerBuffer_t3443159558_il2cpp_TypeInfo_var);
		DownloadHandlerBuffer__ctor_m2242528619(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityWebRequest_set_downloadHandler_m3504922771(L_9, L_10, /*hidden argument*/NULL);
		Dictionary_2_t3943999495 * L_11 = ___headers2;
		NullCheck(L_11);
		Enumerator_t969056901  L_12 = Dictionary_2_GetEnumerator_m1234042134(L_11, /*hidden argument*/Dictionary_2_GetEnumerator_m1234042134_RuntimeMethod_var);
		V_3 = L_12;
	}

IL_0060:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0086;
		}

IL_0065:
		{
			KeyValuePair_2_t1701344717  L_13 = Enumerator_get_Current_m648019172((&V_3), /*hidden argument*/Enumerator_get_Current_m648019172_RuntimeMethod_var);
			V_2 = L_13;
			UnityWebRequest_t254341728 * L_14 = __this->get__uwr_0();
			String_t* L_15 = KeyValuePair_2_get_Key_m540686856((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m540686856_RuntimeMethod_var);
			String_t* L_16 = KeyValuePair_2_get_Value_m551560767((&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m551560767_RuntimeMethod_var);
			NullCheck(L_14);
			UnityWebRequest_SetRequestHeader_m1686880873(L_14, L_15, L_16, /*hidden argument*/NULL);
		}

IL_0086:
		{
			bool L_17 = Enumerator_MoveNext_m2794591321((&V_3), /*hidden argument*/Enumerator_MoveNext_m2794591321_RuntimeMethod_var);
			if (L_17)
			{
				goto IL_0065;
			}
		}

IL_0092:
		{
			IL2CPP_LEAVE(0xA5, FINALLY_0097);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0097;
	}

FINALLY_0097:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m882561911((&V_3), /*hidden argument*/Enumerator_Dispose_m882561911_RuntimeMethod_var);
		IL2CPP_END_FINALLY(151)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(151)
	{
		IL2CPP_JUMP_TBL(0xA5, IL_00a5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a5:
	{
		UnityWebRequest_t254341728 * L_18 = __this->get__uwr_0();
		NullCheck(L_18);
		UnityWebRequest_SendWebRequest_m3795216673(L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Byte[] UnityEngine.WWW::get_bytes()
extern "C"  ByteU5BU5D_t3397334013* WWW_get_bytes_m3375427027 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_bytes_m3375427027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	DownloadHandler_t1216180266 * V_1 = NULL;
	{
		bool L_0 = WWW_WaitUntilDoneIfPossible_m2158502638(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)0));
		goto IL_0054;
	}

IL_0018:
	{
		UnityWebRequest_t254341728 * L_1 = __this->get__uwr_0();
		NullCheck(L_1);
		bool L_2 = UnityWebRequest_get_isNetworkError_m1187301034(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		V_0 = (ByteU5BU5D_t3397334013*)NULL;
		goto IL_0054;
	}

IL_002f:
	{
		UnityWebRequest_t254341728 * L_3 = __this->get__uwr_0();
		NullCheck(L_3);
		DownloadHandler_t1216180266 * L_4 = UnityWebRequest_get_downloadHandler_m2452966308(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		DownloadHandler_t1216180266 * L_5 = V_1;
		if (L_5)
		{
			goto IL_0048;
		}
	}
	{
		V_0 = (ByteU5BU5D_t3397334013*)NULL;
		goto IL_0054;
	}

IL_0048:
	{
		DownloadHandler_t1216180266 * L_6 = V_1;
		NullCheck(L_6);
		ByteU5BU5D_t3397334013* L_7 = DownloadHandler_get_data_m1774581504(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0054;
	}

IL_0054:
	{
		ByteU5BU5D_t3397334013* L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.WWW::get_bytesDownloaded()
extern "C"  int32_t WWW_get_bytesDownloaded_m346322524 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		UnityWebRequest_t254341728 * L_0 = __this->get__uwr_0();
		NullCheck(L_0);
		uint64_t L_1 = UnityWebRequest_get_downloadedBytes_m2753884688(L_0, /*hidden argument*/NULL);
		V_0 = (((int32_t)((int32_t)L_1)));
		goto IL_0013;
	}

IL_0013:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.WWW::get_error()
extern "C"  String_t* WWW_get_error_m2450520467 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_error_m2450520467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		UnityWebRequest_t254341728 * L_0 = __this->get__uwr_0();
		NullCheck(L_0);
		bool L_1 = UnityWebRequest_get_isDone_m3430883570(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		V_0 = (String_t*)NULL;
		goto IL_0087;
	}

IL_0018:
	{
		UnityWebRequest_t254341728 * L_2 = __this->get__uwr_0();
		NullCheck(L_2);
		bool L_3 = UnityWebRequest_get_isNetworkError_m1187301034(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0039;
		}
	}
	{
		UnityWebRequest_t254341728 * L_4 = __this->get__uwr_0();
		NullCheck(L_4);
		String_t* L_5 = UnityWebRequest_get_error_m714775961(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0087;
	}

IL_0039:
	{
		UnityWebRequest_t254341728 * L_6 = __this->get__uwr_0();
		NullCheck(L_6);
		int64_t L_7 = UnityWebRequest_get_responseCode_m237434835(L_6, /*hidden argument*/NULL);
		if ((((int64_t)L_7) < ((int64_t)(((int64_t)((int64_t)((int32_t)400)))))))
		{
			goto IL_0080;
		}
	}
	{
		UnityWebRequest_t254341728 * L_8 = __this->get__uwr_0();
		NullCheck(L_8);
		int64_t L_9 = UnityWebRequest_get_responseCode_m237434835(L_8, /*hidden argument*/NULL);
		int64_t L_10 = L_9;
		RuntimeObject * L_11 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_10);
		UnityWebRequest_t254341728 * L_12 = __this->get__uwr_0();
		NullCheck(L_12);
		int64_t L_13 = UnityWebRequest_get_responseCode_m237434835(L_12, /*hidden argument*/NULL);
		String_t* L_14 = WWW_GetStatusCodeName_m170717970(__this, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2512070489, L_11, L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		goto IL_0087;
	}

IL_0080:
	{
		V_0 = (String_t*)NULL;
		goto IL_0087;
	}

IL_0087:
	{
		String_t* L_16 = V_0;
		return L_16;
	}
}
// System.Boolean UnityEngine.WWW::get_isDone()
extern "C"  bool WWW_get_isDone_m720669976 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		UnityWebRequest_t254341728 * L_0 = __this->get__uwr_0();
		NullCheck(L_0);
		bool L_1 = UnityWebRequest_get_isDone_m3430883570(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> UnityEngine.WWW::get_responseHeaders()
extern "C"  Dictionary_2_t3943999495 * WWW_get_responseHeaders_m1819170249 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_responseHeaders_m1819170249_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3943999495 * V_0 = NULL;
	{
		bool L_0 = WWW_get_isDone_m720669976(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		UnityWebRequest_t254341728 * L_1 = __this->get__uwr_0();
		NullCheck(L_1);
		bool L_2 = UnityWebRequest_get_isNetworkError_m1187301034(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}

IL_001c:
	{
		V_0 = (Dictionary_2_t3943999495 *)NULL;
		goto IL_0088;
	}

IL_0023:
	{
		Dictionary_2_t3943999495 * L_3 = __this->get__responseHeaders_1();
		if (L_3)
		{
			goto IL_007c;
		}
	}
	{
		UnityWebRequest_t254341728 * L_4 = __this->get__uwr_0();
		NullCheck(L_4);
		Dictionary_2_t3943999495 * L_5 = UnityWebRequest_GetResponseHeaders_m2859494488(L_4, /*hidden argument*/NULL);
		__this->set__responseHeaders_1(L_5);
		Dictionary_2_t3943999495 * L_6 = __this->get__responseHeaders_1();
		UnityWebRequest_t254341728 * L_7 = __this->get__uwr_0();
		NullCheck(L_7);
		int64_t L_8 = UnityWebRequest_get_responseCode_m237434835(L_7, /*hidden argument*/NULL);
		int64_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Int64_t909078037_il2cpp_TypeInfo_var, &L_9);
		UnityWebRequest_t254341728 * L_11 = __this->get__uwr_0();
		NullCheck(L_11);
		int64_t L_12 = UnityWebRequest_get_responseCode_m237434835(L_11, /*hidden argument*/NULL);
		String_t* L_13 = WWW_GetStatusCodeName_m170717970(__this, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m1811873526(NULL /*static, unused*/, _stringLiteral2722398782, L_10, L_13, /*hidden argument*/NULL);
		NullCheck(L_6);
		Dictionary_2_set_Item_m65971941(L_6, _stringLiteral1794480464, L_14, /*hidden argument*/Dictionary_2_set_Item_m65971941_RuntimeMethod_var);
	}

IL_007c:
	{
		Dictionary_2_t3943999495 * L_15 = __this->get__responseHeaders_1();
		V_0 = L_15;
		goto IL_0088;
	}

IL_0088:
	{
		Dictionary_2_t3943999495 * L_16 = V_0;
		return L_16;
	}
}
// System.String UnityEngine.WWW::get_text()
extern "C"  String_t* WWW_get_text_m3992458586 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_get_text_m3992458586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	DownloadHandler_t1216180266 * V_1 = NULL;
	{
		bool L_0 = WWW_WaitUntilDoneIfPossible_m2158502638(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		V_0 = _stringLiteral371857150;
		goto IL_005b;
	}

IL_0017:
	{
		UnityWebRequest_t254341728 * L_1 = __this->get__uwr_0();
		NullCheck(L_1);
		bool L_2 = UnityWebRequest_get_isNetworkError_m1187301034(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		V_0 = _stringLiteral371857150;
		goto IL_005b;
	}

IL_0032:
	{
		UnityWebRequest_t254341728 * L_3 = __this->get__uwr_0();
		NullCheck(L_3);
		DownloadHandler_t1216180266 * L_4 = UnityWebRequest_get_downloadHandler_m2452966308(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		DownloadHandler_t1216180266 * L_5 = V_1;
		if (L_5)
		{
			goto IL_004f;
		}
	}
	{
		V_0 = _stringLiteral371857150;
		goto IL_005b;
	}

IL_004f:
	{
		DownloadHandler_t1216180266 * L_6 = V_1;
		NullCheck(L_6);
		String_t* L_7 = DownloadHandler_get_text_m515133966(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_005b;
	}

IL_005b:
	{
		String_t* L_8 = V_0;
		return L_8;
	}
}
// System.Single UnityEngine.WWW::get_uploadProgress()
extern "C"  float WWW_get_uploadProgress_m1484023030 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		UnityWebRequest_t254341728 * L_0 = __this->get__uwr_0();
		NullCheck(L_0);
		float L_1 = UnityWebRequest_get_uploadProgress_m1088271350(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_001e;
		}
	}
	{
		V_0 = (0.0f);
	}

IL_001e:
	{
		float L_3 = V_0;
		V_1 = L_3;
		goto IL_0025;
	}

IL_0025:
	{
		float L_4 = V_1;
		return L_4;
	}
}
// System.String UnityEngine.WWW::get_url()
extern "C"  String_t* WWW_get_url_m1322408312 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		UnityWebRequest_t254341728 * L_0 = __this->get__uwr_0();
		NullCheck(L_0);
		String_t* L_1 = UnityWebRequest_get_url_m575042514(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.WWW::get_keepWaiting()
extern "C"  bool WWW_get_keepWaiting_m3934668334 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		UnityWebRequest_t254341728 * L_0 = __this->get__uwr_0();
		NullCheck(L_0);
		bool L_1 = UnityWebRequest_get_isDone_m3430883570(L_0, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.WWW::Dispose()
extern "C"  void WWW_Dispose_m630585886 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	{
		UnityWebRequest_t254341728 * L_0 = __this->get__uwr_0();
		NullCheck(L_0);
		UnityWebRequest_Dispose_m2948426222(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.WWW::WaitUntilDoneIfPossible()
extern "C"  bool WWW_WaitUntilDoneIfPossible_m2158502638 (WWW_t2919945039 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_WaitUntilDoneIfPossible_m2158502638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		UnityWebRequest_t254341728 * L_0 = __this->get__uwr_0();
		NullCheck(L_0);
		bool L_1 = UnityWebRequest_get_isDone_m3430883570(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_005f;
	}

IL_0018:
	{
		String_t* L_2 = WWW_get_url_m1322408312(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = String_StartsWith_m46695182(L_2, _stringLiteral2494949592, 5, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004d;
		}
	}
	{
		goto IL_0036;
	}

IL_0034:
	{
	}

IL_0036:
	{
		UnityWebRequest_t254341728 * L_4 = __this->get__uwr_0();
		NullCheck(L_4);
		bool L_5 = UnityWebRequest_get_isDone_m3430883570(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_005f;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral309755548, /*hidden argument*/NULL);
		V_0 = (bool)0;
		goto IL_005f;
	}

IL_005f:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.String UnityEngine.WWW::GetStatusCodeName(System.Int64)
extern "C"  String_t* WWW_GetStatusCodeName_m170717970 (WWW_t2919945039 * __this, int64_t ___statusCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WWW_GetStatusCodeName_m170717970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		int64_t L_0 = ___statusCode0;
		if ((((int64_t)L_0) < ((int64_t)(((int64_t)((int64_t)((int32_t)400)))))))
		{
			goto IL_006b;
		}
	}
	{
		int64_t L_1 = ___statusCode0;
		if ((((int64_t)L_1) > ((int64_t)(((int64_t)((int64_t)((int32_t)416)))))))
		{
			goto IL_006b;
		}
	}
	{
		int64_t L_2 = ___statusCode0;
		switch ((((int32_t)((int32_t)((int64_t)((int64_t)L_2-(int64_t)(((int64_t)((int64_t)((int32_t)400))))))))))
		{
			case 0:
			{
				goto IL_01d9;
			}
			case 1:
			{
				goto IL_01e4;
			}
			case 2:
			{
				goto IL_01ef;
			}
			case 3:
			{
				goto IL_01fa;
			}
			case 4:
			{
				goto IL_0205;
			}
			case 5:
			{
				goto IL_0210;
			}
			case 6:
			{
				goto IL_021b;
			}
			case 7:
			{
				goto IL_0226;
			}
			case 8:
			{
				goto IL_0231;
			}
			case 9:
			{
				goto IL_023c;
			}
			case 10:
			{
				goto IL_0247;
			}
			case 11:
			{
				goto IL_0252;
			}
			case 12:
			{
				goto IL_025d;
			}
			case 13:
			{
				goto IL_0268;
			}
			case 14:
			{
				goto IL_0273;
			}
			case 15:
			{
				goto IL_027e;
			}
			case 16:
			{
				goto IL_0289;
			}
		}
	}

IL_006b:
	{
		int64_t L_3 = ___statusCode0;
		if ((((int64_t)L_3) < ((int64_t)(((int64_t)((int64_t)((int32_t)200)))))))
		{
			goto IL_00ad;
		}
	}
	{
		int64_t L_4 = ___statusCode0;
		if ((((int64_t)L_4) > ((int64_t)(((int64_t)((int64_t)((int32_t)206)))))))
		{
			goto IL_00ad;
		}
	}
	{
		int64_t L_5 = ___statusCode0;
		switch ((((int32_t)((int32_t)((int64_t)((int64_t)L_5-(int64_t)(((int64_t)((int64_t)((int32_t)200))))))))))
		{
			case 0:
			{
				goto IL_013f;
			}
			case 1:
			{
				goto IL_014a;
			}
			case 2:
			{
				goto IL_0155;
			}
			case 3:
			{
				goto IL_0160;
			}
			case 4:
			{
				goto IL_016b;
			}
			case 5:
			{
				goto IL_0176;
			}
			case 6:
			{
				goto IL_0181;
			}
		}
	}

IL_00ad:
	{
		int64_t L_6 = ___statusCode0;
		if ((((int64_t)L_6) < ((int64_t)(((int64_t)((int64_t)((int32_t)300)))))))
		{
			goto IL_00f3;
		}
	}
	{
		int64_t L_7 = ___statusCode0;
		if ((((int64_t)L_7) > ((int64_t)(((int64_t)((int64_t)((int32_t)307)))))))
		{
			goto IL_00f3;
		}
	}
	{
		int64_t L_8 = ___statusCode0;
		switch ((((int32_t)((int32_t)((int64_t)((int64_t)L_8-(int64_t)(((int64_t)((int64_t)((int32_t)300))))))))))
		{
			case 0:
			{
				goto IL_018c;
			}
			case 1:
			{
				goto IL_0197;
			}
			case 2:
			{
				goto IL_01a2;
			}
			case 3:
			{
				goto IL_01ad;
			}
			case 4:
			{
				goto IL_01b8;
			}
			case 5:
			{
				goto IL_01c3;
			}
			case 6:
			{
				goto IL_00f3;
			}
			case 7:
			{
				goto IL_01ce;
			}
		}
	}

IL_00f3:
	{
		int64_t L_9 = ___statusCode0;
		if ((((int64_t)L_9) < ((int64_t)(((int64_t)((int64_t)((int32_t)500)))))))
		{
			goto IL_0131;
		}
	}
	{
		int64_t L_10 = ___statusCode0;
		if ((((int64_t)L_10) > ((int64_t)(((int64_t)((int64_t)((int32_t)505)))))))
		{
			goto IL_0131;
		}
	}
	{
		int64_t L_11 = ___statusCode0;
		switch ((((int32_t)((int32_t)((int64_t)((int64_t)L_11-(int64_t)(((int64_t)((int64_t)((int32_t)500))))))))))
		{
			case 0:
			{
				goto IL_029f;
			}
			case 1:
			{
				goto IL_02aa;
			}
			case 2:
			{
				goto IL_02b5;
			}
			case 3:
			{
				goto IL_02c0;
			}
			case 4:
			{
				goto IL_02cb;
			}
			case 5:
			{
				goto IL_02d6;
			}
		}
	}

IL_0131:
	{
		int64_t L_12 = ___statusCode0;
		if ((((int64_t)L_12) == ((int64_t)(((int64_t)((int64_t)((int32_t)41)))))))
		{
			goto IL_0294;
		}
	}
	{
		goto IL_02e1;
	}

IL_013f:
	{
		V_0 = _stringLiteral2041362128;
		goto IL_02ec;
	}

IL_014a:
	{
		V_0 = _stringLiteral75521586;
		goto IL_02ec;
	}

IL_0155:
	{
		V_0 = _stringLiteral3831553393;
		goto IL_02ec;
	}

IL_0160:
	{
		V_0 = _stringLiteral1598235917;
		goto IL_02ec;
	}

IL_016b:
	{
		V_0 = _stringLiteral1042792716;
		goto IL_02ec;
	}

IL_0176:
	{
		V_0 = _stringLiteral3901348444;
		goto IL_02ec;
	}

IL_0181:
	{
		V_0 = _stringLiteral2135358226;
		goto IL_02ec;
	}

IL_018c:
	{
		V_0 = _stringLiteral982223462;
		goto IL_02ec;
	}

IL_0197:
	{
		V_0 = _stringLiteral2038575240;
		goto IL_02ec;
	}

IL_01a2:
	{
		V_0 = _stringLiteral1554773766;
		goto IL_02ec;
	}

IL_01ad:
	{
		V_0 = _stringLiteral3465782523;
		goto IL_02ec;
	}

IL_01b8:
	{
		V_0 = _stringLiteral523764538;
		goto IL_02ec;
	}

IL_01c3:
	{
		V_0 = _stringLiteral3551656767;
		goto IL_02ec;
	}

IL_01ce:
	{
		V_0 = _stringLiteral2809809645;
		goto IL_02ec;
	}

IL_01d9:
	{
		V_0 = _stringLiteral1283101652;
		goto IL_02ec;
	}

IL_01e4:
	{
		V_0 = _stringLiteral3248504360;
		goto IL_02ec;
	}

IL_01ef:
	{
		V_0 = _stringLiteral3161377847;
		goto IL_02ec;
	}

IL_01fa:
	{
		V_0 = _stringLiteral1899870879;
		goto IL_02ec;
	}

IL_0205:
	{
		V_0 = _stringLiteral3643225343;
		goto IL_02ec;
	}

IL_0210:
	{
		V_0 = _stringLiteral62823554;
		goto IL_02ec;
	}

IL_021b:
	{
		V_0 = _stringLiteral663334555;
		goto IL_02ec;
	}

IL_0226:
	{
		V_0 = _stringLiteral3795604005;
		goto IL_02ec;
	}

IL_0231:
	{
		V_0 = _stringLiteral3921440208;
		goto IL_02ec;
	}

IL_023c:
	{
		V_0 = _stringLiteral4253801532;
		goto IL_02ec;
	}

IL_0247:
	{
		V_0 = _stringLiteral1414245911;
		goto IL_02ec;
	}

IL_0252:
	{
		V_0 = _stringLiteral788780341;
		goto IL_02ec;
	}

IL_025d:
	{
		V_0 = _stringLiteral146775263;
		goto IL_02ec;
	}

IL_0268:
	{
		V_0 = _stringLiteral922626835;
		goto IL_02ec;
	}

IL_0273:
	{
		V_0 = _stringLiteral3230308904;
		goto IL_02ec;
	}

IL_027e:
	{
		V_0 = _stringLiteral652983053;
		goto IL_02ec;
	}

IL_0289:
	{
		V_0 = _stringLiteral618349143;
		goto IL_02ec;
	}

IL_0294:
	{
		V_0 = _stringLiteral1214181781;
		goto IL_02ec;
	}

IL_029f:
	{
		V_0 = _stringLiteral2640244280;
		goto IL_02ec;
	}

IL_02aa:
	{
		V_0 = _stringLiteral46458807;
		goto IL_02ec;
	}

IL_02b5:
	{
		V_0 = _stringLiteral4093720035;
		goto IL_02ec;
	}

IL_02c0:
	{
		V_0 = _stringLiteral2585140355;
		goto IL_02ec;
	}

IL_02cb:
	{
		V_0 = _stringLiteral1232914839;
		goto IL_02ec;
	}

IL_02d6:
	{
		V_0 = _stringLiteral2631285777;
		goto IL_02ec;
	}

IL_02e1:
	{
		V_0 = _stringLiteral371857150;
		goto IL_02ec;
	}

IL_02ec:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
