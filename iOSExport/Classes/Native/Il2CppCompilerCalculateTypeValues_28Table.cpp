﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// TestScript
struct TestScript_t905662927;
// TMPro.Examples.TeleType
struct TeleType_t2513439854;
// Scroller
struct Scroller_t2879750794;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// TMPro.Examples.Benchmark01
struct Benchmark01_t2768175604;
// TMPro.TMP_Text
struct TMP_Text_t1920000777;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t2849466151;
// TMPro.Examples.TextConsoleSimulator
struct TextConsoleSimulator_t2207663326;
// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t2679013775;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0
struct U3CAnimateVertexColorsU3Ec__Iterator0_t76635731;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t3449578277;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String[][]
struct StringU5BU5DU5BU5D_t2190260861;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// UnityEngine.Vector3[][]
struct Vector3U5BU5DU5BU5D_t1477540408;
// TMPro.Examples.VertexShakeB
struct VertexShakeB_t3019979320;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// TMPro.Examples.VertexColorCycler
struct VertexColorCycler_t3471086701;
// TMPro.Examples.VertexShakeA
struct VertexShakeA_t3019979321;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t6181308;
// TMPro.Examples.VertexJitter/VertexAnim[]
struct VertexAnimU5BU5D_t1450067904;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t2398608976;
// TMPro.Examples.VertexJitter
struct VertexJitter_t3970559362;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// TMPro.Examples.VertexZoom
struct VertexZoom_t3463934435;
// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1
struct U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// TMPro.Examples.WarpTextExample
struct WarpTextExample_t250871869;
// TMPro.Examples.SkewTextExample
struct SkewTextExample_t3378890949;
// EnvMapAnimator
struct EnvMapAnimator_t1635389402;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t2530419979;
// UnityEngine.Font
struct Font_t4239498691;
// TMPro.TextMeshPro
struct TextMeshPro_t2521834357;
// TMPro.TextContainer
struct TextContainer_t4263764796;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Canvas
struct Canvas_t209405766;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t934157183;
// UnityEngine.UI.Text
struct Text_t356221433;
// CSV.CSVReader
struct CSVReader_t2661561169;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t10059812;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// MapCreator
struct MapCreator_t2101643938;
// UnityEngine.Camera
struct Camera_t189460977;
// RacingWidgets
struct RacingWidgets_t915614521;
// UnityEngine.Transform
struct Transform_t3275118058;
// TMPro.TMP_InputField
struct TMP_InputField_t1778301588;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t3685274804;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.UI.Image
struct Image_t2042527209;
// EntryLink/NoSig
struct NoSig_t75605465;
// BezierPath
struct BezierPath_t978602816;
// Amazon.S3.IAmazonS3
struct IAmazonS3_t196022331;
// Amazon.Runtime.AmazonServiceCallback`2<Amazon.S3.Model.GetObjectRequest,Amazon.S3.Model.GetObjectResponse>
struct AmazonServiceCallback_2_t3959979905;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.Animator
struct Animator_t69676727;
// GForce
struct GForce_t1402080092;
// PathCreator
struct PathCreator_t1368626459;
// RoadCreator
struct RoadCreator_t1201856826;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// TMPro.TMP_TextEventHandler
struct TMP_TextEventHandler_t1517844021;
// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct CharacterSelectionEvent_t2887241789;
// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct WordSelectionEvent_t2871480376;
// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct LineSelectionEvent_t3265414486;
// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct LinkSelectionEvent_t3735310088;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADVIDEOU3EC__ITERATOR3_T4124372415_H
#define U3CLOADVIDEOU3EC__ITERATOR3_T4124372415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestScript/<LoadVideo>c__Iterator3
struct  U3CLoadVideoU3Ec__Iterator3_t4124372415  : public RuntimeObject
{
public:
	// System.String TestScript/<LoadVideo>c__Iterator3::videoURL
	String_t* ___videoURL_0;
	// TestScript TestScript/<LoadVideo>c__Iterator3::$this
	TestScript_t905662927 * ___U24this_1;
	// System.Object TestScript/<LoadVideo>c__Iterator3::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TestScript/<LoadVideo>c__Iterator3::$disposing
	bool ___U24disposing_3;
	// System.Int32 TestScript/<LoadVideo>c__Iterator3::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_videoURL_0() { return static_cast<int32_t>(offsetof(U3CLoadVideoU3Ec__Iterator3_t4124372415, ___videoURL_0)); }
	inline String_t* get_videoURL_0() const { return ___videoURL_0; }
	inline String_t** get_address_of_videoURL_0() { return &___videoURL_0; }
	inline void set_videoURL_0(String_t* value)
	{
		___videoURL_0 = value;
		Il2CppCodeGenWriteBarrier((&___videoURL_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadVideoU3Ec__Iterator3_t4124372415, ___U24this_1)); }
	inline TestScript_t905662927 * get_U24this_1() const { return ___U24this_1; }
	inline TestScript_t905662927 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TestScript_t905662927 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadVideoU3Ec__Iterator3_t4124372415, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadVideoU3Ec__Iterator3_t4124372415, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadVideoU3Ec__Iterator3_t4124372415, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADVIDEOU3EC__ITERATOR3_T4124372415_H
#ifndef U3CLOADVIDEOFILEU3EC__ITERATOR4_T2557724258_H
#define U3CLOADVIDEOFILEU3EC__ITERATOR4_T2557724258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestScript/<LoadVideoFile>c__Iterator4
struct  U3CLoadVideoFileU3Ec__Iterator4_t2557724258  : public RuntimeObject
{
public:
	// TestScript TestScript/<LoadVideoFile>c__Iterator4::$this
	TestScript_t905662927 * ___U24this_0;
	// System.Object TestScript/<LoadVideoFile>c__Iterator4::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean TestScript/<LoadVideoFile>c__Iterator4::$disposing
	bool ___U24disposing_2;
	// System.Int32 TestScript/<LoadVideoFile>c__Iterator4::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLoadVideoFileU3Ec__Iterator4_t2557724258, ___U24this_0)); }
	inline TestScript_t905662927 * get_U24this_0() const { return ___U24this_0; }
	inline TestScript_t905662927 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(TestScript_t905662927 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLoadVideoFileU3Ec__Iterator4_t2557724258, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLoadVideoFileU3Ec__Iterator4_t2557724258, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLoadVideoFileU3Ec__Iterator4_t2557724258, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADVIDEOFILEU3EC__ITERATOR4_T2557724258_H
#ifndef U3CSTARTU3EC__ITERATOR0_T189980609_H
#define U3CSTARTU3EC__ITERATOR0_T189980609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t189980609  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_0;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<counter>__0
	int32_t ___U3CcounterU3E__0_1;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_2;
	// TMPro.Examples.TeleType TMPro.Examples.TeleType/<Start>c__Iterator0::$this
	TeleType_t2513439854 * ___U24this_3;
	// System.Object TMPro.Examples.TeleType/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TMPro.Examples.TeleType/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 TMPro.Examples.TeleType/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U3CtotalVisibleCharactersU3E__0_0)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_0() const { return ___U3CtotalVisibleCharactersU3E__0_0; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_0() { return &___U3CtotalVisibleCharactersU3E__0_0; }
	inline void set_U3CtotalVisibleCharactersU3E__0_0(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U3CcounterU3E__0_1)); }
	inline int32_t get_U3CcounterU3E__0_1() const { return ___U3CcounterU3E__0_1; }
	inline int32_t* get_address_of_U3CcounterU3E__0_1() { return &___U3CcounterU3E__0_1; }
	inline void set_U3CcounterU3E__0_1(int32_t value)
	{
		___U3CcounterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U3CvisibleCountU3E__0_2)); }
	inline int32_t get_U3CvisibleCountU3E__0_2() const { return ___U3CvisibleCountU3E__0_2; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_2() { return &___U3CvisibleCountU3E__0_2; }
	inline void set_U3CvisibleCountU3E__0_2(int32_t value)
	{
		___U3CvisibleCountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U24this_3)); }
	inline TeleType_t2513439854 * get_U24this_3() const { return ___U24this_3; }
	inline TeleType_t2513439854 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TeleType_t2513439854 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t189980609, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T189980609_H
#ifndef RENDEREREXTENSIONS_T1168630521_H
#define RENDEREREXTENSIONS_T1168630521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RendererExtensions
struct  RendererExtensions_t1168630521  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDEREREXTENSIONS_T1168630521_H
#ifndef U3CGETSIZEU3EC__ITERATOR0_T1957153935_H
#define U3CGETSIZEU3EC__ITERATOR0_T1957153935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Scroller/<GetSize>c__Iterator0
struct  U3CGetSizeU3Ec__Iterator0_t1957153935  : public RuntimeObject
{
public:
	// Scroller Scroller/<GetSize>c__Iterator0::$this
	Scroller_t2879750794 * ___U24this_0;
	// System.Object Scroller/<GetSize>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Scroller/<GetSize>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Scroller/<GetSize>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CGetSizeU3Ec__Iterator0_t1957153935, ___U24this_0)); }
	inline Scroller_t2879750794 * get_U24this_0() const { return ___U24this_0; }
	inline Scroller_t2879750794 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(Scroller_t2879750794 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CGetSizeU3Ec__Iterator0_t1957153935, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CGetSizeU3Ec__Iterator0_t1957153935, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CGetSizeU3Ec__Iterator0_t1957153935, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSIZEU3EC__ITERATOR0_T1957153935_H
#ifndef U3CLOADFILESU3EC__ITERATOR0_T2452679976_H
#define U3CLOADFILESU3EC__ITERATOR0_T2452679976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestScript/<LoadFiles>c__Iterator0
struct  U3CLoadFilesU3Ec__Iterator0_t2452679976  : public RuntimeObject
{
public:
	// System.String TestScript/<LoadFiles>c__Iterator0::csvURL
	String_t* ___csvURL_0;
	// UnityEngine.Coroutine TestScript/<LoadFiles>c__Iterator0::<loadCSV>__0
	Coroutine_t2299508840 * ___U3CloadCSVU3E__0_1;
	// System.String TestScript/<LoadFiles>c__Iterator0::videoURL
	String_t* ___videoURL_2;
	// UnityEngine.Coroutine TestScript/<LoadFiles>c__Iterator0::<loadVideo>__0
	Coroutine_t2299508840 * ___U3CloadVideoU3E__0_3;
	// TestScript TestScript/<LoadFiles>c__Iterator0::$this
	TestScript_t905662927 * ___U24this_4;
	// System.Object TestScript/<LoadFiles>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TestScript/<LoadFiles>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 TestScript/<LoadFiles>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_csvURL_0() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator0_t2452679976, ___csvURL_0)); }
	inline String_t* get_csvURL_0() const { return ___csvURL_0; }
	inline String_t** get_address_of_csvURL_0() { return &___csvURL_0; }
	inline void set_csvURL_0(String_t* value)
	{
		___csvURL_0 = value;
		Il2CppCodeGenWriteBarrier((&___csvURL_0), value);
	}

	inline static int32_t get_offset_of_U3CloadCSVU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator0_t2452679976, ___U3CloadCSVU3E__0_1)); }
	inline Coroutine_t2299508840 * get_U3CloadCSVU3E__0_1() const { return ___U3CloadCSVU3E__0_1; }
	inline Coroutine_t2299508840 ** get_address_of_U3CloadCSVU3E__0_1() { return &___U3CloadCSVU3E__0_1; }
	inline void set_U3CloadCSVU3E__0_1(Coroutine_t2299508840 * value)
	{
		___U3CloadCSVU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadCSVU3E__0_1), value);
	}

	inline static int32_t get_offset_of_videoURL_2() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator0_t2452679976, ___videoURL_2)); }
	inline String_t* get_videoURL_2() const { return ___videoURL_2; }
	inline String_t** get_address_of_videoURL_2() { return &___videoURL_2; }
	inline void set_videoURL_2(String_t* value)
	{
		___videoURL_2 = value;
		Il2CppCodeGenWriteBarrier((&___videoURL_2), value);
	}

	inline static int32_t get_offset_of_U3CloadVideoU3E__0_3() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator0_t2452679976, ___U3CloadVideoU3E__0_3)); }
	inline Coroutine_t2299508840 * get_U3CloadVideoU3E__0_3() const { return ___U3CloadVideoU3E__0_3; }
	inline Coroutine_t2299508840 ** get_address_of_U3CloadVideoU3E__0_3() { return &___U3CloadVideoU3E__0_3; }
	inline void set_U3CloadVideoU3E__0_3(Coroutine_t2299508840 * value)
	{
		___U3CloadVideoU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadVideoU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator0_t2452679976, ___U24this_4)); }
	inline TestScript_t905662927 * get_U24this_4() const { return ___U24this_4; }
	inline TestScript_t905662927 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TestScript_t905662927 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator0_t2452679976, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator0_t2452679976, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CLoadFilesU3Ec__Iterator0_t2452679976, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFILESU3EC__ITERATOR0_T2452679976_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1060020671_H
#define U3CSTARTU3EC__ITERATOR0_T1060020671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1060020671  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$this
	Benchmark01_t2768175604 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U24this_1)); }
	inline Benchmark01_t2768175604 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_t2768175604 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_t2768175604 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1060020671, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1060020671_H
#ifndef U3CREVEALCHARACTERSU3EC__ITERATOR0_T1407882744_H
#define U3CREVEALCHARACTERSU3EC__ITERATOR0_T1407882744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0
struct  U3CRevealCharactersU3Ec__Iterator0_t1407882744  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::textComponent
	TMP_Text_t1920000777 * ___textComponent_0;
	// TMPro.TMP_TextInfo TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_3;
	// TMPro.Examples.TextConsoleSimulator TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$this
	TextConsoleSimulator_t2207663326 * ___U24this_4;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealCharacters>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___textComponent_0)); }
	inline TMP_Text_t1920000777 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t1920000777 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t1920000777 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U3CtextInfoU3E__0_1)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__0_1() const { return ___U3CtextInfoU3E__0_1; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__0_1() { return &___U3CtextInfoU3E__0_1; }
	inline void set_U3CtextInfoU3E__0_1(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U3CvisibleCountU3E__0_3)); }
	inline int32_t get_U3CvisibleCountU3E__0_3() const { return ___U3CvisibleCountU3E__0_3; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_3() { return &___U3CvisibleCountU3E__0_3; }
	inline void set_U3CvisibleCountU3E__0_3(int32_t value)
	{
		___U3CvisibleCountU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U24this_4)); }
	inline TextConsoleSimulator_t2207663326 * get_U24this_4() const { return ___U24this_4; }
	inline TextConsoleSimulator_t2207663326 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TextConsoleSimulator_t2207663326 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRevealCharactersU3Ec__Iterator0_t1407882744, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALCHARACTERSU3EC__ITERATOR0_T1407882744_H
#ifndef U3CANIMATEPROPERTIESU3EC__ITERATOR0_T35148318_H
#define U3CANIMATEPROPERTIESU3EC__ITERATOR0_T35148318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0
struct  U3CAnimatePropertiesU3Ec__Iterator0_t35148318  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::<glowPower>__1
	float ___U3CglowPowerU3E__1_0;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$this
	ShaderPropAnimator_t2679013775 * ___U24this_1;
	// System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CglowPowerU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U3CglowPowerU3E__1_0)); }
	inline float get_U3CglowPowerU3E__1_0() const { return ___U3CglowPowerU3E__1_0; }
	inline float* get_address_of_U3CglowPowerU3E__1_0() { return &___U3CglowPowerU3E__1_0; }
	inline void set_U3CglowPowerU3E__1_0(float value)
	{
		___U3CglowPowerU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U24this_1)); }
	inline ShaderPropAnimator_t2679013775 * get_U24this_1() const { return ___U24this_1; }
	inline ShaderPropAnimator_t2679013775 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShaderPropAnimator_t2679013775 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t35148318, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEPROPERTIESU3EC__ITERATOR0_T35148318_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T3069048289_H
#define U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T3069048289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1
struct  U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single> TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::modifiedCharScale
	List_1_t1445631064 * ___modifiedCharScale_0;
	// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1::<>f__ref$0
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_modifiedCharScale_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289, ___modifiedCharScale_0)); }
	inline List_1_t1445631064 * get_modifiedCharScale_0() const { return ___modifiedCharScale_0; }
	inline List_1_t1445631064 ** get_address_of_modifiedCharScale_0() { return &___modifiedCharScale_0; }
	inline void set_modifiedCharScale_0(List_1_t1445631064 * value)
	{
		___modifiedCharScale_0 = value;
		Il2CppCodeGenWriteBarrier((&___modifiedCharScale_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289, ___U3CU3Ef__refU240_1)); }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t76635731 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CAnimateVertexColorsU3Ec__Iterator0_t76635731 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ANONSTOREY1_T3069048289_H
#ifndef U3CSTARTU3EC__ITERATOR0_T17013742_H
#define U3CSTARTU3EC__ITERATOR0_T17013742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t17013742  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$this
	Benchmark01_UGUI_t3449578277 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U24this_1)); }
	inline Benchmark01_UGUI_t3449578277 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_UGUI_t3449578277 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_UGUI_t3449578277 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t17013742, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T17013742_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CREVEALWORDSU3EC__ITERATOR1_T2877888826_H
#define U3CREVEALWORDSU3EC__ITERATOR1_T2877888826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1
struct  U3CRevealWordsU3Ec__Iterator1_t2877888826  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::textComponent
	TMP_Text_t1920000777 * ___textComponent_0;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalWordCount>__0
	int32_t ___U3CtotalWordCountU3E__0_1;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<totalVisibleCharacters>__0
	int32_t ___U3CtotalVisibleCharactersU3E__0_2;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<counter>__0
	int32_t ___U3CcounterU3E__0_3;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<currentWord>__0
	int32_t ___U3CcurrentWordU3E__0_4;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::<visibleCount>__0
	int32_t ___U3CvisibleCountU3E__0_5;
	// System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.TextConsoleSimulator/<RevealWords>c__Iterator1::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___textComponent_0)); }
	inline TMP_Text_t1920000777 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t1920000777 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t1920000777 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_U3CtotalWordCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CtotalWordCountU3E__0_1)); }
	inline int32_t get_U3CtotalWordCountU3E__0_1() const { return ___U3CtotalWordCountU3E__0_1; }
	inline int32_t* get_address_of_U3CtotalWordCountU3E__0_1() { return &___U3CtotalWordCountU3E__0_1; }
	inline void set_U3CtotalWordCountU3E__0_1(int32_t value)
	{
		___U3CtotalWordCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtotalVisibleCharactersU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CtotalVisibleCharactersU3E__0_2)); }
	inline int32_t get_U3CtotalVisibleCharactersU3E__0_2() const { return ___U3CtotalVisibleCharactersU3E__0_2; }
	inline int32_t* get_address_of_U3CtotalVisibleCharactersU3E__0_2() { return &___U3CtotalVisibleCharactersU3E__0_2; }
	inline void set_U3CtotalVisibleCharactersU3E__0_2(int32_t value)
	{
		___U3CtotalVisibleCharactersU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcounterU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CcounterU3E__0_3)); }
	inline int32_t get_U3CcounterU3E__0_3() const { return ___U3CcounterU3E__0_3; }
	inline int32_t* get_address_of_U3CcounterU3E__0_3() { return &___U3CcounterU3E__0_3; }
	inline void set_U3CcounterU3E__0_3(int32_t value)
	{
		___U3CcounterU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentWordU3E__0_4() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CcurrentWordU3E__0_4)); }
	inline int32_t get_U3CcurrentWordU3E__0_4() const { return ___U3CcurrentWordU3E__0_4; }
	inline int32_t* get_address_of_U3CcurrentWordU3E__0_4() { return &___U3CcurrentWordU3E__0_4; }
	inline void set_U3CcurrentWordU3E__0_4(int32_t value)
	{
		___U3CcurrentWordU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CvisibleCountU3E__0_5() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U3CvisibleCountU3E__0_5)); }
	inline int32_t get_U3CvisibleCountU3E__0_5() const { return ___U3CvisibleCountU3E__0_5; }
	inline int32_t* get_address_of_U3CvisibleCountU3E__0_5() { return &___U3CvisibleCountU3E__0_5; }
	inline void set_U3CvisibleCountU3E__0_5(int32_t value)
	{
		___U3CvisibleCountU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRevealWordsU3Ec__Iterator1_t2877888826, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREVEALWORDSU3EC__ITERATOR1_T2877888826_H
#ifndef U3CPREPAREFILESU3EC__ITERATOR1_T2748620044_H
#define U3CPREPAREFILESU3EC__ITERATOR1_T2748620044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestScript/<PrepareFiles>c__Iterator1
struct  U3CPrepareFilesU3Ec__Iterator1_t2748620044  : public RuntimeObject
{
public:
	// System.String TestScript/<PrepareFiles>c__Iterator1::csvURL
	String_t* ___csvURL_0;
	// UnityEngine.Coroutine TestScript/<PrepareFiles>c__Iterator1::<loadCSV>__0
	Coroutine_t2299508840 * ___U3CloadCSVU3E__0_1;
	// UnityEngine.Coroutine TestScript/<PrepareFiles>c__Iterator1::<loadVideo>__0
	Coroutine_t2299508840 * ___U3CloadVideoU3E__0_2;
	// TestScript TestScript/<PrepareFiles>c__Iterator1::$this
	TestScript_t905662927 * ___U24this_3;
	// System.Object TestScript/<PrepareFiles>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TestScript/<PrepareFiles>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 TestScript/<PrepareFiles>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_csvURL_0() { return static_cast<int32_t>(offsetof(U3CPrepareFilesU3Ec__Iterator1_t2748620044, ___csvURL_0)); }
	inline String_t* get_csvURL_0() const { return ___csvURL_0; }
	inline String_t** get_address_of_csvURL_0() { return &___csvURL_0; }
	inline void set_csvURL_0(String_t* value)
	{
		___csvURL_0 = value;
		Il2CppCodeGenWriteBarrier((&___csvURL_0), value);
	}

	inline static int32_t get_offset_of_U3CloadCSVU3E__0_1() { return static_cast<int32_t>(offsetof(U3CPrepareFilesU3Ec__Iterator1_t2748620044, ___U3CloadCSVU3E__0_1)); }
	inline Coroutine_t2299508840 * get_U3CloadCSVU3E__0_1() const { return ___U3CloadCSVU3E__0_1; }
	inline Coroutine_t2299508840 ** get_address_of_U3CloadCSVU3E__0_1() { return &___U3CloadCSVU3E__0_1; }
	inline void set_U3CloadCSVU3E__0_1(Coroutine_t2299508840 * value)
	{
		___U3CloadCSVU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadCSVU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CloadVideoU3E__0_2() { return static_cast<int32_t>(offsetof(U3CPrepareFilesU3Ec__Iterator1_t2748620044, ___U3CloadVideoU3E__0_2)); }
	inline Coroutine_t2299508840 * get_U3CloadVideoU3E__0_2() const { return ___U3CloadVideoU3E__0_2; }
	inline Coroutine_t2299508840 ** get_address_of_U3CloadVideoU3E__0_2() { return &___U3CloadVideoU3E__0_2; }
	inline void set_U3CloadVideoU3E__0_2(Coroutine_t2299508840 * value)
	{
		___U3CloadVideoU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadVideoU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CPrepareFilesU3Ec__Iterator1_t2748620044, ___U24this_3)); }
	inline TestScript_t905662927 * get_U24this_3() const { return ___U24this_3; }
	inline TestScript_t905662927 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TestScript_t905662927 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CPrepareFilesU3Ec__Iterator1_t2748620044, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CPrepareFilesU3Ec__Iterator1_t2748620044, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CPrepareFilesU3Ec__Iterator1_t2748620044, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPREPAREFILESU3EC__ITERATOR1_T2748620044_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef CSVREADER_T2661561169_H
#define CSVREADER_T2661561169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSV.CSVReader
struct  CSVReader_t2661561169  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CSVREADER_T2661561169_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef MATRIX4X4_T2933234003_H
#define MATRIX4X4_T2933234003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2933234003 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2933234003_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2933234003  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2933234003  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2933234003  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2933234003 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2933234003  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2933234003  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2933234003 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2933234003  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2933234003_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef UNITYEVENT_3_T2425689862_H
#define UNITYEVENT_3_T2425689862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t2425689862  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t2425689862, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T2425689862_H
#ifndef FULLCSV_T1597705491_H
#define FULLCSV_T1597705491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSV.FullCSV
struct  FullCSV_t1597705491 
{
public:
	union
	{
		struct
		{
			// System.Int32 CSV.FullCSV::<NumberOfTitles>k__BackingField
			int32_t ___U3CNumberOfTitlesU3Ek__BackingField_0;
			// System.Int32 CSV.FullCSV::<NumberOfLines>k__BackingField
			int32_t ___U3CNumberOfLinesU3Ek__BackingField_1;
			// System.String[] CSV.FullCSV::<Titles>k__BackingField
			StringU5BU5D_t1642385972* ___U3CTitlesU3Ek__BackingField_2;
			// System.String[] CSV.FullCSV::<Lines>k__BackingField
			StringU5BU5D_t1642385972* ___U3CLinesU3Ek__BackingField_3;
			// System.String[][] CSV.FullCSV::<Table>k__BackingField
			StringU5BU5DU5BU5D_t2190260861* ___U3CTableU3Ek__BackingField_4;
		};
		uint8_t FullCSV_t1597705491__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CNumberOfTitlesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FullCSV_t1597705491, ___U3CNumberOfTitlesU3Ek__BackingField_0)); }
	inline int32_t get_U3CNumberOfTitlesU3Ek__BackingField_0() const { return ___U3CNumberOfTitlesU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CNumberOfTitlesU3Ek__BackingField_0() { return &___U3CNumberOfTitlesU3Ek__BackingField_0; }
	inline void set_U3CNumberOfTitlesU3Ek__BackingField_0(int32_t value)
	{
		___U3CNumberOfTitlesU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CNumberOfLinesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FullCSV_t1597705491, ___U3CNumberOfLinesU3Ek__BackingField_1)); }
	inline int32_t get_U3CNumberOfLinesU3Ek__BackingField_1() const { return ___U3CNumberOfLinesU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CNumberOfLinesU3Ek__BackingField_1() { return &___U3CNumberOfLinesU3Ek__BackingField_1; }
	inline void set_U3CNumberOfLinesU3Ek__BackingField_1(int32_t value)
	{
		___U3CNumberOfLinesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTitlesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FullCSV_t1597705491, ___U3CTitlesU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1642385972* get_U3CTitlesU3Ek__BackingField_2() const { return ___U3CTitlesU3Ek__BackingField_2; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CTitlesU3Ek__BackingField_2() { return &___U3CTitlesU3Ek__BackingField_2; }
	inline void set_U3CTitlesU3Ek__BackingField_2(StringU5BU5D_t1642385972* value)
	{
		___U3CTitlesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitlesU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CLinesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FullCSV_t1597705491, ___U3CLinesU3Ek__BackingField_3)); }
	inline StringU5BU5D_t1642385972* get_U3CLinesU3Ek__BackingField_3() const { return ___U3CLinesU3Ek__BackingField_3; }
	inline StringU5BU5D_t1642385972** get_address_of_U3CLinesU3Ek__BackingField_3() { return &___U3CLinesU3Ek__BackingField_3; }
	inline void set_U3CLinesU3Ek__BackingField_3(StringU5BU5D_t1642385972* value)
	{
		___U3CLinesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLinesU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CTableU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FullCSV_t1597705491, ___U3CTableU3Ek__BackingField_4)); }
	inline StringU5BU5DU5BU5D_t2190260861* get_U3CTableU3Ek__BackingField_4() const { return ___U3CTableU3Ek__BackingField_4; }
	inline StringU5BU5DU5BU5D_t2190260861** get_address_of_U3CTableU3Ek__BackingField_4() { return &___U3CTableU3Ek__BackingField_4; }
	inline void set_U3CTableU3Ek__BackingField_4(StringU5BU5DU5BU5D_t2190260861* value)
	{
		___U3CTableU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTableU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CSV.FullCSV
struct FullCSV_t1597705491_marshaled_pinvoke
{
	union
	{
		struct
		{
			int32_t ___U3CNumberOfTitlesU3Ek__BackingField_0;
			int32_t ___U3CNumberOfLinesU3Ek__BackingField_1;
			char** ___U3CTitlesU3Ek__BackingField_2;
			char** ___U3CLinesU3Ek__BackingField_3;
			StringU5BU5DU5BU5D_t2190260861* ___U3CTableU3Ek__BackingField_4;
		};
		uint8_t FullCSV_t1597705491__padding[1];
	};
};
// Native definition for COM marshalling of CSV.FullCSV
struct FullCSV_t1597705491_marshaled_com
{
	union
	{
		struct
		{
			int32_t ___U3CNumberOfTitlesU3Ek__BackingField_0;
			int32_t ___U3CNumberOfLinesU3Ek__BackingField_1;
			Il2CppChar** ___U3CTitlesU3Ek__BackingField_2;
			Il2CppChar** ___U3CLinesU3Ek__BackingField_3;
			StringU5BU5DU5BU5D_t2190260861* ___U3CTableU3Ek__BackingField_4;
		};
		uint8_t FullCSV_t1597705491__padding[1];
	};
};
#endif // FULLCSV_T1597705491_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef UNITYEVENT_3_T1037676193_H
#define UNITYEVENT_3_T1037676193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.String,System.Int32>
struct  UnityEvent_3_t1037676193  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t1037676193, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T1037676193_H
#ifndef RACEINFO_T3176063123_H
#define RACEINFO_T3176063123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Porsche.RaceInfo
struct  RaceInfo_t3176063123 
{
public:
	union
	{
		struct
		{
			// System.Single Porsche.RaceInfo::<Time>k__BackingField
			float ___U3CTimeU3Ek__BackingField_0;
			// System.Int32 Porsche.RaceInfo::<Speed>k__BackingField
			int32_t ___U3CSpeedU3Ek__BackingField_1;
			// System.Int32 Porsche.RaceInfo::<RPM>k__BackingField
			int32_t ___U3CRPMU3Ek__BackingField_2;
			// System.Single Porsche.RaceInfo::<Lat>k__BackingField
			float ___U3CLatU3Ek__BackingField_3;
			// System.Single Porsche.RaceInfo::<Lon>k__BackingField
			float ___U3CLonU3Ek__BackingField_4;
			// System.Single Porsche.RaceInfo::<GX>k__BackingField
			float ___U3CGXU3Ek__BackingField_5;
			// System.Single Porsche.RaceInfo::<GY>k__BackingField
			float ___U3CGYU3Ek__BackingField_6;
		};
		uint8_t RaceInfo_t3176063123__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RaceInfo_t3176063123, ___U3CTimeU3Ek__BackingField_0)); }
	inline float get_U3CTimeU3Ek__BackingField_0() const { return ___U3CTimeU3Ek__BackingField_0; }
	inline float* get_address_of_U3CTimeU3Ek__BackingField_0() { return &___U3CTimeU3Ek__BackingField_0; }
	inline void set_U3CTimeU3Ek__BackingField_0(float value)
	{
		___U3CTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CSpeedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RaceInfo_t3176063123, ___U3CSpeedU3Ek__BackingField_1)); }
	inline int32_t get_U3CSpeedU3Ek__BackingField_1() const { return ___U3CSpeedU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CSpeedU3Ek__BackingField_1() { return &___U3CSpeedU3Ek__BackingField_1; }
	inline void set_U3CSpeedU3Ek__BackingField_1(int32_t value)
	{
		___U3CSpeedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRPMU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RaceInfo_t3176063123, ___U3CRPMU3Ek__BackingField_2)); }
	inline int32_t get_U3CRPMU3Ek__BackingField_2() const { return ___U3CRPMU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CRPMU3Ek__BackingField_2() { return &___U3CRPMU3Ek__BackingField_2; }
	inline void set_U3CRPMU3Ek__BackingField_2(int32_t value)
	{
		___U3CRPMU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLatU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RaceInfo_t3176063123, ___U3CLatU3Ek__BackingField_3)); }
	inline float get_U3CLatU3Ek__BackingField_3() const { return ___U3CLatU3Ek__BackingField_3; }
	inline float* get_address_of_U3CLatU3Ek__BackingField_3() { return &___U3CLatU3Ek__BackingField_3; }
	inline void set_U3CLatU3Ek__BackingField_3(float value)
	{
		___U3CLatU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLonU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RaceInfo_t3176063123, ___U3CLonU3Ek__BackingField_4)); }
	inline float get_U3CLonU3Ek__BackingField_4() const { return ___U3CLonU3Ek__BackingField_4; }
	inline float* get_address_of_U3CLonU3Ek__BackingField_4() { return &___U3CLonU3Ek__BackingField_4; }
	inline void set_U3CLonU3Ek__BackingField_4(float value)
	{
		___U3CLonU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CGXU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RaceInfo_t3176063123, ___U3CGXU3Ek__BackingField_5)); }
	inline float get_U3CGXU3Ek__BackingField_5() const { return ___U3CGXU3Ek__BackingField_5; }
	inline float* get_address_of_U3CGXU3Ek__BackingField_5() { return &___U3CGXU3Ek__BackingField_5; }
	inline void set_U3CGXU3Ek__BackingField_5(float value)
	{
		___U3CGXU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CGYU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RaceInfo_t3176063123, ___U3CGYU3Ek__BackingField_6)); }
	inline float get_U3CGYU3Ek__BackingField_6() const { return ___U3CGYU3Ek__BackingField_6; }
	inline float* get_address_of_U3CGYU3Ek__BackingField_6() { return &___U3CGYU3Ek__BackingField_6; }
	inline void set_U3CGYU3Ek__BackingField_6(float value)
	{
		___U3CGYU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RACEINFO_T3176063123_H
#ifndef UNITYEVENT_2_T2762981218_H
#define UNITYEVENT_2_T2762981218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Char,System.Int32>
struct  UnityEvent_2_t2762981218  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t2762981218, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T2762981218_H
#ifndef VERTEXANIM_T2147777005_H
#define VERTEXANIM_T2147777005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter/VertexAnim
struct  VertexAnim_t2147777005 
{
public:
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angleRange
	float ___angleRange_0;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angle
	float ___angle_1;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_angleRange_0() { return static_cast<int32_t>(offsetof(VertexAnim_t2147777005, ___angleRange_0)); }
	inline float get_angleRange_0() const { return ___angleRange_0; }
	inline float* get_address_of_angleRange_0() { return &___angleRange_0; }
	inline void set_angleRange_0(float value)
	{
		___angleRange_0 = value;
	}

	inline static int32_t get_offset_of_angle_1() { return static_cast<int32_t>(offsetof(VertexAnim_t2147777005, ___angle_1)); }
	inline float get_angle_1() const { return ___angle_1; }
	inline float* get_address_of_angle_1() { return &___angle_1; }
	inline void set_angle_1(float value)
	{
		___angle_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(VertexAnim_t2147777005, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXANIM_T2147777005_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef WORDSELECTIONEVENT_T2871480376_H
#define WORDSELECTIONEVENT_T2871480376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/WordSelectionEvent
struct  WordSelectionEvent_t2871480376  : public UnityEvent_3_t2425689862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORDSELECTIONEVENT_T2871480376_H
#ifndef CHARACTERSELECTIONEVENT_T2887241789_H
#define CHARACTERSELECTIONEVENT_T2887241789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/CharacterSelectionEvent
struct  CharacterSelectionEvent_t2887241789  : public UnityEvent_2_t2762981218
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERSELECTIONEVENT_T2887241789_H
#ifndef OBJECTTYPE_T309451146_H
#define OBJECTTYPE_T309451146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01/objectType
struct  objectType_t309451146 
{
public:
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01/objectType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(objectType_t309451146, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTYPE_T309451146_H
#ifndef LINKSELECTIONEVENT_T3735310088_H
#define LINKSELECTIONEVENT_T3735310088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LinkSelectionEvent
struct  LinkSelectionEvent_t3735310088  : public UnityEvent_3_t1037676193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSELECTIONEVENT_T3735310088_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T4210043720_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T4210043720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__0_0;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<copyOfVertices>__0
	Vector3U5BU5DU5BU5D_t1477540408* ___U3CcopyOfVerticesU3E__0_1;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_2;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<lineCount>__1
	int32_t ___U3ClineCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t2933234003  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexShakeB TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$this
	VertexShakeB_t3019979320 * ___U24this_5;
	// System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720, ___U3CcopyOfVerticesU3E__0_1)); }
	inline Vector3U5BU5DU5BU5D_t1477540408* get_U3CcopyOfVerticesU3E__0_1() const { return ___U3CcopyOfVerticesU3E__0_1; }
	inline Vector3U5BU5DU5BU5D_t1477540408** get_address_of_U3CcopyOfVerticesU3E__0_1() { return &___U3CcopyOfVerticesU3E__0_1; }
	inline void set_U3CcopyOfVerticesU3E__0_1(Vector3U5BU5DU5BU5D_t1477540408* value)
	{
		___U3CcopyOfVerticesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcopyOfVerticesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720, ___U3CcharacterCountU3E__1_2)); }
	inline int32_t get_U3CcharacterCountU3E__1_2() const { return ___U3CcharacterCountU3E__1_2; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_2() { return &___U3CcharacterCountU3E__1_2; }
	inline void set_U3CcharacterCountU3E__1_2(int32_t value)
	{
		___U3CcharacterCountU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3ClineCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720, ___U3ClineCountU3E__1_3)); }
	inline int32_t get_U3ClineCountU3E__1_3() const { return ___U3ClineCountU3E__1_3; }
	inline int32_t* get_address_of_U3ClineCountU3E__1_3() { return &___U3ClineCountU3E__1_3; }
	inline void set_U3ClineCountU3E__1_3(int32_t value)
	{
		___U3ClineCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t2933234003  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t2933234003  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720, ___U24this_5)); }
	inline VertexShakeB_t3019979320 * get_U24this_5() const { return ___U24this_5; }
	inline VertexShakeB_t3019979320 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexShakeB_t3019979320 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T4210043720_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T1112749575_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T1112749575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__0_0;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<currentCharacter>__0
	int32_t ___U3CcurrentCharacterU3E__0_1;
	// UnityEngine.Color32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<c0>__0
	Color32_t874517518  ___U3Cc0U3E__0_2;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<materialIndex>__1
	int32_t ___U3CmaterialIndexU3E__1_4;
	// UnityEngine.Color32[] TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<newVertexColors>__1
	Color32U5BU5D_t30278651* ___U3CnewVertexColorsU3E__1_5;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::<vertexIndex>__1
	int32_t ___U3CvertexIndexU3E__1_6;
	// TMPro.Examples.VertexColorCycler TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$this
	VertexColorCycler_t3471086701 * ___U24this_7;
	// System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcurrentCharacterU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U3CcurrentCharacterU3E__0_1)); }
	inline int32_t get_U3CcurrentCharacterU3E__0_1() const { return ___U3CcurrentCharacterU3E__0_1; }
	inline int32_t* get_address_of_U3CcurrentCharacterU3E__0_1() { return &___U3CcurrentCharacterU3E__0_1; }
	inline void set_U3CcurrentCharacterU3E__0_1(int32_t value)
	{
		___U3CcurrentCharacterU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cc0U3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U3Cc0U3E__0_2)); }
	inline Color32_t874517518  get_U3Cc0U3E__0_2() const { return ___U3Cc0U3E__0_2; }
	inline Color32_t874517518 * get_address_of_U3Cc0U3E__0_2() { return &___U3Cc0U3E__0_2; }
	inline void set_U3Cc0U3E__0_2(Color32_t874517518  value)
	{
		___U3Cc0U3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmaterialIndexU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U3CmaterialIndexU3E__1_4)); }
	inline int32_t get_U3CmaterialIndexU3E__1_4() const { return ___U3CmaterialIndexU3E__1_4; }
	inline int32_t* get_address_of_U3CmaterialIndexU3E__1_4() { return &___U3CmaterialIndexU3E__1_4; }
	inline void set_U3CmaterialIndexU3E__1_4(int32_t value)
	{
		___U3CmaterialIndexU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CnewVertexColorsU3E__1_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U3CnewVertexColorsU3E__1_5)); }
	inline Color32U5BU5D_t30278651* get_U3CnewVertexColorsU3E__1_5() const { return ___U3CnewVertexColorsU3E__1_5; }
	inline Color32U5BU5D_t30278651** get_address_of_U3CnewVertexColorsU3E__1_5() { return &___U3CnewVertexColorsU3E__1_5; }
	inline void set_U3CnewVertexColorsU3E__1_5(Color32U5BU5D_t30278651* value)
	{
		___U3CnewVertexColorsU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnewVertexColorsU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U3CvertexIndexU3E__1_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U3CvertexIndexU3E__1_6)); }
	inline int32_t get_U3CvertexIndexU3E__1_6() const { return ___U3CvertexIndexU3E__1_6; }
	inline int32_t* get_address_of_U3CvertexIndexU3E__1_6() { return &___U3CvertexIndexU3E__1_6; }
	inline void set_U3CvertexIndexU3E__1_6(int32_t value)
	{
		___U3CvertexIndexU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U24this_7)); }
	inline VertexColorCycler_t3471086701 * get_U24this_7() const { return ___U24this_7; }
	inline VertexColorCycler_t3471086701 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(VertexColorCycler_t3471086701 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T1112749575_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T3033053737_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T3033053737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__0_0;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<copyOfVertices>__0
	Vector3U5BU5DU5BU5D_t1477540408* ___U3CcopyOfVerticesU3E__0_1;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_2;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<lineCount>__1
	int32_t ___U3ClineCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t2933234003  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexShakeA TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$this
	VertexShakeA_t3019979321 * ___U24this_5;
	// System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737, ___U3CcopyOfVerticesU3E__0_1)); }
	inline Vector3U5BU5DU5BU5D_t1477540408* get_U3CcopyOfVerticesU3E__0_1() const { return ___U3CcopyOfVerticesU3E__0_1; }
	inline Vector3U5BU5DU5BU5D_t1477540408** get_address_of_U3CcopyOfVerticesU3E__0_1() { return &___U3CcopyOfVerticesU3E__0_1; }
	inline void set_U3CcopyOfVerticesU3E__0_1(Vector3U5BU5DU5BU5D_t1477540408* value)
	{
		___U3CcopyOfVerticesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcopyOfVerticesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737, ___U3CcharacterCountU3E__1_2)); }
	inline int32_t get_U3CcharacterCountU3E__1_2() const { return ___U3CcharacterCountU3E__1_2; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_2() { return &___U3CcharacterCountU3E__1_2; }
	inline void set_U3CcharacterCountU3E__1_2(int32_t value)
	{
		___U3CcharacterCountU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3ClineCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737, ___U3ClineCountU3E__1_3)); }
	inline int32_t get_U3ClineCountU3E__1_3() const { return ___U3ClineCountU3E__1_3; }
	inline int32_t* get_address_of_U3ClineCountU3E__1_3() { return &___U3ClineCountU3E__1_3; }
	inline void set_U3ClineCountU3E__1_3(int32_t value)
	{
		___U3ClineCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t2933234003  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t2933234003  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737, ___U24this_5)); }
	inline VertexShakeA_t3019979321 * get_U24this_5() const { return ___U24this_5; }
	inline VertexShakeA_t3019979321 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexShakeA_t3019979321 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T3033053737_H
#ifndef U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T1065331755_H
#define U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T1065331755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1
struct  U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_pos>__0
	Vector3_t2243707580  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<start_color>__0
	Color32_t874517518  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<int_counter>__0
	int32_t ___U3Cint_counterU3E__0_6;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_7;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$this
	TextMeshProFloatingText_t6181308 * ___U24this_8;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>c__Iterator1::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t2243707580  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t2243707580 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t2243707580  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t874517518  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t874517518 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t874517518  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3Cint_counterU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U3Cint_counterU3E__0_6)); }
	inline int32_t get_U3Cint_counterU3E__0_6() const { return ___U3Cint_counterU3E__0_6; }
	inline int32_t* get_address_of_U3Cint_counterU3E__0_6() { return &___U3Cint_counterU3E__0_6; }
	inline void set_U3Cint_counterU3E__0_6(int32_t value)
	{
		___U3Cint_counterU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U3CfadeDurationU3E__0_7)); }
	inline float get_U3CfadeDurationU3E__0_7() const { return ___U3CfadeDurationU3E__0_7; }
	inline float* get_address_of_U3CfadeDurationU3E__0_7() { return &___U3CfadeDurationU3E__0_7; }
	inline void set_U3CfadeDurationU3E__0_7(float value)
	{
		___U3CfadeDurationU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U24this_8)); }
	inline TextMeshProFloatingText_t6181308 * get_U24this_8() const { return ___U24this_8; }
	inline TextMeshProFloatingText_t6181308 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(TextMeshProFloatingText_t6181308 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHFLOATINGTEXTU3EC__ITERATOR1_T1065331755_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T2947903458_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T2947903458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__0_0;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<loopCount>__0
	int32_t ___U3CloopCountU3E__0_1;
	// TMPro.Examples.VertexJitter/VertexAnim[] TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<vertexAnim>__0
	VertexAnimU5BU5D_t1450067904* ___U3CvertexAnimU3E__0_2;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<cachedMeshInfo>__0
	TMP_MeshInfoU5BU5D_t2398608976* ___U3CcachedMeshInfoU3E__0_3;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t2933234003  ___U3CmatrixU3E__2_5;
	// TMPro.Examples.VertexJitter TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$this
	VertexJitter_t3970559362 * ___U24this_6;
	// System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CloopCountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U3CloopCountU3E__0_1)); }
	inline int32_t get_U3CloopCountU3E__0_1() const { return ___U3CloopCountU3E__0_1; }
	inline int32_t* get_address_of_U3CloopCountU3E__0_1() { return &___U3CloopCountU3E__0_1; }
	inline void set_U3CloopCountU3E__0_1(int32_t value)
	{
		___U3CloopCountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CvertexAnimU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U3CvertexAnimU3E__0_2)); }
	inline VertexAnimU5BU5D_t1450067904* get_U3CvertexAnimU3E__0_2() const { return ___U3CvertexAnimU3E__0_2; }
	inline VertexAnimU5BU5D_t1450067904** get_address_of_U3CvertexAnimU3E__0_2() { return &___U3CvertexAnimU3E__0_2; }
	inline void set_U3CvertexAnimU3E__0_2(VertexAnimU5BU5D_t1450067904* value)
	{
		___U3CvertexAnimU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvertexAnimU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoU3E__0_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U3CcachedMeshInfoU3E__0_3)); }
	inline TMP_MeshInfoU5BU5D_t2398608976* get_U3CcachedMeshInfoU3E__0_3() const { return ___U3CcachedMeshInfoU3E__0_3; }
	inline TMP_MeshInfoU5BU5D_t2398608976** get_address_of_U3CcachedMeshInfoU3E__0_3() { return &___U3CcachedMeshInfoU3E__0_3; }
	inline void set_U3CcachedMeshInfoU3E__0_3(TMP_MeshInfoU5BU5D_t2398608976* value)
	{
		___U3CcachedMeshInfoU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcachedMeshInfoU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U3CmatrixU3E__2_5)); }
	inline Matrix4x4_t2933234003  get_U3CmatrixU3E__2_5() const { return ___U3CmatrixU3E__2_5; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CmatrixU3E__2_5() { return &___U3CmatrixU3E__2_5; }
	inline void set_U3CmatrixU3E__2_5(Matrix4x4_t2933234003  value)
	{
		___U3CmatrixU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U24this_6)); }
	inline VertexJitter_t3970559362 * get_U24this_6() const { return ___U24this_6; }
	inline VertexJitter_t3970559362 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(VertexJitter_t3970559362 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T2947903458_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T2802507505_H
#define FPSCOUNTERANCHORPOSITIONS_T2802507505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t2802507505 
{
public:
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t2802507505, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T2802507505_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T3015313749_H
#define FPSCOUNTERANCHORPOSITIONS_T3015313749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t3015313749 
{
public:
	// System.Int32 TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t3015313749, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T3015313749_H
#ifndef LINESELECTIONEVENT_T3265414486_H
#define LINESELECTIONEVENT_T3265414486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler/LineSelectionEvent
struct  LineSelectionEvent_t3265414486  : public UnityEvent_3_t2425689862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESELECTIONEVENT_T3265414486_H
#ifndef FPSCOUNTERANCHORPOSITIONS_T3655978865_H
#define FPSCOUNTERANCHORPOSITIONS_T3655978865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions
struct  FpsCounterAnchorPositions_t3655978865 
{
public:
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FpsCounterAnchorPositions_t3655978865, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTERANCHORPOSITIONS_T3655978865_H
#ifndef U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T3989756759_H
#define U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T3989756759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0
struct  U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<CountDuration>__0
	float ___U3CCountDurationU3E__0_0;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<starting_Count>__0
	float ___U3Cstarting_CountU3E__0_1;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<current_Count>__0
	float ___U3Ccurrent_CountU3E__0_2;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_pos>__0
	Vector3_t2243707580  ___U3Cstart_posU3E__0_3;
	// UnityEngine.Color32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<start_color>__0
	Color32_t874517518  ___U3Cstart_colorU3E__0_4;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<alpha>__0
	float ___U3CalphaU3E__0_5;
	// System.Single TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::<fadeDuration>__0
	float ___U3CfadeDurationU3E__0_6;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$this
	TextMeshProFloatingText_t6181308 * ___U24this_7;
	// System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3CCountDurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U3CCountDurationU3E__0_0)); }
	inline float get_U3CCountDurationU3E__0_0() const { return ___U3CCountDurationU3E__0_0; }
	inline float* get_address_of_U3CCountDurationU3E__0_0() { return &___U3CCountDurationU3E__0_0; }
	inline void set_U3CCountDurationU3E__0_0(float value)
	{
		___U3CCountDurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cstarting_CountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U3Cstarting_CountU3E__0_1)); }
	inline float get_U3Cstarting_CountU3E__0_1() const { return ___U3Cstarting_CountU3E__0_1; }
	inline float* get_address_of_U3Cstarting_CountU3E__0_1() { return &___U3Cstarting_CountU3E__0_1; }
	inline void set_U3Cstarting_CountU3E__0_1(float value)
	{
		___U3Cstarting_CountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_CountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U3Ccurrent_CountU3E__0_2)); }
	inline float get_U3Ccurrent_CountU3E__0_2() const { return ___U3Ccurrent_CountU3E__0_2; }
	inline float* get_address_of_U3Ccurrent_CountU3E__0_2() { return &___U3Ccurrent_CountU3E__0_2; }
	inline void set_U3Ccurrent_CountU3E__0_2(float value)
	{
		___U3Ccurrent_CountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_posU3E__0_3() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U3Cstart_posU3E__0_3)); }
	inline Vector3_t2243707580  get_U3Cstart_posU3E__0_3() const { return ___U3Cstart_posU3E__0_3; }
	inline Vector3_t2243707580 * get_address_of_U3Cstart_posU3E__0_3() { return &___U3Cstart_posU3E__0_3; }
	inline void set_U3Cstart_posU3E__0_3(Vector3_t2243707580  value)
	{
		___U3Cstart_posU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3Cstart_colorU3E__0_4() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U3Cstart_colorU3E__0_4)); }
	inline Color32_t874517518  get_U3Cstart_colorU3E__0_4() const { return ___U3Cstart_colorU3E__0_4; }
	inline Color32_t874517518 * get_address_of_U3Cstart_colorU3E__0_4() { return &___U3Cstart_colorU3E__0_4; }
	inline void set_U3Cstart_colorU3E__0_4(Color32_t874517518  value)
	{
		___U3Cstart_colorU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U3CalphaU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U3CalphaU3E__0_5)); }
	inline float get_U3CalphaU3E__0_5() const { return ___U3CalphaU3E__0_5; }
	inline float* get_address_of_U3CalphaU3E__0_5() { return &___U3CalphaU3E__0_5; }
	inline void set_U3CalphaU3E__0_5(float value)
	{
		___U3CalphaU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CfadeDurationU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U3CfadeDurationU3E__0_6)); }
	inline float get_U3CfadeDurationU3E__0_6() const { return ___U3CfadeDurationU3E__0_6; }
	inline float* get_address_of_U3CfadeDurationU3E__0_6() { return &___U3CfadeDurationU3E__0_6; }
	inline void set_U3CfadeDurationU3E__0_6(float value)
	{
		___U3CfadeDurationU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U24this_7)); }
	inline TextMeshProFloatingText_t6181308 * get_U24this_7() const { return ___U24this_7; }
	inline TextMeshProFloatingText_t6181308 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(TextMeshProFloatingText_t6181308 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDISPLAYTEXTMESHPROFLOATINGTEXTU3EC__ITERATOR0_T3989756759_H
#ifndef U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T76635731_H
#define U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T76635731_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0
struct  U3CAnimateVertexColorsU3Ec__Iterator0_t76635731  : public RuntimeObject
{
public:
	// TMPro.TMP_TextInfo TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<textInfo>__0
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__0_0;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<cachedMeshInfoVertexData>__0
	TMP_MeshInfoU5BU5D_t2398608976* ___U3CcachedMeshInfoVertexDataU3E__0_1;
	// System.Collections.Generic.List`1<System.Int32> TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<scaleSortingOrder>__0
	List_1_t1440998580 * ___U3CscaleSortingOrderU3E__0_2;
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// UnityEngine.Matrix4x4 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::<matrix>__2
	Matrix4x4_t2933234003  ___U3CmatrixU3E__2_4;
	// TMPro.Examples.VertexZoom TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$this
	VertexZoom_t3463934435 * ___U24this_5;
	// System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$PC
	int32_t ___U24PC_8;
	// TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0/<AnimateVertexColors>c__AnonStorey1 TMPro.Examples.VertexZoom/<AnimateVertexColors>c__Iterator0::$locvar0
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289 * ___U24locvar0_9;

public:
	inline static int32_t get_offset_of_U3CtextInfoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U3CtextInfoU3E__0_0)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__0_0() const { return ___U3CtextInfoU3E__0_0; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__0_0() { return &___U3CtextInfoU3E__0_0; }
	inline void set_U3CtextInfoU3E__0_0(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoVertexDataU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U3CcachedMeshInfoVertexDataU3E__0_1)); }
	inline TMP_MeshInfoU5BU5D_t2398608976* get_U3CcachedMeshInfoVertexDataU3E__0_1() const { return ___U3CcachedMeshInfoVertexDataU3E__0_1; }
	inline TMP_MeshInfoU5BU5D_t2398608976** get_address_of_U3CcachedMeshInfoVertexDataU3E__0_1() { return &___U3CcachedMeshInfoVertexDataU3E__0_1; }
	inline void set_U3CcachedMeshInfoVertexDataU3E__0_1(TMP_MeshInfoU5BU5D_t2398608976* value)
	{
		___U3CcachedMeshInfoVertexDataU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcachedMeshInfoVertexDataU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CscaleSortingOrderU3E__0_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U3CscaleSortingOrderU3E__0_2)); }
	inline List_1_t1440998580 * get_U3CscaleSortingOrderU3E__0_2() const { return ___U3CscaleSortingOrderU3E__0_2; }
	inline List_1_t1440998580 ** get_address_of_U3CscaleSortingOrderU3E__0_2() { return &___U3CscaleSortingOrderU3E__0_2; }
	inline void set_U3CscaleSortingOrderU3E__0_2(List_1_t1440998580 * value)
	{
		___U3CscaleSortingOrderU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscaleSortingOrderU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U3CmatrixU3E__2_4)); }
	inline Matrix4x4_t2933234003  get_U3CmatrixU3E__2_4() const { return ___U3CmatrixU3E__2_4; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CmatrixU3E__2_4() { return &___U3CmatrixU3E__2_4; }
	inline void set_U3CmatrixU3E__2_4(Matrix4x4_t2933234003  value)
	{
		___U3CmatrixU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U24this_5)); }
	inline VertexZoom_t3463934435 * get_U24this_5() const { return ___U24this_5; }
	inline VertexZoom_t3463934435 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(VertexZoom_t3463934435 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_9() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ec__Iterator0_t76635731, ___U24locvar0_9)); }
	inline U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289 * get_U24locvar0_9() const { return ___U24locvar0_9; }
	inline U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289 ** get_address_of_U24locvar0_9() { return &___U24locvar0_9; }
	inline void set_U24locvar0_9(U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289 * value)
	{
		___U24locvar0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEVERTEXCOLORSU3EC__ITERATOR0_T76635731_H
#ifndef U3CLOADCSVU3EC__ITERATOR2_T2978953405_H
#define U3CLOADCSVU3EC__ITERATOR2_T2978953405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestScript/<LoadCSV>c__Iterator2
struct  U3CLoadCSVU3Ec__Iterator2_t2978953405  : public RuntimeObject
{
public:
	// System.String TestScript/<LoadCSV>c__Iterator2::csvURL
	String_t* ___csvURL_0;
	// UnityEngine.WWW TestScript/<LoadCSV>c__Iterator2::<csvLink>__0
	WWW_t2919945039 * ___U3CcsvLinkU3E__0_1;
	// CSV.FullCSV TestScript/<LoadCSV>c__Iterator2::<splitCSV>__0
	FullCSV_t1597705491  ___U3CsplitCSVU3E__0_2;
	// TestScript TestScript/<LoadCSV>c__Iterator2::$this
	TestScript_t905662927 * ___U24this_3;
	// System.Object TestScript/<LoadCSV>c__Iterator2::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean TestScript/<LoadCSV>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 TestScript/<LoadCSV>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_csvURL_0() { return static_cast<int32_t>(offsetof(U3CLoadCSVU3Ec__Iterator2_t2978953405, ___csvURL_0)); }
	inline String_t* get_csvURL_0() const { return ___csvURL_0; }
	inline String_t** get_address_of_csvURL_0() { return &___csvURL_0; }
	inline void set_csvURL_0(String_t* value)
	{
		___csvURL_0 = value;
		Il2CppCodeGenWriteBarrier((&___csvURL_0), value);
	}

	inline static int32_t get_offset_of_U3CcsvLinkU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadCSVU3Ec__Iterator2_t2978953405, ___U3CcsvLinkU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CcsvLinkU3E__0_1() const { return ___U3CcsvLinkU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CcsvLinkU3E__0_1() { return &___U3CcsvLinkU3E__0_1; }
	inline void set_U3CcsvLinkU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CcsvLinkU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcsvLinkU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CsplitCSVU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoadCSVU3Ec__Iterator2_t2978953405, ___U3CsplitCSVU3E__0_2)); }
	inline FullCSV_t1597705491  get_U3CsplitCSVU3E__0_2() const { return ___U3CsplitCSVU3E__0_2; }
	inline FullCSV_t1597705491 * get_address_of_U3CsplitCSVU3E__0_2() { return &___U3CsplitCSVU3E__0_2; }
	inline void set_U3CsplitCSVU3E__0_2(FullCSV_t1597705491  value)
	{
		___U3CsplitCSVU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadCSVU3Ec__Iterator2_t2978953405, ___U24this_3)); }
	inline TestScript_t905662927 * get_U24this_3() const { return ___U24this_3; }
	inline TestScript_t905662927 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(TestScript_t905662927 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadCSVU3Ec__Iterator2_t2978953405, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadCSVU3Ec__Iterator2_t2978953405, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadCSVU3Ec__Iterator2_t2978953405, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCSVU3EC__ITERATOR2_T2978953405_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef XYBOTH_T3347462289_H
#define XYBOTH_T3347462289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapCreator/XYBoth
struct  XYBoth_t3347462289 
{
public:
	// System.Int32 MapCreator/XYBoth::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XYBoth_t3347462289, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XYBOTH_T3347462289_H
#ifndef VERTPOSITIONSPACE_T1848587165_H
#define VERTPOSITIONSPACE_T1848587165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoadCreator/VertPositionSpace
struct  VertPositionSpace_t1848587165 
{
public:
	// System.Int32 RoadCreator/VertPositionSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertPositionSpace_t1848587165, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTPOSITIONSPACE_T1848587165_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef CAMERAMODES_T2188281734_H
#define CAMERAMODES_T2188281734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController/CameraModes
struct  CameraModes_t2188281734 
{
public:
	// System.Int32 TMPro.Examples.CameraController/CameraModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraModes_t2188281734, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODES_T2188281734_H
#ifndef MOTIONTYPE_T2454277493_H
#define MOTIONTYPE_T2454277493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin/MotionType
struct  MotionType_t2454277493 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin/MotionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionType_t2454277493, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONTYPE_T2454277493_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T2736974085_H
#define U3CWARPTEXTU3EC__ITERATOR0_T2736974085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t2736974085  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3306541151 * ___U3Cold_curveU3E__0_1;
	// TMPro.TMP_TextInfo TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__1_2;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_3;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_4;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_5;
	// UnityEngine.Vector3[] TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1172311765* ___U3CverticesU3E__2_6;
	// UnityEngine.Matrix4x4 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t2933234003  ___U3CmatrixU3E__2_7;
	// TMPro.Examples.WarpTextExample TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$this
	WarpTextExample_t250871869 * ___U24this_8;
	// System.Object TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_9;
	// System.Boolean TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U3Cold_curveU3E__0_1)); }
	inline AnimationCurve_t3306541151 * get_U3Cold_curveU3E__0_1() const { return ___U3Cold_curveU3E__0_1; }
	inline AnimationCurve_t3306541151 ** get_address_of_U3Cold_curveU3E__0_1() { return &___U3Cold_curveU3E__0_1; }
	inline void set_U3Cold_curveU3E__0_1(AnimationCurve_t3306541151 * value)
	{
		___U3Cold_curveU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U3CtextInfoU3E__1_2)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__1_2() const { return ___U3CtextInfoU3E__1_2; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__1_2() { return &___U3CtextInfoU3E__1_2; }
	inline void set_U3CtextInfoU3E__1_2(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U3CcharacterCountU3E__1_3)); }
	inline int32_t get_U3CcharacterCountU3E__1_3() const { return ___U3CcharacterCountU3E__1_3; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_3() { return &___U3CcharacterCountU3E__1_3; }
	inline void set_U3CcharacterCountU3E__1_3(int32_t value)
	{
		___U3CcharacterCountU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U3CboundsMinXU3E__1_4)); }
	inline float get_U3CboundsMinXU3E__1_4() const { return ___U3CboundsMinXU3E__1_4; }
	inline float* get_address_of_U3CboundsMinXU3E__1_4() { return &___U3CboundsMinXU3E__1_4; }
	inline void set_U3CboundsMinXU3E__1_4(float value)
	{
		___U3CboundsMinXU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U3CboundsMaxXU3E__1_5)); }
	inline float get_U3CboundsMaxXU3E__1_5() const { return ___U3CboundsMaxXU3E__1_5; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_5() { return &___U3CboundsMaxXU3E__1_5; }
	inline void set_U3CboundsMaxXU3E__1_5(float value)
	{
		___U3CboundsMaxXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U3CverticesU3E__2_6)); }
	inline Vector3U5BU5D_t1172311765* get_U3CverticesU3E__2_6() const { return ___U3CverticesU3E__2_6; }
	inline Vector3U5BU5D_t1172311765** get_address_of_U3CverticesU3E__2_6() { return &___U3CverticesU3E__2_6; }
	inline void set_U3CverticesU3E__2_6(Vector3U5BU5D_t1172311765* value)
	{
		___U3CverticesU3E__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_6), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U3CmatrixU3E__2_7)); }
	inline Matrix4x4_t2933234003  get_U3CmatrixU3E__2_7() const { return ___U3CmatrixU3E__2_7; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CmatrixU3E__2_7() { return &___U3CmatrixU3E__2_7; }
	inline void set_U3CmatrixU3E__2_7(Matrix4x4_t2933234003  value)
	{
		___U3CmatrixU3E__2_7 = value;
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U24this_8)); }
	inline WarpTextExample_t250871869 * get_U24this_8() const { return ___U24this_8; }
	inline WarpTextExample_t250871869 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(WarpTextExample_t250871869 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t2736974085, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T2736974085_H
#ifndef U3CWARPTEXTU3EC__ITERATOR0_T1799541603_H
#define U3CWARPTEXTU3EC__ITERATOR0_T1799541603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0
struct  U3CWarpTextU3Ec__Iterator0_t1799541603  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_CurveScale>__0
	float ___U3Cold_CurveScaleU3E__0_0;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_ShearValue>__0
	float ___U3Cold_ShearValueU3E__0_1;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<old_curve>__0
	AnimationCurve_t3306541151 * ___U3Cold_curveU3E__0_2;
	// TMPro.TMP_TextInfo TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<textInfo>__1
	TMP_TextInfo_t2849466151 * ___U3CtextInfoU3E__1_3;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<characterCount>__1
	int32_t ___U3CcharacterCountU3E__1_4;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMinX>__1
	float ___U3CboundsMinXU3E__1_5;
	// System.Single TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<boundsMaxX>__1
	float ___U3CboundsMaxXU3E__1_6;
	// UnityEngine.Vector3[] TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<vertices>__2
	Vector3U5BU5D_t1172311765* ___U3CverticesU3E__2_7;
	// UnityEngine.Matrix4x4 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::<matrix>__2
	Matrix4x4_t2933234003  ___U3CmatrixU3E__2_8;
	// TMPro.Examples.SkewTextExample TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$this
	SkewTextExample_t3378890949 * ___U24this_9;
	// System.Object TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$current
	RuntimeObject * ___U24current_10;
	// System.Boolean TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$disposing
	bool ___U24disposing_11;
	// System.Int32 TMPro.Examples.SkewTextExample/<WarpText>c__Iterator0::$PC
	int32_t ___U24PC_12;

public:
	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3Cold_CurveScaleU3E__0_0)); }
	inline float get_U3Cold_CurveScaleU3E__0_0() const { return ___U3Cold_CurveScaleU3E__0_0; }
	inline float* get_address_of_U3Cold_CurveScaleU3E__0_0() { return &___U3Cold_CurveScaleU3E__0_0; }
	inline void set_U3Cold_CurveScaleU3E__0_0(float value)
	{
		___U3Cold_CurveScaleU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cold_ShearValueU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3Cold_ShearValueU3E__0_1)); }
	inline float get_U3Cold_ShearValueU3E__0_1() const { return ___U3Cold_ShearValueU3E__0_1; }
	inline float* get_address_of_U3Cold_ShearValueU3E__0_1() { return &___U3Cold_ShearValueU3E__0_1; }
	inline void set_U3Cold_ShearValueU3E__0_1(float value)
	{
		___U3Cold_ShearValueU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E__0_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3Cold_curveU3E__0_2)); }
	inline AnimationCurve_t3306541151 * get_U3Cold_curveU3E__0_2() const { return ___U3Cold_curveU3E__0_2; }
	inline AnimationCurve_t3306541151 ** get_address_of_U3Cold_curveU3E__0_2() { return &___U3Cold_curveU3E__0_2; }
	inline void set_U3Cold_curveU3E__0_2(AnimationCurve_t3306541151 * value)
	{
		___U3Cold_curveU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cold_curveU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CtextInfoU3E__1_3)); }
	inline TMP_TextInfo_t2849466151 * get_U3CtextInfoU3E__1_3() const { return ___U3CtextInfoU3E__1_3; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_U3CtextInfoU3E__1_3() { return &___U3CtextInfoU3E__1_3; }
	inline void set_U3CtextInfoU3E__1_3(TMP_TextInfo_t2849466151 * value)
	{
		___U3CtextInfoU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextInfoU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U3CcharacterCountU3E__1_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CcharacterCountU3E__1_4)); }
	inline int32_t get_U3CcharacterCountU3E__1_4() const { return ___U3CcharacterCountU3E__1_4; }
	inline int32_t* get_address_of_U3CcharacterCountU3E__1_4() { return &___U3CcharacterCountU3E__1_4; }
	inline void set_U3CcharacterCountU3E__1_4(int32_t value)
	{
		___U3CcharacterCountU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMinXU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CboundsMinXU3E__1_5)); }
	inline float get_U3CboundsMinXU3E__1_5() const { return ___U3CboundsMinXU3E__1_5; }
	inline float* get_address_of_U3CboundsMinXU3E__1_5() { return &___U3CboundsMinXU3E__1_5; }
	inline void set_U3CboundsMinXU3E__1_5(float value)
	{
		___U3CboundsMinXU3E__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CboundsMaxXU3E__1_6() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CboundsMaxXU3E__1_6)); }
	inline float get_U3CboundsMaxXU3E__1_6() const { return ___U3CboundsMaxXU3E__1_6; }
	inline float* get_address_of_U3CboundsMaxXU3E__1_6() { return &___U3CboundsMaxXU3E__1_6; }
	inline void set_U3CboundsMaxXU3E__1_6(float value)
	{
		___U3CboundsMaxXU3E__1_6 = value;
	}

	inline static int32_t get_offset_of_U3CverticesU3E__2_7() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CverticesU3E__2_7)); }
	inline Vector3U5BU5D_t1172311765* get_U3CverticesU3E__2_7() const { return ___U3CverticesU3E__2_7; }
	inline Vector3U5BU5D_t1172311765** get_address_of_U3CverticesU3E__2_7() { return &___U3CverticesU3E__2_7; }
	inline void set_U3CverticesU3E__2_7(Vector3U5BU5D_t1172311765* value)
	{
		___U3CverticesU3E__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CverticesU3E__2_7), value);
	}

	inline static int32_t get_offset_of_U3CmatrixU3E__2_8() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U3CmatrixU3E__2_8)); }
	inline Matrix4x4_t2933234003  get_U3CmatrixU3E__2_8() const { return ___U3CmatrixU3E__2_8; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CmatrixU3E__2_8() { return &___U3CmatrixU3E__2_8; }
	inline void set_U3CmatrixU3E__2_8(Matrix4x4_t2933234003  value)
	{
		___U3CmatrixU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24this_9() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U24this_9)); }
	inline SkewTextExample_t3378890949 * get_U24this_9() const { return ___U24this_9; }
	inline SkewTextExample_t3378890949 ** get_address_of_U24this_9() { return &___U24this_9; }
	inline void set_U24this_9(SkewTextExample_t3378890949 * value)
	{
		___U24this_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_9), value);
	}

	inline static int32_t get_offset_of_U24current_10() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U24current_10)); }
	inline RuntimeObject * get_U24current_10() const { return ___U24current_10; }
	inline RuntimeObject ** get_address_of_U24current_10() { return &___U24current_10; }
	inline void set_U24current_10(RuntimeObject * value)
	{
		___U24current_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_10), value);
	}

	inline static int32_t get_offset_of_U24disposing_11() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U24disposing_11)); }
	inline bool get_U24disposing_11() const { return ___U24disposing_11; }
	inline bool* get_address_of_U24disposing_11() { return &___U24disposing_11; }
	inline void set_U24disposing_11(bool value)
	{
		___U24disposing_11 = value;
	}

	inline static int32_t get_offset_of_U24PC_12() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ec__Iterator0_t1799541603, ___U24PC_12)); }
	inline int32_t get_U24PC_12() const { return ___U24PC_12; }
	inline int32_t* get_address_of_U24PC_12() { return &___U24PC_12; }
	inline void set_U24PC_12(int32_t value)
	{
		___U24PC_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWARPTEXTU3EC__ITERATOR0_T1799541603_H
#ifndef U3CSTARTU3EC__ITERATOR0_T110035792_H
#define U3CSTARTU3EC__ITERATOR0_T110035792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t110035792  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 EnvMapAnimator/<Start>c__Iterator0::<matrix>__0
	Matrix4x4_t2933234003  ___U3CmatrixU3E__0_0;
	// EnvMapAnimator EnvMapAnimator/<Start>c__Iterator0::$this
	EnvMapAnimator_t1635389402 * ___U24this_1;
	// System.Object EnvMapAnimator/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EnvMapAnimator/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 EnvMapAnimator/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmatrixU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U3CmatrixU3E__0_0)); }
	inline Matrix4x4_t2933234003  get_U3CmatrixU3E__0_0() const { return ___U3CmatrixU3E__0_0; }
	inline Matrix4x4_t2933234003 * get_address_of_U3CmatrixU3E__0_0() { return &___U3CmatrixU3E__0_0; }
	inline void set_U3CmatrixU3E__0_0(Matrix4x4_t2933234003  value)
	{
		___U3CmatrixU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U24this_1)); }
	inline EnvMapAnimator_t1635389402 * get_U24this_1() const { return ___U24this_1; }
	inline EnvMapAnimator_t1635389402 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EnvMapAnimator_t1635389402 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t110035792, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T110035792_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MINMAX_T2491720607_H
#define MINMAX_T2491720607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapCreator/MinMax
struct  MinMax_t2491720607 
{
public:
	union
	{
		struct
		{
			// System.Single MapCreator/MinMax::<minX>k__BackingField
			float ___U3CminXU3Ek__BackingField_0;
			// System.Single MapCreator/MinMax::<minY>k__BackingField
			float ___U3CminYU3Ek__BackingField_1;
			// System.Single MapCreator/MinMax::<maxX>k__BackingField
			float ___U3CmaxXU3Ek__BackingField_2;
			// System.Single MapCreator/MinMax::<maxY>k__BackingField
			float ___U3CmaxYU3Ek__BackingField_3;
			// MapCreator/XYBoth MapCreator/MinMax::<xyBoth>k__BackingField
			int32_t ___U3CxyBothU3Ek__BackingField_4;
			// System.Single MapCreator/MinMax::<xyDiff>k__BackingField
			float ___U3CxyDiffU3Ek__BackingField_5;
			// System.Single MapCreator/MinMax::<xyOtherDiff>k__BackingField
			float ___U3CxyOtherDiffU3Ek__BackingField_6;
		};
		uint8_t MinMax_t2491720607__padding[1];
	};

public:
	inline static int32_t get_offset_of_U3CminXU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MinMax_t2491720607, ___U3CminXU3Ek__BackingField_0)); }
	inline float get_U3CminXU3Ek__BackingField_0() const { return ___U3CminXU3Ek__BackingField_0; }
	inline float* get_address_of_U3CminXU3Ek__BackingField_0() { return &___U3CminXU3Ek__BackingField_0; }
	inline void set_U3CminXU3Ek__BackingField_0(float value)
	{
		___U3CminXU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CminYU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MinMax_t2491720607, ___U3CminYU3Ek__BackingField_1)); }
	inline float get_U3CminYU3Ek__BackingField_1() const { return ___U3CminYU3Ek__BackingField_1; }
	inline float* get_address_of_U3CminYU3Ek__BackingField_1() { return &___U3CminYU3Ek__BackingField_1; }
	inline void set_U3CminYU3Ek__BackingField_1(float value)
	{
		___U3CminYU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CmaxXU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MinMax_t2491720607, ___U3CmaxXU3Ek__BackingField_2)); }
	inline float get_U3CmaxXU3Ek__BackingField_2() const { return ___U3CmaxXU3Ek__BackingField_2; }
	inline float* get_address_of_U3CmaxXU3Ek__BackingField_2() { return &___U3CmaxXU3Ek__BackingField_2; }
	inline void set_U3CmaxXU3Ek__BackingField_2(float value)
	{
		___U3CmaxXU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CmaxYU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MinMax_t2491720607, ___U3CmaxYU3Ek__BackingField_3)); }
	inline float get_U3CmaxYU3Ek__BackingField_3() const { return ___U3CmaxYU3Ek__BackingField_3; }
	inline float* get_address_of_U3CmaxYU3Ek__BackingField_3() { return &___U3CmaxYU3Ek__BackingField_3; }
	inline void set_U3CmaxYU3Ek__BackingField_3(float value)
	{
		___U3CmaxYU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CxyBothU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MinMax_t2491720607, ___U3CxyBothU3Ek__BackingField_4)); }
	inline int32_t get_U3CxyBothU3Ek__BackingField_4() const { return ___U3CxyBothU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CxyBothU3Ek__BackingField_4() { return &___U3CxyBothU3Ek__BackingField_4; }
	inline void set_U3CxyBothU3Ek__BackingField_4(int32_t value)
	{
		___U3CxyBothU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CxyDiffU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MinMax_t2491720607, ___U3CxyDiffU3Ek__BackingField_5)); }
	inline float get_U3CxyDiffU3Ek__BackingField_5() const { return ___U3CxyDiffU3Ek__BackingField_5; }
	inline float* get_address_of_U3CxyDiffU3Ek__BackingField_5() { return &___U3CxyDiffU3Ek__BackingField_5; }
	inline void set_U3CxyDiffU3Ek__BackingField_5(float value)
	{
		___U3CxyDiffU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CxyOtherDiffU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MinMax_t2491720607, ___U3CxyOtherDiffU3Ek__BackingField_6)); }
	inline float get_U3CxyOtherDiffU3Ek__BackingField_6() const { return ___U3CxyOtherDiffU3Ek__BackingField_6; }
	inline float* get_address_of_U3CxyOtherDiffU3Ek__BackingField_6() { return &___U3CxyOtherDiffU3Ek__BackingField_6; }
	inline void set_U3CxyOtherDiffU3Ek__BackingField_6(float value)
	{
		___U3CxyOtherDiffU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINMAX_T2491720607_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef TMP_INPUTVALIDATOR_T3726817866_H
#define TMP_INPUTVALIDATOR_T3726817866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t3726817866  : public ScriptableObject_t1975622470
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T3726817866_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef NOSIG_T75605465_H
#define NOSIG_T75605465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EntryLink/NoSig
struct  NoSig_t75605465  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOSIG_T75605465_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef TMP_PHONENUMBERVALIDATOR_T3703967929_H
#define TMP_PHONENUMBERVALIDATOR_T3703967929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PhoneNumberValidator
struct  TMP_PhoneNumberValidator_t3703967929  : public TMP_InputValidator_t3726817866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PHONENUMBERVALIDATOR_T3703967929_H
#ifndef TMP_DIGITVALIDATOR_T3346203809_H
#define TMP_DIGITVALIDATOR_T3346203809_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DigitValidator
struct  TMP_DigitValidator_t3346203809  : public TMP_InputValidator_t3726817866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DIGITVALIDATOR_T3346203809_H
#ifndef BENCHMARK02_T39292249_H
#define BENCHMARK02_T39292249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t39292249  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t6181308 * ___floatingText_Script_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark02_t39292249, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark02_t39292249, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_4() { return static_cast<int32_t>(offsetof(Benchmark02_t39292249, ___floatingText_Script_4)); }
	inline TextMeshProFloatingText_t6181308 * get_floatingText_Script_4() const { return ___floatingText_Script_4; }
	inline TextMeshProFloatingText_t6181308 ** get_address_of_floatingText_Script_4() { return &___floatingText_Script_4; }
	inline void set_floatingText_Script_4(TextMeshProFloatingText_t6181308 * value)
	{
		___floatingText_Script_4 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T39292249_H
#ifndef BENCHMARK01_T2768175604_H
#define BENCHMARK01_T2768175604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t2768175604  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_2;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t2530419979 * ___TMProFont_3;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t4239498691 * ___TextMeshFont_4;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2521834357 * ___m_textMeshPro_5;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t4263764796 * ___m_textContainer_6;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1641806576 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t193706927 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t193706927 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_TMProFont_3() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___TMProFont_3)); }
	inline TMP_FontAsset_t2530419979 * get_TMProFont_3() const { return ___TMProFont_3; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_TMProFont_3() { return &___TMProFont_3; }
	inline void set_TMProFont_3(TMP_FontAsset_t2530419979 * value)
	{
		___TMProFont_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_3), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___TextMeshFont_4)); }
	inline Font_t4239498691 * get_TextMeshFont_4() const { return ___TextMeshFont_4; }
	inline Font_t4239498691 ** get_address_of_TextMeshFont_4() { return &___TextMeshFont_4; }
	inline void set_TextMeshFont_4(Font_t4239498691 * value)
	{
		___TextMeshFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_4), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_textMeshPro_5)); }
	inline TextMeshPro_t2521834357 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TextMeshPro_t2521834357 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_textContainer_6() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_textContainer_6)); }
	inline TextContainer_t4263764796 * get_m_textContainer_6() const { return ___m_textContainer_6; }
	inline TextContainer_t4263764796 ** get_address_of_m_textContainer_6() { return &___m_textContainer_6; }
	inline void set_m_textContainer_6(TextContainer_t4263764796 * value)
	{
		___m_textContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_textMesh_7)); }
	inline TextMesh_t1641806576 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1641806576 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1641806576 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_material01_10)); }
	inline Material_t193706927 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t193706927 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t193706927 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_t2768175604, ___m_material02_11)); }
	inline Material_t193706927 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t193706927 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t193706927 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T2768175604_H
#ifndef BENCHMARK01_UGUI_T3449578277_H
#define BENCHMARK01_UGUI_T3449578277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3449578277  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_2;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t209405766 * ___canvas_3;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t2530419979 * ___TMProFont_4;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t4239498691 * ___TextMeshFont_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t934157183 * ___m_textMeshPro_6;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t356221433 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t193706927 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t193706927 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_canvas_3() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___canvas_3)); }
	inline Canvas_t209405766 * get_canvas_3() const { return ___canvas_3; }
	inline Canvas_t209405766 ** get_address_of_canvas_3() { return &___canvas_3; }
	inline void set_canvas_3(Canvas_t209405766 * value)
	{
		___canvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_3), value);
	}

	inline static int32_t get_offset_of_TMProFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___TMProFont_4)); }
	inline TMP_FontAsset_t2530419979 * get_TMProFont_4() const { return ___TMProFont_4; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_TMProFont_4() { return &___TMProFont_4; }
	inline void set_TMProFont_4(TMP_FontAsset_t2530419979 * value)
	{
		___TMProFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_4), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___TextMeshFont_5)); }
	inline Font_t4239498691 * get_TextMeshFont_5() const { return ___TextMeshFont_5; }
	inline Font_t4239498691 ** get_address_of_TextMeshFont_5() { return &___TextMeshFont_5; }
	inline void set_TextMeshFont_5(Font_t4239498691 * value)
	{
		___TextMeshFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___m_textMeshPro_6)); }
	inline TextMeshProUGUI_t934157183 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshProUGUI_t934157183 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___m_textMesh_7)); }
	inline Text_t356221433 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline Text_t356221433 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(Text_t356221433 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___m_material01_10)); }
	inline Material_t193706927 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t193706927 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t193706927 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3449578277, ___m_material02_11)); }
	inline Material_t193706927 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t193706927 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t193706927 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_T3449578277_H
#ifndef TESTSCRIPT_T905662927_H
#define TESTSCRIPT_T905662927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestScript
struct  TestScript_t905662927  : public MonoBehaviour_t1158329972
{
public:
	// CSV.CSVReader TestScript::reader
	CSVReader_t2661561169 * ___reader_2;
	// System.Single[] TestScript::times
	SingleU5BU5D_t577127397* ___times_3;
	// System.Int32[] TestScript::speed
	Int32U5BU5D_t3030399641* ___speed_4;
	// System.Int32[] TestScript::rpm
	Int32U5BU5D_t3030399641* ___rpm_5;
	// System.Single[] TestScript::lat
	SingleU5BU5D_t577127397* ___lat_6;
	// System.Single[] TestScript::lon
	SingleU5BU5D_t577127397* ___lon_7;
	// System.Single[] TestScript::gX
	SingleU5BU5D_t577127397* ___gX_8;
	// System.Single[] TestScript::gY
	SingleU5BU5D_t577127397* ___gY_9;
	// UnityEngine.Video.VideoPlayer TestScript::video
	VideoPlayer_t10059812 * ___video_10;
	// UnityEngine.GameObject TestScript::canvas
	GameObject_t1756533147 * ___canvas_11;
	// MapCreator TestScript::mapCreator
	MapCreator_t2101643938 * ___mapCreator_12;
	// UnityEngine.GameObject TestScript::videoMenu
	GameObject_t1756533147 * ___videoMenu_13;
	// UnityEngine.Camera TestScript::viewCamera
	Camera_t189460977 * ___viewCamera_14;
	// System.Single TestScript::videoMenuDistance
	float ___videoMenuDistance_15;
	// RacingWidgets TestScript::widgets
	RacingWidgets_t915614521 * ___widgets_16;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___reader_2)); }
	inline CSVReader_t2661561169 * get_reader_2() const { return ___reader_2; }
	inline CSVReader_t2661561169 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(CSVReader_t2661561169 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_times_3() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___times_3)); }
	inline SingleU5BU5D_t577127397* get_times_3() const { return ___times_3; }
	inline SingleU5BU5D_t577127397** get_address_of_times_3() { return &___times_3; }
	inline void set_times_3(SingleU5BU5D_t577127397* value)
	{
		___times_3 = value;
		Il2CppCodeGenWriteBarrier((&___times_3), value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___speed_4)); }
	inline Int32U5BU5D_t3030399641* get_speed_4() const { return ___speed_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(Int32U5BU5D_t3030399641* value)
	{
		___speed_4 = value;
		Il2CppCodeGenWriteBarrier((&___speed_4), value);
	}

	inline static int32_t get_offset_of_rpm_5() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___rpm_5)); }
	inline Int32U5BU5D_t3030399641* get_rpm_5() const { return ___rpm_5; }
	inline Int32U5BU5D_t3030399641** get_address_of_rpm_5() { return &___rpm_5; }
	inline void set_rpm_5(Int32U5BU5D_t3030399641* value)
	{
		___rpm_5 = value;
		Il2CppCodeGenWriteBarrier((&___rpm_5), value);
	}

	inline static int32_t get_offset_of_lat_6() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___lat_6)); }
	inline SingleU5BU5D_t577127397* get_lat_6() const { return ___lat_6; }
	inline SingleU5BU5D_t577127397** get_address_of_lat_6() { return &___lat_6; }
	inline void set_lat_6(SingleU5BU5D_t577127397* value)
	{
		___lat_6 = value;
		Il2CppCodeGenWriteBarrier((&___lat_6), value);
	}

	inline static int32_t get_offset_of_lon_7() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___lon_7)); }
	inline SingleU5BU5D_t577127397* get_lon_7() const { return ___lon_7; }
	inline SingleU5BU5D_t577127397** get_address_of_lon_7() { return &___lon_7; }
	inline void set_lon_7(SingleU5BU5D_t577127397* value)
	{
		___lon_7 = value;
		Il2CppCodeGenWriteBarrier((&___lon_7), value);
	}

	inline static int32_t get_offset_of_gX_8() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___gX_8)); }
	inline SingleU5BU5D_t577127397* get_gX_8() const { return ___gX_8; }
	inline SingleU5BU5D_t577127397** get_address_of_gX_8() { return &___gX_8; }
	inline void set_gX_8(SingleU5BU5D_t577127397* value)
	{
		___gX_8 = value;
		Il2CppCodeGenWriteBarrier((&___gX_8), value);
	}

	inline static int32_t get_offset_of_gY_9() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___gY_9)); }
	inline SingleU5BU5D_t577127397* get_gY_9() const { return ___gY_9; }
	inline SingleU5BU5D_t577127397** get_address_of_gY_9() { return &___gY_9; }
	inline void set_gY_9(SingleU5BU5D_t577127397* value)
	{
		___gY_9 = value;
		Il2CppCodeGenWriteBarrier((&___gY_9), value);
	}

	inline static int32_t get_offset_of_video_10() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___video_10)); }
	inline VideoPlayer_t10059812 * get_video_10() const { return ___video_10; }
	inline VideoPlayer_t10059812 ** get_address_of_video_10() { return &___video_10; }
	inline void set_video_10(VideoPlayer_t10059812 * value)
	{
		___video_10 = value;
		Il2CppCodeGenWriteBarrier((&___video_10), value);
	}

	inline static int32_t get_offset_of_canvas_11() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___canvas_11)); }
	inline GameObject_t1756533147 * get_canvas_11() const { return ___canvas_11; }
	inline GameObject_t1756533147 ** get_address_of_canvas_11() { return &___canvas_11; }
	inline void set_canvas_11(GameObject_t1756533147 * value)
	{
		___canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_11), value);
	}

	inline static int32_t get_offset_of_mapCreator_12() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___mapCreator_12)); }
	inline MapCreator_t2101643938 * get_mapCreator_12() const { return ___mapCreator_12; }
	inline MapCreator_t2101643938 ** get_address_of_mapCreator_12() { return &___mapCreator_12; }
	inline void set_mapCreator_12(MapCreator_t2101643938 * value)
	{
		___mapCreator_12 = value;
		Il2CppCodeGenWriteBarrier((&___mapCreator_12), value);
	}

	inline static int32_t get_offset_of_videoMenu_13() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___videoMenu_13)); }
	inline GameObject_t1756533147 * get_videoMenu_13() const { return ___videoMenu_13; }
	inline GameObject_t1756533147 ** get_address_of_videoMenu_13() { return &___videoMenu_13; }
	inline void set_videoMenu_13(GameObject_t1756533147 * value)
	{
		___videoMenu_13 = value;
		Il2CppCodeGenWriteBarrier((&___videoMenu_13), value);
	}

	inline static int32_t get_offset_of_viewCamera_14() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___viewCamera_14)); }
	inline Camera_t189460977 * get_viewCamera_14() const { return ___viewCamera_14; }
	inline Camera_t189460977 ** get_address_of_viewCamera_14() { return &___viewCamera_14; }
	inline void set_viewCamera_14(Camera_t189460977 * value)
	{
		___viewCamera_14 = value;
		Il2CppCodeGenWriteBarrier((&___viewCamera_14), value);
	}

	inline static int32_t get_offset_of_videoMenuDistance_15() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___videoMenuDistance_15)); }
	inline float get_videoMenuDistance_15() const { return ___videoMenuDistance_15; }
	inline float* get_address_of_videoMenuDistance_15() { return &___videoMenuDistance_15; }
	inline void set_videoMenuDistance_15(float value)
	{
		___videoMenuDistance_15 = value;
	}

	inline static int32_t get_offset_of_widgets_16() { return static_cast<int32_t>(offsetof(TestScript_t905662927, ___widgets_16)); }
	inline RacingWidgets_t915614521 * get_widgets_16() const { return ___widgets_16; }
	inline RacingWidgets_t915614521 ** get_address_of_widgets_16() { return &___widgets_16; }
	inline void set_widgets_16(RacingWidgets_t915614521 * value)
	{
		___widgets_16 = value;
		Il2CppCodeGenWriteBarrier((&___widgets_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTSCRIPT_T905662927_H
#ifndef OBJECTSPIN_T3410945885_H
#define OBJECTSPIN_T3410945885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin
struct  ObjectSpin_t3410945885  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_2;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_3;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_t3275118058 * ___m_transform_4;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_5;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_t2243707580  ___m_prevPOS_6;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_t2243707580  ___m_initial_Rotation_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_t2243707580  ___m_initial_Position_8;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_t874517518  ___m_lightColor_9;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_10;
	// TMPro.Examples.ObjectSpin/MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_11;

public:
	inline static int32_t get_offset_of_SpinSpeed_2() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___SpinSpeed_2)); }
	inline float get_SpinSpeed_2() const { return ___SpinSpeed_2; }
	inline float* get_address_of_SpinSpeed_2() { return &___SpinSpeed_2; }
	inline void set_SpinSpeed_2(float value)
	{
		___SpinSpeed_2 = value;
	}

	inline static int32_t get_offset_of_RotationRange_3() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___RotationRange_3)); }
	inline int32_t get_RotationRange_3() const { return ___RotationRange_3; }
	inline int32_t* get_address_of_RotationRange_3() { return &___RotationRange_3; }
	inline void set_RotationRange_3(int32_t value)
	{
		___RotationRange_3 = value;
	}

	inline static int32_t get_offset_of_m_transform_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_transform_4)); }
	inline Transform_t3275118058 * get_m_transform_4() const { return ___m_transform_4; }
	inline Transform_t3275118058 ** get_address_of_m_transform_4() { return &___m_transform_4; }
	inline void set_m_transform_4(Transform_t3275118058 * value)
	{
		___m_transform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_4), value);
	}

	inline static int32_t get_offset_of_m_time_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_time_5)); }
	inline float get_m_time_5() const { return ___m_time_5; }
	inline float* get_address_of_m_time_5() { return &___m_time_5; }
	inline void set_m_time_5(float value)
	{
		___m_time_5 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_prevPOS_6)); }
	inline Vector3_t2243707580  get_m_prevPOS_6() const { return ___m_prevPOS_6; }
	inline Vector3_t2243707580 * get_address_of_m_prevPOS_6() { return &___m_prevPOS_6; }
	inline void set_m_prevPOS_6(Vector3_t2243707580  value)
	{
		___m_prevPOS_6 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_initial_Rotation_7)); }
	inline Vector3_t2243707580  get_m_initial_Rotation_7() const { return ___m_initial_Rotation_7; }
	inline Vector3_t2243707580 * get_address_of_m_initial_Rotation_7() { return &___m_initial_Rotation_7; }
	inline void set_m_initial_Rotation_7(Vector3_t2243707580  value)
	{
		___m_initial_Rotation_7 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_initial_Position_8)); }
	inline Vector3_t2243707580  get_m_initial_Position_8() const { return ___m_initial_Position_8; }
	inline Vector3_t2243707580 * get_address_of_m_initial_Position_8() { return &___m_initial_Position_8; }
	inline void set_m_initial_Position_8(Vector3_t2243707580  value)
	{
		___m_initial_Position_8 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___m_lightColor_9)); }
	inline Color32_t874517518  get_m_lightColor_9() const { return ___m_lightColor_9; }
	inline Color32_t874517518 * get_address_of_m_lightColor_9() { return &___m_lightColor_9; }
	inline void set_m_lightColor_9(Color32_t874517518  value)
	{
		___m_lightColor_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___frames_10)); }
	inline int32_t get_frames_10() const { return ___frames_10; }
	inline int32_t* get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(int32_t value)
	{
		___frames_10 = value;
	}

	inline static int32_t get_offset_of_Motion_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t3410945885, ___Motion_11)); }
	inline int32_t get_Motion_11() const { return ___Motion_11; }
	inline int32_t* get_address_of_Motion_11() { return &___Motion_11; }
	inline void set_Motion_11(int32_t value)
	{
		___Motion_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPIN_T3410945885_H
#ifndef CHATCONTROLLER_T2669781690_H
#define CHATCONTROLLER_T2669781690_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatController
struct  ChatController_t2669781690  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_t1778301588 * ___TMP_ChatInput_2;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t1920000777 * ___TMP_ChatOutput_3;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_t3248359358 * ___ChatScrollbar_4;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_2() { return static_cast<int32_t>(offsetof(ChatController_t2669781690, ___TMP_ChatInput_2)); }
	inline TMP_InputField_t1778301588 * get_TMP_ChatInput_2() const { return ___TMP_ChatInput_2; }
	inline TMP_InputField_t1778301588 ** get_address_of_TMP_ChatInput_2() { return &___TMP_ChatInput_2; }
	inline void set_TMP_ChatInput_2(TMP_InputField_t1778301588 * value)
	{
		___TMP_ChatInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatInput_2), value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_3() { return static_cast<int32_t>(offsetof(ChatController_t2669781690, ___TMP_ChatOutput_3)); }
	inline TMP_Text_t1920000777 * get_TMP_ChatOutput_3() const { return ___TMP_ChatOutput_3; }
	inline TMP_Text_t1920000777 ** get_address_of_TMP_ChatOutput_3() { return &___TMP_ChatOutput_3; }
	inline void set_TMP_ChatOutput_3(TMP_Text_t1920000777 * value)
	{
		___TMP_ChatOutput_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatOutput_3), value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_4() { return static_cast<int32_t>(offsetof(ChatController_t2669781690, ___ChatScrollbar_4)); }
	inline Scrollbar_t3248359358 * get_ChatScrollbar_4() const { return ___ChatScrollbar_4; }
	inline Scrollbar_t3248359358 ** get_address_of_ChatScrollbar_4() { return &___ChatScrollbar_4; }
	inline void set_ChatScrollbar_4(Scrollbar_t3248359358 * value)
	{
		___ChatScrollbar_4 = value;
		Il2CppCodeGenWriteBarrier((&___ChatScrollbar_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCONTROLLER_T2669781690_H
#ifndef ENVMAPANIMATOR_T1635389402_H
#define ENVMAPANIMATOR_T1635389402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t1635389402  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t2243707580  ___RotationSpeeds_2;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t1920000777 * ___m_textMeshPro_3;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t193706927 * ___m_material_4;

public:
	inline static int32_t get_offset_of_RotationSpeeds_2() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1635389402, ___RotationSpeeds_2)); }
	inline Vector3_t2243707580  get_RotationSpeeds_2() const { return ___RotationSpeeds_2; }
	inline Vector3_t2243707580 * get_address_of_RotationSpeeds_2() { return &___RotationSpeeds_2; }
	inline void set_RotationSpeeds_2(Vector3_t2243707580  value)
	{
		___RotationSpeeds_2 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_3() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1635389402, ___m_textMeshPro_3)); }
	inline TMP_Text_t1920000777 * get_m_textMeshPro_3() const { return ___m_textMeshPro_3; }
	inline TMP_Text_t1920000777 ** get_address_of_m_textMeshPro_3() { return &___m_textMeshPro_3; }
	inline void set_m_textMeshPro_3(TMP_Text_t1920000777 * value)
	{
		___m_textMeshPro_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_3), value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1635389402, ___m_material_4)); }
	inline Material_t193706927 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t193706927 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t193706927 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVMAPANIMATOR_T1635389402_H
#ifndef CAMERACONTROLLER_T766129913_H
#define CAMERACONTROLLER_T766129913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController
struct  CameraController_t766129913  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_t3275118058 * ___cameraTransform_2;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_t3275118058 * ___dummyTarget_3;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_t3275118058 * ___CameraTarget_4;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_5;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_6;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_7;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_8;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_9;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_11;
	// TMPro.Examples.CameraController/CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_12;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_13;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_14;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_15;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_16;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_17;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_18;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_t2243707580  ___currentVelocity_19;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_t2243707580  ___desiredPosition_20;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_21;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_22;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_t2243707580  ___moveVector_23;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_24;

public:
	inline static int32_t get_offset_of_cameraTransform_2() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___cameraTransform_2)); }
	inline Transform_t3275118058 * get_cameraTransform_2() const { return ___cameraTransform_2; }
	inline Transform_t3275118058 ** get_address_of_cameraTransform_2() { return &___cameraTransform_2; }
	inline void set_cameraTransform_2(Transform_t3275118058 * value)
	{
		___cameraTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_2), value);
	}

	inline static int32_t get_offset_of_dummyTarget_3() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___dummyTarget_3)); }
	inline Transform_t3275118058 * get_dummyTarget_3() const { return ___dummyTarget_3; }
	inline Transform_t3275118058 ** get_address_of_dummyTarget_3() { return &___dummyTarget_3; }
	inline void set_dummyTarget_3(Transform_t3275118058 * value)
	{
		___dummyTarget_3 = value;
		Il2CppCodeGenWriteBarrier((&___dummyTarget_3), value);
	}

	inline static int32_t get_offset_of_CameraTarget_4() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___CameraTarget_4)); }
	inline Transform_t3275118058 * get_CameraTarget_4() const { return ___CameraTarget_4; }
	inline Transform_t3275118058 ** get_address_of_CameraTarget_4() { return &___CameraTarget_4; }
	inline void set_CameraTarget_4(Transform_t3275118058 * value)
	{
		___CameraTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTarget_4), value);
	}

	inline static int32_t get_offset_of_FollowDistance_5() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___FollowDistance_5)); }
	inline float get_FollowDistance_5() const { return ___FollowDistance_5; }
	inline float* get_address_of_FollowDistance_5() { return &___FollowDistance_5; }
	inline void set_FollowDistance_5(float value)
	{
		___FollowDistance_5 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_6() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MaxFollowDistance_6)); }
	inline float get_MaxFollowDistance_6() const { return ___MaxFollowDistance_6; }
	inline float* get_address_of_MaxFollowDistance_6() { return &___MaxFollowDistance_6; }
	inline void set_MaxFollowDistance_6(float value)
	{
		___MaxFollowDistance_6 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MinFollowDistance_7)); }
	inline float get_MinFollowDistance_7() const { return ___MinFollowDistance_7; }
	inline float* get_address_of_MinFollowDistance_7() { return &___MinFollowDistance_7; }
	inline void set_MinFollowDistance_7(float value)
	{
		___MinFollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_8() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___ElevationAngle_8)); }
	inline float get_ElevationAngle_8() const { return ___ElevationAngle_8; }
	inline float* get_address_of_ElevationAngle_8() { return &___ElevationAngle_8; }
	inline void set_ElevationAngle_8(float value)
	{
		___ElevationAngle_8 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_9() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MaxElevationAngle_9)); }
	inline float get_MaxElevationAngle_9() const { return ___MaxElevationAngle_9; }
	inline float* get_address_of_MaxElevationAngle_9() { return &___MaxElevationAngle_9; }
	inline void set_MaxElevationAngle_9(float value)
	{
		___MaxElevationAngle_9 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MinElevationAngle_10)); }
	inline float get_MinElevationAngle_10() const { return ___MinElevationAngle_10; }
	inline float* get_address_of_MinElevationAngle_10() { return &___MinElevationAngle_10; }
	inline void set_MinElevationAngle_10(float value)
	{
		___MinElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_11() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___OrbitalAngle_11)); }
	inline float get_OrbitalAngle_11() const { return ___OrbitalAngle_11; }
	inline float* get_address_of_OrbitalAngle_11() { return &___OrbitalAngle_11; }
	inline void set_OrbitalAngle_11(float value)
	{
		___OrbitalAngle_11 = value;
	}

	inline static int32_t get_offset_of_CameraMode_12() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___CameraMode_12)); }
	inline int32_t get_CameraMode_12() const { return ___CameraMode_12; }
	inline int32_t* get_address_of_CameraMode_12() { return &___CameraMode_12; }
	inline void set_CameraMode_12(int32_t value)
	{
		___CameraMode_12 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_13() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MovementSmoothing_13)); }
	inline bool get_MovementSmoothing_13() const { return ___MovementSmoothing_13; }
	inline bool* get_address_of_MovementSmoothing_13() { return &___MovementSmoothing_13; }
	inline void set_MovementSmoothing_13(bool value)
	{
		___MovementSmoothing_13 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_14() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___RotationSmoothing_14)); }
	inline bool get_RotationSmoothing_14() const { return ___RotationSmoothing_14; }
	inline bool* get_address_of_RotationSmoothing_14() { return &___RotationSmoothing_14; }
	inline void set_RotationSmoothing_14(bool value)
	{
		___RotationSmoothing_14 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___previousSmoothing_15)); }
	inline bool get_previousSmoothing_15() const { return ___previousSmoothing_15; }
	inline bool* get_address_of_previousSmoothing_15() { return &___previousSmoothing_15; }
	inline void set_previousSmoothing_15(bool value)
	{
		___previousSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_16() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MovementSmoothingValue_16)); }
	inline float get_MovementSmoothingValue_16() const { return ___MovementSmoothingValue_16; }
	inline float* get_address_of_MovementSmoothingValue_16() { return &___MovementSmoothingValue_16; }
	inline void set_MovementSmoothingValue_16(float value)
	{
		___MovementSmoothingValue_16 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_17() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___RotationSmoothingValue_17)); }
	inline float get_RotationSmoothingValue_17() const { return ___RotationSmoothingValue_17; }
	inline float* get_address_of_RotationSmoothingValue_17() { return &___RotationSmoothingValue_17; }
	inline void set_RotationSmoothingValue_17(float value)
	{
		___RotationSmoothingValue_17 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_18() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___MoveSensitivity_18)); }
	inline float get_MoveSensitivity_18() const { return ___MoveSensitivity_18; }
	inline float* get_address_of_MoveSensitivity_18() { return &___MoveSensitivity_18; }
	inline void set_MoveSensitivity_18(float value)
	{
		___MoveSensitivity_18 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_19() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___currentVelocity_19)); }
	inline Vector3_t2243707580  get_currentVelocity_19() const { return ___currentVelocity_19; }
	inline Vector3_t2243707580 * get_address_of_currentVelocity_19() { return &___currentVelocity_19; }
	inline void set_currentVelocity_19(Vector3_t2243707580  value)
	{
		___currentVelocity_19 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_20() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___desiredPosition_20)); }
	inline Vector3_t2243707580  get_desiredPosition_20() const { return ___desiredPosition_20; }
	inline Vector3_t2243707580 * get_address_of_desiredPosition_20() { return &___desiredPosition_20; }
	inline void set_desiredPosition_20(Vector3_t2243707580  value)
	{
		___desiredPosition_20 = value;
	}

	inline static int32_t get_offset_of_mouseX_21() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___mouseX_21)); }
	inline float get_mouseX_21() const { return ___mouseX_21; }
	inline float* get_address_of_mouseX_21() { return &___mouseX_21; }
	inline void set_mouseX_21(float value)
	{
		___mouseX_21 = value;
	}

	inline static int32_t get_offset_of_mouseY_22() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___mouseY_22)); }
	inline float get_mouseY_22() const { return ___mouseY_22; }
	inline float* get_address_of_mouseY_22() { return &___mouseY_22; }
	inline void set_mouseY_22(float value)
	{
		___mouseY_22 = value;
	}

	inline static int32_t get_offset_of_moveVector_23() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___moveVector_23)); }
	inline Vector3_t2243707580  get_moveVector_23() const { return ___moveVector_23; }
	inline Vector3_t2243707580 * get_address_of_moveVector_23() { return &___moveVector_23; }
	inline void set_moveVector_23(Vector3_t2243707580  value)
	{
		___moveVector_23 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_24() { return static_cast<int32_t>(offsetof(CameraController_t766129913, ___mouseWheel_24)); }
	inline float get_mouseWheel_24() const { return ___mouseWheel_24; }
	inline float* get_address_of_mouseWheel_24() { return &___mouseWheel_24; }
	inline void set_mouseWheel_24(float value)
	{
		___mouseWheel_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T766129913_H
#ifndef BENCHMARK03_T1605376190_H
#define BENCHMARK03_T1605376190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_t1605376190  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t4239498691 * ___TheFont_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark03_t1605376190, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark03_t1605376190, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(Benchmark03_t1605376190, ___TheFont_4)); }
	inline Font_t4239498691 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t4239498691 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t4239498691 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_T1605376190_H
#ifndef BENCHMARK04_T3171460131_H
#define BENCHMARK04_T3171460131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark04
struct  Benchmark04_t3171460131  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_3;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_4;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_5;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_t3275118058 * ___m_Transform_6;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_3() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___MinPointSize_3)); }
	inline int32_t get_MinPointSize_3() const { return ___MinPointSize_3; }
	inline int32_t* get_address_of_MinPointSize_3() { return &___MinPointSize_3; }
	inline void set_MinPointSize_3(int32_t value)
	{
		___MinPointSize_3 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_4() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___MaxPointSize_4)); }
	inline int32_t get_MaxPointSize_4() const { return ___MaxPointSize_4; }
	inline int32_t* get_address_of_MaxPointSize_4() { return &___MaxPointSize_4; }
	inline void set_MaxPointSize_4(int32_t value)
	{
		___MaxPointSize_4 = value;
	}

	inline static int32_t get_offset_of_Steps_5() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___Steps_5)); }
	inline int32_t get_Steps_5() const { return ___Steps_5; }
	inline int32_t* get_address_of_Steps_5() { return &___Steps_5; }
	inline void set_Steps_5(int32_t value)
	{
		___Steps_5 = value;
	}

	inline static int32_t get_offset_of_m_Transform_6() { return static_cast<int32_t>(offsetof(Benchmark04_t3171460131, ___m_Transform_6)); }
	inline Transform_t3275118058 * get_m_Transform_6() const { return ___m_Transform_6; }
	inline Transform_t3275118058 ** get_address_of_m_Transform_6() { return &___m_Transform_6; }
	inline void set_m_Transform_6(Transform_t3275118058 * value)
	{
		___m_Transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK04_T3171460131_H
#ifndef CAMERARAYCAST_T2547618150_H
#define CAMERARAYCAST_T2547618150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraRaycast
struct  CameraRaycast_t2547618150  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera CameraRaycast::cam
	Camera_t189460977 * ___cam_2;
	// UnityEngine.EventSystems.EventSystem CameraRaycast::eventSystem
	EventSystem_t3466835263 * ___eventSystem_3;
	// UnityEngine.GameObject CameraRaycast::lastObjectDown
	GameObject_t1756533147 * ___lastObjectDown_4;
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> CameraRaycast::results
	List_1_t3685274804 * ___results_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> CameraRaycast::mouseOver
	List_1_t1125654279 * ___mouseOver_6;
	// UnityEngine.EventSystems.PointerEventData CameraRaycast::pointerData
	PointerEventData_t1599784723 * ___pointerData_7;

public:
	inline static int32_t get_offset_of_cam_2() { return static_cast<int32_t>(offsetof(CameraRaycast_t2547618150, ___cam_2)); }
	inline Camera_t189460977 * get_cam_2() const { return ___cam_2; }
	inline Camera_t189460977 ** get_address_of_cam_2() { return &___cam_2; }
	inline void set_cam_2(Camera_t189460977 * value)
	{
		___cam_2 = value;
		Il2CppCodeGenWriteBarrier((&___cam_2), value);
	}

	inline static int32_t get_offset_of_eventSystem_3() { return static_cast<int32_t>(offsetof(CameraRaycast_t2547618150, ___eventSystem_3)); }
	inline EventSystem_t3466835263 * get_eventSystem_3() const { return ___eventSystem_3; }
	inline EventSystem_t3466835263 ** get_address_of_eventSystem_3() { return &___eventSystem_3; }
	inline void set_eventSystem_3(EventSystem_t3466835263 * value)
	{
		___eventSystem_3 = value;
		Il2CppCodeGenWriteBarrier((&___eventSystem_3), value);
	}

	inline static int32_t get_offset_of_lastObjectDown_4() { return static_cast<int32_t>(offsetof(CameraRaycast_t2547618150, ___lastObjectDown_4)); }
	inline GameObject_t1756533147 * get_lastObjectDown_4() const { return ___lastObjectDown_4; }
	inline GameObject_t1756533147 ** get_address_of_lastObjectDown_4() { return &___lastObjectDown_4; }
	inline void set_lastObjectDown_4(GameObject_t1756533147 * value)
	{
		___lastObjectDown_4 = value;
		Il2CppCodeGenWriteBarrier((&___lastObjectDown_4), value);
	}

	inline static int32_t get_offset_of_results_5() { return static_cast<int32_t>(offsetof(CameraRaycast_t2547618150, ___results_5)); }
	inline List_1_t3685274804 * get_results_5() const { return ___results_5; }
	inline List_1_t3685274804 ** get_address_of_results_5() { return &___results_5; }
	inline void set_results_5(List_1_t3685274804 * value)
	{
		___results_5 = value;
		Il2CppCodeGenWriteBarrier((&___results_5), value);
	}

	inline static int32_t get_offset_of_mouseOver_6() { return static_cast<int32_t>(offsetof(CameraRaycast_t2547618150, ___mouseOver_6)); }
	inline List_1_t1125654279 * get_mouseOver_6() const { return ___mouseOver_6; }
	inline List_1_t1125654279 ** get_address_of_mouseOver_6() { return &___mouseOver_6; }
	inline void set_mouseOver_6(List_1_t1125654279 * value)
	{
		___mouseOver_6 = value;
		Il2CppCodeGenWriteBarrier((&___mouseOver_6), value);
	}

	inline static int32_t get_offset_of_pointerData_7() { return static_cast<int32_t>(offsetof(CameraRaycast_t2547618150, ___pointerData_7)); }
	inline PointerEventData_t1599784723 * get_pointerData_7() const { return ___pointerData_7; }
	inline PointerEventData_t1599784723 ** get_address_of_pointerData_7() { return &___pointerData_7; }
	inline void set_pointerData_7(PointerEventData_t1599784723 * value)
	{
		___pointerData_7 = value;
		Il2CppCodeGenWriteBarrier((&___pointerData_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERARAYCAST_T2547618150_H
#ifndef ENTRYLINK_T1717819552_H
#define ENTRYLINK_T1717819552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EntryLink
struct  EntryLink_t1717819552  : public MonoBehaviour_t1158329972
{
public:
	// System.String EntryLink::<keyLink>k__BackingField
	String_t* ___U3CkeyLinkU3Ek__BackingField_2;
	// System.String EntryLink::<URLLink>k__BackingField
	String_t* ___U3CURLLinkU3Ek__BackingField_3;
	// System.String EntryLink::<CSVLink>k__BackingField
	String_t* ___U3CCSVLinkU3Ek__BackingField_4;
	// TMPro.TextMeshProUGUI EntryLink::entryText
	TextMeshProUGUI_t934157183 * ___entryText_5;
	// UnityEngine.UI.Image EntryLink::image
	Image_t2042527209 * ___image_6;
	// System.Single EntryLink::exitAlpha
	float ___exitAlpha_7;
	// System.Single EntryLink::enterAlpha
	float ___enterAlpha_8;
	// System.Single EntryLink::clickAlpha
	float ___clickAlpha_9;
	// System.Boolean EntryLink::isOn
	bool ___isOn_11;

public:
	inline static int32_t get_offset_of_U3CkeyLinkU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552, ___U3CkeyLinkU3Ek__BackingField_2)); }
	inline String_t* get_U3CkeyLinkU3Ek__BackingField_2() const { return ___U3CkeyLinkU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CkeyLinkU3Ek__BackingField_2() { return &___U3CkeyLinkU3Ek__BackingField_2; }
	inline void set_U3CkeyLinkU3Ek__BackingField_2(String_t* value)
	{
		___U3CkeyLinkU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyLinkU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CURLLinkU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552, ___U3CURLLinkU3Ek__BackingField_3)); }
	inline String_t* get_U3CURLLinkU3Ek__BackingField_3() const { return ___U3CURLLinkU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CURLLinkU3Ek__BackingField_3() { return &___U3CURLLinkU3Ek__BackingField_3; }
	inline void set_U3CURLLinkU3Ek__BackingField_3(String_t* value)
	{
		___U3CURLLinkU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CURLLinkU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCSVLinkU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552, ___U3CCSVLinkU3Ek__BackingField_4)); }
	inline String_t* get_U3CCSVLinkU3Ek__BackingField_4() const { return ___U3CCSVLinkU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CCSVLinkU3Ek__BackingField_4() { return &___U3CCSVLinkU3Ek__BackingField_4; }
	inline void set_U3CCSVLinkU3Ek__BackingField_4(String_t* value)
	{
		___U3CCSVLinkU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCSVLinkU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_entryText_5() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552, ___entryText_5)); }
	inline TextMeshProUGUI_t934157183 * get_entryText_5() const { return ___entryText_5; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_entryText_5() { return &___entryText_5; }
	inline void set_entryText_5(TextMeshProUGUI_t934157183 * value)
	{
		___entryText_5 = value;
		Il2CppCodeGenWriteBarrier((&___entryText_5), value);
	}

	inline static int32_t get_offset_of_image_6() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552, ___image_6)); }
	inline Image_t2042527209 * get_image_6() const { return ___image_6; }
	inline Image_t2042527209 ** get_address_of_image_6() { return &___image_6; }
	inline void set_image_6(Image_t2042527209 * value)
	{
		___image_6 = value;
		Il2CppCodeGenWriteBarrier((&___image_6), value);
	}

	inline static int32_t get_offset_of_exitAlpha_7() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552, ___exitAlpha_7)); }
	inline float get_exitAlpha_7() const { return ___exitAlpha_7; }
	inline float* get_address_of_exitAlpha_7() { return &___exitAlpha_7; }
	inline void set_exitAlpha_7(float value)
	{
		___exitAlpha_7 = value;
	}

	inline static int32_t get_offset_of_enterAlpha_8() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552, ___enterAlpha_8)); }
	inline float get_enterAlpha_8() const { return ___enterAlpha_8; }
	inline float* get_address_of_enterAlpha_8() { return &___enterAlpha_8; }
	inline void set_enterAlpha_8(float value)
	{
		___enterAlpha_8 = value;
	}

	inline static int32_t get_offset_of_clickAlpha_9() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552, ___clickAlpha_9)); }
	inline float get_clickAlpha_9() const { return ___clickAlpha_9; }
	inline float* get_address_of_clickAlpha_9() { return &___clickAlpha_9; }
	inline void set_clickAlpha_9(float value)
	{
		___clickAlpha_9 = value;
	}

	inline static int32_t get_offset_of_isOn_11() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552, ___isOn_11)); }
	inline bool get_isOn_11() const { return ___isOn_11; }
	inline bool* get_address_of_isOn_11() { return &___isOn_11; }
	inline void set_isOn_11(bool value)
	{
		___isOn_11 = value;
	}
};

struct EntryLink_t1717819552_StaticFields
{
public:
	// EntryLink EntryLink::currentLink
	EntryLink_t1717819552 * ___currentLink_10;
	// EntryLink/NoSig EntryLink::SelectAssigned
	NoSig_t75605465 * ___SelectAssigned_12;

public:
	inline static int32_t get_offset_of_currentLink_10() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552_StaticFields, ___currentLink_10)); }
	inline EntryLink_t1717819552 * get_currentLink_10() const { return ___currentLink_10; }
	inline EntryLink_t1717819552 ** get_address_of_currentLink_10() { return &___currentLink_10; }
	inline void set_currentLink_10(EntryLink_t1717819552 * value)
	{
		___currentLink_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentLink_10), value);
	}

	inline static int32_t get_offset_of_SelectAssigned_12() { return static_cast<int32_t>(offsetof(EntryLink_t1717819552_StaticFields, ___SelectAssigned_12)); }
	inline NoSig_t75605465 * get_SelectAssigned_12() const { return ___SelectAssigned_12; }
	inline NoSig_t75605465 ** get_address_of_SelectAssigned_12() { return &___SelectAssigned_12; }
	inline void set_SelectAssigned_12(NoSig_t75605465 * value)
	{
		___SelectAssigned_12 = value;
		Il2CppCodeGenWriteBarrier((&___SelectAssigned_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRYLINK_T1717819552_H
#ifndef ROADCREATOR_T1201856826_H
#define ROADCREATOR_T1201856826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoadCreator
struct  RoadCreator_t1201856826  : public MonoBehaviour_t1158329972
{
public:
	// System.Single RoadCreator::spacing
	float ___spacing_2;
	// System.Single RoadCreator::roadWidth
	float ___roadWidth_3;
	// System.Boolean RoadCreator::autoUpdate
	bool ___autoUpdate_4;
	// System.Single RoadCreator::tiling
	float ___tiling_5;
	// RoadCreator/VertPositionSpace RoadCreator::vertSpace
	int32_t ___vertSpace_6;

public:
	inline static int32_t get_offset_of_spacing_2() { return static_cast<int32_t>(offsetof(RoadCreator_t1201856826, ___spacing_2)); }
	inline float get_spacing_2() const { return ___spacing_2; }
	inline float* get_address_of_spacing_2() { return &___spacing_2; }
	inline void set_spacing_2(float value)
	{
		___spacing_2 = value;
	}

	inline static int32_t get_offset_of_roadWidth_3() { return static_cast<int32_t>(offsetof(RoadCreator_t1201856826, ___roadWidth_3)); }
	inline float get_roadWidth_3() const { return ___roadWidth_3; }
	inline float* get_address_of_roadWidth_3() { return &___roadWidth_3; }
	inline void set_roadWidth_3(float value)
	{
		___roadWidth_3 = value;
	}

	inline static int32_t get_offset_of_autoUpdate_4() { return static_cast<int32_t>(offsetof(RoadCreator_t1201856826, ___autoUpdate_4)); }
	inline bool get_autoUpdate_4() const { return ___autoUpdate_4; }
	inline bool* get_address_of_autoUpdate_4() { return &___autoUpdate_4; }
	inline void set_autoUpdate_4(bool value)
	{
		___autoUpdate_4 = value;
	}

	inline static int32_t get_offset_of_tiling_5() { return static_cast<int32_t>(offsetof(RoadCreator_t1201856826, ___tiling_5)); }
	inline float get_tiling_5() const { return ___tiling_5; }
	inline float* get_address_of_tiling_5() { return &___tiling_5; }
	inline void set_tiling_5(float value)
	{
		___tiling_5 = value;
	}

	inline static int32_t get_offset_of_vertSpace_6() { return static_cast<int32_t>(offsetof(RoadCreator_t1201856826, ___vertSpace_6)); }
	inline int32_t get_vertSpace_6() const { return ___vertSpace_6; }
	inline int32_t* get_address_of_vertSpace_6() { return &___vertSpace_6; }
	inline void set_vertSpace_6(int32_t value)
	{
		___vertSpace_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROADCREATOR_T1201856826_H
#ifndef PATHCREATOR_T1368626459_H
#define PATHCREATOR_T1368626459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathCreator
struct  PathCreator_t1368626459  : public MonoBehaviour_t1158329972
{
public:
	// BezierPath PathCreator::path
	BezierPath_t978602816 * ___path_2;
	// UnityEngine.Color PathCreator::anchorCol
	Color_t2020392075  ___anchorCol_3;
	// UnityEngine.Color PathCreator::controlCol
	Color_t2020392075  ___controlCol_4;
	// UnityEngine.Color PathCreator::segmentCol
	Color_t2020392075  ___segmentCol_5;
	// UnityEngine.Color PathCreator::selectedSegmentCol
	Color_t2020392075  ___selectedSegmentCol_6;
	// System.Single PathCreator::anchorDiameter
	float ___anchorDiameter_7;
	// System.Single PathCreator::controlDiameter
	float ___controlDiameter_8;
	// System.Boolean PathCreator::displayControlPoints
	bool ___displayControlPoints_9;

public:
	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(PathCreator_t1368626459, ___path_2)); }
	inline BezierPath_t978602816 * get_path_2() const { return ___path_2; }
	inline BezierPath_t978602816 ** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(BezierPath_t978602816 * value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((&___path_2), value);
	}

	inline static int32_t get_offset_of_anchorCol_3() { return static_cast<int32_t>(offsetof(PathCreator_t1368626459, ___anchorCol_3)); }
	inline Color_t2020392075  get_anchorCol_3() const { return ___anchorCol_3; }
	inline Color_t2020392075 * get_address_of_anchorCol_3() { return &___anchorCol_3; }
	inline void set_anchorCol_3(Color_t2020392075  value)
	{
		___anchorCol_3 = value;
	}

	inline static int32_t get_offset_of_controlCol_4() { return static_cast<int32_t>(offsetof(PathCreator_t1368626459, ___controlCol_4)); }
	inline Color_t2020392075  get_controlCol_4() const { return ___controlCol_4; }
	inline Color_t2020392075 * get_address_of_controlCol_4() { return &___controlCol_4; }
	inline void set_controlCol_4(Color_t2020392075  value)
	{
		___controlCol_4 = value;
	}

	inline static int32_t get_offset_of_segmentCol_5() { return static_cast<int32_t>(offsetof(PathCreator_t1368626459, ___segmentCol_5)); }
	inline Color_t2020392075  get_segmentCol_5() const { return ___segmentCol_5; }
	inline Color_t2020392075 * get_address_of_segmentCol_5() { return &___segmentCol_5; }
	inline void set_segmentCol_5(Color_t2020392075  value)
	{
		___segmentCol_5 = value;
	}

	inline static int32_t get_offset_of_selectedSegmentCol_6() { return static_cast<int32_t>(offsetof(PathCreator_t1368626459, ___selectedSegmentCol_6)); }
	inline Color_t2020392075  get_selectedSegmentCol_6() const { return ___selectedSegmentCol_6; }
	inline Color_t2020392075 * get_address_of_selectedSegmentCol_6() { return &___selectedSegmentCol_6; }
	inline void set_selectedSegmentCol_6(Color_t2020392075  value)
	{
		___selectedSegmentCol_6 = value;
	}

	inline static int32_t get_offset_of_anchorDiameter_7() { return static_cast<int32_t>(offsetof(PathCreator_t1368626459, ___anchorDiameter_7)); }
	inline float get_anchorDiameter_7() const { return ___anchorDiameter_7; }
	inline float* get_address_of_anchorDiameter_7() { return &___anchorDiameter_7; }
	inline void set_anchorDiameter_7(float value)
	{
		___anchorDiameter_7 = value;
	}

	inline static int32_t get_offset_of_controlDiameter_8() { return static_cast<int32_t>(offsetof(PathCreator_t1368626459, ___controlDiameter_8)); }
	inline float get_controlDiameter_8() const { return ___controlDiameter_8; }
	inline float* get_address_of_controlDiameter_8() { return &___controlDiameter_8; }
	inline void set_controlDiameter_8(float value)
	{
		___controlDiameter_8 = value;
	}

	inline static int32_t get_offset_of_displayControlPoints_9() { return static_cast<int32_t>(offsetof(PathCreator_t1368626459, ___displayControlPoints_9)); }
	inline bool get_displayControlPoints_9() const { return ___displayControlPoints_9; }
	inline bool* get_address_of_displayControlPoints_9() { return &___displayControlPoints_9; }
	inline void set_displayControlPoints_9(bool value)
	{
		___displayControlPoints_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCREATOR_T1368626459_H
#ifndef PATHPLACER_T683486292_H
#define PATHPLACER_T683486292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PathPlacer
struct  PathPlacer_t683486292  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PathPlacer::spacing
	float ___spacing_2;
	// System.Single PathPlacer::resolution
	float ___resolution_3;

public:
	inline static int32_t get_offset_of_spacing_2() { return static_cast<int32_t>(offsetof(PathPlacer_t683486292, ___spacing_2)); }
	inline float get_spacing_2() const { return ___spacing_2; }
	inline float* get_address_of_spacing_2() { return &___spacing_2; }
	inline void set_spacing_2(float value)
	{
		___spacing_2 = value;
	}

	inline static int32_t get_offset_of_resolution_3() { return static_cast<int32_t>(offsetof(PathPlacer_t683486292, ___resolution_3)); }
	inline float get_resolution_3() const { return ___resolution_3; }
	inline float* get_address_of_resolution_3() { return &___resolution_3; }
	inline void set_resolution_3(float value)
	{
		___resolution_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHPLACER_T683486292_H
#ifndef GFORCE_T1402080092_H
#define GFORCE_T1402080092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GForce
struct  GForce_t1402080092  : public MonoBehaviour_t1158329972
{
public:
	// System.Single GForce::minForce
	float ___minForce_2;
	// System.Single GForce::maxForce
	float ___maxForce_3;
	// System.Single GForce::maxPosition
	float ___maxPosition_4;
	// System.Single GForce::maxRadians
	float ___maxRadians_5;

public:
	inline static int32_t get_offset_of_minForce_2() { return static_cast<int32_t>(offsetof(GForce_t1402080092, ___minForce_2)); }
	inline float get_minForce_2() const { return ___minForce_2; }
	inline float* get_address_of_minForce_2() { return &___minForce_2; }
	inline void set_minForce_2(float value)
	{
		___minForce_2 = value;
	}

	inline static int32_t get_offset_of_maxForce_3() { return static_cast<int32_t>(offsetof(GForce_t1402080092, ___maxForce_3)); }
	inline float get_maxForce_3() const { return ___maxForce_3; }
	inline float* get_address_of_maxForce_3() { return &___maxForce_3; }
	inline void set_maxForce_3(float value)
	{
		___maxForce_3 = value;
	}

	inline static int32_t get_offset_of_maxPosition_4() { return static_cast<int32_t>(offsetof(GForce_t1402080092, ___maxPosition_4)); }
	inline float get_maxPosition_4() const { return ___maxPosition_4; }
	inline float* get_address_of_maxPosition_4() { return &___maxPosition_4; }
	inline void set_maxPosition_4(float value)
	{
		___maxPosition_4 = value;
	}

	inline static int32_t get_offset_of_maxRadians_5() { return static_cast<int32_t>(offsetof(GForce_t1402080092, ___maxRadians_5)); }
	inline float get_maxRadians_5() const { return ___maxRadians_5; }
	inline float* get_address_of_maxRadians_5() { return &___maxRadians_5; }
	inline void set_maxRadians_5(float value)
	{
		___maxRadians_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GFORCE_T1402080092_H
#ifndef S3CONNECT_T2820114966_H
#define S3CONNECT_T2820114966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AWSSDK.Examples.S3Connect
struct  S3Connect_t2820114966  : public MonoBehaviour_t1158329972
{
public:
	// System.String AWSSDK.Examples.S3Connect::S3BucketName
	String_t* ___S3BucketName_2;
	// System.String AWSSDK.Examples.S3Connect::S3Key
	String_t* ___S3Key_3;
	// UnityEngine.GameObject AWSSDK.Examples.S3Connect::linkEntryPrefab
	GameObject_t1756533147 * ___linkEntryPrefab_4;
	// UnityEngine.GameObject AWSSDK.Examples.S3Connect::linkEntryParent
	GameObject_t1756533147 * ___linkEntryParent_5;
	// Amazon.S3.IAmazonS3 AWSSDK.Examples.S3Connect::_s3Client
	RuntimeObject* ____s3Client_6;

public:
	inline static int32_t get_offset_of_S3BucketName_2() { return static_cast<int32_t>(offsetof(S3Connect_t2820114966, ___S3BucketName_2)); }
	inline String_t* get_S3BucketName_2() const { return ___S3BucketName_2; }
	inline String_t** get_address_of_S3BucketName_2() { return &___S3BucketName_2; }
	inline void set_S3BucketName_2(String_t* value)
	{
		___S3BucketName_2 = value;
		Il2CppCodeGenWriteBarrier((&___S3BucketName_2), value);
	}

	inline static int32_t get_offset_of_S3Key_3() { return static_cast<int32_t>(offsetof(S3Connect_t2820114966, ___S3Key_3)); }
	inline String_t* get_S3Key_3() const { return ___S3Key_3; }
	inline String_t** get_address_of_S3Key_3() { return &___S3Key_3; }
	inline void set_S3Key_3(String_t* value)
	{
		___S3Key_3 = value;
		Il2CppCodeGenWriteBarrier((&___S3Key_3), value);
	}

	inline static int32_t get_offset_of_linkEntryPrefab_4() { return static_cast<int32_t>(offsetof(S3Connect_t2820114966, ___linkEntryPrefab_4)); }
	inline GameObject_t1756533147 * get_linkEntryPrefab_4() const { return ___linkEntryPrefab_4; }
	inline GameObject_t1756533147 ** get_address_of_linkEntryPrefab_4() { return &___linkEntryPrefab_4; }
	inline void set_linkEntryPrefab_4(GameObject_t1756533147 * value)
	{
		___linkEntryPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___linkEntryPrefab_4), value);
	}

	inline static int32_t get_offset_of_linkEntryParent_5() { return static_cast<int32_t>(offsetof(S3Connect_t2820114966, ___linkEntryParent_5)); }
	inline GameObject_t1756533147 * get_linkEntryParent_5() const { return ___linkEntryParent_5; }
	inline GameObject_t1756533147 ** get_address_of_linkEntryParent_5() { return &___linkEntryParent_5; }
	inline void set_linkEntryParent_5(GameObject_t1756533147 * value)
	{
		___linkEntryParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkEntryParent_5), value);
	}

	inline static int32_t get_offset_of__s3Client_6() { return static_cast<int32_t>(offsetof(S3Connect_t2820114966, ____s3Client_6)); }
	inline RuntimeObject* get__s3Client_6() const { return ____s3Client_6; }
	inline RuntimeObject** get_address_of__s3Client_6() { return &____s3Client_6; }
	inline void set__s3Client_6(RuntimeObject* value)
	{
		____s3Client_6 = value;
		Il2CppCodeGenWriteBarrier((&____s3Client_6), value);
	}
};

struct S3Connect_t2820114966_StaticFields
{
public:
	// Amazon.Runtime.AmazonServiceCallback`2<Amazon.S3.Model.GetObjectRequest,Amazon.S3.Model.GetObjectResponse> AWSSDK.Examples.S3Connect::<>f__am$cache0
	AmazonServiceCallback_2_t3959979905 * ___U3CU3Ef__amU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_7() { return static_cast<int32_t>(offsetof(S3Connect_t2820114966_StaticFields, ___U3CU3Ef__amU24cache0_7)); }
	inline AmazonServiceCallback_2_t3959979905 * get_U3CU3Ef__amU24cache0_7() const { return ___U3CU3Ef__amU24cache0_7; }
	inline AmazonServiceCallback_2_t3959979905 ** get_address_of_U3CU3Ef__amU24cache0_7() { return &___U3CU3Ef__amU24cache0_7; }
	inline void set_U3CU3Ef__amU24cache0_7(AmazonServiceCallback_2_t3959979905 * value)
	{
		___U3CU3Ef__amU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3CONNECT_T2820114966_H
#ifndef SCROLLER_T2879750794_H
#define SCROLLER_T2879750794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Scroller
struct  Scroller_t2879750794  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Scroller::scrollAmount
	float ___scrollAmount_2;
	// System.Single Scroller::minAmount
	float ___minAmount_3;
	// System.Single Scroller::maxAmount
	float ___maxAmount_4;
	// UnityEngine.UI.Button Scroller::upButton
	Button_t2872111280 * ___upButton_5;
	// UnityEngine.UI.Button Scroller::downButton
	Button_t2872111280 * ___downButton_6;

public:
	inline static int32_t get_offset_of_scrollAmount_2() { return static_cast<int32_t>(offsetof(Scroller_t2879750794, ___scrollAmount_2)); }
	inline float get_scrollAmount_2() const { return ___scrollAmount_2; }
	inline float* get_address_of_scrollAmount_2() { return &___scrollAmount_2; }
	inline void set_scrollAmount_2(float value)
	{
		___scrollAmount_2 = value;
	}

	inline static int32_t get_offset_of_minAmount_3() { return static_cast<int32_t>(offsetof(Scroller_t2879750794, ___minAmount_3)); }
	inline float get_minAmount_3() const { return ___minAmount_3; }
	inline float* get_address_of_minAmount_3() { return &___minAmount_3; }
	inline void set_minAmount_3(float value)
	{
		___minAmount_3 = value;
	}

	inline static int32_t get_offset_of_maxAmount_4() { return static_cast<int32_t>(offsetof(Scroller_t2879750794, ___maxAmount_4)); }
	inline float get_maxAmount_4() const { return ___maxAmount_4; }
	inline float* get_address_of_maxAmount_4() { return &___maxAmount_4; }
	inline void set_maxAmount_4(float value)
	{
		___maxAmount_4 = value;
	}

	inline static int32_t get_offset_of_upButton_5() { return static_cast<int32_t>(offsetof(Scroller_t2879750794, ___upButton_5)); }
	inline Button_t2872111280 * get_upButton_5() const { return ___upButton_5; }
	inline Button_t2872111280 ** get_address_of_upButton_5() { return &___upButton_5; }
	inline void set_upButton_5(Button_t2872111280 * value)
	{
		___upButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___upButton_5), value);
	}

	inline static int32_t get_offset_of_downButton_6() { return static_cast<int32_t>(offsetof(Scroller_t2879750794, ___downButton_6)); }
	inline Button_t2872111280 * get_downButton_6() const { return ___downButton_6; }
	inline Button_t2872111280 ** get_address_of_downButton_6() { return &___downButton_6; }
	inline void set_downButton_6(Button_t2872111280 * value)
	{
		___downButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___downButton_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLER_T2879750794_H
#ifndef RACINGWIDGETS_T915614521_H
#define RACINGWIDGETS_T915614521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RacingWidgets
struct  RacingWidgets_t915614521  : public MonoBehaviour_t1158329972
{
public:
	// System.Single RacingWidgets::timeMonospacing
	float ___timeMonospacing_3;
	// System.Single RacingWidgets::speedMonospacing
	float ___speedMonospacing_4;
	// System.Single RacingWidgets::rpmMonospacing
	float ___rpmMonospacing_5;
	// TMPro.TextMeshProUGUI RacingWidgets::timeText
	TextMeshProUGUI_t934157183 * ___timeText_6;
	// TMPro.TextMeshProUGUI RacingWidgets::speedText
	TextMeshProUGUI_t934157183 * ___speedText_7;
	// TMPro.TextMeshProUGUI RacingWidgets::rpmText
	TextMeshProUGUI_t934157183 * ___rpmText_8;
	// UnityEngine.Animator RacingWidgets::rpmAnimator
	Animator_t69676727 * ___rpmAnimator_9;
	// UnityEngine.Animator RacingWidgets::speedAnimator
	Animator_t69676727 * ___speedAnimator_10;
	// GForce RacingWidgets::gForceDot
	GForce_t1402080092 * ___gForceDot_11;

public:
	inline static int32_t get_offset_of_timeMonospacing_3() { return static_cast<int32_t>(offsetof(RacingWidgets_t915614521, ___timeMonospacing_3)); }
	inline float get_timeMonospacing_3() const { return ___timeMonospacing_3; }
	inline float* get_address_of_timeMonospacing_3() { return &___timeMonospacing_3; }
	inline void set_timeMonospacing_3(float value)
	{
		___timeMonospacing_3 = value;
	}

	inline static int32_t get_offset_of_speedMonospacing_4() { return static_cast<int32_t>(offsetof(RacingWidgets_t915614521, ___speedMonospacing_4)); }
	inline float get_speedMonospacing_4() const { return ___speedMonospacing_4; }
	inline float* get_address_of_speedMonospacing_4() { return &___speedMonospacing_4; }
	inline void set_speedMonospacing_4(float value)
	{
		___speedMonospacing_4 = value;
	}

	inline static int32_t get_offset_of_rpmMonospacing_5() { return static_cast<int32_t>(offsetof(RacingWidgets_t915614521, ___rpmMonospacing_5)); }
	inline float get_rpmMonospacing_5() const { return ___rpmMonospacing_5; }
	inline float* get_address_of_rpmMonospacing_5() { return &___rpmMonospacing_5; }
	inline void set_rpmMonospacing_5(float value)
	{
		___rpmMonospacing_5 = value;
	}

	inline static int32_t get_offset_of_timeText_6() { return static_cast<int32_t>(offsetof(RacingWidgets_t915614521, ___timeText_6)); }
	inline TextMeshProUGUI_t934157183 * get_timeText_6() const { return ___timeText_6; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_timeText_6() { return &___timeText_6; }
	inline void set_timeText_6(TextMeshProUGUI_t934157183 * value)
	{
		___timeText_6 = value;
		Il2CppCodeGenWriteBarrier((&___timeText_6), value);
	}

	inline static int32_t get_offset_of_speedText_7() { return static_cast<int32_t>(offsetof(RacingWidgets_t915614521, ___speedText_7)); }
	inline TextMeshProUGUI_t934157183 * get_speedText_7() const { return ___speedText_7; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_speedText_7() { return &___speedText_7; }
	inline void set_speedText_7(TextMeshProUGUI_t934157183 * value)
	{
		___speedText_7 = value;
		Il2CppCodeGenWriteBarrier((&___speedText_7), value);
	}

	inline static int32_t get_offset_of_rpmText_8() { return static_cast<int32_t>(offsetof(RacingWidgets_t915614521, ___rpmText_8)); }
	inline TextMeshProUGUI_t934157183 * get_rpmText_8() const { return ___rpmText_8; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_rpmText_8() { return &___rpmText_8; }
	inline void set_rpmText_8(TextMeshProUGUI_t934157183 * value)
	{
		___rpmText_8 = value;
		Il2CppCodeGenWriteBarrier((&___rpmText_8), value);
	}

	inline static int32_t get_offset_of_rpmAnimator_9() { return static_cast<int32_t>(offsetof(RacingWidgets_t915614521, ___rpmAnimator_9)); }
	inline Animator_t69676727 * get_rpmAnimator_9() const { return ___rpmAnimator_9; }
	inline Animator_t69676727 ** get_address_of_rpmAnimator_9() { return &___rpmAnimator_9; }
	inline void set_rpmAnimator_9(Animator_t69676727 * value)
	{
		___rpmAnimator_9 = value;
		Il2CppCodeGenWriteBarrier((&___rpmAnimator_9), value);
	}

	inline static int32_t get_offset_of_speedAnimator_10() { return static_cast<int32_t>(offsetof(RacingWidgets_t915614521, ___speedAnimator_10)); }
	inline Animator_t69676727 * get_speedAnimator_10() const { return ___speedAnimator_10; }
	inline Animator_t69676727 ** get_address_of_speedAnimator_10() { return &___speedAnimator_10; }
	inline void set_speedAnimator_10(Animator_t69676727 * value)
	{
		___speedAnimator_10 = value;
		Il2CppCodeGenWriteBarrier((&___speedAnimator_10), value);
	}

	inline static int32_t get_offset_of_gForceDot_11() { return static_cast<int32_t>(offsetof(RacingWidgets_t915614521, ___gForceDot_11)); }
	inline GForce_t1402080092 * get_gForceDot_11() const { return ___gForceDot_11; }
	inline GForce_t1402080092 ** get_address_of_gForceDot_11() { return &___gForceDot_11; }
	inline void set_gForceDot_11(GForce_t1402080092 * value)
	{
		___gForceDot_11 = value;
		Il2CppCodeGenWriteBarrier((&___gForceDot_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RACINGWIDGETS_T915614521_H
#ifndef MAPCREATOR_T2101643938_H
#define MAPCREATOR_T2101643938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapCreator
struct  MapCreator_t2101643938  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera MapCreator::mapCamera
	Camera_t189460977 * ___mapCamera_2;
	// UnityEngine.GameObject MapCreator::roadObject
	GameObject_t1756533147 * ___roadObject_3;
	// UnityEngine.GameObject MapCreator::mapBlip
	GameObject_t1756533147 * ___mapBlip_4;
	// System.Single MapCreator::cameraSize
	float ___cameraSize_5;
	// System.Single MapCreator::trackLoopThreshold
	float ___trackLoopThreshold_6;
	// PathCreator MapCreator::pathCreator
	PathCreator_t1368626459 * ___pathCreator_7;
	// BezierPath MapCreator::path
	BezierPath_t978602816 * ___path_8;
	// RoadCreator MapCreator::roadCreator
	RoadCreator_t1201856826 * ___roadCreator_9;
	// MapCreator/MinMax MapCreator::coordMinMax
	MinMax_t2491720607  ___coordMinMax_10;

public:
	inline static int32_t get_offset_of_mapCamera_2() { return static_cast<int32_t>(offsetof(MapCreator_t2101643938, ___mapCamera_2)); }
	inline Camera_t189460977 * get_mapCamera_2() const { return ___mapCamera_2; }
	inline Camera_t189460977 ** get_address_of_mapCamera_2() { return &___mapCamera_2; }
	inline void set_mapCamera_2(Camera_t189460977 * value)
	{
		___mapCamera_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapCamera_2), value);
	}

	inline static int32_t get_offset_of_roadObject_3() { return static_cast<int32_t>(offsetof(MapCreator_t2101643938, ___roadObject_3)); }
	inline GameObject_t1756533147 * get_roadObject_3() const { return ___roadObject_3; }
	inline GameObject_t1756533147 ** get_address_of_roadObject_3() { return &___roadObject_3; }
	inline void set_roadObject_3(GameObject_t1756533147 * value)
	{
		___roadObject_3 = value;
		Il2CppCodeGenWriteBarrier((&___roadObject_3), value);
	}

	inline static int32_t get_offset_of_mapBlip_4() { return static_cast<int32_t>(offsetof(MapCreator_t2101643938, ___mapBlip_4)); }
	inline GameObject_t1756533147 * get_mapBlip_4() const { return ___mapBlip_4; }
	inline GameObject_t1756533147 ** get_address_of_mapBlip_4() { return &___mapBlip_4; }
	inline void set_mapBlip_4(GameObject_t1756533147 * value)
	{
		___mapBlip_4 = value;
		Il2CppCodeGenWriteBarrier((&___mapBlip_4), value);
	}

	inline static int32_t get_offset_of_cameraSize_5() { return static_cast<int32_t>(offsetof(MapCreator_t2101643938, ___cameraSize_5)); }
	inline float get_cameraSize_5() const { return ___cameraSize_5; }
	inline float* get_address_of_cameraSize_5() { return &___cameraSize_5; }
	inline void set_cameraSize_5(float value)
	{
		___cameraSize_5 = value;
	}

	inline static int32_t get_offset_of_trackLoopThreshold_6() { return static_cast<int32_t>(offsetof(MapCreator_t2101643938, ___trackLoopThreshold_6)); }
	inline float get_trackLoopThreshold_6() const { return ___trackLoopThreshold_6; }
	inline float* get_address_of_trackLoopThreshold_6() { return &___trackLoopThreshold_6; }
	inline void set_trackLoopThreshold_6(float value)
	{
		___trackLoopThreshold_6 = value;
	}

	inline static int32_t get_offset_of_pathCreator_7() { return static_cast<int32_t>(offsetof(MapCreator_t2101643938, ___pathCreator_7)); }
	inline PathCreator_t1368626459 * get_pathCreator_7() const { return ___pathCreator_7; }
	inline PathCreator_t1368626459 ** get_address_of_pathCreator_7() { return &___pathCreator_7; }
	inline void set_pathCreator_7(PathCreator_t1368626459 * value)
	{
		___pathCreator_7 = value;
		Il2CppCodeGenWriteBarrier((&___pathCreator_7), value);
	}

	inline static int32_t get_offset_of_path_8() { return static_cast<int32_t>(offsetof(MapCreator_t2101643938, ___path_8)); }
	inline BezierPath_t978602816 * get_path_8() const { return ___path_8; }
	inline BezierPath_t978602816 ** get_address_of_path_8() { return &___path_8; }
	inline void set_path_8(BezierPath_t978602816 * value)
	{
		___path_8 = value;
		Il2CppCodeGenWriteBarrier((&___path_8), value);
	}

	inline static int32_t get_offset_of_roadCreator_9() { return static_cast<int32_t>(offsetof(MapCreator_t2101643938, ___roadCreator_9)); }
	inline RoadCreator_t1201856826 * get_roadCreator_9() const { return ___roadCreator_9; }
	inline RoadCreator_t1201856826 ** get_address_of_roadCreator_9() { return &___roadCreator_9; }
	inline void set_roadCreator_9(RoadCreator_t1201856826 * value)
	{
		___roadCreator_9 = value;
		Il2CppCodeGenWriteBarrier((&___roadCreator_9), value);
	}

	inline static int32_t get_offset_of_coordMinMax_10() { return static_cast<int32_t>(offsetof(MapCreator_t2101643938, ___coordMinMax_10)); }
	inline MinMax_t2491720607  get_coordMinMax_10() const { return ___coordMinMax_10; }
	inline MinMax_t2491720607 * get_address_of_coordMinMax_10() { return &___coordMinMax_10; }
	inline void set_coordMinMax_10(MinMax_t2491720607  value)
	{
		___coordMinMax_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPCREATOR_T2101643938_H
#ifndef PLAYBUTTON_T1602700306_H
#define PLAYBUTTON_T1602700306_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayButton
struct  PlayButton_t1602700306  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button PlayButton::playButton
	Button_t2872111280 * ___playButton_2;
	// UnityEngine.GameObject PlayButton::canvasToHide
	GameObject_t1756533147 * ___canvasToHide_3;
	// UnityEngine.UI.Toggle PlayButton::demoCSV
	Toggle_t3976754468 * ___demoCSV_4;
	// TestScript PlayButton::testScript
	TestScript_t905662927 * ___testScript_5;

public:
	inline static int32_t get_offset_of_playButton_2() { return static_cast<int32_t>(offsetof(PlayButton_t1602700306, ___playButton_2)); }
	inline Button_t2872111280 * get_playButton_2() const { return ___playButton_2; }
	inline Button_t2872111280 ** get_address_of_playButton_2() { return &___playButton_2; }
	inline void set_playButton_2(Button_t2872111280 * value)
	{
		___playButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___playButton_2), value);
	}

	inline static int32_t get_offset_of_canvasToHide_3() { return static_cast<int32_t>(offsetof(PlayButton_t1602700306, ___canvasToHide_3)); }
	inline GameObject_t1756533147 * get_canvasToHide_3() const { return ___canvasToHide_3; }
	inline GameObject_t1756533147 ** get_address_of_canvasToHide_3() { return &___canvasToHide_3; }
	inline void set_canvasToHide_3(GameObject_t1756533147 * value)
	{
		___canvasToHide_3 = value;
		Il2CppCodeGenWriteBarrier((&___canvasToHide_3), value);
	}

	inline static int32_t get_offset_of_demoCSV_4() { return static_cast<int32_t>(offsetof(PlayButton_t1602700306, ___demoCSV_4)); }
	inline Toggle_t3976754468 * get_demoCSV_4() const { return ___demoCSV_4; }
	inline Toggle_t3976754468 ** get_address_of_demoCSV_4() { return &___demoCSV_4; }
	inline void set_demoCSV_4(Toggle_t3976754468 * value)
	{
		___demoCSV_4 = value;
		Il2CppCodeGenWriteBarrier((&___demoCSV_4), value);
	}

	inline static int32_t get_offset_of_testScript_5() { return static_cast<int32_t>(offsetof(PlayButton_t1602700306, ___testScript_5)); }
	inline TestScript_t905662927 * get_testScript_5() const { return ___testScript_5; }
	inline TestScript_t905662927 ** get_address_of_testScript_5() { return &___testScript_5; }
	inline void set_testScript_5(TestScript_t905662927 * value)
	{
		___testScript_5 = value;
		Il2CppCodeGenWriteBarrier((&___testScript_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYBUTTON_T1602700306_H
#ifndef SHADERPROPANIMATOR_T2679013775_H
#define SHADERPROPANIMATOR_T2679013775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator
struct  ShaderPropAnimator_t2679013775  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t257310565 * ___m_Renderer_2;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_t193706927 * ___m_Material_3;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_t3306541151 * ___GlowCurve_4;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_5;

public:
	inline static int32_t get_offset_of_m_Renderer_2() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t2679013775, ___m_Renderer_2)); }
	inline Renderer_t257310565 * get_m_Renderer_2() const { return ___m_Renderer_2; }
	inline Renderer_t257310565 ** get_address_of_m_Renderer_2() { return &___m_Renderer_2; }
	inline void set_m_Renderer_2(Renderer_t257310565 * value)
	{
		___m_Renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t2679013775, ___m_Material_3)); }
	inline Material_t193706927 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t193706927 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t193706927 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}

	inline static int32_t get_offset_of_GlowCurve_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t2679013775, ___GlowCurve_4)); }
	inline AnimationCurve_t3306541151 * get_GlowCurve_4() const { return ___GlowCurve_4; }
	inline AnimationCurve_t3306541151 ** get_address_of_GlowCurve_4() { return &___GlowCurve_4; }
	inline void set_GlowCurve_4(AnimationCurve_t3306541151 * value)
	{
		___GlowCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___GlowCurve_4), value);
	}

	inline static int32_t get_offset_of_m_frame_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t2679013775, ___m_frame_5)); }
	inline float get_m_frame_5() const { return ___m_frame_5; }
	inline float* get_address_of_m_frame_5() { return &___m_frame_5; }
	inline void set_m_frame_5(float value)
	{
		___m_frame_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPANIMATOR_T2679013775_H
#ifndef TMPRO_INSTRUCTIONOVERLAY_T4177302973_H
#define TMPRO_INSTRUCTIONOVERLAY_T4177302973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMPro_InstructionOverlay
struct  TMPro_InstructionOverlay_t4177302973  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions TMPro.Examples.TMPro_InstructionOverlay::AnchorPosition
	int32_t ___AnchorPosition_2;
	// TMPro.TextMeshPro TMPro.Examples.TMPro_InstructionOverlay::m_TextMeshPro
	TextMeshPro_t2521834357 * ___m_TextMeshPro_4;
	// TMPro.TextContainer TMPro.Examples.TMPro_InstructionOverlay::m_textContainer
	TextContainer_t4263764796 * ___m_textContainer_5;
	// UnityEngine.Transform TMPro.Examples.TMPro_InstructionOverlay::m_frameCounter_transform
	Transform_t3275118058 * ___m_frameCounter_transform_6;
	// UnityEngine.Camera TMPro.Examples.TMPro_InstructionOverlay::m_camera
	Camera_t189460977 * ___m_camera_7;

public:
	inline static int32_t get_offset_of_AnchorPosition_2() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4177302973, ___AnchorPosition_2)); }
	inline int32_t get_AnchorPosition_2() const { return ___AnchorPosition_2; }
	inline int32_t* get_address_of_AnchorPosition_2() { return &___AnchorPosition_2; }
	inline void set_AnchorPosition_2(int32_t value)
	{
		___AnchorPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_TextMeshPro_4() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4177302973, ___m_TextMeshPro_4)); }
	inline TextMeshPro_t2521834357 * get_m_TextMeshPro_4() const { return ___m_TextMeshPro_4; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_TextMeshPro_4() { return &___m_TextMeshPro_4; }
	inline void set_m_TextMeshPro_4(TextMeshPro_t2521834357 * value)
	{
		___m_TextMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_textContainer_5() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4177302973, ___m_textContainer_5)); }
	inline TextContainer_t4263764796 * get_m_textContainer_5() const { return ___m_textContainer_5; }
	inline TextContainer_t4263764796 ** get_address_of_m_textContainer_5() { return &___m_textContainer_5; }
	inline void set_m_textContainer_5(TextContainer_t4263764796 * value)
	{
		___m_textContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_5), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_6() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4177302973, ___m_frameCounter_transform_6)); }
	inline Transform_t3275118058 * get_m_frameCounter_transform_6() const { return ___m_frameCounter_transform_6; }
	inline Transform_t3275118058 ** get_address_of_m_frameCounter_transform_6() { return &___m_frameCounter_transform_6; }
	inline void set_m_frameCounter_transform_6(Transform_t3275118058 * value)
	{
		___m_frameCounter_transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_6), value);
	}

	inline static int32_t get_offset_of_m_camera_7() { return static_cast<int32_t>(offsetof(TMPro_InstructionOverlay_t4177302973, ___m_camera_7)); }
	inline Camera_t189460977 * get_m_camera_7() const { return ___m_camera_7; }
	inline Camera_t189460977 ** get_address_of_m_camera_7() { return &___m_camera_7; }
	inline void set_m_camera_7(Camera_t189460977 * value)
	{
		___m_camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_INSTRUCTIONOVERLAY_T4177302973_H
#ifndef TMP_UIFRAMERATECOUNTER_T4016109979_H
#define TMP_UIFRAMERATECOUNTER_T4016109979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_UiFrameRateCounter
struct  TMP_UiFrameRateCounter_t4016109979  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::UpdateInterval
	float ___UpdateInterval_2;
	// System.Single TMPro.Examples.TMP_UiFrameRateCounter::m_LastInterval
	float ___m_LastInterval_3;
	// System.Int32 TMPro.Examples.TMP_UiFrameRateCounter::m_Frames
	int32_t ___m_Frames_4;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_5;
	// System.String TMPro.Examples.TMP_UiFrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_6;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_UiFrameRateCounter::m_TextMeshPro
	TextMeshProUGUI_t934157183 * ___m_TextMeshPro_8;
	// UnityEngine.RectTransform TMPro.Examples.TMP_UiFrameRateCounter::m_frameCounter_transform
	RectTransform_t3349966182 * ___m_frameCounter_transform_9;
	// TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_UiFrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_10;

public:
	inline static int32_t get_offset_of_UpdateInterval_2() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t4016109979, ___UpdateInterval_2)); }
	inline float get_UpdateInterval_2() const { return ___UpdateInterval_2; }
	inline float* get_address_of_UpdateInterval_2() { return &___UpdateInterval_2; }
	inline void set_UpdateInterval_2(float value)
	{
		___UpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_3() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t4016109979, ___m_LastInterval_3)); }
	inline float get_m_LastInterval_3() const { return ___m_LastInterval_3; }
	inline float* get_address_of_m_LastInterval_3() { return &___m_LastInterval_3; }
	inline void set_m_LastInterval_3(float value)
	{
		___m_LastInterval_3 = value;
	}

	inline static int32_t get_offset_of_m_Frames_4() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t4016109979, ___m_Frames_4)); }
	inline int32_t get_m_Frames_4() const { return ___m_Frames_4; }
	inline int32_t* get_address_of_m_Frames_4() { return &___m_Frames_4; }
	inline void set_m_Frames_4(int32_t value)
	{
		___m_Frames_4 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_5() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t4016109979, ___AnchorPosition_5)); }
	inline int32_t get_AnchorPosition_5() const { return ___AnchorPosition_5; }
	inline int32_t* get_address_of_AnchorPosition_5() { return &___AnchorPosition_5; }
	inline void set_AnchorPosition_5(int32_t value)
	{
		___AnchorPosition_5 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_6() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t4016109979, ___htmlColorTag_6)); }
	inline String_t* get_htmlColorTag_6() const { return ___htmlColorTag_6; }
	inline String_t** get_address_of_htmlColorTag_6() { return &___htmlColorTag_6; }
	inline void set_htmlColorTag_6(String_t* value)
	{
		___htmlColorTag_6 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_6), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_8() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t4016109979, ___m_TextMeshPro_8)); }
	inline TextMeshProUGUI_t934157183 * get_m_TextMeshPro_8() const { return ___m_TextMeshPro_8; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_m_TextMeshPro_8() { return &___m_TextMeshPro_8; }
	inline void set_m_TextMeshPro_8(TextMeshProUGUI_t934157183 * value)
	{
		___m_TextMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_9() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t4016109979, ___m_frameCounter_transform_9)); }
	inline RectTransform_t3349966182 * get_m_frameCounter_transform_9() const { return ___m_frameCounter_transform_9; }
	inline RectTransform_t3349966182 ** get_address_of_m_frameCounter_transform_9() { return &___m_frameCounter_transform_9; }
	inline void set_m_frameCounter_transform_9(RectTransform_t3349966182 * value)
	{
		___m_frameCounter_transform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_9), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_10() { return static_cast<int32_t>(offsetof(TMP_UiFrameRateCounter_t4016109979, ___last_AnchorPosition_10)); }
	inline int32_t get_last_AnchorPosition_10() const { return ___last_AnchorPosition_10; }
	inline int32_t* get_address_of_last_AnchorPosition_10() { return &___last_AnchorPosition_10; }
	inline void set_last_AnchorPosition_10(int32_t value)
	{
		___last_AnchorPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UIFRAMERATECOUNTER_T4016109979_H
#ifndef TMP_FRAMERATECOUNTER_T2302739001_H
#define TMP_FRAMERATECOUNTER_T2302739001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_FrameRateCounter
struct  TMP_FrameRateCounter_t2302739001  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TMPro.Examples.TMP_FrameRateCounter::UpdateInterval
	float ___UpdateInterval_2;
	// System.Single TMPro.Examples.TMP_FrameRateCounter::m_LastInterval
	float ___m_LastInterval_3;
	// System.Int32 TMPro.Examples.TMP_FrameRateCounter::m_Frames
	int32_t ___m_Frames_4;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::AnchorPosition
	int32_t ___AnchorPosition_5;
	// System.String TMPro.Examples.TMP_FrameRateCounter::htmlColorTag
	String_t* ___htmlColorTag_6;
	// TMPro.TextMeshPro TMPro.Examples.TMP_FrameRateCounter::m_TextMeshPro
	TextMeshPro_t2521834357 * ___m_TextMeshPro_8;
	// UnityEngine.Transform TMPro.Examples.TMP_FrameRateCounter::m_frameCounter_transform
	Transform_t3275118058 * ___m_frameCounter_transform_9;
	// UnityEngine.Camera TMPro.Examples.TMP_FrameRateCounter::m_camera
	Camera_t189460977 * ___m_camera_10;
	// TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions TMPro.Examples.TMP_FrameRateCounter::last_AnchorPosition
	int32_t ___last_AnchorPosition_11;

public:
	inline static int32_t get_offset_of_UpdateInterval_2() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t2302739001, ___UpdateInterval_2)); }
	inline float get_UpdateInterval_2() const { return ___UpdateInterval_2; }
	inline float* get_address_of_UpdateInterval_2() { return &___UpdateInterval_2; }
	inline void set_UpdateInterval_2(float value)
	{
		___UpdateInterval_2 = value;
	}

	inline static int32_t get_offset_of_m_LastInterval_3() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t2302739001, ___m_LastInterval_3)); }
	inline float get_m_LastInterval_3() const { return ___m_LastInterval_3; }
	inline float* get_address_of_m_LastInterval_3() { return &___m_LastInterval_3; }
	inline void set_m_LastInterval_3(float value)
	{
		___m_LastInterval_3 = value;
	}

	inline static int32_t get_offset_of_m_Frames_4() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t2302739001, ___m_Frames_4)); }
	inline int32_t get_m_Frames_4() const { return ___m_Frames_4; }
	inline int32_t* get_address_of_m_Frames_4() { return &___m_Frames_4; }
	inline void set_m_Frames_4(int32_t value)
	{
		___m_Frames_4 = value;
	}

	inline static int32_t get_offset_of_AnchorPosition_5() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t2302739001, ___AnchorPosition_5)); }
	inline int32_t get_AnchorPosition_5() const { return ___AnchorPosition_5; }
	inline int32_t* get_address_of_AnchorPosition_5() { return &___AnchorPosition_5; }
	inline void set_AnchorPosition_5(int32_t value)
	{
		___AnchorPosition_5 = value;
	}

	inline static int32_t get_offset_of_htmlColorTag_6() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t2302739001, ___htmlColorTag_6)); }
	inline String_t* get_htmlColorTag_6() const { return ___htmlColorTag_6; }
	inline String_t** get_address_of_htmlColorTag_6() { return &___htmlColorTag_6; }
	inline void set_htmlColorTag_6(String_t* value)
	{
		___htmlColorTag_6 = value;
		Il2CppCodeGenWriteBarrier((&___htmlColorTag_6), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_8() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t2302739001, ___m_TextMeshPro_8)); }
	inline TextMeshPro_t2521834357 * get_m_TextMeshPro_8() const { return ___m_TextMeshPro_8; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_TextMeshPro_8() { return &___m_TextMeshPro_8; }
	inline void set_m_TextMeshPro_8(TextMeshPro_t2521834357 * value)
	{
		___m_TextMeshPro_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_8), value);
	}

	inline static int32_t get_offset_of_m_frameCounter_transform_9() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t2302739001, ___m_frameCounter_transform_9)); }
	inline Transform_t3275118058 * get_m_frameCounter_transform_9() const { return ___m_frameCounter_transform_9; }
	inline Transform_t3275118058 ** get_address_of_m_frameCounter_transform_9() { return &___m_frameCounter_transform_9; }
	inline void set_m_frameCounter_transform_9(Transform_t3275118058 * value)
	{
		___m_frameCounter_transform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_frameCounter_transform_9), value);
	}

	inline static int32_t get_offset_of_m_camera_10() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t2302739001, ___m_camera_10)); }
	inline Camera_t189460977 * get_m_camera_10() const { return ___m_camera_10; }
	inline Camera_t189460977 ** get_address_of_m_camera_10() { return &___m_camera_10; }
	inline void set_m_camera_10(Camera_t189460977 * value)
	{
		___m_camera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_camera_10), value);
	}

	inline static int32_t get_offset_of_last_AnchorPosition_11() { return static_cast<int32_t>(offsetof(TMP_FrameRateCounter_t2302739001, ___last_AnchorPosition_11)); }
	inline int32_t get_last_AnchorPosition_11() const { return ___last_AnchorPosition_11; }
	inline int32_t* get_address_of_last_AnchorPosition_11() { return &___last_AnchorPosition_11; }
	inline void set_last_AnchorPosition_11(int32_t value)
	{
		___last_AnchorPosition_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FRAMERATECOUNTER_T2302739001_H
#ifndef TMP_EXAMPLESCRIPT_01_T2316744565_H
#define TMP_EXAMPLESCRIPT_01_T2316744565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_ExampleScript_01
struct  TMP_ExampleScript_01_t2316744565  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.Examples.TMP_ExampleScript_01/objectType TMPro.Examples.TMP_ExampleScript_01::ObjectType
	int32_t ___ObjectType_2;
	// System.Boolean TMPro.Examples.TMP_ExampleScript_01::isStatic
	bool ___isStatic_3;
	// TMPro.TMP_Text TMPro.Examples.TMP_ExampleScript_01::m_text
	TMP_Text_t1920000777 * ___m_text_4;
	// System.Int32 TMPro.Examples.TMP_ExampleScript_01::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_ObjectType_2() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t2316744565, ___ObjectType_2)); }
	inline int32_t get_ObjectType_2() const { return ___ObjectType_2; }
	inline int32_t* get_address_of_ObjectType_2() { return &___ObjectType_2; }
	inline void set_ObjectType_2(int32_t value)
	{
		___ObjectType_2 = value;
	}

	inline static int32_t get_offset_of_isStatic_3() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t2316744565, ___isStatic_3)); }
	inline bool get_isStatic_3() const { return ___isStatic_3; }
	inline bool* get_address_of_isStatic_3() { return &___isStatic_3; }
	inline void set_isStatic_3(bool value)
	{
		___isStatic_3 = value;
	}

	inline static int32_t get_offset_of_m_text_4() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t2316744565, ___m_text_4)); }
	inline TMP_Text_t1920000777 * get_m_text_4() const { return ___m_text_4; }
	inline TMP_Text_t1920000777 ** get_address_of_m_text_4() { return &___m_text_4; }
	inline void set_m_text_4(TMP_Text_t1920000777 * value)
	{
		___m_text_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_4), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(TMP_ExampleScript_01_t2316744565, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_EXAMPLESCRIPT_01_T2316744565_H
#ifndef VERTEXCOLORCYCLER_T3471086701_H
#define VERTEXCOLORCYCLER_T3471086701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexColorCycler
struct  VertexColorCycler_t3471086701  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_Text TMPro.Examples.VertexColorCycler::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_2;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(VertexColorCycler_t3471086701, ___m_TextComponent_2)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXCOLORCYCLER_T3471086701_H
#ifndef TMP_TEXTEVENTCHECK_T3099661323_H
#define TMP_TEXTEVENTCHECK_T3099661323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextEventCheck
struct  TMP_TextEventCheck_t3099661323  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_TextEventHandler TMPro.Examples.TMP_TextEventCheck::TextEventHandler
	TMP_TextEventHandler_t1517844021 * ___TextEventHandler_2;

public:
	inline static int32_t get_offset_of_TextEventHandler_2() { return static_cast<int32_t>(offsetof(TMP_TextEventCheck_t3099661323, ___TextEventHandler_2)); }
	inline TMP_TextEventHandler_t1517844021 * get_TextEventHandler_2() const { return ___TextEventHandler_2; }
	inline TMP_TextEventHandler_t1517844021 ** get_address_of_TextEventHandler_2() { return &___TextEventHandler_2; }
	inline void set_TextEventHandler_2(TMP_TextEventHandler_t1517844021 * value)
	{
		___TextEventHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___TextEventHandler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTCHECK_T3099661323_H
#ifndef TMP_TEXTEVENTHANDLER_T1517844021_H
#define TMP_TEXTEVENTHANDLER_T1517844021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextEventHandler
struct  TMP_TextEventHandler_t1517844021  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::m_OnCharacterSelection
	CharacterSelectionEvent_t2887241789 * ___m_OnCharacterSelection_2;
	// TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::m_OnWordSelection
	WordSelectionEvent_t2871480376 * ___m_OnWordSelection_3;
	// TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::m_OnLineSelection
	LineSelectionEvent_t3265414486 * ___m_OnLineSelection_4;
	// TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::m_OnLinkSelection
	LinkSelectionEvent_t3735310088 * ___m_OnLinkSelection_5;
	// TMPro.TMP_Text TMPro.TMP_TextEventHandler::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_6;
	// UnityEngine.Camera TMPro.TMP_TextEventHandler::m_Camera
	Camera_t189460977 * ___m_Camera_7;
	// UnityEngine.Canvas TMPro.TMP_TextEventHandler::m_Canvas
	Canvas_t209405766 * ___m_Canvas_8;
	// System.Int32 TMPro.TMP_TextEventHandler::m_selectedLink
	int32_t ___m_selectedLink_9;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastCharIndex
	int32_t ___m_lastCharIndex_10;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastWordIndex
	int32_t ___m_lastWordIndex_11;
	// System.Int32 TMPro.TMP_TextEventHandler::m_lastLineIndex
	int32_t ___m_lastLineIndex_12;

public:
	inline static int32_t get_offset_of_m_OnCharacterSelection_2() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_OnCharacterSelection_2)); }
	inline CharacterSelectionEvent_t2887241789 * get_m_OnCharacterSelection_2() const { return ___m_OnCharacterSelection_2; }
	inline CharacterSelectionEvent_t2887241789 ** get_address_of_m_OnCharacterSelection_2() { return &___m_OnCharacterSelection_2; }
	inline void set_m_OnCharacterSelection_2(CharacterSelectionEvent_t2887241789 * value)
	{
		___m_OnCharacterSelection_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCharacterSelection_2), value);
	}

	inline static int32_t get_offset_of_m_OnWordSelection_3() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_OnWordSelection_3)); }
	inline WordSelectionEvent_t2871480376 * get_m_OnWordSelection_3() const { return ___m_OnWordSelection_3; }
	inline WordSelectionEvent_t2871480376 ** get_address_of_m_OnWordSelection_3() { return &___m_OnWordSelection_3; }
	inline void set_m_OnWordSelection_3(WordSelectionEvent_t2871480376 * value)
	{
		___m_OnWordSelection_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnWordSelection_3), value);
	}

	inline static int32_t get_offset_of_m_OnLineSelection_4() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_OnLineSelection_4)); }
	inline LineSelectionEvent_t3265414486 * get_m_OnLineSelection_4() const { return ___m_OnLineSelection_4; }
	inline LineSelectionEvent_t3265414486 ** get_address_of_m_OnLineSelection_4() { return &___m_OnLineSelection_4; }
	inline void set_m_OnLineSelection_4(LineSelectionEvent_t3265414486 * value)
	{
		___m_OnLineSelection_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLineSelection_4), value);
	}

	inline static int32_t get_offset_of_m_OnLinkSelection_5() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_OnLinkSelection_5)); }
	inline LinkSelectionEvent_t3735310088 * get_m_OnLinkSelection_5() const { return ___m_OnLinkSelection_5; }
	inline LinkSelectionEvent_t3735310088 ** get_address_of_m_OnLinkSelection_5() { return &___m_OnLinkSelection_5; }
	inline void set_m_OnLinkSelection_5(LinkSelectionEvent_t3735310088 * value)
	{
		___m_OnLinkSelection_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnLinkSelection_5), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_6() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_TextComponent_6)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_6() const { return ___m_TextComponent_6; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_6() { return &___m_TextComponent_6; }
	inline void set_m_TextComponent_6(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_6), value);
	}

	inline static int32_t get_offset_of_m_Camera_7() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_Camera_7)); }
	inline Camera_t189460977 * get_m_Camera_7() const { return ___m_Camera_7; }
	inline Camera_t189460977 ** get_address_of_m_Camera_7() { return &___m_Camera_7; }
	inline void set_m_Camera_7(Camera_t189460977 * value)
	{
		___m_Camera_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_7), value);
	}

	inline static int32_t get_offset_of_m_Canvas_8() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_Canvas_8)); }
	inline Canvas_t209405766 * get_m_Canvas_8() const { return ___m_Canvas_8; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_8() { return &___m_Canvas_8; }
	inline void set_m_Canvas_8(Canvas_t209405766 * value)
	{
		___m_Canvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_8), value);
	}

	inline static int32_t get_offset_of_m_selectedLink_9() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_selectedLink_9)); }
	inline int32_t get_m_selectedLink_9() const { return ___m_selectedLink_9; }
	inline int32_t* get_address_of_m_selectedLink_9() { return &___m_selectedLink_9; }
	inline void set_m_selectedLink_9(int32_t value)
	{
		___m_selectedLink_9 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_10() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_lastCharIndex_10)); }
	inline int32_t get_m_lastCharIndex_10() const { return ___m_lastCharIndex_10; }
	inline int32_t* get_address_of_m_lastCharIndex_10() { return &___m_lastCharIndex_10; }
	inline void set_m_lastCharIndex_10(int32_t value)
	{
		___m_lastCharIndex_10 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_11() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_lastWordIndex_11)); }
	inline int32_t get_m_lastWordIndex_11() const { return ___m_lastWordIndex_11; }
	inline int32_t* get_address_of_m_lastWordIndex_11() { return &___m_lastWordIndex_11; }
	inline void set_m_lastWordIndex_11(int32_t value)
	{
		___m_lastWordIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_lastLineIndex_12() { return static_cast<int32_t>(offsetof(TMP_TextEventHandler_t1517844021, ___m_lastLineIndex_12)); }
	inline int32_t get_m_lastLineIndex_12() const { return ___m_lastLineIndex_12; }
	inline int32_t* get_address_of_m_lastLineIndex_12() { return &___m_lastLineIndex_12; }
	inline void set_m_lastLineIndex_12(int32_t value)
	{
		___m_lastLineIndex_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTEVENTHANDLER_T1517844021_H
#ifndef TMP_TEXTINFODEBUGTOOL_T548826358_H
#define TMP_TEXTINFODEBUGTOOL_T548826358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextInfoDebugTool
struct  TMP_TextInfoDebugTool_t548826358  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowCharacters
	bool ___ShowCharacters_2;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowWords
	bool ___ShowWords_3;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLinks
	bool ___ShowLinks_4;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowLines
	bool ___ShowLines_5;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowMeshBounds
	bool ___ShowMeshBounds_6;
	// System.Boolean TMPro.Examples.TMP_TextInfoDebugTool::ShowTextBounds
	bool ___ShowTextBounds_7;
	// System.String TMPro.Examples.TMP_TextInfoDebugTool::ObjectStats
	String_t* ___ObjectStats_8;
	// TMPro.TMP_Text TMPro.Examples.TMP_TextInfoDebugTool::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_9;
	// UnityEngine.Transform TMPro.Examples.TMP_TextInfoDebugTool::m_Transform
	Transform_t3275118058 * ___m_Transform_10;

public:
	inline static int32_t get_offset_of_ShowCharacters_2() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t548826358, ___ShowCharacters_2)); }
	inline bool get_ShowCharacters_2() const { return ___ShowCharacters_2; }
	inline bool* get_address_of_ShowCharacters_2() { return &___ShowCharacters_2; }
	inline void set_ShowCharacters_2(bool value)
	{
		___ShowCharacters_2 = value;
	}

	inline static int32_t get_offset_of_ShowWords_3() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t548826358, ___ShowWords_3)); }
	inline bool get_ShowWords_3() const { return ___ShowWords_3; }
	inline bool* get_address_of_ShowWords_3() { return &___ShowWords_3; }
	inline void set_ShowWords_3(bool value)
	{
		___ShowWords_3 = value;
	}

	inline static int32_t get_offset_of_ShowLinks_4() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t548826358, ___ShowLinks_4)); }
	inline bool get_ShowLinks_4() const { return ___ShowLinks_4; }
	inline bool* get_address_of_ShowLinks_4() { return &___ShowLinks_4; }
	inline void set_ShowLinks_4(bool value)
	{
		___ShowLinks_4 = value;
	}

	inline static int32_t get_offset_of_ShowLines_5() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t548826358, ___ShowLines_5)); }
	inline bool get_ShowLines_5() const { return ___ShowLines_5; }
	inline bool* get_address_of_ShowLines_5() { return &___ShowLines_5; }
	inline void set_ShowLines_5(bool value)
	{
		___ShowLines_5 = value;
	}

	inline static int32_t get_offset_of_ShowMeshBounds_6() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t548826358, ___ShowMeshBounds_6)); }
	inline bool get_ShowMeshBounds_6() const { return ___ShowMeshBounds_6; }
	inline bool* get_address_of_ShowMeshBounds_6() { return &___ShowMeshBounds_6; }
	inline void set_ShowMeshBounds_6(bool value)
	{
		___ShowMeshBounds_6 = value;
	}

	inline static int32_t get_offset_of_ShowTextBounds_7() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t548826358, ___ShowTextBounds_7)); }
	inline bool get_ShowTextBounds_7() const { return ___ShowTextBounds_7; }
	inline bool* get_address_of_ShowTextBounds_7() { return &___ShowTextBounds_7; }
	inline void set_ShowTextBounds_7(bool value)
	{
		___ShowTextBounds_7 = value;
	}

	inline static int32_t get_offset_of_ObjectStats_8() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t548826358, ___ObjectStats_8)); }
	inline String_t* get_ObjectStats_8() const { return ___ObjectStats_8; }
	inline String_t** get_address_of_ObjectStats_8() { return &___ObjectStats_8; }
	inline void set_ObjectStats_8(String_t* value)
	{
		___ObjectStats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectStats_8), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_9() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t548826358, ___m_TextComponent_9)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_9() const { return ___m_TextComponent_9; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_9() { return &___m_TextComponent_9; }
	inline void set_m_TextComponent_9(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_9), value);
	}

	inline static int32_t get_offset_of_m_Transform_10() { return static_cast<int32_t>(offsetof(TMP_TextInfoDebugTool_t548826358, ___m_Transform_10)); }
	inline Transform_t3275118058 * get_m_Transform_10() const { return ___m_Transform_10; }
	inline Transform_t3275118058 ** get_address_of_m_Transform_10() { return &___m_Transform_10; }
	inline void set_m_Transform_10(Transform_t3275118058 * value)
	{
		___m_Transform_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFODEBUGTOOL_T548826358_H
#ifndef TMP_TEXTSELECTOR_B_T2419327983_H
#define TMP_TEXTSELECTOR_B_T2419327983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_B
struct  TMP_TextSelector_B_t2419327983  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::TextPopup_Prefab_01
	RectTransform_t3349966182 * ___TextPopup_Prefab_01_2;
	// UnityEngine.RectTransform TMPro.Examples.TMP_TextSelector_B::m_TextPopup_RectTransform
	RectTransform_t3349966182 * ___m_TextPopup_RectTransform_3;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextPopup_TMPComponent
	TextMeshProUGUI_t934157183 * ___m_TextPopup_TMPComponent_4;
	// TMPro.TextMeshProUGUI TMPro.Examples.TMP_TextSelector_B::m_TextMeshPro
	TextMeshProUGUI_t934157183 * ___m_TextMeshPro_7;
	// UnityEngine.Canvas TMPro.Examples.TMP_TextSelector_B::m_Canvas
	Canvas_t209405766 * ___m_Canvas_8;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_B::m_Camera
	Camera_t189460977 * ___m_Camera_9;
	// System.Boolean TMPro.Examples.TMP_TextSelector_B::isHoveringObject
	bool ___isHoveringObject_10;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedWord
	int32_t ___m_selectedWord_11;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_selectedLink
	int32_t ___m_selectedLink_12;
	// System.Int32 TMPro.Examples.TMP_TextSelector_B::m_lastIndex
	int32_t ___m_lastIndex_13;
	// UnityEngine.Matrix4x4 TMPro.Examples.TMP_TextSelector_B::m_matrix
	Matrix4x4_t2933234003  ___m_matrix_14;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.TMP_TextSelector_B::m_cachedMeshInfoVertexData
	TMP_MeshInfoU5BU5D_t2398608976* ___m_cachedMeshInfoVertexData_15;

public:
	inline static int32_t get_offset_of_TextPopup_Prefab_01_2() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___TextPopup_Prefab_01_2)); }
	inline RectTransform_t3349966182 * get_TextPopup_Prefab_01_2() const { return ___TextPopup_Prefab_01_2; }
	inline RectTransform_t3349966182 ** get_address_of_TextPopup_Prefab_01_2() { return &___TextPopup_Prefab_01_2; }
	inline void set_TextPopup_Prefab_01_2(RectTransform_t3349966182 * value)
	{
		___TextPopup_Prefab_01_2 = value;
		Il2CppCodeGenWriteBarrier((&___TextPopup_Prefab_01_2), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_RectTransform_3() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_TextPopup_RectTransform_3)); }
	inline RectTransform_t3349966182 * get_m_TextPopup_RectTransform_3() const { return ___m_TextPopup_RectTransform_3; }
	inline RectTransform_t3349966182 ** get_address_of_m_TextPopup_RectTransform_3() { return &___m_TextPopup_RectTransform_3; }
	inline void set_m_TextPopup_RectTransform_3(RectTransform_t3349966182 * value)
	{
		___m_TextPopup_RectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_RectTransform_3), value);
	}

	inline static int32_t get_offset_of_m_TextPopup_TMPComponent_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_TextPopup_TMPComponent_4)); }
	inline TextMeshProUGUI_t934157183 * get_m_TextPopup_TMPComponent_4() const { return ___m_TextPopup_TMPComponent_4; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_m_TextPopup_TMPComponent_4() { return &___m_TextPopup_TMPComponent_4; }
	inline void set_m_TextPopup_TMPComponent_4(TextMeshProUGUI_t934157183 * value)
	{
		___m_TextPopup_TMPComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextPopup_TMPComponent_4), value);
	}

	inline static int32_t get_offset_of_m_TextMeshPro_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_TextMeshPro_7)); }
	inline TextMeshProUGUI_t934157183 * get_m_TextMeshPro_7() const { return ___m_TextMeshPro_7; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_m_TextMeshPro_7() { return &___m_TextMeshPro_7; }
	inline void set_m_TextMeshPro_7(TextMeshProUGUI_t934157183 * value)
	{
		___m_TextMeshPro_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_7), value);
	}

	inline static int32_t get_offset_of_m_Canvas_8() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_Canvas_8)); }
	inline Canvas_t209405766 * get_m_Canvas_8() const { return ___m_Canvas_8; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_8() { return &___m_Canvas_8; }
	inline void set_m_Canvas_8(Canvas_t209405766 * value)
	{
		___m_Canvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_8), value);
	}

	inline static int32_t get_offset_of_m_Camera_9() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_Camera_9)); }
	inline Camera_t189460977 * get_m_Camera_9() const { return ___m_Camera_9; }
	inline Camera_t189460977 ** get_address_of_m_Camera_9() { return &___m_Camera_9; }
	inline void set_m_Camera_9(Camera_t189460977 * value)
	{
		___m_Camera_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_9), value);
	}

	inline static int32_t get_offset_of_isHoveringObject_10() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___isHoveringObject_10)); }
	inline bool get_isHoveringObject_10() const { return ___isHoveringObject_10; }
	inline bool* get_address_of_isHoveringObject_10() { return &___isHoveringObject_10; }
	inline void set_isHoveringObject_10(bool value)
	{
		___isHoveringObject_10 = value;
	}

	inline static int32_t get_offset_of_m_selectedWord_11() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_selectedWord_11)); }
	inline int32_t get_m_selectedWord_11() const { return ___m_selectedWord_11; }
	inline int32_t* get_address_of_m_selectedWord_11() { return &___m_selectedWord_11; }
	inline void set_m_selectedWord_11(int32_t value)
	{
		___m_selectedWord_11 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_12() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_selectedLink_12)); }
	inline int32_t get_m_selectedLink_12() const { return ___m_selectedLink_12; }
	inline int32_t* get_address_of_m_selectedLink_12() { return &___m_selectedLink_12; }
	inline void set_m_selectedLink_12(int32_t value)
	{
		___m_selectedLink_12 = value;
	}

	inline static int32_t get_offset_of_m_lastIndex_13() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_lastIndex_13)); }
	inline int32_t get_m_lastIndex_13() const { return ___m_lastIndex_13; }
	inline int32_t* get_address_of_m_lastIndex_13() { return &___m_lastIndex_13; }
	inline void set_m_lastIndex_13(int32_t value)
	{
		___m_lastIndex_13 = value;
	}

	inline static int32_t get_offset_of_m_matrix_14() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_matrix_14)); }
	inline Matrix4x4_t2933234003  get_m_matrix_14() const { return ___m_matrix_14; }
	inline Matrix4x4_t2933234003 * get_address_of_m_matrix_14() { return &___m_matrix_14; }
	inline void set_m_matrix_14(Matrix4x4_t2933234003  value)
	{
		___m_matrix_14 = value;
	}

	inline static int32_t get_offset_of_m_cachedMeshInfoVertexData_15() { return static_cast<int32_t>(offsetof(TMP_TextSelector_B_t2419327983, ___m_cachedMeshInfoVertexData_15)); }
	inline TMP_MeshInfoU5BU5D_t2398608976* get_m_cachedMeshInfoVertexData_15() const { return ___m_cachedMeshInfoVertexData_15; }
	inline TMP_MeshInfoU5BU5D_t2398608976** get_address_of_m_cachedMeshInfoVertexData_15() { return &___m_cachedMeshInfoVertexData_15; }
	inline void set_m_cachedMeshInfoVertexData_15(TMP_MeshInfoU5BU5D_t2398608976* value)
	{
		___m_cachedMeshInfoVertexData_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_cachedMeshInfoVertexData_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_B_T2419327983_H
#ifndef TMP_TEXTSELECTOR_A_T2419327982_H
#define TMP_TEXTSELECTOR_A_T2419327982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TMP_TextSelector_A
struct  TMP_TextSelector_A_t2419327982  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TextMeshPro TMPro.Examples.TMP_TextSelector_A::m_TextMeshPro
	TextMeshPro_t2521834357 * ___m_TextMeshPro_2;
	// UnityEngine.Camera TMPro.Examples.TMP_TextSelector_A::m_Camera
	Camera_t189460977 * ___m_Camera_3;
	// System.Boolean TMPro.Examples.TMP_TextSelector_A::m_isHoveringObject
	bool ___m_isHoveringObject_4;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_selectedLink
	int32_t ___m_selectedLink_5;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastCharIndex
	int32_t ___m_lastCharIndex_6;
	// System.Int32 TMPro.Examples.TMP_TextSelector_A::m_lastWordIndex
	int32_t ___m_lastWordIndex_7;

public:
	inline static int32_t get_offset_of_m_TextMeshPro_2() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t2419327982, ___m_TextMeshPro_2)); }
	inline TextMeshPro_t2521834357 * get_m_TextMeshPro_2() const { return ___m_TextMeshPro_2; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_TextMeshPro_2() { return &___m_TextMeshPro_2; }
	inline void set_m_TextMeshPro_2(TextMeshPro_t2521834357 * value)
	{
		___m_TextMeshPro_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextMeshPro_2), value);
	}

	inline static int32_t get_offset_of_m_Camera_3() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t2419327982, ___m_Camera_3)); }
	inline Camera_t189460977 * get_m_Camera_3() const { return ___m_Camera_3; }
	inline Camera_t189460977 ** get_address_of_m_Camera_3() { return &___m_Camera_3; }
	inline void set_m_Camera_3(Camera_t189460977 * value)
	{
		___m_Camera_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_3), value);
	}

	inline static int32_t get_offset_of_m_isHoveringObject_4() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t2419327982, ___m_isHoveringObject_4)); }
	inline bool get_m_isHoveringObject_4() const { return ___m_isHoveringObject_4; }
	inline bool* get_address_of_m_isHoveringObject_4() { return &___m_isHoveringObject_4; }
	inline void set_m_isHoveringObject_4(bool value)
	{
		___m_isHoveringObject_4 = value;
	}

	inline static int32_t get_offset_of_m_selectedLink_5() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t2419327982, ___m_selectedLink_5)); }
	inline int32_t get_m_selectedLink_5() const { return ___m_selectedLink_5; }
	inline int32_t* get_address_of_m_selectedLink_5() { return &___m_selectedLink_5; }
	inline void set_m_selectedLink_5(int32_t value)
	{
		___m_selectedLink_5 = value;
	}

	inline static int32_t get_offset_of_m_lastCharIndex_6() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t2419327982, ___m_lastCharIndex_6)); }
	inline int32_t get_m_lastCharIndex_6() const { return ___m_lastCharIndex_6; }
	inline int32_t* get_address_of_m_lastCharIndex_6() { return &___m_lastCharIndex_6; }
	inline void set_m_lastCharIndex_6(int32_t value)
	{
		___m_lastCharIndex_6 = value;
	}

	inline static int32_t get_offset_of_m_lastWordIndex_7() { return static_cast<int32_t>(offsetof(TMP_TextSelector_A_t2419327982, ___m_lastWordIndex_7)); }
	inline int32_t get_m_lastWordIndex_7() const { return ___m_lastWordIndex_7; }
	inline int32_t* get_address_of_m_lastWordIndex_7() { return &___m_lastWordIndex_7; }
	inline void set_m_lastWordIndex_7(int32_t value)
	{
		___m_lastWordIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTSELECTOR_A_T2419327982_H
#ifndef VERTEXJITTER_T3970559362_H
#define VERTEXJITTER_T3970559362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexJitter
struct  VertexJitter_t3970559362  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TMPro.Examples.VertexJitter::AngleMultiplier
	float ___AngleMultiplier_2;
	// System.Single TMPro.Examples.VertexJitter::SpeedMultiplier
	float ___SpeedMultiplier_3;
	// System.Single TMPro.Examples.VertexJitter::CurveScale
	float ___CurveScale_4;
	// TMPro.TMP_Text TMPro.Examples.VertexJitter::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_5;
	// System.Boolean TMPro.Examples.VertexJitter::hasTextChanged
	bool ___hasTextChanged_6;

public:
	inline static int32_t get_offset_of_AngleMultiplier_2() { return static_cast<int32_t>(offsetof(VertexJitter_t3970559362, ___AngleMultiplier_2)); }
	inline float get_AngleMultiplier_2() const { return ___AngleMultiplier_2; }
	inline float* get_address_of_AngleMultiplier_2() { return &___AngleMultiplier_2; }
	inline void set_AngleMultiplier_2(float value)
	{
		___AngleMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_3() { return static_cast<int32_t>(offsetof(VertexJitter_t3970559362, ___SpeedMultiplier_3)); }
	inline float get_SpeedMultiplier_3() const { return ___SpeedMultiplier_3; }
	inline float* get_address_of_SpeedMultiplier_3() { return &___SpeedMultiplier_3; }
	inline void set_SpeedMultiplier_3(float value)
	{
		___SpeedMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(VertexJitter_t3970559362, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_5() { return static_cast<int32_t>(offsetof(VertexJitter_t3970559362, ___m_TextComponent_5)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_5() const { return ___m_TextComponent_5; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_5() { return &___m_TextComponent_5; }
	inline void set_m_TextComponent_5(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_5), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_6() { return static_cast<int32_t>(offsetof(VertexJitter_t3970559362, ___hasTextChanged_6)); }
	inline bool get_hasTextChanged_6() const { return ___hasTextChanged_6; }
	inline bool* get_address_of_hasTextChanged_6() { return &___hasTextChanged_6; }
	inline void set_hasTextChanged_6(bool value)
	{
		___hasTextChanged_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXJITTER_T3970559362_H
#ifndef TELETYPE_T2513439854_H
#define TELETYPE_T2513439854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TeleType
struct  TeleType_t2513439854  : public MonoBehaviour_t1158329972
{
public:
	// System.String TMPro.Examples.TeleType::label01
	String_t* ___label01_2;
	// System.String TMPro.Examples.TeleType::label02
	String_t* ___label02_3;
	// TMPro.TMP_Text TMPro.Examples.TeleType::m_textMeshPro
	TMP_Text_t1920000777 * ___m_textMeshPro_4;

public:
	inline static int32_t get_offset_of_label01_2() { return static_cast<int32_t>(offsetof(TeleType_t2513439854, ___label01_2)); }
	inline String_t* get_label01_2() const { return ___label01_2; }
	inline String_t** get_address_of_label01_2() { return &___label01_2; }
	inline void set_label01_2(String_t* value)
	{
		___label01_2 = value;
		Il2CppCodeGenWriteBarrier((&___label01_2), value);
	}

	inline static int32_t get_offset_of_label02_3() { return static_cast<int32_t>(offsetof(TeleType_t2513439854, ___label02_3)); }
	inline String_t* get_label02_3() const { return ___label02_3; }
	inline String_t** get_address_of_label02_3() { return &___label02_3; }
	inline void set_label02_3(String_t* value)
	{
		___label02_3 = value;
		Il2CppCodeGenWriteBarrier((&___label02_3), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(TeleType_t2513439854, ___m_textMeshPro_4)); }
	inline TMP_Text_t1920000777 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TMP_Text_t1920000777 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TMP_Text_t1920000777 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELETYPE_T2513439854_H
#ifndef VERTEXZOOM_T3463934435_H
#define VERTEXZOOM_T3463934435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexZoom
struct  VertexZoom_t3463934435  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TMPro.Examples.VertexZoom::AngleMultiplier
	float ___AngleMultiplier_2;
	// System.Single TMPro.Examples.VertexZoom::SpeedMultiplier
	float ___SpeedMultiplier_3;
	// System.Single TMPro.Examples.VertexZoom::CurveScale
	float ___CurveScale_4;
	// TMPro.TMP_Text TMPro.Examples.VertexZoom::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_5;
	// System.Boolean TMPro.Examples.VertexZoom::hasTextChanged
	bool ___hasTextChanged_6;

public:
	inline static int32_t get_offset_of_AngleMultiplier_2() { return static_cast<int32_t>(offsetof(VertexZoom_t3463934435, ___AngleMultiplier_2)); }
	inline float get_AngleMultiplier_2() const { return ___AngleMultiplier_2; }
	inline float* get_address_of_AngleMultiplier_2() { return &___AngleMultiplier_2; }
	inline void set_AngleMultiplier_2(float value)
	{
		___AngleMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_3() { return static_cast<int32_t>(offsetof(VertexZoom_t3463934435, ___SpeedMultiplier_3)); }
	inline float get_SpeedMultiplier_3() const { return ___SpeedMultiplier_3; }
	inline float* get_address_of_SpeedMultiplier_3() { return &___SpeedMultiplier_3; }
	inline void set_SpeedMultiplier_3(float value)
	{
		___SpeedMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(VertexZoom_t3463934435, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_5() { return static_cast<int32_t>(offsetof(VertexZoom_t3463934435, ___m_TextComponent_5)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_5() const { return ___m_TextComponent_5; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_5() { return &___m_TextComponent_5; }
	inline void set_m_TextComponent_5(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_5), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_6() { return static_cast<int32_t>(offsetof(VertexZoom_t3463934435, ___hasTextChanged_6)); }
	inline bool get_hasTextChanged_6() const { return ___hasTextChanged_6; }
	inline bool* get_address_of_hasTextChanged_6() { return &___hasTextChanged_6; }
	inline void set_hasTextChanged_6(bool value)
	{
		___hasTextChanged_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXZOOM_T3463934435_H
#ifndef SKEWTEXTEXAMPLE_T3378890949_H
#define SKEWTEXTEXAMPLE_T3378890949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SkewTextExample
struct  SkewTextExample_t3378890949  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_Text TMPro.Examples.SkewTextExample::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_2;
	// UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::VertexCurve
	AnimationCurve_t3306541151 * ___VertexCurve_3;
	// System.Single TMPro.Examples.SkewTextExample::CurveScale
	float ___CurveScale_4;
	// System.Single TMPro.Examples.SkewTextExample::ShearAmount
	float ___ShearAmount_5;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(SkewTextExample_t3378890949, ___m_TextComponent_2)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_VertexCurve_3() { return static_cast<int32_t>(offsetof(SkewTextExample_t3378890949, ___VertexCurve_3)); }
	inline AnimationCurve_t3306541151 * get_VertexCurve_3() const { return ___VertexCurve_3; }
	inline AnimationCurve_t3306541151 ** get_address_of_VertexCurve_3() { return &___VertexCurve_3; }
	inline void set_VertexCurve_3(AnimationCurve_t3306541151 * value)
	{
		___VertexCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_3), value);
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(SkewTextExample_t3378890949, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_ShearAmount_5() { return static_cast<int32_t>(offsetof(SkewTextExample_t3378890949, ___ShearAmount_5)); }
	inline float get_ShearAmount_5() const { return ___ShearAmount_5; }
	inline float* get_address_of_ShearAmount_5() { return &___ShearAmount_5; }
	inline void set_ShearAmount_5(float value)
	{
		___ShearAmount_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKEWTEXTEXAMPLE_T3378890949_H
#ifndef SIMPLESCRIPT_T767280347_H
#define SIMPLESCRIPT_T767280347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SimpleScript
struct  SimpleScript_t767280347  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t2521834357 * ___m_textMeshPro_2;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_4;

public:
	inline static int32_t get_offset_of_m_textMeshPro_2() { return static_cast<int32_t>(offsetof(SimpleScript_t767280347, ___m_textMeshPro_2)); }
	inline TextMeshPro_t2521834357 * get_m_textMeshPro_2() const { return ___m_textMeshPro_2; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_textMeshPro_2() { return &___m_textMeshPro_2; }
	inline void set_m_textMeshPro_2(TextMeshPro_t2521834357 * value)
	{
		___m_textMeshPro_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_2), value);
	}

	inline static int32_t get_offset_of_m_frame_4() { return static_cast<int32_t>(offsetof(SimpleScript_t767280347, ___m_frame_4)); }
	inline float get_m_frame_4() const { return ___m_frame_4; }
	inline float* get_address_of_m_frame_4() { return &___m_frame_4; }
	inline void set_m_frame_4(float value)
	{
		___m_frame_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESCRIPT_T767280347_H
#ifndef WARPTEXTEXAMPLE_T250871869_H
#define WARPTEXTEXAMPLE_T250871869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.WarpTextExample
struct  WarpTextExample_t250871869  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_Text TMPro.Examples.WarpTextExample::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_2;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::VertexCurve
	AnimationCurve_t3306541151 * ___VertexCurve_3;
	// System.Single TMPro.Examples.WarpTextExample::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.WarpTextExample::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.WarpTextExample::CurveScale
	float ___CurveScale_6;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(WarpTextExample_t250871869, ___m_TextComponent_2)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_VertexCurve_3() { return static_cast<int32_t>(offsetof(WarpTextExample_t250871869, ___VertexCurve_3)); }
	inline AnimationCurve_t3306541151 * get_VertexCurve_3() const { return ___VertexCurve_3; }
	inline AnimationCurve_t3306541151 ** get_address_of_VertexCurve_3() { return &___VertexCurve_3; }
	inline void set_VertexCurve_3(AnimationCurve_t3306541151 * value)
	{
		___VertexCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___VertexCurve_3), value);
	}

	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(WarpTextExample_t250871869, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(WarpTextExample_t250871869, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(WarpTextExample_t250871869, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARPTEXTEXAMPLE_T250871869_H
#ifndef VERTEXSHAKEA_T3019979321_H
#define VERTEXSHAKEA_T3019979321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeA
struct  VertexShakeA_t3019979321  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TMPro.Examples.VertexShakeA::AngleMultiplier
	float ___AngleMultiplier_2;
	// System.Single TMPro.Examples.VertexShakeA::SpeedMultiplier
	float ___SpeedMultiplier_3;
	// System.Single TMPro.Examples.VertexShakeA::ScaleMultiplier
	float ___ScaleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeA::RotationMultiplier
	float ___RotationMultiplier_5;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeA::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_6;
	// System.Boolean TMPro.Examples.VertexShakeA::hasTextChanged
	bool ___hasTextChanged_7;

public:
	inline static int32_t get_offset_of_AngleMultiplier_2() { return static_cast<int32_t>(offsetof(VertexShakeA_t3019979321, ___AngleMultiplier_2)); }
	inline float get_AngleMultiplier_2() const { return ___AngleMultiplier_2; }
	inline float* get_address_of_AngleMultiplier_2() { return &___AngleMultiplier_2; }
	inline void set_AngleMultiplier_2(float value)
	{
		___AngleMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_3() { return static_cast<int32_t>(offsetof(VertexShakeA_t3019979321, ___SpeedMultiplier_3)); }
	inline float get_SpeedMultiplier_3() const { return ___SpeedMultiplier_3; }
	inline float* get_address_of_SpeedMultiplier_3() { return &___SpeedMultiplier_3; }
	inline void set_SpeedMultiplier_3(float value)
	{
		___SpeedMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_ScaleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeA_t3019979321, ___ScaleMultiplier_4)); }
	inline float get_ScaleMultiplier_4() const { return ___ScaleMultiplier_4; }
	inline float* get_address_of_ScaleMultiplier_4() { return &___ScaleMultiplier_4; }
	inline void set_ScaleMultiplier_4(float value)
	{
		___ScaleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_RotationMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeA_t3019979321, ___RotationMultiplier_5)); }
	inline float get_RotationMultiplier_5() const { return ___RotationMultiplier_5; }
	inline float* get_address_of_RotationMultiplier_5() { return &___RotationMultiplier_5; }
	inline void set_RotationMultiplier_5(float value)
	{
		___RotationMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_6() { return static_cast<int32_t>(offsetof(VertexShakeA_t3019979321, ___m_TextComponent_6)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_6() const { return ___m_TextComponent_6; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_6() { return &___m_TextComponent_6; }
	inline void set_m_TextComponent_6(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_6), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_7() { return static_cast<int32_t>(offsetof(VertexShakeA_t3019979321, ___hasTextChanged_7)); }
	inline bool get_hasTextChanged_7() const { return ___hasTextChanged_7; }
	inline bool* get_address_of_hasTextChanged_7() { return &___hasTextChanged_7; }
	inline void set_hasTextChanged_7(bool value)
	{
		___hasTextChanged_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSHAKEA_T3019979321_H
#ifndef TEXTMESHSPAWNER_T3794282288_H
#define TEXTMESHSPAWNER_T3794282288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshSpawner
struct  TextMeshSpawner_t3794282288  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 TMPro.Examples.TextMeshSpawner::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.TextMeshSpawner::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// UnityEngine.Font TMPro.Examples.TextMeshSpawner::TheFont
	Font_t4239498691 * ___TheFont_4;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshSpawner::floatingText_Script
	TextMeshProFloatingText_t6181308 * ___floatingText_Script_5;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t3794282288, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t3794282288, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t3794282288, ___TheFont_4)); }
	inline Font_t4239498691 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t4239498691 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t4239498691 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}

	inline static int32_t get_offset_of_floatingText_Script_5() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t3794282288, ___floatingText_Script_5)); }
	inline TextMeshProFloatingText_t6181308 * get_floatingText_Script_5() const { return ___floatingText_Script_5; }
	inline TextMeshProFloatingText_t6181308 ** get_address_of_floatingText_Script_5() { return &___floatingText_Script_5; }
	inline void set_floatingText_Script_5(TextMeshProFloatingText_t6181308 * value)
	{
		___floatingText_Script_5 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHSPAWNER_T3794282288_H
#ifndef TEXTMESHPROFLOATINGTEXT_T6181308_H
#define TEXTMESHPROFLOATINGTEXT_T6181308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextMeshProFloatingText
struct  TextMeshProFloatingText_t6181308  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Font TMPro.Examples.TextMeshProFloatingText::TheFont
	Font_t4239498691 * ___TheFont_2;
	// UnityEngine.GameObject TMPro.Examples.TextMeshProFloatingText::m_floatingText
	GameObject_t1756533147 * ___m_floatingText_3;
	// TMPro.TextMeshPro TMPro.Examples.TextMeshProFloatingText::m_textMeshPro
	TextMeshPro_t2521834357 * ___m_textMeshPro_4;
	// UnityEngine.TextMesh TMPro.Examples.TextMeshProFloatingText::m_textMesh
	TextMesh_t1641806576 * ___m_textMesh_5;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_transform
	Transform_t3275118058 * ___m_transform_6;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_floatingText_Transform
	Transform_t3275118058 * ___m_floatingText_Transform_7;
	// UnityEngine.Transform TMPro.Examples.TextMeshProFloatingText::m_cameraTransform
	Transform_t3275118058 * ___m_cameraTransform_8;
	// UnityEngine.Vector3 TMPro.Examples.TextMeshProFloatingText::lastPOS
	Vector3_t2243707580  ___lastPOS_9;
	// UnityEngine.Quaternion TMPro.Examples.TextMeshProFloatingText::lastRotation
	Quaternion_t4030073918  ___lastRotation_10;
	// System.Int32 TMPro.Examples.TextMeshProFloatingText::SpawnType
	int32_t ___SpawnType_11;

public:
	inline static int32_t get_offset_of_TheFont_2() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___TheFont_2)); }
	inline Font_t4239498691 * get_TheFont_2() const { return ___TheFont_2; }
	inline Font_t4239498691 ** get_address_of_TheFont_2() { return &___TheFont_2; }
	inline void set_TheFont_2(Font_t4239498691 * value)
	{
		___TheFont_2 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_2), value);
	}

	inline static int32_t get_offset_of_m_floatingText_3() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___m_floatingText_3)); }
	inline GameObject_t1756533147 * get_m_floatingText_3() const { return ___m_floatingText_3; }
	inline GameObject_t1756533147 ** get_address_of_m_floatingText_3() { return &___m_floatingText_3; }
	inline void set_m_floatingText_3(GameObject_t1756533147 * value)
	{
		___m_floatingText_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_3), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_4() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___m_textMeshPro_4)); }
	inline TextMeshPro_t2521834357 * get_m_textMeshPro_4() const { return ___m_textMeshPro_4; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_textMeshPro_4() { return &___m_textMeshPro_4; }
	inline void set_m_textMeshPro_4(TextMeshPro_t2521834357 * value)
	{
		___m_textMeshPro_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_4), value);
	}

	inline static int32_t get_offset_of_m_textMesh_5() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___m_textMesh_5)); }
	inline TextMesh_t1641806576 * get_m_textMesh_5() const { return ___m_textMesh_5; }
	inline TextMesh_t1641806576 ** get_address_of_m_textMesh_5() { return &___m_textMesh_5; }
	inline void set_m_textMesh_5(TextMesh_t1641806576 * value)
	{
		___m_textMesh_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_5), value);
	}

	inline static int32_t get_offset_of_m_transform_6() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___m_transform_6)); }
	inline Transform_t3275118058 * get_m_transform_6() const { return ___m_transform_6; }
	inline Transform_t3275118058 ** get_address_of_m_transform_6() { return &___m_transform_6; }
	inline void set_m_transform_6(Transform_t3275118058 * value)
	{
		___m_transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_6), value);
	}

	inline static int32_t get_offset_of_m_floatingText_Transform_7() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___m_floatingText_Transform_7)); }
	inline Transform_t3275118058 * get_m_floatingText_Transform_7() const { return ___m_floatingText_Transform_7; }
	inline Transform_t3275118058 ** get_address_of_m_floatingText_Transform_7() { return &___m_floatingText_Transform_7; }
	inline void set_m_floatingText_Transform_7(Transform_t3275118058 * value)
	{
		___m_floatingText_Transform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_floatingText_Transform_7), value);
	}

	inline static int32_t get_offset_of_m_cameraTransform_8() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___m_cameraTransform_8)); }
	inline Transform_t3275118058 * get_m_cameraTransform_8() const { return ___m_cameraTransform_8; }
	inline Transform_t3275118058 ** get_address_of_m_cameraTransform_8() { return &___m_cameraTransform_8; }
	inline void set_m_cameraTransform_8(Transform_t3275118058 * value)
	{
		___m_cameraTransform_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_cameraTransform_8), value);
	}

	inline static int32_t get_offset_of_lastPOS_9() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___lastPOS_9)); }
	inline Vector3_t2243707580  get_lastPOS_9() const { return ___lastPOS_9; }
	inline Vector3_t2243707580 * get_address_of_lastPOS_9() { return &___lastPOS_9; }
	inline void set_lastPOS_9(Vector3_t2243707580  value)
	{
		___lastPOS_9 = value;
	}

	inline static int32_t get_offset_of_lastRotation_10() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___lastRotation_10)); }
	inline Quaternion_t4030073918  get_lastRotation_10() const { return ___lastRotation_10; }
	inline Quaternion_t4030073918 * get_address_of_lastRotation_10() { return &___lastRotation_10; }
	inline void set_lastRotation_10(Quaternion_t4030073918  value)
	{
		___lastRotation_10 = value;
	}

	inline static int32_t get_offset_of_SpawnType_11() { return static_cast<int32_t>(offsetof(TextMeshProFloatingText_t6181308, ___SpawnType_11)); }
	inline int32_t get_SpawnType_11() const { return ___SpawnType_11; }
	inline int32_t* get_address_of_SpawnType_11() { return &___SpawnType_11; }
	inline void set_SpawnType_11(int32_t value)
	{
		___SpawnType_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROFLOATINGTEXT_T6181308_H
#ifndef TEXTCONSOLESIMULATOR_T2207663326_H
#define TEXTCONSOLESIMULATOR_T2207663326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.TextConsoleSimulator
struct  TextConsoleSimulator_t2207663326  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_Text TMPro.Examples.TextConsoleSimulator::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_2;
	// System.Boolean TMPro.Examples.TextConsoleSimulator::hasTextChanged
	bool ___hasTextChanged_3;

public:
	inline static int32_t get_offset_of_m_TextComponent_2() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t2207663326, ___m_TextComponent_2)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_2() const { return ___m_TextComponent_2; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_2() { return &___m_TextComponent_2; }
	inline void set_m_TextComponent_2(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_2), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_3() { return static_cast<int32_t>(offsetof(TextConsoleSimulator_t2207663326, ___hasTextChanged_3)); }
	inline bool get_hasTextChanged_3() const { return ___hasTextChanged_3; }
	inline bool* get_address_of_hasTextChanged_3() { return &___hasTextChanged_3; }
	inline void set_hasTextChanged_3(bool value)
	{
		___hasTextChanged_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONSOLESIMULATOR_T2207663326_H
#ifndef VERTEXSHAKEB_T3019979320_H
#define VERTEXSHAKEB_T3019979320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.VertexShakeB
struct  VertexShakeB_t3019979320  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TMPro.Examples.VertexShakeB::AngleMultiplier
	float ___AngleMultiplier_2;
	// System.Single TMPro.Examples.VertexShakeB::SpeedMultiplier
	float ___SpeedMultiplier_3;
	// System.Single TMPro.Examples.VertexShakeB::CurveScale
	float ___CurveScale_4;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeB::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_5;
	// System.Boolean TMPro.Examples.VertexShakeB::hasTextChanged
	bool ___hasTextChanged_6;

public:
	inline static int32_t get_offset_of_AngleMultiplier_2() { return static_cast<int32_t>(offsetof(VertexShakeB_t3019979320, ___AngleMultiplier_2)); }
	inline float get_AngleMultiplier_2() const { return ___AngleMultiplier_2; }
	inline float* get_address_of_AngleMultiplier_2() { return &___AngleMultiplier_2; }
	inline void set_AngleMultiplier_2(float value)
	{
		___AngleMultiplier_2 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_3() { return static_cast<int32_t>(offsetof(VertexShakeB_t3019979320, ___SpeedMultiplier_3)); }
	inline float get_SpeedMultiplier_3() const { return ___SpeedMultiplier_3; }
	inline float* get_address_of_SpeedMultiplier_3() { return &___SpeedMultiplier_3; }
	inline void set_SpeedMultiplier_3(float value)
	{
		___SpeedMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_CurveScale_4() { return static_cast<int32_t>(offsetof(VertexShakeB_t3019979320, ___CurveScale_4)); }
	inline float get_CurveScale_4() const { return ___CurveScale_4; }
	inline float* get_address_of_CurveScale_4() { return &___CurveScale_4; }
	inline void set_CurveScale_4(float value)
	{
		___CurveScale_4 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_5() { return static_cast<int32_t>(offsetof(VertexShakeB_t3019979320, ___m_TextComponent_5)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_5() const { return ___m_TextComponent_5; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_5() { return &___m_TextComponent_5; }
	inline void set_m_TextComponent_5(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_5), value);
	}

	inline static int32_t get_offset_of_hasTextChanged_6() { return static_cast<int32_t>(offsetof(VertexShakeB_t3019979320, ___hasTextChanged_6)); }
	inline bool get_hasTextChanged_6() const { return ___hasTextChanged_6; }
	inline bool* get_address_of_hasTextChanged_6() { return &___hasTextChanged_6; }
	inline void set_hasTextChanged_6(bool value)
	{
		___hasTextChanged_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSHAKEB_T3019979320_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (PathCreator_t1368626459), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[8] = 
{
	PathCreator_t1368626459::get_offset_of_path_2(),
	PathCreator_t1368626459::get_offset_of_anchorCol_3(),
	PathCreator_t1368626459::get_offset_of_controlCol_4(),
	PathCreator_t1368626459::get_offset_of_segmentCol_5(),
	PathCreator_t1368626459::get_offset_of_selectedSegmentCol_6(),
	PathCreator_t1368626459::get_offset_of_anchorDiameter_7(),
	PathCreator_t1368626459::get_offset_of_controlDiameter_8(),
	PathCreator_t1368626459::get_offset_of_displayControlPoints_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (PathPlacer_t683486292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[2] = 
{
	PathPlacer_t683486292::get_offset_of_spacing_2(),
	PathPlacer_t683486292::get_offset_of_resolution_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { sizeof (RoadCreator_t1201856826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[5] = 
{
	RoadCreator_t1201856826::get_offset_of_spacing_2(),
	RoadCreator_t1201856826::get_offset_of_roadWidth_3(),
	RoadCreator_t1201856826::get_offset_of_autoUpdate_4(),
	RoadCreator_t1201856826::get_offset_of_tiling_5(),
	RoadCreator_t1201856826::get_offset_of_vertSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { sizeof (VertPositionSpace_t1848587165)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2803[3] = 
{
	VertPositionSpace_t1848587165::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { sizeof (CameraRaycast_t2547618150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2804[6] = 
{
	CameraRaycast_t2547618150::get_offset_of_cam_2(),
	CameraRaycast_t2547618150::get_offset_of_eventSystem_3(),
	CameraRaycast_t2547618150::get_offset_of_lastObjectDown_4(),
	CameraRaycast_t2547618150::get_offset_of_results_5(),
	CameraRaycast_t2547618150::get_offset_of_mouseOver_6(),
	CameraRaycast_t2547618150::get_offset_of_pointerData_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { sizeof (FullCSV_t1597705491)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[5] = 
{
	FullCSV_t1597705491::get_offset_of_U3CNumberOfTitlesU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FullCSV_t1597705491::get_offset_of_U3CNumberOfLinesU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FullCSV_t1597705491::get_offset_of_U3CTitlesU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FullCSV_t1597705491::get_offset_of_U3CLinesU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FullCSV_t1597705491::get_offset_of_U3CTableU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { sizeof (CSVReader_t2661561169), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (EntryLink_t1717819552), -1, sizeof(EntryLink_t1717819552_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2807[11] = 
{
	EntryLink_t1717819552::get_offset_of_U3CkeyLinkU3Ek__BackingField_2(),
	EntryLink_t1717819552::get_offset_of_U3CURLLinkU3Ek__BackingField_3(),
	EntryLink_t1717819552::get_offset_of_U3CCSVLinkU3Ek__BackingField_4(),
	EntryLink_t1717819552::get_offset_of_entryText_5(),
	EntryLink_t1717819552::get_offset_of_image_6(),
	EntryLink_t1717819552::get_offset_of_exitAlpha_7(),
	EntryLink_t1717819552::get_offset_of_enterAlpha_8(),
	EntryLink_t1717819552::get_offset_of_clickAlpha_9(),
	EntryLink_t1717819552_StaticFields::get_offset_of_currentLink_10(),
	EntryLink_t1717819552::get_offset_of_isOn_11(),
	EntryLink_t1717819552_StaticFields::get_offset_of_SelectAssigned_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (NoSig_t75605465), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (GForce_t1402080092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[4] = 
{
	GForce_t1402080092::get_offset_of_minForce_2(),
	GForce_t1402080092::get_offset_of_maxForce_3(),
	GForce_t1402080092::get_offset_of_maxPosition_4(),
	GForce_t1402080092::get_offset_of_maxRadians_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (MapCreator_t2101643938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[9] = 
{
	MapCreator_t2101643938::get_offset_of_mapCamera_2(),
	MapCreator_t2101643938::get_offset_of_roadObject_3(),
	MapCreator_t2101643938::get_offset_of_mapBlip_4(),
	MapCreator_t2101643938::get_offset_of_cameraSize_5(),
	MapCreator_t2101643938::get_offset_of_trackLoopThreshold_6(),
	MapCreator_t2101643938::get_offset_of_pathCreator_7(),
	MapCreator_t2101643938::get_offset_of_path_8(),
	MapCreator_t2101643938::get_offset_of_roadCreator_9(),
	MapCreator_t2101643938::get_offset_of_coordMinMax_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (XYBoth_t3347462289)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2811[4] = 
{
	XYBoth_t3347462289::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (MinMax_t2491720607)+ sizeof (RuntimeObject), sizeof(MinMax_t2491720607 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2812[7] = 
{
	MinMax_t2491720607::get_offset_of_U3CminXU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MinMax_t2491720607::get_offset_of_U3CminYU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MinMax_t2491720607::get_offset_of_U3CmaxXU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MinMax_t2491720607::get_offset_of_U3CmaxYU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MinMax_t2491720607::get_offset_of_U3CxyBothU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MinMax_t2491720607::get_offset_of_U3CxyDiffU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MinMax_t2491720607::get_offset_of_U3CxyOtherDiffU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (PlayButton_t1602700306), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[4] = 
{
	PlayButton_t1602700306::get_offset_of_playButton_2(),
	PlayButton_t1602700306::get_offset_of_canvasToHide_3(),
	PlayButton_t1602700306::get_offset_of_demoCSV_4(),
	PlayButton_t1602700306::get_offset_of_testScript_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (RacingWidgets_t915614521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[10] = 
{
	0,
	RacingWidgets_t915614521::get_offset_of_timeMonospacing_3(),
	RacingWidgets_t915614521::get_offset_of_speedMonospacing_4(),
	RacingWidgets_t915614521::get_offset_of_rpmMonospacing_5(),
	RacingWidgets_t915614521::get_offset_of_timeText_6(),
	RacingWidgets_t915614521::get_offset_of_speedText_7(),
	RacingWidgets_t915614521::get_offset_of_rpmText_8(),
	RacingWidgets_t915614521::get_offset_of_rpmAnimator_9(),
	RacingWidgets_t915614521::get_offset_of_speedAnimator_10(),
	RacingWidgets_t915614521::get_offset_of_gForceDot_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (RendererExtensions_t1168630521), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (S3Connect_t2820114966), -1, sizeof(S3Connect_t2820114966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2816[6] = 
{
	S3Connect_t2820114966::get_offset_of_S3BucketName_2(),
	S3Connect_t2820114966::get_offset_of_S3Key_3(),
	S3Connect_t2820114966::get_offset_of_linkEntryPrefab_4(),
	S3Connect_t2820114966::get_offset_of_linkEntryParent_5(),
	S3Connect_t2820114966::get_offset_of__s3Client_6(),
	S3Connect_t2820114966_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (Scroller_t2879750794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[5] = 
{
	Scroller_t2879750794::get_offset_of_scrollAmount_2(),
	Scroller_t2879750794::get_offset_of_minAmount_3(),
	Scroller_t2879750794::get_offset_of_maxAmount_4(),
	Scroller_t2879750794::get_offset_of_upButton_5(),
	Scroller_t2879750794::get_offset_of_downButton_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (U3CGetSizeU3Ec__Iterator0_t1957153935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[4] = 
{
	U3CGetSizeU3Ec__Iterator0_t1957153935::get_offset_of_U24this_0(),
	U3CGetSizeU3Ec__Iterator0_t1957153935::get_offset_of_U24current_1(),
	U3CGetSizeU3Ec__Iterator0_t1957153935::get_offset_of_U24disposing_2(),
	U3CGetSizeU3Ec__Iterator0_t1957153935::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (RaceInfo_t3176063123)+ sizeof (RuntimeObject), sizeof(RaceInfo_t3176063123 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2819[7] = 
{
	RaceInfo_t3176063123::get_offset_of_U3CTimeU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaceInfo_t3176063123::get_offset_of_U3CSpeedU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaceInfo_t3176063123::get_offset_of_U3CRPMU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaceInfo_t3176063123::get_offset_of_U3CLatU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaceInfo_t3176063123::get_offset_of_U3CLonU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaceInfo_t3176063123::get_offset_of_U3CGXU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaceInfo_t3176063123::get_offset_of_U3CGYU3Ek__BackingField_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (TestScript_t905662927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[15] = 
{
	TestScript_t905662927::get_offset_of_reader_2(),
	TestScript_t905662927::get_offset_of_times_3(),
	TestScript_t905662927::get_offset_of_speed_4(),
	TestScript_t905662927::get_offset_of_rpm_5(),
	TestScript_t905662927::get_offset_of_lat_6(),
	TestScript_t905662927::get_offset_of_lon_7(),
	TestScript_t905662927::get_offset_of_gX_8(),
	TestScript_t905662927::get_offset_of_gY_9(),
	TestScript_t905662927::get_offset_of_video_10(),
	TestScript_t905662927::get_offset_of_canvas_11(),
	TestScript_t905662927::get_offset_of_mapCreator_12(),
	TestScript_t905662927::get_offset_of_videoMenu_13(),
	TestScript_t905662927::get_offset_of_viewCamera_14(),
	TestScript_t905662927::get_offset_of_videoMenuDistance_15(),
	TestScript_t905662927::get_offset_of_widgets_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (U3CLoadFilesU3Ec__Iterator0_t2452679976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[8] = 
{
	U3CLoadFilesU3Ec__Iterator0_t2452679976::get_offset_of_csvURL_0(),
	U3CLoadFilesU3Ec__Iterator0_t2452679976::get_offset_of_U3CloadCSVU3E__0_1(),
	U3CLoadFilesU3Ec__Iterator0_t2452679976::get_offset_of_videoURL_2(),
	U3CLoadFilesU3Ec__Iterator0_t2452679976::get_offset_of_U3CloadVideoU3E__0_3(),
	U3CLoadFilesU3Ec__Iterator0_t2452679976::get_offset_of_U24this_4(),
	U3CLoadFilesU3Ec__Iterator0_t2452679976::get_offset_of_U24current_5(),
	U3CLoadFilesU3Ec__Iterator0_t2452679976::get_offset_of_U24disposing_6(),
	U3CLoadFilesU3Ec__Iterator0_t2452679976::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (U3CPrepareFilesU3Ec__Iterator1_t2748620044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[7] = 
{
	U3CPrepareFilesU3Ec__Iterator1_t2748620044::get_offset_of_csvURL_0(),
	U3CPrepareFilesU3Ec__Iterator1_t2748620044::get_offset_of_U3CloadCSVU3E__0_1(),
	U3CPrepareFilesU3Ec__Iterator1_t2748620044::get_offset_of_U3CloadVideoU3E__0_2(),
	U3CPrepareFilesU3Ec__Iterator1_t2748620044::get_offset_of_U24this_3(),
	U3CPrepareFilesU3Ec__Iterator1_t2748620044::get_offset_of_U24current_4(),
	U3CPrepareFilesU3Ec__Iterator1_t2748620044::get_offset_of_U24disposing_5(),
	U3CPrepareFilesU3Ec__Iterator1_t2748620044::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (U3CLoadCSVU3Ec__Iterator2_t2978953405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[7] = 
{
	U3CLoadCSVU3Ec__Iterator2_t2978953405::get_offset_of_csvURL_0(),
	U3CLoadCSVU3Ec__Iterator2_t2978953405::get_offset_of_U3CcsvLinkU3E__0_1(),
	U3CLoadCSVU3Ec__Iterator2_t2978953405::get_offset_of_U3CsplitCSVU3E__0_2(),
	U3CLoadCSVU3Ec__Iterator2_t2978953405::get_offset_of_U24this_3(),
	U3CLoadCSVU3Ec__Iterator2_t2978953405::get_offset_of_U24current_4(),
	U3CLoadCSVU3Ec__Iterator2_t2978953405::get_offset_of_U24disposing_5(),
	U3CLoadCSVU3Ec__Iterator2_t2978953405::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (U3CLoadVideoU3Ec__Iterator3_t4124372415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[5] = 
{
	U3CLoadVideoU3Ec__Iterator3_t4124372415::get_offset_of_videoURL_0(),
	U3CLoadVideoU3Ec__Iterator3_t4124372415::get_offset_of_U24this_1(),
	U3CLoadVideoU3Ec__Iterator3_t4124372415::get_offset_of_U24current_2(),
	U3CLoadVideoU3Ec__Iterator3_t4124372415::get_offset_of_U24disposing_3(),
	U3CLoadVideoU3Ec__Iterator3_t4124372415::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (U3CLoadVideoFileU3Ec__Iterator4_t2557724258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[4] = 
{
	U3CLoadVideoFileU3Ec__Iterator4_t2557724258::get_offset_of_U24this_0(),
	U3CLoadVideoFileU3Ec__Iterator4_t2557724258::get_offset_of_U24current_1(),
	U3CLoadVideoFileU3Ec__Iterator4_t2557724258::get_offset_of_U24disposing_2(),
	U3CLoadVideoFileU3Ec__Iterator4_t2557724258::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (Benchmark01_t2768175604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[10] = 
{
	Benchmark01_t2768175604::get_offset_of_BenchmarkType_2(),
	Benchmark01_t2768175604::get_offset_of_TMProFont_3(),
	Benchmark01_t2768175604::get_offset_of_TextMeshFont_4(),
	Benchmark01_t2768175604::get_offset_of_m_textMeshPro_5(),
	Benchmark01_t2768175604::get_offset_of_m_textContainer_6(),
	Benchmark01_t2768175604::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_t2768175604::get_offset_of_m_material01_10(),
	Benchmark01_t2768175604::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (U3CStartU3Ec__Iterator0_t1060020671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[5] = 
{
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1060020671::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { sizeof (Benchmark01_UGUI_t3449578277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[10] = 
{
	Benchmark01_UGUI_t3449578277::get_offset_of_BenchmarkType_2(),
	Benchmark01_UGUI_t3449578277::get_offset_of_canvas_3(),
	Benchmark01_UGUI_t3449578277::get_offset_of_TMProFont_4(),
	Benchmark01_UGUI_t3449578277::get_offset_of_TextMeshFont_5(),
	Benchmark01_UGUI_t3449578277::get_offset_of_m_textMeshPro_6(),
	Benchmark01_UGUI_t3449578277::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_UGUI_t3449578277::get_offset_of_m_material01_10(),
	Benchmark01_UGUI_t3449578277::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { sizeof (U3CStartU3Ec__Iterator0_t17013742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[5] = 
{
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t17013742::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (Benchmark02_t39292249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[3] = 
{
	Benchmark02_t39292249::get_offset_of_SpawnType_2(),
	Benchmark02_t39292249::get_offset_of_NumberOfNPC_3(),
	Benchmark02_t39292249::get_offset_of_floatingText_Script_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (Benchmark03_t1605376190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2831[3] = 
{
	Benchmark03_t1605376190::get_offset_of_SpawnType_2(),
	Benchmark03_t1605376190::get_offset_of_NumberOfNPC_3(),
	Benchmark03_t1605376190::get_offset_of_TheFont_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (Benchmark04_t3171460131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[5] = 
{
	Benchmark04_t3171460131::get_offset_of_SpawnType_2(),
	Benchmark04_t3171460131::get_offset_of_MinPointSize_3(),
	Benchmark04_t3171460131::get_offset_of_MaxPointSize_4(),
	Benchmark04_t3171460131::get_offset_of_Steps_5(),
	Benchmark04_t3171460131::get_offset_of_m_Transform_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (CameraController_t766129913), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[25] = 
{
	CameraController_t766129913::get_offset_of_cameraTransform_2(),
	CameraController_t766129913::get_offset_of_dummyTarget_3(),
	CameraController_t766129913::get_offset_of_CameraTarget_4(),
	CameraController_t766129913::get_offset_of_FollowDistance_5(),
	CameraController_t766129913::get_offset_of_MaxFollowDistance_6(),
	CameraController_t766129913::get_offset_of_MinFollowDistance_7(),
	CameraController_t766129913::get_offset_of_ElevationAngle_8(),
	CameraController_t766129913::get_offset_of_MaxElevationAngle_9(),
	CameraController_t766129913::get_offset_of_MinElevationAngle_10(),
	CameraController_t766129913::get_offset_of_OrbitalAngle_11(),
	CameraController_t766129913::get_offset_of_CameraMode_12(),
	CameraController_t766129913::get_offset_of_MovementSmoothing_13(),
	CameraController_t766129913::get_offset_of_RotationSmoothing_14(),
	CameraController_t766129913::get_offset_of_previousSmoothing_15(),
	CameraController_t766129913::get_offset_of_MovementSmoothingValue_16(),
	CameraController_t766129913::get_offset_of_RotationSmoothingValue_17(),
	CameraController_t766129913::get_offset_of_MoveSensitivity_18(),
	CameraController_t766129913::get_offset_of_currentVelocity_19(),
	CameraController_t766129913::get_offset_of_desiredPosition_20(),
	CameraController_t766129913::get_offset_of_mouseX_21(),
	CameraController_t766129913::get_offset_of_mouseY_22(),
	CameraController_t766129913::get_offset_of_moveVector_23(),
	CameraController_t766129913::get_offset_of_mouseWheel_24(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (CameraModes_t2188281734)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2834[4] = 
{
	CameraModes_t2188281734::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (ChatController_t2669781690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[3] = 
{
	ChatController_t2669781690::get_offset_of_TMP_ChatInput_2(),
	ChatController_t2669781690::get_offset_of_TMP_ChatOutput_3(),
	ChatController_t2669781690::get_offset_of_ChatScrollbar_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (EnvMapAnimator_t1635389402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[3] = 
{
	EnvMapAnimator_t1635389402::get_offset_of_RotationSpeeds_2(),
	EnvMapAnimator_t1635389402::get_offset_of_m_textMeshPro_3(),
	EnvMapAnimator_t1635389402::get_offset_of_m_material_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (U3CStartU3Ec__Iterator0_t110035792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[5] = 
{
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U3CmatrixU3E__0_0(),
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t110035792::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (ObjectSpin_t3410945885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[10] = 
{
	ObjectSpin_t3410945885::get_offset_of_SpinSpeed_2(),
	ObjectSpin_t3410945885::get_offset_of_RotationRange_3(),
	ObjectSpin_t3410945885::get_offset_of_m_transform_4(),
	ObjectSpin_t3410945885::get_offset_of_m_time_5(),
	ObjectSpin_t3410945885::get_offset_of_m_prevPOS_6(),
	ObjectSpin_t3410945885::get_offset_of_m_initial_Rotation_7(),
	ObjectSpin_t3410945885::get_offset_of_m_initial_Position_8(),
	ObjectSpin_t3410945885::get_offset_of_m_lightColor_9(),
	ObjectSpin_t3410945885::get_offset_of_frames_10(),
	ObjectSpin_t3410945885::get_offset_of_Motion_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (MotionType_t2454277493)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2839[4] = 
{
	MotionType_t2454277493::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (ShaderPropAnimator_t2679013775), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[4] = 
{
	ShaderPropAnimator_t2679013775::get_offset_of_m_Renderer_2(),
	ShaderPropAnimator_t2679013775::get_offset_of_m_Material_3(),
	ShaderPropAnimator_t2679013775::get_offset_of_GlowCurve_4(),
	ShaderPropAnimator_t2679013775::get_offset_of_m_frame_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (U3CAnimatePropertiesU3Ec__Iterator0_t35148318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[5] = 
{
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U3CglowPowerU3E__1_0(),
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U24this_1(),
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U24current_2(),
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U24disposing_3(),
	U3CAnimatePropertiesU3Ec__Iterator0_t35148318::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (SimpleScript_t767280347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[3] = 
{
	SimpleScript_t767280347::get_offset_of_m_textMeshPro_2(),
	0,
	SimpleScript_t767280347::get_offset_of_m_frame_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (SkewTextExample_t3378890949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[4] = 
{
	SkewTextExample_t3378890949::get_offset_of_m_TextComponent_2(),
	SkewTextExample_t3378890949::get_offset_of_VertexCurve_3(),
	SkewTextExample_t3378890949::get_offset_of_CurveScale_4(),
	SkewTextExample_t3378890949::get_offset_of_ShearAmount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (U3CWarpTextU3Ec__Iterator0_t1799541603), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[13] = 
{
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3Cold_ShearValueU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3Cold_curveU3E__0_2(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CtextInfoU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CboundsMinXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CboundsMaxXU3E__1_6(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CverticesU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U3CmatrixU3E__2_8(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U24this_9(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U24current_10(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U24disposing_11(),
	U3CWarpTextU3Ec__Iterator0_t1799541603::get_offset_of_U24PC_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (TeleType_t2513439854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[3] = 
{
	TeleType_t2513439854::get_offset_of_label01_2(),
	TeleType_t2513439854::get_offset_of_label02_3(),
	TeleType_t2513439854::get_offset_of_m_textMeshPro_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (U3CStartU3Ec__Iterator0_t189980609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[7] = 
{
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U3CtotalVisibleCharactersU3E__0_0(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U3CcounterU3E__0_1(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U3CvisibleCountU3E__0_2(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t189980609::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (TextConsoleSimulator_t2207663326), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[2] = 
{
	TextConsoleSimulator_t2207663326::get_offset_of_m_TextComponent_2(),
	TextConsoleSimulator_t2207663326::get_offset_of_hasTextChanged_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (U3CRevealCharactersU3Ec__Iterator0_t1407882744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[8] = 
{
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_textComponent_0(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U3CtextInfoU3E__0_1(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U3CvisibleCountU3E__0_3(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U24this_4(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U24current_5(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U24disposing_6(),
	U3CRevealCharactersU3Ec__Iterator0_t1407882744::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (U3CRevealWordsU3Ec__Iterator1_t2877888826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2849[9] = 
{
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_textComponent_0(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CtotalWordCountU3E__0_1(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CtotalVisibleCharactersU3E__0_2(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CcounterU3E__0_3(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CcurrentWordU3E__0_4(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U3CvisibleCountU3E__0_5(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U24current_6(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U24disposing_7(),
	U3CRevealWordsU3Ec__Iterator1_t2877888826::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (TextMeshProFloatingText_t6181308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2850[10] = 
{
	TextMeshProFloatingText_t6181308::get_offset_of_TheFont_2(),
	TextMeshProFloatingText_t6181308::get_offset_of_m_floatingText_3(),
	TextMeshProFloatingText_t6181308::get_offset_of_m_textMeshPro_4(),
	TextMeshProFloatingText_t6181308::get_offset_of_m_textMesh_5(),
	TextMeshProFloatingText_t6181308::get_offset_of_m_transform_6(),
	TextMeshProFloatingText_t6181308::get_offset_of_m_floatingText_Transform_7(),
	TextMeshProFloatingText_t6181308::get_offset_of_m_cameraTransform_8(),
	TextMeshProFloatingText_t6181308::get_offset_of_lastPOS_9(),
	TextMeshProFloatingText_t6181308::get_offset_of_lastRotation_10(),
	TextMeshProFloatingText_t6181308::get_offset_of_SpawnType_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[11] = 
{
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U3CfadeDurationU3E__0_6(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U24this_7(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U24current_8(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U24disposing_9(),
	U3CDisplayTextMeshProFloatingTextU3Ec__Iterator0_t3989756759::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[12] = 
{
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U3CCountDurationU3E__0_0(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U3Cstarting_CountU3E__0_1(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U3Ccurrent_CountU3E__0_2(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U3Cstart_posU3E__0_3(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U3Cstart_colorU3E__0_4(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U3CalphaU3E__0_5(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U3Cint_counterU3E__0_6(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U3CfadeDurationU3E__0_7(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U24this_8(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U24current_9(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U24disposing_10(),
	U3CDisplayTextMeshFloatingTextU3Ec__Iterator1_t1065331755::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { sizeof (TextMeshSpawner_t3794282288), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[4] = 
{
	TextMeshSpawner_t3794282288::get_offset_of_SpawnType_2(),
	TextMeshSpawner_t3794282288::get_offset_of_NumberOfNPC_3(),
	TextMeshSpawner_t3794282288::get_offset_of_TheFont_4(),
	TextMeshSpawner_t3794282288::get_offset_of_floatingText_Script_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (TMP_DigitValidator_t3346203809), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (TMP_ExampleScript_01_t2316744565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[5] = 
{
	TMP_ExampleScript_01_t2316744565::get_offset_of_ObjectType_2(),
	TMP_ExampleScript_01_t2316744565::get_offset_of_isStatic_3(),
	TMP_ExampleScript_01_t2316744565::get_offset_of_m_text_4(),
	0,
	TMP_ExampleScript_01_t2316744565::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (objectType_t309451146)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2856[3] = 
{
	objectType_t309451146::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (TMP_FrameRateCounter_t2302739001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[10] = 
{
	TMP_FrameRateCounter_t2302739001::get_offset_of_UpdateInterval_2(),
	TMP_FrameRateCounter_t2302739001::get_offset_of_m_LastInterval_3(),
	TMP_FrameRateCounter_t2302739001::get_offset_of_m_Frames_4(),
	TMP_FrameRateCounter_t2302739001::get_offset_of_AnchorPosition_5(),
	TMP_FrameRateCounter_t2302739001::get_offset_of_htmlColorTag_6(),
	0,
	TMP_FrameRateCounter_t2302739001::get_offset_of_m_TextMeshPro_8(),
	TMP_FrameRateCounter_t2302739001::get_offset_of_m_frameCounter_transform_9(),
	TMP_FrameRateCounter_t2302739001::get_offset_of_m_camera_10(),
	TMP_FrameRateCounter_t2302739001::get_offset_of_last_AnchorPosition_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (FpsCounterAnchorPositions_t2802507505)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2858[5] = 
{
	FpsCounterAnchorPositions_t2802507505::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (TMP_PhoneNumberValidator_t3703967929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (TMP_TextEventCheck_t3099661323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2860[1] = 
{
	TMP_TextEventCheck_t3099661323::get_offset_of_TextEventHandler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (TMP_TextEventHandler_t1517844021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[11] = 
{
	TMP_TextEventHandler_t1517844021::get_offset_of_m_OnCharacterSelection_2(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_OnWordSelection_3(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_OnLineSelection_4(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_OnLinkSelection_5(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_TextComponent_6(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_Camera_7(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_Canvas_8(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_selectedLink_9(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_lastCharIndex_10(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_lastWordIndex_11(),
	TMP_TextEventHandler_t1517844021::get_offset_of_m_lastLineIndex_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (CharacterSelectionEvent_t2887241789), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (WordSelectionEvent_t2871480376), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (LineSelectionEvent_t3265414486), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (LinkSelectionEvent_t3735310088), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (TMP_TextInfoDebugTool_t548826358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[9] = 
{
	TMP_TextInfoDebugTool_t548826358::get_offset_of_ShowCharacters_2(),
	TMP_TextInfoDebugTool_t548826358::get_offset_of_ShowWords_3(),
	TMP_TextInfoDebugTool_t548826358::get_offset_of_ShowLinks_4(),
	TMP_TextInfoDebugTool_t548826358::get_offset_of_ShowLines_5(),
	TMP_TextInfoDebugTool_t548826358::get_offset_of_ShowMeshBounds_6(),
	TMP_TextInfoDebugTool_t548826358::get_offset_of_ShowTextBounds_7(),
	TMP_TextInfoDebugTool_t548826358::get_offset_of_ObjectStats_8(),
	TMP_TextInfoDebugTool_t548826358::get_offset_of_m_TextComponent_9(),
	TMP_TextInfoDebugTool_t548826358::get_offset_of_m_Transform_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (TMP_TextSelector_A_t2419327982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[6] = 
{
	TMP_TextSelector_A_t2419327982::get_offset_of_m_TextMeshPro_2(),
	TMP_TextSelector_A_t2419327982::get_offset_of_m_Camera_3(),
	TMP_TextSelector_A_t2419327982::get_offset_of_m_isHoveringObject_4(),
	TMP_TextSelector_A_t2419327982::get_offset_of_m_selectedLink_5(),
	TMP_TextSelector_A_t2419327982::get_offset_of_m_lastCharIndex_6(),
	TMP_TextSelector_A_t2419327982::get_offset_of_m_lastWordIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (TMP_TextSelector_B_t2419327983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[14] = 
{
	TMP_TextSelector_B_t2419327983::get_offset_of_TextPopup_Prefab_01_2(),
	TMP_TextSelector_B_t2419327983::get_offset_of_m_TextPopup_RectTransform_3(),
	TMP_TextSelector_B_t2419327983::get_offset_of_m_TextPopup_TMPComponent_4(),
	0,
	0,
	TMP_TextSelector_B_t2419327983::get_offset_of_m_TextMeshPro_7(),
	TMP_TextSelector_B_t2419327983::get_offset_of_m_Canvas_8(),
	TMP_TextSelector_B_t2419327983::get_offset_of_m_Camera_9(),
	TMP_TextSelector_B_t2419327983::get_offset_of_isHoveringObject_10(),
	TMP_TextSelector_B_t2419327983::get_offset_of_m_selectedWord_11(),
	TMP_TextSelector_B_t2419327983::get_offset_of_m_selectedLink_12(),
	TMP_TextSelector_B_t2419327983::get_offset_of_m_lastIndex_13(),
	TMP_TextSelector_B_t2419327983::get_offset_of_m_matrix_14(),
	TMP_TextSelector_B_t2419327983::get_offset_of_m_cachedMeshInfoVertexData_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (TMP_UiFrameRateCounter_t4016109979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[9] = 
{
	TMP_UiFrameRateCounter_t4016109979::get_offset_of_UpdateInterval_2(),
	TMP_UiFrameRateCounter_t4016109979::get_offset_of_m_LastInterval_3(),
	TMP_UiFrameRateCounter_t4016109979::get_offset_of_m_Frames_4(),
	TMP_UiFrameRateCounter_t4016109979::get_offset_of_AnchorPosition_5(),
	TMP_UiFrameRateCounter_t4016109979::get_offset_of_htmlColorTag_6(),
	0,
	TMP_UiFrameRateCounter_t4016109979::get_offset_of_m_TextMeshPro_8(),
	TMP_UiFrameRateCounter_t4016109979::get_offset_of_m_frameCounter_transform_9(),
	TMP_UiFrameRateCounter_t4016109979::get_offset_of_last_AnchorPosition_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (FpsCounterAnchorPositions_t3655978865)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2870[5] = 
{
	FpsCounterAnchorPositions_t3655978865::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (TMPro_InstructionOverlay_t4177302973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[6] = 
{
	TMPro_InstructionOverlay_t4177302973::get_offset_of_AnchorPosition_2(),
	0,
	TMPro_InstructionOverlay_t4177302973::get_offset_of_m_TextMeshPro_4(),
	TMPro_InstructionOverlay_t4177302973::get_offset_of_m_textContainer_5(),
	TMPro_InstructionOverlay_t4177302973::get_offset_of_m_frameCounter_transform_6(),
	TMPro_InstructionOverlay_t4177302973::get_offset_of_m_camera_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (FpsCounterAnchorPositions_t3015313749)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2872[5] = 
{
	FpsCounterAnchorPositions_t3015313749::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (VertexColorCycler_t3471086701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[1] = 
{
	VertexColorCycler_t3471086701::get_offset_of_m_TextComponent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[11] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U3CcurrentCharacterU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U3Cc0U3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U3CmaterialIndexU3E__1_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U3CnewVertexColorsU3E__1_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U3CvertexIndexU3E__1_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U24this_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U24current_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U24disposing_9(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t1112749575::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (VertexJitter_t3970559362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[5] = 
{
	VertexJitter_t3970559362::get_offset_of_AngleMultiplier_2(),
	VertexJitter_t3970559362::get_offset_of_SpeedMultiplier_3(),
	VertexJitter_t3970559362::get_offset_of_CurveScale_4(),
	VertexJitter_t3970559362::get_offset_of_m_TextComponent_5(),
	VertexJitter_t3970559362::get_offset_of_hasTextChanged_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (VertexAnim_t2147777005)+ sizeof (RuntimeObject), sizeof(VertexAnim_t2147777005 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2876[3] = 
{
	VertexAnim_t2147777005::get_offset_of_angleRange_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t2147777005::get_offset_of_angle_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexAnim_t2147777005::get_offset_of_speed_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2877[10] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U3CloopCountU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U3CvertexAnimU3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U3CcachedMeshInfoU3E__0_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U3CcharacterCountU3E__1_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U3CmatrixU3E__2_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U24this_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U24current_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U24disposing_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t2947903458::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (VertexShakeA_t3019979321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[6] = 
{
	VertexShakeA_t3019979321::get_offset_of_AngleMultiplier_2(),
	VertexShakeA_t3019979321::get_offset_of_SpeedMultiplier_3(),
	VertexShakeA_t3019979321::get_offset_of_ScaleMultiplier_4(),
	VertexShakeA_t3019979321::get_offset_of_RotationMultiplier_5(),
	VertexShakeA_t3019979321::get_offset_of_m_TextComponent_6(),
	VertexShakeA_t3019979321::get_offset_of_hasTextChanged_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[9] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737::get_offset_of_U3CcopyOfVerticesU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737::get_offset_of_U3CcharacterCountU3E__1_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737::get_offset_of_U3ClineCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t3033053737::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (VertexShakeB_t3019979320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[5] = 
{
	VertexShakeB_t3019979320::get_offset_of_AngleMultiplier_2(),
	VertexShakeB_t3019979320::get_offset_of_SpeedMultiplier_3(),
	VertexShakeB_t3019979320::get_offset_of_CurveScale_4(),
	VertexShakeB_t3019979320::get_offset_of_m_TextComponent_5(),
	VertexShakeB_t3019979320::get_offset_of_hasTextChanged_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[9] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720::get_offset_of_U3CcopyOfVerticesU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720::get_offset_of_U3CcharacterCountU3E__1_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720::get_offset_of_U3ClineCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t4210043720::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (VertexZoom_t3463934435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[5] = 
{
	VertexZoom_t3463934435::get_offset_of_AngleMultiplier_2(),
	VertexZoom_t3463934435::get_offset_of_SpeedMultiplier_3(),
	VertexZoom_t3463934435::get_offset_of_CurveScale_4(),
	VertexZoom_t3463934435::get_offset_of_m_TextComponent_5(),
	VertexZoom_t3463934435::get_offset_of_hasTextChanged_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (U3CAnimateVertexColorsU3Ec__Iterator0_t76635731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[10] = 
{
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U3CtextInfoU3E__0_0(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U3CcachedMeshInfoVertexDataU3E__0_1(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U3CscaleSortingOrderU3E__0_2(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U3CmatrixU3E__2_4(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U24this_5(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U24current_6(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U24disposing_7(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U24PC_8(),
	U3CAnimateVertexColorsU3Ec__Iterator0_t76635731::get_offset_of_U24locvar0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2884[2] = 
{
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289::get_offset_of_modifiedCharScale_0(),
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t3069048289::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (WarpTextExample_t250871869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[5] = 
{
	WarpTextExample_t250871869::get_offset_of_m_TextComponent_2(),
	WarpTextExample_t250871869::get_offset_of_VertexCurve_3(),
	WarpTextExample_t250871869::get_offset_of_AngleMultiplier_4(),
	WarpTextExample_t250871869::get_offset_of_SpeedMultiplier_5(),
	WarpTextExample_t250871869::get_offset_of_CurveScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (U3CWarpTextU3Ec__Iterator0_t2736974085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[12] = 
{
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U3Cold_curveU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U3CtextInfoU3E__1_2(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U3CboundsMinXU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U3CboundsMaxXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U3CverticesU3E__2_6(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U3CmatrixU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U24this_8(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U24current_9(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U24disposing_10(),
	U3CWarpTextU3Ec__Iterator0_t2736974085::get_offset_of_U24PC_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
