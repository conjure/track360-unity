﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct List_1_t2526446185;
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>
struct IndexedSet_1_t3055593353;
// Amazon.V4ClientSection
struct V4ClientSection_t2149061698;
// Amazon.Runtime.AmazonServiceCallback`2<Amazon.S3.Model.GetObjectRequest,Amazon.S3.Model.GetObjectResponse>
struct AmazonServiceCallback_2_t3959979905;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t1976155184;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_t701624289;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2020713228;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t2262346586;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t890994926;
// Amazon.Runtime.AmazonServiceCallback`2<Amazon.S3.Model.ListObjectsRequest,Amazon.S3.Model.ListObjectsResponse>
struct AmazonServiceCallback_2_t2696981651;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t3928470916;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t3435657708;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t2260664863;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t2213949596;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t3246763936;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t1074830945;
// Amazon.Util.Internal.ElementInformation
struct ElementInformation_t3988909444;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.String
struct String_t;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;
// Amazon.Runtime.Internal.RuntimePipeline
struct RuntimePipeline_t3355992556;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t4078933728;
// Amazon.Runtime.PreRequestEventHandler
struct PreRequestEventHandler_t345425304;
// Amazon.Runtime.RequestEventHandler
struct RequestEventHandler_t2213783891;
// Amazon.Runtime.ResponseEventHandler
struct ResponseEventHandler_t3870676125;
// Amazon.Runtime.ExceptionEventHandler
struct ExceptionEventHandler_t3236465969;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>
struct Dictionary_2_t3557729749;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Collider
struct Collider_t3497673348;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t243638650;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1612828713;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.Material
struct Material_t193706927;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1214023521;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4176517891;
// Amazon.S3.Model.DeleteObjectsResponse
struct DeleteObjectsResponse_t4016776830;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t2719087314;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t3529018992;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t3345875600;
// System.Predicate`1<UnityEngine.UI.Toggle>
struct Predicate_1_t2419724583;
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct Func_2_t2318645467;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t859513320;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t3244928895;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2665681875;
// UnityEngine.UI.RectangularVertexClipper
struct RectangularVertexClipper_t3349113845;
// System.Collections.Generic.HashSet`1<UnityEngine.UI.IClippable>
struct HashSet_1_t274736911;
// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D>
struct List_1_t525307096;
// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_t1794825321;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t2111116400;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.ToggleGroup
struct ToggleGroup_t1030026315;
// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t1896830814;
// UnityEngine.UI.FontData
struct FontData_t2614388407;
// UnityEngine.TextGenerator
struct TextGenerator_t647235000;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// UnityEngine.Texture
struct Texture_t2243626319;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3783534237_H
#define U3CMODULEU3E_T3783534237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534237 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534237_H
#ifndef RECTANGULARVERTEXCLIPPER_T3349113845_H
#define RECTANGULARVERTEXCLIPPER_T3349113845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RectangularVertexClipper
struct  RectangularVertexClipper_t3349113845  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_WorldCorners
	Vector3U5BU5D_t1172311765* ___m_WorldCorners_0;
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_CanvasCorners
	Vector3U5BU5D_t1172311765* ___m_CanvasCorners_1;

public:
	inline static int32_t get_offset_of_m_WorldCorners_0() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t3349113845, ___m_WorldCorners_0)); }
	inline Vector3U5BU5D_t1172311765* get_m_WorldCorners_0() const { return ___m_WorldCorners_0; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_WorldCorners_0() { return &___m_WorldCorners_0; }
	inline void set_m_WorldCorners_0(Vector3U5BU5D_t1172311765* value)
	{
		___m_WorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorldCorners_0), value);
	}

	inline static int32_t get_offset_of_m_CanvasCorners_1() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t3349113845, ___m_CanvasCorners_1)); }
	inline Vector3U5BU5D_t1172311765* get_m_CanvasCorners_1() const { return ___m_CanvasCorners_1; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_CanvasCorners_1() { return &___m_CanvasCorners_1; }
	inline void set_m_CanvasCorners_1(Vector3U5BU5D_t1172311765* value)
	{
		___m_CanvasCorners_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasCorners_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGULARVERTEXCLIPPER_T3349113845_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3228926346_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3228926346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t3349966182 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___rectTransform_0)); }
	inline RectTransform_t3349966182 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3349966182 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3349966182 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3228926346_H
#ifndef STENCILMATERIAL_T1630303189_H
#define STENCILMATERIAL_T1630303189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.StencilMaterial
struct  StencilMaterial_t1630303189  : public RuntimeObject
{
public:

public:
};

struct StencilMaterial_t1630303189_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.StencilMaterial/MatEntry> UnityEngine.UI.StencilMaterial::m_List
	List_1_t2526446185 * ___m_List_0;

public:
	inline static int32_t get_offset_of_m_List_0() { return static_cast<int32_t>(offsetof(StencilMaterial_t1630303189_StaticFields, ___m_List_0)); }
	inline List_1_t2526446185 * get_m_List_0() const { return ___m_List_0; }
	inline List_1_t2526446185 ** get_address_of_m_List_0() { return &___m_List_0; }
	inline void set_m_List_0(List_1_t2526446185 * value)
	{
		___m_List_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_List_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STENCILMATERIAL_T1630303189_H
#ifndef SETPROPERTYUTILITY_T4019374597_H
#define SETPROPERTYUTILITY_T4019374597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SetPropertyUtility
struct  SetPropertyUtility_t4019374597  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROPERTYUTILITY_T4019374597_H
#ifndef CLIPPING_T223789604_H
#define CLIPPING_T223789604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Clipping
struct  Clipping_t223789604  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_T223789604_H
#ifndef CLIPPERREGISTRY_T1349564894_H
#define CLIPPERREGISTRY_T1349564894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ClipperRegistry
struct  ClipperRegistry_t1349564894  : public RuntimeObject
{
public:
	// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper> UnityEngine.UI.ClipperRegistry::m_Clippers
	IndexedSet_1_t3055593353 * ___m_Clippers_1;

public:
	inline static int32_t get_offset_of_m_Clippers_1() { return static_cast<int32_t>(offsetof(ClipperRegistry_t1349564894, ___m_Clippers_1)); }
	inline IndexedSet_1_t3055593353 * get_m_Clippers_1() const { return ___m_Clippers_1; }
	inline IndexedSet_1_t3055593353 ** get_address_of_m_Clippers_1() { return &___m_Clippers_1; }
	inline void set_m_Clippers_1(IndexedSet_1_t3055593353 * value)
	{
		___m_Clippers_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clippers_1), value);
	}
};

struct ClipperRegistry_t1349564894_StaticFields
{
public:
	// UnityEngine.UI.ClipperRegistry UnityEngine.UI.ClipperRegistry::s_Instance
	ClipperRegistry_t1349564894 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(ClipperRegistry_t1349564894_StaticFields, ___s_Instance_0)); }
	inline ClipperRegistry_t1349564894 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline ClipperRegistry_t1349564894 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(ClipperRegistry_t1349564894 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPERREGISTRY_T1349564894_H
#ifndef V4CLIENTSECTIONROOT_T1193884458_H
#define V4CLIENTSECTIONROOT_T1193884458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.V4ClientSectionRoot
struct  V4ClientSectionRoot_t1193884458  : public RuntimeObject
{
public:
	// Amazon.V4ClientSection Amazon.V4ClientSectionRoot::<S3>k__BackingField
	V4ClientSection_t2149061698 * ___U3CS3U3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CS3U3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(V4ClientSectionRoot_t1193884458, ___U3CS3U3Ek__BackingField_0)); }
	inline V4ClientSection_t2149061698 * get_U3CS3U3Ek__BackingField_0() const { return ___U3CS3U3Ek__BackingField_0; }
	inline V4ClientSection_t2149061698 ** get_address_of_U3CS3U3Ek__BackingField_0() { return &___U3CS3U3Ek__BackingField_0; }
	inline void set_U3CS3U3Ek__BackingField_0(V4ClientSection_t2149061698 * value)
	{
		___U3CS3U3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CS3U3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // V4CLIENTSECTIONROOT_T1193884458_H
#ifndef U3CU3EC__DISPLAYCLASS85_0_T3636365575_H
#define U3CU3EC__DISPLAYCLASS85_0_T3636365575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.AmazonS3Client/<>c__DisplayClass85_0
struct  U3CU3Ec__DisplayClass85_0_t3636365575  : public RuntimeObject
{
public:
	// Amazon.Runtime.AmazonServiceCallback`2<Amazon.S3.Model.GetObjectRequest,Amazon.S3.Model.GetObjectResponse> Amazon.S3.AmazonS3Client/<>c__DisplayClass85_0::callback
	AmazonServiceCallback_2_t3959979905 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass85_0_t3636365575, ___callback_0)); }
	inline AmazonServiceCallback_2_t3959979905 * get_callback_0() const { return ___callback_0; }
	inline AmazonServiceCallback_2_t3959979905 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(AmazonServiceCallback_2_t3959979905 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS85_0_T3636365575_H
#ifndef BASEVERTEXEFFECT_T2504093552_H
#define BASEVERTEXEFFECT_T2504093552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2504093552  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2504093552_H
#ifndef AWSCONFIGSS3_T3080748296_H
#define AWSCONFIGSS3_T3080748296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSConfigsS3
struct  AWSConfigsS3_t3080748296  : public RuntimeObject
{
public:

public:
};

struct AWSConfigsS3_t3080748296_StaticFields
{
public:
	// System.Boolean Amazon.AWSConfigsS3::_useSignatureVersion4
	bool ____useSignatureVersion4_0;
	// System.Boolean Amazon.AWSConfigsS3::<UseSigV4SetExplicitly>k__BackingField
	bool ___U3CUseSigV4SetExplicitlyU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__useSignatureVersion4_0() { return static_cast<int32_t>(offsetof(AWSConfigsS3_t3080748296_StaticFields, ____useSignatureVersion4_0)); }
	inline bool get__useSignatureVersion4_0() const { return ____useSignatureVersion4_0; }
	inline bool* get_address_of__useSignatureVersion4_0() { return &____useSignatureVersion4_0; }
	inline void set__useSignatureVersion4_0(bool value)
	{
		____useSignatureVersion4_0 = value;
	}

	inline static int32_t get_offset_of_U3CUseSigV4SetExplicitlyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AWSConfigsS3_t3080748296_StaticFields, ___U3CUseSigV4SetExplicitlyU3Ek__BackingField_1)); }
	inline bool get_U3CUseSigV4SetExplicitlyU3Ek__BackingField_1() const { return ___U3CUseSigV4SetExplicitlyU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CUseSigV4SetExplicitlyU3Ek__BackingField_1() { return &___U3CUseSigV4SetExplicitlyU3Ek__BackingField_1; }
	inline void set_U3CUseSigV4SetExplicitlyU3Ek__BackingField_1(bool value)
	{
		___U3CUseSigV4SetExplicitlyU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AWSCONFIGSS3_T3080748296_H
#ifndef LAYOUTUTILITY_T4076838048_H
#define LAYOUTUTILITY_T4076838048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t4076838048  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t4076838048_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache0
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache1
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache2
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache3
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache4
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache5
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache6
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache7
	Func_2_t1976155184 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t4076838048_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t1976155184 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t1976155184 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t1976155184 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T4076838048_H
#ifndef LAYOUTREBUILDER_T2155218138_H
#define LAYOUTREBUILDER_T2155218138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t2155218138  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t3349966182 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138, ___m_ToRebuild_0)); }
	inline RectTransform_t3349966182 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t3349966182 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t3349966182 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t2155218138_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_t701624289 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mg$cache0
	ReapplyDrivenProperties_t2020713228 * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache0
	Predicate_1_t2262346586 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache1
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache2
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache3
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache4
	UnityAction_1_t890994926 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_t701624289 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_t701624289 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_t701624289 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t2020713228 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t2020713228 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t2020713228 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2262346586 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2262346586 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2262346586 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t2155218138_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_t890994926 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_t890994926 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_t890994926 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T2155218138_H
#ifndef U3CU3EC__DISPLAYCLASS105_0_T177521762_H
#define U3CU3EC__DISPLAYCLASS105_0_T177521762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.AmazonS3Client/<>c__DisplayClass105_0
struct  U3CU3Ec__DisplayClass105_0_t177521762  : public RuntimeObject
{
public:
	// Amazon.Runtime.AmazonServiceCallback`2<Amazon.S3.Model.ListObjectsRequest,Amazon.S3.Model.ListObjectsResponse> Amazon.S3.AmazonS3Client/<>c__DisplayClass105_0::callback
	AmazonServiceCallback_2_t2696981651 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass105_0_t177521762, ___callback_0)); }
	inline AmazonServiceCallback_2_t2696981651 * get_callback_0() const { return ___callback_0; }
	inline AmazonServiceCallback_2_t2696981651 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(AmazonServiceCallback_2_t2696981651 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS105_0_T177521762_H
#ifndef REFLECTIONMETHODSCACHE_T3343836395_H
#define REFLECTIONMETHODSCACHE_T3343836395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t3343836395  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t3928470916 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t3435657708 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t2260664863 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t2213949596 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t3246763936 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t1074830945 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___raycast3D_0)); }
	inline Raycast3DCallback_t3928470916 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t3928470916 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t3928470916 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t3435657708 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t3435657708 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t3435657708 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___raycast2D_2)); }
	inline Raycast2DCallback_t2260664863 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t2260664863 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t2260664863 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t2213949596 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t2213949596 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t2213949596 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t3246763936 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t3246763936 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t3246763936 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t1074830945 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t1074830945 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t1074830945 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t3343836395_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t3343836395 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t3343836395_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t3343836395 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t3343836395 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t3343836395 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T3343836395_H
#ifndef CONFIGURATIONELEMENT_T1996355484_H
#define CONFIGURATIONELEMENT_T1996355484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.ConfigurationElement
struct  ConfigurationElement_t1996355484  : public RuntimeObject
{
public:
	// Amazon.Util.Internal.ElementInformation Amazon.Util.Internal.ConfigurationElement::<ElementInformation>k__BackingField
	ElementInformation_t3988909444 * ___U3CElementInformationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CElementInformationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1996355484, ___U3CElementInformationU3Ek__BackingField_0)); }
	inline ElementInformation_t3988909444 * get_U3CElementInformationU3Ek__BackingField_0() const { return ___U3CElementInformationU3Ek__BackingField_0; }
	inline ElementInformation_t3988909444 ** get_address_of_U3CElementInformationU3Ek__BackingField_0() { return &___U3CElementInformationU3Ek__BackingField_0; }
	inline void set_U3CElementInformationU3Ek__BackingField_0(ElementInformation_t3988909444 * value)
	{
		___U3CElementInformationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CElementInformationU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENT_T1996355484_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef AMAZONSERVICECLIENT_T3583134838_H
#define AMAZONSERVICECLIENT_T3583134838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonServiceClient
struct  AmazonServiceClient_t3583134838  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.AmazonServiceClient::_disposed
	bool ____disposed_0;
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.AmazonServiceClient::_logger
	Logger_t2262497814 * ____logger_1;
	// Amazon.Runtime.Internal.RuntimePipeline Amazon.Runtime.AmazonServiceClient::<RuntimePipeline>k__BackingField
	RuntimePipeline_t3355992556 * ___U3CRuntimePipelineU3Ek__BackingField_2;
	// Amazon.Runtime.AWSCredentials Amazon.Runtime.AmazonServiceClient::<Credentials>k__BackingField
	AWSCredentials_t3583921007 * ___U3CCredentialsU3Ek__BackingField_3;
	// Amazon.Runtime.IClientConfig Amazon.Runtime.AmazonServiceClient::<Config>k__BackingField
	RuntimeObject* ___U3CConfigU3Ek__BackingField_4;
	// Amazon.Runtime.PreRequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeMarshallingEvent
	PreRequestEventHandler_t345425304 * ___mBeforeMarshallingEvent_5;
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeRequestEvent
	RequestEventHandler_t2213783891 * ___mBeforeRequestEvent_6;
	// Amazon.Runtime.ResponseEventHandler Amazon.Runtime.AmazonServiceClient::mAfterResponseEvent
	ResponseEventHandler_t3870676125 * ___mAfterResponseEvent_7;
	// Amazon.Runtime.ExceptionEventHandler Amazon.Runtime.AmazonServiceClient::mExceptionEvent
	ExceptionEventHandler_t3236465969 * ___mExceptionEvent_8;
	// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonServiceClient::<Signer>k__BackingField
	AbstractAWSSigner_t2114314031 * ___U3CSignerU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ____logger_1)); }
	inline Logger_t2262497814 * get__logger_1() const { return ____logger_1; }
	inline Logger_t2262497814 ** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(Logger_t2262497814 * value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier((&____logger_1), value);
	}

	inline static int32_t get_offset_of_U3CRuntimePipelineU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CRuntimePipelineU3Ek__BackingField_2)); }
	inline RuntimePipeline_t3355992556 * get_U3CRuntimePipelineU3Ek__BackingField_2() const { return ___U3CRuntimePipelineU3Ek__BackingField_2; }
	inline RuntimePipeline_t3355992556 ** get_address_of_U3CRuntimePipelineU3Ek__BackingField_2() { return &___U3CRuntimePipelineU3Ek__BackingField_2; }
	inline void set_U3CRuntimePipelineU3Ek__BackingField_2(RuntimePipeline_t3355992556 * value)
	{
		___U3CRuntimePipelineU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRuntimePipelineU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CCredentialsU3Ek__BackingField_3)); }
	inline AWSCredentials_t3583921007 * get_U3CCredentialsU3Ek__BackingField_3() const { return ___U3CCredentialsU3Ek__BackingField_3; }
	inline AWSCredentials_t3583921007 ** get_address_of_U3CCredentialsU3Ek__BackingField_3() { return &___U3CCredentialsU3Ek__BackingField_3; }
	inline void set_U3CCredentialsU3Ek__BackingField_3(AWSCredentials_t3583921007 * value)
	{
		___U3CCredentialsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCredentialsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CConfigU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CConfigU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CConfigU3Ek__BackingField_4() const { return ___U3CConfigU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CConfigU3Ek__BackingField_4() { return &___U3CConfigU3Ek__BackingField_4; }
	inline void set_U3CConfigU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CConfigU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_mBeforeMarshallingEvent_5() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mBeforeMarshallingEvent_5)); }
	inline PreRequestEventHandler_t345425304 * get_mBeforeMarshallingEvent_5() const { return ___mBeforeMarshallingEvent_5; }
	inline PreRequestEventHandler_t345425304 ** get_address_of_mBeforeMarshallingEvent_5() { return &___mBeforeMarshallingEvent_5; }
	inline void set_mBeforeMarshallingEvent_5(PreRequestEventHandler_t345425304 * value)
	{
		___mBeforeMarshallingEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___mBeforeMarshallingEvent_5), value);
	}

	inline static int32_t get_offset_of_mBeforeRequestEvent_6() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mBeforeRequestEvent_6)); }
	inline RequestEventHandler_t2213783891 * get_mBeforeRequestEvent_6() const { return ___mBeforeRequestEvent_6; }
	inline RequestEventHandler_t2213783891 ** get_address_of_mBeforeRequestEvent_6() { return &___mBeforeRequestEvent_6; }
	inline void set_mBeforeRequestEvent_6(RequestEventHandler_t2213783891 * value)
	{
		___mBeforeRequestEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBeforeRequestEvent_6), value);
	}

	inline static int32_t get_offset_of_mAfterResponseEvent_7() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mAfterResponseEvent_7)); }
	inline ResponseEventHandler_t3870676125 * get_mAfterResponseEvent_7() const { return ___mAfterResponseEvent_7; }
	inline ResponseEventHandler_t3870676125 ** get_address_of_mAfterResponseEvent_7() { return &___mAfterResponseEvent_7; }
	inline void set_mAfterResponseEvent_7(ResponseEventHandler_t3870676125 * value)
	{
		___mAfterResponseEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___mAfterResponseEvent_7), value);
	}

	inline static int32_t get_offset_of_mExceptionEvent_8() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mExceptionEvent_8)); }
	inline ExceptionEventHandler_t3236465969 * get_mExceptionEvent_8() const { return ___mExceptionEvent_8; }
	inline ExceptionEventHandler_t3236465969 ** get_address_of_mExceptionEvent_8() { return &___mExceptionEvent_8; }
	inline void set_mExceptionEvent_8(ExceptionEventHandler_t3236465969 * value)
	{
		___mExceptionEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___mExceptionEvent_8), value);
	}

	inline static int32_t get_offset_of_U3CSignerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CSignerU3Ek__BackingField_9)); }
	inline AbstractAWSSigner_t2114314031 * get_U3CSignerU3Ek__BackingField_9() const { return ___U3CSignerU3Ek__BackingField_9; }
	inline AbstractAWSSigner_t2114314031 ** get_address_of_U3CSignerU3Ek__BackingField_9() { return &___U3CSignerU3Ek__BackingField_9; }
	inline void set_U3CSignerU3Ek__BackingField_9(AbstractAWSSigner_t2114314031 * value)
	{
		___U3CSignerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSignerU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONSERVICECLIENT_T3583134838_H
#ifndef CONSTANTCLASS_T4000559886_H
#define CONSTANTCLASS_T4000559886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ConstantClass
struct  ConstantClass_t4000559886  : public RuntimeObject
{
public:
	// System.String Amazon.Runtime.ConstantClass::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConstantClass_t4000559886, ___U3CValueU3Ek__BackingField_2)); }
	inline String_t* get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(String_t* value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_2), value);
	}
};

struct ConstantClass_t4000559886_StaticFields
{
public:
	// System.Object Amazon.Runtime.ConstantClass::staticFieldsLock
	RuntimeObject * ___staticFieldsLock_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>> Amazon.Runtime.ConstantClass::staticFields
	Dictionary_2_t3557729749 * ___staticFields_1;

public:
	inline static int32_t get_offset_of_staticFieldsLock_0() { return static_cast<int32_t>(offsetof(ConstantClass_t4000559886_StaticFields, ___staticFieldsLock_0)); }
	inline RuntimeObject * get_staticFieldsLock_0() const { return ___staticFieldsLock_0; }
	inline RuntimeObject ** get_address_of_staticFieldsLock_0() { return &___staticFieldsLock_0; }
	inline void set_staticFieldsLock_0(RuntimeObject * value)
	{
		___staticFieldsLock_0 = value;
		Il2CppCodeGenWriteBarrier((&___staticFieldsLock_0), value);
	}

	inline static int32_t get_offset_of_staticFields_1() { return static_cast<int32_t>(offsetof(ConstantClass_t4000559886_StaticFields, ___staticFields_1)); }
	inline Dictionary_2_t3557729749 * get_staticFields_1() const { return ___staticFields_1; }
	inline Dictionary_2_t3557729749 ** get_address_of_staticFields_1() { return &___staticFields_1; }
	inline void set_staticFields_1(Dictionary_2_t3557729749 * value)
	{
		___staticFields_1 = value;
		Il2CppCodeGenWriteBarrier((&___staticFields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTCLASS_T4000559886_H
#ifndef MASKUTILITIES_T1936577068_H
#define MASKUTILITIES_T1936577068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskUtilities
struct  MaskUtilities_t1936577068  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKUTILITIES_T1936577068_H
#ifndef MISC_T2977957982_H
#define MISC_T2977957982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Misc
struct  Misc_t2977957982  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISC_T2977957982_H
#ifndef U3CCLICKREPEATU3EC__ITERATOR0_T4156771994_H
#define U3CCLICKREPEATU3EC__ITERATOR0_T4156771994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator0
struct  U3CClickRepeatU3Ec__Iterator0_t4156771994  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator0::eventData
	PointerEventData_t1599784723 * ___eventData_0;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator0::$this
	Scrollbar_t3248359358 * ___U24this_1;
	// System.Object UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UnityEngine.UI.Scrollbar/<ClickRepeat>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator0_t4156771994, ___eventData_0)); }
	inline PointerEventData_t1599784723 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t1599784723 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t1599784723 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator0_t4156771994, ___U24this_1)); }
	inline Scrollbar_t3248359358 * get_U24this_1() const { return ___U24this_1; }
	inline Scrollbar_t3248359358 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Scrollbar_t3248359358 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator0_t4156771994, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator0_t4156771994, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CClickRepeatU3Ec__Iterator0_t4156771994, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLICKREPEATU3EC__ITERATOR0_T4156771994_H
#ifndef REPLICATIONSTATUS_T1991002226_H
#define REPLICATIONSTATUS_T1991002226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.ReplicationStatus
struct  ReplicationStatus_t1991002226  : public ConstantClass_t4000559886
{
public:

public:
};

struct ReplicationStatus_t1991002226_StaticFields
{
public:
	// Amazon.S3.ReplicationStatus Amazon.S3.ReplicationStatus::Pending
	ReplicationStatus_t1991002226 * ___Pending_3;
	// Amazon.S3.ReplicationStatus Amazon.S3.ReplicationStatus::Completed
	ReplicationStatus_t1991002226 * ___Completed_4;
	// Amazon.S3.ReplicationStatus Amazon.S3.ReplicationStatus::Replica
	ReplicationStatus_t1991002226 * ___Replica_5;
	// Amazon.S3.ReplicationStatus Amazon.S3.ReplicationStatus::Failed
	ReplicationStatus_t1991002226 * ___Failed_6;

public:
	inline static int32_t get_offset_of_Pending_3() { return static_cast<int32_t>(offsetof(ReplicationStatus_t1991002226_StaticFields, ___Pending_3)); }
	inline ReplicationStatus_t1991002226 * get_Pending_3() const { return ___Pending_3; }
	inline ReplicationStatus_t1991002226 ** get_address_of_Pending_3() { return &___Pending_3; }
	inline void set_Pending_3(ReplicationStatus_t1991002226 * value)
	{
		___Pending_3 = value;
		Il2CppCodeGenWriteBarrier((&___Pending_3), value);
	}

	inline static int32_t get_offset_of_Completed_4() { return static_cast<int32_t>(offsetof(ReplicationStatus_t1991002226_StaticFields, ___Completed_4)); }
	inline ReplicationStatus_t1991002226 * get_Completed_4() const { return ___Completed_4; }
	inline ReplicationStatus_t1991002226 ** get_address_of_Completed_4() { return &___Completed_4; }
	inline void set_Completed_4(ReplicationStatus_t1991002226 * value)
	{
		___Completed_4 = value;
		Il2CppCodeGenWriteBarrier((&___Completed_4), value);
	}

	inline static int32_t get_offset_of_Replica_5() { return static_cast<int32_t>(offsetof(ReplicationStatus_t1991002226_StaticFields, ___Replica_5)); }
	inline ReplicationStatus_t1991002226 * get_Replica_5() const { return ___Replica_5; }
	inline ReplicationStatus_t1991002226 ** get_address_of_Replica_5() { return &___Replica_5; }
	inline void set_Replica_5(ReplicationStatus_t1991002226 * value)
	{
		___Replica_5 = value;
		Il2CppCodeGenWriteBarrier((&___Replica_5), value);
	}

	inline static int32_t get_offset_of_Failed_6() { return static_cast<int32_t>(offsetof(ReplicationStatus_t1991002226_StaticFields, ___Failed_6)); }
	inline ReplicationStatus_t1991002226 * get_Failed_6() const { return ___Failed_6; }
	inline ReplicationStatus_t1991002226 ** get_address_of_Failed_6() { return &___Failed_6; }
	inline void set_Failed_6(ReplicationStatus_t1991002226 * value)
	{
		___Failed_6 = value;
		Il2CppCodeGenWriteBarrier((&___Failed_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLICATIONSTATUS_T1991002226_H
#ifndef REQUESTPAYER_T3334661492_H
#define REQUESTPAYER_T3334661492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.RequestPayer
struct  RequestPayer_t3334661492  : public ConstantClass_t4000559886
{
public:

public:
};

struct RequestPayer_t3334661492_StaticFields
{
public:
	// Amazon.S3.RequestPayer Amazon.S3.RequestPayer::Requester
	RequestPayer_t3334661492 * ___Requester_3;

public:
	inline static int32_t get_offset_of_Requester_3() { return static_cast<int32_t>(offsetof(RequestPayer_t3334661492_StaticFields, ___Requester_3)); }
	inline RequestPayer_t3334661492 * get_Requester_3() const { return ___Requester_3; }
	inline RequestPayer_t3334661492 ** get_address_of_Requester_3() { return &___Requester_3; }
	inline void set_Requester_3(RequestPayer_t3334661492 * value)
	{
		___Requester_3 = value;
		Il2CppCodeGenWriteBarrier((&___Requester_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTPAYER_T3334661492_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef REQUESTCHARGED_T2438105727_H
#define REQUESTCHARGED_T2438105727_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.RequestCharged
struct  RequestCharged_t2438105727  : public ConstantClass_t4000559886
{
public:

public:
};

struct RequestCharged_t2438105727_StaticFields
{
public:
	// Amazon.S3.RequestCharged Amazon.S3.RequestCharged::Requester
	RequestCharged_t2438105727 * ___Requester_3;

public:
	inline static int32_t get_offset_of_Requester_3() { return static_cast<int32_t>(offsetof(RequestCharged_t2438105727_StaticFields, ___Requester_3)); }
	inline RequestCharged_t2438105727 * get_Requester_3() const { return ___Requester_3; }
	inline RequestCharged_t2438105727 ** get_address_of_Requester_3() { return &___Requester_3; }
	inline void set_Requester_3(RequestCharged_t2438105727 * value)
	{
		___Requester_3 = value;
		Il2CppCodeGenWriteBarrier((&___Requester_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTCHARGED_T2438105727_H
#ifndef AMAZONS3CLIENT_T548780019_H
#define AMAZONS3CLIENT_T548780019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.AmazonS3Client
struct  AmazonS3Client_t548780019  : public AmazonServiceClient_t3583134838
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3CLIENT_T548780019_H
#ifndef S3REGION_T723338532_H
#define S3REGION_T723338532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.S3Region
struct  S3Region_t723338532  : public ConstantClass_t4000559886
{
public:

public:
};

struct S3Region_t723338532_StaticFields
{
public:
	// Amazon.S3.S3Region Amazon.S3.S3Region::US
	S3Region_t723338532 * ___US_3;
	// Amazon.S3.S3Region Amazon.S3.S3Region::USE2
	S3Region_t723338532 * ___USE2_4;
	// Amazon.S3.S3Region Amazon.S3.S3Region::EU
	S3Region_t723338532 * ___EU_5;
	// Amazon.S3.S3Region Amazon.S3.S3Region::EUW1
	S3Region_t723338532 * ___EUW1_6;
	// Amazon.S3.S3Region Amazon.S3.S3Region::EUW2
	S3Region_t723338532 * ___EUW2_7;
	// Amazon.S3.S3Region Amazon.S3.S3Region::EUW3
	S3Region_t723338532 * ___EUW3_8;
	// Amazon.S3.S3Region Amazon.S3.S3Region::EUC1
	S3Region_t723338532 * ___EUC1_9;
	// Amazon.S3.S3Region Amazon.S3.S3Region::USW1
	S3Region_t723338532 * ___USW1_10;
	// Amazon.S3.S3Region Amazon.S3.S3Region::USW2
	S3Region_t723338532 * ___USW2_11;
	// Amazon.S3.S3Region Amazon.S3.S3Region::GOVW1
	S3Region_t723338532 * ___GOVW1_12;
	// Amazon.S3.S3Region Amazon.S3.S3Region::APS1
	S3Region_t723338532 * ___APS1_13;
	// Amazon.S3.S3Region Amazon.S3.S3Region::APS2
	S3Region_t723338532 * ___APS2_14;
	// Amazon.S3.S3Region Amazon.S3.S3Region::APN1
	S3Region_t723338532 * ___APN1_15;
	// Amazon.S3.S3Region Amazon.S3.S3Region::APN2
	S3Region_t723338532 * ___APN2_16;
	// Amazon.S3.S3Region Amazon.S3.S3Region::APS3
	S3Region_t723338532 * ___APS3_17;
	// Amazon.S3.S3Region Amazon.S3.S3Region::SAE1
	S3Region_t723338532 * ___SAE1_18;
	// Amazon.S3.S3Region Amazon.S3.S3Region::CN1
	S3Region_t723338532 * ___CN1_19;
	// Amazon.S3.S3Region Amazon.S3.S3Region::CNW1
	S3Region_t723338532 * ___CNW1_20;
	// Amazon.S3.S3Region Amazon.S3.S3Region::CAN1
	S3Region_t723338532 * ___CAN1_21;
	// Amazon.S3.S3Region Amazon.S3.S3Region::SFO
	S3Region_t723338532 * ___SFO_22;
	// Amazon.S3.S3Region Amazon.S3.S3Region::CN
	S3Region_t723338532 * ___CN_23;
	// Amazon.S3.S3Region Amazon.S3.S3Region::GOV
	S3Region_t723338532 * ___GOV_24;

public:
	inline static int32_t get_offset_of_US_3() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___US_3)); }
	inline S3Region_t723338532 * get_US_3() const { return ___US_3; }
	inline S3Region_t723338532 ** get_address_of_US_3() { return &___US_3; }
	inline void set_US_3(S3Region_t723338532 * value)
	{
		___US_3 = value;
		Il2CppCodeGenWriteBarrier((&___US_3), value);
	}

	inline static int32_t get_offset_of_USE2_4() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___USE2_4)); }
	inline S3Region_t723338532 * get_USE2_4() const { return ___USE2_4; }
	inline S3Region_t723338532 ** get_address_of_USE2_4() { return &___USE2_4; }
	inline void set_USE2_4(S3Region_t723338532 * value)
	{
		___USE2_4 = value;
		Il2CppCodeGenWriteBarrier((&___USE2_4), value);
	}

	inline static int32_t get_offset_of_EU_5() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___EU_5)); }
	inline S3Region_t723338532 * get_EU_5() const { return ___EU_5; }
	inline S3Region_t723338532 ** get_address_of_EU_5() { return &___EU_5; }
	inline void set_EU_5(S3Region_t723338532 * value)
	{
		___EU_5 = value;
		Il2CppCodeGenWriteBarrier((&___EU_5), value);
	}

	inline static int32_t get_offset_of_EUW1_6() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___EUW1_6)); }
	inline S3Region_t723338532 * get_EUW1_6() const { return ___EUW1_6; }
	inline S3Region_t723338532 ** get_address_of_EUW1_6() { return &___EUW1_6; }
	inline void set_EUW1_6(S3Region_t723338532 * value)
	{
		___EUW1_6 = value;
		Il2CppCodeGenWriteBarrier((&___EUW1_6), value);
	}

	inline static int32_t get_offset_of_EUW2_7() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___EUW2_7)); }
	inline S3Region_t723338532 * get_EUW2_7() const { return ___EUW2_7; }
	inline S3Region_t723338532 ** get_address_of_EUW2_7() { return &___EUW2_7; }
	inline void set_EUW2_7(S3Region_t723338532 * value)
	{
		___EUW2_7 = value;
		Il2CppCodeGenWriteBarrier((&___EUW2_7), value);
	}

	inline static int32_t get_offset_of_EUW3_8() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___EUW3_8)); }
	inline S3Region_t723338532 * get_EUW3_8() const { return ___EUW3_8; }
	inline S3Region_t723338532 ** get_address_of_EUW3_8() { return &___EUW3_8; }
	inline void set_EUW3_8(S3Region_t723338532 * value)
	{
		___EUW3_8 = value;
		Il2CppCodeGenWriteBarrier((&___EUW3_8), value);
	}

	inline static int32_t get_offset_of_EUC1_9() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___EUC1_9)); }
	inline S3Region_t723338532 * get_EUC1_9() const { return ___EUC1_9; }
	inline S3Region_t723338532 ** get_address_of_EUC1_9() { return &___EUC1_9; }
	inline void set_EUC1_9(S3Region_t723338532 * value)
	{
		___EUC1_9 = value;
		Il2CppCodeGenWriteBarrier((&___EUC1_9), value);
	}

	inline static int32_t get_offset_of_USW1_10() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___USW1_10)); }
	inline S3Region_t723338532 * get_USW1_10() const { return ___USW1_10; }
	inline S3Region_t723338532 ** get_address_of_USW1_10() { return &___USW1_10; }
	inline void set_USW1_10(S3Region_t723338532 * value)
	{
		___USW1_10 = value;
		Il2CppCodeGenWriteBarrier((&___USW1_10), value);
	}

	inline static int32_t get_offset_of_USW2_11() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___USW2_11)); }
	inline S3Region_t723338532 * get_USW2_11() const { return ___USW2_11; }
	inline S3Region_t723338532 ** get_address_of_USW2_11() { return &___USW2_11; }
	inline void set_USW2_11(S3Region_t723338532 * value)
	{
		___USW2_11 = value;
		Il2CppCodeGenWriteBarrier((&___USW2_11), value);
	}

	inline static int32_t get_offset_of_GOVW1_12() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___GOVW1_12)); }
	inline S3Region_t723338532 * get_GOVW1_12() const { return ___GOVW1_12; }
	inline S3Region_t723338532 ** get_address_of_GOVW1_12() { return &___GOVW1_12; }
	inline void set_GOVW1_12(S3Region_t723338532 * value)
	{
		___GOVW1_12 = value;
		Il2CppCodeGenWriteBarrier((&___GOVW1_12), value);
	}

	inline static int32_t get_offset_of_APS1_13() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___APS1_13)); }
	inline S3Region_t723338532 * get_APS1_13() const { return ___APS1_13; }
	inline S3Region_t723338532 ** get_address_of_APS1_13() { return &___APS1_13; }
	inline void set_APS1_13(S3Region_t723338532 * value)
	{
		___APS1_13 = value;
		Il2CppCodeGenWriteBarrier((&___APS1_13), value);
	}

	inline static int32_t get_offset_of_APS2_14() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___APS2_14)); }
	inline S3Region_t723338532 * get_APS2_14() const { return ___APS2_14; }
	inline S3Region_t723338532 ** get_address_of_APS2_14() { return &___APS2_14; }
	inline void set_APS2_14(S3Region_t723338532 * value)
	{
		___APS2_14 = value;
		Il2CppCodeGenWriteBarrier((&___APS2_14), value);
	}

	inline static int32_t get_offset_of_APN1_15() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___APN1_15)); }
	inline S3Region_t723338532 * get_APN1_15() const { return ___APN1_15; }
	inline S3Region_t723338532 ** get_address_of_APN1_15() { return &___APN1_15; }
	inline void set_APN1_15(S3Region_t723338532 * value)
	{
		___APN1_15 = value;
		Il2CppCodeGenWriteBarrier((&___APN1_15), value);
	}

	inline static int32_t get_offset_of_APN2_16() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___APN2_16)); }
	inline S3Region_t723338532 * get_APN2_16() const { return ___APN2_16; }
	inline S3Region_t723338532 ** get_address_of_APN2_16() { return &___APN2_16; }
	inline void set_APN2_16(S3Region_t723338532 * value)
	{
		___APN2_16 = value;
		Il2CppCodeGenWriteBarrier((&___APN2_16), value);
	}

	inline static int32_t get_offset_of_APS3_17() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___APS3_17)); }
	inline S3Region_t723338532 * get_APS3_17() const { return ___APS3_17; }
	inline S3Region_t723338532 ** get_address_of_APS3_17() { return &___APS3_17; }
	inline void set_APS3_17(S3Region_t723338532 * value)
	{
		___APS3_17 = value;
		Il2CppCodeGenWriteBarrier((&___APS3_17), value);
	}

	inline static int32_t get_offset_of_SAE1_18() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___SAE1_18)); }
	inline S3Region_t723338532 * get_SAE1_18() const { return ___SAE1_18; }
	inline S3Region_t723338532 ** get_address_of_SAE1_18() { return &___SAE1_18; }
	inline void set_SAE1_18(S3Region_t723338532 * value)
	{
		___SAE1_18 = value;
		Il2CppCodeGenWriteBarrier((&___SAE1_18), value);
	}

	inline static int32_t get_offset_of_CN1_19() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___CN1_19)); }
	inline S3Region_t723338532 * get_CN1_19() const { return ___CN1_19; }
	inline S3Region_t723338532 ** get_address_of_CN1_19() { return &___CN1_19; }
	inline void set_CN1_19(S3Region_t723338532 * value)
	{
		___CN1_19 = value;
		Il2CppCodeGenWriteBarrier((&___CN1_19), value);
	}

	inline static int32_t get_offset_of_CNW1_20() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___CNW1_20)); }
	inline S3Region_t723338532 * get_CNW1_20() const { return ___CNW1_20; }
	inline S3Region_t723338532 ** get_address_of_CNW1_20() { return &___CNW1_20; }
	inline void set_CNW1_20(S3Region_t723338532 * value)
	{
		___CNW1_20 = value;
		Il2CppCodeGenWriteBarrier((&___CNW1_20), value);
	}

	inline static int32_t get_offset_of_CAN1_21() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___CAN1_21)); }
	inline S3Region_t723338532 * get_CAN1_21() const { return ___CAN1_21; }
	inline S3Region_t723338532 ** get_address_of_CAN1_21() { return &___CAN1_21; }
	inline void set_CAN1_21(S3Region_t723338532 * value)
	{
		___CAN1_21 = value;
		Il2CppCodeGenWriteBarrier((&___CAN1_21), value);
	}

	inline static int32_t get_offset_of_SFO_22() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___SFO_22)); }
	inline S3Region_t723338532 * get_SFO_22() const { return ___SFO_22; }
	inline S3Region_t723338532 ** get_address_of_SFO_22() { return &___SFO_22; }
	inline void set_SFO_22(S3Region_t723338532 * value)
	{
		___SFO_22 = value;
		Il2CppCodeGenWriteBarrier((&___SFO_22), value);
	}

	inline static int32_t get_offset_of_CN_23() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___CN_23)); }
	inline S3Region_t723338532 * get_CN_23() const { return ___CN_23; }
	inline S3Region_t723338532 ** get_address_of_CN_23() { return &___CN_23; }
	inline void set_CN_23(S3Region_t723338532 * value)
	{
		___CN_23 = value;
		Il2CppCodeGenWriteBarrier((&___CN_23), value);
	}

	inline static int32_t get_offset_of_GOV_24() { return static_cast<int32_t>(offsetof(S3Region_t723338532_StaticFields, ___GOV_24)); }
	inline S3Region_t723338532 * get_GOV_24() const { return ___GOV_24; }
	inline S3Region_t723338532 ** get_address_of_GOV_24() { return &___GOV_24; }
	inline void set_GOV_24(S3Region_t723338532 * value)
	{
		___GOV_24 = value;
		Il2CppCodeGenWriteBarrier((&___GOV_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3REGION_T723338532_H
#ifndef U24ARRAYTYPEU3D12_T1568637717_H
#define U24ARRAYTYPEU3D12_T1568637717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t1568637717 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t1568637717__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T1568637717_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef S3STORAGECLASS_T454477475_H
#define S3STORAGECLASS_T454477475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.S3StorageClass
struct  S3StorageClass_t454477475  : public ConstantClass_t4000559886
{
public:

public:
};

struct S3StorageClass_t454477475_StaticFields
{
public:
	// Amazon.S3.S3StorageClass Amazon.S3.S3StorageClass::Standard
	S3StorageClass_t454477475 * ___Standard_3;
	// Amazon.S3.S3StorageClass Amazon.S3.S3StorageClass::ReducedRedundancy
	S3StorageClass_t454477475 * ___ReducedRedundancy_4;
	// Amazon.S3.S3StorageClass Amazon.S3.S3StorageClass::Glacier
	S3StorageClass_t454477475 * ___Glacier_5;
	// Amazon.S3.S3StorageClass Amazon.S3.S3StorageClass::StandardInfrequentAccess
	S3StorageClass_t454477475 * ___StandardInfrequentAccess_6;

public:
	inline static int32_t get_offset_of_Standard_3() { return static_cast<int32_t>(offsetof(S3StorageClass_t454477475_StaticFields, ___Standard_3)); }
	inline S3StorageClass_t454477475 * get_Standard_3() const { return ___Standard_3; }
	inline S3StorageClass_t454477475 ** get_address_of_Standard_3() { return &___Standard_3; }
	inline void set_Standard_3(S3StorageClass_t454477475 * value)
	{
		___Standard_3 = value;
		Il2CppCodeGenWriteBarrier((&___Standard_3), value);
	}

	inline static int32_t get_offset_of_ReducedRedundancy_4() { return static_cast<int32_t>(offsetof(S3StorageClass_t454477475_StaticFields, ___ReducedRedundancy_4)); }
	inline S3StorageClass_t454477475 * get_ReducedRedundancy_4() const { return ___ReducedRedundancy_4; }
	inline S3StorageClass_t454477475 ** get_address_of_ReducedRedundancy_4() { return &___ReducedRedundancy_4; }
	inline void set_ReducedRedundancy_4(S3StorageClass_t454477475 * value)
	{
		___ReducedRedundancy_4 = value;
		Il2CppCodeGenWriteBarrier((&___ReducedRedundancy_4), value);
	}

	inline static int32_t get_offset_of_Glacier_5() { return static_cast<int32_t>(offsetof(S3StorageClass_t454477475_StaticFields, ___Glacier_5)); }
	inline S3StorageClass_t454477475 * get_Glacier_5() const { return ___Glacier_5; }
	inline S3StorageClass_t454477475 ** get_address_of_Glacier_5() { return &___Glacier_5; }
	inline void set_Glacier_5(S3StorageClass_t454477475 * value)
	{
		___Glacier_5 = value;
		Il2CppCodeGenWriteBarrier((&___Glacier_5), value);
	}

	inline static int32_t get_offset_of_StandardInfrequentAccess_6() { return static_cast<int32_t>(offsetof(S3StorageClass_t454477475_StaticFields, ___StandardInfrequentAccess_6)); }
	inline S3StorageClass_t454477475 * get_StandardInfrequentAccess_6() const { return ___StandardInfrequentAccess_6; }
	inline S3StorageClass_t454477475 ** get_address_of_StandardInfrequentAccess_6() { return &___StandardInfrequentAccess_6; }
	inline void set_StandardInfrequentAccess_6(S3StorageClass_t454477475 * value)
	{
		___StandardInfrequentAccess_6 = value;
		Il2CppCodeGenWriteBarrier((&___StandardInfrequentAccess_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3STORAGECLASS_T454477475_H
#ifndef SERVERSIDEENCRYPTIONMETHOD_T608782770_H
#define SERVERSIDEENCRYPTIONMETHOD_T608782770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.ServerSideEncryptionMethod
struct  ServerSideEncryptionMethod_t608782770  : public ConstantClass_t4000559886
{
public:

public:
};

struct ServerSideEncryptionMethod_t608782770_StaticFields
{
public:
	// Amazon.S3.ServerSideEncryptionMethod Amazon.S3.ServerSideEncryptionMethod::None
	ServerSideEncryptionMethod_t608782770 * ___None_3;
	// Amazon.S3.ServerSideEncryptionMethod Amazon.S3.ServerSideEncryptionMethod::AES256
	ServerSideEncryptionMethod_t608782770 * ___AES256_4;
	// Amazon.S3.ServerSideEncryptionMethod Amazon.S3.ServerSideEncryptionMethod::AWSKMS
	ServerSideEncryptionMethod_t608782770 * ___AWSKMS_5;

public:
	inline static int32_t get_offset_of_None_3() { return static_cast<int32_t>(offsetof(ServerSideEncryptionMethod_t608782770_StaticFields, ___None_3)); }
	inline ServerSideEncryptionMethod_t608782770 * get_None_3() const { return ___None_3; }
	inline ServerSideEncryptionMethod_t608782770 ** get_address_of_None_3() { return &___None_3; }
	inline void set_None_3(ServerSideEncryptionMethod_t608782770 * value)
	{
		___None_3 = value;
		Il2CppCodeGenWriteBarrier((&___None_3), value);
	}

	inline static int32_t get_offset_of_AES256_4() { return static_cast<int32_t>(offsetof(ServerSideEncryptionMethod_t608782770_StaticFields, ___AES256_4)); }
	inline ServerSideEncryptionMethod_t608782770 * get_AES256_4() const { return ___AES256_4; }
	inline ServerSideEncryptionMethod_t608782770 ** get_address_of_AES256_4() { return &___AES256_4; }
	inline void set_AES256_4(ServerSideEncryptionMethod_t608782770 * value)
	{
		___AES256_4 = value;
		Il2CppCodeGenWriteBarrier((&___AES256_4), value);
	}

	inline static int32_t get_offset_of_AWSKMS_5() { return static_cast<int32_t>(offsetof(ServerSideEncryptionMethod_t608782770_StaticFields, ___AWSKMS_5)); }
	inline ServerSideEncryptionMethod_t608782770 * get_AWSKMS_5() const { return ___AWSKMS_5; }
	inline ServerSideEncryptionMethod_t608782770 ** get_address_of_AWSKMS_5() { return &___AWSKMS_5; }
	inline void set_AWSKMS_5(ServerSideEncryptionMethod_t608782770 * value)
	{
		___AWSKMS_5 = value;
		Il2CppCodeGenWriteBarrier((&___AWSKMS_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSIDEENCRYPTIONMETHOD_T608782770_H
#ifndef ENCODINGTYPE_T159362831_H
#define ENCODINGTYPE_T159362831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.EncodingType
struct  EncodingType_t159362831  : public ConstantClass_t4000559886
{
public:

public:
};

struct EncodingType_t159362831_StaticFields
{
public:
	// Amazon.S3.EncodingType Amazon.S3.EncodingType::Url
	EncodingType_t159362831 * ___Url_3;

public:
	inline static int32_t get_offset_of_Url_3() { return static_cast<int32_t>(offsetof(EncodingType_t159362831_StaticFields, ___Url_3)); }
	inline EncodingType_t159362831 * get_Url_3() const { return ___Url_3; }
	inline EncodingType_t159362831 ** get_address_of_Url_3() { return &___Url_3; }
	inline void set_Url_3(EncodingType_t159362831 * value)
	{
		___Url_3 = value;
		Il2CppCodeGenWriteBarrier((&___Url_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODINGTYPE_T159362831_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef SERVERSIDEENCRYPTIONCUSTOMERMETHOD_T3201425490_H
#define SERVERSIDEENCRYPTIONCUSTOMERMETHOD_T3201425490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.ServerSideEncryptionCustomerMethod
struct  ServerSideEncryptionCustomerMethod_t3201425490  : public ConstantClass_t4000559886
{
public:

public:
};

struct ServerSideEncryptionCustomerMethod_t3201425490_StaticFields
{
public:
	// Amazon.S3.ServerSideEncryptionCustomerMethod Amazon.S3.ServerSideEncryptionCustomerMethod::None
	ServerSideEncryptionCustomerMethod_t3201425490 * ___None_3;
	// Amazon.S3.ServerSideEncryptionCustomerMethod Amazon.S3.ServerSideEncryptionCustomerMethod::AES256
	ServerSideEncryptionCustomerMethod_t3201425490 * ___AES256_4;

public:
	inline static int32_t get_offset_of_None_3() { return static_cast<int32_t>(offsetof(ServerSideEncryptionCustomerMethod_t3201425490_StaticFields, ___None_3)); }
	inline ServerSideEncryptionCustomerMethod_t3201425490 * get_None_3() const { return ___None_3; }
	inline ServerSideEncryptionCustomerMethod_t3201425490 ** get_address_of_None_3() { return &___None_3; }
	inline void set_None_3(ServerSideEncryptionCustomerMethod_t3201425490 * value)
	{
		___None_3 = value;
		Il2CppCodeGenWriteBarrier((&___None_3), value);
	}

	inline static int32_t get_offset_of_AES256_4() { return static_cast<int32_t>(offsetof(ServerSideEncryptionCustomerMethod_t3201425490_StaticFields, ___AES256_4)); }
	inline ServerSideEncryptionCustomerMethod_t3201425490 * get_AES256_4() const { return ___AES256_4; }
	inline ServerSideEncryptionCustomerMethod_t3201425490 ** get_address_of_AES256_4() { return &___AES256_4; }
	inline void set_AES256_4(ServerSideEncryptionCustomerMethod_t3201425490 * value)
	{
		___AES256_4 = value;
		Il2CppCodeGenWriteBarrier((&___AES256_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSIDEENCRYPTIONCUSTOMERMETHOD_T3201425490_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef SPRITESTATE_T1353336012_H
#define SPRITESTATE_T1353336012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1353336012 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t309593783 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t309593783 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_HighlightedSprite_0)); }
	inline Sprite_t309593783 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t309593783 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t309593783 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_PressedSprite_1)); }
	inline Sprite_t309593783 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t309593783 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t309593783 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_DisabledSprite_2)); }
	inline Sprite_t309593783 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t309593783 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t309593783 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_pinvoke
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_com
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1353336012_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef UNITYEVENT_1_T3863924733_H
#define UNITYEVENT_1_T3863924733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t3863924733  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3863924733, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3863924733_H
#ifndef UNITYEVENT_1_T2114859947_H
#define UNITYEVENT_1_T2114859947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2114859947  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2114859947, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2114859947_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T154385424_H
#define DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t154385424 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T154385424_H
#ifndef UNITYEVENT_1_T2282057594_H
#define UNITYEVENT_1_T2282057594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Vector2>
struct  UnityEvent_1_t2282057594  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2282057594, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2282057594_H
#ifndef NULLABLE_1_T2088641033_H
#define NULLABLE_1_T2088641033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t2088641033 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2088641033_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef RAYCASTHIT2D_T4063908774_H
#define RAYCASTHIT2D_T4063908774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t4063908774 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2243707579  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2243707579  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2243707579  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t646061738 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Centroid_0)); }
	inline Vector2_t2243707579  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2243707579 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2243707579  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Point_1)); }
	inline Vector2_t2243707579  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2243707579 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2243707579  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Normal_2)); }
	inline Vector2_t2243707579  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2243707579 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2243707579  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Collider_5)); }
	inline Collider2D_t646061738 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t646061738 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t646061738 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_pinvoke
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_com
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T4063908774_H
#ifndef ERRORTYPE_T1448377524_H
#define ERRORTYPE_T1448377524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ErrorType
struct  ErrorType_t1448377524 
{
public:
	// System.Int32 Amazon.Runtime.ErrorType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ErrorType_t1448377524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORTYPE_T1448377524_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305144_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305144  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t1568637717  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t1568637717  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t1568637717 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t1568637717  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305144_H
#ifndef HTTPSTATUSCODE_T1898409641_H
#define HTTPSTATUSCODE_T1898409641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_t1898409641 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t1898409641, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T1898409641_H
#ifndef V4CLIENTSECTION_T2149061698_H
#define V4CLIENTSECTION_T2149061698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.V4ClientSection
struct  V4ClientSection_t2149061698  : public ConfigurationElement_t1996355484
{
public:
	// System.Nullable`1<System.Boolean> Amazon.V4ClientSection::<UseSignatureVersion4>k__BackingField
	Nullable_1_t2088641033  ___U3CUseSignatureVersion4U3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CUseSignatureVersion4U3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(V4ClientSection_t2149061698, ___U3CUseSignatureVersion4U3Ek__BackingField_1)); }
	inline Nullable_1_t2088641033  get_U3CUseSignatureVersion4U3Ek__BackingField_1() const { return ___U3CUseSignatureVersion4U3Ek__BackingField_1; }
	inline Nullable_1_t2088641033 * get_address_of_U3CUseSignatureVersion4U3Ek__BackingField_1() { return &___U3CUseSignatureVersion4U3Ek__BackingField_1; }
	inline void set_U3CUseSignatureVersion4U3Ek__BackingField_1(Nullable_1_t2088641033  value)
	{
		___U3CUseSignatureVersion4U3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // V4CLIENTSECTION_T2149061698_H
#ifndef RAYCASTHIT_T87180320_H
#define RAYCASTHIT_T87180320_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t87180320 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t2243707580  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t2243707580  ___m_Normal_1;
	// System.Int32 UnityEngine.RaycastHit::m_FaceID
	int32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2243707579  ___m_UV_4;
	// UnityEngine.Collider UnityEngine.RaycastHit::m_Collider
	Collider_t3497673348 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Point_0)); }
	inline Vector3_t2243707580  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t2243707580 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t2243707580  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Normal_1)); }
	inline Vector3_t2243707580  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t2243707580 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t2243707580  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_FaceID_2)); }
	inline int32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline int32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(int32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_UV_4)); }
	inline Vector2_t2243707579  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2243707579 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2243707579  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t87180320, ___m_Collider_5)); }
	inline Collider_t3497673348 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider_t3497673348 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider_t3497673348 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_pinvoke
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit
struct RaycastHit_t87180320_marshaled_com
{
	Vector3_t2243707580  ___m_Point_0;
	Vector3_t2243707580  ___m_Normal_1;
	int32_t ___m_FaceID_2;
	float ___m_Distance_3;
	Vector2_t2243707579  ___m_UV_4;
	Collider_t3497673348 * ___m_Collider_5;
};
#endif // RAYCASTHIT_T87180320_H
#ifndef STENCILOP_T2936374925_H
#define STENCILOP_T2936374925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.StencilOp
struct  StencilOp_t2936374925 
{
public:
	// System.Int32 UnityEngine.Rendering.StencilOp::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StencilOp_t2936374925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STENCILOP_T2936374925_H
#ifndef COMPAREFUNCTION_T457874581_H
#define COMPAREFUNCTION_T457874581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.CompareFunction
struct  CompareFunction_t457874581 
{
public:
	// System.Int32 UnityEngine.Rendering.CompareFunction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompareFunction_t457874581, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPAREFUNCTION_T457874581_H
#ifndef BOUNDS_T3033363703_H
#define BOUNDS_T3033363703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3033363703 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t2243707580  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t2243707580  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Center_0)); }
	inline Vector3_t2243707580  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t2243707580 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t2243707580  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Extents_1)); }
	inline Vector3_t2243707580  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t2243707580 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t2243707580  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3033363703_H
#ifndef COLORBLOCK_T2652774230_H
#define COLORBLOCK_T2652774230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2652774230 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2020392075  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2020392075  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2020392075  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2020392075  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_NormalColor_0)); }
	inline Color_t2020392075  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2020392075 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2020392075  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_HighlightedColor_1)); }
	inline Color_t2020392075  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2020392075 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2020392075  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_PressedColor_2)); }
	inline Color_t2020392075  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2020392075 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2020392075  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_DisabledColor_3)); }
	inline Color_t2020392075  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2020392075 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2020392075  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2652774230_H
#ifndef COLORWRITEMASK_T926634530_H
#define COLORWRITEMASK_T926634530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ColorWriteMask
struct  ColorWriteMask_t926634530 
{
public:
	// System.Int32 UnityEngine.Rendering.ColorWriteMask::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorWriteMask_t926634530, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWRITEMASK_T926634530_H
#ifndef RAY_T2469606224_H
#define RAY_T2469606224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t2469606224 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t2243707580  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t2243707580  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Origin_0)); }
	inline Vector3_t2243707580  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t2243707580 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t2243707580  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t2469606224, ___m_Direction_1)); }
	inline Vector3_t2243707580  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t2243707580 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t2243707580  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T2469606224_H
#ifndef SIGNINGALGORITHM_T3740229458_H
#define SIGNINGALGORITHM_T3740229458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.SigningAlgorithm
struct  SigningAlgorithm_t3740229458 
{
public:
	// System.Int32 Amazon.Runtime.SigningAlgorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SigningAlgorithm_t3740229458, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNINGALGORITHM_T3740229458_H
#ifndef TEXTANCHOR_T112990806_H
#define TEXTANCHOR_T112990806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t112990806 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t112990806, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T112990806_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef SELECTIONSTATE_T3187567897_H
#define SELECTIONSTATE_T3187567897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t3187567897 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t3187567897, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T3187567897_H
#ifndef TRANSITION_T605142169_H
#define TRANSITION_T605142169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t605142169 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t605142169, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T605142169_H
#ifndef DIRECTION_T1525323322_H
#define DIRECTION_T1525323322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Direction
struct  Direction_t1525323322 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t1525323322, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T1525323322_H
#ifndef AXIS_T2427050347_H
#define AXIS_T2427050347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar/Axis
struct  Axis_t2427050347 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t2427050347, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T2427050347_H
#ifndef FITMODE_T4030874534_H
#define FITMODE_T4030874534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter/FitMode
struct  FitMode_t4030874534 
{
public:
	// System.Int32 UnityEngine.UI.ContentSizeFitter/FitMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FitMode_t4030874534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FITMODE_T4030874534_H
#ifndef SCROLLRECTEVENT_T3529018992_H
#define SCROLLRECTEVENT_T3529018992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct  ScrollRectEvent_t3529018992  : public UnityEvent_1_t2282057594
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECTEVENT_T3529018992_H
#ifndef AXIS_T1431825778_H
#define AXIS_T1431825778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Axis
struct  Axis_t1431825778 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t1431825778, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T1431825778_H
#ifndef MODE_T1081683921_H
#define MODE_T1081683921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1081683921 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1081683921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1081683921_H
#ifndef CORNER_T1077473318_H
#define CORNER_T1077473318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Corner
struct  Corner_t1077473318 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Corner::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Corner_t1077473318, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_T1077473318_H
#ifndef TOGGLETRANSITION_T1114673831_H
#define TOGGLETRANSITION_T1114673831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle/ToggleTransition
struct  ToggleTransition_t1114673831 
{
public:
	// System.Int32 UnityEngine.UI.Toggle/ToggleTransition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ToggleTransition_t1114673831, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLETRANSITION_T1114673831_H
#ifndef TOGGLEEVENT_T1896830814_H
#define TOGGLEEVENT_T1896830814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle/ToggleEvent
struct  ToggleEvent_t1896830814  : public UnityEvent_1_t3863924733
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEEVENT_T1896830814_H
#ifndef SCROLLEVENT_T1794825321_H
#define SCROLLEVENT_T1794825321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar/ScrollEvent
struct  ScrollEvent_t1794825321  : public UnityEvent_1_t2114859947
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLEVENT_T1794825321_H
#ifndef DIRECTION_T3696775921_H
#define DIRECTION_T3696775921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar/Direction
struct  Direction_t3696775921 
{
public:
	// System.Int32 UnityEngine.UI.Scrollbar/Direction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Direction_t3696775921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTION_T3696775921_H
#ifndef AXIS_T375128448_H
#define AXIS_T375128448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/Axis
struct  Axis_t375128448 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t375128448, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T375128448_H
#ifndef SCALEMODE_T987318053_H
#define SCALEMODE_T987318053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScaleMode
struct  ScaleMode_t987318053 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScaleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScaleMode_t987318053, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEMODE_T987318053_H
#ifndef SCREENMATCHMODE_T1916789528_H
#define SCREENMATCHMODE_T1916789528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScreenMatchMode
struct  ScreenMatchMode_t1916789528 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScreenMatchMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenMatchMode_t1916789528, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENMATCHMODE_T1916789528_H
#ifndef ASPECTMODE_T1166448724_H
#define ASPECTMODE_T1166448724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter/AspectMode
struct  AspectMode_t1166448724 
{
public:
	// System.Int32 UnityEngine.UI.AspectRatioFitter/AspectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectMode_t1166448724, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTMODE_T1166448724_H
#ifndef SLIDEREVENT_T2111116400_H
#define SLIDEREVENT_T2111116400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider/SliderEvent
struct  SliderEvent_t2111116400  : public UnityEvent_1_t2114859947
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDEREVENT_T2111116400_H
#ifndef CONSTRAINT_T3558160636_H
#define CONSTRAINT_T3558160636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Constraint
struct  Constraint_t3558160636 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Constraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Constraint_t3558160636, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T3558160636_H
#ifndef MOVEMENTTYPE_T905360158_H
#define MOVEMENTTYPE_T905360158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/MovementType
struct  MovementType_t905360158 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/MovementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MovementType_t905360158, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENTTYPE_T905360158_H
#ifndef UNIT_T3220761768_H
#define UNIT_T3220761768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/Unit
struct  Unit_t3220761768 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/Unit::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Unit_t3220761768, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T3220761768_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef VERTEXHELPER_T385374196_H
#define VERTEXHELPER_T385374196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t385374196  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t1612828712 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t243638650 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t1612828711 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t1612828711 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t1612828711 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t1612828711 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t1612828712 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t1612828713 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t1440998580 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Positions_0)); }
	inline List_1_t1612828712 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t1612828712 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t1612828712 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Colors_1)); }
	inline List_1_t243638650 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t243638650 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t243638650 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv0S_2)); }
	inline List_1_t1612828711 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t1612828711 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t1612828711 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv1S_3)); }
	inline List_1_t1612828711 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t1612828711 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t1612828711 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv2S_4)); }
	inline List_1_t1612828711 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t1612828711 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t1612828711 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Uv3S_5)); }
	inline List_1_t1612828711 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t1612828711 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t1612828711 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Normals_6)); }
	inline List_1_t1612828712 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t1612828712 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t1612828712 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Tangents_7)); }
	inline List_1_t1612828713 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t1612828713 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t1612828713 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196, ___m_Indices_8)); }
	inline List_1_t1440998580 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t1440998580 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t1440998580 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t385374196_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t2243707581  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t2243707580  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t2243707581  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t2243707581 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t2243707581  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t385374196_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t2243707580  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t2243707580 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t2243707580  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T385374196_H
#ifndef SCROLLBARVISIBILITY_T3834843475_H
#define SCROLLBARVISIBILITY_T3834843475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect/ScrollbarVisibility
struct  ScrollbarVisibility_t3834843475 
{
public:
	// System.Int32 UnityEngine.UI.ScrollRect/ScrollbarVisibility::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrollbarVisibility_t3834843475, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBARVISIBILITY_T3834843475_H
#ifndef AMAZONSERVICEEXCEPTION_T3748559634_H
#define AMAZONSERVICEEXCEPTION_T3748559634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonServiceException
struct  AmazonServiceException_t3748559634  : public Exception_t1927440687
{
public:
	// Amazon.Runtime.ErrorType Amazon.Runtime.AmazonServiceException::errorType
	int32_t ___errorType_11;
	// System.String Amazon.Runtime.AmazonServiceException::errorCode
	String_t* ___errorCode_12;
	// System.String Amazon.Runtime.AmazonServiceException::requestId
	String_t* ___requestId_13;
	// System.Net.HttpStatusCode Amazon.Runtime.AmazonServiceException::statusCode
	int32_t ___statusCode_14;

public:
	inline static int32_t get_offset_of_errorType_11() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___errorType_11)); }
	inline int32_t get_errorType_11() const { return ___errorType_11; }
	inline int32_t* get_address_of_errorType_11() { return &___errorType_11; }
	inline void set_errorType_11(int32_t value)
	{
		___errorType_11 = value;
	}

	inline static int32_t get_offset_of_errorCode_12() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___errorCode_12)); }
	inline String_t* get_errorCode_12() const { return ___errorCode_12; }
	inline String_t** get_address_of_errorCode_12() { return &___errorCode_12; }
	inline void set_errorCode_12(String_t* value)
	{
		___errorCode_12 = value;
		Il2CppCodeGenWriteBarrier((&___errorCode_12), value);
	}

	inline static int32_t get_offset_of_requestId_13() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___requestId_13)); }
	inline String_t* get_requestId_13() const { return ___requestId_13; }
	inline String_t** get_address_of_requestId_13() { return &___requestId_13; }
	inline void set_requestId_13(String_t* value)
	{
		___requestId_13 = value;
		Il2CppCodeGenWriteBarrier((&___requestId_13), value);
	}

	inline static int32_t get_offset_of_statusCode_14() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___statusCode_14)); }
	inline int32_t get_statusCode_14() const { return ___statusCode_14; }
	inline int32_t* get_address_of_statusCode_14() { return &___statusCode_14; }
	inline void set_statusCode_14(int32_t value)
	{
		___statusCode_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONSERVICEEXCEPTION_T3748559634_H
#ifndef MATENTRY_T3157325053_H
#define MATENTRY_T3157325053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.StencilMaterial/MatEntry
struct  MatEntry_t3157325053  : public RuntimeObject
{
public:
	// UnityEngine.Material UnityEngine.UI.StencilMaterial/MatEntry::baseMat
	Material_t193706927 * ___baseMat_0;
	// UnityEngine.Material UnityEngine.UI.StencilMaterial/MatEntry::customMat
	Material_t193706927 * ___customMat_1;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::count
	int32_t ___count_2;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::stencilId
	int32_t ___stencilId_3;
	// UnityEngine.Rendering.StencilOp UnityEngine.UI.StencilMaterial/MatEntry::operation
	int32_t ___operation_4;
	// UnityEngine.Rendering.CompareFunction UnityEngine.UI.StencilMaterial/MatEntry::compareFunction
	int32_t ___compareFunction_5;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::readMask
	int32_t ___readMask_6;
	// System.Int32 UnityEngine.UI.StencilMaterial/MatEntry::writeMask
	int32_t ___writeMask_7;
	// System.Boolean UnityEngine.UI.StencilMaterial/MatEntry::useAlphaClip
	bool ___useAlphaClip_8;
	// UnityEngine.Rendering.ColorWriteMask UnityEngine.UI.StencilMaterial/MatEntry::colorMask
	int32_t ___colorMask_9;

public:
	inline static int32_t get_offset_of_baseMat_0() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___baseMat_0)); }
	inline Material_t193706927 * get_baseMat_0() const { return ___baseMat_0; }
	inline Material_t193706927 ** get_address_of_baseMat_0() { return &___baseMat_0; }
	inline void set_baseMat_0(Material_t193706927 * value)
	{
		___baseMat_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseMat_0), value);
	}

	inline static int32_t get_offset_of_customMat_1() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___customMat_1)); }
	inline Material_t193706927 * get_customMat_1() const { return ___customMat_1; }
	inline Material_t193706927 ** get_address_of_customMat_1() { return &___customMat_1; }
	inline void set_customMat_1(Material_t193706927 * value)
	{
		___customMat_1 = value;
		Il2CppCodeGenWriteBarrier((&___customMat_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_stencilId_3() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___stencilId_3)); }
	inline int32_t get_stencilId_3() const { return ___stencilId_3; }
	inline int32_t* get_address_of_stencilId_3() { return &___stencilId_3; }
	inline void set_stencilId_3(int32_t value)
	{
		___stencilId_3 = value;
	}

	inline static int32_t get_offset_of_operation_4() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___operation_4)); }
	inline int32_t get_operation_4() const { return ___operation_4; }
	inline int32_t* get_address_of_operation_4() { return &___operation_4; }
	inline void set_operation_4(int32_t value)
	{
		___operation_4 = value;
	}

	inline static int32_t get_offset_of_compareFunction_5() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___compareFunction_5)); }
	inline int32_t get_compareFunction_5() const { return ___compareFunction_5; }
	inline int32_t* get_address_of_compareFunction_5() { return &___compareFunction_5; }
	inline void set_compareFunction_5(int32_t value)
	{
		___compareFunction_5 = value;
	}

	inline static int32_t get_offset_of_readMask_6() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___readMask_6)); }
	inline int32_t get_readMask_6() const { return ___readMask_6; }
	inline int32_t* get_address_of_readMask_6() { return &___readMask_6; }
	inline void set_readMask_6(int32_t value)
	{
		___readMask_6 = value;
	}

	inline static int32_t get_offset_of_writeMask_7() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___writeMask_7)); }
	inline int32_t get_writeMask_7() const { return ___writeMask_7; }
	inline int32_t* get_address_of_writeMask_7() { return &___writeMask_7; }
	inline void set_writeMask_7(int32_t value)
	{
		___writeMask_7 = value;
	}

	inline static int32_t get_offset_of_useAlphaClip_8() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___useAlphaClip_8)); }
	inline bool get_useAlphaClip_8() const { return ___useAlphaClip_8; }
	inline bool* get_address_of_useAlphaClip_8() { return &___useAlphaClip_8; }
	inline void set_useAlphaClip_8(bool value)
	{
		___useAlphaClip_8 = value;
	}

	inline static int32_t get_offset_of_colorMask_9() { return static_cast<int32_t>(offsetof(MatEntry_t3157325053, ___colorMask_9)); }
	inline int32_t get_colorMask_9() const { return ___colorMask_9; }
	inline int32_t* get_address_of_colorMask_9() { return &___colorMask_9; }
	inline void set_colorMask_9(int32_t value)
	{
		___colorMask_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATENTRY_T3157325053_H
#ifndef CLIENTCONFIG_T3664713661_H
#define CLIENTCONFIG_T3664713661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ClientConfig
struct  ClientConfig_t3664713661  : public RuntimeObject
{
public:
	// Amazon.RegionEndpoint Amazon.Runtime.ClientConfig::regionEndpoint
	RegionEndpoint_t661522805 * ___regionEndpoint_2;
	// System.Boolean Amazon.Runtime.ClientConfig::probeForRegionEndpoint
	bool ___probeForRegionEndpoint_3;
	// System.Boolean Amazon.Runtime.ClientConfig::throttleRetries
	bool ___throttleRetries_4;
	// System.Boolean Amazon.Runtime.ClientConfig::useHttp
	bool ___useHttp_5;
	// System.String Amazon.Runtime.ClientConfig::serviceURL
	String_t* ___serviceURL_6;
	// System.String Amazon.Runtime.ClientConfig::authRegion
	String_t* ___authRegion_7;
	// System.String Amazon.Runtime.ClientConfig::authServiceName
	String_t* ___authServiceName_8;
	// System.String Amazon.Runtime.ClientConfig::signatureVersion
	String_t* ___signatureVersion_9;
	// Amazon.Runtime.SigningAlgorithm Amazon.Runtime.ClientConfig::signatureMethod
	int32_t ___signatureMethod_10;
	// System.Int32 Amazon.Runtime.ClientConfig::maxErrorRetry
	int32_t ___maxErrorRetry_11;
	// System.Boolean Amazon.Runtime.ClientConfig::logResponse
	bool ___logResponse_12;
	// System.Int32 Amazon.Runtime.ClientConfig::bufferSize
	int32_t ___bufferSize_13;
	// System.Int64 Amazon.Runtime.ClientConfig::progressUpdateInterval
	int64_t ___progressUpdateInterval_14;
	// System.Boolean Amazon.Runtime.ClientConfig::resignRetries
	bool ___resignRetries_15;
	// System.Boolean Amazon.Runtime.ClientConfig::logMetrics
	bool ___logMetrics_16;
	// System.Boolean Amazon.Runtime.ClientConfig::disableLogging
	bool ___disableLogging_17;
	// System.Boolean Amazon.Runtime.ClientConfig::allowAutoRedirect
	bool ___allowAutoRedirect_18;
	// System.Boolean Amazon.Runtime.ClientConfig::useDualstackEndpoint
	bool ___useDualstackEndpoint_19;
	// System.Int32 Amazon.Runtime.ClientConfig::proxyPort
	int32_t ___proxyPort_20;

public:
	inline static int32_t get_offset_of_regionEndpoint_2() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___regionEndpoint_2)); }
	inline RegionEndpoint_t661522805 * get_regionEndpoint_2() const { return ___regionEndpoint_2; }
	inline RegionEndpoint_t661522805 ** get_address_of_regionEndpoint_2() { return &___regionEndpoint_2; }
	inline void set_regionEndpoint_2(RegionEndpoint_t661522805 * value)
	{
		___regionEndpoint_2 = value;
		Il2CppCodeGenWriteBarrier((&___regionEndpoint_2), value);
	}

	inline static int32_t get_offset_of_probeForRegionEndpoint_3() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___probeForRegionEndpoint_3)); }
	inline bool get_probeForRegionEndpoint_3() const { return ___probeForRegionEndpoint_3; }
	inline bool* get_address_of_probeForRegionEndpoint_3() { return &___probeForRegionEndpoint_3; }
	inline void set_probeForRegionEndpoint_3(bool value)
	{
		___probeForRegionEndpoint_3 = value;
	}

	inline static int32_t get_offset_of_throttleRetries_4() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___throttleRetries_4)); }
	inline bool get_throttleRetries_4() const { return ___throttleRetries_4; }
	inline bool* get_address_of_throttleRetries_4() { return &___throttleRetries_4; }
	inline void set_throttleRetries_4(bool value)
	{
		___throttleRetries_4 = value;
	}

	inline static int32_t get_offset_of_useHttp_5() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___useHttp_5)); }
	inline bool get_useHttp_5() const { return ___useHttp_5; }
	inline bool* get_address_of_useHttp_5() { return &___useHttp_5; }
	inline void set_useHttp_5(bool value)
	{
		___useHttp_5 = value;
	}

	inline static int32_t get_offset_of_serviceURL_6() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___serviceURL_6)); }
	inline String_t* get_serviceURL_6() const { return ___serviceURL_6; }
	inline String_t** get_address_of_serviceURL_6() { return &___serviceURL_6; }
	inline void set_serviceURL_6(String_t* value)
	{
		___serviceURL_6 = value;
		Il2CppCodeGenWriteBarrier((&___serviceURL_6), value);
	}

	inline static int32_t get_offset_of_authRegion_7() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___authRegion_7)); }
	inline String_t* get_authRegion_7() const { return ___authRegion_7; }
	inline String_t** get_address_of_authRegion_7() { return &___authRegion_7; }
	inline void set_authRegion_7(String_t* value)
	{
		___authRegion_7 = value;
		Il2CppCodeGenWriteBarrier((&___authRegion_7), value);
	}

	inline static int32_t get_offset_of_authServiceName_8() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___authServiceName_8)); }
	inline String_t* get_authServiceName_8() const { return ___authServiceName_8; }
	inline String_t** get_address_of_authServiceName_8() { return &___authServiceName_8; }
	inline void set_authServiceName_8(String_t* value)
	{
		___authServiceName_8 = value;
		Il2CppCodeGenWriteBarrier((&___authServiceName_8), value);
	}

	inline static int32_t get_offset_of_signatureVersion_9() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___signatureVersion_9)); }
	inline String_t* get_signatureVersion_9() const { return ___signatureVersion_9; }
	inline String_t** get_address_of_signatureVersion_9() { return &___signatureVersion_9; }
	inline void set_signatureVersion_9(String_t* value)
	{
		___signatureVersion_9 = value;
		Il2CppCodeGenWriteBarrier((&___signatureVersion_9), value);
	}

	inline static int32_t get_offset_of_signatureMethod_10() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___signatureMethod_10)); }
	inline int32_t get_signatureMethod_10() const { return ___signatureMethod_10; }
	inline int32_t* get_address_of_signatureMethod_10() { return &___signatureMethod_10; }
	inline void set_signatureMethod_10(int32_t value)
	{
		___signatureMethod_10 = value;
	}

	inline static int32_t get_offset_of_maxErrorRetry_11() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___maxErrorRetry_11)); }
	inline int32_t get_maxErrorRetry_11() const { return ___maxErrorRetry_11; }
	inline int32_t* get_address_of_maxErrorRetry_11() { return &___maxErrorRetry_11; }
	inline void set_maxErrorRetry_11(int32_t value)
	{
		___maxErrorRetry_11 = value;
	}

	inline static int32_t get_offset_of_logResponse_12() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___logResponse_12)); }
	inline bool get_logResponse_12() const { return ___logResponse_12; }
	inline bool* get_address_of_logResponse_12() { return &___logResponse_12; }
	inline void set_logResponse_12(bool value)
	{
		___logResponse_12 = value;
	}

	inline static int32_t get_offset_of_bufferSize_13() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___bufferSize_13)); }
	inline int32_t get_bufferSize_13() const { return ___bufferSize_13; }
	inline int32_t* get_address_of_bufferSize_13() { return &___bufferSize_13; }
	inline void set_bufferSize_13(int32_t value)
	{
		___bufferSize_13 = value;
	}

	inline static int32_t get_offset_of_progressUpdateInterval_14() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___progressUpdateInterval_14)); }
	inline int64_t get_progressUpdateInterval_14() const { return ___progressUpdateInterval_14; }
	inline int64_t* get_address_of_progressUpdateInterval_14() { return &___progressUpdateInterval_14; }
	inline void set_progressUpdateInterval_14(int64_t value)
	{
		___progressUpdateInterval_14 = value;
	}

	inline static int32_t get_offset_of_resignRetries_15() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___resignRetries_15)); }
	inline bool get_resignRetries_15() const { return ___resignRetries_15; }
	inline bool* get_address_of_resignRetries_15() { return &___resignRetries_15; }
	inline void set_resignRetries_15(bool value)
	{
		___resignRetries_15 = value;
	}

	inline static int32_t get_offset_of_logMetrics_16() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___logMetrics_16)); }
	inline bool get_logMetrics_16() const { return ___logMetrics_16; }
	inline bool* get_address_of_logMetrics_16() { return &___logMetrics_16; }
	inline void set_logMetrics_16(bool value)
	{
		___logMetrics_16 = value;
	}

	inline static int32_t get_offset_of_disableLogging_17() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___disableLogging_17)); }
	inline bool get_disableLogging_17() const { return ___disableLogging_17; }
	inline bool* get_address_of_disableLogging_17() { return &___disableLogging_17; }
	inline void set_disableLogging_17(bool value)
	{
		___disableLogging_17 = value;
	}

	inline static int32_t get_offset_of_allowAutoRedirect_18() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___allowAutoRedirect_18)); }
	inline bool get_allowAutoRedirect_18() const { return ___allowAutoRedirect_18; }
	inline bool* get_address_of_allowAutoRedirect_18() { return &___allowAutoRedirect_18; }
	inline void set_allowAutoRedirect_18(bool value)
	{
		___allowAutoRedirect_18 = value;
	}

	inline static int32_t get_offset_of_useDualstackEndpoint_19() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___useDualstackEndpoint_19)); }
	inline bool get_useDualstackEndpoint_19() const { return ___useDualstackEndpoint_19; }
	inline bool* get_address_of_useDualstackEndpoint_19() { return &___useDualstackEndpoint_19; }
	inline void set_useDualstackEndpoint_19(bool value)
	{
		___useDualstackEndpoint_19 = value;
	}

	inline static int32_t get_offset_of_proxyPort_20() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___proxyPort_20)); }
	inline int32_t get_proxyPort_20() const { return ___proxyPort_20; }
	inline int32_t* get_address_of_proxyPort_20() { return &___proxyPort_20; }
	inline void set_proxyPort_20(int32_t value)
	{
		___proxyPort_20 = value;
	}
};

struct ClientConfig_t3664713661_StaticFields
{
public:
	// System.TimeSpan Amazon.Runtime.ClientConfig::InfiniteTimeout
	TimeSpan_t3430258949  ___InfiniteTimeout_0;
	// System.TimeSpan Amazon.Runtime.ClientConfig::MaxTimeout
	TimeSpan_t3430258949  ___MaxTimeout_1;

public:
	inline static int32_t get_offset_of_InfiniteTimeout_0() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661_StaticFields, ___InfiniteTimeout_0)); }
	inline TimeSpan_t3430258949  get_InfiniteTimeout_0() const { return ___InfiniteTimeout_0; }
	inline TimeSpan_t3430258949 * get_address_of_InfiniteTimeout_0() { return &___InfiniteTimeout_0; }
	inline void set_InfiniteTimeout_0(TimeSpan_t3430258949  value)
	{
		___InfiniteTimeout_0 = value;
	}

	inline static int32_t get_offset_of_MaxTimeout_1() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661_StaticFields, ___MaxTimeout_1)); }
	inline TimeSpan_t3430258949  get_MaxTimeout_1() const { return ___MaxTimeout_1; }
	inline TimeSpan_t3430258949 * get_address_of_MaxTimeout_1() { return &___MaxTimeout_1; }
	inline void set_MaxTimeout_1(TimeSpan_t3430258949  value)
	{
		___MaxTimeout_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCONFIG_T3664713661_H
#ifndef NAVIGATION_T1571958496_H
#define NAVIGATION_T1571958496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t1571958496 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t1490392188 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnUp_1)); }
	inline Selectable_t1490392188 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t1490392188 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnDown_2)); }
	inline Selectable_t1490392188 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t1490392188 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnLeft_3)); }
	inline Selectable_t1490392188 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t1490392188 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnRight_4)); }
	inline Selectable_t1490392188 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t1490392188 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T1571958496_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T1074830945_H
#define GETRAYCASTNONALLOCCALLBACK_T1074830945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t1074830945  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T1074830945_H
#ifndef AMAZONS3CONFIG_T29117630_H
#define AMAZONS3CONFIG_T29117630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.AmazonS3Config
struct  AmazonS3Config_t29117630  : public ClientConfig_t3664713661
{
public:
	// System.Boolean Amazon.S3.AmazonS3Config::forcePathStyle
	bool ___forcePathStyle_21;
	// System.Boolean Amazon.S3.AmazonS3Config::useAccelerateEndpoint
	bool ___useAccelerateEndpoint_22;
	// System.String Amazon.S3.AmazonS3Config::_userAgent
	String_t* ____userAgent_24;

public:
	inline static int32_t get_offset_of_forcePathStyle_21() { return static_cast<int32_t>(offsetof(AmazonS3Config_t29117630, ___forcePathStyle_21)); }
	inline bool get_forcePathStyle_21() const { return ___forcePathStyle_21; }
	inline bool* get_address_of_forcePathStyle_21() { return &___forcePathStyle_21; }
	inline void set_forcePathStyle_21(bool value)
	{
		___forcePathStyle_21 = value;
	}

	inline static int32_t get_offset_of_useAccelerateEndpoint_22() { return static_cast<int32_t>(offsetof(AmazonS3Config_t29117630, ___useAccelerateEndpoint_22)); }
	inline bool get_useAccelerateEndpoint_22() const { return ___useAccelerateEndpoint_22; }
	inline bool* get_address_of_useAccelerateEndpoint_22() { return &___useAccelerateEndpoint_22; }
	inline void set_useAccelerateEndpoint_22(bool value)
	{
		___useAccelerateEndpoint_22 = value;
	}

	inline static int32_t get_offset_of__userAgent_24() { return static_cast<int32_t>(offsetof(AmazonS3Config_t29117630, ____userAgent_24)); }
	inline String_t* get__userAgent_24() const { return ____userAgent_24; }
	inline String_t** get_address_of__userAgent_24() { return &____userAgent_24; }
	inline void set__userAgent_24(String_t* value)
	{
		____userAgent_24 = value;
		Il2CppCodeGenWriteBarrier((&____userAgent_24), value);
	}
};

struct AmazonS3Config_t29117630_StaticFields
{
public:
	// System.String Amazon.S3.AmazonS3Config::UserAgentString
	String_t* ___UserAgentString_23;

public:
	inline static int32_t get_offset_of_UserAgentString_23() { return static_cast<int32_t>(offsetof(AmazonS3Config_t29117630_StaticFields, ___UserAgentString_23)); }
	inline String_t* get_UserAgentString_23() const { return ___UserAgentString_23; }
	inline String_t** get_address_of_UserAgentString_23() { return &___UserAgentString_23; }
	inline void set_UserAgentString_23(String_t* value)
	{
		___UserAgentString_23 = value;
		Il2CppCodeGenWriteBarrier((&___UserAgentString_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3CONFIG_T29117630_H
#ifndef AMAZONS3EXCEPTION_T3554613873_H
#define AMAZONS3EXCEPTION_T3554613873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.AmazonS3Exception
struct  AmazonS3Exception_t3554613873  : public AmazonServiceException_t3748559634
{
public:
	// System.String Amazon.S3.AmazonS3Exception::<AmazonId2>k__BackingField
	String_t* ___U3CAmazonId2U3Ek__BackingField_15;
	// System.String Amazon.S3.AmazonS3Exception::<AmazonCloudFrontId>k__BackingField
	String_t* ___U3CAmazonCloudFrontIdU3Ek__BackingField_16;
	// System.String Amazon.S3.AmazonS3Exception::<ResponseBody>k__BackingField
	String_t* ___U3CResponseBodyU3Ek__BackingField_17;
	// System.String Amazon.S3.AmazonS3Exception::<Region>k__BackingField
	String_t* ___U3CRegionU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_U3CAmazonId2U3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AmazonS3Exception_t3554613873, ___U3CAmazonId2U3Ek__BackingField_15)); }
	inline String_t* get_U3CAmazonId2U3Ek__BackingField_15() const { return ___U3CAmazonId2U3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CAmazonId2U3Ek__BackingField_15() { return &___U3CAmazonId2U3Ek__BackingField_15; }
	inline void set_U3CAmazonId2U3Ek__BackingField_15(String_t* value)
	{
		___U3CAmazonId2U3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAmazonId2U3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CAmazonCloudFrontIdU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AmazonS3Exception_t3554613873, ___U3CAmazonCloudFrontIdU3Ek__BackingField_16)); }
	inline String_t* get_U3CAmazonCloudFrontIdU3Ek__BackingField_16() const { return ___U3CAmazonCloudFrontIdU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CAmazonCloudFrontIdU3Ek__BackingField_16() { return &___U3CAmazonCloudFrontIdU3Ek__BackingField_16; }
	inline void set_U3CAmazonCloudFrontIdU3Ek__BackingField_16(String_t* value)
	{
		___U3CAmazonCloudFrontIdU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAmazonCloudFrontIdU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_U3CResponseBodyU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(AmazonS3Exception_t3554613873, ___U3CResponseBodyU3Ek__BackingField_17)); }
	inline String_t* get_U3CResponseBodyU3Ek__BackingField_17() const { return ___U3CResponseBodyU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CResponseBodyU3Ek__BackingField_17() { return &___U3CResponseBodyU3Ek__BackingField_17; }
	inline void set_U3CResponseBodyU3Ek__BackingField_17(String_t* value)
	{
		___U3CResponseBodyU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseBodyU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(AmazonS3Exception_t3554613873, ___U3CRegionU3Ek__BackingField_18)); }
	inline String_t* get_U3CRegionU3Ek__BackingField_18() const { return ___U3CRegionU3Ek__BackingField_18; }
	inline String_t** get_address_of_U3CRegionU3Ek__BackingField_18() { return &___U3CRegionU3Ek__BackingField_18; }
	inline void set_U3CRegionU3Ek__BackingField_18(String_t* value)
	{
		___U3CRegionU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRegionU3Ek__BackingField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3EXCEPTION_T3554613873_H
#ifndef RAYCAST3DCALLBACK_T3928470916_H
#define RAYCAST3DCALLBACK_T3928470916_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t3928470916  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T3928470916_H
#ifndef RAYCAST2DCALLBACK_T2260664863_H
#define RAYCAST2DCALLBACK_T2260664863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t2260664863  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T2260664863_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T3246763936_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T3246763936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t3246763936  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T3246763936_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T2213949596_H
#define GETRAYINTERSECTIONALLCALLBACK_T2213949596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t2213949596  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T2213949596_H
#ifndef RAYCASTALLCALLBACK_T3435657708_H
#define RAYCASTALLCALLBACK_T3435657708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t3435657708  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T3435657708_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef DELETEOBJECTSEXCEPTION_T338470828_H
#define DELETEOBJECTSEXCEPTION_T338470828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.DeleteObjectsException
struct  DeleteObjectsException_t338470828  : public AmazonS3Exception_t3554613873
{
public:
	// Amazon.S3.Model.DeleteObjectsResponse Amazon.S3.DeleteObjectsException::response
	DeleteObjectsResponse_t4016776830 * ___response_19;

public:
	inline static int32_t get_offset_of_response_19() { return static_cast<int32_t>(offsetof(DeleteObjectsException_t338470828, ___response_19)); }
	inline DeleteObjectsResponse_t4016776830 * get_response_19() const { return ___response_19; }
	inline DeleteObjectsResponse_t4016776830 ** get_address_of_response_19() { return &___response_19; }
	inline void set_response_19(DeleteObjectsResponse_t4016776830 * value)
	{
		___response_19 = value;
		Il2CppCodeGenWriteBarrier((&___response_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEOBJECTSEXCEPTION_T338470828_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef LAYOUTGROUP_T3962498969_H
#define LAYOUTGROUP_T3962498969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t3962498969  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t3387826427 * ___m_Padding_2;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_3;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2243707579  ___m_TotalMinSize_6;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2243707579  ___m_TotalPreferredSize_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2243707579  ___m_TotalFlexibleSize_8;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t2719087314 * ___m_RectChildren_9;

public:
	inline static int32_t get_offset_of_m_Padding_2() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Padding_2)); }
	inline RectOffset_t3387826427 * get_m_Padding_2() const { return ___m_Padding_2; }
	inline RectOffset_t3387826427 ** get_address_of_m_Padding_2() { return &___m_Padding_2; }
	inline void set_m_Padding_2(RectOffset_t3387826427 * value)
	{
		___m_Padding_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_2), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_3() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_ChildAlignment_3)); }
	inline int32_t get_m_ChildAlignment_3() const { return ___m_ChildAlignment_3; }
	inline int32_t* get_address_of_m_ChildAlignment_3() { return &___m_ChildAlignment_3; }
	inline void set_m_ChildAlignment_3(int32_t value)
	{
		___m_ChildAlignment_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalMinSize_6)); }
	inline Vector2_t2243707579  get_m_TotalMinSize_6() const { return ___m_TotalMinSize_6; }
	inline Vector2_t2243707579 * get_address_of_m_TotalMinSize_6() { return &___m_TotalMinSize_6; }
	inline void set_m_TotalMinSize_6(Vector2_t2243707579  value)
	{
		___m_TotalMinSize_6 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalPreferredSize_7)); }
	inline Vector2_t2243707579  get_m_TotalPreferredSize_7() const { return ___m_TotalPreferredSize_7; }
	inline Vector2_t2243707579 * get_address_of_m_TotalPreferredSize_7() { return &___m_TotalPreferredSize_7; }
	inline void set_m_TotalPreferredSize_7(Vector2_t2243707579  value)
	{
		___m_TotalPreferredSize_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_TotalFlexibleSize_8)); }
	inline Vector2_t2243707579  get_m_TotalFlexibleSize_8() const { return ___m_TotalFlexibleSize_8; }
	inline Vector2_t2243707579 * get_address_of_m_TotalFlexibleSize_8() { return &___m_TotalFlexibleSize_8; }
	inline void set_m_TotalFlexibleSize_8(Vector2_t2243707579  value)
	{
		___m_TotalFlexibleSize_8 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t3962498969, ___m_RectChildren_9)); }
	inline List_1_t2719087314 * get_m_RectChildren_9() const { return ___m_RectChildren_9; }
	inline List_1_t2719087314 ** get_address_of_m_RectChildren_9() { return &___m_RectChildren_9; }
	inline void set_m_RectChildren_9(List_1_t2719087314 * value)
	{
		___m_RectChildren_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T3962498969_H
#ifndef LAYOUTELEMENT_T2808691390_H
#define LAYOUTELEMENT_T2808691390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutElement
struct  LayoutElement_t2808691390  : public UIBehaviour_t3960014691
{
public:
	// System.Boolean UnityEngine.UI.LayoutElement::m_IgnoreLayout
	bool ___m_IgnoreLayout_2;
	// System.Single UnityEngine.UI.LayoutElement::m_MinWidth
	float ___m_MinWidth_3;
	// System.Single UnityEngine.UI.LayoutElement::m_MinHeight
	float ___m_MinHeight_4;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredWidth
	float ___m_PreferredWidth_5;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredHeight
	float ___m_PreferredHeight_6;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleWidth
	float ___m_FlexibleWidth_7;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleHeight
	float ___m_FlexibleHeight_8;
	// System.Int32 UnityEngine.UI.LayoutElement::m_LayoutPriority
	int32_t ___m_LayoutPriority_9;

public:
	inline static int32_t get_offset_of_m_IgnoreLayout_2() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_IgnoreLayout_2)); }
	inline bool get_m_IgnoreLayout_2() const { return ___m_IgnoreLayout_2; }
	inline bool* get_address_of_m_IgnoreLayout_2() { return &___m_IgnoreLayout_2; }
	inline void set_m_IgnoreLayout_2(bool value)
	{
		___m_IgnoreLayout_2 = value;
	}

	inline static int32_t get_offset_of_m_MinWidth_3() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_MinWidth_3)); }
	inline float get_m_MinWidth_3() const { return ___m_MinWidth_3; }
	inline float* get_address_of_m_MinWidth_3() { return &___m_MinWidth_3; }
	inline void set_m_MinWidth_3(float value)
	{
		___m_MinWidth_3 = value;
	}

	inline static int32_t get_offset_of_m_MinHeight_4() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_MinHeight_4)); }
	inline float get_m_MinHeight_4() const { return ___m_MinHeight_4; }
	inline float* get_address_of_m_MinHeight_4() { return &___m_MinHeight_4; }
	inline void set_m_MinHeight_4(float value)
	{
		___m_MinHeight_4 = value;
	}

	inline static int32_t get_offset_of_m_PreferredWidth_5() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_PreferredWidth_5)); }
	inline float get_m_PreferredWidth_5() const { return ___m_PreferredWidth_5; }
	inline float* get_address_of_m_PreferredWidth_5() { return &___m_PreferredWidth_5; }
	inline void set_m_PreferredWidth_5(float value)
	{
		___m_PreferredWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_PreferredHeight_6() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_PreferredHeight_6)); }
	inline float get_m_PreferredHeight_6() const { return ___m_PreferredHeight_6; }
	inline float* get_address_of_m_PreferredHeight_6() { return &___m_PreferredHeight_6; }
	inline void set_m_PreferredHeight_6(float value)
	{
		___m_PreferredHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleWidth_7() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_FlexibleWidth_7)); }
	inline float get_m_FlexibleWidth_7() const { return ___m_FlexibleWidth_7; }
	inline float* get_address_of_m_FlexibleWidth_7() { return &___m_FlexibleWidth_7; }
	inline void set_m_FlexibleWidth_7(float value)
	{
		___m_FlexibleWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleHeight_8() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_FlexibleHeight_8)); }
	inline float get_m_FlexibleHeight_8() const { return ___m_FlexibleHeight_8; }
	inline float* get_address_of_m_FlexibleHeight_8() { return &___m_FlexibleHeight_8; }
	inline void set_m_FlexibleHeight_8(float value)
	{
		___m_FlexibleHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_LayoutPriority_9() { return static_cast<int32_t>(offsetof(LayoutElement_t2808691390, ___m_LayoutPriority_9)); }
	inline int32_t get_m_LayoutPriority_9() const { return ___m_LayoutPriority_9; }
	inline int32_t* get_address_of_m_LayoutPriority_9() { return &___m_LayoutPriority_9; }
	inline void set_m_LayoutPriority_9(int32_t value)
	{
		___m_LayoutPriority_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENT_T2808691390_H
#ifndef ASPECTRATIOFITTER_T3114550109_H
#define ASPECTRATIOFITTER_T3114550109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter
struct  AspectRatioFitter_t3114550109  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::m_AspectMode
	int32_t ___m_AspectMode_2;
	// System.Single UnityEngine.UI.AspectRatioFitter::m_AspectRatio
	float ___m_AspectRatio_3;
	// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.AspectRatioFitter::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_AspectMode_2() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_AspectMode_2)); }
	inline int32_t get_m_AspectMode_2() const { return ___m_AspectMode_2; }
	inline int32_t* get_address_of_m_AspectMode_2() { return &___m_AspectMode_2; }
	inline void set_m_AspectMode_2(int32_t value)
	{
		___m_AspectMode_2 = value;
	}

	inline static int32_t get_offset_of_m_AspectRatio_3() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_AspectRatio_3)); }
	inline float get_m_AspectRatio_3() const { return ___m_AspectRatio_3; }
	inline float* get_address_of_m_AspectRatio_3() { return &___m_AspectRatio_3; }
	inline void set_m_AspectRatio_3(float value)
	{
		___m_AspectRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3114550109, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOFITTER_T3114550109_H
#ifndef SCROLLRECT_T1199013257_H
#define SCROLLRECT_T1199013257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ScrollRect
struct  ScrollRect_t1199013257  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Content
	RectTransform_t3349966182 * ___m_Content_2;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Horizontal
	bool ___m_Horizontal_3;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Vertical
	bool ___m_Vertical_4;
	// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::m_MovementType
	int32_t ___m_MovementType_5;
	// System.Single UnityEngine.UI.ScrollRect::m_Elasticity
	float ___m_Elasticity_6;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Inertia
	bool ___m_Inertia_7;
	// System.Single UnityEngine.UI.ScrollRect::m_DecelerationRate
	float ___m_DecelerationRate_8;
	// System.Single UnityEngine.UI.ScrollRect::m_ScrollSensitivity
	float ___m_ScrollSensitivity_9;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Viewport
	RectTransform_t3349966182 * ___m_Viewport_10;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_HorizontalScrollbar
	Scrollbar_t3248359358 * ___m_HorizontalScrollbar_11;
	// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::m_VerticalScrollbar
	Scrollbar_t3248359358 * ___m_VerticalScrollbar_12;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_HorizontalScrollbarVisibility
	int32_t ___m_HorizontalScrollbarVisibility_13;
	// UnityEngine.UI.ScrollRect/ScrollbarVisibility UnityEngine.UI.ScrollRect::m_VerticalScrollbarVisibility
	int32_t ___m_VerticalScrollbarVisibility_14;
	// System.Single UnityEngine.UI.ScrollRect::m_HorizontalScrollbarSpacing
	float ___m_HorizontalScrollbarSpacing_15;
	// System.Single UnityEngine.UI.ScrollRect::m_VerticalScrollbarSpacing
	float ___m_VerticalScrollbarSpacing_16;
	// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::m_OnValueChanged
	ScrollRectEvent_t3529018992 * ___m_OnValueChanged_17;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PointerStartLocalCursor
	Vector2_t2243707579  ___m_PointerStartLocalCursor_18;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_ContentStartPosition
	Vector2_t2243707579  ___m_ContentStartPosition_19;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_ViewRect
	RectTransform_t3349966182 * ___m_ViewRect_20;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ContentBounds
	Bounds_t3033363703  ___m_ContentBounds_21;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_ViewBounds
	Bounds_t3033363703  ___m_ViewBounds_22;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_Velocity
	Vector2_t2243707579  ___m_Velocity_23;
	// System.Boolean UnityEngine.UI.ScrollRect::m_Dragging
	bool ___m_Dragging_24;
	// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::m_PrevPosition
	Vector2_t2243707579  ___m_PrevPosition_25;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevContentBounds
	Bounds_t3033363703  ___m_PrevContentBounds_26;
	// UnityEngine.Bounds UnityEngine.UI.ScrollRect::m_PrevViewBounds
	Bounds_t3033363703  ___m_PrevViewBounds_27;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HasRebuiltLayout
	bool ___m_HasRebuiltLayout_28;
	// System.Boolean UnityEngine.UI.ScrollRect::m_HSliderExpand
	bool ___m_HSliderExpand_29;
	// System.Boolean UnityEngine.UI.ScrollRect::m_VSliderExpand
	bool ___m_VSliderExpand_30;
	// System.Single UnityEngine.UI.ScrollRect::m_HSliderHeight
	float ___m_HSliderHeight_31;
	// System.Single UnityEngine.UI.ScrollRect::m_VSliderWidth
	float ___m_VSliderWidth_32;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_Rect
	RectTransform_t3349966182 * ___m_Rect_33;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_HorizontalScrollbarRect
	RectTransform_t3349966182 * ___m_HorizontalScrollbarRect_34;
	// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::m_VerticalScrollbarRect
	RectTransform_t3349966182 * ___m_VerticalScrollbarRect_35;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ScrollRect::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_36;
	// UnityEngine.Vector3[] UnityEngine.UI.ScrollRect::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_37;

public:
	inline static int32_t get_offset_of_m_Content_2() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Content_2)); }
	inline RectTransform_t3349966182 * get_m_Content_2() const { return ___m_Content_2; }
	inline RectTransform_t3349966182 ** get_address_of_m_Content_2() { return &___m_Content_2; }
	inline void set_m_Content_2(RectTransform_t3349966182 * value)
	{
		___m_Content_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_2), value);
	}

	inline static int32_t get_offset_of_m_Horizontal_3() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Horizontal_3)); }
	inline bool get_m_Horizontal_3() const { return ___m_Horizontal_3; }
	inline bool* get_address_of_m_Horizontal_3() { return &___m_Horizontal_3; }
	inline void set_m_Horizontal_3(bool value)
	{
		___m_Horizontal_3 = value;
	}

	inline static int32_t get_offset_of_m_Vertical_4() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Vertical_4)); }
	inline bool get_m_Vertical_4() const { return ___m_Vertical_4; }
	inline bool* get_address_of_m_Vertical_4() { return &___m_Vertical_4; }
	inline void set_m_Vertical_4(bool value)
	{
		___m_Vertical_4 = value;
	}

	inline static int32_t get_offset_of_m_MovementType_5() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_MovementType_5)); }
	inline int32_t get_m_MovementType_5() const { return ___m_MovementType_5; }
	inline int32_t* get_address_of_m_MovementType_5() { return &___m_MovementType_5; }
	inline void set_m_MovementType_5(int32_t value)
	{
		___m_MovementType_5 = value;
	}

	inline static int32_t get_offset_of_m_Elasticity_6() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Elasticity_6)); }
	inline float get_m_Elasticity_6() const { return ___m_Elasticity_6; }
	inline float* get_address_of_m_Elasticity_6() { return &___m_Elasticity_6; }
	inline void set_m_Elasticity_6(float value)
	{
		___m_Elasticity_6 = value;
	}

	inline static int32_t get_offset_of_m_Inertia_7() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Inertia_7)); }
	inline bool get_m_Inertia_7() const { return ___m_Inertia_7; }
	inline bool* get_address_of_m_Inertia_7() { return &___m_Inertia_7; }
	inline void set_m_Inertia_7(bool value)
	{
		___m_Inertia_7 = value;
	}

	inline static int32_t get_offset_of_m_DecelerationRate_8() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_DecelerationRate_8)); }
	inline float get_m_DecelerationRate_8() const { return ___m_DecelerationRate_8; }
	inline float* get_address_of_m_DecelerationRate_8() { return &___m_DecelerationRate_8; }
	inline void set_m_DecelerationRate_8(float value)
	{
		___m_DecelerationRate_8 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_9() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ScrollSensitivity_9)); }
	inline float get_m_ScrollSensitivity_9() const { return ___m_ScrollSensitivity_9; }
	inline float* get_address_of_m_ScrollSensitivity_9() { return &___m_ScrollSensitivity_9; }
	inline void set_m_ScrollSensitivity_9(float value)
	{
		___m_ScrollSensitivity_9 = value;
	}

	inline static int32_t get_offset_of_m_Viewport_10() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Viewport_10)); }
	inline RectTransform_t3349966182 * get_m_Viewport_10() const { return ___m_Viewport_10; }
	inline RectTransform_t3349966182 ** get_address_of_m_Viewport_10() { return &___m_Viewport_10; }
	inline void set_m_Viewport_10(RectTransform_t3349966182 * value)
	{
		___m_Viewport_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Viewport_10), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbar_11() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbar_11)); }
	inline Scrollbar_t3248359358 * get_m_HorizontalScrollbar_11() const { return ___m_HorizontalScrollbar_11; }
	inline Scrollbar_t3248359358 ** get_address_of_m_HorizontalScrollbar_11() { return &___m_HorizontalScrollbar_11; }
	inline void set_m_HorizontalScrollbar_11(Scrollbar_t3248359358 * value)
	{
		___m_HorizontalScrollbar_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbar_11), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_12() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbar_12)); }
	inline Scrollbar_t3248359358 * get_m_VerticalScrollbar_12() const { return ___m_VerticalScrollbar_12; }
	inline Scrollbar_t3248359358 ** get_address_of_m_VerticalScrollbar_12() { return &___m_VerticalScrollbar_12; }
	inline void set_m_VerticalScrollbar_12(Scrollbar_t3248359358 * value)
	{
		___m_VerticalScrollbar_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_12), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarVisibility_13() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbarVisibility_13)); }
	inline int32_t get_m_HorizontalScrollbarVisibility_13() const { return ___m_HorizontalScrollbarVisibility_13; }
	inline int32_t* get_address_of_m_HorizontalScrollbarVisibility_13() { return &___m_HorizontalScrollbarVisibility_13; }
	inline void set_m_HorizontalScrollbarVisibility_13(int32_t value)
	{
		___m_HorizontalScrollbarVisibility_13 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarVisibility_14() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbarVisibility_14)); }
	inline int32_t get_m_VerticalScrollbarVisibility_14() const { return ___m_VerticalScrollbarVisibility_14; }
	inline int32_t* get_address_of_m_VerticalScrollbarVisibility_14() { return &___m_VerticalScrollbarVisibility_14; }
	inline void set_m_VerticalScrollbarVisibility_14(int32_t value)
	{
		___m_VerticalScrollbarVisibility_14 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarSpacing_15() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbarSpacing_15)); }
	inline float get_m_HorizontalScrollbarSpacing_15() const { return ___m_HorizontalScrollbarSpacing_15; }
	inline float* get_address_of_m_HorizontalScrollbarSpacing_15() { return &___m_HorizontalScrollbarSpacing_15; }
	inline void set_m_HorizontalScrollbarSpacing_15(float value)
	{
		___m_HorizontalScrollbarSpacing_15 = value;
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarSpacing_16() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbarSpacing_16)); }
	inline float get_m_VerticalScrollbarSpacing_16() const { return ___m_VerticalScrollbarSpacing_16; }
	inline float* get_address_of_m_VerticalScrollbarSpacing_16() { return &___m_VerticalScrollbarSpacing_16; }
	inline void set_m_VerticalScrollbarSpacing_16(float value)
	{
		___m_VerticalScrollbarSpacing_16 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_17() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_OnValueChanged_17)); }
	inline ScrollRectEvent_t3529018992 * get_m_OnValueChanged_17() const { return ___m_OnValueChanged_17; }
	inline ScrollRectEvent_t3529018992 ** get_address_of_m_OnValueChanged_17() { return &___m_OnValueChanged_17; }
	inline void set_m_OnValueChanged_17(ScrollRectEvent_t3529018992 * value)
	{
		___m_OnValueChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_17), value);
	}

	inline static int32_t get_offset_of_m_PointerStartLocalCursor_18() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PointerStartLocalCursor_18)); }
	inline Vector2_t2243707579  get_m_PointerStartLocalCursor_18() const { return ___m_PointerStartLocalCursor_18; }
	inline Vector2_t2243707579 * get_address_of_m_PointerStartLocalCursor_18() { return &___m_PointerStartLocalCursor_18; }
	inline void set_m_PointerStartLocalCursor_18(Vector2_t2243707579  value)
	{
		___m_PointerStartLocalCursor_18 = value;
	}

	inline static int32_t get_offset_of_m_ContentStartPosition_19() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ContentStartPosition_19)); }
	inline Vector2_t2243707579  get_m_ContentStartPosition_19() const { return ___m_ContentStartPosition_19; }
	inline Vector2_t2243707579 * get_address_of_m_ContentStartPosition_19() { return &___m_ContentStartPosition_19; }
	inline void set_m_ContentStartPosition_19(Vector2_t2243707579  value)
	{
		___m_ContentStartPosition_19 = value;
	}

	inline static int32_t get_offset_of_m_ViewRect_20() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ViewRect_20)); }
	inline RectTransform_t3349966182 * get_m_ViewRect_20() const { return ___m_ViewRect_20; }
	inline RectTransform_t3349966182 ** get_address_of_m_ViewRect_20() { return &___m_ViewRect_20; }
	inline void set_m_ViewRect_20(RectTransform_t3349966182 * value)
	{
		___m_ViewRect_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ViewRect_20), value);
	}

	inline static int32_t get_offset_of_m_ContentBounds_21() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ContentBounds_21)); }
	inline Bounds_t3033363703  get_m_ContentBounds_21() const { return ___m_ContentBounds_21; }
	inline Bounds_t3033363703 * get_address_of_m_ContentBounds_21() { return &___m_ContentBounds_21; }
	inline void set_m_ContentBounds_21(Bounds_t3033363703  value)
	{
		___m_ContentBounds_21 = value;
	}

	inline static int32_t get_offset_of_m_ViewBounds_22() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_ViewBounds_22)); }
	inline Bounds_t3033363703  get_m_ViewBounds_22() const { return ___m_ViewBounds_22; }
	inline Bounds_t3033363703 * get_address_of_m_ViewBounds_22() { return &___m_ViewBounds_22; }
	inline void set_m_ViewBounds_22(Bounds_t3033363703  value)
	{
		___m_ViewBounds_22 = value;
	}

	inline static int32_t get_offset_of_m_Velocity_23() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Velocity_23)); }
	inline Vector2_t2243707579  get_m_Velocity_23() const { return ___m_Velocity_23; }
	inline Vector2_t2243707579 * get_address_of_m_Velocity_23() { return &___m_Velocity_23; }
	inline void set_m_Velocity_23(Vector2_t2243707579  value)
	{
		___m_Velocity_23 = value;
	}

	inline static int32_t get_offset_of_m_Dragging_24() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Dragging_24)); }
	inline bool get_m_Dragging_24() const { return ___m_Dragging_24; }
	inline bool* get_address_of_m_Dragging_24() { return &___m_Dragging_24; }
	inline void set_m_Dragging_24(bool value)
	{
		___m_Dragging_24 = value;
	}

	inline static int32_t get_offset_of_m_PrevPosition_25() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PrevPosition_25)); }
	inline Vector2_t2243707579  get_m_PrevPosition_25() const { return ___m_PrevPosition_25; }
	inline Vector2_t2243707579 * get_address_of_m_PrevPosition_25() { return &___m_PrevPosition_25; }
	inline void set_m_PrevPosition_25(Vector2_t2243707579  value)
	{
		___m_PrevPosition_25 = value;
	}

	inline static int32_t get_offset_of_m_PrevContentBounds_26() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PrevContentBounds_26)); }
	inline Bounds_t3033363703  get_m_PrevContentBounds_26() const { return ___m_PrevContentBounds_26; }
	inline Bounds_t3033363703 * get_address_of_m_PrevContentBounds_26() { return &___m_PrevContentBounds_26; }
	inline void set_m_PrevContentBounds_26(Bounds_t3033363703  value)
	{
		___m_PrevContentBounds_26 = value;
	}

	inline static int32_t get_offset_of_m_PrevViewBounds_27() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_PrevViewBounds_27)); }
	inline Bounds_t3033363703  get_m_PrevViewBounds_27() const { return ___m_PrevViewBounds_27; }
	inline Bounds_t3033363703 * get_address_of_m_PrevViewBounds_27() { return &___m_PrevViewBounds_27; }
	inline void set_m_PrevViewBounds_27(Bounds_t3033363703  value)
	{
		___m_PrevViewBounds_27 = value;
	}

	inline static int32_t get_offset_of_m_HasRebuiltLayout_28() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HasRebuiltLayout_28)); }
	inline bool get_m_HasRebuiltLayout_28() const { return ___m_HasRebuiltLayout_28; }
	inline bool* get_address_of_m_HasRebuiltLayout_28() { return &___m_HasRebuiltLayout_28; }
	inline void set_m_HasRebuiltLayout_28(bool value)
	{
		___m_HasRebuiltLayout_28 = value;
	}

	inline static int32_t get_offset_of_m_HSliderExpand_29() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HSliderExpand_29)); }
	inline bool get_m_HSliderExpand_29() const { return ___m_HSliderExpand_29; }
	inline bool* get_address_of_m_HSliderExpand_29() { return &___m_HSliderExpand_29; }
	inline void set_m_HSliderExpand_29(bool value)
	{
		___m_HSliderExpand_29 = value;
	}

	inline static int32_t get_offset_of_m_VSliderExpand_30() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VSliderExpand_30)); }
	inline bool get_m_VSliderExpand_30() const { return ___m_VSliderExpand_30; }
	inline bool* get_address_of_m_VSliderExpand_30() { return &___m_VSliderExpand_30; }
	inline void set_m_VSliderExpand_30(bool value)
	{
		___m_VSliderExpand_30 = value;
	}

	inline static int32_t get_offset_of_m_HSliderHeight_31() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HSliderHeight_31)); }
	inline float get_m_HSliderHeight_31() const { return ___m_HSliderHeight_31; }
	inline float* get_address_of_m_HSliderHeight_31() { return &___m_HSliderHeight_31; }
	inline void set_m_HSliderHeight_31(float value)
	{
		___m_HSliderHeight_31 = value;
	}

	inline static int32_t get_offset_of_m_VSliderWidth_32() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VSliderWidth_32)); }
	inline float get_m_VSliderWidth_32() const { return ___m_VSliderWidth_32; }
	inline float* get_address_of_m_VSliderWidth_32() { return &___m_VSliderWidth_32; }
	inline void set_m_VSliderWidth_32(float value)
	{
		___m_VSliderWidth_32 = value;
	}

	inline static int32_t get_offset_of_m_Rect_33() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Rect_33)); }
	inline RectTransform_t3349966182 * get_m_Rect_33() const { return ___m_Rect_33; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_33() { return &___m_Rect_33; }
	inline void set_m_Rect_33(RectTransform_t3349966182 * value)
	{
		___m_Rect_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_33), value);
	}

	inline static int32_t get_offset_of_m_HorizontalScrollbarRect_34() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_HorizontalScrollbarRect_34)); }
	inline RectTransform_t3349966182 * get_m_HorizontalScrollbarRect_34() const { return ___m_HorizontalScrollbarRect_34; }
	inline RectTransform_t3349966182 ** get_address_of_m_HorizontalScrollbarRect_34() { return &___m_HorizontalScrollbarRect_34; }
	inline void set_m_HorizontalScrollbarRect_34(RectTransform_t3349966182 * value)
	{
		___m_HorizontalScrollbarRect_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_HorizontalScrollbarRect_34), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarRect_35() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_VerticalScrollbarRect_35)); }
	inline RectTransform_t3349966182 * get_m_VerticalScrollbarRect_35() const { return ___m_VerticalScrollbarRect_35; }
	inline RectTransform_t3349966182 ** get_address_of_m_VerticalScrollbarRect_35() { return &___m_VerticalScrollbarRect_35; }
	inline void set_m_VerticalScrollbarRect_35(RectTransform_t3349966182 * value)
	{
		___m_VerticalScrollbarRect_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarRect_35), value);
	}

	inline static int32_t get_offset_of_m_Tracker_36() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Tracker_36)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_36() const { return ___m_Tracker_36; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_36() { return &___m_Tracker_36; }
	inline void set_m_Tracker_36(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_36 = value;
	}

	inline static int32_t get_offset_of_m_Corners_37() { return static_cast<int32_t>(offsetof(ScrollRect_t1199013257, ___m_Corners_37)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_37() const { return ___m_Corners_37; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_37() { return &___m_Corners_37; }
	inline void set_m_Corners_37(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLRECT_T1199013257_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef BASEMESHEFFECT_T1728560551_H
#define BASEMESHEFFECT_T1728560551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t1728560551  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t2426225576 * ___m_Graphic_2;

public:
	inline static int32_t get_offset_of_m_Graphic_2() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t1728560551, ___m_Graphic_2)); }
	inline Graphic_t2426225576 * get_m_Graphic_2() const { return ___m_Graphic_2; }
	inline Graphic_t2426225576 ** get_address_of_m_Graphic_2() { return &___m_Graphic_2; }
	inline void set_m_Graphic_2(Graphic_t2426225576 * value)
	{
		___m_Graphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T1728560551_H
#ifndef TOGGLEGROUP_T1030026315_H
#define TOGGLEGROUP_T1030026315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ToggleGroup
struct  ToggleGroup_t1030026315  : public UIBehaviour_t3960014691
{
public:
	// System.Boolean UnityEngine.UI.ToggleGroup::m_AllowSwitchOff
	bool ___m_AllowSwitchOff_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::m_Toggles
	List_1_t3345875600 * ___m_Toggles_3;

public:
	inline static int32_t get_offset_of_m_AllowSwitchOff_2() { return static_cast<int32_t>(offsetof(ToggleGroup_t1030026315, ___m_AllowSwitchOff_2)); }
	inline bool get_m_AllowSwitchOff_2() const { return ___m_AllowSwitchOff_2; }
	inline bool* get_address_of_m_AllowSwitchOff_2() { return &___m_AllowSwitchOff_2; }
	inline void set_m_AllowSwitchOff_2(bool value)
	{
		___m_AllowSwitchOff_2 = value;
	}

	inline static int32_t get_offset_of_m_Toggles_3() { return static_cast<int32_t>(offsetof(ToggleGroup_t1030026315, ___m_Toggles_3)); }
	inline List_1_t3345875600 * get_m_Toggles_3() const { return ___m_Toggles_3; }
	inline List_1_t3345875600 ** get_address_of_m_Toggles_3() { return &___m_Toggles_3; }
	inline void set_m_Toggles_3(List_1_t3345875600 * value)
	{
		___m_Toggles_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Toggles_3), value);
	}
};

struct ToggleGroup_t1030026315_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::<>f__am$cache0
	Predicate_1_t2419724583 * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<UnityEngine.UI.Toggle,System.Boolean> UnityEngine.UI.ToggleGroup::<>f__am$cache1
	Func_2_t2318645467 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(ToggleGroup_t1030026315_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2419724583 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2419724583 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2419724583 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(ToggleGroup_t1030026315_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_t2318645467 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_t2318645467 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_t2318645467 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLEGROUP_T1030026315_H
#ifndef CANVASSCALER_T2574720772_H
#define CANVASSCALER_T2574720772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler
struct  CanvasScaler_t2574720772  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::m_UiScaleMode
	int32_t ___m_UiScaleMode_2;
	// System.Single UnityEngine.UI.CanvasScaler::m_ReferencePixelsPerUnit
	float ___m_ReferencePixelsPerUnit_3;
	// System.Single UnityEngine.UI.CanvasScaler::m_ScaleFactor
	float ___m_ScaleFactor_4;
	// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::m_ReferenceResolution
	Vector2_t2243707579  ___m_ReferenceResolution_5;
	// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::m_ScreenMatchMode
	int32_t ___m_ScreenMatchMode_6;
	// System.Single UnityEngine.UI.CanvasScaler::m_MatchWidthOrHeight
	float ___m_MatchWidthOrHeight_7;
	// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::m_PhysicalUnit
	int32_t ___m_PhysicalUnit_9;
	// System.Single UnityEngine.UI.CanvasScaler::m_FallbackScreenDPI
	float ___m_FallbackScreenDPI_10;
	// System.Single UnityEngine.UI.CanvasScaler::m_DefaultSpriteDPI
	float ___m_DefaultSpriteDPI_11;
	// System.Single UnityEngine.UI.CanvasScaler::m_DynamicPixelsPerUnit
	float ___m_DynamicPixelsPerUnit_12;
	// UnityEngine.Canvas UnityEngine.UI.CanvasScaler::m_Canvas
	Canvas_t209405766 * ___m_Canvas_13;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevScaleFactor
	float ___m_PrevScaleFactor_14;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevReferencePixelsPerUnit
	float ___m_PrevReferencePixelsPerUnit_15;

public:
	inline static int32_t get_offset_of_m_UiScaleMode_2() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_UiScaleMode_2)); }
	inline int32_t get_m_UiScaleMode_2() const { return ___m_UiScaleMode_2; }
	inline int32_t* get_address_of_m_UiScaleMode_2() { return &___m_UiScaleMode_2; }
	inline void set_m_UiScaleMode_2(int32_t value)
	{
		___m_UiScaleMode_2 = value;
	}

	inline static int32_t get_offset_of_m_ReferencePixelsPerUnit_3() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ReferencePixelsPerUnit_3)); }
	inline float get_m_ReferencePixelsPerUnit_3() const { return ___m_ReferencePixelsPerUnit_3; }
	inline float* get_address_of_m_ReferencePixelsPerUnit_3() { return &___m_ReferencePixelsPerUnit_3; }
	inline void set_m_ReferencePixelsPerUnit_3(float value)
	{
		___m_ReferencePixelsPerUnit_3 = value;
	}

	inline static int32_t get_offset_of_m_ScaleFactor_4() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ScaleFactor_4)); }
	inline float get_m_ScaleFactor_4() const { return ___m_ScaleFactor_4; }
	inline float* get_address_of_m_ScaleFactor_4() { return &___m_ScaleFactor_4; }
	inline void set_m_ScaleFactor_4(float value)
	{
		___m_ScaleFactor_4 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceResolution_5() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ReferenceResolution_5)); }
	inline Vector2_t2243707579  get_m_ReferenceResolution_5() const { return ___m_ReferenceResolution_5; }
	inline Vector2_t2243707579 * get_address_of_m_ReferenceResolution_5() { return &___m_ReferenceResolution_5; }
	inline void set_m_ReferenceResolution_5(Vector2_t2243707579  value)
	{
		___m_ReferenceResolution_5 = value;
	}

	inline static int32_t get_offset_of_m_ScreenMatchMode_6() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_ScreenMatchMode_6)); }
	inline int32_t get_m_ScreenMatchMode_6() const { return ___m_ScreenMatchMode_6; }
	inline int32_t* get_address_of_m_ScreenMatchMode_6() { return &___m_ScreenMatchMode_6; }
	inline void set_m_ScreenMatchMode_6(int32_t value)
	{
		___m_ScreenMatchMode_6 = value;
	}

	inline static int32_t get_offset_of_m_MatchWidthOrHeight_7() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_MatchWidthOrHeight_7)); }
	inline float get_m_MatchWidthOrHeight_7() const { return ___m_MatchWidthOrHeight_7; }
	inline float* get_address_of_m_MatchWidthOrHeight_7() { return &___m_MatchWidthOrHeight_7; }
	inline void set_m_MatchWidthOrHeight_7(float value)
	{
		___m_MatchWidthOrHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_PhysicalUnit_9() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_PhysicalUnit_9)); }
	inline int32_t get_m_PhysicalUnit_9() const { return ___m_PhysicalUnit_9; }
	inline int32_t* get_address_of_m_PhysicalUnit_9() { return &___m_PhysicalUnit_9; }
	inline void set_m_PhysicalUnit_9(int32_t value)
	{
		___m_PhysicalUnit_9 = value;
	}

	inline static int32_t get_offset_of_m_FallbackScreenDPI_10() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_FallbackScreenDPI_10)); }
	inline float get_m_FallbackScreenDPI_10() const { return ___m_FallbackScreenDPI_10; }
	inline float* get_address_of_m_FallbackScreenDPI_10() { return &___m_FallbackScreenDPI_10; }
	inline void set_m_FallbackScreenDPI_10(float value)
	{
		___m_FallbackScreenDPI_10 = value;
	}

	inline static int32_t get_offset_of_m_DefaultSpriteDPI_11() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_DefaultSpriteDPI_11)); }
	inline float get_m_DefaultSpriteDPI_11() const { return ___m_DefaultSpriteDPI_11; }
	inline float* get_address_of_m_DefaultSpriteDPI_11() { return &___m_DefaultSpriteDPI_11; }
	inline void set_m_DefaultSpriteDPI_11(float value)
	{
		___m_DefaultSpriteDPI_11 = value;
	}

	inline static int32_t get_offset_of_m_DynamicPixelsPerUnit_12() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_DynamicPixelsPerUnit_12)); }
	inline float get_m_DynamicPixelsPerUnit_12() const { return ___m_DynamicPixelsPerUnit_12; }
	inline float* get_address_of_m_DynamicPixelsPerUnit_12() { return &___m_DynamicPixelsPerUnit_12; }
	inline void set_m_DynamicPixelsPerUnit_12(float value)
	{
		___m_DynamicPixelsPerUnit_12 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_Canvas_13)); }
	inline Canvas_t209405766 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_t209405766 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_13), value);
	}

	inline static int32_t get_offset_of_m_PrevScaleFactor_14() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_PrevScaleFactor_14)); }
	inline float get_m_PrevScaleFactor_14() const { return ___m_PrevScaleFactor_14; }
	inline float* get_address_of_m_PrevScaleFactor_14() { return &___m_PrevScaleFactor_14; }
	inline void set_m_PrevScaleFactor_14(float value)
	{
		___m_PrevScaleFactor_14 = value;
	}

	inline static int32_t get_offset_of_m_PrevReferencePixelsPerUnit_15() { return static_cast<int32_t>(offsetof(CanvasScaler_t2574720772, ___m_PrevReferencePixelsPerUnit_15)); }
	inline float get_m_PrevReferencePixelsPerUnit_15() const { return ___m_PrevReferencePixelsPerUnit_15; }
	inline float* get_address_of_m_PrevReferencePixelsPerUnit_15() { return &___m_PrevReferencePixelsPerUnit_15; }
	inline void set_m_PrevReferencePixelsPerUnit_15(float value)
	{
		___m_PrevReferencePixelsPerUnit_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSCALER_T2574720772_H
#ifndef CONTENTSIZEFITTER_T1325211874_H
#define CONTENTSIZEFITTER_T1325211874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter
struct  ContentSizeFitter_t1325211874  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_HorizontalFit
	int32_t ___m_HorizontalFit_2;
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_VerticalFit
	int32_t ___m_VerticalFit_3;
	// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::m_Rect
	RectTransform_t3349966182 * ___m_Rect_4;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ContentSizeFitter::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_5;

public:
	inline static int32_t get_offset_of_m_HorizontalFit_2() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_HorizontalFit_2)); }
	inline int32_t get_m_HorizontalFit_2() const { return ___m_HorizontalFit_2; }
	inline int32_t* get_address_of_m_HorizontalFit_2() { return &___m_HorizontalFit_2; }
	inline void set_m_HorizontalFit_2(int32_t value)
	{
		___m_HorizontalFit_2 = value;
	}

	inline static int32_t get_offset_of_m_VerticalFit_3() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_VerticalFit_3)); }
	inline int32_t get_m_VerticalFit_3() const { return ___m_VerticalFit_3; }
	inline int32_t* get_address_of_m_VerticalFit_3() { return &___m_VerticalFit_3; }
	inline void set_m_VerticalFit_3(int32_t value)
	{
		___m_VerticalFit_3 = value;
	}

	inline static int32_t get_offset_of_m_Rect_4() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_Rect_4)); }
	inline RectTransform_t3349966182 * get_m_Rect_4() const { return ___m_Rect_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_Rect_4() { return &___m_Rect_4; }
	inline void set_m_Rect_4(RectTransform_t3349966182 * value)
	{
		___m_Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_4), value);
	}

	inline static int32_t get_offset_of_m_Tracker_5() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t1325211874, ___m_Tracker_5)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_5() const { return ___m_Tracker_5; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_5() { return &___m_Tracker_5; }
	inline void set_m_Tracker_5(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTSIZEFITTER_T1325211874_H
#ifndef SELECTABLE_T1490392188_H
#define SELECTABLE_T1490392188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t1490392188  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1571958496  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2652774230  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1353336012  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t3244928895 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t2426225576 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2665681875 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Navigation_3)); }
	inline Navigation_t1571958496  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t1571958496 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t1571958496  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Colors_5)); }
	inline ColorBlock_t2652774230  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2652774230 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2652774230  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_SpriteState_6)); }
	inline SpriteState_t1353336012  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1353336012 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1353336012  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t3244928895 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t3244928895 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t3244928895 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_TargetGraphic_9)); }
	inline Graphic_t2426225576 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t2426225576 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t2426225576 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CanvasGroupCache_15)); }
	inline List_1_t2665681875 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t2665681875 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t2665681875 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t1490392188_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t859513320 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t1490392188_StaticFields, ___s_List_2)); }
	inline List_1_t859513320 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t859513320 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t859513320 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T1490392188_H
#ifndef RECTMASK2D_T1156185964_H
#define RECTMASK2D_T1156185964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RectMask2D
struct  RectMask2D_t1156185964  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.RectangularVertexClipper UnityEngine.UI.RectMask2D::m_VertexClipper
	RectangularVertexClipper_t3349113845 * ___m_VertexClipper_2;
	// UnityEngine.RectTransform UnityEngine.UI.RectMask2D::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_3;
	// System.Collections.Generic.HashSet`1<UnityEngine.UI.IClippable> UnityEngine.UI.RectMask2D::m_ClipTargets
	HashSet_1_t274736911 * ___m_ClipTargets_4;
	// System.Boolean UnityEngine.UI.RectMask2D::m_ShouldRecalculateClipRects
	bool ___m_ShouldRecalculateClipRects_5;
	// System.Collections.Generic.List`1<UnityEngine.UI.RectMask2D> UnityEngine.UI.RectMask2D::m_Clippers
	List_1_t525307096 * ___m_Clippers_6;
	// UnityEngine.Rect UnityEngine.UI.RectMask2D::m_LastClipRectCanvasSpace
	Rect_t3681755626  ___m_LastClipRectCanvasSpace_7;
	// System.Boolean UnityEngine.UI.RectMask2D::m_LastValidClipRect
	bool ___m_LastValidClipRect_8;
	// System.Boolean UnityEngine.UI.RectMask2D::m_ForceClip
	bool ___m_ForceClip_9;

public:
	inline static int32_t get_offset_of_m_VertexClipper_2() { return static_cast<int32_t>(offsetof(RectMask2D_t1156185964, ___m_VertexClipper_2)); }
	inline RectangularVertexClipper_t3349113845 * get_m_VertexClipper_2() const { return ___m_VertexClipper_2; }
	inline RectangularVertexClipper_t3349113845 ** get_address_of_m_VertexClipper_2() { return &___m_VertexClipper_2; }
	inline void set_m_VertexClipper_2(RectangularVertexClipper_t3349113845 * value)
	{
		___m_VertexClipper_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_VertexClipper_2), value);
	}

	inline static int32_t get_offset_of_m_RectTransform_3() { return static_cast<int32_t>(offsetof(RectMask2D_t1156185964, ___m_RectTransform_3)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_3() const { return ___m_RectTransform_3; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_3() { return &___m_RectTransform_3; }
	inline void set_m_RectTransform_3(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_3), value);
	}

	inline static int32_t get_offset_of_m_ClipTargets_4() { return static_cast<int32_t>(offsetof(RectMask2D_t1156185964, ___m_ClipTargets_4)); }
	inline HashSet_1_t274736911 * get_m_ClipTargets_4() const { return ___m_ClipTargets_4; }
	inline HashSet_1_t274736911 ** get_address_of_m_ClipTargets_4() { return &___m_ClipTargets_4; }
	inline void set_m_ClipTargets_4(HashSet_1_t274736911 * value)
	{
		___m_ClipTargets_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClipTargets_4), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculateClipRects_5() { return static_cast<int32_t>(offsetof(RectMask2D_t1156185964, ___m_ShouldRecalculateClipRects_5)); }
	inline bool get_m_ShouldRecalculateClipRects_5() const { return ___m_ShouldRecalculateClipRects_5; }
	inline bool* get_address_of_m_ShouldRecalculateClipRects_5() { return &___m_ShouldRecalculateClipRects_5; }
	inline void set_m_ShouldRecalculateClipRects_5(bool value)
	{
		___m_ShouldRecalculateClipRects_5 = value;
	}

	inline static int32_t get_offset_of_m_Clippers_6() { return static_cast<int32_t>(offsetof(RectMask2D_t1156185964, ___m_Clippers_6)); }
	inline List_1_t525307096 * get_m_Clippers_6() const { return ___m_Clippers_6; }
	inline List_1_t525307096 ** get_address_of_m_Clippers_6() { return &___m_Clippers_6; }
	inline void set_m_Clippers_6(List_1_t525307096 * value)
	{
		___m_Clippers_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Clippers_6), value);
	}

	inline static int32_t get_offset_of_m_LastClipRectCanvasSpace_7() { return static_cast<int32_t>(offsetof(RectMask2D_t1156185964, ___m_LastClipRectCanvasSpace_7)); }
	inline Rect_t3681755626  get_m_LastClipRectCanvasSpace_7() const { return ___m_LastClipRectCanvasSpace_7; }
	inline Rect_t3681755626 * get_address_of_m_LastClipRectCanvasSpace_7() { return &___m_LastClipRectCanvasSpace_7; }
	inline void set_m_LastClipRectCanvasSpace_7(Rect_t3681755626  value)
	{
		___m_LastClipRectCanvasSpace_7 = value;
	}

	inline static int32_t get_offset_of_m_LastValidClipRect_8() { return static_cast<int32_t>(offsetof(RectMask2D_t1156185964, ___m_LastValidClipRect_8)); }
	inline bool get_m_LastValidClipRect_8() const { return ___m_LastValidClipRect_8; }
	inline bool* get_address_of_m_LastValidClipRect_8() { return &___m_LastValidClipRect_8; }
	inline void set_m_LastValidClipRect_8(bool value)
	{
		___m_LastValidClipRect_8 = value;
	}

	inline static int32_t get_offset_of_m_ForceClip_9() { return static_cast<int32_t>(offsetof(RectMask2D_t1156185964, ___m_ForceClip_9)); }
	inline bool get_m_ForceClip_9() const { return ___m_ForceClip_9; }
	inline bool* get_address_of_m_ForceClip_9() { return &___m_ForceClip_9; }
	inline void set_m_ForceClip_9(bool value)
	{
		___m_ForceClip_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTMASK2D_T1156185964_H
#ifndef POSITIONASUV1_T1102546563_H
#define POSITIONASUV1_T1102546563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t1102546563  : public BaseMeshEffect_t1728560551
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T1102546563_H
#ifndef SCROLLBAR_T3248359358_H
#define SCROLLBAR_T3248359358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Scrollbar
struct  Scrollbar_t3248359358  : public Selectable_t1490392188
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_HandleRect
	RectTransform_t3349966182 * ___m_HandleRect_16;
	// UnityEngine.UI.Scrollbar/Direction UnityEngine.UI.Scrollbar::m_Direction
	int32_t ___m_Direction_17;
	// System.Single UnityEngine.UI.Scrollbar::m_Value
	float ___m_Value_18;
	// System.Single UnityEngine.UI.Scrollbar::m_Size
	float ___m_Size_19;
	// System.Int32 UnityEngine.UI.Scrollbar::m_NumberOfSteps
	int32_t ___m_NumberOfSteps_20;
	// UnityEngine.UI.Scrollbar/ScrollEvent UnityEngine.UI.Scrollbar::m_OnValueChanged
	ScrollEvent_t1794825321 * ___m_OnValueChanged_21;
	// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::m_ContainerRect
	RectTransform_t3349966182 * ___m_ContainerRect_22;
	// UnityEngine.Vector2 UnityEngine.UI.Scrollbar::m_Offset
	Vector2_t2243707579  ___m_Offset_23;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Scrollbar::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_24;
	// UnityEngine.Coroutine UnityEngine.UI.Scrollbar::m_PointerDownRepeat
	Coroutine_t2299508840 * ___m_PointerDownRepeat_25;
	// System.Boolean UnityEngine.UI.Scrollbar::isPointerDownAndNotDragging
	bool ___isPointerDownAndNotDragging_26;

public:
	inline static int32_t get_offset_of_m_HandleRect_16() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_HandleRect_16)); }
	inline RectTransform_t3349966182 * get_m_HandleRect_16() const { return ___m_HandleRect_16; }
	inline RectTransform_t3349966182 ** get_address_of_m_HandleRect_16() { return &___m_HandleRect_16; }
	inline void set_m_HandleRect_16(RectTransform_t3349966182 * value)
	{
		___m_HandleRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_16), value);
	}

	inline static int32_t get_offset_of_m_Direction_17() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_Direction_17)); }
	inline int32_t get_m_Direction_17() const { return ___m_Direction_17; }
	inline int32_t* get_address_of_m_Direction_17() { return &___m_Direction_17; }
	inline void set_m_Direction_17(int32_t value)
	{
		___m_Direction_17 = value;
	}

	inline static int32_t get_offset_of_m_Value_18() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_Value_18)); }
	inline float get_m_Value_18() const { return ___m_Value_18; }
	inline float* get_address_of_m_Value_18() { return &___m_Value_18; }
	inline void set_m_Value_18(float value)
	{
		___m_Value_18 = value;
	}

	inline static int32_t get_offset_of_m_Size_19() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_Size_19)); }
	inline float get_m_Size_19() const { return ___m_Size_19; }
	inline float* get_address_of_m_Size_19() { return &___m_Size_19; }
	inline void set_m_Size_19(float value)
	{
		___m_Size_19 = value;
	}

	inline static int32_t get_offset_of_m_NumberOfSteps_20() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_NumberOfSteps_20)); }
	inline int32_t get_m_NumberOfSteps_20() const { return ___m_NumberOfSteps_20; }
	inline int32_t* get_address_of_m_NumberOfSteps_20() { return &___m_NumberOfSteps_20; }
	inline void set_m_NumberOfSteps_20(int32_t value)
	{
		___m_NumberOfSteps_20 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_21() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_OnValueChanged_21)); }
	inline ScrollEvent_t1794825321 * get_m_OnValueChanged_21() const { return ___m_OnValueChanged_21; }
	inline ScrollEvent_t1794825321 ** get_address_of_m_OnValueChanged_21() { return &___m_OnValueChanged_21; }
	inline void set_m_OnValueChanged_21(ScrollEvent_t1794825321 * value)
	{
		___m_OnValueChanged_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_21), value);
	}

	inline static int32_t get_offset_of_m_ContainerRect_22() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_ContainerRect_22)); }
	inline RectTransform_t3349966182 * get_m_ContainerRect_22() const { return ___m_ContainerRect_22; }
	inline RectTransform_t3349966182 ** get_address_of_m_ContainerRect_22() { return &___m_ContainerRect_22; }
	inline void set_m_ContainerRect_22(RectTransform_t3349966182 * value)
	{
		___m_ContainerRect_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ContainerRect_22), value);
	}

	inline static int32_t get_offset_of_m_Offset_23() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_Offset_23)); }
	inline Vector2_t2243707579  get_m_Offset_23() const { return ___m_Offset_23; }
	inline Vector2_t2243707579 * get_address_of_m_Offset_23() { return &___m_Offset_23; }
	inline void set_m_Offset_23(Vector2_t2243707579  value)
	{
		___m_Offset_23 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_24() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_Tracker_24)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_24() const { return ___m_Tracker_24; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_24() { return &___m_Tracker_24; }
	inline void set_m_Tracker_24(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_24 = value;
	}

	inline static int32_t get_offset_of_m_PointerDownRepeat_25() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___m_PointerDownRepeat_25)); }
	inline Coroutine_t2299508840 * get_m_PointerDownRepeat_25() const { return ___m_PointerDownRepeat_25; }
	inline Coroutine_t2299508840 ** get_address_of_m_PointerDownRepeat_25() { return &___m_PointerDownRepeat_25; }
	inline void set_m_PointerDownRepeat_25(Coroutine_t2299508840 * value)
	{
		___m_PointerDownRepeat_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerDownRepeat_25), value);
	}

	inline static int32_t get_offset_of_isPointerDownAndNotDragging_26() { return static_cast<int32_t>(offsetof(Scrollbar_t3248359358, ___isPointerDownAndNotDragging_26)); }
	inline bool get_isPointerDownAndNotDragging_26() const { return ___isPointerDownAndNotDragging_26; }
	inline bool* get_address_of_isPointerDownAndNotDragging_26() { return &___isPointerDownAndNotDragging_26; }
	inline void set_isPointerDownAndNotDragging_26(bool value)
	{
		___isPointerDownAndNotDragging_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLBAR_T3248359358_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef GRIDLAYOUTGROUP_T1515633077_H
#define GRIDLAYOUTGROUP_T1515633077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup
struct  GridLayoutGroup_t1515633077  : public LayoutGroup_t3962498969
{
public:
	// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::m_StartCorner
	int32_t ___m_StartCorner_10;
	// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::m_StartAxis
	int32_t ___m_StartAxis_11;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_CellSize
	Vector2_t2243707579  ___m_CellSize_12;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_Spacing
	Vector2_t2243707579  ___m_Spacing_13;
	// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::m_Constraint
	int32_t ___m_Constraint_14;
	// System.Int32 UnityEngine.UI.GridLayoutGroup::m_ConstraintCount
	int32_t ___m_ConstraintCount_15;

public:
	inline static int32_t get_offset_of_m_StartCorner_10() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_StartCorner_10)); }
	inline int32_t get_m_StartCorner_10() const { return ___m_StartCorner_10; }
	inline int32_t* get_address_of_m_StartCorner_10() { return &___m_StartCorner_10; }
	inline void set_m_StartCorner_10(int32_t value)
	{
		___m_StartCorner_10 = value;
	}

	inline static int32_t get_offset_of_m_StartAxis_11() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_StartAxis_11)); }
	inline int32_t get_m_StartAxis_11() const { return ___m_StartAxis_11; }
	inline int32_t* get_address_of_m_StartAxis_11() { return &___m_StartAxis_11; }
	inline void set_m_StartAxis_11(int32_t value)
	{
		___m_StartAxis_11 = value;
	}

	inline static int32_t get_offset_of_m_CellSize_12() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_CellSize_12)); }
	inline Vector2_t2243707579  get_m_CellSize_12() const { return ___m_CellSize_12; }
	inline Vector2_t2243707579 * get_address_of_m_CellSize_12() { return &___m_CellSize_12; }
	inline void set_m_CellSize_12(Vector2_t2243707579  value)
	{
		___m_CellSize_12 = value;
	}

	inline static int32_t get_offset_of_m_Spacing_13() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_Spacing_13)); }
	inline Vector2_t2243707579  get_m_Spacing_13() const { return ___m_Spacing_13; }
	inline Vector2_t2243707579 * get_address_of_m_Spacing_13() { return &___m_Spacing_13; }
	inline void set_m_Spacing_13(Vector2_t2243707579  value)
	{
		___m_Spacing_13 = value;
	}

	inline static int32_t get_offset_of_m_Constraint_14() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_Constraint_14)); }
	inline int32_t get_m_Constraint_14() const { return ___m_Constraint_14; }
	inline int32_t* get_address_of_m_Constraint_14() { return &___m_Constraint_14; }
	inline void set_m_Constraint_14(int32_t value)
	{
		___m_Constraint_14 = value;
	}

	inline static int32_t get_offset_of_m_ConstraintCount_15() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t1515633077, ___m_ConstraintCount_15)); }
	inline int32_t get_m_ConstraintCount_15() const { return ___m_ConstraintCount_15; }
	inline int32_t* get_address_of_m_ConstraintCount_15() { return &___m_ConstraintCount_15; }
	inline void set_m_ConstraintCount_15(int32_t value)
	{
		___m_ConstraintCount_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDLAYOUTGROUP_T1515633077_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t1968298610  : public LayoutGroup_t3962498969
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_10;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_11;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_14;

public:
	inline static int32_t get_offset_of_m_Spacing_10() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_Spacing_10)); }
	inline float get_m_Spacing_10() const { return ___m_Spacing_10; }
	inline float* get_address_of_m_Spacing_10() { return &___m_Spacing_10; }
	inline void set_m_Spacing_10(float value)
	{
		___m_Spacing_10 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_11() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildForceExpandWidth_11)); }
	inline bool get_m_ChildForceExpandWidth_11() const { return ___m_ChildForceExpandWidth_11; }
	inline bool* get_address_of_m_ChildForceExpandWidth_11() { return &___m_ChildForceExpandWidth_11; }
	inline void set_m_ChildForceExpandWidth_11(bool value)
	{
		___m_ChildForceExpandWidth_11 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildForceExpandHeight_12)); }
	inline bool get_m_ChildForceExpandHeight_12() const { return ___m_ChildForceExpandHeight_12; }
	inline bool* get_address_of_m_ChildForceExpandHeight_12() { return &___m_ChildForceExpandHeight_12; }
	inline void set_m_ChildForceExpandHeight_12(bool value)
	{
		___m_ChildForceExpandHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildControlWidth_13)); }
	inline bool get_m_ChildControlWidth_13() const { return ___m_ChildControlWidth_13; }
	inline bool* get_address_of_m_ChildControlWidth_13() { return &___m_ChildControlWidth_13; }
	inline void set_m_ChildControlWidth_13(bool value)
	{
		___m_ChildControlWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t1968298610, ___m_ChildControlHeight_14)); }
	inline bool get_m_ChildControlHeight_14() const { return ___m_ChildControlHeight_14; }
	inline bool* get_address_of_m_ChildControlHeight_14() { return &___m_ChildControlHeight_14; }
	inline void set_m_ChildControlHeight_14(bool value)
	{
		___m_ChildControlHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T1968298610_H
#ifndef SHADOW_T4269599528_H
#define SHADOW_T4269599528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t4269599528  : public BaseMeshEffect_t1728560551
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2020392075  ___m_EffectColor_3;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2243707579  ___m_EffectDistance_4;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_5;

public:
	inline static int32_t get_offset_of_m_EffectColor_3() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectColor_3)); }
	inline Color_t2020392075  get_m_EffectColor_3() const { return ___m_EffectColor_3; }
	inline Color_t2020392075 * get_address_of_m_EffectColor_3() { return &___m_EffectColor_3; }
	inline void set_m_EffectColor_3(Color_t2020392075  value)
	{
		___m_EffectColor_3 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_4() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_EffectDistance_4)); }
	inline Vector2_t2243707579  get_m_EffectDistance_4() const { return ___m_EffectDistance_4; }
	inline Vector2_t2243707579 * get_address_of_m_EffectDistance_4() { return &___m_EffectDistance_4; }
	inline void set_m_EffectDistance_4(Vector2_t2243707579  value)
	{
		___m_EffectDistance_4 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_5() { return static_cast<int32_t>(offsetof(Shadow_t4269599528, ___m_UseGraphicAlpha_5)); }
	inline bool get_m_UseGraphicAlpha_5() const { return ___m_UseGraphicAlpha_5; }
	inline bool* get_address_of_m_UseGraphicAlpha_5() { return &___m_UseGraphicAlpha_5; }
	inline void set_m_UseGraphicAlpha_5(bool value)
	{
		___m_UseGraphicAlpha_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T4269599528_H
#ifndef SLIDER_T297367283_H
#define SLIDER_T297367283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Slider
struct  Slider_t297367283  : public Selectable_t1490392188
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t3349966182 * ___m_FillRect_16;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t3349966182 * ___m_HandleRect_17;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_18;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_19;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_20;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_21;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_22;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t2111116400 * ___m_OnValueChanged_23;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t2042527209 * ___m_FillImage_24;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_t3275118058 * ___m_FillTransform_25;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t3349966182 * ___m_FillContainerRect_26;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_t3275118058 * ___m_HandleTransform_27;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t3349966182 * ___m_HandleContainerRect_28;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_t2243707579  ___m_Offset_29;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t154385424  ___m_Tracker_30;

public:
	inline static int32_t get_offset_of_m_FillRect_16() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_FillRect_16)); }
	inline RectTransform_t3349966182 * get_m_FillRect_16() const { return ___m_FillRect_16; }
	inline RectTransform_t3349966182 ** get_address_of_m_FillRect_16() { return &___m_FillRect_16; }
	inline void set_m_FillRect_16(RectTransform_t3349966182 * value)
	{
		___m_FillRect_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillRect_16), value);
	}

	inline static int32_t get_offset_of_m_HandleRect_17() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_HandleRect_17)); }
	inline RectTransform_t3349966182 * get_m_HandleRect_17() const { return ___m_HandleRect_17; }
	inline RectTransform_t3349966182 ** get_address_of_m_HandleRect_17() { return &___m_HandleRect_17; }
	inline void set_m_HandleRect_17(RectTransform_t3349966182 * value)
	{
		___m_HandleRect_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleRect_17), value);
	}

	inline static int32_t get_offset_of_m_Direction_18() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_Direction_18)); }
	inline int32_t get_m_Direction_18() const { return ___m_Direction_18; }
	inline int32_t* get_address_of_m_Direction_18() { return &___m_Direction_18; }
	inline void set_m_Direction_18(int32_t value)
	{
		___m_Direction_18 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_19() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_MinValue_19)); }
	inline float get_m_MinValue_19() const { return ___m_MinValue_19; }
	inline float* get_address_of_m_MinValue_19() { return &___m_MinValue_19; }
	inline void set_m_MinValue_19(float value)
	{
		___m_MinValue_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_20() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_MaxValue_20)); }
	inline float get_m_MaxValue_20() const { return ___m_MaxValue_20; }
	inline float* get_address_of_m_MaxValue_20() { return &___m_MaxValue_20; }
	inline void set_m_MaxValue_20(float value)
	{
		___m_MaxValue_20 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_21() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_WholeNumbers_21)); }
	inline bool get_m_WholeNumbers_21() const { return ___m_WholeNumbers_21; }
	inline bool* get_address_of_m_WholeNumbers_21() { return &___m_WholeNumbers_21; }
	inline void set_m_WholeNumbers_21(bool value)
	{
		___m_WholeNumbers_21 = value;
	}

	inline static int32_t get_offset_of_m_Value_22() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_Value_22)); }
	inline float get_m_Value_22() const { return ___m_Value_22; }
	inline float* get_address_of_m_Value_22() { return &___m_Value_22; }
	inline void set_m_Value_22(float value)
	{
		___m_Value_22 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_23() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_OnValueChanged_23)); }
	inline SliderEvent_t2111116400 * get_m_OnValueChanged_23() const { return ___m_OnValueChanged_23; }
	inline SliderEvent_t2111116400 ** get_address_of_m_OnValueChanged_23() { return &___m_OnValueChanged_23; }
	inline void set_m_OnValueChanged_23(SliderEvent_t2111116400 * value)
	{
		___m_OnValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_23), value);
	}

	inline static int32_t get_offset_of_m_FillImage_24() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_FillImage_24)); }
	inline Image_t2042527209 * get_m_FillImage_24() const { return ___m_FillImage_24; }
	inline Image_t2042527209 ** get_address_of_m_FillImage_24() { return &___m_FillImage_24; }
	inline void set_m_FillImage_24(Image_t2042527209 * value)
	{
		___m_FillImage_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillImage_24), value);
	}

	inline static int32_t get_offset_of_m_FillTransform_25() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_FillTransform_25)); }
	inline Transform_t3275118058 * get_m_FillTransform_25() const { return ___m_FillTransform_25; }
	inline Transform_t3275118058 ** get_address_of_m_FillTransform_25() { return &___m_FillTransform_25; }
	inline void set_m_FillTransform_25(Transform_t3275118058 * value)
	{
		___m_FillTransform_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillTransform_25), value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_26() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_FillContainerRect_26)); }
	inline RectTransform_t3349966182 * get_m_FillContainerRect_26() const { return ___m_FillContainerRect_26; }
	inline RectTransform_t3349966182 ** get_address_of_m_FillContainerRect_26() { return &___m_FillContainerRect_26; }
	inline void set_m_FillContainerRect_26(RectTransform_t3349966182 * value)
	{
		___m_FillContainerRect_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillContainerRect_26), value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_27() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_HandleTransform_27)); }
	inline Transform_t3275118058 * get_m_HandleTransform_27() const { return ___m_HandleTransform_27; }
	inline Transform_t3275118058 ** get_address_of_m_HandleTransform_27() { return &___m_HandleTransform_27; }
	inline void set_m_HandleTransform_27(Transform_t3275118058 * value)
	{
		___m_HandleTransform_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleTransform_27), value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_28() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_HandleContainerRect_28)); }
	inline RectTransform_t3349966182 * get_m_HandleContainerRect_28() const { return ___m_HandleContainerRect_28; }
	inline RectTransform_t3349966182 ** get_address_of_m_HandleContainerRect_28() { return &___m_HandleContainerRect_28; }
	inline void set_m_HandleContainerRect_28(RectTransform_t3349966182 * value)
	{
		___m_HandleContainerRect_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_HandleContainerRect_28), value);
	}

	inline static int32_t get_offset_of_m_Offset_29() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_Offset_29)); }
	inline Vector2_t2243707579  get_m_Offset_29() const { return ___m_Offset_29; }
	inline Vector2_t2243707579 * get_address_of_m_Offset_29() { return &___m_Offset_29; }
	inline void set_m_Offset_29(Vector2_t2243707579  value)
	{
		___m_Offset_29 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_30() { return static_cast<int32_t>(offsetof(Slider_t297367283, ___m_Tracker_30)); }
	inline DrivenRectTransformTracker_t154385424  get_m_Tracker_30() const { return ___m_Tracker_30; }
	inline DrivenRectTransformTracker_t154385424 * get_address_of_m_Tracker_30() { return &___m_Tracker_30; }
	inline void set_m_Tracker_30(DrivenRectTransformTracker_t154385424  value)
	{
		___m_Tracker_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLIDER_T297367283_H
#ifndef TOGGLE_T3976754468_H
#define TOGGLE_T3976754468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Toggle
struct  Toggle_t3976754468  : public Selectable_t1490392188
{
public:
	// UnityEngine.UI.Toggle/ToggleTransition UnityEngine.UI.Toggle::toggleTransition
	int32_t ___toggleTransition_16;
	// UnityEngine.UI.Graphic UnityEngine.UI.Toggle::graphic
	Graphic_t2426225576 * ___graphic_17;
	// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::m_Group
	ToggleGroup_t1030026315 * ___m_Group_18;
	// UnityEngine.UI.Toggle/ToggleEvent UnityEngine.UI.Toggle::onValueChanged
	ToggleEvent_t1896830814 * ___onValueChanged_19;
	// System.Boolean UnityEngine.UI.Toggle::m_IsOn
	bool ___m_IsOn_20;

public:
	inline static int32_t get_offset_of_toggleTransition_16() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___toggleTransition_16)); }
	inline int32_t get_toggleTransition_16() const { return ___toggleTransition_16; }
	inline int32_t* get_address_of_toggleTransition_16() { return &___toggleTransition_16; }
	inline void set_toggleTransition_16(int32_t value)
	{
		___toggleTransition_16 = value;
	}

	inline static int32_t get_offset_of_graphic_17() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___graphic_17)); }
	inline Graphic_t2426225576 * get_graphic_17() const { return ___graphic_17; }
	inline Graphic_t2426225576 ** get_address_of_graphic_17() { return &___graphic_17; }
	inline void set_graphic_17(Graphic_t2426225576 * value)
	{
		___graphic_17 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_17), value);
	}

	inline static int32_t get_offset_of_m_Group_18() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___m_Group_18)); }
	inline ToggleGroup_t1030026315 * get_m_Group_18() const { return ___m_Group_18; }
	inline ToggleGroup_t1030026315 ** get_address_of_m_Group_18() { return &___m_Group_18; }
	inline void set_m_Group_18(ToggleGroup_t1030026315 * value)
	{
		___m_Group_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Group_18), value);
	}

	inline static int32_t get_offset_of_onValueChanged_19() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___onValueChanged_19)); }
	inline ToggleEvent_t1896830814 * get_onValueChanged_19() const { return ___onValueChanged_19; }
	inline ToggleEvent_t1896830814 ** get_address_of_onValueChanged_19() { return &___onValueChanged_19; }
	inline void set_onValueChanged_19(ToggleEvent_t1896830814 * value)
	{
		___onValueChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___onValueChanged_19), value);
	}

	inline static int32_t get_offset_of_m_IsOn_20() { return static_cast<int32_t>(offsetof(Toggle_t3976754468, ___m_IsOn_20)); }
	inline bool get_m_IsOn_20() const { return ___m_IsOn_20; }
	inline bool* get_address_of_m_IsOn_20() { return &___m_IsOn_20; }
	inline void set_m_IsOn_20(bool value)
	{
		___m_IsOn_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOGGLE_T3976754468_H
#ifndef VERTICALLAYOUTGROUP_T2468316403_H
#define VERTICALLAYOUTGROUP_T2468316403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_t2468316403  : public HorizontalOrVerticalLayoutGroup_t1968298610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_T2468316403_H
#ifndef OUTLINE_T1417504278_H
#define OUTLINE_T1417504278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t1417504278  : public Shadow_t4269599528
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T1417504278_H
#ifndef TEXT_T356221433_H
#define TEXT_T356221433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t356221433  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t2614388407 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t647235000 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t647235000 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t3048644023* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_FontData_28)); }
	inline FontData_t2614388407 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t2614388407 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t2614388407 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCache_30)); }
	inline TextGenerator_t647235000 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t647235000 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t647235000 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t647235000 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t3048644023* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t3048644023* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t356221433_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t193706927 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t356221433_StaticFields, ___s_DefaultText_32)); }
	inline Material_t193706927 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t193706927 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t193706927 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T356221433_H
#ifndef RAWIMAGE_T2749640213_H
#define RAWIMAGE_T2749640213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t2749640213  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t2243626319 * ___m_Texture_28;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t3681755626  ___m_UVRect_29;

public:
	inline static int32_t get_offset_of_m_Texture_28() { return static_cast<int32_t>(offsetof(RawImage_t2749640213, ___m_Texture_28)); }
	inline Texture_t2243626319 * get_m_Texture_28() const { return ___m_Texture_28; }
	inline Texture_t2243626319 ** get_address_of_m_Texture_28() { return &___m_Texture_28; }
	inline void set_m_Texture_28(Texture_t2243626319 * value)
	{
		___m_Texture_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_28), value);
	}

	inline static int32_t get_offset_of_m_UVRect_29() { return static_cast<int32_t>(offsetof(RawImage_t2749640213, ___m_UVRect_29)); }
	inline Rect_t3681755626  get_m_UVRect_29() const { return ___m_UVRect_29; }
	inline Rect_t3681755626 * get_address_of_m_UVRect_29() { return &___m_UVRect_29; }
	inline void set_m_UVRect_29(Rect_t3681755626  value)
	{
		___m_UVRect_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T2749640213_H
#ifndef HORIZONTALLAYOUTGROUP_T2875670365_H
#define HORIZONTALLAYOUTGROUP_T2875670365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_t2875670365  : public HorizontalOrVerticalLayoutGroup_t1968298610
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_T2875670365_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (MaskUtilities_t1936577068), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (Misc_t2977957982), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (Navigation_t1571958496)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[5] = 
{
	Navigation_t1571958496::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnUp_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnDown_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnLeft_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Navigation_t1571958496::get_offset_of_m_SelectOnRight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (Mode_t1081683921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2503[6] = 
{
	Mode_t1081683921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (RawImage_t2749640213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[2] = 
{
	RawImage_t2749640213::get_offset_of_m_Texture_28(),
	RawImage_t2749640213::get_offset_of_m_UVRect_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (RectMask2D_t1156185964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[8] = 
{
	RectMask2D_t1156185964::get_offset_of_m_VertexClipper_2(),
	RectMask2D_t1156185964::get_offset_of_m_RectTransform_3(),
	RectMask2D_t1156185964::get_offset_of_m_ClipTargets_4(),
	RectMask2D_t1156185964::get_offset_of_m_ShouldRecalculateClipRects_5(),
	RectMask2D_t1156185964::get_offset_of_m_Clippers_6(),
	RectMask2D_t1156185964::get_offset_of_m_LastClipRectCanvasSpace_7(),
	RectMask2D_t1156185964::get_offset_of_m_LastValidClipRect_8(),
	RectMask2D_t1156185964::get_offset_of_m_ForceClip_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (Scrollbar_t3248359358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[11] = 
{
	Scrollbar_t3248359358::get_offset_of_m_HandleRect_16(),
	Scrollbar_t3248359358::get_offset_of_m_Direction_17(),
	Scrollbar_t3248359358::get_offset_of_m_Value_18(),
	Scrollbar_t3248359358::get_offset_of_m_Size_19(),
	Scrollbar_t3248359358::get_offset_of_m_NumberOfSteps_20(),
	Scrollbar_t3248359358::get_offset_of_m_OnValueChanged_21(),
	Scrollbar_t3248359358::get_offset_of_m_ContainerRect_22(),
	Scrollbar_t3248359358::get_offset_of_m_Offset_23(),
	Scrollbar_t3248359358::get_offset_of_m_Tracker_24(),
	Scrollbar_t3248359358::get_offset_of_m_PointerDownRepeat_25(),
	Scrollbar_t3248359358::get_offset_of_isPointerDownAndNotDragging_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (Direction_t3696775921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2507[5] = 
{
	Direction_t3696775921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (ScrollEvent_t1794825321), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (Axis_t2427050347)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2509[3] = 
{
	Axis_t2427050347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (U3CClickRepeatU3Ec__Iterator0_t4156771994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[5] = 
{
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_U24this_1(),
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_U24current_2(),
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_U24disposing_3(),
	U3CClickRepeatU3Ec__Iterator0_t4156771994::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (ScrollRect_t1199013257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[36] = 
{
	ScrollRect_t1199013257::get_offset_of_m_Content_2(),
	ScrollRect_t1199013257::get_offset_of_m_Horizontal_3(),
	ScrollRect_t1199013257::get_offset_of_m_Vertical_4(),
	ScrollRect_t1199013257::get_offset_of_m_MovementType_5(),
	ScrollRect_t1199013257::get_offset_of_m_Elasticity_6(),
	ScrollRect_t1199013257::get_offset_of_m_Inertia_7(),
	ScrollRect_t1199013257::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t1199013257::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t1199013257::get_offset_of_m_Viewport_10(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t1199013257::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t1199013257::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t1199013257::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t1199013257::get_offset_of_m_ViewRect_20(),
	ScrollRect_t1199013257::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t1199013257::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t1199013257::get_offset_of_m_Velocity_23(),
	ScrollRect_t1199013257::get_offset_of_m_Dragging_24(),
	ScrollRect_t1199013257::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t1199013257::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t1199013257::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t1199013257::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t1199013257::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t1199013257::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t1199013257::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t1199013257::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t1199013257::get_offset_of_m_Rect_33(),
	ScrollRect_t1199013257::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t1199013257::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t1199013257::get_offset_of_m_Tracker_36(),
	ScrollRect_t1199013257::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (MovementType_t905360158)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2512[4] = 
{
	MovementType_t905360158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { sizeof (ScrollbarVisibility_t3834843475)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2513[4] = 
{
	ScrollbarVisibility_t3834843475::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (ScrollRectEvent_t3529018992), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { sizeof (Selectable_t1490392188), -1, sizeof(Selectable_t1490392188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2515[14] = 
{
	Selectable_t1490392188_StaticFields::get_offset_of_s_List_2(),
	Selectable_t1490392188::get_offset_of_m_Navigation_3(),
	Selectable_t1490392188::get_offset_of_m_Transition_4(),
	Selectable_t1490392188::get_offset_of_m_Colors_5(),
	Selectable_t1490392188::get_offset_of_m_SpriteState_6(),
	Selectable_t1490392188::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t1490392188::get_offset_of_m_Interactable_8(),
	Selectable_t1490392188::get_offset_of_m_TargetGraphic_9(),
	Selectable_t1490392188::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t1490392188::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t1490392188::get_offset_of_U3CisPointerInsideU3Ek__BackingField_12(),
	Selectable_t1490392188::get_offset_of_U3CisPointerDownU3Ek__BackingField_13(),
	Selectable_t1490392188::get_offset_of_U3ChasSelectionU3Ek__BackingField_14(),
	Selectable_t1490392188::get_offset_of_m_CanvasGroupCache_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { sizeof (Transition_t605142169)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2516[5] = 
{
	Transition_t605142169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (SelectionState_t3187567897)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2517[5] = 
{
	SelectionState_t3187567897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (SetPropertyUtility_t4019374597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (Slider_t297367283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[15] = 
{
	Slider_t297367283::get_offset_of_m_FillRect_16(),
	Slider_t297367283::get_offset_of_m_HandleRect_17(),
	Slider_t297367283::get_offset_of_m_Direction_18(),
	Slider_t297367283::get_offset_of_m_MinValue_19(),
	Slider_t297367283::get_offset_of_m_MaxValue_20(),
	Slider_t297367283::get_offset_of_m_WholeNumbers_21(),
	Slider_t297367283::get_offset_of_m_Value_22(),
	Slider_t297367283::get_offset_of_m_OnValueChanged_23(),
	Slider_t297367283::get_offset_of_m_FillImage_24(),
	Slider_t297367283::get_offset_of_m_FillTransform_25(),
	Slider_t297367283::get_offset_of_m_FillContainerRect_26(),
	Slider_t297367283::get_offset_of_m_HandleTransform_27(),
	Slider_t297367283::get_offset_of_m_HandleContainerRect_28(),
	Slider_t297367283::get_offset_of_m_Offset_29(),
	Slider_t297367283::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (Direction_t1525323322)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2520[5] = 
{
	Direction_t1525323322::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (SliderEvent_t2111116400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (Axis_t375128448)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2522[3] = 
{
	Axis_t375128448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (SpriteState_t1353336012)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2523[3] = 
{
	SpriteState_t1353336012::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteState_t1353336012::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteState_t1353336012::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (StencilMaterial_t1630303189), -1, sizeof(StencilMaterial_t1630303189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2524[1] = 
{
	StencilMaterial_t1630303189_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (MatEntry_t3157325053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[10] = 
{
	MatEntry_t3157325053::get_offset_of_baseMat_0(),
	MatEntry_t3157325053::get_offset_of_customMat_1(),
	MatEntry_t3157325053::get_offset_of_count_2(),
	MatEntry_t3157325053::get_offset_of_stencilId_3(),
	MatEntry_t3157325053::get_offset_of_operation_4(),
	MatEntry_t3157325053::get_offset_of_compareFunction_5(),
	MatEntry_t3157325053::get_offset_of_readMask_6(),
	MatEntry_t3157325053::get_offset_of_writeMask_7(),
	MatEntry_t3157325053::get_offset_of_useAlphaClip_8(),
	MatEntry_t3157325053::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (Text_t356221433), -1, sizeof(Text_t356221433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2526[7] = 
{
	Text_t356221433::get_offset_of_m_FontData_28(),
	Text_t356221433::get_offset_of_m_Text_29(),
	Text_t356221433::get_offset_of_m_TextCache_30(),
	Text_t356221433::get_offset_of_m_TextCacheForLayout_31(),
	Text_t356221433_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t356221433::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t356221433::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (Toggle_t3976754468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2527[5] = 
{
	Toggle_t3976754468::get_offset_of_toggleTransition_16(),
	Toggle_t3976754468::get_offset_of_graphic_17(),
	Toggle_t3976754468::get_offset_of_m_Group_18(),
	Toggle_t3976754468::get_offset_of_onValueChanged_19(),
	Toggle_t3976754468::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (ToggleTransition_t1114673831)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2528[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2530[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2531[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (AspectMode_t1166448724)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2537[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (ScaleMode_t987318053)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2539[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2540[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (Unit_t3220761768)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2541[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (FitMode_t4030874534)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2543[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (Corner_t1077473318)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2545[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (Axis_t1431825778)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2546[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (Constraint_t3558160636)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2547[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[8] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
	LayoutElement_t2808691390::get_offset_of_m_LayoutPriority_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2558[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2559[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2565[7] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (GetRayIntersectionAllNonAllocCallback_t3246763936), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (GetRaycastNonAllocCallback_t1074830945), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2572[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305144), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2580[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (U3CModuleU3E_t3783534237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (AWSConfigsS3_t3080748296), -1, sizeof(AWSConfigsS3_t3080748296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2583[2] = 
{
	AWSConfigsS3_t3080748296_StaticFields::get_offset_of__useSignatureVersion4_0(),
	AWSConfigsS3_t3080748296_StaticFields::get_offset_of_U3CUseSigV4SetExplicitlyU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (V4ClientSectionRoot_t1193884458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[1] = 
{
	V4ClientSectionRoot_t1193884458::get_offset_of_U3CS3U3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (V4ClientSection_t2149061698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[1] = 
{
	V4ClientSection_t2149061698::get_offset_of_U3CUseSignatureVersion4U3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (AmazonS3Client_t548780019), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (U3CU3Ec__DisplayClass85_0_t3636365575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[1] = 
{
	U3CU3Ec__DisplayClass85_0_t3636365575::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (U3CU3Ec__DisplayClass105_0_t177521762), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[1] = 
{
	U3CU3Ec__DisplayClass105_0_t177521762::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (AmazonS3Config_t29117630), -1, sizeof(AmazonS3Config_t29117630_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2589[4] = 
{
	AmazonS3Config_t29117630::get_offset_of_forcePathStyle_21(),
	AmazonS3Config_t29117630::get_offset_of_useAccelerateEndpoint_22(),
	AmazonS3Config_t29117630_StaticFields::get_offset_of_UserAgentString_23(),
	AmazonS3Config_t29117630::get_offset_of__userAgent_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (AmazonS3Exception_t3554613873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[4] = 
{
	AmazonS3Exception_t3554613873::get_offset_of_U3CAmazonId2U3Ek__BackingField_15(),
	AmazonS3Exception_t3554613873::get_offset_of_U3CAmazonCloudFrontIdU3Ek__BackingField_16(),
	AmazonS3Exception_t3554613873::get_offset_of_U3CResponseBodyU3Ek__BackingField_17(),
	AmazonS3Exception_t3554613873::get_offset_of_U3CRegionU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (S3Region_t723338532), -1, sizeof(S3Region_t723338532_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2591[22] = 
{
	S3Region_t723338532_StaticFields::get_offset_of_US_3(),
	S3Region_t723338532_StaticFields::get_offset_of_USE2_4(),
	S3Region_t723338532_StaticFields::get_offset_of_EU_5(),
	S3Region_t723338532_StaticFields::get_offset_of_EUW1_6(),
	S3Region_t723338532_StaticFields::get_offset_of_EUW2_7(),
	S3Region_t723338532_StaticFields::get_offset_of_EUW3_8(),
	S3Region_t723338532_StaticFields::get_offset_of_EUC1_9(),
	S3Region_t723338532_StaticFields::get_offset_of_USW1_10(),
	S3Region_t723338532_StaticFields::get_offset_of_USW2_11(),
	S3Region_t723338532_StaticFields::get_offset_of_GOVW1_12(),
	S3Region_t723338532_StaticFields::get_offset_of_APS1_13(),
	S3Region_t723338532_StaticFields::get_offset_of_APS2_14(),
	S3Region_t723338532_StaticFields::get_offset_of_APN1_15(),
	S3Region_t723338532_StaticFields::get_offset_of_APN2_16(),
	S3Region_t723338532_StaticFields::get_offset_of_APS3_17(),
	S3Region_t723338532_StaticFields::get_offset_of_SAE1_18(),
	S3Region_t723338532_StaticFields::get_offset_of_CN1_19(),
	S3Region_t723338532_StaticFields::get_offset_of_CNW1_20(),
	S3Region_t723338532_StaticFields::get_offset_of_CAN1_21(),
	S3Region_t723338532_StaticFields::get_offset_of_SFO_22(),
	S3Region_t723338532_StaticFields::get_offset_of_CN_23(),
	S3Region_t723338532_StaticFields::get_offset_of_GOV_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (S3StorageClass_t454477475), -1, sizeof(S3StorageClass_t454477475_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2592[4] = 
{
	S3StorageClass_t454477475_StaticFields::get_offset_of_Standard_3(),
	S3StorageClass_t454477475_StaticFields::get_offset_of_ReducedRedundancy_4(),
	S3StorageClass_t454477475_StaticFields::get_offset_of_Glacier_5(),
	S3StorageClass_t454477475_StaticFields::get_offset_of_StandardInfrequentAccess_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (ServerSideEncryptionCustomerMethod_t3201425490), -1, sizeof(ServerSideEncryptionCustomerMethod_t3201425490_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2593[2] = 
{
	ServerSideEncryptionCustomerMethod_t3201425490_StaticFields::get_offset_of_None_3(),
	ServerSideEncryptionCustomerMethod_t3201425490_StaticFields::get_offset_of_AES256_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (ServerSideEncryptionMethod_t608782770), -1, sizeof(ServerSideEncryptionMethod_t608782770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2594[3] = 
{
	ServerSideEncryptionMethod_t608782770_StaticFields::get_offset_of_None_3(),
	ServerSideEncryptionMethod_t608782770_StaticFields::get_offset_of_AES256_4(),
	ServerSideEncryptionMethod_t608782770_StaticFields::get_offset_of_AWSKMS_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (EncodingType_t159362831), -1, sizeof(EncodingType_t159362831_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2595[1] = 
{
	EncodingType_t159362831_StaticFields::get_offset_of_Url_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (ReplicationStatus_t1991002226), -1, sizeof(ReplicationStatus_t1991002226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2596[4] = 
{
	ReplicationStatus_t1991002226_StaticFields::get_offset_of_Pending_3(),
	ReplicationStatus_t1991002226_StaticFields::get_offset_of_Completed_4(),
	ReplicationStatus_t1991002226_StaticFields::get_offset_of_Replica_5(),
	ReplicationStatus_t1991002226_StaticFields::get_offset_of_Failed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (RequestPayer_t3334661492), -1, sizeof(RequestPayer_t3334661492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2597[1] = 
{
	RequestPayer_t3334661492_StaticFields::get_offset_of_Requester_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (RequestCharged_t2438105727), -1, sizeof(RequestCharged_t2438105727_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2598[1] = 
{
	RequestCharged_t2438105727_StaticFields::get_offset_of_Requester_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (DeleteObjectsException_t338470828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[1] = 
{
	DeleteObjectsException_t338470828::get_offset_of_response_19(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
