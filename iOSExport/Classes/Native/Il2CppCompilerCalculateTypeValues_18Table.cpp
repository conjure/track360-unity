﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Specialized.ListDictionary
struct ListDictionary_t3458713452;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t1548133672;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t1578612233;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.String
struct String_t;
// System.Xml.XmlParserInput
struct XmlParserInput_t2366782760;
// System.Collections.Stack
struct Stack_t1043988394;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t1113953282;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>[]
struct KeyValuePair_2U5BU5D_t1568024203;
// Mono.Xml.DTDAutomataFactory
struct DTDAutomataFactory_t3605390810;
// Mono.Xml.DTDElementAutomata
struct DTDElementAutomata_t2864881036;
// Mono.Xml.DTDEmptyAutomata
struct DTDEmptyAutomata_t411530619;
// Mono.Xml.DTDAnyAutomata
struct DTDAnyAutomata_t146446906;
// Mono.Xml.DTDInvalidAutomata
struct DTDInvalidAutomata_t247674167;
// Mono.Xml.DTDElementDeclarationCollection
struct DTDElementDeclarationCollection_t2224069626;
// Mono.Xml.DTDAttListDeclarationCollection
struct DTDAttListDeclarationCollection_t243645429;
// Mono.Xml.DTDParameterEntityDeclarationCollection
struct DTDParameterEntityDeclarationCollection_t3496720022;
// Mono.Xml.DTDEntityDeclarationCollection
struct DTDEntityDeclarationCollection_t1212505713;
// Mono.Xml.DTDNotationDeclarationCollection
struct DTDNotationDeclarationCollection_t228085060;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Xml.XmlNode
struct XmlNode_t616554813;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Xml.XmlNode/EmptyNodeList
struct EmptyNodeList_t1718403287;
// System.Xml.XmlDocument
struct XmlDocument_t3649534162;
// System.Xml.XmlNodeListChildren
struct XmlNodeListChildren_t2811458520;
// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t180042139;
// Mono.Xml.DTDContentModel
struct DTDContentModel_t445576364;
// System.Xml.XmlNameEntry
struct XmlNameEntry_t3745551716;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t1287616130;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t2533799901;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.Xml.XmlElement
struct XmlElement_t2877111883;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Xml.XmlImplementation
struct XmlImplementation_t1664517635;
// System.Xml.XmlNameEntryCache
struct XmlNameEntryCache_t3855584002;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_t2964483403;
// Mono.Xml.EntityResolvingXmlReader
struct EntityResolvingXmlReader_t2086920314;
// System.Xml.XmlTextReader
struct XmlTextReader_t3514170725;
// System.Xml.XmlValidatingReader
struct XmlValidatingReader_t3416770767;
// Mono.Xml.DTDValidatingReader/AttributeSlot[]
struct AttributeSlotU5BU5D_t1168612672;
// Mono.Xml.DTDAutomata
struct DTDAutomata_t545990600;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t486731501;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Text.Decoder
struct Decoder_t3792697818;
// System.IO.Stream
struct Stream_t3255436806;
// System.Xml.XmlException
struct XmlException_t4188277960;
// System.Uri
struct Uri_t19570940;
// System.Type
struct Type_t;
// Mono.Xml.DTDNode
struct DTDNode_t1758286970;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_t3063656491;
// System.Xml.XmlInputStream
struct XmlInputStream_t2650744719;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct List_1_t799532586;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_t3359885287;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t145210370;
// System.String[]
struct StringU5BU5D_t1642385972;
// Mono.Xml.DictionaryBase
struct DictionaryBase_t1005937181;
// System.Xml.Schema.XmlSchemaAttribute[]
struct XmlSchemaAttributeU5BU5D_t3434391819;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t135184468;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3928241465;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t313318308;
// Mono.Xml.Schema.XsdParticleStateManager
struct XsdParticleStateManager_t4119977941;
// Mono.Xml.Schema.XsdIDManager
struct XsdIDManager_t3860002335;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Xml.Schema.XmlSchemaPatternFacet
struct XmlSchemaPatternFacet_t2024909611;
// Mono.Xml.DTDContentModelCollection
struct DTDContentModelCollection_t3164170484;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.XmlParserContext
struct XmlParserContext_t2728039553;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_t2400301303;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t287209776;
// Mono.Xml.Schema.XsdIdentityPath[]
struct XsdIdentityPathU5BU5D_t526781831;
// Mono.Xml.Schema.XsdIdentityPath
struct XsdIdentityPath_t2037874;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef XMLSERIALIZERNAMESPACES_T3063656491_H
#define XMLSERIALIZERNAMESPACES_T3063656491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerNamespaces
struct  XmlSerializerNamespaces_t3063656491  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary System.Xml.Serialization.XmlSerializerNamespaces::namespaces
	ListDictionary_t3458713452 * ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XmlSerializerNamespaces_t3063656491, ___namespaces_0)); }
	inline ListDictionary_t3458713452 * get_namespaces_0() const { return ___namespaces_0; }
	inline ListDictionary_t3458713452 ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(ListDictionary_t3458713452 * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERNAMESPACES_T3063656491_H
#ifndef XMLREADER_T3675626668_H
#define XMLREADER_T3675626668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3675626668  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t1548133672 * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t1578612233 * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_t3675626668, ___binary_0)); }
	inline XmlReaderBinarySupport_t1548133672 * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_t1548133672 ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_t1548133672 * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_t3675626668, ___settings_1)); }
	inline XmlReaderSettings_t1578612233 * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_t1578612233 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_t1578612233 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3675626668_H
#ifndef XPATHITEM_T3130801258_H
#define XPATHITEM_T3130801258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t3130801258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T3130801258_H
#ifndef XMLIMPLEMENTATION_T1664517635_H
#define XMLIMPLEMENTATION_T1664517635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlImplementation
struct  XmlImplementation_t1664517635  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlImplementation::InternalNameTable
	XmlNameTable_t1345805268 * ___InternalNameTable_0;

public:
	inline static int32_t get_offset_of_InternalNameTable_0() { return static_cast<int32_t>(offsetof(XmlImplementation_t1664517635, ___InternalNameTable_0)); }
	inline XmlNameTable_t1345805268 * get_InternalNameTable_0() const { return ___InternalNameTable_0; }
	inline XmlNameTable_t1345805268 ** get_address_of_InternalNameTable_0() { return &___InternalNameTable_0; }
	inline void set_InternalNameTable_0(XmlNameTable_t1345805268 * value)
	{
		___InternalNameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___InternalNameTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIMPLEMENTATION_T1664517635_H
#ifndef ENTRY_T2583369454_H
#define ENTRY_T2583369454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable/Entry
struct  Entry_t2583369454  : public RuntimeObject
{
public:
	// System.String System.Xml.NameTable/Entry::str
	String_t* ___str_0;
	// System.Int32 System.Xml.NameTable/Entry::hash
	int32_t ___hash_1;
	// System.Int32 System.Xml.NameTable/Entry::len
	int32_t ___len_2;
	// System.Xml.NameTable/Entry System.Xml.NameTable/Entry::next
	Entry_t2583369454 * ___next_3;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(Entry_t2583369454, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(Entry_t2583369454, ___hash_1)); }
	inline int32_t get_hash_1() const { return ___hash_1; }
	inline int32_t* get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(int32_t value)
	{
		___hash_1 = value;
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(Entry_t2583369454, ___len_2)); }
	inline int32_t get_len_2() const { return ___len_2; }
	inline int32_t* get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(int32_t value)
	{
		___len_2 = value;
	}

	inline static int32_t get_offset_of_next_3() { return static_cast<int32_t>(offsetof(Entry_t2583369454, ___next_3)); }
	inline Entry_t2583369454 * get_next_3() const { return ___next_3; }
	inline Entry_t2583369454 ** get_address_of_next_3() { return &___next_3; }
	inline void set_next_3(Entry_t2583369454 * value)
	{
		___next_3 = value;
		Il2CppCodeGenWriteBarrier((&___next_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_T2583369454_H
#ifndef ATTRIBUTESLOT_T1499247213_H
#define ATTRIBUTESLOT_T1499247213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDValidatingReader/AttributeSlot
struct  AttributeSlot_t1499247213  : public RuntimeObject
{
public:
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::Name
	String_t* ___Name_0;
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::LocalName
	String_t* ___LocalName_1;
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::NS
	String_t* ___NS_2;
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::Prefix
	String_t* ___Prefix_3;
	// System.String Mono.Xml.DTDValidatingReader/AttributeSlot::Value
	String_t* ___Value_4;
	// System.Boolean Mono.Xml.DTDValidatingReader/AttributeSlot::IsDefault
	bool ___IsDefault_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AttributeSlot_t1499247213, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(AttributeSlot_t1499247213, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(AttributeSlot_t1499247213, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_Prefix_3() { return static_cast<int32_t>(offsetof(AttributeSlot_t1499247213, ___Prefix_3)); }
	inline String_t* get_Prefix_3() const { return ___Prefix_3; }
	inline String_t** get_address_of_Prefix_3() { return &___Prefix_3; }
	inline void set_Prefix_3(String_t* value)
	{
		___Prefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_3), value);
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(AttributeSlot_t1499247213, ___Value_4)); }
	inline String_t* get_Value_4() const { return ___Value_4; }
	inline String_t** get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(String_t* value)
	{
		___Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___Value_4), value);
	}

	inline static int32_t get_offset_of_IsDefault_5() { return static_cast<int32_t>(offsetof(AttributeSlot_t1499247213, ___IsDefault_5)); }
	inline bool get_IsDefault_5() const { return ___IsDefault_5; }
	inline bool* get_address_of_IsDefault_5() { return &___IsDefault_5; }
	inline void set_IsDefault_5(bool value)
	{
		___IsDefault_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTESLOT_T1499247213_H
#ifndef DTDREADER_T2453137441_H
#define DTDREADER_T2453137441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DTDReader
struct  DTDReader_t2453137441  : public RuntimeObject
{
public:
	// System.Xml.XmlParserInput System.Xml.DTDReader::currentInput
	XmlParserInput_t2366782760 * ___currentInput_0;
	// System.Collections.Stack System.Xml.DTDReader::parserInputStack
	Stack_t1043988394 * ___parserInputStack_1;
	// System.Char[] System.Xml.DTDReader::nameBuffer
	CharU5BU5D_t1328083999* ___nameBuffer_2;
	// System.Int32 System.Xml.DTDReader::nameLength
	int32_t ___nameLength_3;
	// System.Int32 System.Xml.DTDReader::nameCapacity
	int32_t ___nameCapacity_4;
	// System.Text.StringBuilder System.Xml.DTDReader::valueBuffer
	StringBuilder_t1221177846 * ___valueBuffer_5;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLineNumber
	int32_t ___currentLinkedNodeLineNumber_6;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLinePosition
	int32_t ___currentLinkedNodeLinePosition_7;
	// System.Int32 System.Xml.DTDReader::dtdIncludeSect
	int32_t ___dtdIncludeSect_8;
	// System.Boolean System.Xml.DTDReader::normalization
	bool ___normalization_9;
	// System.Boolean System.Xml.DTDReader::processingInternalSubset
	bool ___processingInternalSubset_10;
	// System.String System.Xml.DTDReader::cachedPublicId
	String_t* ___cachedPublicId_11;
	// System.String System.Xml.DTDReader::cachedSystemId
	String_t* ___cachedSystemId_12;
	// Mono.Xml.DTDObjectModel System.Xml.DTDReader::DTD
	DTDObjectModel_t1113953282 * ___DTD_13;

public:
	inline static int32_t get_offset_of_currentInput_0() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___currentInput_0)); }
	inline XmlParserInput_t2366782760 * get_currentInput_0() const { return ___currentInput_0; }
	inline XmlParserInput_t2366782760 ** get_address_of_currentInput_0() { return &___currentInput_0; }
	inline void set_currentInput_0(XmlParserInput_t2366782760 * value)
	{
		___currentInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentInput_0), value);
	}

	inline static int32_t get_offset_of_parserInputStack_1() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___parserInputStack_1)); }
	inline Stack_t1043988394 * get_parserInputStack_1() const { return ___parserInputStack_1; }
	inline Stack_t1043988394 ** get_address_of_parserInputStack_1() { return &___parserInputStack_1; }
	inline void set_parserInputStack_1(Stack_t1043988394 * value)
	{
		___parserInputStack_1 = value;
		Il2CppCodeGenWriteBarrier((&___parserInputStack_1), value);
	}

	inline static int32_t get_offset_of_nameBuffer_2() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___nameBuffer_2)); }
	inline CharU5BU5D_t1328083999* get_nameBuffer_2() const { return ___nameBuffer_2; }
	inline CharU5BU5D_t1328083999** get_address_of_nameBuffer_2() { return &___nameBuffer_2; }
	inline void set_nameBuffer_2(CharU5BU5D_t1328083999* value)
	{
		___nameBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameBuffer_2), value);
	}

	inline static int32_t get_offset_of_nameLength_3() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___nameLength_3)); }
	inline int32_t get_nameLength_3() const { return ___nameLength_3; }
	inline int32_t* get_address_of_nameLength_3() { return &___nameLength_3; }
	inline void set_nameLength_3(int32_t value)
	{
		___nameLength_3 = value;
	}

	inline static int32_t get_offset_of_nameCapacity_4() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___nameCapacity_4)); }
	inline int32_t get_nameCapacity_4() const { return ___nameCapacity_4; }
	inline int32_t* get_address_of_nameCapacity_4() { return &___nameCapacity_4; }
	inline void set_nameCapacity_4(int32_t value)
	{
		___nameCapacity_4 = value;
	}

	inline static int32_t get_offset_of_valueBuffer_5() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___valueBuffer_5)); }
	inline StringBuilder_t1221177846 * get_valueBuffer_5() const { return ___valueBuffer_5; }
	inline StringBuilder_t1221177846 ** get_address_of_valueBuffer_5() { return &___valueBuffer_5; }
	inline void set_valueBuffer_5(StringBuilder_t1221177846 * value)
	{
		___valueBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuffer_5), value);
	}

	inline static int32_t get_offset_of_currentLinkedNodeLineNumber_6() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___currentLinkedNodeLineNumber_6)); }
	inline int32_t get_currentLinkedNodeLineNumber_6() const { return ___currentLinkedNodeLineNumber_6; }
	inline int32_t* get_address_of_currentLinkedNodeLineNumber_6() { return &___currentLinkedNodeLineNumber_6; }
	inline void set_currentLinkedNodeLineNumber_6(int32_t value)
	{
		___currentLinkedNodeLineNumber_6 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLinePosition_7() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___currentLinkedNodeLinePosition_7)); }
	inline int32_t get_currentLinkedNodeLinePosition_7() const { return ___currentLinkedNodeLinePosition_7; }
	inline int32_t* get_address_of_currentLinkedNodeLinePosition_7() { return &___currentLinkedNodeLinePosition_7; }
	inline void set_currentLinkedNodeLinePosition_7(int32_t value)
	{
		___currentLinkedNodeLinePosition_7 = value;
	}

	inline static int32_t get_offset_of_dtdIncludeSect_8() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___dtdIncludeSect_8)); }
	inline int32_t get_dtdIncludeSect_8() const { return ___dtdIncludeSect_8; }
	inline int32_t* get_address_of_dtdIncludeSect_8() { return &___dtdIncludeSect_8; }
	inline void set_dtdIncludeSect_8(int32_t value)
	{
		___dtdIncludeSect_8 = value;
	}

	inline static int32_t get_offset_of_normalization_9() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___normalization_9)); }
	inline bool get_normalization_9() const { return ___normalization_9; }
	inline bool* get_address_of_normalization_9() { return &___normalization_9; }
	inline void set_normalization_9(bool value)
	{
		___normalization_9 = value;
	}

	inline static int32_t get_offset_of_processingInternalSubset_10() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___processingInternalSubset_10)); }
	inline bool get_processingInternalSubset_10() const { return ___processingInternalSubset_10; }
	inline bool* get_address_of_processingInternalSubset_10() { return &___processingInternalSubset_10; }
	inline void set_processingInternalSubset_10(bool value)
	{
		___processingInternalSubset_10 = value;
	}

	inline static int32_t get_offset_of_cachedPublicId_11() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___cachedPublicId_11)); }
	inline String_t* get_cachedPublicId_11() const { return ___cachedPublicId_11; }
	inline String_t** get_address_of_cachedPublicId_11() { return &___cachedPublicId_11; }
	inline void set_cachedPublicId_11(String_t* value)
	{
		___cachedPublicId_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedPublicId_11), value);
	}

	inline static int32_t get_offset_of_cachedSystemId_12() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___cachedSystemId_12)); }
	inline String_t* get_cachedSystemId_12() const { return ___cachedSystemId_12; }
	inline String_t** get_address_of_cachedSystemId_12() { return &___cachedSystemId_12; }
	inline void set_cachedSystemId_12(String_t* value)
	{
		___cachedSystemId_12 = value;
		Il2CppCodeGenWriteBarrier((&___cachedSystemId_12), value);
	}

	inline static int32_t get_offset_of_DTD_13() { return static_cast<int32_t>(offsetof(DTDReader_t2453137441, ___DTD_13)); }
	inline DTDObjectModel_t1113953282 * get_DTD_13() const { return ___DTD_13; }
	inline DTDObjectModel_t1113953282 ** get_address_of_DTD_13() { return &___DTD_13; }
	inline void set_DTD_13(DTDObjectModel_t1113953282 * value)
	{
		___DTD_13 = value;
		Il2CppCodeGenWriteBarrier((&___DTD_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDREADER_T2453137441_H
#ifndef DTDAUTOMATAFACTORY_T3605390810_H
#define DTDAUTOMATAFACTORY_T3605390810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAutomataFactory
struct  DTDAutomataFactory_t3605390810  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDAutomataFactory::root
	DTDObjectModel_t1113953282 * ___root_0;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::choiceTable
	Hashtable_t909839986 * ___choiceTable_1;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::sequenceTable
	Hashtable_t909839986 * ___sequenceTable_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t3605390810, ___root_0)); }
	inline DTDObjectModel_t1113953282 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1113953282 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1113953282 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_choiceTable_1() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t3605390810, ___choiceTable_1)); }
	inline Hashtable_t909839986 * get_choiceTable_1() const { return ___choiceTable_1; }
	inline Hashtable_t909839986 ** get_address_of_choiceTable_1() { return &___choiceTable_1; }
	inline void set_choiceTable_1(Hashtable_t909839986 * value)
	{
		___choiceTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___choiceTable_1), value);
	}

	inline static int32_t get_offset_of_sequenceTable_2() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t3605390810, ___sequenceTable_2)); }
	inline Hashtable_t909839986 * get_sequenceTable_2() const { return ___sequenceTable_2; }
	inline Hashtable_t909839986 ** get_address_of_sequenceTable_2() { return &___sequenceTable_2; }
	inline void set_sequenceTable_2(Hashtable_t909839986 * value)
	{
		___sequenceTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDAUTOMATAFACTORY_T3605390810_H
#ifndef DTDCONTENTMODELCOLLECTION_T3164170484_H
#define DTDCONTENTMODELCOLLECTION_T3164170484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModelCollection
struct  DTDContentModelCollection_t3164170484  : public RuntimeObject
{
public:
	// System.Collections.ArrayList Mono.Xml.DTDContentModelCollection::contentModel
	ArrayList_t4252133567 * ___contentModel_0;

public:
	inline static int32_t get_offset_of_contentModel_0() { return static_cast<int32_t>(offsetof(DTDContentModelCollection_t3164170484, ___contentModel_0)); }
	inline ArrayList_t4252133567 * get_contentModel_0() const { return ___contentModel_0; }
	inline ArrayList_t4252133567 ** get_address_of_contentModel_0() { return &___contentModel_0; }
	inline void set_contentModel_0(ArrayList_t4252133567 * value)
	{
		___contentModel_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODELCOLLECTION_T3164170484_H
#ifndef LIST_1_T799532586_H
#define LIST_1_T799532586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  List_1_t799532586  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t1568024203* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t799532586, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t1568024203* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t1568024203** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t1568024203* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t799532586, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t799532586, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t799532586_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyValuePair_2U5BU5D_t1568024203* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t799532586_StaticFields, ___EmptyArray_4)); }
	inline KeyValuePair_2U5BU5D_t1568024203* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyValuePair_2U5BU5D_t1568024203** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyValuePair_2U5BU5D_t1568024203* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T799532586_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef DTDNODE_T1758286970_H
#define DTDNODE_T1758286970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNode
struct  DTDNode_t1758286970  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDNode::root
	DTDObjectModel_t1113953282 * ___root_0;
	// System.Boolean Mono.Xml.DTDNode::isInternalSubset
	bool ___isInternalSubset_1;
	// System.String Mono.Xml.DTDNode::baseURI
	String_t* ___baseURI_2;
	// System.Int32 Mono.Xml.DTDNode::lineNumber
	int32_t ___lineNumber_3;
	// System.Int32 Mono.Xml.DTDNode::linePosition
	int32_t ___linePosition_4;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDNode_t1758286970, ___root_0)); }
	inline DTDObjectModel_t1113953282 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1113953282 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1113953282 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_isInternalSubset_1() { return static_cast<int32_t>(offsetof(DTDNode_t1758286970, ___isInternalSubset_1)); }
	inline bool get_isInternalSubset_1() const { return ___isInternalSubset_1; }
	inline bool* get_address_of_isInternalSubset_1() { return &___isInternalSubset_1; }
	inline void set_isInternalSubset_1(bool value)
	{
		___isInternalSubset_1 = value;
	}

	inline static int32_t get_offset_of_baseURI_2() { return static_cast<int32_t>(offsetof(DTDNode_t1758286970, ___baseURI_2)); }
	inline String_t* get_baseURI_2() const { return ___baseURI_2; }
	inline String_t** get_address_of_baseURI_2() { return &___baseURI_2; }
	inline void set_baseURI_2(String_t* value)
	{
		___baseURI_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_2), value);
	}

	inline static int32_t get_offset_of_lineNumber_3() { return static_cast<int32_t>(offsetof(DTDNode_t1758286970, ___lineNumber_3)); }
	inline int32_t get_lineNumber_3() const { return ___lineNumber_3; }
	inline int32_t* get_address_of_lineNumber_3() { return &___lineNumber_3; }
	inline void set_lineNumber_3(int32_t value)
	{
		___lineNumber_3 = value;
	}

	inline static int32_t get_offset_of_linePosition_4() { return static_cast<int32_t>(offsetof(DTDNode_t1758286970, ___linePosition_4)); }
	inline int32_t get_linePosition_4() const { return ___linePosition_4; }
	inline int32_t* get_address_of_linePosition_4() { return &___linePosition_4; }
	inline void set_linePosition_4(int32_t value)
	{
		___linePosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNODE_T1758286970_H
#ifndef DTDAUTOMATA_T545990600_H
#define DTDAUTOMATA_T545990600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAutomata
struct  DTDAutomata_t545990600  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDAutomata::root
	DTDObjectModel_t1113953282 * ___root_0;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDAutomata_t545990600, ___root_0)); }
	inline DTDObjectModel_t1113953282 * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1113953282 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1113953282 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDAUTOMATA_T545990600_H
#ifndef DTDPARAMETERENTITYDECLARATIONCOLLECTION_T3496720022_H
#define DTDPARAMETERENTITYDECLARATIONCOLLECTION_T3496720022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclarationCollection
struct  DTDParameterEntityDeclarationCollection_t3496720022  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.DTDParameterEntityDeclarationCollection::peDecls
	Hashtable_t909839986 * ___peDecls_0;
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDParameterEntityDeclarationCollection::root
	DTDObjectModel_t1113953282 * ___root_1;

public:
	inline static int32_t get_offset_of_peDecls_0() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_t3496720022, ___peDecls_0)); }
	inline Hashtable_t909839986 * get_peDecls_0() const { return ___peDecls_0; }
	inline Hashtable_t909839986 ** get_address_of_peDecls_0() { return &___peDecls_0; }
	inline void set_peDecls_0(Hashtable_t909839986 * value)
	{
		___peDecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_t3496720022, ___root_1)); }
	inline DTDObjectModel_t1113953282 * get_root_1() const { return ___root_1; }
	inline DTDObjectModel_t1113953282 ** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(DTDObjectModel_t1113953282 * value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATIONCOLLECTION_T3496720022_H
#ifndef DTDOBJECTMODEL_T1113953282_H
#define DTDOBJECTMODEL_T1113953282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDObjectModel
struct  DTDObjectModel_t1113953282  : public RuntimeObject
{
public:
	// Mono.Xml.DTDAutomataFactory Mono.Xml.DTDObjectModel::factory
	DTDAutomataFactory_t3605390810 * ___factory_0;
	// Mono.Xml.DTDElementAutomata Mono.Xml.DTDObjectModel::rootAutomata
	DTDElementAutomata_t2864881036 * ___rootAutomata_1;
	// Mono.Xml.DTDEmptyAutomata Mono.Xml.DTDObjectModel::emptyAutomata
	DTDEmptyAutomata_t411530619 * ___emptyAutomata_2;
	// Mono.Xml.DTDAnyAutomata Mono.Xml.DTDObjectModel::anyAutomata
	DTDAnyAutomata_t146446906 * ___anyAutomata_3;
	// Mono.Xml.DTDInvalidAutomata Mono.Xml.DTDObjectModel::invalidAutomata
	DTDInvalidAutomata_t247674167 * ___invalidAutomata_4;
	// Mono.Xml.DTDElementDeclarationCollection Mono.Xml.DTDObjectModel::elementDecls
	DTDElementDeclarationCollection_t2224069626 * ___elementDecls_5;
	// Mono.Xml.DTDAttListDeclarationCollection Mono.Xml.DTDObjectModel::attListDecls
	DTDAttListDeclarationCollection_t243645429 * ___attListDecls_6;
	// Mono.Xml.DTDParameterEntityDeclarationCollection Mono.Xml.DTDObjectModel::peDecls
	DTDParameterEntityDeclarationCollection_t3496720022 * ___peDecls_7;
	// Mono.Xml.DTDEntityDeclarationCollection Mono.Xml.DTDObjectModel::entityDecls
	DTDEntityDeclarationCollection_t1212505713 * ___entityDecls_8;
	// Mono.Xml.DTDNotationDeclarationCollection Mono.Xml.DTDObjectModel::notationDecls
	DTDNotationDeclarationCollection_t228085060 * ___notationDecls_9;
	// System.Collections.ArrayList Mono.Xml.DTDObjectModel::validationErrors
	ArrayList_t4252133567 * ___validationErrors_10;
	// System.Xml.XmlResolver Mono.Xml.DTDObjectModel::resolver
	XmlResolver_t2024571559 * ___resolver_11;
	// System.Xml.XmlNameTable Mono.Xml.DTDObjectModel::nameTable
	XmlNameTable_t1345805268 * ___nameTable_12;
	// System.Collections.Hashtable Mono.Xml.DTDObjectModel::externalResources
	Hashtable_t909839986 * ___externalResources_13;
	// System.String Mono.Xml.DTDObjectModel::baseURI
	String_t* ___baseURI_14;
	// System.String Mono.Xml.DTDObjectModel::name
	String_t* ___name_15;
	// System.String Mono.Xml.DTDObjectModel::publicId
	String_t* ___publicId_16;
	// System.String Mono.Xml.DTDObjectModel::systemId
	String_t* ___systemId_17;
	// System.String Mono.Xml.DTDObjectModel::intSubset
	String_t* ___intSubset_18;
	// System.Boolean Mono.Xml.DTDObjectModel::intSubsetHasPERef
	bool ___intSubsetHasPERef_19;
	// System.Boolean Mono.Xml.DTDObjectModel::isStandalone
	bool ___isStandalone_20;
	// System.Int32 Mono.Xml.DTDObjectModel::lineNumber
	int32_t ___lineNumber_21;
	// System.Int32 Mono.Xml.DTDObjectModel::linePosition
	int32_t ___linePosition_22;

public:
	inline static int32_t get_offset_of_factory_0() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___factory_0)); }
	inline DTDAutomataFactory_t3605390810 * get_factory_0() const { return ___factory_0; }
	inline DTDAutomataFactory_t3605390810 ** get_address_of_factory_0() { return &___factory_0; }
	inline void set_factory_0(DTDAutomataFactory_t3605390810 * value)
	{
		___factory_0 = value;
		Il2CppCodeGenWriteBarrier((&___factory_0), value);
	}

	inline static int32_t get_offset_of_rootAutomata_1() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___rootAutomata_1)); }
	inline DTDElementAutomata_t2864881036 * get_rootAutomata_1() const { return ___rootAutomata_1; }
	inline DTDElementAutomata_t2864881036 ** get_address_of_rootAutomata_1() { return &___rootAutomata_1; }
	inline void set_rootAutomata_1(DTDElementAutomata_t2864881036 * value)
	{
		___rootAutomata_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootAutomata_1), value);
	}

	inline static int32_t get_offset_of_emptyAutomata_2() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___emptyAutomata_2)); }
	inline DTDEmptyAutomata_t411530619 * get_emptyAutomata_2() const { return ___emptyAutomata_2; }
	inline DTDEmptyAutomata_t411530619 ** get_address_of_emptyAutomata_2() { return &___emptyAutomata_2; }
	inline void set_emptyAutomata_2(DTDEmptyAutomata_t411530619 * value)
	{
		___emptyAutomata_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAutomata_2), value);
	}

	inline static int32_t get_offset_of_anyAutomata_3() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___anyAutomata_3)); }
	inline DTDAnyAutomata_t146446906 * get_anyAutomata_3() const { return ___anyAutomata_3; }
	inline DTDAnyAutomata_t146446906 ** get_address_of_anyAutomata_3() { return &___anyAutomata_3; }
	inline void set_anyAutomata_3(DTDAnyAutomata_t146446906 * value)
	{
		___anyAutomata_3 = value;
		Il2CppCodeGenWriteBarrier((&___anyAutomata_3), value);
	}

	inline static int32_t get_offset_of_invalidAutomata_4() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___invalidAutomata_4)); }
	inline DTDInvalidAutomata_t247674167 * get_invalidAutomata_4() const { return ___invalidAutomata_4; }
	inline DTDInvalidAutomata_t247674167 ** get_address_of_invalidAutomata_4() { return &___invalidAutomata_4; }
	inline void set_invalidAutomata_4(DTDInvalidAutomata_t247674167 * value)
	{
		___invalidAutomata_4 = value;
		Il2CppCodeGenWriteBarrier((&___invalidAutomata_4), value);
	}

	inline static int32_t get_offset_of_elementDecls_5() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___elementDecls_5)); }
	inline DTDElementDeclarationCollection_t2224069626 * get_elementDecls_5() const { return ___elementDecls_5; }
	inline DTDElementDeclarationCollection_t2224069626 ** get_address_of_elementDecls_5() { return &___elementDecls_5; }
	inline void set_elementDecls_5(DTDElementDeclarationCollection_t2224069626 * value)
	{
		___elementDecls_5 = value;
		Il2CppCodeGenWriteBarrier((&___elementDecls_5), value);
	}

	inline static int32_t get_offset_of_attListDecls_6() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___attListDecls_6)); }
	inline DTDAttListDeclarationCollection_t243645429 * get_attListDecls_6() const { return ___attListDecls_6; }
	inline DTDAttListDeclarationCollection_t243645429 ** get_address_of_attListDecls_6() { return &___attListDecls_6; }
	inline void set_attListDecls_6(DTDAttListDeclarationCollection_t243645429 * value)
	{
		___attListDecls_6 = value;
		Il2CppCodeGenWriteBarrier((&___attListDecls_6), value);
	}

	inline static int32_t get_offset_of_peDecls_7() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___peDecls_7)); }
	inline DTDParameterEntityDeclarationCollection_t3496720022 * get_peDecls_7() const { return ___peDecls_7; }
	inline DTDParameterEntityDeclarationCollection_t3496720022 ** get_address_of_peDecls_7() { return &___peDecls_7; }
	inline void set_peDecls_7(DTDParameterEntityDeclarationCollection_t3496720022 * value)
	{
		___peDecls_7 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_7), value);
	}

	inline static int32_t get_offset_of_entityDecls_8() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___entityDecls_8)); }
	inline DTDEntityDeclarationCollection_t1212505713 * get_entityDecls_8() const { return ___entityDecls_8; }
	inline DTDEntityDeclarationCollection_t1212505713 ** get_address_of_entityDecls_8() { return &___entityDecls_8; }
	inline void set_entityDecls_8(DTDEntityDeclarationCollection_t1212505713 * value)
	{
		___entityDecls_8 = value;
		Il2CppCodeGenWriteBarrier((&___entityDecls_8), value);
	}

	inline static int32_t get_offset_of_notationDecls_9() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___notationDecls_9)); }
	inline DTDNotationDeclarationCollection_t228085060 * get_notationDecls_9() const { return ___notationDecls_9; }
	inline DTDNotationDeclarationCollection_t228085060 ** get_address_of_notationDecls_9() { return &___notationDecls_9; }
	inline void set_notationDecls_9(DTDNotationDeclarationCollection_t228085060 * value)
	{
		___notationDecls_9 = value;
		Il2CppCodeGenWriteBarrier((&___notationDecls_9), value);
	}

	inline static int32_t get_offset_of_validationErrors_10() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___validationErrors_10)); }
	inline ArrayList_t4252133567 * get_validationErrors_10() const { return ___validationErrors_10; }
	inline ArrayList_t4252133567 ** get_address_of_validationErrors_10() { return &___validationErrors_10; }
	inline void set_validationErrors_10(ArrayList_t4252133567 * value)
	{
		___validationErrors_10 = value;
		Il2CppCodeGenWriteBarrier((&___validationErrors_10), value);
	}

	inline static int32_t get_offset_of_resolver_11() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___resolver_11)); }
	inline XmlResolver_t2024571559 * get_resolver_11() const { return ___resolver_11; }
	inline XmlResolver_t2024571559 ** get_address_of_resolver_11() { return &___resolver_11; }
	inline void set_resolver_11(XmlResolver_t2024571559 * value)
	{
		___resolver_11 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_11), value);
	}

	inline static int32_t get_offset_of_nameTable_12() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___nameTable_12)); }
	inline XmlNameTable_t1345805268 * get_nameTable_12() const { return ___nameTable_12; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_12() { return &___nameTable_12; }
	inline void set_nameTable_12(XmlNameTable_t1345805268 * value)
	{
		___nameTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_12), value);
	}

	inline static int32_t get_offset_of_externalResources_13() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___externalResources_13)); }
	inline Hashtable_t909839986 * get_externalResources_13() const { return ___externalResources_13; }
	inline Hashtable_t909839986 ** get_address_of_externalResources_13() { return &___externalResources_13; }
	inline void set_externalResources_13(Hashtable_t909839986 * value)
	{
		___externalResources_13 = value;
		Il2CppCodeGenWriteBarrier((&___externalResources_13), value);
	}

	inline static int32_t get_offset_of_baseURI_14() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___baseURI_14)); }
	inline String_t* get_baseURI_14() const { return ___baseURI_14; }
	inline String_t** get_address_of_baseURI_14() { return &___baseURI_14; }
	inline void set_baseURI_14(String_t* value)
	{
		___baseURI_14 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_14), value);
	}

	inline static int32_t get_offset_of_name_15() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___name_15)); }
	inline String_t* get_name_15() const { return ___name_15; }
	inline String_t** get_address_of_name_15() { return &___name_15; }
	inline void set_name_15(String_t* value)
	{
		___name_15 = value;
		Il2CppCodeGenWriteBarrier((&___name_15), value);
	}

	inline static int32_t get_offset_of_publicId_16() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___publicId_16)); }
	inline String_t* get_publicId_16() const { return ___publicId_16; }
	inline String_t** get_address_of_publicId_16() { return &___publicId_16; }
	inline void set_publicId_16(String_t* value)
	{
		___publicId_16 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_16), value);
	}

	inline static int32_t get_offset_of_systemId_17() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___systemId_17)); }
	inline String_t* get_systemId_17() const { return ___systemId_17; }
	inline String_t** get_address_of_systemId_17() { return &___systemId_17; }
	inline void set_systemId_17(String_t* value)
	{
		___systemId_17 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_17), value);
	}

	inline static int32_t get_offset_of_intSubset_18() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___intSubset_18)); }
	inline String_t* get_intSubset_18() const { return ___intSubset_18; }
	inline String_t** get_address_of_intSubset_18() { return &___intSubset_18; }
	inline void set_intSubset_18(String_t* value)
	{
		___intSubset_18 = value;
		Il2CppCodeGenWriteBarrier((&___intSubset_18), value);
	}

	inline static int32_t get_offset_of_intSubsetHasPERef_19() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___intSubsetHasPERef_19)); }
	inline bool get_intSubsetHasPERef_19() const { return ___intSubsetHasPERef_19; }
	inline bool* get_address_of_intSubsetHasPERef_19() { return &___intSubsetHasPERef_19; }
	inline void set_intSubsetHasPERef_19(bool value)
	{
		___intSubsetHasPERef_19 = value;
	}

	inline static int32_t get_offset_of_isStandalone_20() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___isStandalone_20)); }
	inline bool get_isStandalone_20() const { return ___isStandalone_20; }
	inline bool* get_address_of_isStandalone_20() { return &___isStandalone_20; }
	inline void set_isStandalone_20(bool value)
	{
		___isStandalone_20 = value;
	}

	inline static int32_t get_offset_of_lineNumber_21() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___lineNumber_21)); }
	inline int32_t get_lineNumber_21() const { return ___lineNumber_21; }
	inline int32_t* get_address_of_lineNumber_21() { return &___lineNumber_21; }
	inline void set_lineNumber_21(int32_t value)
	{
		___lineNumber_21 = value;
	}

	inline static int32_t get_offset_of_linePosition_22() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1113953282, ___linePosition_22)); }
	inline int32_t get_linePosition_22() const { return ___linePosition_22; }
	inline int32_t* get_address_of_linePosition_22() { return &___linePosition_22; }
	inline void set_linePosition_22(int32_t value)
	{
		___linePosition_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOBJECTMODEL_T1113953282_H
#ifndef XQUERYCONVERT_T3510797773_H
#define XQUERYCONVERT_T3510797773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XQueryConvert
struct  XQueryConvert_t3510797773  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XQUERYCONVERT_T3510797773_H
#ifndef XMLNAMEDNODEMAP_T145210370_H
#define XMLNAMEDNODEMAP_T145210370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_t145210370  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t616554813 * ___parent_1;
	// System.Collections.ArrayList System.Xml.XmlNamedNodeMap::nodeList
	ArrayList_t4252133567 * ___nodeList_2;
	// System.Boolean System.Xml.XmlNamedNodeMap::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t145210370, ___parent_1)); }
	inline XmlNode_t616554813 * get_parent_1() const { return ___parent_1; }
	inline XmlNode_t616554813 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(XmlNode_t616554813 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___parent_1), value);
	}

	inline static int32_t get_offset_of_nodeList_2() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t145210370, ___nodeList_2)); }
	inline ArrayList_t4252133567 * get_nodeList_2() const { return ___nodeList_2; }
	inline ArrayList_t4252133567 ** get_address_of_nodeList_2() { return &___nodeList_2; }
	inline void set_nodeList_2(ArrayList_t4252133567 * value)
	{
		___nodeList_2 = value;
		Il2CppCodeGenWriteBarrier((&___nodeList_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t145210370, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct XmlNamedNodeMap_t145210370_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t145210370_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEDNODEMAP_T145210370_H
#ifndef CODEIDENTIFIER_T3245527920_H
#define CODEIDENTIFIER_T3245527920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.CodeIdentifier
struct  CodeIdentifier_t3245527920  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEIDENTIFIER_T3245527920_H
#ifndef XMLCHAR_T1369421061_H
#define XMLCHAR_T1369421061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChar
struct  XmlChar_t1369421061  : public RuntimeObject
{
public:

public:
};

struct XmlChar_t1369421061_StaticFields
{
public:
	// System.Char[] System.Xml.XmlChar::WhitespaceChars
	CharU5BU5D_t1328083999* ___WhitespaceChars_0;
	// System.Byte[] System.Xml.XmlChar::firstNamePages
	ByteU5BU5D_t3397334013* ___firstNamePages_1;
	// System.Byte[] System.Xml.XmlChar::namePages
	ByteU5BU5D_t3397334013* ___namePages_2;
	// System.UInt32[] System.Xml.XmlChar::nameBitmap
	UInt32U5BU5D_t59386216* ___nameBitmap_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlChar::<>f__switch$map47
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map47_4;

public:
	inline static int32_t get_offset_of_WhitespaceChars_0() { return static_cast<int32_t>(offsetof(XmlChar_t1369421061_StaticFields, ___WhitespaceChars_0)); }
	inline CharU5BU5D_t1328083999* get_WhitespaceChars_0() const { return ___WhitespaceChars_0; }
	inline CharU5BU5D_t1328083999** get_address_of_WhitespaceChars_0() { return &___WhitespaceChars_0; }
	inline void set_WhitespaceChars_0(CharU5BU5D_t1328083999* value)
	{
		___WhitespaceChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___WhitespaceChars_0), value);
	}

	inline static int32_t get_offset_of_firstNamePages_1() { return static_cast<int32_t>(offsetof(XmlChar_t1369421061_StaticFields, ___firstNamePages_1)); }
	inline ByteU5BU5D_t3397334013* get_firstNamePages_1() const { return ___firstNamePages_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_firstNamePages_1() { return &___firstNamePages_1; }
	inline void set_firstNamePages_1(ByteU5BU5D_t3397334013* value)
	{
		___firstNamePages_1 = value;
		Il2CppCodeGenWriteBarrier((&___firstNamePages_1), value);
	}

	inline static int32_t get_offset_of_namePages_2() { return static_cast<int32_t>(offsetof(XmlChar_t1369421061_StaticFields, ___namePages_2)); }
	inline ByteU5BU5D_t3397334013* get_namePages_2() const { return ___namePages_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_namePages_2() { return &___namePages_2; }
	inline void set_namePages_2(ByteU5BU5D_t3397334013* value)
	{
		___namePages_2 = value;
		Il2CppCodeGenWriteBarrier((&___namePages_2), value);
	}

	inline static int32_t get_offset_of_nameBitmap_3() { return static_cast<int32_t>(offsetof(XmlChar_t1369421061_StaticFields, ___nameBitmap_3)); }
	inline UInt32U5BU5D_t59386216* get_nameBitmap_3() const { return ___nameBitmap_3; }
	inline UInt32U5BU5D_t59386216** get_address_of_nameBitmap_3() { return &___nameBitmap_3; }
	inline void set_nameBitmap_3(UInt32U5BU5D_t59386216* value)
	{
		___nameBitmap_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameBitmap_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map47_4() { return static_cast<int32_t>(offsetof(XmlChar_t1369421061_StaticFields, ___U3CU3Ef__switchU24map47_4)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map47_4() const { return ___U3CU3Ef__switchU24map47_4; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map47_4() { return &___U3CU3Ef__switchU24map47_4; }
	inline void set_U3CU3Ef__switchU24map47_4(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map47_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map47_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHAR_T1369421061_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public RuntimeObject
{
public:

public:
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_0)); }
	inline Stream_t3255436806 * get_Null_0() const { return ___Null_0; }
	inline Stream_t3255436806 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t3255436806 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef TYPETRANSLATOR_T1077722680_H
#define TYPETRANSLATOR_T1077722680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeTranslator
struct  TypeTranslator_t1077722680  : public RuntimeObject
{
public:

public:
};

struct TypeTranslator_t1077722680_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::nameCache
	Hashtable_t909839986 * ___nameCache_0;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::primitiveTypes
	Hashtable_t909839986 * ___primitiveTypes_1;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::primitiveArrayTypes
	Hashtable_t909839986 * ___primitiveArrayTypes_2;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::nullableTypes
	Hashtable_t909839986 * ___nullableTypes_3;

public:
	inline static int32_t get_offset_of_nameCache_0() { return static_cast<int32_t>(offsetof(TypeTranslator_t1077722680_StaticFields, ___nameCache_0)); }
	inline Hashtable_t909839986 * get_nameCache_0() const { return ___nameCache_0; }
	inline Hashtable_t909839986 ** get_address_of_nameCache_0() { return &___nameCache_0; }
	inline void set_nameCache_0(Hashtable_t909839986 * value)
	{
		___nameCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameCache_0), value);
	}

	inline static int32_t get_offset_of_primitiveTypes_1() { return static_cast<int32_t>(offsetof(TypeTranslator_t1077722680_StaticFields, ___primitiveTypes_1)); }
	inline Hashtable_t909839986 * get_primitiveTypes_1() const { return ___primitiveTypes_1; }
	inline Hashtable_t909839986 ** get_address_of_primitiveTypes_1() { return &___primitiveTypes_1; }
	inline void set_primitiveTypes_1(Hashtable_t909839986 * value)
	{
		___primitiveTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveTypes_1), value);
	}

	inline static int32_t get_offset_of_primitiveArrayTypes_2() { return static_cast<int32_t>(offsetof(TypeTranslator_t1077722680_StaticFields, ___primitiveArrayTypes_2)); }
	inline Hashtable_t909839986 * get_primitiveArrayTypes_2() const { return ___primitiveArrayTypes_2; }
	inline Hashtable_t909839986 ** get_address_of_primitiveArrayTypes_2() { return &___primitiveArrayTypes_2; }
	inline void set_primitiveArrayTypes_2(Hashtable_t909839986 * value)
	{
		___primitiveArrayTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveArrayTypes_2), value);
	}

	inline static int32_t get_offset_of_nullableTypes_3() { return static_cast<int32_t>(offsetof(TypeTranslator_t1077722680_StaticFields, ___nullableTypes_3)); }
	inline Hashtable_t909839986 * get_nullableTypes_3() const { return ___nullableTypes_3; }
	inline Hashtable_t909839986 ** get_address_of_nullableTypes_3() { return &___nullableTypes_3; }
	inline void set_nullableTypes_3(Hashtable_t909839986 * value)
	{
		___nullableTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___nullableTypes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPETRANSLATOR_T1077722680_H
#ifndef TEXTREADER_T1561828458_H
#define TEXTREADER_T1561828458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_t1561828458  : public RuntimeObject
{
public:

public:
};

struct TextReader_t1561828458_StaticFields
{
public:
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t1561828458 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(TextReader_t1561828458_StaticFields, ___Null_0)); }
	inline TextReader_t1561828458 * get_Null_0() const { return ___Null_0; }
	inline TextReader_t1561828458 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(TextReader_t1561828458 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADER_T1561828458_H
#ifndef XMLNODE_T616554813_H
#define XMLNODE_T616554813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t616554813  : public RuntimeObject
{
public:
	// System.Xml.XmlDocument System.Xml.XmlNode::ownerDocument
	XmlDocument_t3649534162 * ___ownerDocument_1;
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t616554813 * ___parentNode_2;
	// System.Xml.XmlNodeListChildren System.Xml.XmlNode::childNodes
	XmlNodeListChildren_t2811458520 * ___childNodes_3;

public:
	inline static int32_t get_offset_of_ownerDocument_1() { return static_cast<int32_t>(offsetof(XmlNode_t616554813, ___ownerDocument_1)); }
	inline XmlDocument_t3649534162 * get_ownerDocument_1() const { return ___ownerDocument_1; }
	inline XmlDocument_t3649534162 ** get_address_of_ownerDocument_1() { return &___ownerDocument_1; }
	inline void set_ownerDocument_1(XmlDocument_t3649534162 * value)
	{
		___ownerDocument_1 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_1), value);
	}

	inline static int32_t get_offset_of_parentNode_2() { return static_cast<int32_t>(offsetof(XmlNode_t616554813, ___parentNode_2)); }
	inline XmlNode_t616554813 * get_parentNode_2() const { return ___parentNode_2; }
	inline XmlNode_t616554813 ** get_address_of_parentNode_2() { return &___parentNode_2; }
	inline void set_parentNode_2(XmlNode_t616554813 * value)
	{
		___parentNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_2), value);
	}

	inline static int32_t get_offset_of_childNodes_3() { return static_cast<int32_t>(offsetof(XmlNode_t616554813, ___childNodes_3)); }
	inline XmlNodeListChildren_t2811458520 * get_childNodes_3() const { return ___childNodes_3; }
	inline XmlNodeListChildren_t2811458520 ** get_address_of_childNodes_3() { return &___childNodes_3; }
	inline void set_childNodes_3(XmlNodeListChildren_t2811458520 * value)
	{
		___childNodes_3 = value;
		Il2CppCodeGenWriteBarrier((&___childNodes_3), value);
	}
};

struct XmlNode_t616554813_StaticFields
{
public:
	// System.Xml.XmlNode/EmptyNodeList System.Xml.XmlNode::emptyList
	EmptyNodeList_t1718403287 * ___emptyList_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNode::<>f__switch$map44
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map44_4;

public:
	inline static int32_t get_offset_of_emptyList_0() { return static_cast<int32_t>(offsetof(XmlNode_t616554813_StaticFields, ___emptyList_0)); }
	inline EmptyNodeList_t1718403287 * get_emptyList_0() const { return ___emptyList_0; }
	inline EmptyNodeList_t1718403287 ** get_address_of_emptyList_0() { return &___emptyList_0; }
	inline void set_emptyList_0(EmptyNodeList_t1718403287 * value)
	{
		___emptyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map44_4() { return static_cast<int32_t>(offsetof(XmlNode_t616554813_StaticFields, ___U3CU3Ef__switchU24map44_4)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map44_4() const { return ___U3CU3Ef__switchU24map44_4; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map44_4() { return &___U3CU3Ef__switchU24map44_4; }
	inline void set_U3CU3Ef__switchU24map44_4(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map44_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map44_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODE_T616554813_H
#ifndef XMLNAMETABLE_T1345805268_H
#define XMLNAMETABLE_T1345805268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t1345805268  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T1345805268_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef NAMETABLE_T594386929_H
#define NAMETABLE_T594386929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable
struct  NameTable_t594386929  : public XmlNameTable_t1345805268
{
public:
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_0;
	// System.Xml.NameTable/Entry[] System.Xml.NameTable::buckets
	EntryU5BU5D_t180042139* ___buckets_1;
	// System.Int32 System.Xml.NameTable::size
	int32_t ___size_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_buckets_1() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___buckets_1)); }
	inline EntryU5BU5D_t180042139* get_buckets_1() const { return ___buckets_1; }
	inline EntryU5BU5D_t180042139** get_address_of_buckets_1() { return &___buckets_1; }
	inline void set_buckets_1(EntryU5BU5D_t180042139* value)
	{
		___buckets_1 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(NameTable_t594386929, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETABLE_T594386929_H
#ifndef DTDELEMENTDECLARATION_T8748002_H
#define DTDELEMENTDECLARATION_T8748002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclaration
struct  DTDElementDeclaration_t8748002  : public DTDNode_t1758286970
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDElementDeclaration::root
	DTDObjectModel_t1113953282 * ___root_5;
	// Mono.Xml.DTDContentModel Mono.Xml.DTDElementDeclaration::contentModel
	DTDContentModel_t445576364 * ___contentModel_6;
	// System.String Mono.Xml.DTDElementDeclaration::name
	String_t* ___name_7;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isEmpty
	bool ___isEmpty_8;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isAny
	bool ___isAny_9;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isMixedContent
	bool ___isMixedContent_10;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t8748002, ___root_5)); }
	inline DTDObjectModel_t1113953282 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1113953282 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1113953282 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_contentModel_6() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t8748002, ___contentModel_6)); }
	inline DTDContentModel_t445576364 * get_contentModel_6() const { return ___contentModel_6; }
	inline DTDContentModel_t445576364 ** get_address_of_contentModel_6() { return &___contentModel_6; }
	inline void set_contentModel_6(DTDContentModel_t445576364 * value)
	{
		___contentModel_6 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t8748002, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_isEmpty_8() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t8748002, ___isEmpty_8)); }
	inline bool get_isEmpty_8() const { return ___isEmpty_8; }
	inline bool* get_address_of_isEmpty_8() { return &___isEmpty_8; }
	inline void set_isEmpty_8(bool value)
	{
		___isEmpty_8 = value;
	}

	inline static int32_t get_offset_of_isAny_9() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t8748002, ___isAny_9)); }
	inline bool get_isAny_9() const { return ___isAny_9; }
	inline bool* get_address_of_isAny_9() { return &___isAny_9; }
	inline void set_isAny_9(bool value)
	{
		___isAny_9 = value;
	}

	inline static int32_t get_offset_of_isMixedContent_10() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t8748002, ___isMixedContent_10)); }
	inline bool get_isMixedContent_10() const { return ___isMixedContent_10; }
	inline bool* get_address_of_isMixedContent_10() { return &___isMixedContent_10; }
	inline void set_isMixedContent_10(bool value)
	{
		___isMixedContent_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATION_T8748002_H
#ifndef XMLATTRIBUTE_T175731005_H
#define XMLATTRIBUTE_T175731005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttribute
struct  XmlAttribute_t175731005  : public XmlNode_t616554813
{
public:
	// System.Xml.XmlNameEntry System.Xml.XmlAttribute::name
	XmlNameEntry_t3745551716 * ___name_5;
	// System.Boolean System.Xml.XmlAttribute::isDefault
	bool ___isDefault_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastLinkedChild
	XmlLinkedNode_t1287616130 * ___lastLinkedChild_7;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlAttribute::schemaInfo
	RuntimeObject* ___schemaInfo_8;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XmlAttribute_t175731005, ___name_5)); }
	inline XmlNameEntry_t3745551716 * get_name_5() const { return ___name_5; }
	inline XmlNameEntry_t3745551716 ** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(XmlNameEntry_t3745551716 * value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_isDefault_6() { return static_cast<int32_t>(offsetof(XmlAttribute_t175731005, ___isDefault_6)); }
	inline bool get_isDefault_6() const { return ___isDefault_6; }
	inline bool* get_address_of_isDefault_6() { return &___isDefault_6; }
	inline void set_isDefault_6(bool value)
	{
		___isDefault_6 = value;
	}

	inline static int32_t get_offset_of_lastLinkedChild_7() { return static_cast<int32_t>(offsetof(XmlAttribute_t175731005, ___lastLinkedChild_7)); }
	inline XmlLinkedNode_t1287616130 * get_lastLinkedChild_7() const { return ___lastLinkedChild_7; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastLinkedChild_7() { return &___lastLinkedChild_7; }
	inline void set_lastLinkedChild_7(XmlLinkedNode_t1287616130 * value)
	{
		___lastLinkedChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_7), value);
	}

	inline static int32_t get_offset_of_schemaInfo_8() { return static_cast<int32_t>(offsetof(XmlAttribute_t175731005, ___schemaInfo_8)); }
	inline RuntimeObject* get_schemaInfo_8() const { return ___schemaInfo_8; }
	inline RuntimeObject** get_address_of_schemaInfo_8() { return &___schemaInfo_8; }
	inline void set_schemaInfo_8(RuntimeObject* value)
	{
		___schemaInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTE_T175731005_H
#ifndef XMLDOCUMENTFRAGMENT_T3083262362_H
#define XMLDOCUMENTFRAGMENT_T3083262362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentFragment
struct  XmlDocumentFragment_t3083262362  : public XmlNode_t616554813
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlDocumentFragment::lastLinkedChild
	XmlLinkedNode_t1287616130 * ___lastLinkedChild_5;

public:
	inline static int32_t get_offset_of_lastLinkedChild_5() { return static_cast<int32_t>(offsetof(XmlDocumentFragment_t3083262362, ___lastLinkedChild_5)); }
	inline XmlLinkedNode_t1287616130 * get_lastLinkedChild_5() const { return ___lastLinkedChild_5; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastLinkedChild_5() { return &___lastLinkedChild_5; }
	inline void set_lastLinkedChild_5(XmlLinkedNode_t1287616130 * value)
	{
		___lastLinkedChild_5 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTFRAGMENT_T3083262362_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2510243513 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef XMLATTRIBUTECOLLECTION_T3359885287_H
#define XMLATTRIBUTECOLLECTION_T3359885287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttributeCollection
struct  XmlAttributeCollection_t3359885287  : public XmlNamedNodeMap_t145210370
{
public:
	// System.Xml.XmlElement System.Xml.XmlAttributeCollection::ownerElement
	XmlElement_t2877111883 * ___ownerElement_4;
	// System.Xml.XmlDocument System.Xml.XmlAttributeCollection::ownerDocument
	XmlDocument_t3649534162 * ___ownerDocument_5;

public:
	inline static int32_t get_offset_of_ownerElement_4() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t3359885287, ___ownerElement_4)); }
	inline XmlElement_t2877111883 * get_ownerElement_4() const { return ___ownerElement_4; }
	inline XmlElement_t2877111883 ** get_address_of_ownerElement_4() { return &___ownerElement_4; }
	inline void set_ownerElement_4(XmlElement_t2877111883 * value)
	{
		___ownerElement_4 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElement_4), value);
	}

	inline static int32_t get_offset_of_ownerDocument_5() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t3359885287, ___ownerDocument_5)); }
	inline XmlDocument_t3649534162 * get_ownerDocument_5() const { return ___ownerDocument_5; }
	inline XmlDocument_t3649534162 ** get_address_of_ownerDocument_5() { return &___ownerDocument_5; }
	inline void set_ownerDocument_5(XmlDocument_t3649534162 * value)
	{
		___ownerDocument_5 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTECOLLECTION_T3359885287_H
#ifndef DTDATTLISTDECLARATION_T2272374839_H
#define DTDATTLISTDECLARATION_T2272374839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclaration
struct  DTDAttListDeclaration_t2272374839  : public DTDNode_t1758286970
{
public:
	// System.String Mono.Xml.DTDAttListDeclaration::name
	String_t* ___name_5;
	// System.Collections.Hashtable Mono.Xml.DTDAttListDeclaration::attributeOrders
	Hashtable_t909839986 * ___attributeOrders_6;
	// System.Collections.ArrayList Mono.Xml.DTDAttListDeclaration::attributes
	ArrayList_t4252133567 * ___attributes_7;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t2272374839, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_attributeOrders_6() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t2272374839, ___attributeOrders_6)); }
	inline Hashtable_t909839986 * get_attributeOrders_6() const { return ___attributeOrders_6; }
	inline Hashtable_t909839986 ** get_address_of_attributeOrders_6() { return &___attributeOrders_6; }
	inline void set_attributeOrders_6(Hashtable_t909839986 * value)
	{
		___attributeOrders_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributeOrders_6), value);
	}

	inline static int32_t get_offset_of_attributes_7() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t2272374839, ___attributes_7)); }
	inline ArrayList_t4252133567 * get_attributes_7() const { return ___attributes_7; }
	inline ArrayList_t4252133567 ** get_address_of_attributes_7() { return &___attributes_7; }
	inline void set_attributes_7(ArrayList_t4252133567 * value)
	{
		___attributes_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATION_T2272374839_H
#ifndef XMLDOCUMENT_T3649534162_H
#define XMLDOCUMENT_T3649534162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocument
struct  XmlDocument_t3649534162  : public XmlNode_t616554813
{
public:
	// System.Boolean System.Xml.XmlDocument::optimal_create_element
	bool ___optimal_create_element_6;
	// System.Boolean System.Xml.XmlDocument::optimal_create_attribute
	bool ___optimal_create_attribute_7;
	// System.Xml.XmlNameTable System.Xml.XmlDocument::nameTable
	XmlNameTable_t1345805268 * ___nameTable_8;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_9;
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_t1664517635 * ___implementation_10;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_11;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_t2024571559 * ___resolver_12;
	// System.Collections.Hashtable System.Xml.XmlDocument::idTable
	Hashtable_t909839986 * ___idTable_13;
	// System.Xml.XmlNameEntryCache System.Xml.XmlDocument::nameCache
	XmlNameEntryCache_t3855584002 * ___nameCache_14;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastLinkedChild
	XmlLinkedNode_t1287616130 * ___lastLinkedChild_15;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::schemaInfo
	RuntimeObject* ___schemaInfo_16;
	// System.Boolean System.Xml.XmlDocument::loadMode
	bool ___loadMode_17;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanged
	XmlNodeChangedEventHandler_t2964483403 * ___NodeChanged_18;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanging
	XmlNodeChangedEventHandler_t2964483403 * ___NodeChanging_19;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserted
	XmlNodeChangedEventHandler_t2964483403 * ___NodeInserted_20;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserting
	XmlNodeChangedEventHandler_t2964483403 * ___NodeInserting_21;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoved
	XmlNodeChangedEventHandler_t2964483403 * ___NodeRemoved_22;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoving
	XmlNodeChangedEventHandler_t2964483403 * ___NodeRemoving_23;

public:
	inline static int32_t get_offset_of_optimal_create_element_6() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___optimal_create_element_6)); }
	inline bool get_optimal_create_element_6() const { return ___optimal_create_element_6; }
	inline bool* get_address_of_optimal_create_element_6() { return &___optimal_create_element_6; }
	inline void set_optimal_create_element_6(bool value)
	{
		___optimal_create_element_6 = value;
	}

	inline static int32_t get_offset_of_optimal_create_attribute_7() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___optimal_create_attribute_7)); }
	inline bool get_optimal_create_attribute_7() const { return ___optimal_create_attribute_7; }
	inline bool* get_address_of_optimal_create_attribute_7() { return &___optimal_create_attribute_7; }
	inline void set_optimal_create_attribute_7(bool value)
	{
		___optimal_create_attribute_7 = value;
	}

	inline static int32_t get_offset_of_nameTable_8() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___nameTable_8)); }
	inline XmlNameTable_t1345805268 * get_nameTable_8() const { return ___nameTable_8; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_8() { return &___nameTable_8; }
	inline void set_nameTable_8(XmlNameTable_t1345805268 * value)
	{
		___nameTable_8 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_8), value);
	}

	inline static int32_t get_offset_of_baseURI_9() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___baseURI_9)); }
	inline String_t* get_baseURI_9() const { return ___baseURI_9; }
	inline String_t** get_address_of_baseURI_9() { return &___baseURI_9; }
	inline void set_baseURI_9(String_t* value)
	{
		___baseURI_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_9), value);
	}

	inline static int32_t get_offset_of_implementation_10() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___implementation_10)); }
	inline XmlImplementation_t1664517635 * get_implementation_10() const { return ___implementation_10; }
	inline XmlImplementation_t1664517635 ** get_address_of_implementation_10() { return &___implementation_10; }
	inline void set_implementation_10(XmlImplementation_t1664517635 * value)
	{
		___implementation_10 = value;
		Il2CppCodeGenWriteBarrier((&___implementation_10), value);
	}

	inline static int32_t get_offset_of_preserveWhitespace_11() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___preserveWhitespace_11)); }
	inline bool get_preserveWhitespace_11() const { return ___preserveWhitespace_11; }
	inline bool* get_address_of_preserveWhitespace_11() { return &___preserveWhitespace_11; }
	inline void set_preserveWhitespace_11(bool value)
	{
		___preserveWhitespace_11 = value;
	}

	inline static int32_t get_offset_of_resolver_12() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___resolver_12)); }
	inline XmlResolver_t2024571559 * get_resolver_12() const { return ___resolver_12; }
	inline XmlResolver_t2024571559 ** get_address_of_resolver_12() { return &___resolver_12; }
	inline void set_resolver_12(XmlResolver_t2024571559 * value)
	{
		___resolver_12 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_12), value);
	}

	inline static int32_t get_offset_of_idTable_13() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___idTable_13)); }
	inline Hashtable_t909839986 * get_idTable_13() const { return ___idTable_13; }
	inline Hashtable_t909839986 ** get_address_of_idTable_13() { return &___idTable_13; }
	inline void set_idTable_13(Hashtable_t909839986 * value)
	{
		___idTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___idTable_13), value);
	}

	inline static int32_t get_offset_of_nameCache_14() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___nameCache_14)); }
	inline XmlNameEntryCache_t3855584002 * get_nameCache_14() const { return ___nameCache_14; }
	inline XmlNameEntryCache_t3855584002 ** get_address_of_nameCache_14() { return &___nameCache_14; }
	inline void set_nameCache_14(XmlNameEntryCache_t3855584002 * value)
	{
		___nameCache_14 = value;
		Il2CppCodeGenWriteBarrier((&___nameCache_14), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_15() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___lastLinkedChild_15)); }
	inline XmlLinkedNode_t1287616130 * get_lastLinkedChild_15() const { return ___lastLinkedChild_15; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastLinkedChild_15() { return &___lastLinkedChild_15; }
	inline void set_lastLinkedChild_15(XmlLinkedNode_t1287616130 * value)
	{
		___lastLinkedChild_15 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_15), value);
	}

	inline static int32_t get_offset_of_schemaInfo_16() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___schemaInfo_16)); }
	inline RuntimeObject* get_schemaInfo_16() const { return ___schemaInfo_16; }
	inline RuntimeObject** get_address_of_schemaInfo_16() { return &___schemaInfo_16; }
	inline void set_schemaInfo_16(RuntimeObject* value)
	{
		___schemaInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_16), value);
	}

	inline static int32_t get_offset_of_loadMode_17() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___loadMode_17)); }
	inline bool get_loadMode_17() const { return ___loadMode_17; }
	inline bool* get_address_of_loadMode_17() { return &___loadMode_17; }
	inline void set_loadMode_17(bool value)
	{
		___loadMode_17 = value;
	}

	inline static int32_t get_offset_of_NodeChanged_18() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___NodeChanged_18)); }
	inline XmlNodeChangedEventHandler_t2964483403 * get_NodeChanged_18() const { return ___NodeChanged_18; }
	inline XmlNodeChangedEventHandler_t2964483403 ** get_address_of_NodeChanged_18() { return &___NodeChanged_18; }
	inline void set_NodeChanged_18(XmlNodeChangedEventHandler_t2964483403 * value)
	{
		___NodeChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanged_18), value);
	}

	inline static int32_t get_offset_of_NodeChanging_19() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___NodeChanging_19)); }
	inline XmlNodeChangedEventHandler_t2964483403 * get_NodeChanging_19() const { return ___NodeChanging_19; }
	inline XmlNodeChangedEventHandler_t2964483403 ** get_address_of_NodeChanging_19() { return &___NodeChanging_19; }
	inline void set_NodeChanging_19(XmlNodeChangedEventHandler_t2964483403 * value)
	{
		___NodeChanging_19 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanging_19), value);
	}

	inline static int32_t get_offset_of_NodeInserted_20() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___NodeInserted_20)); }
	inline XmlNodeChangedEventHandler_t2964483403 * get_NodeInserted_20() const { return ___NodeInserted_20; }
	inline XmlNodeChangedEventHandler_t2964483403 ** get_address_of_NodeInserted_20() { return &___NodeInserted_20; }
	inline void set_NodeInserted_20(XmlNodeChangedEventHandler_t2964483403 * value)
	{
		___NodeInserted_20 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserted_20), value);
	}

	inline static int32_t get_offset_of_NodeInserting_21() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___NodeInserting_21)); }
	inline XmlNodeChangedEventHandler_t2964483403 * get_NodeInserting_21() const { return ___NodeInserting_21; }
	inline XmlNodeChangedEventHandler_t2964483403 ** get_address_of_NodeInserting_21() { return &___NodeInserting_21; }
	inline void set_NodeInserting_21(XmlNodeChangedEventHandler_t2964483403 * value)
	{
		___NodeInserting_21 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserting_21), value);
	}

	inline static int32_t get_offset_of_NodeRemoved_22() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___NodeRemoved_22)); }
	inline XmlNodeChangedEventHandler_t2964483403 * get_NodeRemoved_22() const { return ___NodeRemoved_22; }
	inline XmlNodeChangedEventHandler_t2964483403 ** get_address_of_NodeRemoved_22() { return &___NodeRemoved_22; }
	inline void set_NodeRemoved_22(XmlNodeChangedEventHandler_t2964483403 * value)
	{
		___NodeRemoved_22 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoved_22), value);
	}

	inline static int32_t get_offset_of_NodeRemoving_23() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162, ___NodeRemoving_23)); }
	inline XmlNodeChangedEventHandler_t2964483403 * get_NodeRemoving_23() const { return ___NodeRemoving_23; }
	inline XmlNodeChangedEventHandler_t2964483403 ** get_address_of_NodeRemoving_23() { return &___NodeRemoving_23; }
	inline void set_NodeRemoving_23(XmlNodeChangedEventHandler_t2964483403 * value)
	{
		___NodeRemoving_23 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoving_23), value);
	}
};

struct XmlDocument_t3649534162_StaticFields
{
public:
	// System.Type[] System.Xml.XmlDocument::optimal_create_types
	TypeU5BU5D_t1664964607* ___optimal_create_types_5;

public:
	inline static int32_t get_offset_of_optimal_create_types_5() { return static_cast<int32_t>(offsetof(XmlDocument_t3649534162_StaticFields, ___optimal_create_types_5)); }
	inline TypeU5BU5D_t1664964607* get_optimal_create_types_5() const { return ___optimal_create_types_5; }
	inline TypeU5BU5D_t1664964607** get_address_of_optimal_create_types_5() { return &___optimal_create_types_5; }
	inline void set_optimal_create_types_5(TypeU5BU5D_t1664964607* value)
	{
		___optimal_create_types_5 = value;
		Il2CppCodeGenWriteBarrier((&___optimal_create_types_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENT_T3649534162_H
#ifndef DTDVALIDATINGREADER_T4120969348_H
#define DTDVALIDATINGREADER_T4120969348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDValidatingReader
struct  DTDValidatingReader_t4120969348  : public XmlReader_t3675626668
{
public:
	// Mono.Xml.EntityResolvingXmlReader Mono.Xml.DTDValidatingReader::reader
	EntityResolvingXmlReader_t2086920314 * ___reader_2;
	// System.Xml.XmlTextReader Mono.Xml.DTDValidatingReader::sourceTextReader
	XmlTextReader_t3514170725 * ___sourceTextReader_3;
	// System.Xml.XmlValidatingReader Mono.Xml.DTDValidatingReader::validatingReader
	XmlValidatingReader_t3416770767 * ___validatingReader_4;
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDValidatingReader::dtd
	DTDObjectModel_t1113953282 * ___dtd_5;
	// System.Xml.XmlResolver Mono.Xml.DTDValidatingReader::resolver
	XmlResolver_t2024571559 * ___resolver_6;
	// System.String Mono.Xml.DTDValidatingReader::currentElement
	String_t* ___currentElement_7;
	// Mono.Xml.DTDValidatingReader/AttributeSlot[] Mono.Xml.DTDValidatingReader::attributes
	AttributeSlotU5BU5D_t1168612672* ___attributes_8;
	// System.Int32 Mono.Xml.DTDValidatingReader::attributeCount
	int32_t ___attributeCount_9;
	// System.Int32 Mono.Xml.DTDValidatingReader::currentAttribute
	int32_t ___currentAttribute_10;
	// System.Boolean Mono.Xml.DTDValidatingReader::consumedAttribute
	bool ___consumedAttribute_11;
	// System.Collections.Stack Mono.Xml.DTDValidatingReader::elementStack
	Stack_t1043988394 * ___elementStack_12;
	// System.Collections.Stack Mono.Xml.DTDValidatingReader::automataStack
	Stack_t1043988394 * ___automataStack_13;
	// System.Boolean Mono.Xml.DTDValidatingReader::popScope
	bool ___popScope_14;
	// System.Boolean Mono.Xml.DTDValidatingReader::isStandalone
	bool ___isStandalone_15;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDValidatingReader::currentAutomata
	DTDAutomata_t545990600 * ___currentAutomata_16;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDValidatingReader::previousAutomata
	DTDAutomata_t545990600 * ___previousAutomata_17;
	// System.Collections.ArrayList Mono.Xml.DTDValidatingReader::idList
	ArrayList_t4252133567 * ___idList_18;
	// System.Collections.ArrayList Mono.Xml.DTDValidatingReader::missingIDReferences
	ArrayList_t4252133567 * ___missingIDReferences_19;
	// System.Xml.XmlNamespaceManager Mono.Xml.DTDValidatingReader::nsmgr
	XmlNamespaceManager_t486731501 * ___nsmgr_20;
	// System.String Mono.Xml.DTDValidatingReader::currentTextValue
	String_t* ___currentTextValue_21;
	// System.String Mono.Xml.DTDValidatingReader::constructingTextValue
	String_t* ___constructingTextValue_22;
	// System.Boolean Mono.Xml.DTDValidatingReader::shouldResetCurrentTextValue
	bool ___shouldResetCurrentTextValue_23;
	// System.Boolean Mono.Xml.DTDValidatingReader::isSignificantWhitespace
	bool ___isSignificantWhitespace_24;
	// System.Boolean Mono.Xml.DTDValidatingReader::isWhitespace
	bool ___isWhitespace_25;
	// System.Boolean Mono.Xml.DTDValidatingReader::isText
	bool ___isText_26;
	// System.Collections.Stack Mono.Xml.DTDValidatingReader::attributeValueEntityStack
	Stack_t1043988394 * ___attributeValueEntityStack_27;
	// System.Text.StringBuilder Mono.Xml.DTDValidatingReader::valueBuilder
	StringBuilder_t1221177846 * ___valueBuilder_28;
	// System.Char[] Mono.Xml.DTDValidatingReader::whitespaceChars
	CharU5BU5D_t1328083999* ___whitespaceChars_29;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___reader_2)); }
	inline EntityResolvingXmlReader_t2086920314 * get_reader_2() const { return ___reader_2; }
	inline EntityResolvingXmlReader_t2086920314 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(EntityResolvingXmlReader_t2086920314 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_sourceTextReader_3() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___sourceTextReader_3)); }
	inline XmlTextReader_t3514170725 * get_sourceTextReader_3() const { return ___sourceTextReader_3; }
	inline XmlTextReader_t3514170725 ** get_address_of_sourceTextReader_3() { return &___sourceTextReader_3; }
	inline void set_sourceTextReader_3(XmlTextReader_t3514170725 * value)
	{
		___sourceTextReader_3 = value;
		Il2CppCodeGenWriteBarrier((&___sourceTextReader_3), value);
	}

	inline static int32_t get_offset_of_validatingReader_4() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___validatingReader_4)); }
	inline XmlValidatingReader_t3416770767 * get_validatingReader_4() const { return ___validatingReader_4; }
	inline XmlValidatingReader_t3416770767 ** get_address_of_validatingReader_4() { return &___validatingReader_4; }
	inline void set_validatingReader_4(XmlValidatingReader_t3416770767 * value)
	{
		___validatingReader_4 = value;
		Il2CppCodeGenWriteBarrier((&___validatingReader_4), value);
	}

	inline static int32_t get_offset_of_dtd_5() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___dtd_5)); }
	inline DTDObjectModel_t1113953282 * get_dtd_5() const { return ___dtd_5; }
	inline DTDObjectModel_t1113953282 ** get_address_of_dtd_5() { return &___dtd_5; }
	inline void set_dtd_5(DTDObjectModel_t1113953282 * value)
	{
		___dtd_5 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_5), value);
	}

	inline static int32_t get_offset_of_resolver_6() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___resolver_6)); }
	inline XmlResolver_t2024571559 * get_resolver_6() const { return ___resolver_6; }
	inline XmlResolver_t2024571559 ** get_address_of_resolver_6() { return &___resolver_6; }
	inline void set_resolver_6(XmlResolver_t2024571559 * value)
	{
		___resolver_6 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_6), value);
	}

	inline static int32_t get_offset_of_currentElement_7() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___currentElement_7)); }
	inline String_t* get_currentElement_7() const { return ___currentElement_7; }
	inline String_t** get_address_of_currentElement_7() { return &___currentElement_7; }
	inline void set_currentElement_7(String_t* value)
	{
		___currentElement_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentElement_7), value);
	}

	inline static int32_t get_offset_of_attributes_8() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___attributes_8)); }
	inline AttributeSlotU5BU5D_t1168612672* get_attributes_8() const { return ___attributes_8; }
	inline AttributeSlotU5BU5D_t1168612672** get_address_of_attributes_8() { return &___attributes_8; }
	inline void set_attributes_8(AttributeSlotU5BU5D_t1168612672* value)
	{
		___attributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_8), value);
	}

	inline static int32_t get_offset_of_attributeCount_9() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___attributeCount_9)); }
	inline int32_t get_attributeCount_9() const { return ___attributeCount_9; }
	inline int32_t* get_address_of_attributeCount_9() { return &___attributeCount_9; }
	inline void set_attributeCount_9(int32_t value)
	{
		___attributeCount_9 = value;
	}

	inline static int32_t get_offset_of_currentAttribute_10() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___currentAttribute_10)); }
	inline int32_t get_currentAttribute_10() const { return ___currentAttribute_10; }
	inline int32_t* get_address_of_currentAttribute_10() { return &___currentAttribute_10; }
	inline void set_currentAttribute_10(int32_t value)
	{
		___currentAttribute_10 = value;
	}

	inline static int32_t get_offset_of_consumedAttribute_11() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___consumedAttribute_11)); }
	inline bool get_consumedAttribute_11() const { return ___consumedAttribute_11; }
	inline bool* get_address_of_consumedAttribute_11() { return &___consumedAttribute_11; }
	inline void set_consumedAttribute_11(bool value)
	{
		___consumedAttribute_11 = value;
	}

	inline static int32_t get_offset_of_elementStack_12() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___elementStack_12)); }
	inline Stack_t1043988394 * get_elementStack_12() const { return ___elementStack_12; }
	inline Stack_t1043988394 ** get_address_of_elementStack_12() { return &___elementStack_12; }
	inline void set_elementStack_12(Stack_t1043988394 * value)
	{
		___elementStack_12 = value;
		Il2CppCodeGenWriteBarrier((&___elementStack_12), value);
	}

	inline static int32_t get_offset_of_automataStack_13() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___automataStack_13)); }
	inline Stack_t1043988394 * get_automataStack_13() const { return ___automataStack_13; }
	inline Stack_t1043988394 ** get_address_of_automataStack_13() { return &___automataStack_13; }
	inline void set_automataStack_13(Stack_t1043988394 * value)
	{
		___automataStack_13 = value;
		Il2CppCodeGenWriteBarrier((&___automataStack_13), value);
	}

	inline static int32_t get_offset_of_popScope_14() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___popScope_14)); }
	inline bool get_popScope_14() const { return ___popScope_14; }
	inline bool* get_address_of_popScope_14() { return &___popScope_14; }
	inline void set_popScope_14(bool value)
	{
		___popScope_14 = value;
	}

	inline static int32_t get_offset_of_isStandalone_15() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___isStandalone_15)); }
	inline bool get_isStandalone_15() const { return ___isStandalone_15; }
	inline bool* get_address_of_isStandalone_15() { return &___isStandalone_15; }
	inline void set_isStandalone_15(bool value)
	{
		___isStandalone_15 = value;
	}

	inline static int32_t get_offset_of_currentAutomata_16() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___currentAutomata_16)); }
	inline DTDAutomata_t545990600 * get_currentAutomata_16() const { return ___currentAutomata_16; }
	inline DTDAutomata_t545990600 ** get_address_of_currentAutomata_16() { return &___currentAutomata_16; }
	inline void set_currentAutomata_16(DTDAutomata_t545990600 * value)
	{
		___currentAutomata_16 = value;
		Il2CppCodeGenWriteBarrier((&___currentAutomata_16), value);
	}

	inline static int32_t get_offset_of_previousAutomata_17() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___previousAutomata_17)); }
	inline DTDAutomata_t545990600 * get_previousAutomata_17() const { return ___previousAutomata_17; }
	inline DTDAutomata_t545990600 ** get_address_of_previousAutomata_17() { return &___previousAutomata_17; }
	inline void set_previousAutomata_17(DTDAutomata_t545990600 * value)
	{
		___previousAutomata_17 = value;
		Il2CppCodeGenWriteBarrier((&___previousAutomata_17), value);
	}

	inline static int32_t get_offset_of_idList_18() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___idList_18)); }
	inline ArrayList_t4252133567 * get_idList_18() const { return ___idList_18; }
	inline ArrayList_t4252133567 ** get_address_of_idList_18() { return &___idList_18; }
	inline void set_idList_18(ArrayList_t4252133567 * value)
	{
		___idList_18 = value;
		Il2CppCodeGenWriteBarrier((&___idList_18), value);
	}

	inline static int32_t get_offset_of_missingIDReferences_19() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___missingIDReferences_19)); }
	inline ArrayList_t4252133567 * get_missingIDReferences_19() const { return ___missingIDReferences_19; }
	inline ArrayList_t4252133567 ** get_address_of_missingIDReferences_19() { return &___missingIDReferences_19; }
	inline void set_missingIDReferences_19(ArrayList_t4252133567 * value)
	{
		___missingIDReferences_19 = value;
		Il2CppCodeGenWriteBarrier((&___missingIDReferences_19), value);
	}

	inline static int32_t get_offset_of_nsmgr_20() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___nsmgr_20)); }
	inline XmlNamespaceManager_t486731501 * get_nsmgr_20() const { return ___nsmgr_20; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_nsmgr_20() { return &___nsmgr_20; }
	inline void set_nsmgr_20(XmlNamespaceManager_t486731501 * value)
	{
		___nsmgr_20 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_20), value);
	}

	inline static int32_t get_offset_of_currentTextValue_21() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___currentTextValue_21)); }
	inline String_t* get_currentTextValue_21() const { return ___currentTextValue_21; }
	inline String_t** get_address_of_currentTextValue_21() { return &___currentTextValue_21; }
	inline void set_currentTextValue_21(String_t* value)
	{
		___currentTextValue_21 = value;
		Il2CppCodeGenWriteBarrier((&___currentTextValue_21), value);
	}

	inline static int32_t get_offset_of_constructingTextValue_22() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___constructingTextValue_22)); }
	inline String_t* get_constructingTextValue_22() const { return ___constructingTextValue_22; }
	inline String_t** get_address_of_constructingTextValue_22() { return &___constructingTextValue_22; }
	inline void set_constructingTextValue_22(String_t* value)
	{
		___constructingTextValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___constructingTextValue_22), value);
	}

	inline static int32_t get_offset_of_shouldResetCurrentTextValue_23() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___shouldResetCurrentTextValue_23)); }
	inline bool get_shouldResetCurrentTextValue_23() const { return ___shouldResetCurrentTextValue_23; }
	inline bool* get_address_of_shouldResetCurrentTextValue_23() { return &___shouldResetCurrentTextValue_23; }
	inline void set_shouldResetCurrentTextValue_23(bool value)
	{
		___shouldResetCurrentTextValue_23 = value;
	}

	inline static int32_t get_offset_of_isSignificantWhitespace_24() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___isSignificantWhitespace_24)); }
	inline bool get_isSignificantWhitespace_24() const { return ___isSignificantWhitespace_24; }
	inline bool* get_address_of_isSignificantWhitespace_24() { return &___isSignificantWhitespace_24; }
	inline void set_isSignificantWhitespace_24(bool value)
	{
		___isSignificantWhitespace_24 = value;
	}

	inline static int32_t get_offset_of_isWhitespace_25() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___isWhitespace_25)); }
	inline bool get_isWhitespace_25() const { return ___isWhitespace_25; }
	inline bool* get_address_of_isWhitespace_25() { return &___isWhitespace_25; }
	inline void set_isWhitespace_25(bool value)
	{
		___isWhitespace_25 = value;
	}

	inline static int32_t get_offset_of_isText_26() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___isText_26)); }
	inline bool get_isText_26() const { return ___isText_26; }
	inline bool* get_address_of_isText_26() { return &___isText_26; }
	inline void set_isText_26(bool value)
	{
		___isText_26 = value;
	}

	inline static int32_t get_offset_of_attributeValueEntityStack_27() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___attributeValueEntityStack_27)); }
	inline Stack_t1043988394 * get_attributeValueEntityStack_27() const { return ___attributeValueEntityStack_27; }
	inline Stack_t1043988394 ** get_address_of_attributeValueEntityStack_27() { return &___attributeValueEntityStack_27; }
	inline void set_attributeValueEntityStack_27(Stack_t1043988394 * value)
	{
		___attributeValueEntityStack_27 = value;
		Il2CppCodeGenWriteBarrier((&___attributeValueEntityStack_27), value);
	}

	inline static int32_t get_offset_of_valueBuilder_28() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___valueBuilder_28)); }
	inline StringBuilder_t1221177846 * get_valueBuilder_28() const { return ___valueBuilder_28; }
	inline StringBuilder_t1221177846 ** get_address_of_valueBuilder_28() { return &___valueBuilder_28; }
	inline void set_valueBuilder_28(StringBuilder_t1221177846 * value)
	{
		___valueBuilder_28 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuilder_28), value);
	}

	inline static int32_t get_offset_of_whitespaceChars_29() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348, ___whitespaceChars_29)); }
	inline CharU5BU5D_t1328083999* get_whitespaceChars_29() const { return ___whitespaceChars_29; }
	inline CharU5BU5D_t1328083999** get_address_of_whitespaceChars_29() { return &___whitespaceChars_29; }
	inline void set_whitespaceChars_29(CharU5BU5D_t1328083999* value)
	{
		___whitespaceChars_29 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceChars_29), value);
	}
};

struct DTDValidatingReader_t4120969348_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.DTDValidatingReader::<>f__switch$map43
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map43_30;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map43_30() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t4120969348_StaticFields, ___U3CU3Ef__switchU24map43_30)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map43_30() const { return ___U3CU3Ef__switchU24map43_30; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map43_30() { return &___U3CU3Ef__switchU24map43_30; }
	inline void set_U3CU3Ef__switchU24map43_30(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map43_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map43_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDVALIDATINGREADER_T4120969348_H
#ifndef NONBLOCKINGSTREAMREADER_T3963211903_H
#define NONBLOCKINGSTREAMREADER_T3963211903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NonBlockingStreamReader
struct  NonBlockingStreamReader_t3963211903  : public TextReader_t1561828458
{
public:
	// System.Byte[] System.Xml.NonBlockingStreamReader::input_buffer
	ByteU5BU5D_t3397334013* ___input_buffer_1;
	// System.Char[] System.Xml.NonBlockingStreamReader::decoded_buffer
	CharU5BU5D_t1328083999* ___decoded_buffer_2;
	// System.Int32 System.Xml.NonBlockingStreamReader::decoded_count
	int32_t ___decoded_count_3;
	// System.Int32 System.Xml.NonBlockingStreamReader::pos
	int32_t ___pos_4;
	// System.Int32 System.Xml.NonBlockingStreamReader::buffer_size
	int32_t ___buffer_size_5;
	// System.Text.Encoding System.Xml.NonBlockingStreamReader::encoding
	Encoding_t663144255 * ___encoding_6;
	// System.Text.Decoder System.Xml.NonBlockingStreamReader::decoder
	Decoder_t3792697818 * ___decoder_7;
	// System.IO.Stream System.Xml.NonBlockingStreamReader::base_stream
	Stream_t3255436806 * ___base_stream_8;
	// System.Boolean System.Xml.NonBlockingStreamReader::mayBlock
	bool ___mayBlock_9;
	// System.Text.StringBuilder System.Xml.NonBlockingStreamReader::line_builder
	StringBuilder_t1221177846 * ___line_builder_10;
	// System.Boolean System.Xml.NonBlockingStreamReader::foundCR
	bool ___foundCR_11;

public:
	inline static int32_t get_offset_of_input_buffer_1() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___input_buffer_1)); }
	inline ByteU5BU5D_t3397334013* get_input_buffer_1() const { return ___input_buffer_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_input_buffer_1() { return &___input_buffer_1; }
	inline void set_input_buffer_1(ByteU5BU5D_t3397334013* value)
	{
		___input_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_buffer_1), value);
	}

	inline static int32_t get_offset_of_decoded_buffer_2() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___decoded_buffer_2)); }
	inline CharU5BU5D_t1328083999* get_decoded_buffer_2() const { return ___decoded_buffer_2; }
	inline CharU5BU5D_t1328083999** get_address_of_decoded_buffer_2() { return &___decoded_buffer_2; }
	inline void set_decoded_buffer_2(CharU5BU5D_t1328083999* value)
	{
		___decoded_buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___decoded_buffer_2), value);
	}

	inline static int32_t get_offset_of_decoded_count_3() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___decoded_count_3)); }
	inline int32_t get_decoded_count_3() const { return ___decoded_count_3; }
	inline int32_t* get_address_of_decoded_count_3() { return &___decoded_count_3; }
	inline void set_decoded_count_3(int32_t value)
	{
		___decoded_count_3 = value;
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___pos_4)); }
	inline int32_t get_pos_4() const { return ___pos_4; }
	inline int32_t* get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(int32_t value)
	{
		___pos_4 = value;
	}

	inline static int32_t get_offset_of_buffer_size_5() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___buffer_size_5)); }
	inline int32_t get_buffer_size_5() const { return ___buffer_size_5; }
	inline int32_t* get_address_of_buffer_size_5() { return &___buffer_size_5; }
	inline void set_buffer_size_5(int32_t value)
	{
		___buffer_size_5 = value;
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___encoding_6)); }
	inline Encoding_t663144255 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t663144255 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t663144255 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_decoder_7() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___decoder_7)); }
	inline Decoder_t3792697818 * get_decoder_7() const { return ___decoder_7; }
	inline Decoder_t3792697818 ** get_address_of_decoder_7() { return &___decoder_7; }
	inline void set_decoder_7(Decoder_t3792697818 * value)
	{
		___decoder_7 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_7), value);
	}

	inline static int32_t get_offset_of_base_stream_8() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___base_stream_8)); }
	inline Stream_t3255436806 * get_base_stream_8() const { return ___base_stream_8; }
	inline Stream_t3255436806 ** get_address_of_base_stream_8() { return &___base_stream_8; }
	inline void set_base_stream_8(Stream_t3255436806 * value)
	{
		___base_stream_8 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_8), value);
	}

	inline static int32_t get_offset_of_mayBlock_9() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___mayBlock_9)); }
	inline bool get_mayBlock_9() const { return ___mayBlock_9; }
	inline bool* get_address_of_mayBlock_9() { return &___mayBlock_9; }
	inline void set_mayBlock_9(bool value)
	{
		___mayBlock_9 = value;
	}

	inline static int32_t get_offset_of_line_builder_10() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___line_builder_10)); }
	inline StringBuilder_t1221177846 * get_line_builder_10() const { return ___line_builder_10; }
	inline StringBuilder_t1221177846 ** get_address_of_line_builder_10() { return &___line_builder_10; }
	inline void set_line_builder_10(StringBuilder_t1221177846 * value)
	{
		___line_builder_10 = value;
		Il2CppCodeGenWriteBarrier((&___line_builder_10), value);
	}

	inline static int32_t get_offset_of_foundCR_11() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_t3963211903, ___foundCR_11)); }
	inline bool get_foundCR_11() const { return ___foundCR_11; }
	inline bool* get_address_of_foundCR_11() { return &___foundCR_11; }
	inline void set_foundCR_11(bool value)
	{
		___foundCR_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONBLOCKINGSTREAMREADER_T3963211903_H
#ifndef XMLINPUTSTREAM_T2650744719_H
#define XMLINPUTSTREAM_T2650744719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlInputStream
struct  XmlInputStream_t2650744719  : public Stream_t3255436806
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::enc
	Encoding_t663144255 * ___enc_2;
	// System.IO.Stream System.Xml.XmlInputStream::stream
	Stream_t3255436806 * ___stream_3;
	// System.Byte[] System.Xml.XmlInputStream::buffer
	ByteU5BU5D_t3397334013* ___buffer_4;
	// System.Int32 System.Xml.XmlInputStream::bufLength
	int32_t ___bufLength_5;
	// System.Int32 System.Xml.XmlInputStream::bufPos
	int32_t ___bufPos_6;

public:
	inline static int32_t get_offset_of_enc_2() { return static_cast<int32_t>(offsetof(XmlInputStream_t2650744719, ___enc_2)); }
	inline Encoding_t663144255 * get_enc_2() const { return ___enc_2; }
	inline Encoding_t663144255 ** get_address_of_enc_2() { return &___enc_2; }
	inline void set_enc_2(Encoding_t663144255 * value)
	{
		___enc_2 = value;
		Il2CppCodeGenWriteBarrier((&___enc_2), value);
	}

	inline static int32_t get_offset_of_stream_3() { return static_cast<int32_t>(offsetof(XmlInputStream_t2650744719, ___stream_3)); }
	inline Stream_t3255436806 * get_stream_3() const { return ___stream_3; }
	inline Stream_t3255436806 ** get_address_of_stream_3() { return &___stream_3; }
	inline void set_stream_3(Stream_t3255436806 * value)
	{
		___stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___stream_3), value);
	}

	inline static int32_t get_offset_of_buffer_4() { return static_cast<int32_t>(offsetof(XmlInputStream_t2650744719, ___buffer_4)); }
	inline ByteU5BU5D_t3397334013* get_buffer_4() const { return ___buffer_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_buffer_4() { return &___buffer_4; }
	inline void set_buffer_4(ByteU5BU5D_t3397334013* value)
	{
		___buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_4), value);
	}

	inline static int32_t get_offset_of_bufLength_5() { return static_cast<int32_t>(offsetof(XmlInputStream_t2650744719, ___bufLength_5)); }
	inline int32_t get_bufLength_5() const { return ___bufLength_5; }
	inline int32_t* get_address_of_bufLength_5() { return &___bufLength_5; }
	inline void set_bufLength_5(int32_t value)
	{
		___bufLength_5 = value;
	}

	inline static int32_t get_offset_of_bufPos_6() { return static_cast<int32_t>(offsetof(XmlInputStream_t2650744719, ___bufPos_6)); }
	inline int32_t get_bufPos_6() const { return ___bufPos_6; }
	inline int32_t* get_address_of_bufPos_6() { return &___bufPos_6; }
	inline void set_bufPos_6(int32_t value)
	{
		___bufPos_6 = value;
	}
};

struct XmlInputStream_t2650744719_StaticFields
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::StrictUTF8
	Encoding_t663144255 * ___StrictUTF8_1;
	// System.Xml.XmlException System.Xml.XmlInputStream::encodingException
	XmlException_t4188277960 * ___encodingException_7;

public:
	inline static int32_t get_offset_of_StrictUTF8_1() { return static_cast<int32_t>(offsetof(XmlInputStream_t2650744719_StaticFields, ___StrictUTF8_1)); }
	inline Encoding_t663144255 * get_StrictUTF8_1() const { return ___StrictUTF8_1; }
	inline Encoding_t663144255 ** get_address_of_StrictUTF8_1() { return &___StrictUTF8_1; }
	inline void set_StrictUTF8_1(Encoding_t663144255 * value)
	{
		___StrictUTF8_1 = value;
		Il2CppCodeGenWriteBarrier((&___StrictUTF8_1), value);
	}

	inline static int32_t get_offset_of_encodingException_7() { return static_cast<int32_t>(offsetof(XmlInputStream_t2650744719_StaticFields, ___encodingException_7)); }
	inline XmlException_t4188277960 * get_encodingException_7() const { return ___encodingException_7; }
	inline XmlException_t4188277960 ** get_address_of_encodingException_7() { return &___encodingException_7; }
	inline void set_encodingException_7(XmlException_t4188277960 * value)
	{
		___encodingException_7 = value;
		Il2CppCodeGenWriteBarrier((&___encodingException_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLINPUTSTREAM_T2650744719_H
#ifndef XMLLINKEDNODE_T1287616130_H
#define XMLLINKEDNODE_T1287616130_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t1287616130  : public XmlNode_t616554813
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::nextSibling
	XmlLinkedNode_t1287616130 * ___nextSibling_5;

public:
	inline static int32_t get_offset_of_nextSibling_5() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t1287616130, ___nextSibling_5)); }
	inline XmlLinkedNode_t1287616130 * get_nextSibling_5() const { return ___nextSibling_5; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_nextSibling_5() { return &___nextSibling_5; }
	inline void set_nextSibling_5(XmlLinkedNode_t1287616130 * value)
	{
		___nextSibling_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextSibling_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLINKEDNODE_T1287616130_H
#ifndef DTDENTITYBASE_T2353758560_H
#define DTDENTITYBASE_T2353758560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityBase
struct  DTDEntityBase_t2353758560  : public DTDNode_t1758286970
{
public:
	// System.String Mono.Xml.DTDEntityBase::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDEntityBase::publicId
	String_t* ___publicId_6;
	// System.String Mono.Xml.DTDEntityBase::systemId
	String_t* ___systemId_7;
	// System.String Mono.Xml.DTDEntityBase::literalValue
	String_t* ___literalValue_8;
	// System.String Mono.Xml.DTDEntityBase::replacementText
	String_t* ___replacementText_9;
	// System.String Mono.Xml.DTDEntityBase::uriString
	String_t* ___uriString_10;
	// System.Uri Mono.Xml.DTDEntityBase::absUri
	Uri_t19570940 * ___absUri_11;
	// System.Boolean Mono.Xml.DTDEntityBase::isInvalid
	bool ___isInvalid_12;
	// System.Boolean Mono.Xml.DTDEntityBase::loadFailed
	bool ___loadFailed_13;
	// System.Xml.XmlResolver Mono.Xml.DTDEntityBase::resolver
	XmlResolver_t2024571559 * ___resolver_14;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_systemId_7() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___systemId_7)); }
	inline String_t* get_systemId_7() const { return ___systemId_7; }
	inline String_t** get_address_of_systemId_7() { return &___systemId_7; }
	inline void set_systemId_7(String_t* value)
	{
		___systemId_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_7), value);
	}

	inline static int32_t get_offset_of_literalValue_8() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___literalValue_8)); }
	inline String_t* get_literalValue_8() const { return ___literalValue_8; }
	inline String_t** get_address_of_literalValue_8() { return &___literalValue_8; }
	inline void set_literalValue_8(String_t* value)
	{
		___literalValue_8 = value;
		Il2CppCodeGenWriteBarrier((&___literalValue_8), value);
	}

	inline static int32_t get_offset_of_replacementText_9() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___replacementText_9)); }
	inline String_t* get_replacementText_9() const { return ___replacementText_9; }
	inline String_t** get_address_of_replacementText_9() { return &___replacementText_9; }
	inline void set_replacementText_9(String_t* value)
	{
		___replacementText_9 = value;
		Il2CppCodeGenWriteBarrier((&___replacementText_9), value);
	}

	inline static int32_t get_offset_of_uriString_10() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___uriString_10)); }
	inline String_t* get_uriString_10() const { return ___uriString_10; }
	inline String_t** get_address_of_uriString_10() { return &___uriString_10; }
	inline void set_uriString_10(String_t* value)
	{
		___uriString_10 = value;
		Il2CppCodeGenWriteBarrier((&___uriString_10), value);
	}

	inline static int32_t get_offset_of_absUri_11() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___absUri_11)); }
	inline Uri_t19570940 * get_absUri_11() const { return ___absUri_11; }
	inline Uri_t19570940 ** get_address_of_absUri_11() { return &___absUri_11; }
	inline void set_absUri_11(Uri_t19570940 * value)
	{
		___absUri_11 = value;
		Il2CppCodeGenWriteBarrier((&___absUri_11), value);
	}

	inline static int32_t get_offset_of_isInvalid_12() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___isInvalid_12)); }
	inline bool get_isInvalid_12() const { return ___isInvalid_12; }
	inline bool* get_address_of_isInvalid_12() { return &___isInvalid_12; }
	inline void set_isInvalid_12(bool value)
	{
		___isInvalid_12 = value;
	}

	inline static int32_t get_offset_of_loadFailed_13() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___loadFailed_13)); }
	inline bool get_loadFailed_13() const { return ___loadFailed_13; }
	inline bool* get_address_of_loadFailed_13() { return &___loadFailed_13; }
	inline void set_loadFailed_13(bool value)
	{
		___loadFailed_13 = value;
	}

	inline static int32_t get_offset_of_resolver_14() { return static_cast<int32_t>(offsetof(DTDEntityBase_t2353758560, ___resolver_14)); }
	inline XmlResolver_t2024571559 * get_resolver_14() const { return ___resolver_14; }
	inline XmlResolver_t2024571559 ** get_address_of_resolver_14() { return &___resolver_14; }
	inline void set_resolver_14(XmlResolver_t2024571559 * value)
	{
		___resolver_14 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYBASE_T2353758560_H
#ifndef XMLENTITY_T4027255380_H
#define XMLENTITY_T4027255380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntity
struct  XmlEntity_t4027255380  : public XmlNode_t616554813
{
public:
	// System.String System.Xml.XmlEntity::name
	String_t* ___name_5;
	// System.String System.Xml.XmlEntity::NDATA
	String_t* ___NDATA_6;
	// System.String System.Xml.XmlEntity::publicId
	String_t* ___publicId_7;
	// System.String System.Xml.XmlEntity::systemId
	String_t* ___systemId_8;
	// System.String System.Xml.XmlEntity::baseUri
	String_t* ___baseUri_9;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntity::lastLinkedChild
	XmlLinkedNode_t1287616130 * ___lastLinkedChild_10;
	// System.Boolean System.Xml.XmlEntity::contentAlreadySet
	bool ___contentAlreadySet_11;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_NDATA_6() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___NDATA_6)); }
	inline String_t* get_NDATA_6() const { return ___NDATA_6; }
	inline String_t** get_address_of_NDATA_6() { return &___NDATA_6; }
	inline void set_NDATA_6(String_t* value)
	{
		___NDATA_6 = value;
		Il2CppCodeGenWriteBarrier((&___NDATA_6), value);
	}

	inline static int32_t get_offset_of_publicId_7() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___publicId_7)); }
	inline String_t* get_publicId_7() const { return ___publicId_7; }
	inline String_t** get_address_of_publicId_7() { return &___publicId_7; }
	inline void set_publicId_7(String_t* value)
	{
		___publicId_7 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_7), value);
	}

	inline static int32_t get_offset_of_systemId_8() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___systemId_8)); }
	inline String_t* get_systemId_8() const { return ___systemId_8; }
	inline String_t** get_address_of_systemId_8() { return &___systemId_8; }
	inline void set_systemId_8(String_t* value)
	{
		___systemId_8 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_8), value);
	}

	inline static int32_t get_offset_of_baseUri_9() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___baseUri_9)); }
	inline String_t* get_baseUri_9() const { return ___baseUri_9; }
	inline String_t** get_address_of_baseUri_9() { return &___baseUri_9; }
	inline void set_baseUri_9(String_t* value)
	{
		___baseUri_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_9), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_10() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___lastLinkedChild_10)); }
	inline XmlLinkedNode_t1287616130 * get_lastLinkedChild_10() const { return ___lastLinkedChild_10; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastLinkedChild_10() { return &___lastLinkedChild_10; }
	inline void set_lastLinkedChild_10(XmlLinkedNode_t1287616130 * value)
	{
		___lastLinkedChild_10 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_10), value);
	}

	inline static int32_t get_offset_of_contentAlreadySet_11() { return static_cast<int32_t>(offsetof(XmlEntity_t4027255380, ___contentAlreadySet_11)); }
	inline bool get_contentAlreadySet_11() const { return ___contentAlreadySet_11; }
	inline bool* get_address_of_contentAlreadySet_11() { return &___contentAlreadySet_11; }
	inline void set_contentAlreadySet_11(bool value)
	{
		___contentAlreadySet_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITY_T4027255380_H
#ifndef DTDNOTATIONDECLARATION_T1758408116_H
#define DTDNOTATIONDECLARATION_T1758408116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclaration
struct  DTDNotationDeclaration_t1758408116  : public DTDNode_t1758286970
{
public:
	// System.String Mono.Xml.DTDNotationDeclaration::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDNotationDeclaration::localName
	String_t* ___localName_6;
	// System.String Mono.Xml.DTDNotationDeclaration::prefix
	String_t* ___prefix_7;
	// System.String Mono.Xml.DTDNotationDeclaration::publicId
	String_t* ___publicId_8;
	// System.String Mono.Xml.DTDNotationDeclaration::systemId
	String_t* ___systemId_9;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1758408116, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_localName_6() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1758408116, ___localName_6)); }
	inline String_t* get_localName_6() const { return ___localName_6; }
	inline String_t** get_address_of_localName_6() { return &___localName_6; }
	inline void set_localName_6(String_t* value)
	{
		___localName_6 = value;
		Il2CppCodeGenWriteBarrier((&___localName_6), value);
	}

	inline static int32_t get_offset_of_prefix_7() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1758408116, ___prefix_7)); }
	inline String_t* get_prefix_7() const { return ___prefix_7; }
	inline String_t** get_address_of_prefix_7() { return &___prefix_7; }
	inline void set_prefix_7(String_t* value)
	{
		___prefix_7 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_7), value);
	}

	inline static int32_t get_offset_of_publicId_8() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1758408116, ___publicId_8)); }
	inline String_t* get_publicId_8() const { return ___publicId_8; }
	inline String_t** get_address_of_publicId_8() { return &___publicId_8; }
	inline void set_publicId_8(String_t* value)
	{
		___publicId_8 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_8), value);
	}

	inline static int32_t get_offset_of_systemId_9() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t1758408116, ___systemId_9)); }
	inline String_t* get_systemId_9() const { return ___systemId_9; }
	inline String_t** get_address_of_systemId_9() { return &___systemId_9; }
	inline void set_systemId_9(String_t* value)
	{
		___systemId_9 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATION_T1758408116_H
#ifndef XMLROOTATTRIBUTE_T3527426713_H
#define XMLROOTATTRIBUTE_T3527426713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlRootAttribute
struct  XmlRootAttribute_t3527426713  : public Attribute_t542643598
{
public:
	// System.String System.Xml.Serialization.XmlRootAttribute::elementName
	String_t* ___elementName_0;
	// System.Boolean System.Xml.Serialization.XmlRootAttribute::isNullable
	bool ___isNullable_1;
	// System.String System.Xml.Serialization.XmlRootAttribute::ns
	String_t* ___ns_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t3527426713, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_isNullable_1() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t3527426713, ___isNullable_1)); }
	inline bool get_isNullable_1() const { return ___isNullable_1; }
	inline bool* get_address_of_isNullable_1() { return &___isNullable_1; }
	inline void set_isNullable_1(bool value)
	{
		___isNullable_1 = value;
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t3527426713, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLROOTATTRIBUTE_T3527426713_H
#ifndef XMLNAMESPACEDECLARATIONSATTRIBUTE_T2069522403_H
#define XMLNAMESPACEDECLARATIONSATTRIBUTE_T2069522403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlNamespaceDeclarationsAttribute
struct  XmlNamespaceDeclarationsAttribute_t2069522403  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEDECLARATIONSATTRIBUTE_T2069522403_H
#ifndef XMLSCHEMAPROVIDERATTRIBUTE_T2486667559_H
#define XMLSCHEMAPROVIDERATTRIBUTE_T2486667559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSchemaProviderAttribute
struct  XmlSchemaProviderAttribute_t2486667559  : public Attribute_t542643598
{
public:
	// System.String System.Xml.Serialization.XmlSchemaProviderAttribute::_methodName
	String_t* ____methodName_0;
	// System.Boolean System.Xml.Serialization.XmlSchemaProviderAttribute::_isAny
	bool ____isAny_1;

public:
	inline static int32_t get_offset_of__methodName_0() { return static_cast<int32_t>(offsetof(XmlSchemaProviderAttribute_t2486667559, ____methodName_0)); }
	inline String_t* get__methodName_0() const { return ____methodName_0; }
	inline String_t** get_address_of__methodName_0() { return &____methodName_0; }
	inline void set__methodName_0(String_t* value)
	{
		____methodName_0 = value;
		Il2CppCodeGenWriteBarrier((&____methodName_0), value);
	}

	inline static int32_t get_offset_of__isAny_1() { return static_cast<int32_t>(offsetof(XmlSchemaProviderAttribute_t2486667559, ____isAny_1)); }
	inline bool get__isAny_1() const { return ____isAny_1; }
	inline bool* get_address_of__isAny_1() { return &____isAny_1; }
	inline void set__isAny_1(bool value)
	{
		____isAny_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPROVIDERATTRIBUTE_T2486667559_H
#ifndef DTDELEMENTAUTOMATA_T2864881036_H
#define DTDELEMENTAUTOMATA_T2864881036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementAutomata
struct  DTDElementAutomata_t2864881036  : public DTDAutomata_t545990600
{
public:
	// System.String Mono.Xml.DTDElementAutomata::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(DTDElementAutomata_t2864881036, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTAUTOMATA_T2864881036_H
#ifndef XMLTEXTATTRIBUTE_T3321178844_H
#define XMLTEXTATTRIBUTE_T3321178844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTextAttribute
struct  XmlTextAttribute_t3321178844  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTATTRIBUTE_T3321178844_H
#ifndef XMLENUMATTRIBUTE_T919400678_H
#define XMLENUMATTRIBUTE_T919400678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlEnumAttribute
struct  XmlEnumAttribute_t919400678  : public Attribute_t542643598
{
public:
	// System.String System.Xml.Serialization.XmlEnumAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XmlEnumAttribute_t919400678, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENUMATTRIBUTE_T919400678_H
#ifndef XMLANYELEMENTATTRIBUTE_T2502375235_H
#define XMLANYELEMENTATTRIBUTE_T2502375235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyElementAttribute
struct  XmlAnyElementAttribute_t2502375235  : public Attribute_t542643598
{
public:
	// System.Int32 System.Xml.Serialization.XmlAnyElementAttribute::order
	int32_t ___order_0;

public:
	inline static int32_t get_offset_of_order_0() { return static_cast<int32_t>(offsetof(XmlAnyElementAttribute_t2502375235, ___order_0)); }
	inline int32_t get_order_0() const { return ___order_0; }
	inline int32_t* get_address_of_order_0() { return &___order_0; }
	inline void set_order_0(int32_t value)
	{
		___order_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYELEMENTATTRIBUTE_T2502375235_H
#ifndef XMLANYATTRIBUTEATTRIBUTE_T713947513_H
#define XMLANYATTRIBUTEATTRIBUTE_T713947513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyAttributeAttribute
struct  XmlAnyAttributeAttribute_t713947513  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYATTRIBUTEATTRIBUTE_T713947513_H
#ifndef XMLATTRIBUTEATTRIBUTE_T850813783_H
#define XMLATTRIBUTEATTRIBUTE_T850813783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributeAttribute
struct  XmlAttributeAttribute_t850813783  : public Attribute_t542643598
{
public:
	// System.String System.Xml.Serialization.XmlAttributeAttribute::attributeName
	String_t* ___attributeName_0;
	// System.String System.Xml.Serialization.XmlAttributeAttribute::dataType
	String_t* ___dataType_1;

public:
	inline static int32_t get_offset_of_attributeName_0() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t850813783, ___attributeName_0)); }
	inline String_t* get_attributeName_0() const { return ___attributeName_0; }
	inline String_t** get_address_of_attributeName_0() { return &___attributeName_0; }
	inline void set_attributeName_0(String_t* value)
	{
		___attributeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeName_0), value);
	}

	inline static int32_t get_offset_of_dataType_1() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_t850813783, ___dataType_1)); }
	inline String_t* get_dataType_1() const { return ___dataType_1; }
	inline String_t** get_address_of_dataType_1() { return &___dataType_1; }
	inline void set_dataType_1(String_t* value)
	{
		___dataType_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTEATTRIBUTE_T850813783_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef XMLELEMENTATTRIBUTE_T2182839281_H
#define XMLELEMENTATTRIBUTE_T2182839281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlElementAttribute
struct  XmlElementAttribute_t2182839281  : public Attribute_t542643598
{
public:
	// System.String System.Xml.Serialization.XmlElementAttribute::elementName
	String_t* ___elementName_0;
	// System.Type System.Xml.Serialization.XmlElementAttribute::type
	Type_t * ___type_1;
	// System.Int32 System.Xml.Serialization.XmlElementAttribute::order
	int32_t ___order_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t2182839281, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t2182839281, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t2182839281, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTATTRIBUTE_T2182839281_H
#ifndef DTDINVALIDAUTOMATA_T247674167_H
#define DTDINVALIDAUTOMATA_T247674167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDInvalidAutomata
struct  DTDInvalidAutomata_t247674167  : public DTDAutomata_t545990600
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDINVALIDAUTOMATA_T247674167_H
#ifndef DTDANYAUTOMATA_T146446906_H
#define DTDANYAUTOMATA_T146446906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAnyAutomata
struct  DTDAnyAutomata_t146446906  : public DTDAutomata_t545990600
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDANYAUTOMATA_T146446906_H
#ifndef DICTIONARYBASE_T1005937181_H
#define DICTIONARYBASE_T1005937181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase
struct  DictionaryBase_t1005937181  : public List_1_t799532586
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYBASE_T1005937181_H
#ifndef XMLIGNOREATTRIBUTE_T2333915871_H
#define XMLIGNOREATTRIBUTE_T2333915871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlIgnoreAttribute
struct  XmlIgnoreAttribute_t2333915871  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIGNOREATTRIBUTE_T2333915871_H
#ifndef KEYVALUEPAIR_2_T1430411454_H
#define KEYVALUEPAIR_2_T1430411454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>
struct  KeyValuePair_2_t1430411454 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	DTDNode_t1758286970 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1430411454, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1430411454, ___value_1)); }
	inline DTDNode_t1758286970 * get_value_1() const { return ___value_1; }
	inline DTDNode_t1758286970 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(DTDNode_t1758286970 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1430411454_H
#ifndef DTDONEORMOREAUTOMATA_T1559764132_H
#define DTDONEORMOREAUTOMATA_T1559764132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDOneOrMoreAutomata
struct  DTDOneOrMoreAutomata_t1559764132  : public DTDAutomata_t545990600
{
public:
	// Mono.Xml.DTDAutomata Mono.Xml.DTDOneOrMoreAutomata::children
	DTDAutomata_t545990600 * ___children_1;

public:
	inline static int32_t get_offset_of_children_1() { return static_cast<int32_t>(offsetof(DTDOneOrMoreAutomata_t1559764132, ___children_1)); }
	inline DTDAutomata_t545990600 * get_children_1() const { return ___children_1; }
	inline DTDAutomata_t545990600 ** get_address_of_children_1() { return &___children_1; }
	inline void set_children_1(DTDAutomata_t545990600 * value)
	{
		___children_1 = value;
		Il2CppCodeGenWriteBarrier((&___children_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDONEORMOREAUTOMATA_T1559764132_H
#ifndef DTDSEQUENCEAUTOMATA_T1228770437_H
#define DTDSEQUENCEAUTOMATA_T1228770437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDSequenceAutomata
struct  DTDSequenceAutomata_t1228770437  : public DTDAutomata_t545990600
{
public:
	// Mono.Xml.DTDAutomata Mono.Xml.DTDSequenceAutomata::left
	DTDAutomata_t545990600 * ___left_1;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDSequenceAutomata::right
	DTDAutomata_t545990600 * ___right_2;
	// System.Boolean Mono.Xml.DTDSequenceAutomata::hasComputedEmptiable
	bool ___hasComputedEmptiable_3;
	// System.Boolean Mono.Xml.DTDSequenceAutomata::cachedEmptiable
	bool ___cachedEmptiable_4;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_t1228770437, ___left_1)); }
	inline DTDAutomata_t545990600 * get_left_1() const { return ___left_1; }
	inline DTDAutomata_t545990600 ** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(DTDAutomata_t545990600 * value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier((&___left_1), value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_t1228770437, ___right_2)); }
	inline DTDAutomata_t545990600 * get_right_2() const { return ___right_2; }
	inline DTDAutomata_t545990600 ** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(DTDAutomata_t545990600 * value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier((&___right_2), value);
	}

	inline static int32_t get_offset_of_hasComputedEmptiable_3() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_t1228770437, ___hasComputedEmptiable_3)); }
	inline bool get_hasComputedEmptiable_3() const { return ___hasComputedEmptiable_3; }
	inline bool* get_address_of_hasComputedEmptiable_3() { return &___hasComputedEmptiable_3; }
	inline void set_hasComputedEmptiable_3(bool value)
	{
		___hasComputedEmptiable_3 = value;
	}

	inline static int32_t get_offset_of_cachedEmptiable_4() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_t1228770437, ___cachedEmptiable_4)); }
	inline bool get_cachedEmptiable_4() const { return ___cachedEmptiable_4; }
	inline bool* get_address_of_cachedEmptiable_4() { return &___cachedEmptiable_4; }
	inline void set_cachedEmptiable_4(bool value)
	{
		___cachedEmptiable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDSEQUENCEAUTOMATA_T1228770437_H
#ifndef DTDCHOICEAUTOMATA_T2810241733_H
#define DTDCHOICEAUTOMATA_T2810241733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDChoiceAutomata
struct  DTDChoiceAutomata_t2810241733  : public DTDAutomata_t545990600
{
public:
	// Mono.Xml.DTDAutomata Mono.Xml.DTDChoiceAutomata::left
	DTDAutomata_t545990600 * ___left_1;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDChoiceAutomata::right
	DTDAutomata_t545990600 * ___right_2;
	// System.Boolean Mono.Xml.DTDChoiceAutomata::hasComputedEmptiable
	bool ___hasComputedEmptiable_3;
	// System.Boolean Mono.Xml.DTDChoiceAutomata::cachedEmptiable
	bool ___cachedEmptiable_4;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t2810241733, ___left_1)); }
	inline DTDAutomata_t545990600 * get_left_1() const { return ___left_1; }
	inline DTDAutomata_t545990600 ** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(DTDAutomata_t545990600 * value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier((&___left_1), value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t2810241733, ___right_2)); }
	inline DTDAutomata_t545990600 * get_right_2() const { return ___right_2; }
	inline DTDAutomata_t545990600 ** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(DTDAutomata_t545990600 * value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier((&___right_2), value);
	}

	inline static int32_t get_offset_of_hasComputedEmptiable_3() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t2810241733, ___hasComputedEmptiable_3)); }
	inline bool get_hasComputedEmptiable_3() const { return ___hasComputedEmptiable_3; }
	inline bool* get_address_of_hasComputedEmptiable_3() { return &___hasComputedEmptiable_3; }
	inline void set_hasComputedEmptiable_3(bool value)
	{
		___hasComputedEmptiable_3 = value;
	}

	inline static int32_t get_offset_of_cachedEmptiable_4() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t2810241733, ___cachedEmptiable_4)); }
	inline bool get_cachedEmptiable_4() const { return ___cachedEmptiable_4; }
	inline bool* get_address_of_cachedEmptiable_4() { return &___cachedEmptiable_4; }
	inline void set_cachedEmptiable_4(bool value)
	{
		___cachedEmptiable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCHOICEAUTOMATA_T2810241733_H
#ifndef DTDEMPTYAUTOMATA_T411530619_H
#define DTDEMPTYAUTOMATA_T411530619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEmptyAutomata
struct  DTDEmptyAutomata_t411530619  : public DTDAutomata_t545990600
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDEMPTYAUTOMATA_T411530619_H
#ifndef XMLDECLARATION_T1545359137_H
#define XMLDECLARATION_T1545359137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDeclaration
struct  XmlDeclaration_t1545359137  : public XmlLinkedNode_t1287616130
{
public:
	// System.String System.Xml.XmlDeclaration::encoding
	String_t* ___encoding_6;
	// System.String System.Xml.XmlDeclaration::standalone
	String_t* ___standalone_7;
	// System.String System.Xml.XmlDeclaration::version
	String_t* ___version_8;

public:
	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1545359137, ___encoding_6)); }
	inline String_t* get_encoding_6() const { return ___encoding_6; }
	inline String_t** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(String_t* value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_standalone_7() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1545359137, ___standalone_7)); }
	inline String_t* get_standalone_7() const { return ___standalone_7; }
	inline String_t** get_address_of_standalone_7() { return &___standalone_7; }
	inline void set_standalone_7(String_t* value)
	{
		___standalone_7 = value;
		Il2CppCodeGenWriteBarrier((&___standalone_7), value);
	}

	inline static int32_t get_offset_of_version_8() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1545359137, ___version_8)); }
	inline String_t* get_version_8() const { return ___version_8; }
	inline String_t** get_address_of_version_8() { return &___version_8; }
	inline void set_version_8(String_t* value)
	{
		___version_8 = value;
		Il2CppCodeGenWriteBarrier((&___version_8), value);
	}
};

struct XmlDeclaration_t1545359137_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlDeclaration::<>f__switch$map4A
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map4A_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4A_9() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1545359137_StaticFields, ___U3CU3Ef__switchU24map4A_9)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map4A_9() const { return ___U3CU3Ef__switchU24map4A_9; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map4A_9() { return &___U3CU3Ef__switchU24map4A_9; }
	inline void set_U3CU3Ef__switchU24map4A_9(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map4A_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4A_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATION_T1545359137_H
#ifndef DATETIMESTYLES_T370343085_H
#define DATETIMESTYLES_T370343085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.DateTimeStyles
struct  DateTimeStyles_t370343085 
{
public:
	// System.Int32 System.Globalization.DateTimeStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeStyles_t370343085, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMESTYLES_T370343085_H
#ifndef XMLSCHEMAOBJECT_T2050913741_H
#define XMLSCHEMAOBJECT_T2050913741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObject
struct  XmlSchemaObject_t2050913741  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaObject::lineNumber
	int32_t ___lineNumber_0;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::linePosition
	int32_t ___linePosition_1;
	// System.String System.Xml.Schema.XmlSchemaObject::sourceUri
	String_t* ___sourceUri_2;
	// System.Xml.Serialization.XmlSerializerNamespaces System.Xml.Schema.XmlSchemaObject::namespaces
	XmlSerializerNamespaces_t3063656491 * ___namespaces_3;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaObject::unhandledAttributeList
	ArrayList_t4252133567 * ___unhandledAttributeList_4;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isCompiled
	bool ___isCompiled_5;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::errorCount
	int32_t ___errorCount_6;
	// System.Guid System.Xml.Schema.XmlSchemaObject::CompilationId
	Guid_t  ___CompilationId_7;
	// System.Guid System.Xml.Schema.XmlSchemaObject::ValidationId
	Guid_t  ___ValidationId_8;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isRedefineChild
	bool ___isRedefineChild_9;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isRedefinedComponent
	bool ___isRedefinedComponent_10;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::redefinedObject
	XmlSchemaObject_t2050913741 * ___redefinedObject_11;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::parent
	XmlSchemaObject_t2050913741 * ___parent_12;

public:
	inline static int32_t get_offset_of_lineNumber_0() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___lineNumber_0)); }
	inline int32_t get_lineNumber_0() const { return ___lineNumber_0; }
	inline int32_t* get_address_of_lineNumber_0() { return &___lineNumber_0; }
	inline void set_lineNumber_0(int32_t value)
	{
		___lineNumber_0 = value;
	}

	inline static int32_t get_offset_of_linePosition_1() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___linePosition_1)); }
	inline int32_t get_linePosition_1() const { return ___linePosition_1; }
	inline int32_t* get_address_of_linePosition_1() { return &___linePosition_1; }
	inline void set_linePosition_1(int32_t value)
	{
		___linePosition_1 = value;
	}

	inline static int32_t get_offset_of_sourceUri_2() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___sourceUri_2)); }
	inline String_t* get_sourceUri_2() const { return ___sourceUri_2; }
	inline String_t** get_address_of_sourceUri_2() { return &___sourceUri_2; }
	inline void set_sourceUri_2(String_t* value)
	{
		___sourceUri_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_2), value);
	}

	inline static int32_t get_offset_of_namespaces_3() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___namespaces_3)); }
	inline XmlSerializerNamespaces_t3063656491 * get_namespaces_3() const { return ___namespaces_3; }
	inline XmlSerializerNamespaces_t3063656491 ** get_address_of_namespaces_3() { return &___namespaces_3; }
	inline void set_namespaces_3(XmlSerializerNamespaces_t3063656491 * value)
	{
		___namespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_3), value);
	}

	inline static int32_t get_offset_of_unhandledAttributeList_4() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___unhandledAttributeList_4)); }
	inline ArrayList_t4252133567 * get_unhandledAttributeList_4() const { return ___unhandledAttributeList_4; }
	inline ArrayList_t4252133567 ** get_address_of_unhandledAttributeList_4() { return &___unhandledAttributeList_4; }
	inline void set_unhandledAttributeList_4(ArrayList_t4252133567 * value)
	{
		___unhandledAttributeList_4 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributeList_4), value);
	}

	inline static int32_t get_offset_of_isCompiled_5() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___isCompiled_5)); }
	inline bool get_isCompiled_5() const { return ___isCompiled_5; }
	inline bool* get_address_of_isCompiled_5() { return &___isCompiled_5; }
	inline void set_isCompiled_5(bool value)
	{
		___isCompiled_5 = value;
	}

	inline static int32_t get_offset_of_errorCount_6() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___errorCount_6)); }
	inline int32_t get_errorCount_6() const { return ___errorCount_6; }
	inline int32_t* get_address_of_errorCount_6() { return &___errorCount_6; }
	inline void set_errorCount_6(int32_t value)
	{
		___errorCount_6 = value;
	}

	inline static int32_t get_offset_of_CompilationId_7() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___CompilationId_7)); }
	inline Guid_t  get_CompilationId_7() const { return ___CompilationId_7; }
	inline Guid_t * get_address_of_CompilationId_7() { return &___CompilationId_7; }
	inline void set_CompilationId_7(Guid_t  value)
	{
		___CompilationId_7 = value;
	}

	inline static int32_t get_offset_of_ValidationId_8() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___ValidationId_8)); }
	inline Guid_t  get_ValidationId_8() const { return ___ValidationId_8; }
	inline Guid_t * get_address_of_ValidationId_8() { return &___ValidationId_8; }
	inline void set_ValidationId_8(Guid_t  value)
	{
		___ValidationId_8 = value;
	}

	inline static int32_t get_offset_of_isRedefineChild_9() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___isRedefineChild_9)); }
	inline bool get_isRedefineChild_9() const { return ___isRedefineChild_9; }
	inline bool* get_address_of_isRedefineChild_9() { return &___isRedefineChild_9; }
	inline void set_isRedefineChild_9(bool value)
	{
		___isRedefineChild_9 = value;
	}

	inline static int32_t get_offset_of_isRedefinedComponent_10() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___isRedefinedComponent_10)); }
	inline bool get_isRedefinedComponent_10() const { return ___isRedefinedComponent_10; }
	inline bool* get_address_of_isRedefinedComponent_10() { return &___isRedefinedComponent_10; }
	inline void set_isRedefinedComponent_10(bool value)
	{
		___isRedefinedComponent_10 = value;
	}

	inline static int32_t get_offset_of_redefinedObject_11() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___redefinedObject_11)); }
	inline XmlSchemaObject_t2050913741 * get_redefinedObject_11() const { return ___redefinedObject_11; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_redefinedObject_11() { return &___redefinedObject_11; }
	inline void set_redefinedObject_11(XmlSchemaObject_t2050913741 * value)
	{
		___redefinedObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___redefinedObject_11), value);
	}

	inline static int32_t get_offset_of_parent_12() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___parent_12)); }
	inline XmlSchemaObject_t2050913741 * get_parent_12() const { return ___parent_12; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_parent_12() { return &___parent_12; }
	inline void set_parent_12(XmlSchemaObject_t2050913741 * value)
	{
		___parent_12 = value;
		Il2CppCodeGenWriteBarrier((&___parent_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECT_T2050913741_H
#ifndef XMLSCHEMAUSE_T3553149267_H
#define XMLSCHEMAUSE_T3553149267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUse
struct  XmlSchemaUse_t3553149267 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaUse::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaUse_t3553149267, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUSE_T3553149267_H
#ifndef XMLDATETIMESERIALIZATIONMODE_T137774893_H
#define XMLDATETIMESERIALIZATIONMODE_T137774893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDateTimeSerializationMode
struct  XmlDateTimeSerializationMode_t137774893 
{
public:
	// System.Int32 System.Xml.XmlDateTimeSerializationMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlDateTimeSerializationMode_t137774893, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDATETIMESERIALIZATIONMODE_T137774893_H
#ifndef FACET_T3019654938_H
#define FACET_T3019654938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet/Facet
struct  Facet_t3019654938 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaFacet/Facet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Facet_t3019654938, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACET_T3019654938_H
#ifndef XMLCHARACTERDATA_T575748506_H
#define XMLCHARACTERDATA_T575748506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t575748506  : public XmlLinkedNode_t1287616130
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_6;

public:
	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(XmlCharacterData_t575748506, ___data_6)); }
	inline String_t* get_data_6() const { return ___data_6; }
	inline String_t** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(String_t* value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier((&___data_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARACTERDATA_T575748506_H
#ifndef XMLSTREAMREADER_T2725532304_H
#define XMLSTREAMREADER_T2725532304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStreamReader
struct  XmlStreamReader_t2725532304  : public NonBlockingStreamReader_t3963211903
{
public:
	// System.Xml.XmlInputStream System.Xml.XmlStreamReader::input
	XmlInputStream_t2650744719 * ___input_12;

public:
	inline static int32_t get_offset_of_input_12() { return static_cast<int32_t>(offsetof(XmlStreamReader_t2725532304, ___input_12)); }
	inline XmlInputStream_t2650744719 * get_input_12() const { return ___input_12; }
	inline XmlInputStream_t2650744719 ** get_address_of_input_12() { return &___input_12; }
	inline void set_input_12(XmlInputStream_t2650744719 * value)
	{
		___input_12 = value;
		Il2CppCodeGenWriteBarrier((&___input_12), value);
	}
};

struct XmlStreamReader_t2725532304_StaticFields
{
public:
	// System.Xml.XmlException System.Xml.XmlStreamReader::invalidDataException
	XmlException_t4188277960 * ___invalidDataException_13;

public:
	inline static int32_t get_offset_of_invalidDataException_13() { return static_cast<int32_t>(offsetof(XmlStreamReader_t2725532304_StaticFields, ___invalidDataException_13)); }
	inline XmlException_t4188277960 * get_invalidDataException_13() const { return ___invalidDataException_13; }
	inline XmlException_t4188277960 ** get_address_of_invalidDataException_13() { return &___invalidDataException_13; }
	inline void set_invalidDataException_13(XmlException_t4188277960 * value)
	{
		___invalidDataException_13 = value;
		Il2CppCodeGenWriteBarrier((&___invalidDataException_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTREAMREADER_T2725532304_H
#ifndef XMLSCHEMAEXCEPTION_T4082200141_H
#define XMLSCHEMAEXCEPTION_T4082200141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaException
struct  XmlSchemaException_t4082200141  : public SystemException_t3877406272
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaException::hasLineInfo
	bool ___hasLineInfo_11;
	// System.Int32 System.Xml.Schema.XmlSchemaException::lineNumber
	int32_t ___lineNumber_12;
	// System.Int32 System.Xml.Schema.XmlSchemaException::linePosition
	int32_t ___linePosition_13;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaException::sourceObj
	XmlSchemaObject_t2050913741 * ___sourceObj_14;
	// System.String System.Xml.Schema.XmlSchemaException::sourceUri
	String_t* ___sourceUri_15;

public:
	inline static int32_t get_offset_of_hasLineInfo_11() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___hasLineInfo_11)); }
	inline bool get_hasLineInfo_11() const { return ___hasLineInfo_11; }
	inline bool* get_address_of_hasLineInfo_11() { return &___hasLineInfo_11; }
	inline void set_hasLineInfo_11(bool value)
	{
		___hasLineInfo_11 = value;
	}

	inline static int32_t get_offset_of_lineNumber_12() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___lineNumber_12)); }
	inline int32_t get_lineNumber_12() const { return ___lineNumber_12; }
	inline int32_t* get_address_of_lineNumber_12() { return &___lineNumber_12; }
	inline void set_lineNumber_12(int32_t value)
	{
		___lineNumber_12 = value;
	}

	inline static int32_t get_offset_of_linePosition_13() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___linePosition_13)); }
	inline int32_t get_linePosition_13() const { return ___linePosition_13; }
	inline int32_t* get_address_of_linePosition_13() { return &___linePosition_13; }
	inline void set_linePosition_13(int32_t value)
	{
		___linePosition_13 = value;
	}

	inline static int32_t get_offset_of_sourceObj_14() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___sourceObj_14)); }
	inline XmlSchemaObject_t2050913741 * get_sourceObj_14() const { return ___sourceObj_14; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_sourceObj_14() { return &___sourceObj_14; }
	inline void set_sourceObj_14(XmlSchemaObject_t2050913741 * value)
	{
		___sourceObj_14 = value;
		Il2CppCodeGenWriteBarrier((&___sourceObj_14), value);
	}

	inline static int32_t get_offset_of_sourceUri_15() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___sourceUri_15)); }
	inline String_t* get_sourceUri_15() const { return ___sourceUri_15; }
	inline String_t** get_address_of_sourceUri_15() { return &___sourceUri_15; }
	inline void set_sourceUri_15(String_t* value)
	{
		___sourceUri_15 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXCEPTION_T4082200141_H
#ifndef XMLSCHEMADERIVATIONMETHOD_T3165007540_H
#define XMLSCHEMADERIVATIONMETHOD_T3165007540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDerivationMethod
struct  XmlSchemaDerivationMethod_t3165007540 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDerivationMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaDerivationMethod_t3165007540, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADERIVATIONMETHOD_T3165007540_H
#ifndef ENUMERATOR_T334262260_H
#define ENUMERATOR_T334262260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  Enumerator_t334262260 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t799532586 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	KeyValuePair_2_t1430411454  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t334262260, ___l_0)); }
	inline List_1_t799532586 * get_l_0() const { return ___l_0; }
	inline List_1_t799532586 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t799532586 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t334262260, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t334262260, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t334262260, ___current_3)); }
	inline KeyValuePair_2_t1430411454  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1430411454 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1430411454  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T334262260_H
#ifndef XMLELEMENT_T2877111883_H
#define XMLELEMENT_T2877111883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlElement
struct  XmlElement_t2877111883  : public XmlLinkedNode_t1287616130
{
public:
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_t3359885287 * ___attributes_6;
	// System.Xml.XmlNameEntry System.Xml.XmlElement::name
	XmlNameEntry_t3745551716 * ___name_7;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastLinkedChild
	XmlLinkedNode_t1287616130 * ___lastLinkedChild_8;
	// System.Boolean System.Xml.XmlElement::isNotEmpty
	bool ___isNotEmpty_9;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlElement::schemaInfo
	RuntimeObject* ___schemaInfo_10;

public:
	inline static int32_t get_offset_of_attributes_6() { return static_cast<int32_t>(offsetof(XmlElement_t2877111883, ___attributes_6)); }
	inline XmlAttributeCollection_t3359885287 * get_attributes_6() const { return ___attributes_6; }
	inline XmlAttributeCollection_t3359885287 ** get_address_of_attributes_6() { return &___attributes_6; }
	inline void set_attributes_6(XmlAttributeCollection_t3359885287 * value)
	{
		___attributes_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(XmlElement_t2877111883, ___name_7)); }
	inline XmlNameEntry_t3745551716 * get_name_7() const { return ___name_7; }
	inline XmlNameEntry_t3745551716 ** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(XmlNameEntry_t3745551716 * value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_8() { return static_cast<int32_t>(offsetof(XmlElement_t2877111883, ___lastLinkedChild_8)); }
	inline XmlLinkedNode_t1287616130 * get_lastLinkedChild_8() const { return ___lastLinkedChild_8; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastLinkedChild_8() { return &___lastLinkedChild_8; }
	inline void set_lastLinkedChild_8(XmlLinkedNode_t1287616130 * value)
	{
		___lastLinkedChild_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_8), value);
	}

	inline static int32_t get_offset_of_isNotEmpty_9() { return static_cast<int32_t>(offsetof(XmlElement_t2877111883, ___isNotEmpty_9)); }
	inline bool get_isNotEmpty_9() const { return ___isNotEmpty_9; }
	inline bool* get_address_of_isNotEmpty_9() { return &___isNotEmpty_9; }
	inline void set_isNotEmpty_9(bool value)
	{
		___isNotEmpty_9 = value;
	}

	inline static int32_t get_offset_of_schemaInfo_10() { return static_cast<int32_t>(offsetof(XmlElement_t2877111883, ___schemaInfo_10)); }
	inline RuntimeObject* get_schemaInfo_10() const { return ___schemaInfo_10; }
	inline RuntimeObject** get_address_of_schemaInfo_10() { return &___schemaInfo_10; }
	inline void set_schemaInfo_10(RuntimeObject* value)
	{
		___schemaInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENT_T2877111883_H
#ifndef XMLDOCUMENTTYPE_T824160610_H
#define XMLDOCUMENTTYPE_T824160610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentType
struct  XmlDocumentType_t824160610  : public XmlLinkedNode_t1287616130
{
public:
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::entities
	XmlNamedNodeMap_t145210370 * ___entities_6;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::notations
	XmlNamedNodeMap_t145210370 * ___notations_7;
	// Mono.Xml.DTDObjectModel System.Xml.XmlDocumentType::dtd
	DTDObjectModel_t1113953282 * ___dtd_8;

public:
	inline static int32_t get_offset_of_entities_6() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___entities_6)); }
	inline XmlNamedNodeMap_t145210370 * get_entities_6() const { return ___entities_6; }
	inline XmlNamedNodeMap_t145210370 ** get_address_of_entities_6() { return &___entities_6; }
	inline void set_entities_6(XmlNamedNodeMap_t145210370 * value)
	{
		___entities_6 = value;
		Il2CppCodeGenWriteBarrier((&___entities_6), value);
	}

	inline static int32_t get_offset_of_notations_7() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___notations_7)); }
	inline XmlNamedNodeMap_t145210370 * get_notations_7() const { return ___notations_7; }
	inline XmlNamedNodeMap_t145210370 ** get_address_of_notations_7() { return &___notations_7; }
	inline void set_notations_7(XmlNamedNodeMap_t145210370 * value)
	{
		___notations_7 = value;
		Il2CppCodeGenWriteBarrier((&___notations_7), value);
	}

	inline static int32_t get_offset_of_dtd_8() { return static_cast<int32_t>(offsetof(XmlDocumentType_t824160610, ___dtd_8)); }
	inline DTDObjectModel_t1113953282 * get_dtd_8() const { return ___dtd_8; }
	inline DTDObjectModel_t1113953282 ** get_address_of_dtd_8() { return &___dtd_8; }
	inline void set_dtd_8(DTDObjectModel_t1113953282 * value)
	{
		___dtd_8 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPE_T824160610_H
#ifndef XMLEXCEPTION_T4188277960_H
#define XMLEXCEPTION_T4188277960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlException
struct  XmlException_t4188277960  : public SystemException_t3877406272
{
public:
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_12;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_13;
	// System.String System.Xml.XmlException::res
	String_t* ___res_14;
	// System.String[] System.Xml.XmlException::messages
	StringU5BU5D_t1642385972* ___messages_15;

public:
	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_linePosition_12() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___linePosition_12)); }
	inline int32_t get_linePosition_12() const { return ___linePosition_12; }
	inline int32_t* get_address_of_linePosition_12() { return &___linePosition_12; }
	inline void set_linePosition_12(int32_t value)
	{
		___linePosition_12 = value;
	}

	inline static int32_t get_offset_of_sourceUri_13() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___sourceUri_13)); }
	inline String_t* get_sourceUri_13() const { return ___sourceUri_13; }
	inline String_t** get_address_of_sourceUri_13() { return &___sourceUri_13; }
	inline void set_sourceUri_13(String_t* value)
	{
		___sourceUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_13), value);
	}

	inline static int32_t get_offset_of_res_14() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___res_14)); }
	inline String_t* get_res_14() const { return ___res_14; }
	inline String_t** get_address_of_res_14() { return &___res_14; }
	inline void set_res_14(String_t* value)
	{
		___res_14 = value;
		Il2CppCodeGenWriteBarrier((&___res_14), value);
	}

	inline static int32_t get_offset_of_messages_15() { return static_cast<int32_t>(offsetof(XmlException_t4188277960, ___messages_15)); }
	inline StringU5BU5D_t1642385972* get_messages_15() const { return ___messages_15; }
	inline StringU5BU5D_t1642385972** get_address_of_messages_15() { return &___messages_15; }
	inline void set_messages_15(StringU5BU5D_t1642385972* value)
	{
		___messages_15 = value;
		Il2CppCodeGenWriteBarrier((&___messages_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEXCEPTION_T4188277960_H
#ifndef XMLENTITYREFERENCE_T3053868353_H
#define XMLENTITYREFERENCE_T3053868353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntityReference
struct  XmlEntityReference_t3053868353  : public XmlLinkedNode_t1287616130
{
public:
	// System.String System.Xml.XmlEntityReference::entityName
	String_t* ___entityName_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntityReference::lastLinkedChild
	XmlLinkedNode_t1287616130 * ___lastLinkedChild_7;

public:
	inline static int32_t get_offset_of_entityName_6() { return static_cast<int32_t>(offsetof(XmlEntityReference_t3053868353, ___entityName_6)); }
	inline String_t* get_entityName_6() const { return ___entityName_6; }
	inline String_t** get_address_of_entityName_6() { return &___entityName_6; }
	inline void set_entityName_6(String_t* value)
	{
		___entityName_6 = value;
		Il2CppCodeGenWriteBarrier((&___entityName_6), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_7() { return static_cast<int32_t>(offsetof(XmlEntityReference_t3053868353, ___lastLinkedChild_7)); }
	inline XmlLinkedNode_t1287616130 * get_lastLinkedChild_7() const { return ___lastLinkedChild_7; }
	inline XmlLinkedNode_t1287616130 ** get_address_of_lastLinkedChild_7() { return &___lastLinkedChild_7; }
	inline void set_lastLinkedChild_7(XmlLinkedNode_t1287616130 * value)
	{
		___lastLinkedChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITYREFERENCE_T3053868353_H
#ifndef DTDPARAMETERENTITYDECLARATION_T252230634_H
#define DTDPARAMETERENTITYDECLARATION_T252230634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclaration
struct  DTDParameterEntityDeclaration_t252230634  : public DTDEntityBase_t2353758560
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATION_T252230634_H
#ifndef DTDENTITYDECLARATION_T4283284771_H
#define DTDENTITYDECLARATION_T4283284771_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclaration
struct  DTDEntityDeclaration_t4283284771  : public DTDEntityBase_t2353758560
{
public:
	// System.String Mono.Xml.DTDEntityDeclaration::entityValue
	String_t* ___entityValue_15;
	// System.String Mono.Xml.DTDEntityDeclaration::notationName
	String_t* ___notationName_16;
	// System.Collections.ArrayList Mono.Xml.DTDEntityDeclaration::ReferencingEntities
	ArrayList_t4252133567 * ___ReferencingEntities_17;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::scanned
	bool ___scanned_18;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::recursed
	bool ___recursed_19;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::hasExternalReference
	bool ___hasExternalReference_20;

public:
	inline static int32_t get_offset_of_entityValue_15() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t4283284771, ___entityValue_15)); }
	inline String_t* get_entityValue_15() const { return ___entityValue_15; }
	inline String_t** get_address_of_entityValue_15() { return &___entityValue_15; }
	inline void set_entityValue_15(String_t* value)
	{
		___entityValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___entityValue_15), value);
	}

	inline static int32_t get_offset_of_notationName_16() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t4283284771, ___notationName_16)); }
	inline String_t* get_notationName_16() const { return ___notationName_16; }
	inline String_t** get_address_of_notationName_16() { return &___notationName_16; }
	inline void set_notationName_16(String_t* value)
	{
		___notationName_16 = value;
		Il2CppCodeGenWriteBarrier((&___notationName_16), value);
	}

	inline static int32_t get_offset_of_ReferencingEntities_17() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t4283284771, ___ReferencingEntities_17)); }
	inline ArrayList_t4252133567 * get_ReferencingEntities_17() const { return ___ReferencingEntities_17; }
	inline ArrayList_t4252133567 ** get_address_of_ReferencingEntities_17() { return &___ReferencingEntities_17; }
	inline void set_ReferencingEntities_17(ArrayList_t4252133567 * value)
	{
		___ReferencingEntities_17 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencingEntities_17), value);
	}

	inline static int32_t get_offset_of_scanned_18() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t4283284771, ___scanned_18)); }
	inline bool get_scanned_18() const { return ___scanned_18; }
	inline bool* get_address_of_scanned_18() { return &___scanned_18; }
	inline void set_scanned_18(bool value)
	{
		___scanned_18 = value;
	}

	inline static int32_t get_offset_of_recursed_19() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t4283284771, ___recursed_19)); }
	inline bool get_recursed_19() const { return ___recursed_19; }
	inline bool* get_address_of_recursed_19() { return &___recursed_19; }
	inline void set_recursed_19(bool value)
	{
		___recursed_19 = value;
	}

	inline static int32_t get_offset_of_hasExternalReference_20() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t4283284771, ___hasExternalReference_20)); }
	inline bool get_hasExternalReference_20() const { return ___hasExternalReference_20; }
	inline bool* get_address_of_hasExternalReference_20() { return &___hasExternalReference_20; }
	inline void set_hasExternalReference_20(bool value)
	{
		___hasExternalReference_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATION_T4283284771_H
#ifndef DTDCONTENTORDERTYPE_T3150259539_H
#define DTDCONTENTORDERTYPE_T3150259539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentOrderType
struct  DTDContentOrderType_t3150259539 
{
public:
	// System.Int32 Mono.Xml.DTDContentOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDContentOrderType_t3150259539, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTORDERTYPE_T3150259539_H
#ifndef DTDCOLLECTIONBASE_T2621362935_H
#define DTDCOLLECTIONBASE_T2621362935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDCollectionBase
struct  DTDCollectionBase_t2621362935  : public DictionaryBase_t1005937181
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDCollectionBase::root
	DTDObjectModel_t1113953282 * ___root_5;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDCollectionBase_t2621362935, ___root_5)); }
	inline DTDObjectModel_t1113953282 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1113953282 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1113953282 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCOLLECTIONBASE_T2621362935_H
#ifndef DTDATTRIBUTEOCCURENCETYPE_T2819881069_H
#define DTDATTRIBUTEOCCURENCETYPE_T2819881069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttributeOccurenceType
struct  DTDAttributeOccurenceType_t2819881069 
{
public:
	// System.Int32 Mono.Xml.DTDAttributeOccurenceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDAttributeOccurenceType_t2819881069, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTRIBUTEOCCURENCETYPE_T2819881069_H
#ifndef TRANSITION_T34209471_H
#define TRANSITION_T34209471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidator/Transition
struct  Transition_t34209471 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidator/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t34209471, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T34209471_H
#ifndef SCHEMATYPES_T3045759914_H
#define SCHEMATYPES_T3045759914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SchemaTypes
struct  SchemaTypes_t3045759914 
{
public:
	// System.Int32 System.Xml.Serialization.SchemaTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SchemaTypes_t3045759914, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMATYPES_T3045759914_H
#ifndef CONFORMANCELEVEL_T3761201363_H
#define CONFORMANCELEVEL_T3761201363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t3761201363 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConformanceLevel_t3761201363, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T3761201363_H
#ifndef XMLTYPECODE_T58293802_H
#define XMLTYPECODE_T58293802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlTypeCode
struct  XmlTypeCode_t58293802 
{
public:
	// System.Int32 System.Xml.Schema.XmlTypeCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlTypeCode_t58293802, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPECODE_T58293802_H
#ifndef XMLSCHEMAVALIDITY_T995929432_H
#define XMLSCHEMAVALIDITY_T995929432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidity
struct  XmlSchemaValidity_t995929432 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaValidity_t995929432, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDITY_T995929432_H
#ifndef XMLSEVERITYTYPE_T3547578624_H
#define XMLSEVERITYTYPE_T3547578624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSeverityType
struct  XmlSeverityType_t3547578624 
{
public:
	// System.Int32 System.Xml.Schema.XmlSeverityType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSeverityType_t3547578624, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSEVERITYTYPE_T3547578624_H
#ifndef ENTITYHANDLING_T3960499440_H
#define ENTITYHANDLING_T3960499440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_t3960499440 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EntityHandling_t3960499440, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_T3960499440_H
#ifndef WRITESTATE_T1534871862_H
#define WRITESTATE_T1534871862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_t1534871862 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WriteState_t1534871862, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T1534871862_H
#ifndef VALIDATIONTYPE_T1401987383_H
#define VALIDATIONTYPE_T1401987383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_t1401987383 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ValidationType_t1401987383, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_T1401987383_H
#ifndef DTDOCCURENCE_T99371501_H
#define DTDOCCURENCE_T99371501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDOccurence
struct  DTDOccurence_t99371501 
{
public:
	// System.Int32 Mono.Xml.DTDOccurence::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDOccurence_t99371501, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOCCURENCE_T99371501_H
#ifndef XMLSCHEMAVALIDATIONFLAGS_T910489930_H
#define XMLSCHEMAVALIDATIONFLAGS_T910489930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationFlags
struct  XmlSchemaValidationFlags_t910489930 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaValidationFlags_t910489930, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONFLAGS_T910489930_H
#ifndef READSTATE_T3138905245_H
#define READSTATE_T3138905245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_t3138905245 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_t3138905245, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_T3138905245_H
#ifndef FORMATTING_T1126649075_H
#define FORMATTING_T1126649075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Formatting
struct  Formatting_t1126649075 
{
public:
	// System.Int32 System.Xml.Formatting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Formatting_t1126649075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_T1126649075_H
#ifndef WHITESPACEHANDLING_T3754063142_H
#define WHITESPACEHANDLING_T3754063142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WhitespaceHandling
struct  WhitespaceHandling_t3754063142 
{
public:
	// System.Int32 System.Xml.WhitespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WhitespaceHandling_t3754063142, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITESPACEHANDLING_T3754063142_H
#ifndef NAMESPACEHANDLING_T1452270444_H
#define NAMESPACEHANDLING_T1452270444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_t1452270444 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamespaceHandling_t1452270444, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_T1452270444_H
#ifndef NEWLINEHANDLING_T1737195169_H
#define NEWLINEHANDLING_T1737195169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t1737195169 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NewLineHandling_t1737195169, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T1737195169_H
#ifndef XMLSCHEMAUTIL_T3576230726_H
#define XMLSCHEMAUTIL_T3576230726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUtil
struct  XmlSchemaUtil_t3576230726  : public RuntimeObject
{
public:

public:
};

struct XmlSchemaUtil_t3576230726_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::FinalAllowed
	int32_t ___FinalAllowed_0;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::ElementBlockAllowed
	int32_t ___ElementBlockAllowed_1;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::ComplexTypeBlockAllowed
	int32_t ___ComplexTypeBlockAllowed_2;
	// System.Boolean System.Xml.Schema.XmlSchemaUtil::StrictMsCompliant
	bool ___StrictMsCompliant_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switch$map36
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map36_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switch$map37
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map37_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switch$map38
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map38_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switch$map39
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map39_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switch$map3A
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map3A_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switch$map3B
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map3B_9;

public:
	inline static int32_t get_offset_of_FinalAllowed_0() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___FinalAllowed_0)); }
	inline int32_t get_FinalAllowed_0() const { return ___FinalAllowed_0; }
	inline int32_t* get_address_of_FinalAllowed_0() { return &___FinalAllowed_0; }
	inline void set_FinalAllowed_0(int32_t value)
	{
		___FinalAllowed_0 = value;
	}

	inline static int32_t get_offset_of_ElementBlockAllowed_1() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___ElementBlockAllowed_1)); }
	inline int32_t get_ElementBlockAllowed_1() const { return ___ElementBlockAllowed_1; }
	inline int32_t* get_address_of_ElementBlockAllowed_1() { return &___ElementBlockAllowed_1; }
	inline void set_ElementBlockAllowed_1(int32_t value)
	{
		___ElementBlockAllowed_1 = value;
	}

	inline static int32_t get_offset_of_ComplexTypeBlockAllowed_2() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___ComplexTypeBlockAllowed_2)); }
	inline int32_t get_ComplexTypeBlockAllowed_2() const { return ___ComplexTypeBlockAllowed_2; }
	inline int32_t* get_address_of_ComplexTypeBlockAllowed_2() { return &___ComplexTypeBlockAllowed_2; }
	inline void set_ComplexTypeBlockAllowed_2(int32_t value)
	{
		___ComplexTypeBlockAllowed_2 = value;
	}

	inline static int32_t get_offset_of_StrictMsCompliant_3() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___StrictMsCompliant_3)); }
	inline bool get_StrictMsCompliant_3() const { return ___StrictMsCompliant_3; }
	inline bool* get_address_of_StrictMsCompliant_3() { return &___StrictMsCompliant_3; }
	inline void set_StrictMsCompliant_3(bool value)
	{
		___StrictMsCompliant_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map36_4() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___U3CU3Ef__switchU24map36_4)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map36_4() const { return ___U3CU3Ef__switchU24map36_4; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map36_4() { return &___U3CU3Ef__switchU24map36_4; }
	inline void set_U3CU3Ef__switchU24map36_4(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map36_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map36_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map37_5() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___U3CU3Ef__switchU24map37_5)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map37_5() const { return ___U3CU3Ef__switchU24map37_5; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map37_5() { return &___U3CU3Ef__switchU24map37_5; }
	inline void set_U3CU3Ef__switchU24map37_5(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map37_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map37_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map38_6() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___U3CU3Ef__switchU24map38_6)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map38_6() const { return ___U3CU3Ef__switchU24map38_6; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map38_6() { return &___U3CU3Ef__switchU24map38_6; }
	inline void set_U3CU3Ef__switchU24map38_6(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map38_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map38_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map39_7() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___U3CU3Ef__switchU24map39_7)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map39_7() const { return ___U3CU3Ef__switchU24map39_7; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map39_7() { return &___U3CU3Ef__switchU24map39_7; }
	inline void set_U3CU3Ef__switchU24map39_7(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map39_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map39_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3A_8() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___U3CU3Ef__switchU24map3A_8)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map3A_8() const { return ___U3CU3Ef__switchU24map3A_8; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map3A_8() { return &___U3CU3Ef__switchU24map3A_8; }
	inline void set_U3CU3Ef__switchU24map3A_8(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map3A_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3A_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3B_9() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_t3576230726_StaticFields, ___U3CU3Ef__switchU24map3B_9)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map3B_9() const { return ___U3CU3Ef__switchU24map3B_9; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map3B_9() { return &___U3CU3Ef__switchU24map3B_9; }
	inline void set_U3CU3Ef__switchU24map3B_9(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map3B_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3B_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUTIL_T3576230726_H
#ifndef U3CU3EC__ITERATOR3_T3518389200_H
#define U3CU3EC__ITERATOR3_T3518389200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase/<>c__Iterator3
struct  U3CU3Ec__Iterator3_t3518389200  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>> Mono.Xml.DictionaryBase/<>c__Iterator3::<$s_431>__0
	Enumerator_t334262260  ___U3CU24s_431U3E__0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode> Mono.Xml.DictionaryBase/<>c__Iterator3::<p>__1
	KeyValuePair_2_t1430411454  ___U3CpU3E__1_1;
	// System.Int32 Mono.Xml.DictionaryBase/<>c__Iterator3::$PC
	int32_t ___U24PC_2;
	// Mono.Xml.DTDNode Mono.Xml.DictionaryBase/<>c__Iterator3::$current
	DTDNode_t1758286970 * ___U24current_3;
	// Mono.Xml.DictionaryBase Mono.Xml.DictionaryBase/<>c__Iterator3::<>f__this
	DictionaryBase_t1005937181 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_431U3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t3518389200, ___U3CU24s_431U3E__0_0)); }
	inline Enumerator_t334262260  get_U3CU24s_431U3E__0_0() const { return ___U3CU24s_431U3E__0_0; }
	inline Enumerator_t334262260 * get_address_of_U3CU24s_431U3E__0_0() { return &___U3CU24s_431U3E__0_0; }
	inline void set_U3CU24s_431U3E__0_0(Enumerator_t334262260  value)
	{
		___U3CU24s_431U3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t3518389200, ___U3CpU3E__1_1)); }
	inline KeyValuePair_2_t1430411454  get_U3CpU3E__1_1() const { return ___U3CpU3E__1_1; }
	inline KeyValuePair_2_t1430411454 * get_address_of_U3CpU3E__1_1() { return &___U3CpU3E__1_1; }
	inline void set_U3CpU3E__1_1(KeyValuePair_2_t1430411454  value)
	{
		___U3CpU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t3518389200, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t3518389200, ___U24current_3)); }
	inline DTDNode_t1758286970 * get_U24current_3() const { return ___U24current_3; }
	inline DTDNode_t1758286970 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(DTDNode_t1758286970 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_t3518389200, ___U3CU3Ef__this_4)); }
	inline DictionaryBase_t1005937181 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline DictionaryBase_t1005937181 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(DictionaryBase_t1005937181 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR3_T3518389200_H
#ifndef XMLSCHEMAVALIDATOR_T1978690704_H
#define XMLSCHEMAVALIDATOR_T1978690704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidator
struct  XmlSchemaValidator_t1978690704  : public RuntimeObject
{
public:
	// System.Object System.Xml.Schema.XmlSchemaValidator::nominalEventSender
	RuntimeObject * ___nominalEventSender_1;
	// System.Xml.IXmlLineInfo System.Xml.Schema.XmlSchemaValidator::lineInfo
	RuntimeObject* ___lineInfo_2;
	// System.Xml.IXmlNamespaceResolver System.Xml.Schema.XmlSchemaValidator::nsResolver
	RuntimeObject* ___nsResolver_3;
	// System.Uri System.Xml.Schema.XmlSchemaValidator::sourceUri
	Uri_t19570940 * ___sourceUri_4;
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchemaValidator::nameTable
	XmlNameTable_t1345805268 * ___nameTable_5;
	// System.Xml.Schema.XmlSchemaSet System.Xml.Schema.XmlSchemaValidator::schemas
	XmlSchemaSet_t313318308 * ___schemas_6;
	// System.Xml.XmlResolver System.Xml.Schema.XmlSchemaValidator::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_7;
	// System.Xml.Schema.XmlSchemaValidationFlags System.Xml.Schema.XmlSchemaValidator::options
	int32_t ___options_8;
	// System.Xml.Schema.XmlSchemaValidator/Transition System.Xml.Schema.XmlSchemaValidator::transition
	int32_t ___transition_9;
	// Mono.Xml.Schema.XsdParticleStateManager System.Xml.Schema.XmlSchemaValidator::state
	XsdParticleStateManager_t4119977941 * ___state_10;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaValidator::occuredAtts
	ArrayList_t4252133567 * ___occuredAtts_11;
	// System.Xml.Schema.XmlSchemaAttribute[] System.Xml.Schema.XmlSchemaValidator::defaultAttributes
	XmlSchemaAttributeU5BU5D_t3434391819* ___defaultAttributes_12;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaValidator::defaultAttributesCache
	ArrayList_t4252133567 * ___defaultAttributesCache_13;
	// Mono.Xml.Schema.XsdIDManager System.Xml.Schema.XmlSchemaValidator::idManager
	XsdIDManager_t3860002335 * ___idManager_14;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaValidator::keyTables
	ArrayList_t4252133567 * ___keyTables_15;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaValidator::currentKeyFieldConsumers
	ArrayList_t4252133567 * ___currentKeyFieldConsumers_16;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaValidator::tmpKeyrefPool
	ArrayList_t4252133567 * ___tmpKeyrefPool_17;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaValidator::elementQNameStack
	ArrayList_t4252133567 * ___elementQNameStack_18;
	// System.Text.StringBuilder System.Xml.Schema.XmlSchemaValidator::storedCharacters
	StringBuilder_t1221177846 * ___storedCharacters_19;
	// System.Boolean System.Xml.Schema.XmlSchemaValidator::shouldValidateCharacters
	bool ___shouldValidateCharacters_20;
	// System.Int32 System.Xml.Schema.XmlSchemaValidator::depth
	int32_t ___depth_21;
	// System.Int32 System.Xml.Schema.XmlSchemaValidator::xsiNilDepth
	int32_t ___xsiNilDepth_22;
	// System.Int32 System.Xml.Schema.XmlSchemaValidator::skipValidationDepth
	int32_t ___skipValidationDepth_23;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaValidator::CurrentAttributeType
	XmlSchemaDatatype_t1195946242 * ___CurrentAttributeType_24;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaValidator::ValidationEventHandler
	ValidationEventHandler_t1580700381 * ___ValidationEventHandler_25;

public:
	inline static int32_t get_offset_of_nominalEventSender_1() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___nominalEventSender_1)); }
	inline RuntimeObject * get_nominalEventSender_1() const { return ___nominalEventSender_1; }
	inline RuntimeObject ** get_address_of_nominalEventSender_1() { return &___nominalEventSender_1; }
	inline void set_nominalEventSender_1(RuntimeObject * value)
	{
		___nominalEventSender_1 = value;
		Il2CppCodeGenWriteBarrier((&___nominalEventSender_1), value);
	}

	inline static int32_t get_offset_of_lineInfo_2() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___lineInfo_2)); }
	inline RuntimeObject* get_lineInfo_2() const { return ___lineInfo_2; }
	inline RuntimeObject** get_address_of_lineInfo_2() { return &___lineInfo_2; }
	inline void set_lineInfo_2(RuntimeObject* value)
	{
		___lineInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_2), value);
	}

	inline static int32_t get_offset_of_nsResolver_3() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___nsResolver_3)); }
	inline RuntimeObject* get_nsResolver_3() const { return ___nsResolver_3; }
	inline RuntimeObject** get_address_of_nsResolver_3() { return &___nsResolver_3; }
	inline void set_nsResolver_3(RuntimeObject* value)
	{
		___nsResolver_3 = value;
		Il2CppCodeGenWriteBarrier((&___nsResolver_3), value);
	}

	inline static int32_t get_offset_of_sourceUri_4() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___sourceUri_4)); }
	inline Uri_t19570940 * get_sourceUri_4() const { return ___sourceUri_4; }
	inline Uri_t19570940 ** get_address_of_sourceUri_4() { return &___sourceUri_4; }
	inline void set_sourceUri_4(Uri_t19570940 * value)
	{
		___sourceUri_4 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_4), value);
	}

	inline static int32_t get_offset_of_nameTable_5() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___nameTable_5)); }
	inline XmlNameTable_t1345805268 * get_nameTable_5() const { return ___nameTable_5; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_5() { return &___nameTable_5; }
	inline void set_nameTable_5(XmlNameTable_t1345805268 * value)
	{
		___nameTable_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_5), value);
	}

	inline static int32_t get_offset_of_schemas_6() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___schemas_6)); }
	inline XmlSchemaSet_t313318308 * get_schemas_6() const { return ___schemas_6; }
	inline XmlSchemaSet_t313318308 ** get_address_of_schemas_6() { return &___schemas_6; }
	inline void set_schemas_6(XmlSchemaSet_t313318308 * value)
	{
		___schemas_6 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_6), value);
	}

	inline static int32_t get_offset_of_xmlResolver_7() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___xmlResolver_7)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_7() const { return ___xmlResolver_7; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_7() { return &___xmlResolver_7; }
	inline void set_xmlResolver_7(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_7), value);
	}

	inline static int32_t get_offset_of_options_8() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___options_8)); }
	inline int32_t get_options_8() const { return ___options_8; }
	inline int32_t* get_address_of_options_8() { return &___options_8; }
	inline void set_options_8(int32_t value)
	{
		___options_8 = value;
	}

	inline static int32_t get_offset_of_transition_9() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___transition_9)); }
	inline int32_t get_transition_9() const { return ___transition_9; }
	inline int32_t* get_address_of_transition_9() { return &___transition_9; }
	inline void set_transition_9(int32_t value)
	{
		___transition_9 = value;
	}

	inline static int32_t get_offset_of_state_10() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___state_10)); }
	inline XsdParticleStateManager_t4119977941 * get_state_10() const { return ___state_10; }
	inline XsdParticleStateManager_t4119977941 ** get_address_of_state_10() { return &___state_10; }
	inline void set_state_10(XsdParticleStateManager_t4119977941 * value)
	{
		___state_10 = value;
		Il2CppCodeGenWriteBarrier((&___state_10), value);
	}

	inline static int32_t get_offset_of_occuredAtts_11() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___occuredAtts_11)); }
	inline ArrayList_t4252133567 * get_occuredAtts_11() const { return ___occuredAtts_11; }
	inline ArrayList_t4252133567 ** get_address_of_occuredAtts_11() { return &___occuredAtts_11; }
	inline void set_occuredAtts_11(ArrayList_t4252133567 * value)
	{
		___occuredAtts_11 = value;
		Il2CppCodeGenWriteBarrier((&___occuredAtts_11), value);
	}

	inline static int32_t get_offset_of_defaultAttributes_12() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___defaultAttributes_12)); }
	inline XmlSchemaAttributeU5BU5D_t3434391819* get_defaultAttributes_12() const { return ___defaultAttributes_12; }
	inline XmlSchemaAttributeU5BU5D_t3434391819** get_address_of_defaultAttributes_12() { return &___defaultAttributes_12; }
	inline void set_defaultAttributes_12(XmlSchemaAttributeU5BU5D_t3434391819* value)
	{
		___defaultAttributes_12 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributes_12), value);
	}

	inline static int32_t get_offset_of_defaultAttributesCache_13() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___defaultAttributesCache_13)); }
	inline ArrayList_t4252133567 * get_defaultAttributesCache_13() const { return ___defaultAttributesCache_13; }
	inline ArrayList_t4252133567 ** get_address_of_defaultAttributesCache_13() { return &___defaultAttributesCache_13; }
	inline void set_defaultAttributesCache_13(ArrayList_t4252133567 * value)
	{
		___defaultAttributesCache_13 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributesCache_13), value);
	}

	inline static int32_t get_offset_of_idManager_14() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___idManager_14)); }
	inline XsdIDManager_t3860002335 * get_idManager_14() const { return ___idManager_14; }
	inline XsdIDManager_t3860002335 ** get_address_of_idManager_14() { return &___idManager_14; }
	inline void set_idManager_14(XsdIDManager_t3860002335 * value)
	{
		___idManager_14 = value;
		Il2CppCodeGenWriteBarrier((&___idManager_14), value);
	}

	inline static int32_t get_offset_of_keyTables_15() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___keyTables_15)); }
	inline ArrayList_t4252133567 * get_keyTables_15() const { return ___keyTables_15; }
	inline ArrayList_t4252133567 ** get_address_of_keyTables_15() { return &___keyTables_15; }
	inline void set_keyTables_15(ArrayList_t4252133567 * value)
	{
		___keyTables_15 = value;
		Il2CppCodeGenWriteBarrier((&___keyTables_15), value);
	}

	inline static int32_t get_offset_of_currentKeyFieldConsumers_16() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___currentKeyFieldConsumers_16)); }
	inline ArrayList_t4252133567 * get_currentKeyFieldConsumers_16() const { return ___currentKeyFieldConsumers_16; }
	inline ArrayList_t4252133567 ** get_address_of_currentKeyFieldConsumers_16() { return &___currentKeyFieldConsumers_16; }
	inline void set_currentKeyFieldConsumers_16(ArrayList_t4252133567 * value)
	{
		___currentKeyFieldConsumers_16 = value;
		Il2CppCodeGenWriteBarrier((&___currentKeyFieldConsumers_16), value);
	}

	inline static int32_t get_offset_of_tmpKeyrefPool_17() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___tmpKeyrefPool_17)); }
	inline ArrayList_t4252133567 * get_tmpKeyrefPool_17() const { return ___tmpKeyrefPool_17; }
	inline ArrayList_t4252133567 ** get_address_of_tmpKeyrefPool_17() { return &___tmpKeyrefPool_17; }
	inline void set_tmpKeyrefPool_17(ArrayList_t4252133567 * value)
	{
		___tmpKeyrefPool_17 = value;
		Il2CppCodeGenWriteBarrier((&___tmpKeyrefPool_17), value);
	}

	inline static int32_t get_offset_of_elementQNameStack_18() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___elementQNameStack_18)); }
	inline ArrayList_t4252133567 * get_elementQNameStack_18() const { return ___elementQNameStack_18; }
	inline ArrayList_t4252133567 ** get_address_of_elementQNameStack_18() { return &___elementQNameStack_18; }
	inline void set_elementQNameStack_18(ArrayList_t4252133567 * value)
	{
		___elementQNameStack_18 = value;
		Il2CppCodeGenWriteBarrier((&___elementQNameStack_18), value);
	}

	inline static int32_t get_offset_of_storedCharacters_19() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___storedCharacters_19)); }
	inline StringBuilder_t1221177846 * get_storedCharacters_19() const { return ___storedCharacters_19; }
	inline StringBuilder_t1221177846 ** get_address_of_storedCharacters_19() { return &___storedCharacters_19; }
	inline void set_storedCharacters_19(StringBuilder_t1221177846 * value)
	{
		___storedCharacters_19 = value;
		Il2CppCodeGenWriteBarrier((&___storedCharacters_19), value);
	}

	inline static int32_t get_offset_of_shouldValidateCharacters_20() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___shouldValidateCharacters_20)); }
	inline bool get_shouldValidateCharacters_20() const { return ___shouldValidateCharacters_20; }
	inline bool* get_address_of_shouldValidateCharacters_20() { return &___shouldValidateCharacters_20; }
	inline void set_shouldValidateCharacters_20(bool value)
	{
		___shouldValidateCharacters_20 = value;
	}

	inline static int32_t get_offset_of_depth_21() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___depth_21)); }
	inline int32_t get_depth_21() const { return ___depth_21; }
	inline int32_t* get_address_of_depth_21() { return &___depth_21; }
	inline void set_depth_21(int32_t value)
	{
		___depth_21 = value;
	}

	inline static int32_t get_offset_of_xsiNilDepth_22() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___xsiNilDepth_22)); }
	inline int32_t get_xsiNilDepth_22() const { return ___xsiNilDepth_22; }
	inline int32_t* get_address_of_xsiNilDepth_22() { return &___xsiNilDepth_22; }
	inline void set_xsiNilDepth_22(int32_t value)
	{
		___xsiNilDepth_22 = value;
	}

	inline static int32_t get_offset_of_skipValidationDepth_23() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___skipValidationDepth_23)); }
	inline int32_t get_skipValidationDepth_23() const { return ___skipValidationDepth_23; }
	inline int32_t* get_address_of_skipValidationDepth_23() { return &___skipValidationDepth_23; }
	inline void set_skipValidationDepth_23(int32_t value)
	{
		___skipValidationDepth_23 = value;
	}

	inline static int32_t get_offset_of_CurrentAttributeType_24() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___CurrentAttributeType_24)); }
	inline XmlSchemaDatatype_t1195946242 * get_CurrentAttributeType_24() const { return ___CurrentAttributeType_24; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_CurrentAttributeType_24() { return &___CurrentAttributeType_24; }
	inline void set_CurrentAttributeType_24(XmlSchemaDatatype_t1195946242 * value)
	{
		___CurrentAttributeType_24 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentAttributeType_24), value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_25() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704, ___ValidationEventHandler_25)); }
	inline ValidationEventHandler_t1580700381 * get_ValidationEventHandler_25() const { return ___ValidationEventHandler_25; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_ValidationEventHandler_25() { return &___ValidationEventHandler_25; }
	inline void set_ValidationEventHandler_25(ValidationEventHandler_t1580700381 * value)
	{
		___ValidationEventHandler_25 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_25), value);
	}
};

struct XmlSchemaValidator_t1978690704_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAttribute[] System.Xml.Schema.XmlSchemaValidator::emptyAttributeArray
	XmlSchemaAttributeU5BU5D_t3434391819* ___emptyAttributeArray_0;

public:
	inline static int32_t get_offset_of_emptyAttributeArray_0() { return static_cast<int32_t>(offsetof(XmlSchemaValidator_t1978690704_StaticFields, ___emptyAttributeArray_0)); }
	inline XmlSchemaAttributeU5BU5D_t3434391819* get_emptyAttributeArray_0() const { return ___emptyAttributeArray_0; }
	inline XmlSchemaAttributeU5BU5D_t3434391819** get_address_of_emptyAttributeArray_0() { return &___emptyAttributeArray_0; }
	inline void set_emptyAttributeArray_0(XmlSchemaAttributeU5BU5D_t3434391819* value)
	{
		___emptyAttributeArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAttributeArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATOR_T1978690704_H
#ifndef XMLSCHEMAVALIDATIONEXCEPTION_T2387044366_H
#define XMLSCHEMAVALIDATIONEXCEPTION_T2387044366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationException
struct  XmlSchemaValidationException_t2387044366  : public XmlSchemaException_t4082200141
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONEXCEPTION_T2387044366_H
#ifndef TYPEDATA_T3979356678_H
#define TYPEDATA_T3979356678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeData
struct  TypeData_t3979356678  : public RuntimeObject
{
public:
	// System.Type System.Xml.Serialization.TypeData::type
	Type_t * ___type_0;
	// System.String System.Xml.Serialization.TypeData::elementName
	String_t* ___elementName_1;
	// System.Xml.Serialization.SchemaTypes System.Xml.Serialization.TypeData::sType
	int32_t ___sType_2;
	// System.Type System.Xml.Serialization.TypeData::listItemType
	Type_t * ___listItemType_3;
	// System.String System.Xml.Serialization.TypeData::typeName
	String_t* ___typeName_4;
	// System.String System.Xml.Serialization.TypeData::fullTypeName
	String_t* ___fullTypeName_5;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::listItemTypeData
	TypeData_t3979356678 * ___listItemTypeData_6;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::mappedType
	TypeData_t3979356678 * ___mappedType_7;
	// System.Xml.Schema.XmlSchemaPatternFacet System.Xml.Serialization.TypeData::facet
	XmlSchemaPatternFacet_t2024909611 * ___facet_8;
	// System.Boolean System.Xml.Serialization.TypeData::hasPublicConstructor
	bool ___hasPublicConstructor_9;
	// System.Boolean System.Xml.Serialization.TypeData::nullableOverride
	bool ___nullableOverride_10;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_elementName_1() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___elementName_1)); }
	inline String_t* get_elementName_1() const { return ___elementName_1; }
	inline String_t** get_address_of_elementName_1() { return &___elementName_1; }
	inline void set_elementName_1(String_t* value)
	{
		___elementName_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_1), value);
	}

	inline static int32_t get_offset_of_sType_2() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___sType_2)); }
	inline int32_t get_sType_2() const { return ___sType_2; }
	inline int32_t* get_address_of_sType_2() { return &___sType_2; }
	inline void set_sType_2(int32_t value)
	{
		___sType_2 = value;
	}

	inline static int32_t get_offset_of_listItemType_3() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___listItemType_3)); }
	inline Type_t * get_listItemType_3() const { return ___listItemType_3; }
	inline Type_t ** get_address_of_listItemType_3() { return &___listItemType_3; }
	inline void set_listItemType_3(Type_t * value)
	{
		___listItemType_3 = value;
		Il2CppCodeGenWriteBarrier((&___listItemType_3), value);
	}

	inline static int32_t get_offset_of_typeName_4() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___typeName_4)); }
	inline String_t* get_typeName_4() const { return ___typeName_4; }
	inline String_t** get_address_of_typeName_4() { return &___typeName_4; }
	inline void set_typeName_4(String_t* value)
	{
		___typeName_4 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_4), value);
	}

	inline static int32_t get_offset_of_fullTypeName_5() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___fullTypeName_5)); }
	inline String_t* get_fullTypeName_5() const { return ___fullTypeName_5; }
	inline String_t** get_address_of_fullTypeName_5() { return &___fullTypeName_5; }
	inline void set_fullTypeName_5(String_t* value)
	{
		___fullTypeName_5 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_5), value);
	}

	inline static int32_t get_offset_of_listItemTypeData_6() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___listItemTypeData_6)); }
	inline TypeData_t3979356678 * get_listItemTypeData_6() const { return ___listItemTypeData_6; }
	inline TypeData_t3979356678 ** get_address_of_listItemTypeData_6() { return &___listItemTypeData_6; }
	inline void set_listItemTypeData_6(TypeData_t3979356678 * value)
	{
		___listItemTypeData_6 = value;
		Il2CppCodeGenWriteBarrier((&___listItemTypeData_6), value);
	}

	inline static int32_t get_offset_of_mappedType_7() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___mappedType_7)); }
	inline TypeData_t3979356678 * get_mappedType_7() const { return ___mappedType_7; }
	inline TypeData_t3979356678 ** get_address_of_mappedType_7() { return &___mappedType_7; }
	inline void set_mappedType_7(TypeData_t3979356678 * value)
	{
		___mappedType_7 = value;
		Il2CppCodeGenWriteBarrier((&___mappedType_7), value);
	}

	inline static int32_t get_offset_of_facet_8() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___facet_8)); }
	inline XmlSchemaPatternFacet_t2024909611 * get_facet_8() const { return ___facet_8; }
	inline XmlSchemaPatternFacet_t2024909611 ** get_address_of_facet_8() { return &___facet_8; }
	inline void set_facet_8(XmlSchemaPatternFacet_t2024909611 * value)
	{
		___facet_8 = value;
		Il2CppCodeGenWriteBarrier((&___facet_8), value);
	}

	inline static int32_t get_offset_of_hasPublicConstructor_9() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___hasPublicConstructor_9)); }
	inline bool get_hasPublicConstructor_9() const { return ___hasPublicConstructor_9; }
	inline bool* get_address_of_hasPublicConstructor_9() { return &___hasPublicConstructor_9; }
	inline void set_hasPublicConstructor_9(bool value)
	{
		___hasPublicConstructor_9 = value;
	}

	inline static int32_t get_offset_of_nullableOverride_10() { return static_cast<int32_t>(offsetof(TypeData_t3979356678, ___nullableOverride_10)); }
	inline bool get_nullableOverride_10() const { return ___nullableOverride_10; }
	inline bool* get_address_of_nullableOverride_10() { return &___nullableOverride_10; }
	inline void set_nullableOverride_10(bool value)
	{
		___nullableOverride_10 = value;
	}
};

struct TypeData_t3979356678_StaticFields
{
public:
	// System.String[] System.Xml.Serialization.TypeData::keywords
	StringU5BU5D_t1642385972* ___keywords_11;

public:
	inline static int32_t get_offset_of_keywords_11() { return static_cast<int32_t>(offsetof(TypeData_t3979356678_StaticFields, ___keywords_11)); }
	inline StringU5BU5D_t1642385972* get_keywords_11() const { return ___keywords_11; }
	inline StringU5BU5D_t1642385972** get_address_of_keywords_11() { return &___keywords_11; }
	inline void set_keywords_11(StringU5BU5D_t1642385972* value)
	{
		___keywords_11 = value;
		Il2CppCodeGenWriteBarrier((&___keywords_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDATA_T3979356678_H
#ifndef DTDATTRIBUTEDEFINITION_T3692870749_H
#define DTDATTRIBUTEDEFINITION_T3692870749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttributeDefinition
struct  DTDAttributeDefinition_t3692870749  : public DTDNode_t1758286970
{
public:
	// System.String Mono.Xml.DTDAttributeDefinition::name
	String_t* ___name_5;
	// System.Xml.Schema.XmlSchemaDatatype Mono.Xml.DTDAttributeDefinition::datatype
	XmlSchemaDatatype_t1195946242 * ___datatype_6;
	// System.Collections.ArrayList Mono.Xml.DTDAttributeDefinition::enumeratedLiterals
	ArrayList_t4252133567 * ___enumeratedLiterals_7;
	// System.String Mono.Xml.DTDAttributeDefinition::unresolvedDefault
	String_t* ___unresolvedDefault_8;
	// System.Collections.ArrayList Mono.Xml.DTDAttributeDefinition::enumeratedNotations
	ArrayList_t4252133567 * ___enumeratedNotations_9;
	// Mono.Xml.DTDAttributeOccurenceType Mono.Xml.DTDAttributeDefinition::occurenceType
	int32_t ___occurenceType_10;
	// System.String Mono.Xml.DTDAttributeDefinition::resolvedDefaultValue
	String_t* ___resolvedDefaultValue_11;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3692870749, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_datatype_6() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3692870749, ___datatype_6)); }
	inline XmlSchemaDatatype_t1195946242 * get_datatype_6() const { return ___datatype_6; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_datatype_6() { return &___datatype_6; }
	inline void set_datatype_6(XmlSchemaDatatype_t1195946242 * value)
	{
		___datatype_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_6), value);
	}

	inline static int32_t get_offset_of_enumeratedLiterals_7() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3692870749, ___enumeratedLiterals_7)); }
	inline ArrayList_t4252133567 * get_enumeratedLiterals_7() const { return ___enumeratedLiterals_7; }
	inline ArrayList_t4252133567 ** get_address_of_enumeratedLiterals_7() { return &___enumeratedLiterals_7; }
	inline void set_enumeratedLiterals_7(ArrayList_t4252133567 * value)
	{
		___enumeratedLiterals_7 = value;
		Il2CppCodeGenWriteBarrier((&___enumeratedLiterals_7), value);
	}

	inline static int32_t get_offset_of_unresolvedDefault_8() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3692870749, ___unresolvedDefault_8)); }
	inline String_t* get_unresolvedDefault_8() const { return ___unresolvedDefault_8; }
	inline String_t** get_address_of_unresolvedDefault_8() { return &___unresolvedDefault_8; }
	inline void set_unresolvedDefault_8(String_t* value)
	{
		___unresolvedDefault_8 = value;
		Il2CppCodeGenWriteBarrier((&___unresolvedDefault_8), value);
	}

	inline static int32_t get_offset_of_enumeratedNotations_9() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3692870749, ___enumeratedNotations_9)); }
	inline ArrayList_t4252133567 * get_enumeratedNotations_9() const { return ___enumeratedNotations_9; }
	inline ArrayList_t4252133567 ** get_address_of_enumeratedNotations_9() { return &___enumeratedNotations_9; }
	inline void set_enumeratedNotations_9(ArrayList_t4252133567 * value)
	{
		___enumeratedNotations_9 = value;
		Il2CppCodeGenWriteBarrier((&___enumeratedNotations_9), value);
	}

	inline static int32_t get_offset_of_occurenceType_10() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3692870749, ___occurenceType_10)); }
	inline int32_t get_occurenceType_10() const { return ___occurenceType_10; }
	inline int32_t* get_address_of_occurenceType_10() { return &___occurenceType_10; }
	inline void set_occurenceType_10(int32_t value)
	{
		___occurenceType_10 = value;
	}

	inline static int32_t get_offset_of_resolvedDefaultValue_11() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_t3692870749, ___resolvedDefaultValue_11)); }
	inline String_t* get_resolvedDefaultValue_11() const { return ___resolvedDefaultValue_11; }
	inline String_t** get_address_of_resolvedDefaultValue_11() { return &___resolvedDefaultValue_11; }
	inline void set_resolvedDefaultValue_11(String_t* value)
	{
		___resolvedDefaultValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___resolvedDefaultValue_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTRIBUTEDEFINITION_T3692870749_H
#ifndef DTDCONTENTMODEL_T445576364_H
#define DTDCONTENTMODEL_T445576364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModel
struct  DTDContentModel_t445576364  : public DTDNode_t1758286970
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDContentModel::root
	DTDObjectModel_t1113953282 * ___root_5;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDContentModel::compiledAutomata
	DTDAutomata_t545990600 * ___compiledAutomata_6;
	// System.String Mono.Xml.DTDContentModel::ownerElementName
	String_t* ___ownerElementName_7;
	// System.String Mono.Xml.DTDContentModel::elementName
	String_t* ___elementName_8;
	// Mono.Xml.DTDContentOrderType Mono.Xml.DTDContentModel::orderType
	int32_t ___orderType_9;
	// Mono.Xml.DTDContentModelCollection Mono.Xml.DTDContentModel::childModels
	DTDContentModelCollection_t3164170484 * ___childModels_10;
	// Mono.Xml.DTDOccurence Mono.Xml.DTDContentModel::occurence
	int32_t ___occurence_11;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDContentModel_t445576364, ___root_5)); }
	inline DTDObjectModel_t1113953282 * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1113953282 ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1113953282 * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_compiledAutomata_6() { return static_cast<int32_t>(offsetof(DTDContentModel_t445576364, ___compiledAutomata_6)); }
	inline DTDAutomata_t545990600 * get_compiledAutomata_6() const { return ___compiledAutomata_6; }
	inline DTDAutomata_t545990600 ** get_address_of_compiledAutomata_6() { return &___compiledAutomata_6; }
	inline void set_compiledAutomata_6(DTDAutomata_t545990600 * value)
	{
		___compiledAutomata_6 = value;
		Il2CppCodeGenWriteBarrier((&___compiledAutomata_6), value);
	}

	inline static int32_t get_offset_of_ownerElementName_7() { return static_cast<int32_t>(offsetof(DTDContentModel_t445576364, ___ownerElementName_7)); }
	inline String_t* get_ownerElementName_7() const { return ___ownerElementName_7; }
	inline String_t** get_address_of_ownerElementName_7() { return &___ownerElementName_7; }
	inline void set_ownerElementName_7(String_t* value)
	{
		___ownerElementName_7 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElementName_7), value);
	}

	inline static int32_t get_offset_of_elementName_8() { return static_cast<int32_t>(offsetof(DTDContentModel_t445576364, ___elementName_8)); }
	inline String_t* get_elementName_8() const { return ___elementName_8; }
	inline String_t** get_address_of_elementName_8() { return &___elementName_8; }
	inline void set_elementName_8(String_t* value)
	{
		___elementName_8 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_8), value);
	}

	inline static int32_t get_offset_of_orderType_9() { return static_cast<int32_t>(offsetof(DTDContentModel_t445576364, ___orderType_9)); }
	inline int32_t get_orderType_9() const { return ___orderType_9; }
	inline int32_t* get_address_of_orderType_9() { return &___orderType_9; }
	inline void set_orderType_9(int32_t value)
	{
		___orderType_9 = value;
	}

	inline static int32_t get_offset_of_childModels_10() { return static_cast<int32_t>(offsetof(DTDContentModel_t445576364, ___childModels_10)); }
	inline DTDContentModelCollection_t3164170484 * get_childModels_10() const { return ___childModels_10; }
	inline DTDContentModelCollection_t3164170484 ** get_address_of_childModels_10() { return &___childModels_10; }
	inline void set_childModels_10(DTDContentModelCollection_t3164170484 * value)
	{
		___childModels_10 = value;
		Il2CppCodeGenWriteBarrier((&___childModels_10), value);
	}

	inline static int32_t get_offset_of_occurence_11() { return static_cast<int32_t>(offsetof(DTDContentModel_t445576364, ___occurence_11)); }
	inline int32_t get_occurence_11() const { return ___occurence_11; }
	inline int32_t* get_address_of_occurence_11() { return &___occurence_11; }
	inline void set_occurence_11(int32_t value)
	{
		___occurence_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODEL_T445576364_H
#ifndef XMLCDATASECTION_T1124775823_H
#define XMLCDATASECTION_T1124775823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCDataSection
struct  XmlCDataSection_t1124775823  : public XmlCharacterData_t575748506
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCDATASECTION_T1124775823_H
#ifndef XMLCOMMENT_T3999331572_H
#define XMLCOMMENT_T3999331572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlComment
struct  XmlComment_t3999331572  : public XmlCharacterData_t575748506
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCOMMENT_T3999331572_H
#ifndef XMLCONVERT_T1936105738_H
#define XMLCONVERT_T1936105738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConvert
struct  XmlConvert_t1936105738  : public RuntimeObject
{
public:

public:
};

struct XmlConvert_t1936105738_StaticFields
{
public:
	// System.String[] System.Xml.XmlConvert::datetimeFormats
	StringU5BU5D_t1642385972* ___datetimeFormats_0;
	// System.String[] System.Xml.XmlConvert::defaultDateTimeFormats
	StringU5BU5D_t1642385972* ___defaultDateTimeFormats_1;
	// System.String[] System.Xml.XmlConvert::roundtripDateTimeFormats
	StringU5BU5D_t1642385972* ___roundtripDateTimeFormats_2;
	// System.String[] System.Xml.XmlConvert::localDateTimeFormats
	StringU5BU5D_t1642385972* ___localDateTimeFormats_3;
	// System.String[] System.Xml.XmlConvert::utcDateTimeFormats
	StringU5BU5D_t1642385972* ___utcDateTimeFormats_4;
	// System.String[] System.Xml.XmlConvert::unspecifiedDateTimeFormats
	StringU5BU5D_t1642385972* ___unspecifiedDateTimeFormats_5;
	// System.Globalization.DateTimeStyles System.Xml.XmlConvert::_defaultStyle
	int32_t ____defaultStyle_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlConvert::<>f__switch$map49
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map49_7;

public:
	inline static int32_t get_offset_of_datetimeFormats_0() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___datetimeFormats_0)); }
	inline StringU5BU5D_t1642385972* get_datetimeFormats_0() const { return ___datetimeFormats_0; }
	inline StringU5BU5D_t1642385972** get_address_of_datetimeFormats_0() { return &___datetimeFormats_0; }
	inline void set_datetimeFormats_0(StringU5BU5D_t1642385972* value)
	{
		___datetimeFormats_0 = value;
		Il2CppCodeGenWriteBarrier((&___datetimeFormats_0), value);
	}

	inline static int32_t get_offset_of_defaultDateTimeFormats_1() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___defaultDateTimeFormats_1)); }
	inline StringU5BU5D_t1642385972* get_defaultDateTimeFormats_1() const { return ___defaultDateTimeFormats_1; }
	inline StringU5BU5D_t1642385972** get_address_of_defaultDateTimeFormats_1() { return &___defaultDateTimeFormats_1; }
	inline void set_defaultDateTimeFormats_1(StringU5BU5D_t1642385972* value)
	{
		___defaultDateTimeFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultDateTimeFormats_1), value);
	}

	inline static int32_t get_offset_of_roundtripDateTimeFormats_2() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___roundtripDateTimeFormats_2)); }
	inline StringU5BU5D_t1642385972* get_roundtripDateTimeFormats_2() const { return ___roundtripDateTimeFormats_2; }
	inline StringU5BU5D_t1642385972** get_address_of_roundtripDateTimeFormats_2() { return &___roundtripDateTimeFormats_2; }
	inline void set_roundtripDateTimeFormats_2(StringU5BU5D_t1642385972* value)
	{
		___roundtripDateTimeFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___roundtripDateTimeFormats_2), value);
	}

	inline static int32_t get_offset_of_localDateTimeFormats_3() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___localDateTimeFormats_3)); }
	inline StringU5BU5D_t1642385972* get_localDateTimeFormats_3() const { return ___localDateTimeFormats_3; }
	inline StringU5BU5D_t1642385972** get_address_of_localDateTimeFormats_3() { return &___localDateTimeFormats_3; }
	inline void set_localDateTimeFormats_3(StringU5BU5D_t1642385972* value)
	{
		___localDateTimeFormats_3 = value;
		Il2CppCodeGenWriteBarrier((&___localDateTimeFormats_3), value);
	}

	inline static int32_t get_offset_of_utcDateTimeFormats_4() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___utcDateTimeFormats_4)); }
	inline StringU5BU5D_t1642385972* get_utcDateTimeFormats_4() const { return ___utcDateTimeFormats_4; }
	inline StringU5BU5D_t1642385972** get_address_of_utcDateTimeFormats_4() { return &___utcDateTimeFormats_4; }
	inline void set_utcDateTimeFormats_4(StringU5BU5D_t1642385972* value)
	{
		___utcDateTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___utcDateTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_unspecifiedDateTimeFormats_5() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___unspecifiedDateTimeFormats_5)); }
	inline StringU5BU5D_t1642385972* get_unspecifiedDateTimeFormats_5() const { return ___unspecifiedDateTimeFormats_5; }
	inline StringU5BU5D_t1642385972** get_address_of_unspecifiedDateTimeFormats_5() { return &___unspecifiedDateTimeFormats_5; }
	inline void set_unspecifiedDateTimeFormats_5(StringU5BU5D_t1642385972* value)
	{
		___unspecifiedDateTimeFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___unspecifiedDateTimeFormats_5), value);
	}

	inline static int32_t get_offset_of__defaultStyle_6() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ____defaultStyle_6)); }
	inline int32_t get__defaultStyle_6() const { return ____defaultStyle_6; }
	inline int32_t* get_address_of__defaultStyle_6() { return &____defaultStyle_6; }
	inline void set__defaultStyle_6(int32_t value)
	{
		____defaultStyle_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map49_7() { return static_cast<int32_t>(offsetof(XmlConvert_t1936105738_StaticFields, ___U3CU3Ef__switchU24map49_7)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map49_7() const { return ___U3CU3Ef__switchU24map49_7; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map49_7() { return &___U3CU3Ef__switchU24map49_7; }
	inline void set_U3CU3Ef__switchU24map49_7(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map49_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map49_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONVERT_T1936105738_H
#ifndef ENTITYRESOLVINGXMLREADER_T2086920314_H
#define ENTITYRESOLVINGXMLREADER_T2086920314_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.EntityResolvingXmlReader
struct  EntityResolvingXmlReader_t2086920314  : public XmlReader_t3675626668
{
public:
	// Mono.Xml.EntityResolvingXmlReader Mono.Xml.EntityResolvingXmlReader::entity
	EntityResolvingXmlReader_t2086920314 * ___entity_2;
	// System.Xml.XmlReader Mono.Xml.EntityResolvingXmlReader::source
	XmlReader_t3675626668 * ___source_3;
	// System.Xml.XmlParserContext Mono.Xml.EntityResolvingXmlReader::context
	XmlParserContext_t2728039553 * ___context_4;
	// System.Xml.XmlResolver Mono.Xml.EntityResolvingXmlReader::resolver
	XmlResolver_t2024571559 * ___resolver_5;
	// System.Xml.EntityHandling Mono.Xml.EntityResolvingXmlReader::entity_handling
	int32_t ___entity_handling_6;
	// System.Boolean Mono.Xml.EntityResolvingXmlReader::entity_inside_attr
	bool ___entity_inside_attr_7;
	// System.Boolean Mono.Xml.EntityResolvingXmlReader::inside_attr
	bool ___inside_attr_8;
	// System.Boolean Mono.Xml.EntityResolvingXmlReader::do_resolve
	bool ___do_resolve_9;

public:
	inline static int32_t get_offset_of_entity_2() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t2086920314, ___entity_2)); }
	inline EntityResolvingXmlReader_t2086920314 * get_entity_2() const { return ___entity_2; }
	inline EntityResolvingXmlReader_t2086920314 ** get_address_of_entity_2() { return &___entity_2; }
	inline void set_entity_2(EntityResolvingXmlReader_t2086920314 * value)
	{
		___entity_2 = value;
		Il2CppCodeGenWriteBarrier((&___entity_2), value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t2086920314, ___source_3)); }
	inline XmlReader_t3675626668 * get_source_3() const { return ___source_3; }
	inline XmlReader_t3675626668 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(XmlReader_t3675626668 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_context_4() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t2086920314, ___context_4)); }
	inline XmlParserContext_t2728039553 * get_context_4() const { return ___context_4; }
	inline XmlParserContext_t2728039553 ** get_address_of_context_4() { return &___context_4; }
	inline void set_context_4(XmlParserContext_t2728039553 * value)
	{
		___context_4 = value;
		Il2CppCodeGenWriteBarrier((&___context_4), value);
	}

	inline static int32_t get_offset_of_resolver_5() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t2086920314, ___resolver_5)); }
	inline XmlResolver_t2024571559 * get_resolver_5() const { return ___resolver_5; }
	inline XmlResolver_t2024571559 ** get_address_of_resolver_5() { return &___resolver_5; }
	inline void set_resolver_5(XmlResolver_t2024571559 * value)
	{
		___resolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_5), value);
	}

	inline static int32_t get_offset_of_entity_handling_6() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t2086920314, ___entity_handling_6)); }
	inline int32_t get_entity_handling_6() const { return ___entity_handling_6; }
	inline int32_t* get_address_of_entity_handling_6() { return &___entity_handling_6; }
	inline void set_entity_handling_6(int32_t value)
	{
		___entity_handling_6 = value;
	}

	inline static int32_t get_offset_of_entity_inside_attr_7() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t2086920314, ___entity_inside_attr_7)); }
	inline bool get_entity_inside_attr_7() const { return ___entity_inside_attr_7; }
	inline bool* get_address_of_entity_inside_attr_7() { return &___entity_inside_attr_7; }
	inline void set_entity_inside_attr_7(bool value)
	{
		___entity_inside_attr_7 = value;
	}

	inline static int32_t get_offset_of_inside_attr_8() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t2086920314, ___inside_attr_8)); }
	inline bool get_inside_attr_8() const { return ___inside_attr_8; }
	inline bool* get_address_of_inside_attr_8() { return &___inside_attr_8; }
	inline void set_inside_attr_8(bool value)
	{
		___inside_attr_8 = value;
	}

	inline static int32_t get_offset_of_do_resolve_9() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t2086920314, ___do_resolve_9)); }
	inline bool get_do_resolve_9() const { return ___do_resolve_9; }
	inline bool* get_address_of_do_resolve_9() { return &___do_resolve_9; }
	inline void set_do_resolve_9(bool value)
	{
		___do_resolve_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYRESOLVINGXMLREADER_T2086920314_H
#ifndef DTDELEMENTDECLARATIONCOLLECTION_T2224069626_H
#define DTDELEMENTDECLARATIONCOLLECTION_T2224069626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclarationCollection
struct  DTDElementDeclarationCollection_t2224069626  : public DTDCollectionBase_t2621362935
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATIONCOLLECTION_T2224069626_H
#ifndef XMLSCHEMAANNOTATED_T2082486936_H
#define XMLSCHEMAANNOTATED_T2082486936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotated
struct  XmlSchemaAnnotated_t2082486936  : public XmlSchemaObject_t2050913741
{
public:
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaAnnotated::annotation
	XmlSchemaAnnotation_t2400301303 * ___annotation_13;
	// System.String System.Xml.Schema.XmlSchemaAnnotated::id
	String_t* ___id_14;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchemaAnnotated::unhandledAttributes
	XmlAttributeU5BU5D_t287209776* ___unhandledAttributes_15;

public:
	inline static int32_t get_offset_of_annotation_13() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t2082486936, ___annotation_13)); }
	inline XmlSchemaAnnotation_t2400301303 * get_annotation_13() const { return ___annotation_13; }
	inline XmlSchemaAnnotation_t2400301303 ** get_address_of_annotation_13() { return &___annotation_13; }
	inline void set_annotation_13(XmlSchemaAnnotation_t2400301303 * value)
	{
		___annotation_13 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_13), value);
	}

	inline static int32_t get_offset_of_id_14() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t2082486936, ___id_14)); }
	inline String_t* get_id_14() const { return ___id_14; }
	inline String_t** get_address_of_id_14() { return &___id_14; }
	inline void set_id_14(String_t* value)
	{
		___id_14 = value;
		Il2CppCodeGenWriteBarrier((&___id_14), value);
	}

	inline static int32_t get_offset_of_unhandledAttributes_15() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t2082486936, ___unhandledAttributes_15)); }
	inline XmlAttributeU5BU5D_t287209776* get_unhandledAttributes_15() const { return ___unhandledAttributes_15; }
	inline XmlAttributeU5BU5D_t287209776** get_address_of_unhandledAttributes_15() { return &___unhandledAttributes_15; }
	inline void set_unhandledAttributes_15(XmlAttributeU5BU5D_t287209776* value)
	{
		___unhandledAttributes_15 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributes_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANNOTATED_T2082486936_H
#ifndef DTDATTLISTDECLARATIONCOLLECTION_T243645429_H
#define DTDATTLISTDECLARATIONCOLLECTION_T243645429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclarationCollection
struct  DTDAttListDeclarationCollection_t243645429  : public DTDCollectionBase_t2621362935
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATIONCOLLECTION_T243645429_H
#ifndef DTDNOTATIONDECLARATIONCOLLECTION_T228085060_H
#define DTDNOTATIONDECLARATIONCOLLECTION_T228085060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclarationCollection
struct  DTDNotationDeclarationCollection_t228085060  : public DTDCollectionBase_t2621362935
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATIONCOLLECTION_T228085060_H
#ifndef DTDENTITYDECLARATIONCOLLECTION_T1212505713_H
#define DTDENTITYDECLARATIONCOLLECTION_T1212505713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclarationCollection
struct  DTDEntityDeclarationCollection_t1212505713  : public DTDCollectionBase_t2621362935
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATIONCOLLECTION_T1212505713_H
#ifndef XMLSCHEMAXPATH_T604820427_H
#define XMLSCHEMAXPATH_T604820427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaXPath
struct  XmlSchemaXPath_t604820427  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaXPath::xpath
	String_t* ___xpath_16;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XmlSchemaXPath::nsmgr
	XmlNamespaceManager_t486731501 * ___nsmgr_17;
	// System.Boolean System.Xml.Schema.XmlSchemaXPath::isSelector
	bool ___isSelector_18;
	// Mono.Xml.Schema.XsdIdentityPath[] System.Xml.Schema.XmlSchemaXPath::compiledExpression
	XsdIdentityPathU5BU5D_t526781831* ___compiledExpression_19;
	// Mono.Xml.Schema.XsdIdentityPath System.Xml.Schema.XmlSchemaXPath::currentPath
	XsdIdentityPath_t2037874 * ___currentPath_20;

public:
	inline static int32_t get_offset_of_xpath_16() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t604820427, ___xpath_16)); }
	inline String_t* get_xpath_16() const { return ___xpath_16; }
	inline String_t** get_address_of_xpath_16() { return &___xpath_16; }
	inline void set_xpath_16(String_t* value)
	{
		___xpath_16 = value;
		Il2CppCodeGenWriteBarrier((&___xpath_16), value);
	}

	inline static int32_t get_offset_of_nsmgr_17() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t604820427, ___nsmgr_17)); }
	inline XmlNamespaceManager_t486731501 * get_nsmgr_17() const { return ___nsmgr_17; }
	inline XmlNamespaceManager_t486731501 ** get_address_of_nsmgr_17() { return &___nsmgr_17; }
	inline void set_nsmgr_17(XmlNamespaceManager_t486731501 * value)
	{
		___nsmgr_17 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_17), value);
	}

	inline static int32_t get_offset_of_isSelector_18() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t604820427, ___isSelector_18)); }
	inline bool get_isSelector_18() const { return ___isSelector_18; }
	inline bool* get_address_of_isSelector_18() { return &___isSelector_18; }
	inline void set_isSelector_18(bool value)
	{
		___isSelector_18 = value;
	}

	inline static int32_t get_offset_of_compiledExpression_19() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t604820427, ___compiledExpression_19)); }
	inline XsdIdentityPathU5BU5D_t526781831* get_compiledExpression_19() const { return ___compiledExpression_19; }
	inline XsdIdentityPathU5BU5D_t526781831** get_address_of_compiledExpression_19() { return &___compiledExpression_19; }
	inline void set_compiledExpression_19(XsdIdentityPathU5BU5D_t526781831* value)
	{
		___compiledExpression_19 = value;
		Il2CppCodeGenWriteBarrier((&___compiledExpression_19), value);
	}

	inline static int32_t get_offset_of_currentPath_20() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t604820427, ___currentPath_20)); }
	inline XsdIdentityPath_t2037874 * get_currentPath_20() const { return ___currentPath_20; }
	inline XsdIdentityPath_t2037874 ** get_address_of_currentPath_20() { return &___currentPath_20; }
	inline void set_currentPath_20(XsdIdentityPath_t2037874 * value)
	{
		___currentPath_20 = value;
		Il2CppCodeGenWriteBarrier((&___currentPath_20), value);
	}
};

struct XmlSchemaXPath_t604820427_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaXPath::<>f__switch$map3C
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map3C_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3C_21() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t604820427_StaticFields, ___U3CU3Ef__switchU24map3C_21)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map3C_21() const { return ___U3CU3Ef__switchU24map3C_21; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map3C_21() { return &___U3CU3Ef__switchU24map3C_21; }
	inline void set_U3CU3Ef__switchU24map3C_21(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map3C_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3C_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAXPATH_T604820427_H
#ifndef XMLSCHEMAFACET_T614309579_H
#define XMLSCHEMAFACET_T614309579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet
struct  XmlSchemaFacet_t614309579  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaFacet::isFixed
	bool ___isFixed_17;
	// System.String System.Xml.Schema.XmlSchemaFacet::val
	String_t* ___val_18;

public:
	inline static int32_t get_offset_of_isFixed_17() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t614309579, ___isFixed_17)); }
	inline bool get_isFixed_17() const { return ___isFixed_17; }
	inline bool* get_address_of_isFixed_17() { return &___isFixed_17; }
	inline void set_isFixed_17(bool value)
	{
		___isFixed_17 = value;
	}

	inline static int32_t get_offset_of_val_18() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t614309579, ___val_18)); }
	inline String_t* get_val_18() const { return ___val_18; }
	inline String_t** get_address_of_val_18() { return &___val_18; }
	inline void set_val_18(String_t* value)
	{
		___val_18 = value;
		Il2CppCodeGenWriteBarrier((&___val_18), value);
	}
};

struct XmlSchemaFacet_t614309579_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaFacet/Facet System.Xml.Schema.XmlSchemaFacet::AllFacets
	int32_t ___AllFacets_16;

public:
	inline static int32_t get_offset_of_AllFacets_16() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t614309579_StaticFields, ___AllFacets_16)); }
	inline int32_t get_AllFacets_16() const { return ___AllFacets_16; }
	inline int32_t* get_address_of_AllFacets_16() { return &___AllFacets_16; }
	inline void set_AllFacets_16(int32_t value)
	{
		___AllFacets_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFACET_T614309579_H
#ifndef XMLSCHEMAWHITESPACEFACET_T2007056012_H
#define XMLSCHEMAWHITESPACEFACET_T2007056012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaWhiteSpaceFacet
struct  XmlSchemaWhiteSpaceFacet_t2007056012  : public XmlSchemaFacet_t614309579
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAWHITESPACEFACET_T2007056012_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (XmlSchemaUse_t3553149267)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[5] = 
{
	XmlSchemaUse_t3553149267::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (XmlSchemaUtil_t3576230726), -1, sizeof(XmlSchemaUtil_t3576230726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1801[10] = 
{
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_StrictMsCompliant_3(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map36_4(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map37_5(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map38_6(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map39_7(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map3A_8(),
	XmlSchemaUtil_t3576230726_StaticFields::get_offset_of_U3CU3Ef__switchU24map3B_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (XmlSchemaValidationException_t2387044366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (XmlSchemaValidationFlags_t910489930)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1803[7] = 
{
	XmlSchemaValidationFlags_t910489930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (XmlSchemaValidator_t1978690704), -1, sizeof(XmlSchemaValidator_t1978690704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1804[26] = 
{
	XmlSchemaValidator_t1978690704_StaticFields::get_offset_of_emptyAttributeArray_0(),
	XmlSchemaValidator_t1978690704::get_offset_of_nominalEventSender_1(),
	XmlSchemaValidator_t1978690704::get_offset_of_lineInfo_2(),
	XmlSchemaValidator_t1978690704::get_offset_of_nsResolver_3(),
	XmlSchemaValidator_t1978690704::get_offset_of_sourceUri_4(),
	XmlSchemaValidator_t1978690704::get_offset_of_nameTable_5(),
	XmlSchemaValidator_t1978690704::get_offset_of_schemas_6(),
	XmlSchemaValidator_t1978690704::get_offset_of_xmlResolver_7(),
	XmlSchemaValidator_t1978690704::get_offset_of_options_8(),
	XmlSchemaValidator_t1978690704::get_offset_of_transition_9(),
	XmlSchemaValidator_t1978690704::get_offset_of_state_10(),
	XmlSchemaValidator_t1978690704::get_offset_of_occuredAtts_11(),
	XmlSchemaValidator_t1978690704::get_offset_of_defaultAttributes_12(),
	XmlSchemaValidator_t1978690704::get_offset_of_defaultAttributesCache_13(),
	XmlSchemaValidator_t1978690704::get_offset_of_idManager_14(),
	XmlSchemaValidator_t1978690704::get_offset_of_keyTables_15(),
	XmlSchemaValidator_t1978690704::get_offset_of_currentKeyFieldConsumers_16(),
	XmlSchemaValidator_t1978690704::get_offset_of_tmpKeyrefPool_17(),
	XmlSchemaValidator_t1978690704::get_offset_of_elementQNameStack_18(),
	XmlSchemaValidator_t1978690704::get_offset_of_storedCharacters_19(),
	XmlSchemaValidator_t1978690704::get_offset_of_shouldValidateCharacters_20(),
	XmlSchemaValidator_t1978690704::get_offset_of_depth_21(),
	XmlSchemaValidator_t1978690704::get_offset_of_xsiNilDepth_22(),
	XmlSchemaValidator_t1978690704::get_offset_of_skipValidationDepth_23(),
	XmlSchemaValidator_t1978690704::get_offset_of_CurrentAttributeType_24(),
	XmlSchemaValidator_t1978690704::get_offset_of_ValidationEventHandler_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (Transition_t34209471)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[5] = 
{
	Transition_t34209471::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (XmlSchemaValidity_t995929432)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1806[4] = 
{
	XmlSchemaValidity_t995929432::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (XmlSchemaWhiteSpaceFacet_t2007056012), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (XmlSchemaXPath_t604820427), -1, sizeof(XmlSchemaXPath_t604820427_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1808[6] = 
{
	XmlSchemaXPath_t604820427::get_offset_of_xpath_16(),
	XmlSchemaXPath_t604820427::get_offset_of_nsmgr_17(),
	XmlSchemaXPath_t604820427::get_offset_of_isSelector_18(),
	XmlSchemaXPath_t604820427::get_offset_of_compiledExpression_19(),
	XmlSchemaXPath_t604820427::get_offset_of_currentPath_20(),
	XmlSchemaXPath_t604820427_StaticFields::get_offset_of_U3CU3Ef__switchU24map3C_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (XmlSeverityType_t3547578624)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1809[3] = 
{
	XmlSeverityType_t3547578624::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (XmlTypeCode_t58293802)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[56] = 
{
	XmlTypeCode_t58293802::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (CodeIdentifier_t3245527920), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (SchemaTypes_t3045759914)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1813[9] = 
{
	SchemaTypes_t3045759914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (TypeData_t3979356678), -1, sizeof(TypeData_t3979356678_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1814[12] = 
{
	TypeData_t3979356678::get_offset_of_type_0(),
	TypeData_t3979356678::get_offset_of_elementName_1(),
	TypeData_t3979356678::get_offset_of_sType_2(),
	TypeData_t3979356678::get_offset_of_listItemType_3(),
	TypeData_t3979356678::get_offset_of_typeName_4(),
	TypeData_t3979356678::get_offset_of_fullTypeName_5(),
	TypeData_t3979356678::get_offset_of_listItemTypeData_6(),
	TypeData_t3979356678::get_offset_of_mappedType_7(),
	TypeData_t3979356678::get_offset_of_facet_8(),
	TypeData_t3979356678::get_offset_of_hasPublicConstructor_9(),
	TypeData_t3979356678::get_offset_of_nullableOverride_10(),
	TypeData_t3979356678_StaticFields::get_offset_of_keywords_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (TypeTranslator_t1077722680), -1, sizeof(TypeTranslator_t1077722680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	TypeTranslator_t1077722680_StaticFields::get_offset_of_nameCache_0(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_primitiveTypes_1(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_primitiveArrayTypes_2(),
	TypeTranslator_t1077722680_StaticFields::get_offset_of_nullableTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (XmlAnyAttributeAttribute_t713947513), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (XmlAnyElementAttribute_t2502375235), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[1] = 
{
	XmlAnyElementAttribute_t2502375235::get_offset_of_order_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (XmlAttributeAttribute_t850813783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[2] = 
{
	XmlAttributeAttribute_t850813783::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_t850813783::get_offset_of_dataType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (XmlElementAttribute_t2182839281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[3] = 
{
	XmlElementAttribute_t2182839281::get_offset_of_elementName_0(),
	XmlElementAttribute_t2182839281::get_offset_of_type_1(),
	XmlElementAttribute_t2182839281::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (XmlEnumAttribute_t919400678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[1] = 
{
	XmlEnumAttribute_t919400678::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (XmlIgnoreAttribute_t2333915871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (XmlNamespaceDeclarationsAttribute_t2069522403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (XmlRootAttribute_t3527426713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[3] = 
{
	XmlRootAttribute_t3527426713::get_offset_of_elementName_0(),
	XmlRootAttribute_t3527426713::get_offset_of_isNullable_1(),
	XmlRootAttribute_t3527426713::get_offset_of_ns_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (XmlSchemaProviderAttribute_t2486667559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[2] = 
{
	XmlSchemaProviderAttribute_t2486667559::get_offset_of__methodName_0(),
	XmlSchemaProviderAttribute_t2486667559::get_offset_of__isAny_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (XmlSerializerNamespaces_t3063656491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[1] = 
{
	XmlSerializerNamespaces_t3063656491::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (XmlTextAttribute_t3321178844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (XPathItem_t3130801258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (ConformanceLevel_t3761201363)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1829[4] = 
{
	ConformanceLevel_t3761201363::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (DTDAutomataFactory_t3605390810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[3] = 
{
	DTDAutomataFactory_t3605390810::get_offset_of_root_0(),
	DTDAutomataFactory_t3605390810::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t3605390810::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (DTDAutomata_t545990600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[1] = 
{
	DTDAutomata_t545990600::get_offset_of_root_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (DTDElementAutomata_t2864881036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[1] = 
{
	DTDElementAutomata_t2864881036::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (DTDChoiceAutomata_t2810241733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[4] = 
{
	DTDChoiceAutomata_t2810241733::get_offset_of_left_1(),
	DTDChoiceAutomata_t2810241733::get_offset_of_right_2(),
	DTDChoiceAutomata_t2810241733::get_offset_of_hasComputedEmptiable_3(),
	DTDChoiceAutomata_t2810241733::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (DTDSequenceAutomata_t1228770437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[4] = 
{
	DTDSequenceAutomata_t1228770437::get_offset_of_left_1(),
	DTDSequenceAutomata_t1228770437::get_offset_of_right_2(),
	DTDSequenceAutomata_t1228770437::get_offset_of_hasComputedEmptiable_3(),
	DTDSequenceAutomata_t1228770437::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (DTDOneOrMoreAutomata_t1559764132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[1] = 
{
	DTDOneOrMoreAutomata_t1559764132::get_offset_of_children_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (DTDEmptyAutomata_t411530619), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (DTDAnyAutomata_t146446906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (DTDInvalidAutomata_t247674167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (DTDObjectModel_t1113953282), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[23] = 
{
	DTDObjectModel_t1113953282::get_offset_of_factory_0(),
	DTDObjectModel_t1113953282::get_offset_of_rootAutomata_1(),
	DTDObjectModel_t1113953282::get_offset_of_emptyAutomata_2(),
	DTDObjectModel_t1113953282::get_offset_of_anyAutomata_3(),
	DTDObjectModel_t1113953282::get_offset_of_invalidAutomata_4(),
	DTDObjectModel_t1113953282::get_offset_of_elementDecls_5(),
	DTDObjectModel_t1113953282::get_offset_of_attListDecls_6(),
	DTDObjectModel_t1113953282::get_offset_of_peDecls_7(),
	DTDObjectModel_t1113953282::get_offset_of_entityDecls_8(),
	DTDObjectModel_t1113953282::get_offset_of_notationDecls_9(),
	DTDObjectModel_t1113953282::get_offset_of_validationErrors_10(),
	DTDObjectModel_t1113953282::get_offset_of_resolver_11(),
	DTDObjectModel_t1113953282::get_offset_of_nameTable_12(),
	DTDObjectModel_t1113953282::get_offset_of_externalResources_13(),
	DTDObjectModel_t1113953282::get_offset_of_baseURI_14(),
	DTDObjectModel_t1113953282::get_offset_of_name_15(),
	DTDObjectModel_t1113953282::get_offset_of_publicId_16(),
	DTDObjectModel_t1113953282::get_offset_of_systemId_17(),
	DTDObjectModel_t1113953282::get_offset_of_intSubset_18(),
	DTDObjectModel_t1113953282::get_offset_of_intSubsetHasPERef_19(),
	DTDObjectModel_t1113953282::get_offset_of_isStandalone_20(),
	DTDObjectModel_t1113953282::get_offset_of_lineNumber_21(),
	DTDObjectModel_t1113953282::get_offset_of_linePosition_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (DictionaryBase_t1005937181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (U3CU3Ec__Iterator3_t3518389200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[5] = 
{
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU24s_431U3E__0_0(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_t3518389200::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (DTDCollectionBase_t2621362935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[1] = 
{
	DTDCollectionBase_t2621362935::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (DTDElementDeclarationCollection_t2224069626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (DTDAttListDeclarationCollection_t243645429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (DTDEntityDeclarationCollection_t1212505713), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (DTDNotationDeclarationCollection_t228085060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (DTDContentModel_t445576364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[7] = 
{
	DTDContentModel_t445576364::get_offset_of_root_5(),
	DTDContentModel_t445576364::get_offset_of_compiledAutomata_6(),
	DTDContentModel_t445576364::get_offset_of_ownerElementName_7(),
	DTDContentModel_t445576364::get_offset_of_elementName_8(),
	DTDContentModel_t445576364::get_offset_of_orderType_9(),
	DTDContentModel_t445576364::get_offset_of_childModels_10(),
	DTDContentModel_t445576364::get_offset_of_occurence_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (DTDContentModelCollection_t3164170484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[1] = 
{
	DTDContentModelCollection_t3164170484::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (DTDNode_t1758286970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[5] = 
{
	DTDNode_t1758286970::get_offset_of_root_0(),
	DTDNode_t1758286970::get_offset_of_isInternalSubset_1(),
	DTDNode_t1758286970::get_offset_of_baseURI_2(),
	DTDNode_t1758286970::get_offset_of_lineNumber_3(),
	DTDNode_t1758286970::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (DTDElementDeclaration_t8748002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[6] = 
{
	DTDElementDeclaration_t8748002::get_offset_of_root_5(),
	DTDElementDeclaration_t8748002::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t8748002::get_offset_of_name_7(),
	DTDElementDeclaration_t8748002::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t8748002::get_offset_of_isAny_9(),
	DTDElementDeclaration_t8748002::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (DTDAttributeDefinition_t3692870749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[7] = 
{
	DTDAttributeDefinition_t3692870749::get_offset_of_name_5(),
	DTDAttributeDefinition_t3692870749::get_offset_of_datatype_6(),
	DTDAttributeDefinition_t3692870749::get_offset_of_enumeratedLiterals_7(),
	DTDAttributeDefinition_t3692870749::get_offset_of_unresolvedDefault_8(),
	DTDAttributeDefinition_t3692870749::get_offset_of_enumeratedNotations_9(),
	DTDAttributeDefinition_t3692870749::get_offset_of_occurenceType_10(),
	DTDAttributeDefinition_t3692870749::get_offset_of_resolvedDefaultValue_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (DTDAttListDeclaration_t2272374839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[3] = 
{
	DTDAttListDeclaration_t2272374839::get_offset_of_name_5(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t2272374839::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (DTDEntityBase_t2353758560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[10] = 
{
	DTDEntityBase_t2353758560::get_offset_of_name_5(),
	DTDEntityBase_t2353758560::get_offset_of_publicId_6(),
	DTDEntityBase_t2353758560::get_offset_of_systemId_7(),
	DTDEntityBase_t2353758560::get_offset_of_literalValue_8(),
	DTDEntityBase_t2353758560::get_offset_of_replacementText_9(),
	DTDEntityBase_t2353758560::get_offset_of_uriString_10(),
	DTDEntityBase_t2353758560::get_offset_of_absUri_11(),
	DTDEntityBase_t2353758560::get_offset_of_isInvalid_12(),
	DTDEntityBase_t2353758560::get_offset_of_loadFailed_13(),
	DTDEntityBase_t2353758560::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (DTDEntityDeclaration_t4283284771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[6] = 
{
	DTDEntityDeclaration_t4283284771::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t4283284771::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t4283284771::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t4283284771::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t4283284771::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t4283284771::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (DTDNotationDeclaration_t1758408116), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[5] = 
{
	DTDNotationDeclaration_t1758408116::get_offset_of_name_5(),
	DTDNotationDeclaration_t1758408116::get_offset_of_localName_6(),
	DTDNotationDeclaration_t1758408116::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t1758408116::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t1758408116::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (DTDParameterEntityDeclarationCollection_t3496720022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[2] = 
{
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_t3496720022::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (DTDParameterEntityDeclaration_t252230634), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (DTDContentOrderType_t3150259539)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1858[4] = 
{
	DTDContentOrderType_t3150259539::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (DTDAttributeOccurenceType_t2819881069)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[5] = 
{
	DTDAttributeOccurenceType_t2819881069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (DTDOccurence_t99371501)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1860[5] = 
{
	DTDOccurence_t99371501::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (DTDReader_t2453137441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[14] = 
{
	DTDReader_t2453137441::get_offset_of_currentInput_0(),
	DTDReader_t2453137441::get_offset_of_parserInputStack_1(),
	DTDReader_t2453137441::get_offset_of_nameBuffer_2(),
	DTDReader_t2453137441::get_offset_of_nameLength_3(),
	DTDReader_t2453137441::get_offset_of_nameCapacity_4(),
	DTDReader_t2453137441::get_offset_of_valueBuffer_5(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t2453137441::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t2453137441::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t2453137441::get_offset_of_normalization_9(),
	DTDReader_t2453137441::get_offset_of_processingInternalSubset_10(),
	DTDReader_t2453137441::get_offset_of_cachedPublicId_11(),
	DTDReader_t2453137441::get_offset_of_cachedSystemId_12(),
	DTDReader_t2453137441::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (DTDValidatingReader_t4120969348), -1, sizeof(DTDValidatingReader_t4120969348_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1862[29] = 
{
	DTDValidatingReader_t4120969348::get_offset_of_reader_2(),
	DTDValidatingReader_t4120969348::get_offset_of_sourceTextReader_3(),
	DTDValidatingReader_t4120969348::get_offset_of_validatingReader_4(),
	DTDValidatingReader_t4120969348::get_offset_of_dtd_5(),
	DTDValidatingReader_t4120969348::get_offset_of_resolver_6(),
	DTDValidatingReader_t4120969348::get_offset_of_currentElement_7(),
	DTDValidatingReader_t4120969348::get_offset_of_attributes_8(),
	DTDValidatingReader_t4120969348::get_offset_of_attributeCount_9(),
	DTDValidatingReader_t4120969348::get_offset_of_currentAttribute_10(),
	DTDValidatingReader_t4120969348::get_offset_of_consumedAttribute_11(),
	DTDValidatingReader_t4120969348::get_offset_of_elementStack_12(),
	DTDValidatingReader_t4120969348::get_offset_of_automataStack_13(),
	DTDValidatingReader_t4120969348::get_offset_of_popScope_14(),
	DTDValidatingReader_t4120969348::get_offset_of_isStandalone_15(),
	DTDValidatingReader_t4120969348::get_offset_of_currentAutomata_16(),
	DTDValidatingReader_t4120969348::get_offset_of_previousAutomata_17(),
	DTDValidatingReader_t4120969348::get_offset_of_idList_18(),
	DTDValidatingReader_t4120969348::get_offset_of_missingIDReferences_19(),
	DTDValidatingReader_t4120969348::get_offset_of_nsmgr_20(),
	DTDValidatingReader_t4120969348::get_offset_of_currentTextValue_21(),
	DTDValidatingReader_t4120969348::get_offset_of_constructingTextValue_22(),
	DTDValidatingReader_t4120969348::get_offset_of_shouldResetCurrentTextValue_23(),
	DTDValidatingReader_t4120969348::get_offset_of_isSignificantWhitespace_24(),
	DTDValidatingReader_t4120969348::get_offset_of_isWhitespace_25(),
	DTDValidatingReader_t4120969348::get_offset_of_isText_26(),
	DTDValidatingReader_t4120969348::get_offset_of_attributeValueEntityStack_27(),
	DTDValidatingReader_t4120969348::get_offset_of_valueBuilder_28(),
	DTDValidatingReader_t4120969348::get_offset_of_whitespaceChars_29(),
	DTDValidatingReader_t4120969348_StaticFields::get_offset_of_U3CU3Ef__switchU24map43_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (AttributeSlot_t1499247213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[6] = 
{
	AttributeSlot_t1499247213::get_offset_of_Name_0(),
	AttributeSlot_t1499247213::get_offset_of_LocalName_1(),
	AttributeSlot_t1499247213::get_offset_of_NS_2(),
	AttributeSlot_t1499247213::get_offset_of_Prefix_3(),
	AttributeSlot_t1499247213::get_offset_of_Value_4(),
	AttributeSlot_t1499247213::get_offset_of_IsDefault_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (EntityHandling_t3960499440)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1864[3] = 
{
	EntityHandling_t3960499440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (EntityResolvingXmlReader_t2086920314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[8] = 
{
	EntityResolvingXmlReader_t2086920314::get_offset_of_entity_2(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_source_3(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_context_4(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_resolver_5(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_entity_handling_6(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_entity_inside_attr_7(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_inside_attr_8(),
	EntityResolvingXmlReader_t2086920314::get_offset_of_do_resolve_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (Formatting_t1126649075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1866[3] = 
{
	Formatting_t1126649075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (NameTable_t594386929), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[3] = 
{
	NameTable_t594386929::get_offset_of_count_0(),
	NameTable_t594386929::get_offset_of_buckets_1(),
	NameTable_t594386929::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (Entry_t2583369454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[4] = 
{
	Entry_t2583369454::get_offset_of_str_0(),
	Entry_t2583369454::get_offset_of_hash_1(),
	Entry_t2583369454::get_offset_of_len_2(),
	Entry_t2583369454::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (NamespaceHandling_t1452270444)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1872[3] = 
{
	NamespaceHandling_t1452270444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (NewLineHandling_t1737195169)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1873[4] = 
{
	NewLineHandling_t1737195169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (ReadState_t3138905245)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1874[6] = 
{
	ReadState_t3138905245::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (ValidationType_t1401987383)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1875[6] = 
{
	ValidationType_t1401987383::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (WhitespaceHandling_t3754063142)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1876[4] = 
{
	WhitespaceHandling_t3754063142::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (WriteState_t1534871862)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1877[8] = 
{
	WriteState_t1534871862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (XQueryConvert_t3510797773), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (XmlAttribute_t175731005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[4] = 
{
	XmlAttribute_t175731005::get_offset_of_name_5(),
	XmlAttribute_t175731005::get_offset_of_isDefault_6(),
	XmlAttribute_t175731005::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t175731005::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (XmlAttributeCollection_t3359885287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[2] = 
{
	XmlAttributeCollection_t3359885287::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t3359885287::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (XmlCDataSection_t1124775823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (XmlChar_t1369421061), -1, sizeof(XmlChar_t1369421061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1882[5] = 
{
	XmlChar_t1369421061_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t1369421061_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t1369421061_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t1369421061_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t1369421061_StaticFields::get_offset_of_U3CU3Ef__switchU24map47_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (XmlCharacterData_t575748506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[1] = 
{
	XmlCharacterData_t575748506::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (XmlComment_t3999331572), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (XmlConvert_t1936105738), -1, sizeof(XmlConvert_t1936105738_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1885[8] = 
{
	XmlConvert_t1936105738_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t1936105738_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t1936105738_StaticFields::get_offset_of_U3CU3Ef__switchU24map49_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (XmlDateTimeSerializationMode_t137774893)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1886[5] = 
{
	XmlDateTimeSerializationMode_t137774893::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (XmlDeclaration_t1545359137), -1, sizeof(XmlDeclaration_t1545359137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1887[4] = 
{
	XmlDeclaration_t1545359137::get_offset_of_encoding_6(),
	XmlDeclaration_t1545359137::get_offset_of_standalone_7(),
	XmlDeclaration_t1545359137::get_offset_of_version_8(),
	XmlDeclaration_t1545359137_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (XmlDocument_t3649534162), -1, sizeof(XmlDocument_t3649534162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1888[19] = 
{
	XmlDocument_t3649534162_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_element_6(),
	XmlDocument_t3649534162::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t3649534162::get_offset_of_nameTable_8(),
	XmlDocument_t3649534162::get_offset_of_baseURI_9(),
	XmlDocument_t3649534162::get_offset_of_implementation_10(),
	XmlDocument_t3649534162::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t3649534162::get_offset_of_resolver_12(),
	XmlDocument_t3649534162::get_offset_of_idTable_13(),
	XmlDocument_t3649534162::get_offset_of_nameCache_14(),
	XmlDocument_t3649534162::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t3649534162::get_offset_of_schemaInfo_16(),
	XmlDocument_t3649534162::get_offset_of_loadMode_17(),
	XmlDocument_t3649534162::get_offset_of_NodeChanged_18(),
	XmlDocument_t3649534162::get_offset_of_NodeChanging_19(),
	XmlDocument_t3649534162::get_offset_of_NodeInserted_20(),
	XmlDocument_t3649534162::get_offset_of_NodeInserting_21(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoved_22(),
	XmlDocument_t3649534162::get_offset_of_NodeRemoving_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (XmlDocumentFragment_t3083262362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[1] = 
{
	XmlDocumentFragment_t3083262362::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (XmlDocumentType_t824160610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[3] = 
{
	XmlDocumentType_t824160610::get_offset_of_entities_6(),
	XmlDocumentType_t824160610::get_offset_of_notations_7(),
	XmlDocumentType_t824160610::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (XmlElement_t2877111883), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[5] = 
{
	XmlElement_t2877111883::get_offset_of_attributes_6(),
	XmlElement_t2877111883::get_offset_of_name_7(),
	XmlElement_t2877111883::get_offset_of_lastLinkedChild_8(),
	XmlElement_t2877111883::get_offset_of_isNotEmpty_9(),
	XmlElement_t2877111883::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (XmlEntity_t4027255380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[7] = 
{
	XmlEntity_t4027255380::get_offset_of_name_5(),
	XmlEntity_t4027255380::get_offset_of_NDATA_6(),
	XmlEntity_t4027255380::get_offset_of_publicId_7(),
	XmlEntity_t4027255380::get_offset_of_systemId_8(),
	XmlEntity_t4027255380::get_offset_of_baseUri_9(),
	XmlEntity_t4027255380::get_offset_of_lastLinkedChild_10(),
	XmlEntity_t4027255380::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (XmlEntityReference_t3053868353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[2] = 
{
	XmlEntityReference_t3053868353::get_offset_of_entityName_6(),
	XmlEntityReference_t3053868353::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (XmlException_t4188277960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[5] = 
{
	XmlException_t4188277960::get_offset_of_lineNumber_11(),
	XmlException_t4188277960::get_offset_of_linePosition_12(),
	XmlException_t4188277960::get_offset_of_sourceUri_13(),
	XmlException_t4188277960::get_offset_of_res_14(),
	XmlException_t4188277960::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (XmlImplementation_t1664517635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[1] = 
{
	XmlImplementation_t1664517635::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (XmlStreamReader_t2725532304), -1, sizeof(XmlStreamReader_t2725532304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1896[2] = 
{
	XmlStreamReader_t2725532304::get_offset_of_input_12(),
	XmlStreamReader_t2725532304_StaticFields::get_offset_of_invalidDataException_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (NonBlockingStreamReader_t3963211903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[11] = 
{
	NonBlockingStreamReader_t3963211903::get_offset_of_input_buffer_1(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_buffer_2(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoded_count_3(),
	NonBlockingStreamReader_t3963211903::get_offset_of_pos_4(),
	NonBlockingStreamReader_t3963211903::get_offset_of_buffer_size_5(),
	NonBlockingStreamReader_t3963211903::get_offset_of_encoding_6(),
	NonBlockingStreamReader_t3963211903::get_offset_of_decoder_7(),
	NonBlockingStreamReader_t3963211903::get_offset_of_base_stream_8(),
	NonBlockingStreamReader_t3963211903::get_offset_of_mayBlock_9(),
	NonBlockingStreamReader_t3963211903::get_offset_of_line_builder_10(),
	NonBlockingStreamReader_t3963211903::get_offset_of_foundCR_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (XmlInputStream_t2650744719), -1, sizeof(XmlInputStream_t2650744719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1898[7] = 
{
	XmlInputStream_t2650744719_StaticFields::get_offset_of_StrictUTF8_1(),
	XmlInputStream_t2650744719::get_offset_of_enc_2(),
	XmlInputStream_t2650744719::get_offset_of_stream_3(),
	XmlInputStream_t2650744719::get_offset_of_buffer_4(),
	XmlInputStream_t2650744719::get_offset_of_bufLength_5(),
	XmlInputStream_t2650744719::get_offset_of_bufPos_6(),
	XmlInputStream_t2650744719_StaticFields::get_offset_of_encodingException_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (XmlLinkedNode_t1287616130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[1] = 
{
	XmlLinkedNode_t1287616130::get_offset_of_nextSibling_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
