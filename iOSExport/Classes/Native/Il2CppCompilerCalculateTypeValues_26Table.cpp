﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.Boolean>
struct Func_2_t1707686766;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>
struct Func_2_t4206299577;
// System.Func`2<System.String,System.String>
struct Func_2_t193026957;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>>
struct Comparison_1_t2963083568;
// System.Collections.Generic.LinkedList`1<System.Action>
struct LinkedList_1_t3531179981;
// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>>
struct Dictionary_2_t2482194404;
// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs>
struct FastAction_2_t2006617618;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Material>
struct FastAction_2_t2217200428;
// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset>
struct FastAction_2_t258946184;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Object>
struct FastAction_2_t3045095618;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro>
struct FastAction_2_t250360562;
// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material>
struct FastAction_3_t260425107;
// TMPro.FastAction`1<System.Boolean>
struct FastAction_1_t3316653872;
// TMPro.FastAction`1<TMPro.TMP_ColorGradient>
struct FastAction_1_t650916501;
// TMPro.FastAction
struct FastAction_t1302743882;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI>
struct FastAction_2_t2957650684;
// TMPro.FastAction`1<UnityEngine.Object>
struct FastAction_1_t512681271;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material>
struct Dictionary_2_t3496499858;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset>
struct Dictionary_2_t1538245614;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset>
struct Dictionary_2_t1649638728;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_ColorGradient>
struct Dictionary_2_t167662982;
// System.String
struct String_t;
// Amazon.Runtime.Internal.Auth.AWS4Signer
struct AWS4Signer_t1011449585;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// Amazon.Runtime.IPipelineHandler
struct IPipelineHandler_t2320756001;
// Amazon.Runtime.RequestEventHandler
struct RequestEventHandler_t2213783891;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_t1230945235;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// ThirdParty.Ionic.Zlib.CrcCalculatorStream
struct CrcCalculatorStream_t2228597532;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// Amazon.Runtime.Internal.Util.CachingWrapperStream
struct CachingWrapperStream_t380166576;
// System.Collections.Generic.List`1<TMPro.KerningPair>
struct List_1_t946875054;
// System.Func`2<TMPro.KerningPair,System.UInt32>
struct Func_2_t2782326444;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// Amazon.Runtime.Internal.Util.LruCache`2<System.String,Amazon.RegionEndpoint>
struct LruCache_2_t3782913914;
// TMPro.TMP_Text
struct TMP_Text_t1920000777;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2828559218;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t1159837347;
// Amazon.S3.S3Region
struct S3Region_t723338532;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t3932231376;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t2530419979;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t2641813093;
// UnityEngine.Material
struct Material_t193706927;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t627890505;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// Amazon.S3.EncodingType
struct EncodingType_t159362831;
// Amazon.S3.RequestPayer
struct RequestPayer_t3334661492;
// Amazon.S3.Model.HeadersCollection
struct HeadersCollection_t2711555648;
// System.IO.Stream
struct Stream_t3255436806;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t1615060493;
// Amazon.Runtime.ResponseMetadata
struct ResponseMetadata_t527027456;
// TMPro.TMP_TextElement
struct TMP_TextElement_t2285620223;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// Amazon.Runtime.Internal.CapacityManager
struct CapacityManager_t2181902141;
// Amazon.Runtime.Internal.RetryCapacity
struct RetryCapacity_t2794668894;
// System.Collections.Generic.ICollection`1<System.Net.HttpStatusCode>
struct ICollection_1_t2850484946;
// System.Collections.Generic.ICollection`1<System.Net.WebExceptionStatus>
struct ICollection_1_t2121448836;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t1578612233;
// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>
struct HashSet_1_t3367932747;
// System.IO.StreamReader
struct StreamReader_t2360341767;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t3116948387;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;
// System.Exception
struct Exception_t1927440687;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t2255878531;
// System.Collections.Generic.List`1<Amazon.S3.Model.S3Object>
struct List_1_t1205837151;
// System.Collections.Generic.List`1<Amazon.S3.Model.DeletedObject>
struct List_1_t2033343350;
// System.Collections.Generic.List`1<Amazon.S3.Model.DeleteError>
struct List_1_t2768982555;
// Amazon.S3.RequestCharged
struct RequestCharged_t2438105727;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t2849466151;
// Amazon.S3.Model.Owner
struct Owner_t97740679;
// Amazon.S3.S3StorageClass
struct S3StorageClass_t454477475;
// Amazon.S3.Model.ByteRange
struct ByteRange_t286214923;
// Amazon.S3.Model.ResponseHeaderOverrides
struct ResponseHeaderOverrides_t382636191;
// Amazon.S3.ServerSideEncryptionCustomerMethod
struct ServerSideEncryptionCustomerMethod_t3201425490;
// Amazon.S3.Model.Expiration
struct Expiration_t12298299;
// Amazon.S3.ServerSideEncryptionMethod
struct ServerSideEncryptionMethod_t608782770;
// Amazon.S3.Model.MetadataCollection
struct MetadataCollection_t3745113723;
// Amazon.S3.ReplicationStatus
struct ReplicationStatus_t1991002226;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// TMPro.TextMeshPro
struct TextMeshPro_t2521834357;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;
// UnityEngine.Transform
struct Transform_t3275118058;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2347923044;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t573465953;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t602810366;
// TMPro.TMP_Glyph
struct TMP_Glyph_t909793902;
// TMPro.TMP_SubMeshUI[]
struct TMP_SubMeshUIU5BU5D_t2798123870;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// TMPro.TMP_SubMesh[]
struct TMP_SubMeshU5BU5D_t4206981150;




#ifndef U3CMODULEU3E_T3783534238_H
#define U3CMODULEU3E_T3783534238_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534238 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534238_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_T3596783049_H
#define U3CU3EC_T3596783049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.AmazonS3PostMarshallHandler/<>c
struct  U3CU3Ec_t3596783049  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3596783049_StaticFields
{
public:
	// Amazon.S3.Internal.AmazonS3PostMarshallHandler/<>c Amazon.S3.Internal.AmazonS3PostMarshallHandler/<>c::<>9
	U3CU3Ec_t3596783049 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.Boolean> Amazon.S3.Internal.AmazonS3PostMarshallHandler/<>c::<>9__8_0
	Func_2_t1707686766 * ___U3CU3E9__8_0_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String> Amazon.S3.Internal.AmazonS3PostMarshallHandler/<>c::<>9__8_1
	Func_2_t4206299577 * ___U3CU3E9__8_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3596783049_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3596783049 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3596783049 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3596783049 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3596783049_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t1707686766 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t1707686766 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t1707686766 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3596783049_StaticFields, ___U3CU3E9__8_1_2)); }
	inline Func_2_t4206299577 * get_U3CU3E9__8_1_2() const { return ___U3CU3E9__8_1_2; }
	inline Func_2_t4206299577 ** get_address_of_U3CU3E9__8_1_2() { return &___U3CU3E9__8_1_2; }
	inline void set_U3CU3E9__8_1_2(Func_2_t4206299577 * value)
	{
		___U3CU3E9__8_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3596783049_H
#ifndef U3CU3EC_T4103665279_H
#define U3CU3EC_T4103665279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.S3Signer/<>c
struct  U3CU3Ec_t4103665279  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4103665279_StaticFields
{
public:
	// Amazon.S3.Internal.S3Signer/<>c Amazon.S3.Internal.S3Signer/<>c::<>9
	U3CU3Ec_t4103665279 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> Amazon.S3.Internal.S3Signer/<>c::<>9__7_0
	Func_2_t193026957 * ___U3CU3E9__7_0_1;
	// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.String>> Amazon.S3.Internal.S3Signer/<>c::<>9__10_0
	Comparison_1_t2963083568 * ___U3CU3E9__10_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4103665279_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4103665279 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4103665279 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4103665279 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4103665279_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_2_t193026957 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_2_t193026957 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_2_t193026957 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4103665279_StaticFields, ___U3CU3E9__10_0_2)); }
	inline Comparison_1_t2963083568 * get_U3CU3E9__10_0_2() const { return ___U3CU3E9__10_0_2; }
	inline Comparison_1_t2963083568 ** get_address_of_U3CU3E9__10_0_2() { return &___U3CU3E9__10_0_2; }
	inline void set_U3CU3E9__10_0_2(Comparison_1_t2963083568 * value)
	{
		___U3CU3E9__10_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4103665279_H
#ifndef FASTACTION_T1302743882_H
#define FASTACTION_T1302743882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FastAction
struct  FastAction_t1302743882  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<System.Action> TMPro.FastAction::delegates
	LinkedList_1_t3531179981 * ___delegates_0;
	// System.Collections.Generic.Dictionary`2<System.Action,System.Collections.Generic.LinkedListNode`1<System.Action>> TMPro.FastAction::lookup
	Dictionary_2_t2482194404 * ___lookup_1;

public:
	inline static int32_t get_offset_of_delegates_0() { return static_cast<int32_t>(offsetof(FastAction_t1302743882, ___delegates_0)); }
	inline LinkedList_1_t3531179981 * get_delegates_0() const { return ___delegates_0; }
	inline LinkedList_1_t3531179981 ** get_address_of_delegates_0() { return &___delegates_0; }
	inline void set_delegates_0(LinkedList_1_t3531179981 * value)
	{
		___delegates_0 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_0), value);
	}

	inline static int32_t get_offset_of_lookup_1() { return static_cast<int32_t>(offsetof(FastAction_t1302743882, ___lookup_1)); }
	inline Dictionary_2_t2482194404 * get_lookup_1() const { return ___lookup_1; }
	inline Dictionary_2_t2482194404 ** get_address_of_lookup_1() { return &___lookup_1; }
	inline void set_lookup_1(Dictionary_2_t2482194404 * value)
	{
		___lookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTACTION_T1302743882_H
#ifndef TMPRO_EXTENSIONMETHODS_T3178223924_H
#define TMPRO_EXTENSIONMETHODS_T3178223924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_ExtensionMethods
struct  TMPro_ExtensionMethods_t3178223924  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EXTENSIONMETHODS_T3178223924_H
#ifndef TMPRO_EVENTMANAGER_T528455910_H
#define TMPRO_EVENTMANAGER_T528455910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_EventManager
struct  TMPro_EventManager_t528455910  : public RuntimeObject
{
public:

public:
};

struct TMPro_EventManager_t528455910_StaticFields
{
public:
	// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs> TMPro.TMPro_EventManager::COMPUTE_DT_EVENT
	FastAction_2_t2006617618 * ___COMPUTE_DT_EVENT_0;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Material> TMPro.TMPro_EventManager::MATERIAL_PROPERTY_EVENT
	FastAction_2_t2217200428 * ___MATERIAL_PROPERTY_EVENT_1;
	// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset> TMPro.TMPro_EventManager::FONT_PROPERTY_EVENT
	FastAction_2_t258946184 * ___FONT_PROPERTY_EVENT_2;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Object> TMPro.TMPro_EventManager::SPRITE_ASSET_PROPERTY_EVENT
	FastAction_2_t3045095618 * ___SPRITE_ASSET_PROPERTY_EVENT_3;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro> TMPro.TMPro_EventManager::TEXTMESHPRO_PROPERTY_EVENT
	FastAction_2_t250360562 * ___TEXTMESHPRO_PROPERTY_EVENT_4;
	// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material> TMPro.TMPro_EventManager::DRAG_AND_DROP_MATERIAL_EVENT
	FastAction_3_t260425107 * ___DRAG_AND_DROP_MATERIAL_EVENT_5;
	// TMPro.FastAction`1<System.Boolean> TMPro.TMPro_EventManager::TEXT_STYLE_PROPERTY_EVENT
	FastAction_1_t3316653872 * ___TEXT_STYLE_PROPERTY_EVENT_6;
	// TMPro.FastAction`1<TMPro.TMP_ColorGradient> TMPro.TMPro_EventManager::COLOR_GRADIENT_PROPERTY_EVENT
	FastAction_1_t650916501 * ___COLOR_GRADIENT_PROPERTY_EVENT_7;
	// TMPro.FastAction TMPro.TMPro_EventManager::TMP_SETTINGS_PROPERTY_EVENT
	FastAction_t1302743882 * ___TMP_SETTINGS_PROPERTY_EVENT_8;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI> TMPro.TMPro_EventManager::TEXTMESHPRO_UGUI_PROPERTY_EVENT
	FastAction_2_t2957650684 * ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9;
	// TMPro.FastAction TMPro.TMPro_EventManager::OnPreRenderObject_Event
	FastAction_t1302743882 * ___OnPreRenderObject_Event_10;
	// TMPro.FastAction`1<UnityEngine.Object> TMPro.TMPro_EventManager::TEXT_CHANGED_EVENT
	FastAction_1_t512681271 * ___TEXT_CHANGED_EVENT_11;

public:
	inline static int32_t get_offset_of_COMPUTE_DT_EVENT_0() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___COMPUTE_DT_EVENT_0)); }
	inline FastAction_2_t2006617618 * get_COMPUTE_DT_EVENT_0() const { return ___COMPUTE_DT_EVENT_0; }
	inline FastAction_2_t2006617618 ** get_address_of_COMPUTE_DT_EVENT_0() { return &___COMPUTE_DT_EVENT_0; }
	inline void set_COMPUTE_DT_EVENT_0(FastAction_2_t2006617618 * value)
	{
		___COMPUTE_DT_EVENT_0 = value;
		Il2CppCodeGenWriteBarrier((&___COMPUTE_DT_EVENT_0), value);
	}

	inline static int32_t get_offset_of_MATERIAL_PROPERTY_EVENT_1() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___MATERIAL_PROPERTY_EVENT_1)); }
	inline FastAction_2_t2217200428 * get_MATERIAL_PROPERTY_EVENT_1() const { return ___MATERIAL_PROPERTY_EVENT_1; }
	inline FastAction_2_t2217200428 ** get_address_of_MATERIAL_PROPERTY_EVENT_1() { return &___MATERIAL_PROPERTY_EVENT_1; }
	inline void set_MATERIAL_PROPERTY_EVENT_1(FastAction_2_t2217200428 * value)
	{
		___MATERIAL_PROPERTY_EVENT_1 = value;
		Il2CppCodeGenWriteBarrier((&___MATERIAL_PROPERTY_EVENT_1), value);
	}

	inline static int32_t get_offset_of_FONT_PROPERTY_EVENT_2() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___FONT_PROPERTY_EVENT_2)); }
	inline FastAction_2_t258946184 * get_FONT_PROPERTY_EVENT_2() const { return ___FONT_PROPERTY_EVENT_2; }
	inline FastAction_2_t258946184 ** get_address_of_FONT_PROPERTY_EVENT_2() { return &___FONT_PROPERTY_EVENT_2; }
	inline void set_FONT_PROPERTY_EVENT_2(FastAction_2_t258946184 * value)
	{
		___FONT_PROPERTY_EVENT_2 = value;
		Il2CppCodeGenWriteBarrier((&___FONT_PROPERTY_EVENT_2), value);
	}

	inline static int32_t get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___SPRITE_ASSET_PROPERTY_EVENT_3)); }
	inline FastAction_2_t3045095618 * get_SPRITE_ASSET_PROPERTY_EVENT_3() const { return ___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline FastAction_2_t3045095618 ** get_address_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return &___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline void set_SPRITE_ASSET_PROPERTY_EVENT_3(FastAction_2_t3045095618 * value)
	{
		___SPRITE_ASSET_PROPERTY_EVENT_3 = value;
		Il2CppCodeGenWriteBarrier((&___SPRITE_ASSET_PROPERTY_EVENT_3), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___TEXTMESHPRO_PROPERTY_EVENT_4)); }
	inline FastAction_2_t250360562 * get_TEXTMESHPRO_PROPERTY_EVENT_4() const { return ___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline FastAction_2_t250360562 ** get_address_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return &___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline void set_TEXTMESHPRO_PROPERTY_EVENT_4(FastAction_2_t250360562 * value)
	{
		___TEXTMESHPRO_PROPERTY_EVENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_PROPERTY_EVENT_4), value);
	}

	inline static int32_t get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___DRAG_AND_DROP_MATERIAL_EVENT_5)); }
	inline FastAction_3_t260425107 * get_DRAG_AND_DROP_MATERIAL_EVENT_5() const { return ___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline FastAction_3_t260425107 ** get_address_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return &___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline void set_DRAG_AND_DROP_MATERIAL_EVENT_5(FastAction_3_t260425107 * value)
	{
		___DRAG_AND_DROP_MATERIAL_EVENT_5 = value;
		Il2CppCodeGenWriteBarrier((&___DRAG_AND_DROP_MATERIAL_EVENT_5), value);
	}

	inline static int32_t get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___TEXT_STYLE_PROPERTY_EVENT_6)); }
	inline FastAction_1_t3316653872 * get_TEXT_STYLE_PROPERTY_EVENT_6() const { return ___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline FastAction_1_t3316653872 ** get_address_of_TEXT_STYLE_PROPERTY_EVENT_6() { return &___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline void set_TEXT_STYLE_PROPERTY_EVENT_6(FastAction_1_t3316653872 * value)
	{
		___TEXT_STYLE_PROPERTY_EVENT_6 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_STYLE_PROPERTY_EVENT_6), value);
	}

	inline static int32_t get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___COLOR_GRADIENT_PROPERTY_EVENT_7)); }
	inline FastAction_1_t650916501 * get_COLOR_GRADIENT_PROPERTY_EVENT_7() const { return ___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline FastAction_1_t650916501 ** get_address_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return &___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline void set_COLOR_GRADIENT_PROPERTY_EVENT_7(FastAction_1_t650916501 * value)
	{
		___COLOR_GRADIENT_PROPERTY_EVENT_7 = value;
		Il2CppCodeGenWriteBarrier((&___COLOR_GRADIENT_PROPERTY_EVENT_7), value);
	}

	inline static int32_t get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___TMP_SETTINGS_PROPERTY_EVENT_8)); }
	inline FastAction_t1302743882 * get_TMP_SETTINGS_PROPERTY_EVENT_8() const { return ___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline FastAction_t1302743882 ** get_address_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return &___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline void set_TMP_SETTINGS_PROPERTY_EVENT_8(FastAction_t1302743882 * value)
	{
		___TMP_SETTINGS_PROPERTY_EVENT_8 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_SETTINGS_PROPERTY_EVENT_8), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9)); }
	inline FastAction_2_t2957650684 * get_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9() const { return ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9; }
	inline FastAction_2_t2957650684 ** get_address_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9() { return &___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9; }
	inline void set_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9(FastAction_2_t2957650684 * value)
	{
		___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_UGUI_PROPERTY_EVENT_9), value);
	}

	inline static int32_t get_offset_of_OnPreRenderObject_Event_10() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___OnPreRenderObject_Event_10)); }
	inline FastAction_t1302743882 * get_OnPreRenderObject_Event_10() const { return ___OnPreRenderObject_Event_10; }
	inline FastAction_t1302743882 ** get_address_of_OnPreRenderObject_Event_10() { return &___OnPreRenderObject_Event_10; }
	inline void set_OnPreRenderObject_Event_10(FastAction_t1302743882 * value)
	{
		___OnPreRenderObject_Event_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnPreRenderObject_Event_10), value);
	}

	inline static int32_t get_offset_of_TEXT_CHANGED_EVENT_11() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t528455910_StaticFields, ___TEXT_CHANGED_EVENT_11)); }
	inline FastAction_1_t512681271 * get_TEXT_CHANGED_EVENT_11() const { return ___TEXT_CHANGED_EVENT_11; }
	inline FastAction_1_t512681271 ** get_address_of_TEXT_CHANGED_EVENT_11() { return &___TEXT_CHANGED_EVENT_11; }
	inline void set_TEXT_CHANGED_EVENT_11(FastAction_1_t512681271 * value)
	{
		___TEXT_CHANGED_EVENT_11 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_CHANGED_EVENT_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EVENTMANAGER_T528455910_H
#ifndef MATERIALREFERENCEMANAGER_T1374850133_H
#define MATERIALREFERENCEMANAGER_T1374850133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReferenceManager
struct  MaterialReferenceManager_t1374850133  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.Material> TMPro.MaterialReferenceManager::m_FontMaterialReferenceLookup
	Dictionary_2_t3496499858 * ___m_FontMaterialReferenceLookup_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_FontAsset> TMPro.MaterialReferenceManager::m_FontAssetReferenceLookup
	Dictionary_2_t1538245614 * ___m_FontAssetReferenceLookup_2;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_SpriteAsset> TMPro.MaterialReferenceManager::m_SpriteAssetReferenceLookup
	Dictionary_2_t1649638728 * ___m_SpriteAssetReferenceLookup_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_ColorGradient> TMPro.MaterialReferenceManager::m_ColorGradientReferenceLookup
	Dictionary_2_t167662982 * ___m_ColorGradientReferenceLookup_4;

public:
	inline static int32_t get_offset_of_m_FontMaterialReferenceLookup_1() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t1374850133, ___m_FontMaterialReferenceLookup_1)); }
	inline Dictionary_2_t3496499858 * get_m_FontMaterialReferenceLookup_1() const { return ___m_FontMaterialReferenceLookup_1; }
	inline Dictionary_2_t3496499858 ** get_address_of_m_FontMaterialReferenceLookup_1() { return &___m_FontMaterialReferenceLookup_1; }
	inline void set_m_FontMaterialReferenceLookup_1(Dictionary_2_t3496499858 * value)
	{
		___m_FontMaterialReferenceLookup_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontMaterialReferenceLookup_1), value);
	}

	inline static int32_t get_offset_of_m_FontAssetReferenceLookup_2() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t1374850133, ___m_FontAssetReferenceLookup_2)); }
	inline Dictionary_2_t1538245614 * get_m_FontAssetReferenceLookup_2() const { return ___m_FontAssetReferenceLookup_2; }
	inline Dictionary_2_t1538245614 ** get_address_of_m_FontAssetReferenceLookup_2() { return &___m_FontAssetReferenceLookup_2; }
	inline void set_m_FontAssetReferenceLookup_2(Dictionary_2_t1538245614 * value)
	{
		___m_FontAssetReferenceLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontAssetReferenceLookup_2), value);
	}

	inline static int32_t get_offset_of_m_SpriteAssetReferenceLookup_3() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t1374850133, ___m_SpriteAssetReferenceLookup_3)); }
	inline Dictionary_2_t1649638728 * get_m_SpriteAssetReferenceLookup_3() const { return ___m_SpriteAssetReferenceLookup_3; }
	inline Dictionary_2_t1649638728 ** get_address_of_m_SpriteAssetReferenceLookup_3() { return &___m_SpriteAssetReferenceLookup_3; }
	inline void set_m_SpriteAssetReferenceLookup_3(Dictionary_2_t1649638728 * value)
	{
		___m_SpriteAssetReferenceLookup_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpriteAssetReferenceLookup_3), value);
	}

	inline static int32_t get_offset_of_m_ColorGradientReferenceLookup_4() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t1374850133, ___m_ColorGradientReferenceLookup_4)); }
	inline Dictionary_2_t167662982 * get_m_ColorGradientReferenceLookup_4() const { return ___m_ColorGradientReferenceLookup_4; }
	inline Dictionary_2_t167662982 ** get_address_of_m_ColorGradientReferenceLookup_4() { return &___m_ColorGradientReferenceLookup_4; }
	inline void set_m_ColorGradientReferenceLookup_4(Dictionary_2_t167662982 * value)
	{
		___m_ColorGradientReferenceLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorGradientReferenceLookup_4), value);
	}
};

struct MaterialReferenceManager_t1374850133_StaticFields
{
public:
	// TMPro.MaterialReferenceManager TMPro.MaterialReferenceManager::s_Instance
	MaterialReferenceManager_t1374850133 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(MaterialReferenceManager_t1374850133_StaticFields, ___s_Instance_0)); }
	inline MaterialReferenceManager_t1374850133 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline MaterialReferenceManager_t1374850133 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(MaterialReferenceManager_t1374850133 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALREFERENCEMANAGER_T1374850133_H
#ifndef LISTOBJECTSREQUESTMARSHALLER_T3330065254_H
#define LISTOBJECTSREQUESTMARSHALLER_T3330065254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.ListObjectsRequestMarshaller
struct  ListObjectsRequestMarshaller_t3330065254  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTOBJECTSREQUESTMARSHALLER_T3330065254_H
#ifndef GETOBJECTREQUESTMARSHALLER_T1832281661_H
#define GETOBJECTREQUESTMARSHALLER_T1832281661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.GetObjectRequestMarshaller
struct  GetObjectRequestMarshaller_t1832281661  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETOBJECTREQUESTMARSHALLER_T1832281661_H
#ifndef CONTENTSITEMUNMARSHALLER_T2864009311_H
#define CONTENTSITEMUNMARSHALLER_T2864009311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.ContentsItemUnmarshaller
struct  ContentsItemUnmarshaller_t2864009311  : public RuntimeObject
{
public:

public:
};

struct ContentsItemUnmarshaller_t2864009311_StaticFields
{
public:
	// Amazon.S3.Model.Internal.MarshallTransformations.ContentsItemUnmarshaller Amazon.S3.Model.Internal.MarshallTransformations.ContentsItemUnmarshaller::_instance
	ContentsItemUnmarshaller_t2864009311 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(ContentsItemUnmarshaller_t2864009311_StaticFields, ____instance_0)); }
	inline ContentsItemUnmarshaller_t2864009311 * get__instance_0() const { return ____instance_0; }
	inline ContentsItemUnmarshaller_t2864009311 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(ContentsItemUnmarshaller_t2864009311 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTSITEMUNMARSHALLER_T2864009311_H
#ifndef OWNERUNMARSHALLER_T3479873017_H
#define OWNERUNMARSHALLER_T3479873017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.OwnerUnmarshaller
struct  OwnerUnmarshaller_t3479873017  : public RuntimeObject
{
public:

public:
};

struct OwnerUnmarshaller_t3479873017_StaticFields
{
public:
	// Amazon.S3.Model.Internal.MarshallTransformations.OwnerUnmarshaller Amazon.S3.Model.Internal.MarshallTransformations.OwnerUnmarshaller::_instance
	OwnerUnmarshaller_t3479873017 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(OwnerUnmarshaller_t3479873017_StaticFields, ____instance_0)); }
	inline OwnerUnmarshaller_t3479873017 * get__instance_0() const { return ____instance_0; }
	inline OwnerUnmarshaller_t3479873017 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(OwnerUnmarshaller_t3479873017 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OWNERUNMARSHALLER_T3479873017_H
#ifndef COMMONPREFIXESITEMUNMARSHALLER_T2768491974_H
#define COMMONPREFIXESITEMUNMARSHALLER_T2768491974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.CommonPrefixesItemUnmarshaller
struct  CommonPrefixesItemUnmarshaller_t2768491974  : public RuntimeObject
{
public:

public:
};

struct CommonPrefixesItemUnmarshaller_t2768491974_StaticFields
{
public:
	// Amazon.S3.Model.Internal.MarshallTransformations.CommonPrefixesItemUnmarshaller Amazon.S3.Model.Internal.MarshallTransformations.CommonPrefixesItemUnmarshaller::_instance
	CommonPrefixesItemUnmarshaller_t2768491974 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(CommonPrefixesItemUnmarshaller_t2768491974_StaticFields, ____instance_0)); }
	inline CommonPrefixesItemUnmarshaller_t2768491974 * get__instance_0() const { return ____instance_0; }
	inline CommonPrefixesItemUnmarshaller_t2768491974 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(CommonPrefixesItemUnmarshaller_t2768491974 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMONPREFIXESITEMUNMARSHALLER_T2768491974_H
#ifndef S3TRANSFORMS_T1521396015_H
#define S3TRANSFORMS_T1521396015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.S3Transforms
struct  S3Transforms_t1521396015  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3TRANSFORMS_T1521396015_H
#ifndef S3ERRORRESPONSEUNMARSHALLER_T140617651_H
#define S3ERRORRESPONSEUNMARSHALLER_T140617651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.S3ErrorResponseUnmarshaller
struct  S3ErrorResponseUnmarshaller_t140617651  : public RuntimeObject
{
public:

public:
};

struct S3ErrorResponseUnmarshaller_t140617651_StaticFields
{
public:
	// Amazon.S3.Model.Internal.MarshallTransformations.S3ErrorResponseUnmarshaller Amazon.S3.Model.Internal.MarshallTransformations.S3ErrorResponseUnmarshaller::_instance
	S3ErrorResponseUnmarshaller_t140617651 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(S3ErrorResponseUnmarshaller_t140617651_StaticFields, ____instance_0)); }
	inline S3ErrorResponseUnmarshaller_t140617651 * get__instance_0() const { return ____instance_0; }
	inline S3ErrorResponseUnmarshaller_t140617651 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(S3ErrorResponseUnmarshaller_t140617651 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3ERRORRESPONSEUNMARSHALLER_T140617651_H
#ifndef FACEINFO_T3239700425_H
#define FACEINFO_T3239700425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FaceInfo
struct  FaceInfo_t3239700425  : public RuntimeObject
{
public:
	// System.String TMPro.FaceInfo::Name
	String_t* ___Name_0;
	// System.Single TMPro.FaceInfo::PointSize
	float ___PointSize_1;
	// System.Single TMPro.FaceInfo::Scale
	float ___Scale_2;
	// System.Int32 TMPro.FaceInfo::CharacterCount
	int32_t ___CharacterCount_3;
	// System.Single TMPro.FaceInfo::LineHeight
	float ___LineHeight_4;
	// System.Single TMPro.FaceInfo::Baseline
	float ___Baseline_5;
	// System.Single TMPro.FaceInfo::Ascender
	float ___Ascender_6;
	// System.Single TMPro.FaceInfo::CapHeight
	float ___CapHeight_7;
	// System.Single TMPro.FaceInfo::Descender
	float ___Descender_8;
	// System.Single TMPro.FaceInfo::CenterLine
	float ___CenterLine_9;
	// System.Single TMPro.FaceInfo::SuperscriptOffset
	float ___SuperscriptOffset_10;
	// System.Single TMPro.FaceInfo::SubscriptOffset
	float ___SubscriptOffset_11;
	// System.Single TMPro.FaceInfo::SubSize
	float ___SubSize_12;
	// System.Single TMPro.FaceInfo::Underline
	float ___Underline_13;
	// System.Single TMPro.FaceInfo::UnderlineThickness
	float ___UnderlineThickness_14;
	// System.Single TMPro.FaceInfo::strikethrough
	float ___strikethrough_15;
	// System.Single TMPro.FaceInfo::strikethroughThickness
	float ___strikethroughThickness_16;
	// System.Single TMPro.FaceInfo::TabWidth
	float ___TabWidth_17;
	// System.Single TMPro.FaceInfo::Padding
	float ___Padding_18;
	// System.Single TMPro.FaceInfo::AtlasWidth
	float ___AtlasWidth_19;
	// System.Single TMPro.FaceInfo::AtlasHeight
	float ___AtlasHeight_20;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_PointSize_1() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___PointSize_1)); }
	inline float get_PointSize_1() const { return ___PointSize_1; }
	inline float* get_address_of_PointSize_1() { return &___PointSize_1; }
	inline void set_PointSize_1(float value)
	{
		___PointSize_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}

	inline static int32_t get_offset_of_CharacterCount_3() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___CharacterCount_3)); }
	inline int32_t get_CharacterCount_3() const { return ___CharacterCount_3; }
	inline int32_t* get_address_of_CharacterCount_3() { return &___CharacterCount_3; }
	inline void set_CharacterCount_3(int32_t value)
	{
		___CharacterCount_3 = value;
	}

	inline static int32_t get_offset_of_LineHeight_4() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___LineHeight_4)); }
	inline float get_LineHeight_4() const { return ___LineHeight_4; }
	inline float* get_address_of_LineHeight_4() { return &___LineHeight_4; }
	inline void set_LineHeight_4(float value)
	{
		___LineHeight_4 = value;
	}

	inline static int32_t get_offset_of_Baseline_5() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___Baseline_5)); }
	inline float get_Baseline_5() const { return ___Baseline_5; }
	inline float* get_address_of_Baseline_5() { return &___Baseline_5; }
	inline void set_Baseline_5(float value)
	{
		___Baseline_5 = value;
	}

	inline static int32_t get_offset_of_Ascender_6() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___Ascender_6)); }
	inline float get_Ascender_6() const { return ___Ascender_6; }
	inline float* get_address_of_Ascender_6() { return &___Ascender_6; }
	inline void set_Ascender_6(float value)
	{
		___Ascender_6 = value;
	}

	inline static int32_t get_offset_of_CapHeight_7() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___CapHeight_7)); }
	inline float get_CapHeight_7() const { return ___CapHeight_7; }
	inline float* get_address_of_CapHeight_7() { return &___CapHeight_7; }
	inline void set_CapHeight_7(float value)
	{
		___CapHeight_7 = value;
	}

	inline static int32_t get_offset_of_Descender_8() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___Descender_8)); }
	inline float get_Descender_8() const { return ___Descender_8; }
	inline float* get_address_of_Descender_8() { return &___Descender_8; }
	inline void set_Descender_8(float value)
	{
		___Descender_8 = value;
	}

	inline static int32_t get_offset_of_CenterLine_9() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___CenterLine_9)); }
	inline float get_CenterLine_9() const { return ___CenterLine_9; }
	inline float* get_address_of_CenterLine_9() { return &___CenterLine_9; }
	inline void set_CenterLine_9(float value)
	{
		___CenterLine_9 = value;
	}

	inline static int32_t get_offset_of_SuperscriptOffset_10() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___SuperscriptOffset_10)); }
	inline float get_SuperscriptOffset_10() const { return ___SuperscriptOffset_10; }
	inline float* get_address_of_SuperscriptOffset_10() { return &___SuperscriptOffset_10; }
	inline void set_SuperscriptOffset_10(float value)
	{
		___SuperscriptOffset_10 = value;
	}

	inline static int32_t get_offset_of_SubscriptOffset_11() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___SubscriptOffset_11)); }
	inline float get_SubscriptOffset_11() const { return ___SubscriptOffset_11; }
	inline float* get_address_of_SubscriptOffset_11() { return &___SubscriptOffset_11; }
	inline void set_SubscriptOffset_11(float value)
	{
		___SubscriptOffset_11 = value;
	}

	inline static int32_t get_offset_of_SubSize_12() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___SubSize_12)); }
	inline float get_SubSize_12() const { return ___SubSize_12; }
	inline float* get_address_of_SubSize_12() { return &___SubSize_12; }
	inline void set_SubSize_12(float value)
	{
		___SubSize_12 = value;
	}

	inline static int32_t get_offset_of_Underline_13() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___Underline_13)); }
	inline float get_Underline_13() const { return ___Underline_13; }
	inline float* get_address_of_Underline_13() { return &___Underline_13; }
	inline void set_Underline_13(float value)
	{
		___Underline_13 = value;
	}

	inline static int32_t get_offset_of_UnderlineThickness_14() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___UnderlineThickness_14)); }
	inline float get_UnderlineThickness_14() const { return ___UnderlineThickness_14; }
	inline float* get_address_of_UnderlineThickness_14() { return &___UnderlineThickness_14; }
	inline void set_UnderlineThickness_14(float value)
	{
		___UnderlineThickness_14 = value;
	}

	inline static int32_t get_offset_of_strikethrough_15() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___strikethrough_15)); }
	inline float get_strikethrough_15() const { return ___strikethrough_15; }
	inline float* get_address_of_strikethrough_15() { return &___strikethrough_15; }
	inline void set_strikethrough_15(float value)
	{
		___strikethrough_15 = value;
	}

	inline static int32_t get_offset_of_strikethroughThickness_16() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___strikethroughThickness_16)); }
	inline float get_strikethroughThickness_16() const { return ___strikethroughThickness_16; }
	inline float* get_address_of_strikethroughThickness_16() { return &___strikethroughThickness_16; }
	inline void set_strikethroughThickness_16(float value)
	{
		___strikethroughThickness_16 = value;
	}

	inline static int32_t get_offset_of_TabWidth_17() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___TabWidth_17)); }
	inline float get_TabWidth_17() const { return ___TabWidth_17; }
	inline float* get_address_of_TabWidth_17() { return &___TabWidth_17; }
	inline void set_TabWidth_17(float value)
	{
		___TabWidth_17 = value;
	}

	inline static int32_t get_offset_of_Padding_18() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___Padding_18)); }
	inline float get_Padding_18() const { return ___Padding_18; }
	inline float* get_address_of_Padding_18() { return &___Padding_18; }
	inline void set_Padding_18(float value)
	{
		___Padding_18 = value;
	}

	inline static int32_t get_offset_of_AtlasWidth_19() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___AtlasWidth_19)); }
	inline float get_AtlasWidth_19() const { return ___AtlasWidth_19; }
	inline float* get_address_of_AtlasWidth_19() { return &___AtlasWidth_19; }
	inline void set_AtlasWidth_19(float value)
	{
		___AtlasWidth_19 = value;
	}

	inline static int32_t get_offset_of_AtlasHeight_20() { return static_cast<int32_t>(offsetof(FaceInfo_t3239700425, ___AtlasHeight_20)); }
	inline float get_AtlasHeight_20() const { return ___AtlasHeight_20; }
	inline float* get_address_of_AtlasHeight_20() { return &___AtlasHeight_20; }
	inline void set_AtlasHeight_20(float value)
	{
		___AtlasHeight_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEINFO_T3239700425_H
#ifndef ABSTRACTAWSSIGNER_T2114314031_H
#define ABSTRACTAWSSIGNER_T2114314031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct  AbstractAWSSigner_t2114314031  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.Auth.AWS4Signer Amazon.Runtime.Internal.Auth.AbstractAWSSigner::_aws4Signer
	AWS4Signer_t1011449585 * ____aws4Signer_0;

public:
	inline static int32_t get_offset_of__aws4Signer_0() { return static_cast<int32_t>(offsetof(AbstractAWSSigner_t2114314031, ____aws4Signer_0)); }
	inline AWS4Signer_t1011449585 * get__aws4Signer_0() const { return ____aws4Signer_0; }
	inline AWS4Signer_t1011449585 ** get_address_of__aws4Signer_0() { return &____aws4Signer_0; }
	inline void set__aws4Signer_0(AWS4Signer_t1011449585 * value)
	{
		____aws4Signer_0 = value;
		Il2CppCodeGenWriteBarrier((&____aws4Signer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTAWSSIGNER_T2114314031_H
#ifndef PIPELINEHANDLER_T1486422324_H
#define PIPELINEHANDLER_T1486422324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.PipelineHandler
struct  PipelineHandler_t1486422324  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.PipelineHandler::<Logger>k__BackingField
	RuntimeObject* ___U3CLoggerU3Ek__BackingField_0;
	// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::<InnerHandler>k__BackingField
	RuntimeObject* ___U3CInnerHandlerU3Ek__BackingField_1;
	// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::<OuterHandler>k__BackingField
	RuntimeObject* ___U3COuterHandlerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CLoggerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PipelineHandler_t1486422324, ___U3CLoggerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CLoggerU3Ek__BackingField_0() const { return ___U3CLoggerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CLoggerU3Ek__BackingField_0() { return &___U3CLoggerU3Ek__BackingField_0; }
	inline void set_U3CLoggerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CLoggerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CInnerHandlerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PipelineHandler_t1486422324, ___U3CInnerHandlerU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CInnerHandlerU3Ek__BackingField_1() const { return ___U3CInnerHandlerU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CInnerHandlerU3Ek__BackingField_1() { return &___U3CInnerHandlerU3Ek__BackingField_1; }
	inline void set_U3CInnerHandlerU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CInnerHandlerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInnerHandlerU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COuterHandlerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PipelineHandler_t1486422324, ___U3COuterHandlerU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3COuterHandlerU3Ek__BackingField_2() const { return ___U3COuterHandlerU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3COuterHandlerU3Ek__BackingField_2() { return &___U3COuterHandlerU3Ek__BackingField_2; }
	inline void set_U3COuterHandlerU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3COuterHandlerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COuterHandlerU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIPELINEHANDLER_T1486422324_H
#ifndef AMAZONWEBSERVICEREQUEST_T3384026212_H
#define AMAZONWEBSERVICEREQUEST_T3384026212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonWebServiceRequest
struct  AmazonWebServiceRequest_t3384026212  : public RuntimeObject
{
public:
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonWebServiceRequest::mBeforeRequestEvent
	RequestEventHandler_t2213783891 * ___mBeforeRequestEvent_0;
	// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.StreamUploadProgressCallback>k__BackingField
	EventHandler_1_t1230945235 * ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Amazon.Runtime.AmazonWebServiceRequest::requestState
	Dictionary_2_t309261261 * ___requestState_2;
	// System.Boolean Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.UseSigV4>k__BackingField
	bool ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_mBeforeRequestEvent_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___mBeforeRequestEvent_0)); }
	inline RequestEventHandler_t2213783891 * get_mBeforeRequestEvent_0() const { return ___mBeforeRequestEvent_0; }
	inline RequestEventHandler_t2213783891 ** get_address_of_mBeforeRequestEvent_0() { return &___mBeforeRequestEvent_0; }
	inline void set_mBeforeRequestEvent_0(RequestEventHandler_t2213783891 * value)
	{
		___mBeforeRequestEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBeforeRequestEvent_0), value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1)); }
	inline EventHandler_1_t1230945235 * get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline EventHandler_1_t1230945235 ** get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1(EventHandler_1_t1230945235 * value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_requestState_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___requestState_2)); }
	inline Dictionary_2_t309261261 * get_requestState_2() const { return ___requestState_2; }
	inline Dictionary_2_t309261261 ** get_address_of_requestState_2() { return &___requestState_2; }
	inline void set_requestState_2(Dictionary_2_t309261261 * value)
	{
		___requestState_2 = value;
		Il2CppCodeGenWriteBarrier((&___requestState_2), value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3)); }
	inline bool get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3; }
	inline bool* get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3(bool value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONWEBSERVICEREQUEST_T3384026212_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef UNMARSHALLERCONTEXT_T1608322995_H
#define UNMARSHALLERCONTEXT_T1608322995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.UnmarshallerContext
struct  UnmarshallerContext_t1608322995  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::disposed
	bool ___disposed_0;
	// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::<MaintainResponseBody>k__BackingField
	bool ___U3CMaintainResponseBodyU3Ek__BackingField_1;
	// ThirdParty.Ionic.Zlib.CrcCalculatorStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::<CrcStream>k__BackingField
	CrcCalculatorStream_t2228597532 * ___U3CCrcStreamU3Ek__BackingField_2;
	// System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::<Crc32Result>k__BackingField
	int32_t ___U3CCrc32ResultU3Ek__BackingField_3;
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.Transform.UnmarshallerContext::<WebResponseData>k__BackingField
	RuntimeObject* ___U3CWebResponseDataU3Ek__BackingField_4;
	// Amazon.Runtime.Internal.Util.CachingWrapperStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::<WrappingStream>k__BackingField
	CachingWrapperStream_t380166576 * ___U3CWrappingStreamU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaintainResponseBodyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CMaintainResponseBodyU3Ek__BackingField_1)); }
	inline bool get_U3CMaintainResponseBodyU3Ek__BackingField_1() const { return ___U3CMaintainResponseBodyU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CMaintainResponseBodyU3Ek__BackingField_1() { return &___U3CMaintainResponseBodyU3Ek__BackingField_1; }
	inline void set_U3CMaintainResponseBodyU3Ek__BackingField_1(bool value)
	{
		___U3CMaintainResponseBodyU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCrcStreamU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CCrcStreamU3Ek__BackingField_2)); }
	inline CrcCalculatorStream_t2228597532 * get_U3CCrcStreamU3Ek__BackingField_2() const { return ___U3CCrcStreamU3Ek__BackingField_2; }
	inline CrcCalculatorStream_t2228597532 ** get_address_of_U3CCrcStreamU3Ek__BackingField_2() { return &___U3CCrcStreamU3Ek__BackingField_2; }
	inline void set_U3CCrcStreamU3Ek__BackingField_2(CrcCalculatorStream_t2228597532 * value)
	{
		___U3CCrcStreamU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCrcStreamU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCrc32ResultU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CCrc32ResultU3Ek__BackingField_3)); }
	inline int32_t get_U3CCrc32ResultU3Ek__BackingField_3() const { return ___U3CCrc32ResultU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCrc32ResultU3Ek__BackingField_3() { return &___U3CCrc32ResultU3Ek__BackingField_3; }
	inline void set_U3CCrc32ResultU3Ek__BackingField_3(int32_t value)
	{
		___U3CCrc32ResultU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CWebResponseDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CWebResponseDataU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CWebResponseDataU3Ek__BackingField_4() const { return ___U3CWebResponseDataU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CWebResponseDataU3Ek__BackingField_4() { return &___U3CWebResponseDataU3Ek__BackingField_4; }
	inline void set_U3CWebResponseDataU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CWebResponseDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWebResponseDataU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CWrappingStreamU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CWrappingStreamU3Ek__BackingField_5)); }
	inline CachingWrapperStream_t380166576 * get_U3CWrappingStreamU3Ek__BackingField_5() const { return ___U3CWrappingStreamU3Ek__BackingField_5; }
	inline CachingWrapperStream_t380166576 ** get_address_of_U3CWrappingStreamU3Ek__BackingField_5() { return &___U3CWrappingStreamU3Ek__BackingField_5; }
	inline void set_U3CWrappingStreamU3Ek__BackingField_5(CachingWrapperStream_t380166576 * value)
	{
		___U3CWrappingStreamU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWrappingStreamU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMARSHALLERCONTEXT_T1608322995_H
#ifndef RESPONSEUNMARSHALLER_T3934041557_H
#define RESPONSEUNMARSHALLER_T3934041557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct  ResponseUnmarshaller_t3934041557  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEUNMARSHALLER_T3934041557_H
#ifndef TMP_TEXTELEMENT_T2285620223_H
#define TMP_TEXTELEMENT_T2285620223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_t2285620223  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_T2285620223_H
#ifndef U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T2874277708_H
#define U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T2874277708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1
struct  U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t2874277708  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddGlyphPairAdjustmentRecord>c__AnonStorey1::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t2874277708, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t2874277708, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDGLYPHPAIRADJUSTMENTRECORDU3EC__ANONSTOREY1_T2874277708_H
#ifndef U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2114388639_H
#define U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2114388639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<AddKerningPair>c__AnonStorey0
struct  U3CAddKerningPairU3Ec__AnonStorey0_t2114388639  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::first
	uint32_t ___first_0;
	// System.UInt32 TMPro.KerningTable/<AddKerningPair>c__AnonStorey0::second
	uint32_t ___second_1;

public:
	inline static int32_t get_offset_of_first_0() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t2114388639, ___first_0)); }
	inline uint32_t get_first_0() const { return ___first_0; }
	inline uint32_t* get_address_of_first_0() { return &___first_0; }
	inline void set_first_0(uint32_t value)
	{
		___first_0 = value;
	}

	inline static int32_t get_offset_of_second_1() { return static_cast<int32_t>(offsetof(U3CAddKerningPairU3Ec__AnonStorey0_t2114388639, ___second_1)); }
	inline uint32_t get_second_1() const { return ___second_1; }
	inline uint32_t* get_address_of_second_1() { return &___second_1; }
	inline void set_second_1(uint32_t value)
	{
		___second_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDKERNINGPAIRU3EC__ANONSTOREY0_T2114388639_H
#ifndef KERNINGTABLE_T2970824110_H
#define KERNINGTABLE_T2970824110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable
struct  KerningTable_t2970824110  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.KerningPair> TMPro.KerningTable::kerningPairs
	List_1_t946875054 * ___kerningPairs_0;

public:
	inline static int32_t get_offset_of_kerningPairs_0() { return static_cast<int32_t>(offsetof(KerningTable_t2970824110, ___kerningPairs_0)); }
	inline List_1_t946875054 * get_kerningPairs_0() const { return ___kerningPairs_0; }
	inline List_1_t946875054 ** get_address_of_kerningPairs_0() { return &___kerningPairs_0; }
	inline void set_kerningPairs_0(List_1_t946875054 * value)
	{
		___kerningPairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___kerningPairs_0), value);
	}
};

struct KerningTable_t2970824110_StaticFields
{
public:
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache0
	Func_2_t2782326444 * ___U3CU3Ef__amU24cache0_1;
	// System.Func`2<TMPro.KerningPair,System.UInt32> TMPro.KerningTable::<>f__am$cache1
	Func_2_t2782326444 * ___U3CU3Ef__amU24cache1_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(KerningTable_t2970824110_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t2782326444 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t2782326444 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t2782326444 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(KerningTable_t2970824110_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Func_2_t2782326444 * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Func_2_t2782326444 ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Func_2_t2782326444 * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGTABLE_T2970824110_H
#ifndef U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T702943500_H
#define U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T702943500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2
struct  U3CRemoveKerningPairU3Ec__AnonStorey2_t702943500  : public RuntimeObject
{
public:
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::left
	int32_t ___left_0;
	// System.Int32 TMPro.KerningTable/<RemoveKerningPair>c__AnonStorey2::right
	int32_t ___right_1;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t702943500, ___left_0)); }
	inline int32_t get_left_0() const { return ___left_0; }
	inline int32_t* get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(int32_t value)
	{
		___left_0 = value;
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(U3CRemoveKerningPairU3Ec__AnonStorey2_t702943500, ___right_1)); }
	inline int32_t get_right_1() const { return ___right_1; }
	inline int32_t* get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(int32_t value)
	{
		___right_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEKERNINGPAIRU3EC__ANONSTOREY2_T702943500_H
#ifndef TMP_COMPATIBILITY_T3260547480_H
#define TMP_COMPATIBILITY_T3260547480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Compatibility
struct  TMP_Compatibility_t3260547480  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_COMPATIBILITY_T3260547480_H
#ifndef SHADERUTILITIES_T2519527413_H
#define SHADERUTILITIES_T2519527413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ShaderUtilities
struct  ShaderUtilities_t2519527413  : public RuntimeObject
{
public:

public:
};

struct ShaderUtilities_t2519527413_StaticFields
{
public:
	// System.Int32 TMPro.ShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 TMPro.ShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 TMPro.ShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_16;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_17;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_18;
	// System.Int32 TMPro.ShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_19;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_20;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_21;
	// System.Int32 TMPro.ShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_22;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_23;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_24;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_25;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_26;
	// System.Int32 TMPro.ShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_27;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_28;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_29;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_30;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_31;
	// System.Int32 TMPro.ShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_32;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_33;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_34;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_35;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_36;
	// System.Int32 TMPro.ShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_37;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_38;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_39;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_40;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_41;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_42;
	// System.Int32 TMPro.ShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_43;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_44;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_45;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_46;
	// System.String TMPro.ShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_47;
	// System.String TMPro.ShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_48;
	// System.String TMPro.ShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_49;
	// System.String TMPro.ShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_50;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_51;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_52;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_53;
	// System.String TMPro.ShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_54;
	// System.String TMPro.ShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_55;
	// System.String TMPro.ShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_56;
	// System.Single TMPro.ShaderUtilities::m_clamp
	float ___m_clamp_57;
	// System.Boolean TMPro.ShaderUtilities::isInitialized
	bool ___isInitialized_58;

public:
	inline static int32_t get_offset_of_ID_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_MainTex_0)); }
	inline int32_t get_ID_MainTex_0() const { return ___ID_MainTex_0; }
	inline int32_t* get_address_of_ID_MainTex_0() { return &___ID_MainTex_0; }
	inline void set_ID_MainTex_0(int32_t value)
	{
		___ID_MainTex_0 = value;
	}

	inline static int32_t get_offset_of_ID_FaceTex_1() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_FaceTex_1)); }
	inline int32_t get_ID_FaceTex_1() const { return ___ID_FaceTex_1; }
	inline int32_t* get_address_of_ID_FaceTex_1() { return &___ID_FaceTex_1; }
	inline void set_ID_FaceTex_1(int32_t value)
	{
		___ID_FaceTex_1 = value;
	}

	inline static int32_t get_offset_of_ID_FaceColor_2() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_FaceColor_2)); }
	inline int32_t get_ID_FaceColor_2() const { return ___ID_FaceColor_2; }
	inline int32_t* get_address_of_ID_FaceColor_2() { return &___ID_FaceColor_2; }
	inline void set_ID_FaceColor_2(int32_t value)
	{
		___ID_FaceColor_2 = value;
	}

	inline static int32_t get_offset_of_ID_FaceDilate_3() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_FaceDilate_3)); }
	inline int32_t get_ID_FaceDilate_3() const { return ___ID_FaceDilate_3; }
	inline int32_t* get_address_of_ID_FaceDilate_3() { return &___ID_FaceDilate_3; }
	inline void set_ID_FaceDilate_3(int32_t value)
	{
		___ID_FaceDilate_3 = value;
	}

	inline static int32_t get_offset_of_ID_Shininess_4() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_Shininess_4)); }
	inline int32_t get_ID_Shininess_4() const { return ___ID_Shininess_4; }
	inline int32_t* get_address_of_ID_Shininess_4() { return &___ID_Shininess_4; }
	inline void set_ID_Shininess_4(int32_t value)
	{
		___ID_Shininess_4 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayColor_5() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_UnderlayColor_5)); }
	inline int32_t get_ID_UnderlayColor_5() const { return ___ID_UnderlayColor_5; }
	inline int32_t* get_address_of_ID_UnderlayColor_5() { return &___ID_UnderlayColor_5; }
	inline void set_ID_UnderlayColor_5(int32_t value)
	{
		___ID_UnderlayColor_5 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetX_6() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_UnderlayOffsetX_6)); }
	inline int32_t get_ID_UnderlayOffsetX_6() const { return ___ID_UnderlayOffsetX_6; }
	inline int32_t* get_address_of_ID_UnderlayOffsetX_6() { return &___ID_UnderlayOffsetX_6; }
	inline void set_ID_UnderlayOffsetX_6(int32_t value)
	{
		___ID_UnderlayOffsetX_6 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetY_7() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_UnderlayOffsetY_7)); }
	inline int32_t get_ID_UnderlayOffsetY_7() const { return ___ID_UnderlayOffsetY_7; }
	inline int32_t* get_address_of_ID_UnderlayOffsetY_7() { return &___ID_UnderlayOffsetY_7; }
	inline void set_ID_UnderlayOffsetY_7(int32_t value)
	{
		___ID_UnderlayOffsetY_7 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayDilate_8() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_UnderlayDilate_8)); }
	inline int32_t get_ID_UnderlayDilate_8() const { return ___ID_UnderlayDilate_8; }
	inline int32_t* get_address_of_ID_UnderlayDilate_8() { return &___ID_UnderlayDilate_8; }
	inline void set_ID_UnderlayDilate_8(int32_t value)
	{
		___ID_UnderlayDilate_8 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlaySoftness_9() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_UnderlaySoftness_9)); }
	inline int32_t get_ID_UnderlaySoftness_9() const { return ___ID_UnderlaySoftness_9; }
	inline int32_t* get_address_of_ID_UnderlaySoftness_9() { return &___ID_UnderlaySoftness_9; }
	inline void set_ID_UnderlaySoftness_9(int32_t value)
	{
		___ID_UnderlaySoftness_9 = value;
	}

	inline static int32_t get_offset_of_ID_WeightNormal_10() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_WeightNormal_10)); }
	inline int32_t get_ID_WeightNormal_10() const { return ___ID_WeightNormal_10; }
	inline int32_t* get_address_of_ID_WeightNormal_10() { return &___ID_WeightNormal_10; }
	inline void set_ID_WeightNormal_10(int32_t value)
	{
		___ID_WeightNormal_10 = value;
	}

	inline static int32_t get_offset_of_ID_WeightBold_11() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_WeightBold_11)); }
	inline int32_t get_ID_WeightBold_11() const { return ___ID_WeightBold_11; }
	inline int32_t* get_address_of_ID_WeightBold_11() { return &___ID_WeightBold_11; }
	inline void set_ID_WeightBold_11(int32_t value)
	{
		___ID_WeightBold_11 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineTex_12() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_OutlineTex_12)); }
	inline int32_t get_ID_OutlineTex_12() const { return ___ID_OutlineTex_12; }
	inline int32_t* get_address_of_ID_OutlineTex_12() { return &___ID_OutlineTex_12; }
	inline void set_ID_OutlineTex_12(int32_t value)
	{
		___ID_OutlineTex_12 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineWidth_13() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_OutlineWidth_13)); }
	inline int32_t get_ID_OutlineWidth_13() const { return ___ID_OutlineWidth_13; }
	inline int32_t* get_address_of_ID_OutlineWidth_13() { return &___ID_OutlineWidth_13; }
	inline void set_ID_OutlineWidth_13(int32_t value)
	{
		___ID_OutlineWidth_13 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineSoftness_14() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_OutlineSoftness_14)); }
	inline int32_t get_ID_OutlineSoftness_14() const { return ___ID_OutlineSoftness_14; }
	inline int32_t* get_address_of_ID_OutlineSoftness_14() { return &___ID_OutlineSoftness_14; }
	inline void set_ID_OutlineSoftness_14(int32_t value)
	{
		___ID_OutlineSoftness_14 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineColor_15() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_OutlineColor_15)); }
	inline int32_t get_ID_OutlineColor_15() const { return ___ID_OutlineColor_15; }
	inline int32_t* get_address_of_ID_OutlineColor_15() { return &___ID_OutlineColor_15; }
	inline void set_ID_OutlineColor_15(int32_t value)
	{
		___ID_OutlineColor_15 = value;
	}

	inline static int32_t get_offset_of_ID_GradientScale_16() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_GradientScale_16)); }
	inline int32_t get_ID_GradientScale_16() const { return ___ID_GradientScale_16; }
	inline int32_t* get_address_of_ID_GradientScale_16() { return &___ID_GradientScale_16; }
	inline void set_ID_GradientScale_16(int32_t value)
	{
		___ID_GradientScale_16 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleX_17() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_ScaleX_17)); }
	inline int32_t get_ID_ScaleX_17() const { return ___ID_ScaleX_17; }
	inline int32_t* get_address_of_ID_ScaleX_17() { return &___ID_ScaleX_17; }
	inline void set_ID_ScaleX_17(int32_t value)
	{
		___ID_ScaleX_17 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleY_18() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_ScaleY_18)); }
	inline int32_t get_ID_ScaleY_18() const { return ___ID_ScaleY_18; }
	inline int32_t* get_address_of_ID_ScaleY_18() { return &___ID_ScaleY_18; }
	inline void set_ID_ScaleY_18(int32_t value)
	{
		___ID_ScaleY_18 = value;
	}

	inline static int32_t get_offset_of_ID_PerspectiveFilter_19() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_PerspectiveFilter_19)); }
	inline int32_t get_ID_PerspectiveFilter_19() const { return ___ID_PerspectiveFilter_19; }
	inline int32_t* get_address_of_ID_PerspectiveFilter_19() { return &___ID_PerspectiveFilter_19; }
	inline void set_ID_PerspectiveFilter_19(int32_t value)
	{
		___ID_PerspectiveFilter_19 = value;
	}

	inline static int32_t get_offset_of_ID_TextureWidth_20() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_TextureWidth_20)); }
	inline int32_t get_ID_TextureWidth_20() const { return ___ID_TextureWidth_20; }
	inline int32_t* get_address_of_ID_TextureWidth_20() { return &___ID_TextureWidth_20; }
	inline void set_ID_TextureWidth_20(int32_t value)
	{
		___ID_TextureWidth_20 = value;
	}

	inline static int32_t get_offset_of_ID_TextureHeight_21() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_TextureHeight_21)); }
	inline int32_t get_ID_TextureHeight_21() const { return ___ID_TextureHeight_21; }
	inline int32_t* get_address_of_ID_TextureHeight_21() { return &___ID_TextureHeight_21; }
	inline void set_ID_TextureHeight_21(int32_t value)
	{
		___ID_TextureHeight_21 = value;
	}

	inline static int32_t get_offset_of_ID_BevelAmount_22() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_BevelAmount_22)); }
	inline int32_t get_ID_BevelAmount_22() const { return ___ID_BevelAmount_22; }
	inline int32_t* get_address_of_ID_BevelAmount_22() { return &___ID_BevelAmount_22; }
	inline void set_ID_BevelAmount_22(int32_t value)
	{
		___ID_BevelAmount_22 = value;
	}

	inline static int32_t get_offset_of_ID_GlowColor_23() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_GlowColor_23)); }
	inline int32_t get_ID_GlowColor_23() const { return ___ID_GlowColor_23; }
	inline int32_t* get_address_of_ID_GlowColor_23() { return &___ID_GlowColor_23; }
	inline void set_ID_GlowColor_23(int32_t value)
	{
		___ID_GlowColor_23 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOffset_24() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_GlowOffset_24)); }
	inline int32_t get_ID_GlowOffset_24() const { return ___ID_GlowOffset_24; }
	inline int32_t* get_address_of_ID_GlowOffset_24() { return &___ID_GlowOffset_24; }
	inline void set_ID_GlowOffset_24(int32_t value)
	{
		___ID_GlowOffset_24 = value;
	}

	inline static int32_t get_offset_of_ID_GlowPower_25() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_GlowPower_25)); }
	inline int32_t get_ID_GlowPower_25() const { return ___ID_GlowPower_25; }
	inline int32_t* get_address_of_ID_GlowPower_25() { return &___ID_GlowPower_25; }
	inline void set_ID_GlowPower_25(int32_t value)
	{
		___ID_GlowPower_25 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOuter_26() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_GlowOuter_26)); }
	inline int32_t get_ID_GlowOuter_26() const { return ___ID_GlowOuter_26; }
	inline int32_t* get_address_of_ID_GlowOuter_26() { return &___ID_GlowOuter_26; }
	inline void set_ID_GlowOuter_26(int32_t value)
	{
		___ID_GlowOuter_26 = value;
	}

	inline static int32_t get_offset_of_ID_LightAngle_27() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_LightAngle_27)); }
	inline int32_t get_ID_LightAngle_27() const { return ___ID_LightAngle_27; }
	inline int32_t* get_address_of_ID_LightAngle_27() { return &___ID_LightAngle_27; }
	inline void set_ID_LightAngle_27(int32_t value)
	{
		___ID_LightAngle_27 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMap_28() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_EnvMap_28)); }
	inline int32_t get_ID_EnvMap_28() const { return ___ID_EnvMap_28; }
	inline int32_t* get_address_of_ID_EnvMap_28() { return &___ID_EnvMap_28; }
	inline void set_ID_EnvMap_28(int32_t value)
	{
		___ID_EnvMap_28 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrix_29() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_EnvMatrix_29)); }
	inline int32_t get_ID_EnvMatrix_29() const { return ___ID_EnvMatrix_29; }
	inline int32_t* get_address_of_ID_EnvMatrix_29() { return &___ID_EnvMatrix_29; }
	inline void set_ID_EnvMatrix_29(int32_t value)
	{
		___ID_EnvMatrix_29 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrixRotation_30() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_EnvMatrixRotation_30)); }
	inline int32_t get_ID_EnvMatrixRotation_30() const { return ___ID_EnvMatrixRotation_30; }
	inline int32_t* get_address_of_ID_EnvMatrixRotation_30() { return &___ID_EnvMatrixRotation_30; }
	inline void set_ID_EnvMatrixRotation_30(int32_t value)
	{
		___ID_EnvMatrixRotation_30 = value;
	}

	inline static int32_t get_offset_of_ID_MaskCoord_31() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_MaskCoord_31)); }
	inline int32_t get_ID_MaskCoord_31() const { return ___ID_MaskCoord_31; }
	inline int32_t* get_address_of_ID_MaskCoord_31() { return &___ID_MaskCoord_31; }
	inline void set_ID_MaskCoord_31(int32_t value)
	{
		___ID_MaskCoord_31 = value;
	}

	inline static int32_t get_offset_of_ID_ClipRect_32() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_ClipRect_32)); }
	inline int32_t get_ID_ClipRect_32() const { return ___ID_ClipRect_32; }
	inline int32_t* get_address_of_ID_ClipRect_32() { return &___ID_ClipRect_32; }
	inline void set_ID_ClipRect_32(int32_t value)
	{
		___ID_ClipRect_32 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessX_33() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_MaskSoftnessX_33)); }
	inline int32_t get_ID_MaskSoftnessX_33() const { return ___ID_MaskSoftnessX_33; }
	inline int32_t* get_address_of_ID_MaskSoftnessX_33() { return &___ID_MaskSoftnessX_33; }
	inline void set_ID_MaskSoftnessX_33(int32_t value)
	{
		___ID_MaskSoftnessX_33 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessY_34() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_MaskSoftnessY_34)); }
	inline int32_t get_ID_MaskSoftnessY_34() const { return ___ID_MaskSoftnessY_34; }
	inline int32_t* get_address_of_ID_MaskSoftnessY_34() { return &___ID_MaskSoftnessY_34; }
	inline void set_ID_MaskSoftnessY_34(int32_t value)
	{
		___ID_MaskSoftnessY_34 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetX_35() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_VertexOffsetX_35)); }
	inline int32_t get_ID_VertexOffsetX_35() const { return ___ID_VertexOffsetX_35; }
	inline int32_t* get_address_of_ID_VertexOffsetX_35() { return &___ID_VertexOffsetX_35; }
	inline void set_ID_VertexOffsetX_35(int32_t value)
	{
		___ID_VertexOffsetX_35 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetY_36() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_VertexOffsetY_36)); }
	inline int32_t get_ID_VertexOffsetY_36() const { return ___ID_VertexOffsetY_36; }
	inline int32_t* get_address_of_ID_VertexOffsetY_36() { return &___ID_VertexOffsetY_36; }
	inline void set_ID_VertexOffsetY_36(int32_t value)
	{
		___ID_VertexOffsetY_36 = value;
	}

	inline static int32_t get_offset_of_ID_UseClipRect_37() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_UseClipRect_37)); }
	inline int32_t get_ID_UseClipRect_37() const { return ___ID_UseClipRect_37; }
	inline int32_t* get_address_of_ID_UseClipRect_37() { return &___ID_UseClipRect_37; }
	inline void set_ID_UseClipRect_37(int32_t value)
	{
		___ID_UseClipRect_37 = value;
	}

	inline static int32_t get_offset_of_ID_StencilID_38() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_StencilID_38)); }
	inline int32_t get_ID_StencilID_38() const { return ___ID_StencilID_38; }
	inline int32_t* get_address_of_ID_StencilID_38() { return &___ID_StencilID_38; }
	inline void set_ID_StencilID_38(int32_t value)
	{
		___ID_StencilID_38 = value;
	}

	inline static int32_t get_offset_of_ID_StencilOp_39() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_StencilOp_39)); }
	inline int32_t get_ID_StencilOp_39() const { return ___ID_StencilOp_39; }
	inline int32_t* get_address_of_ID_StencilOp_39() { return &___ID_StencilOp_39; }
	inline void set_ID_StencilOp_39(int32_t value)
	{
		___ID_StencilOp_39 = value;
	}

	inline static int32_t get_offset_of_ID_StencilComp_40() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_StencilComp_40)); }
	inline int32_t get_ID_StencilComp_40() const { return ___ID_StencilComp_40; }
	inline int32_t* get_address_of_ID_StencilComp_40() { return &___ID_StencilComp_40; }
	inline void set_ID_StencilComp_40(int32_t value)
	{
		___ID_StencilComp_40 = value;
	}

	inline static int32_t get_offset_of_ID_StencilReadMask_41() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_StencilReadMask_41)); }
	inline int32_t get_ID_StencilReadMask_41() const { return ___ID_StencilReadMask_41; }
	inline int32_t* get_address_of_ID_StencilReadMask_41() { return &___ID_StencilReadMask_41; }
	inline void set_ID_StencilReadMask_41(int32_t value)
	{
		___ID_StencilReadMask_41 = value;
	}

	inline static int32_t get_offset_of_ID_StencilWriteMask_42() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_StencilWriteMask_42)); }
	inline int32_t get_ID_StencilWriteMask_42() const { return ___ID_StencilWriteMask_42; }
	inline int32_t* get_address_of_ID_StencilWriteMask_42() { return &___ID_StencilWriteMask_42; }
	inline void set_ID_StencilWriteMask_42(int32_t value)
	{
		___ID_StencilWriteMask_42 = value;
	}

	inline static int32_t get_offset_of_ID_ShaderFlags_43() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_ShaderFlags_43)); }
	inline int32_t get_ID_ShaderFlags_43() const { return ___ID_ShaderFlags_43; }
	inline int32_t* get_address_of_ID_ShaderFlags_43() { return &___ID_ShaderFlags_43; }
	inline void set_ID_ShaderFlags_43(int32_t value)
	{
		___ID_ShaderFlags_43 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_A_44() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_ScaleRatio_A_44)); }
	inline int32_t get_ID_ScaleRatio_A_44() const { return ___ID_ScaleRatio_A_44; }
	inline int32_t* get_address_of_ID_ScaleRatio_A_44() { return &___ID_ScaleRatio_A_44; }
	inline void set_ID_ScaleRatio_A_44(int32_t value)
	{
		___ID_ScaleRatio_A_44 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_B_45() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_ScaleRatio_B_45)); }
	inline int32_t get_ID_ScaleRatio_B_45() const { return ___ID_ScaleRatio_B_45; }
	inline int32_t* get_address_of_ID_ScaleRatio_B_45() { return &___ID_ScaleRatio_B_45; }
	inline void set_ID_ScaleRatio_B_45(int32_t value)
	{
		___ID_ScaleRatio_B_45 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_C_46() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ID_ScaleRatio_C_46)); }
	inline int32_t get_ID_ScaleRatio_C_46() const { return ___ID_ScaleRatio_C_46; }
	inline int32_t* get_address_of_ID_ScaleRatio_C_46() { return &___ID_ScaleRatio_C_46; }
	inline void set_ID_ScaleRatio_C_46(int32_t value)
	{
		___ID_ScaleRatio_C_46 = value;
	}

	inline static int32_t get_offset_of_Keyword_Bevel_47() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___Keyword_Bevel_47)); }
	inline String_t* get_Keyword_Bevel_47() const { return ___Keyword_Bevel_47; }
	inline String_t** get_address_of_Keyword_Bevel_47() { return &___Keyword_Bevel_47; }
	inline void set_Keyword_Bevel_47(String_t* value)
	{
		___Keyword_Bevel_47 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Bevel_47), value);
	}

	inline static int32_t get_offset_of_Keyword_Glow_48() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___Keyword_Glow_48)); }
	inline String_t* get_Keyword_Glow_48() const { return ___Keyword_Glow_48; }
	inline String_t** get_address_of_Keyword_Glow_48() { return &___Keyword_Glow_48; }
	inline void set_Keyword_Glow_48(String_t* value)
	{
		___Keyword_Glow_48 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Glow_48), value);
	}

	inline static int32_t get_offset_of_Keyword_Underlay_49() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___Keyword_Underlay_49)); }
	inline String_t* get_Keyword_Underlay_49() const { return ___Keyword_Underlay_49; }
	inline String_t** get_address_of_Keyword_Underlay_49() { return &___Keyword_Underlay_49; }
	inline void set_Keyword_Underlay_49(String_t* value)
	{
		___Keyword_Underlay_49 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Underlay_49), value);
	}

	inline static int32_t get_offset_of_Keyword_Ratios_50() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___Keyword_Ratios_50)); }
	inline String_t* get_Keyword_Ratios_50() const { return ___Keyword_Ratios_50; }
	inline String_t** get_address_of_Keyword_Ratios_50() { return &___Keyword_Ratios_50; }
	inline void set_Keyword_Ratios_50(String_t* value)
	{
		___Keyword_Ratios_50 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Ratios_50), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_SOFT_51() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___Keyword_MASK_SOFT_51)); }
	inline String_t* get_Keyword_MASK_SOFT_51() const { return ___Keyword_MASK_SOFT_51; }
	inline String_t** get_address_of_Keyword_MASK_SOFT_51() { return &___Keyword_MASK_SOFT_51; }
	inline void set_Keyword_MASK_SOFT_51(String_t* value)
	{
		___Keyword_MASK_SOFT_51 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_SOFT_51), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_HARD_52() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___Keyword_MASK_HARD_52)); }
	inline String_t* get_Keyword_MASK_HARD_52() const { return ___Keyword_MASK_HARD_52; }
	inline String_t** get_address_of_Keyword_MASK_HARD_52() { return &___Keyword_MASK_HARD_52; }
	inline void set_Keyword_MASK_HARD_52(String_t* value)
	{
		___Keyword_MASK_HARD_52 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_HARD_52), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_TEX_53() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___Keyword_MASK_TEX_53)); }
	inline String_t* get_Keyword_MASK_TEX_53() const { return ___Keyword_MASK_TEX_53; }
	inline String_t** get_address_of_Keyword_MASK_TEX_53() { return &___Keyword_MASK_TEX_53; }
	inline void set_Keyword_MASK_TEX_53(String_t* value)
	{
		___Keyword_MASK_TEX_53 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_TEX_53), value);
	}

	inline static int32_t get_offset_of_Keyword_Outline_54() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___Keyword_Outline_54)); }
	inline String_t* get_Keyword_Outline_54() const { return ___Keyword_Outline_54; }
	inline String_t** get_address_of_Keyword_Outline_54() { return &___Keyword_Outline_54; }
	inline void set_Keyword_Outline_54(String_t* value)
	{
		___Keyword_Outline_54 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Outline_54), value);
	}

	inline static int32_t get_offset_of_ShaderTag_ZTestMode_55() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ShaderTag_ZTestMode_55)); }
	inline String_t* get_ShaderTag_ZTestMode_55() const { return ___ShaderTag_ZTestMode_55; }
	inline String_t** get_address_of_ShaderTag_ZTestMode_55() { return &___ShaderTag_ZTestMode_55; }
	inline void set_ShaderTag_ZTestMode_55(String_t* value)
	{
		___ShaderTag_ZTestMode_55 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_ZTestMode_55), value);
	}

	inline static int32_t get_offset_of_ShaderTag_CullMode_56() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___ShaderTag_CullMode_56)); }
	inline String_t* get_ShaderTag_CullMode_56() const { return ___ShaderTag_CullMode_56; }
	inline String_t** get_address_of_ShaderTag_CullMode_56() { return &___ShaderTag_CullMode_56; }
	inline void set_ShaderTag_CullMode_56(String_t* value)
	{
		___ShaderTag_CullMode_56 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_CullMode_56), value);
	}

	inline static int32_t get_offset_of_m_clamp_57() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___m_clamp_57)); }
	inline float get_m_clamp_57() const { return ___m_clamp_57; }
	inline float* get_address_of_m_clamp_57() { return &___m_clamp_57; }
	inline void set_m_clamp_57(float value)
	{
		___m_clamp_57 = value;
	}

	inline static int32_t get_offset_of_isInitialized_58() { return static_cast<int32_t>(offsetof(ShaderUtilities_t2519527413_StaticFields, ___isInitialized_58)); }
	inline bool get_isInitialized_58() const { return ___isInitialized_58; }
	inline bool* get_address_of_isInitialized_58() { return &___isInitialized_58; }
	inline void set_isInitialized_58(bool value)
	{
		___isInitialized_58 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERUTILITIES_T2519527413_H
#ifndef TMP_FONTUTILITIES_T1411032097_H
#define TMP_FONTUTILITIES_T1411032097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontUtilities
struct  TMP_FontUtilities_t1411032097  : public RuntimeObject
{
public:

public:
};

struct TMP_FontUtilities_t1411032097_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_FontUtilities::k_searchedFontAssets
	List_1_t1440998580 * ___k_searchedFontAssets_0;

public:
	inline static int32_t get_offset_of_k_searchedFontAssets_0() { return static_cast<int32_t>(offsetof(TMP_FontUtilities_t1411032097_StaticFields, ___k_searchedFontAssets_0)); }
	inline List_1_t1440998580 * get_k_searchedFontAssets_0() const { return ___k_searchedFontAssets_0; }
	inline List_1_t1440998580 ** get_address_of_k_searchedFontAssets_0() { return &___k_searchedFontAssets_0; }
	inline void set_k_searchedFontAssets_0(List_1_t1440998580 * value)
	{
		___k_searchedFontAssets_0 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedFontAssets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTUTILITIES_T1411032097_H
#ifndef BYTERANGE_T286214923_H
#define BYTERANGE_T286214923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.ByteRange
struct  ByteRange_t286214923  : public RuntimeObject
{
public:
	// System.Int64 Amazon.S3.Model.ByteRange::<Start>k__BackingField
	int64_t ___U3CStartU3Ek__BackingField_0;
	// System.Int64 Amazon.S3.Model.ByteRange::<End>k__BackingField
	int64_t ___U3CEndU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CStartU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ByteRange_t286214923, ___U3CStartU3Ek__BackingField_0)); }
	inline int64_t get_U3CStartU3Ek__BackingField_0() const { return ___U3CStartU3Ek__BackingField_0; }
	inline int64_t* get_address_of_U3CStartU3Ek__BackingField_0() { return &___U3CStartU3Ek__BackingField_0; }
	inline void set_U3CStartU3Ek__BackingField_0(int64_t value)
	{
		___U3CStartU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CEndU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ByteRange_t286214923, ___U3CEndU3Ek__BackingField_1)); }
	inline int64_t get_U3CEndU3Ek__BackingField_1() const { return ___U3CEndU3Ek__BackingField_1; }
	inline int64_t* get_address_of_U3CEndU3Ek__BackingField_1() { return &___U3CEndU3Ek__BackingField_1; }
	inline void set_U3CEndU3Ek__BackingField_1(int64_t value)
	{
		___U3CEndU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTERANGE_T286214923_H
#ifndef AMAZONS3URI_T3833005522_H
#define AMAZONS3URI_T3833005522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Util.AmazonS3Uri
struct  AmazonS3Uri_t3833005522  : public RuntimeObject
{
public:
	// System.Boolean Amazon.S3.Util.AmazonS3Uri::<IsPathStyle>k__BackingField
	bool ___U3CIsPathStyleU3Ek__BackingField_0;
	// System.String Amazon.S3.Util.AmazonS3Uri::<Bucket>k__BackingField
	String_t* ___U3CBucketU3Ek__BackingField_1;
	// System.String Amazon.S3.Util.AmazonS3Uri::<Key>k__BackingField
	String_t* ___U3CKeyU3Ek__BackingField_2;
	// Amazon.RegionEndpoint Amazon.S3.Util.AmazonS3Uri::<Region>k__BackingField
	RegionEndpoint_t661522805 * ___U3CRegionU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CIsPathStyleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AmazonS3Uri_t3833005522, ___U3CIsPathStyleU3Ek__BackingField_0)); }
	inline bool get_U3CIsPathStyleU3Ek__BackingField_0() const { return ___U3CIsPathStyleU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsPathStyleU3Ek__BackingField_0() { return &___U3CIsPathStyleU3Ek__BackingField_0; }
	inline void set_U3CIsPathStyleU3Ek__BackingField_0(bool value)
	{
		___U3CIsPathStyleU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CBucketU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AmazonS3Uri_t3833005522, ___U3CBucketU3Ek__BackingField_1)); }
	inline String_t* get_U3CBucketU3Ek__BackingField_1() const { return ___U3CBucketU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CBucketU3Ek__BackingField_1() { return &___U3CBucketU3Ek__BackingField_1; }
	inline void set_U3CBucketU3Ek__BackingField_1(String_t* value)
	{
		___U3CBucketU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBucketU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CKeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonS3Uri_t3833005522, ___U3CKeyU3Ek__BackingField_2)); }
	inline String_t* get_U3CKeyU3Ek__BackingField_2() const { return ___U3CKeyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CKeyU3Ek__BackingField_2() { return &___U3CKeyU3Ek__BackingField_2; }
	inline void set_U3CKeyU3Ek__BackingField_2(String_t* value)
	{
		___U3CKeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AmazonS3Uri_t3833005522, ___U3CRegionU3Ek__BackingField_3)); }
	inline RegionEndpoint_t661522805 * get_U3CRegionU3Ek__BackingField_3() const { return ___U3CRegionU3Ek__BackingField_3; }
	inline RegionEndpoint_t661522805 ** get_address_of_U3CRegionU3Ek__BackingField_3() { return &___U3CRegionU3Ek__BackingField_3; }
	inline void set_U3CRegionU3Ek__BackingField_3(RegionEndpoint_t661522805 * value)
	{
		___U3CRegionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRegionU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3URI_T3833005522_H
#ifndef DELETEDOBJECT_T2664222218_H
#define DELETEDOBJECT_T2664222218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.DeletedObject
struct  DeletedObject_t2664222218  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEDOBJECT_T2664222218_H
#ifndef S3CONSTANTS_T195587261_H
#define S3CONSTANTS_T195587261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Util.S3Constants
struct  S3Constants_t195587261  : public RuntimeObject
{
public:

public:
};

struct S3Constants_t195587261_StaticFields
{
public:
	// System.Int64 Amazon.S3.Util.S3Constants::MinPartSize
	int64_t ___MinPartSize_0;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataObjectKey
	String_t* ___PostFormDataObjectKey_1;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataAcl
	String_t* ___PostFormDataAcl_2;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataRedirect
	String_t* ___PostFormDataRedirect_3;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataStatus
	String_t* ___PostFormDataStatus_4;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataContentType
	String_t* ___PostFormDataContentType_5;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataMetaPrefix
	String_t* ___PostFormDataMetaPrefix_6;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataXAmzPrefix
	String_t* ___PostFormDataXAmzPrefix_7;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataAccessKeyId
	String_t* ___PostFormDataAccessKeyId_8;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataPolicy
	String_t* ___PostFormDataPolicy_9;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataSignature
	String_t* ___PostFormDataSignature_10;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataXAmzSignature
	String_t* ___PostFormDataXAmzSignature_11;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataXAmzAlgorithm
	String_t* ___PostFormDataXAmzAlgorithm_12;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataXAmzCredential
	String_t* ___PostFormDataXAmzCredential_13;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataXAmzDate
	String_t* ___PostFormDataXAmzDate_14;
	// System.String Amazon.S3.Util.S3Constants::PostFormDataSecurityToken
	String_t* ___PostFormDataSecurityToken_15;
	// System.String Amazon.S3.Util.S3Constants::AmzHeaderMultipartPartsCount
	String_t* ___AmzHeaderMultipartPartsCount_16;
	// System.String Amazon.S3.Util.S3Constants::AmzHeaderRequestPayer
	String_t* ___AmzHeaderRequestPayer_17;
	// System.String Amazon.S3.Util.S3Constants::AmzHeaderRequestCharged
	String_t* ___AmzHeaderRequestCharged_18;
	// System.String Amazon.S3.Util.S3Constants::AmzHeaderTagging
	String_t* ___AmzHeaderTagging_19;
	// System.String Amazon.S3.Util.S3Constants::AmzHeaderTaggingDirective
	String_t* ___AmzHeaderTaggingDirective_20;
	// System.String Amazon.S3.Util.S3Constants::AmzHeaderTaggingCount
	String_t* ___AmzHeaderTaggingCount_21;
	// System.String Amazon.S3.Util.S3Constants::AmzHeaderRestoreOutputPath
	String_t* ___AmzHeaderRestoreOutputPath_22;
	// System.String[] Amazon.S3.Util.S3Constants::BucketVersions
	StringU5BU5D_t1642385972* ___BucketVersions_23;
	// System.String[] Amazon.S3.Util.S3Constants::MetadataDirectives
	StringU5BU5D_t1642385972* ___MetadataDirectives_24;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.S3.Util.S3Constants::GetObjectExtraSubResources
	HashSet_1_t362681087 * ___GetObjectExtraSubResources_25;

public:
	inline static int32_t get_offset_of_MinPartSize_0() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___MinPartSize_0)); }
	inline int64_t get_MinPartSize_0() const { return ___MinPartSize_0; }
	inline int64_t* get_address_of_MinPartSize_0() { return &___MinPartSize_0; }
	inline void set_MinPartSize_0(int64_t value)
	{
		___MinPartSize_0 = value;
	}

	inline static int32_t get_offset_of_PostFormDataObjectKey_1() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataObjectKey_1)); }
	inline String_t* get_PostFormDataObjectKey_1() const { return ___PostFormDataObjectKey_1; }
	inline String_t** get_address_of_PostFormDataObjectKey_1() { return &___PostFormDataObjectKey_1; }
	inline void set_PostFormDataObjectKey_1(String_t* value)
	{
		___PostFormDataObjectKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataObjectKey_1), value);
	}

	inline static int32_t get_offset_of_PostFormDataAcl_2() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataAcl_2)); }
	inline String_t* get_PostFormDataAcl_2() const { return ___PostFormDataAcl_2; }
	inline String_t** get_address_of_PostFormDataAcl_2() { return &___PostFormDataAcl_2; }
	inline void set_PostFormDataAcl_2(String_t* value)
	{
		___PostFormDataAcl_2 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataAcl_2), value);
	}

	inline static int32_t get_offset_of_PostFormDataRedirect_3() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataRedirect_3)); }
	inline String_t* get_PostFormDataRedirect_3() const { return ___PostFormDataRedirect_3; }
	inline String_t** get_address_of_PostFormDataRedirect_3() { return &___PostFormDataRedirect_3; }
	inline void set_PostFormDataRedirect_3(String_t* value)
	{
		___PostFormDataRedirect_3 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataRedirect_3), value);
	}

	inline static int32_t get_offset_of_PostFormDataStatus_4() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataStatus_4)); }
	inline String_t* get_PostFormDataStatus_4() const { return ___PostFormDataStatus_4; }
	inline String_t** get_address_of_PostFormDataStatus_4() { return &___PostFormDataStatus_4; }
	inline void set_PostFormDataStatus_4(String_t* value)
	{
		___PostFormDataStatus_4 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataStatus_4), value);
	}

	inline static int32_t get_offset_of_PostFormDataContentType_5() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataContentType_5)); }
	inline String_t* get_PostFormDataContentType_5() const { return ___PostFormDataContentType_5; }
	inline String_t** get_address_of_PostFormDataContentType_5() { return &___PostFormDataContentType_5; }
	inline void set_PostFormDataContentType_5(String_t* value)
	{
		___PostFormDataContentType_5 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataContentType_5), value);
	}

	inline static int32_t get_offset_of_PostFormDataMetaPrefix_6() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataMetaPrefix_6)); }
	inline String_t* get_PostFormDataMetaPrefix_6() const { return ___PostFormDataMetaPrefix_6; }
	inline String_t** get_address_of_PostFormDataMetaPrefix_6() { return &___PostFormDataMetaPrefix_6; }
	inline void set_PostFormDataMetaPrefix_6(String_t* value)
	{
		___PostFormDataMetaPrefix_6 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataMetaPrefix_6), value);
	}

	inline static int32_t get_offset_of_PostFormDataXAmzPrefix_7() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataXAmzPrefix_7)); }
	inline String_t* get_PostFormDataXAmzPrefix_7() const { return ___PostFormDataXAmzPrefix_7; }
	inline String_t** get_address_of_PostFormDataXAmzPrefix_7() { return &___PostFormDataXAmzPrefix_7; }
	inline void set_PostFormDataXAmzPrefix_7(String_t* value)
	{
		___PostFormDataXAmzPrefix_7 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataXAmzPrefix_7), value);
	}

	inline static int32_t get_offset_of_PostFormDataAccessKeyId_8() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataAccessKeyId_8)); }
	inline String_t* get_PostFormDataAccessKeyId_8() const { return ___PostFormDataAccessKeyId_8; }
	inline String_t** get_address_of_PostFormDataAccessKeyId_8() { return &___PostFormDataAccessKeyId_8; }
	inline void set_PostFormDataAccessKeyId_8(String_t* value)
	{
		___PostFormDataAccessKeyId_8 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataAccessKeyId_8), value);
	}

	inline static int32_t get_offset_of_PostFormDataPolicy_9() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataPolicy_9)); }
	inline String_t* get_PostFormDataPolicy_9() const { return ___PostFormDataPolicy_9; }
	inline String_t** get_address_of_PostFormDataPolicy_9() { return &___PostFormDataPolicy_9; }
	inline void set_PostFormDataPolicy_9(String_t* value)
	{
		___PostFormDataPolicy_9 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataPolicy_9), value);
	}

	inline static int32_t get_offset_of_PostFormDataSignature_10() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataSignature_10)); }
	inline String_t* get_PostFormDataSignature_10() const { return ___PostFormDataSignature_10; }
	inline String_t** get_address_of_PostFormDataSignature_10() { return &___PostFormDataSignature_10; }
	inline void set_PostFormDataSignature_10(String_t* value)
	{
		___PostFormDataSignature_10 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataSignature_10), value);
	}

	inline static int32_t get_offset_of_PostFormDataXAmzSignature_11() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataXAmzSignature_11)); }
	inline String_t* get_PostFormDataXAmzSignature_11() const { return ___PostFormDataXAmzSignature_11; }
	inline String_t** get_address_of_PostFormDataXAmzSignature_11() { return &___PostFormDataXAmzSignature_11; }
	inline void set_PostFormDataXAmzSignature_11(String_t* value)
	{
		___PostFormDataXAmzSignature_11 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataXAmzSignature_11), value);
	}

	inline static int32_t get_offset_of_PostFormDataXAmzAlgorithm_12() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataXAmzAlgorithm_12)); }
	inline String_t* get_PostFormDataXAmzAlgorithm_12() const { return ___PostFormDataXAmzAlgorithm_12; }
	inline String_t** get_address_of_PostFormDataXAmzAlgorithm_12() { return &___PostFormDataXAmzAlgorithm_12; }
	inline void set_PostFormDataXAmzAlgorithm_12(String_t* value)
	{
		___PostFormDataXAmzAlgorithm_12 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataXAmzAlgorithm_12), value);
	}

	inline static int32_t get_offset_of_PostFormDataXAmzCredential_13() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataXAmzCredential_13)); }
	inline String_t* get_PostFormDataXAmzCredential_13() const { return ___PostFormDataXAmzCredential_13; }
	inline String_t** get_address_of_PostFormDataXAmzCredential_13() { return &___PostFormDataXAmzCredential_13; }
	inline void set_PostFormDataXAmzCredential_13(String_t* value)
	{
		___PostFormDataXAmzCredential_13 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataXAmzCredential_13), value);
	}

	inline static int32_t get_offset_of_PostFormDataXAmzDate_14() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataXAmzDate_14)); }
	inline String_t* get_PostFormDataXAmzDate_14() const { return ___PostFormDataXAmzDate_14; }
	inline String_t** get_address_of_PostFormDataXAmzDate_14() { return &___PostFormDataXAmzDate_14; }
	inline void set_PostFormDataXAmzDate_14(String_t* value)
	{
		___PostFormDataXAmzDate_14 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataXAmzDate_14), value);
	}

	inline static int32_t get_offset_of_PostFormDataSecurityToken_15() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___PostFormDataSecurityToken_15)); }
	inline String_t* get_PostFormDataSecurityToken_15() const { return ___PostFormDataSecurityToken_15; }
	inline String_t** get_address_of_PostFormDataSecurityToken_15() { return &___PostFormDataSecurityToken_15; }
	inline void set_PostFormDataSecurityToken_15(String_t* value)
	{
		___PostFormDataSecurityToken_15 = value;
		Il2CppCodeGenWriteBarrier((&___PostFormDataSecurityToken_15), value);
	}

	inline static int32_t get_offset_of_AmzHeaderMultipartPartsCount_16() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___AmzHeaderMultipartPartsCount_16)); }
	inline String_t* get_AmzHeaderMultipartPartsCount_16() const { return ___AmzHeaderMultipartPartsCount_16; }
	inline String_t** get_address_of_AmzHeaderMultipartPartsCount_16() { return &___AmzHeaderMultipartPartsCount_16; }
	inline void set_AmzHeaderMultipartPartsCount_16(String_t* value)
	{
		___AmzHeaderMultipartPartsCount_16 = value;
		Il2CppCodeGenWriteBarrier((&___AmzHeaderMultipartPartsCount_16), value);
	}

	inline static int32_t get_offset_of_AmzHeaderRequestPayer_17() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___AmzHeaderRequestPayer_17)); }
	inline String_t* get_AmzHeaderRequestPayer_17() const { return ___AmzHeaderRequestPayer_17; }
	inline String_t** get_address_of_AmzHeaderRequestPayer_17() { return &___AmzHeaderRequestPayer_17; }
	inline void set_AmzHeaderRequestPayer_17(String_t* value)
	{
		___AmzHeaderRequestPayer_17 = value;
		Il2CppCodeGenWriteBarrier((&___AmzHeaderRequestPayer_17), value);
	}

	inline static int32_t get_offset_of_AmzHeaderRequestCharged_18() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___AmzHeaderRequestCharged_18)); }
	inline String_t* get_AmzHeaderRequestCharged_18() const { return ___AmzHeaderRequestCharged_18; }
	inline String_t** get_address_of_AmzHeaderRequestCharged_18() { return &___AmzHeaderRequestCharged_18; }
	inline void set_AmzHeaderRequestCharged_18(String_t* value)
	{
		___AmzHeaderRequestCharged_18 = value;
		Il2CppCodeGenWriteBarrier((&___AmzHeaderRequestCharged_18), value);
	}

	inline static int32_t get_offset_of_AmzHeaderTagging_19() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___AmzHeaderTagging_19)); }
	inline String_t* get_AmzHeaderTagging_19() const { return ___AmzHeaderTagging_19; }
	inline String_t** get_address_of_AmzHeaderTagging_19() { return &___AmzHeaderTagging_19; }
	inline void set_AmzHeaderTagging_19(String_t* value)
	{
		___AmzHeaderTagging_19 = value;
		Il2CppCodeGenWriteBarrier((&___AmzHeaderTagging_19), value);
	}

	inline static int32_t get_offset_of_AmzHeaderTaggingDirective_20() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___AmzHeaderTaggingDirective_20)); }
	inline String_t* get_AmzHeaderTaggingDirective_20() const { return ___AmzHeaderTaggingDirective_20; }
	inline String_t** get_address_of_AmzHeaderTaggingDirective_20() { return &___AmzHeaderTaggingDirective_20; }
	inline void set_AmzHeaderTaggingDirective_20(String_t* value)
	{
		___AmzHeaderTaggingDirective_20 = value;
		Il2CppCodeGenWriteBarrier((&___AmzHeaderTaggingDirective_20), value);
	}

	inline static int32_t get_offset_of_AmzHeaderTaggingCount_21() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___AmzHeaderTaggingCount_21)); }
	inline String_t* get_AmzHeaderTaggingCount_21() const { return ___AmzHeaderTaggingCount_21; }
	inline String_t** get_address_of_AmzHeaderTaggingCount_21() { return &___AmzHeaderTaggingCount_21; }
	inline void set_AmzHeaderTaggingCount_21(String_t* value)
	{
		___AmzHeaderTaggingCount_21 = value;
		Il2CppCodeGenWriteBarrier((&___AmzHeaderTaggingCount_21), value);
	}

	inline static int32_t get_offset_of_AmzHeaderRestoreOutputPath_22() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___AmzHeaderRestoreOutputPath_22)); }
	inline String_t* get_AmzHeaderRestoreOutputPath_22() const { return ___AmzHeaderRestoreOutputPath_22; }
	inline String_t** get_address_of_AmzHeaderRestoreOutputPath_22() { return &___AmzHeaderRestoreOutputPath_22; }
	inline void set_AmzHeaderRestoreOutputPath_22(String_t* value)
	{
		___AmzHeaderRestoreOutputPath_22 = value;
		Il2CppCodeGenWriteBarrier((&___AmzHeaderRestoreOutputPath_22), value);
	}

	inline static int32_t get_offset_of_BucketVersions_23() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___BucketVersions_23)); }
	inline StringU5BU5D_t1642385972* get_BucketVersions_23() const { return ___BucketVersions_23; }
	inline StringU5BU5D_t1642385972** get_address_of_BucketVersions_23() { return &___BucketVersions_23; }
	inline void set_BucketVersions_23(StringU5BU5D_t1642385972* value)
	{
		___BucketVersions_23 = value;
		Il2CppCodeGenWriteBarrier((&___BucketVersions_23), value);
	}

	inline static int32_t get_offset_of_MetadataDirectives_24() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___MetadataDirectives_24)); }
	inline StringU5BU5D_t1642385972* get_MetadataDirectives_24() const { return ___MetadataDirectives_24; }
	inline StringU5BU5D_t1642385972** get_address_of_MetadataDirectives_24() { return &___MetadataDirectives_24; }
	inline void set_MetadataDirectives_24(StringU5BU5D_t1642385972* value)
	{
		___MetadataDirectives_24 = value;
		Il2CppCodeGenWriteBarrier((&___MetadataDirectives_24), value);
	}

	inline static int32_t get_offset_of_GetObjectExtraSubResources_25() { return static_cast<int32_t>(offsetof(S3Constants_t195587261_StaticFields, ___GetObjectExtraSubResources_25)); }
	inline HashSet_1_t362681087 * get_GetObjectExtraSubResources_25() const { return ___GetObjectExtraSubResources_25; }
	inline HashSet_1_t362681087 ** get_address_of_GetObjectExtraSubResources_25() { return &___GetObjectExtraSubResources_25; }
	inline void set_GetObjectExtraSubResources_25(HashSet_1_t362681087 * value)
	{
		___GetObjectExtraSubResources_25 = value;
		Il2CppCodeGenWriteBarrier((&___GetObjectExtraSubResources_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3CONSTANTS_T195587261_H
#ifndef HEADERSCOLLECTION_T2711555648_H
#define HEADERSCOLLECTION_T2711555648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.HeadersCollection
struct  HeadersCollection_t2711555648  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.S3.Model.HeadersCollection::_values
	RuntimeObject* ____values_0;

public:
	inline static int32_t get_offset_of__values_0() { return static_cast<int32_t>(offsetof(HeadersCollection_t2711555648, ____values_0)); }
	inline RuntimeObject* get__values_0() const { return ____values_0; }
	inline RuntimeObject** get_address_of__values_0() { return &____values_0; }
	inline void set__values_0(RuntimeObject* value)
	{
		____values_0 = value;
		Il2CppCodeGenWriteBarrier((&____values_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERSCOLLECTION_T2711555648_H
#ifndef AMAZONS3UTIL_T2934979980_H
#define AMAZONS3UTIL_T2934979980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Util.AmazonS3Util
struct  AmazonS3Util_t2934979980  : public RuntimeObject
{
public:

public:
};

struct AmazonS3Util_t2934979980_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.S3.Util.AmazonS3Util::extensionToMime
	Dictionary_2_t3943999495 * ___extensionToMime_0;

public:
	inline static int32_t get_offset_of_extensionToMime_0() { return static_cast<int32_t>(offsetof(AmazonS3Util_t2934979980_StaticFields, ___extensionToMime_0)); }
	inline Dictionary_2_t3943999495 * get_extensionToMime_0() const { return ___extensionToMime_0; }
	inline Dictionary_2_t3943999495 ** get_address_of_extensionToMime_0() { return &___extensionToMime_0; }
	inline void set_extensionToMime_0(Dictionary_2_t3943999495 * value)
	{
		___extensionToMime_0 = value;
		Il2CppCodeGenWriteBarrier((&___extensionToMime_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3UTIL_T2934979980_H
#ifndef OWNER_T97740679_H
#define OWNER_T97740679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Owner
struct  Owner_t97740679  : public RuntimeObject
{
public:
	// System.String Amazon.S3.Model.Owner::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_0;
	// System.String Amazon.S3.Model.Owner::<Id>k__BackingField
	String_t* ___U3CIdU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Owner_t97740679, ___U3CDisplayNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_0() const { return ___U3CDisplayNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_0() { return &___U3CDisplayNameU3Ek__BackingField_0; }
	inline void set_U3CDisplayNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Owner_t97740679, ___U3CIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OWNER_T97740679_H
#ifndef METADATACOLLECTION_T3745113723_H
#define METADATACOLLECTION_T3745113723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.MetadataCollection
struct  MetadataCollection_t3745113723  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.S3.Model.MetadataCollection::values
	RuntimeObject* ___values_1;

public:
	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(MetadataCollection_t3745113723, ___values_1)); }
	inline RuntimeObject* get_values_1() const { return ___values_1; }
	inline RuntimeObject** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(RuntimeObject* value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATACOLLECTION_T3745113723_H
#ifndef RESPONSEHEADEROVERRIDES_T382636191_H
#define RESPONSEHEADEROVERRIDES_T382636191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.ResponseHeaderOverrides
struct  ResponseHeaderOverrides_t382636191  : public RuntimeObject
{
public:
	// System.String Amazon.S3.Model.ResponseHeaderOverrides::_contentType
	String_t* ____contentType_6;
	// System.String Amazon.S3.Model.ResponseHeaderOverrides::_contentLanguage
	String_t* ____contentLanguage_7;
	// System.String Amazon.S3.Model.ResponseHeaderOverrides::_expires
	String_t* ____expires_8;
	// System.String Amazon.S3.Model.ResponseHeaderOverrides::_cacheControl
	String_t* ____cacheControl_9;
	// System.String Amazon.S3.Model.ResponseHeaderOverrides::_contentDisposition
	String_t* ____contentDisposition_10;
	// System.String Amazon.S3.Model.ResponseHeaderOverrides::_contentEncoding
	String_t* ____contentEncoding_11;

public:
	inline static int32_t get_offset_of__contentType_6() { return static_cast<int32_t>(offsetof(ResponseHeaderOverrides_t382636191, ____contentType_6)); }
	inline String_t* get__contentType_6() const { return ____contentType_6; }
	inline String_t** get_address_of__contentType_6() { return &____contentType_6; }
	inline void set__contentType_6(String_t* value)
	{
		____contentType_6 = value;
		Il2CppCodeGenWriteBarrier((&____contentType_6), value);
	}

	inline static int32_t get_offset_of__contentLanguage_7() { return static_cast<int32_t>(offsetof(ResponseHeaderOverrides_t382636191, ____contentLanguage_7)); }
	inline String_t* get__contentLanguage_7() const { return ____contentLanguage_7; }
	inline String_t** get_address_of__contentLanguage_7() { return &____contentLanguage_7; }
	inline void set__contentLanguage_7(String_t* value)
	{
		____contentLanguage_7 = value;
		Il2CppCodeGenWriteBarrier((&____contentLanguage_7), value);
	}

	inline static int32_t get_offset_of__expires_8() { return static_cast<int32_t>(offsetof(ResponseHeaderOverrides_t382636191, ____expires_8)); }
	inline String_t* get__expires_8() const { return ____expires_8; }
	inline String_t** get_address_of__expires_8() { return &____expires_8; }
	inline void set__expires_8(String_t* value)
	{
		____expires_8 = value;
		Il2CppCodeGenWriteBarrier((&____expires_8), value);
	}

	inline static int32_t get_offset_of__cacheControl_9() { return static_cast<int32_t>(offsetof(ResponseHeaderOverrides_t382636191, ____cacheControl_9)); }
	inline String_t* get__cacheControl_9() const { return ____cacheControl_9; }
	inline String_t** get_address_of__cacheControl_9() { return &____cacheControl_9; }
	inline void set__cacheControl_9(String_t* value)
	{
		____cacheControl_9 = value;
		Il2CppCodeGenWriteBarrier((&____cacheControl_9), value);
	}

	inline static int32_t get_offset_of__contentDisposition_10() { return static_cast<int32_t>(offsetof(ResponseHeaderOverrides_t382636191, ____contentDisposition_10)); }
	inline String_t* get__contentDisposition_10() const { return ____contentDisposition_10; }
	inline String_t** get_address_of__contentDisposition_10() { return &____contentDisposition_10; }
	inline void set__contentDisposition_10(String_t* value)
	{
		____contentDisposition_10 = value;
		Il2CppCodeGenWriteBarrier((&____contentDisposition_10), value);
	}

	inline static int32_t get_offset_of__contentEncoding_11() { return static_cast<int32_t>(offsetof(ResponseHeaderOverrides_t382636191, ____contentEncoding_11)); }
	inline String_t* get__contentEncoding_11() const { return ____contentEncoding_11; }
	inline String_t** get_address_of__contentEncoding_11() { return &____contentEncoding_11; }
	inline void set__contentEncoding_11(String_t* value)
	{
		____contentEncoding_11 = value;
		Il2CppCodeGenWriteBarrier((&____contentEncoding_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEHEADEROVERRIDES_T382636191_H
#ifndef DELETEERROR_T3399861423_H
#define DELETEERROR_T3399861423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.DeleteError
struct  DeleteError_t3399861423  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEERROR_T3399861423_H
#ifndef BUCKETREGIONDETECTOR_T3645418826_H
#define BUCKETREGIONDETECTOR_T3645418826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Util.BucketRegionDetector
struct  BucketRegionDetector_t3645418826  : public RuntimeObject
{
public:

public:
};

struct BucketRegionDetector_t3645418826_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.LruCache`2<System.String,Amazon.RegionEndpoint> Amazon.S3.Util.BucketRegionDetector::<BucketRegionCache>k__BackingField
	LruCache_2_t3782913914 * ___U3CBucketRegionCacheU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBucketRegionCacheU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BucketRegionDetector_t3645418826_StaticFields, ___U3CBucketRegionCacheU3Ek__BackingField_0)); }
	inline LruCache_2_t3782913914 * get_U3CBucketRegionCacheU3Ek__BackingField_0() const { return ___U3CBucketRegionCacheU3Ek__BackingField_0; }
	inline LruCache_2_t3782913914 ** get_address_of_U3CBucketRegionCacheU3Ek__BackingField_0() { return &___U3CBucketRegionCacheU3Ek__BackingField_0; }
	inline void set_U3CBucketRegionCacheU3Ek__BackingField_0(LruCache_2_t3782913914 * value)
	{
		___U3CBucketRegionCacheU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBucketRegionCacheU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUCKETREGIONDETECTOR_T3645418826_H
#ifndef TMP_WORDINFO_T3807457612_H
#define TMP_WORDINFO_T3807457612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t3807457612 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t1920000777 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3807457612, ___textComponent_0)); }
	inline TMP_Text_t1920000777 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t1920000777 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t1920000777 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3807457612, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3807457612, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t3807457612, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3807457612_marshaled_pinvoke
{
	TMP_Text_t1920000777 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t3807457612_marshaled_com
{
	TMP_Text_t1920000777 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T3807457612_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef TMP_GLYPH_T909793902_H
#define TMP_GLYPH_T909793902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Glyph
struct  TMP_Glyph_t909793902  : public TMP_TextElement_t2285620223
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_GLYPH_T909793902_H
#ifndef TMP_BASICXMLTAGSTACK_T937156555_H
#define TMP_BASICXMLTAGSTACK_T937156555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t937156555 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T937156555_H
#ifndef TMP_SPRITEINFO_T124011303_H
#define TMP_SPRITEINFO_T124011303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t124011303 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t124011303, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t124011303, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t124011303, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T124011303_H
#ifndef NULLABLE_1_T2088641033_H
#define NULLABLE_1_T2088641033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t2088641033 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2088641033_H
#ifndef NULLABLE_1_T3467111648_H
#define NULLABLE_1_T3467111648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t3467111648 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3467111648, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3467111648, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3467111648_H
#ifndef TAGATTRIBUTE_T1488719162_H
#define TAGATTRIBUTE_T1488719162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t1488719162 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t1488719162, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t1488719162, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t1488719162, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T1488719162_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef TMP_XMLTAGSTACK_1_T2730429967_H
#define TMP_XMLTAGSTACK_1_T2730429967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2730429967 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t3030399641* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2730429967, ___itemStack_0)); }
	inline Int32U5BU5D_t3030399641* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t3030399641* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2730429967, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2730429967, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2730429967, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2730429967_H
#ifndef TMP_PAGEINFO_T3845132337_H
#define TMP_PAGEINFO_T3845132337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t3845132337 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t3845132337, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t3845132337, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t3845132337, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t3845132337, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t3845132337, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T3845132337_H
#ifndef TMP_XMLTAGSTACK_1_T1818389866_H
#define TMP_XMLTAGSTACK_1_T1818389866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t1818389866 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2828559218* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_t1159837347 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1818389866, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t2828559218* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t2828559218** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t2828559218* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1818389866, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1818389866, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1818389866, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_t1159837347 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_t1159837347 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_t1159837347 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1818389866_H
#ifndef DELETEBUCKETREQUEST_T3161332728_H
#define DELETEBUCKETREQUEST_T3161332728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.DeleteBucketRequest
struct  DeleteBucketRequest_t3161332728  : public AmazonWebServiceRequest_t3384026212
{
public:
	// Amazon.S3.S3Region Amazon.S3.Model.DeleteBucketRequest::bucketRegion
	S3Region_t723338532 * ___bucketRegion_4;
	// System.Boolean Amazon.S3.Model.DeleteBucketRequest::useClientRegion
	bool ___useClientRegion_5;

public:
	inline static int32_t get_offset_of_bucketRegion_4() { return static_cast<int32_t>(offsetof(DeleteBucketRequest_t3161332728, ___bucketRegion_4)); }
	inline S3Region_t723338532 * get_bucketRegion_4() const { return ___bucketRegion_4; }
	inline S3Region_t723338532 ** get_address_of_bucketRegion_4() { return &___bucketRegion_4; }
	inline void set_bucketRegion_4(S3Region_t723338532 * value)
	{
		___bucketRegion_4 = value;
		Il2CppCodeGenWriteBarrier((&___bucketRegion_4), value);
	}

	inline static int32_t get_offset_of_useClientRegion_5() { return static_cast<int32_t>(offsetof(DeleteBucketRequest_t3161332728, ___useClientRegion_5)); }
	inline bool get_useClientRegion_5() const { return ___useClientRegion_5; }
	inline bool* get_address_of_useClientRegion_5() { return &___useClientRegion_5; }
	inline void set_useClientRegion_5(bool value)
	{
		___useClientRegion_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEBUCKETREQUEST_T3161332728_H
#ifndef TMP_XMLTAGSTACK_1_T2735062451_H
#define TMP_XMLTAGSTACK_1_T2735062451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t2735062451 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t577127397* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2735062451, ___itemStack_0)); }
	inline SingleU5BU5D_t577127397* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t577127397** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t577127397* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2735062451, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2735062451, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2735062451, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2735062451_H
#ifndef KERNINGPAIRKEY_T3915473665_H
#define KERNINGPAIRKEY_T3915473665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPairKey
struct  KerningPairKey_t3915473665 
{
public:
	// System.UInt32 TMPro.KerningPairKey::ascii_Left
	uint32_t ___ascii_Left_0;
	// System.UInt32 TMPro.KerningPairKey::ascii_Right
	uint32_t ___ascii_Right_1;
	// System.UInt32 TMPro.KerningPairKey::key
	uint32_t ___key_2;

public:
	inline static int32_t get_offset_of_ascii_Left_0() { return static_cast<int32_t>(offsetof(KerningPairKey_t3915473665, ___ascii_Left_0)); }
	inline uint32_t get_ascii_Left_0() const { return ___ascii_Left_0; }
	inline uint32_t* get_address_of_ascii_Left_0() { return &___ascii_Left_0; }
	inline void set_ascii_Left_0(uint32_t value)
	{
		___ascii_Left_0 = value;
	}

	inline static int32_t get_offset_of_ascii_Right_1() { return static_cast<int32_t>(offsetof(KerningPairKey_t3915473665, ___ascii_Right_1)); }
	inline uint32_t get_ascii_Right_1() const { return ___ascii_Right_1; }
	inline uint32_t* get_address_of_ascii_Right_1() { return &___ascii_Right_1; }
	inline void set_ascii_Right_1(uint32_t value)
	{
		___ascii_Right_1 = value;
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(KerningPairKey_t3915473665, ___key_2)); }
	inline uint32_t get_key_2() const { return ___key_2; }
	inline uint32_t* get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(uint32_t value)
	{
		___key_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIRKEY_T3915473665_H
#ifndef FONTCREATIONSETTING_T1093397046_H
#define FONTCREATIONSETTING_T1093397046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontCreationSetting
struct  FontCreationSetting_t1093397046 
{
public:
	// System.String TMPro.FontCreationSetting::fontSourcePath
	String_t* ___fontSourcePath_0;
	// System.Int32 TMPro.FontCreationSetting::fontSizingMode
	int32_t ___fontSizingMode_1;
	// System.Int32 TMPro.FontCreationSetting::fontSize
	int32_t ___fontSize_2;
	// System.Int32 TMPro.FontCreationSetting::fontPadding
	int32_t ___fontPadding_3;
	// System.Int32 TMPro.FontCreationSetting::fontPackingMode
	int32_t ___fontPackingMode_4;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasWidth
	int32_t ___fontAtlasWidth_5;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasHeight
	int32_t ___fontAtlasHeight_6;
	// System.Int32 TMPro.FontCreationSetting::fontCharacterSet
	int32_t ___fontCharacterSet_7;
	// System.Int32 TMPro.FontCreationSetting::fontStyle
	int32_t ___fontStyle_8;
	// System.Single TMPro.FontCreationSetting::fontStlyeModifier
	float ___fontStlyeModifier_9;
	// System.Int32 TMPro.FontCreationSetting::fontRenderMode
	int32_t ___fontRenderMode_10;
	// System.Boolean TMPro.FontCreationSetting::fontKerning
	bool ___fontKerning_11;

public:
	inline static int32_t get_offset_of_fontSourcePath_0() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontSourcePath_0)); }
	inline String_t* get_fontSourcePath_0() const { return ___fontSourcePath_0; }
	inline String_t** get_address_of_fontSourcePath_0() { return &___fontSourcePath_0; }
	inline void set_fontSourcePath_0(String_t* value)
	{
		___fontSourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___fontSourcePath_0), value);
	}

	inline static int32_t get_offset_of_fontSizingMode_1() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontSizingMode_1)); }
	inline int32_t get_fontSizingMode_1() const { return ___fontSizingMode_1; }
	inline int32_t* get_address_of_fontSizingMode_1() { return &___fontSizingMode_1; }
	inline void set_fontSizingMode_1(int32_t value)
	{
		___fontSizingMode_1 = value;
	}

	inline static int32_t get_offset_of_fontSize_2() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontSize_2)); }
	inline int32_t get_fontSize_2() const { return ___fontSize_2; }
	inline int32_t* get_address_of_fontSize_2() { return &___fontSize_2; }
	inline void set_fontSize_2(int32_t value)
	{
		___fontSize_2 = value;
	}

	inline static int32_t get_offset_of_fontPadding_3() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontPadding_3)); }
	inline int32_t get_fontPadding_3() const { return ___fontPadding_3; }
	inline int32_t* get_address_of_fontPadding_3() { return &___fontPadding_3; }
	inline void set_fontPadding_3(int32_t value)
	{
		___fontPadding_3 = value;
	}

	inline static int32_t get_offset_of_fontPackingMode_4() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontPackingMode_4)); }
	inline int32_t get_fontPackingMode_4() const { return ___fontPackingMode_4; }
	inline int32_t* get_address_of_fontPackingMode_4() { return &___fontPackingMode_4; }
	inline void set_fontPackingMode_4(int32_t value)
	{
		___fontPackingMode_4 = value;
	}

	inline static int32_t get_offset_of_fontAtlasWidth_5() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontAtlasWidth_5)); }
	inline int32_t get_fontAtlasWidth_5() const { return ___fontAtlasWidth_5; }
	inline int32_t* get_address_of_fontAtlasWidth_5() { return &___fontAtlasWidth_5; }
	inline void set_fontAtlasWidth_5(int32_t value)
	{
		___fontAtlasWidth_5 = value;
	}

	inline static int32_t get_offset_of_fontAtlasHeight_6() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontAtlasHeight_6)); }
	inline int32_t get_fontAtlasHeight_6() const { return ___fontAtlasHeight_6; }
	inline int32_t* get_address_of_fontAtlasHeight_6() { return &___fontAtlasHeight_6; }
	inline void set_fontAtlasHeight_6(int32_t value)
	{
		___fontAtlasHeight_6 = value;
	}

	inline static int32_t get_offset_of_fontCharacterSet_7() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontCharacterSet_7)); }
	inline int32_t get_fontCharacterSet_7() const { return ___fontCharacterSet_7; }
	inline int32_t* get_address_of_fontCharacterSet_7() { return &___fontCharacterSet_7; }
	inline void set_fontCharacterSet_7(int32_t value)
	{
		___fontCharacterSet_7 = value;
	}

	inline static int32_t get_offset_of_fontStyle_8() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontStyle_8)); }
	inline int32_t get_fontStyle_8() const { return ___fontStyle_8; }
	inline int32_t* get_address_of_fontStyle_8() { return &___fontStyle_8; }
	inline void set_fontStyle_8(int32_t value)
	{
		___fontStyle_8 = value;
	}

	inline static int32_t get_offset_of_fontStlyeModifier_9() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontStlyeModifier_9)); }
	inline float get_fontStlyeModifier_9() const { return ___fontStlyeModifier_9; }
	inline float* get_address_of_fontStlyeModifier_9() { return &___fontStlyeModifier_9; }
	inline void set_fontStlyeModifier_9(float value)
	{
		___fontStlyeModifier_9 = value;
	}

	inline static int32_t get_offset_of_fontRenderMode_10() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontRenderMode_10)); }
	inline int32_t get_fontRenderMode_10() const { return ___fontRenderMode_10; }
	inline int32_t* get_address_of_fontRenderMode_10() { return &___fontRenderMode_10; }
	inline void set_fontRenderMode_10(int32_t value)
	{
		___fontRenderMode_10 = value;
	}

	inline static int32_t get_offset_of_fontKerning_11() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontKerning_11)); }
	inline bool get_fontKerning_11() const { return ___fontKerning_11; }
	inline bool* get_address_of_fontKerning_11() { return &___fontKerning_11; }
	inline void set_fontKerning_11(bool value)
	{
		___fontKerning_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t1093397046_marshaled_pinvoke
{
	char* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
// Native definition for COM marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t1093397046_marshaled_com
{
	Il2CppChar* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
#endif // FONTCREATIONSETTING_T1093397046_H
#ifndef TMP_LINKINFO_T3626894960_H
#define TMP_LINKINFO_T3626894960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t3626894960 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t1920000777 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t1328083999* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t3626894960, ___textComponent_0)); }
	inline TMP_Text_t1920000777 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t1920000777 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t1920000777 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t3626894960, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t3626894960, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t3626894960, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t3626894960, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t3626894960, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t3626894960, ___linkID_6)); }
	inline CharU5BU5D_t1328083999* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t1328083999** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t1328083999* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t3626894960_marshaled_pinvoke
{
	TMP_Text_t1920000777 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t3626894960_marshaled_com
{
	TMP_Text_t1920000777 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T3626894960_H
#ifndef NULLABLE_1_T334943763_H
#define NULLABLE_1_T334943763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t334943763 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t334943763, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t334943763, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T334943763_H
#ifndef GLYPHVALUERECORD_T3876405180_H
#define GLYPHVALUERECORD_T3876405180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.GlyphValueRecord
struct  GlyphValueRecord_t3876405180 
{
public:
	// System.Single TMPro.GlyphValueRecord::xPlacement
	float ___xPlacement_0;
	// System.Single TMPro.GlyphValueRecord::yPlacement
	float ___yPlacement_1;
	// System.Single TMPro.GlyphValueRecord::xAdvance
	float ___xAdvance_2;
	// System.Single TMPro.GlyphValueRecord::yAdvance
	float ___yAdvance_3;

public:
	inline static int32_t get_offset_of_xPlacement_0() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t3876405180, ___xPlacement_0)); }
	inline float get_xPlacement_0() const { return ___xPlacement_0; }
	inline float* get_address_of_xPlacement_0() { return &___xPlacement_0; }
	inline void set_xPlacement_0(float value)
	{
		___xPlacement_0 = value;
	}

	inline static int32_t get_offset_of_yPlacement_1() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t3876405180, ___yPlacement_1)); }
	inline float get_yPlacement_1() const { return ___yPlacement_1; }
	inline float* get_address_of_yPlacement_1() { return &___yPlacement_1; }
	inline void set_yPlacement_1(float value)
	{
		___yPlacement_1 = value;
	}

	inline static int32_t get_offset_of_xAdvance_2() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t3876405180, ___xAdvance_2)); }
	inline float get_xAdvance_2() const { return ___xAdvance_2; }
	inline float* get_address_of_xAdvance_2() { return &___xAdvance_2; }
	inline void set_xAdvance_2(float value)
	{
		___xAdvance_2 = value;
	}

	inline static int32_t get_offset_of_yAdvance_3() { return static_cast<int32_t>(offsetof(GlyphValueRecord_t3876405180, ___yAdvance_3)); }
	inline float get_yAdvance_3() const { return ___yAdvance_3; }
	inline float* get_address_of_yAdvance_3() { return &___yAdvance_3; }
	inline void set_yAdvance_3(float value)
	{
		___yAdvance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHVALUERECORD_T3876405180_H
#ifndef MATRIX4X4_T2933234003_H
#define MATRIX4X4_T2933234003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2933234003 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2933234003_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2933234003  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2933234003  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2933234003  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2933234003 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2933234003  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2933234003  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2933234003 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2933234003  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2933234003_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef AMAZONS3KMSHANDLER_T1831047167_H
#define AMAZONS3KMSHANDLER_T1831047167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.AmazonS3KmsHandler
struct  AmazonS3KmsHandler_t1831047167  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3KMSHANDLER_T1831047167_H
#ifndef AMAZONS3PREMARSHALLHANDLER_T3687553405_H
#define AMAZONS3PREMARSHALLHANDLER_T3687553405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.AmazonS3PreMarshallHandler
struct  AmazonS3PreMarshallHandler_t3687553405  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3PREMARSHALLHANDLER_T3687553405_H
#ifndef AMAZONS3POSTMARSHALLHANDLER_T3097908696_H
#define AMAZONS3POSTMARSHALLHANDLER_T3097908696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.AmazonS3PostMarshallHandler
struct  AmazonS3PostMarshallHandler_t3097908696  : public PipelineHandler_t1486422324
{
public:

public:
};

struct AmazonS3PostMarshallHandler_t3097908696_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.Type> Amazon.S3.Internal.AmazonS3PostMarshallHandler::UnsupportedAccelerateRequestTypes
	HashSet_1_t3932231376 * ___UnsupportedAccelerateRequestTypes_3;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.S3.Internal.AmazonS3PostMarshallHandler::sseKeyHeaders
	HashSet_1_t362681087 * ___sseKeyHeaders_4;
	// System.Char[] Amazon.S3.Internal.AmazonS3PostMarshallHandler::separators
	CharU5BU5D_t1328083999* ___separators_5;
	// System.Text.RegularExpressions.Regex Amazon.S3.Internal.AmazonS3PostMarshallHandler::bucketValidationRegex
	Regex_t1803876613 * ___bucketValidationRegex_6;
	// System.Text.RegularExpressions.Regex Amazon.S3.Internal.AmazonS3PostMarshallHandler::dnsValidationRegex1
	Regex_t1803876613 * ___dnsValidationRegex1_7;
	// System.Text.RegularExpressions.Regex Amazon.S3.Internal.AmazonS3PostMarshallHandler::dnsValidationRegex2
	Regex_t1803876613 * ___dnsValidationRegex2_8;
	// System.String[] Amazon.S3.Internal.AmazonS3PostMarshallHandler::invalidPatterns
	StringU5BU5D_t1642385972* ___invalidPatterns_9;

public:
	inline static int32_t get_offset_of_UnsupportedAccelerateRequestTypes_3() { return static_cast<int32_t>(offsetof(AmazonS3PostMarshallHandler_t3097908696_StaticFields, ___UnsupportedAccelerateRequestTypes_3)); }
	inline HashSet_1_t3932231376 * get_UnsupportedAccelerateRequestTypes_3() const { return ___UnsupportedAccelerateRequestTypes_3; }
	inline HashSet_1_t3932231376 ** get_address_of_UnsupportedAccelerateRequestTypes_3() { return &___UnsupportedAccelerateRequestTypes_3; }
	inline void set_UnsupportedAccelerateRequestTypes_3(HashSet_1_t3932231376 * value)
	{
		___UnsupportedAccelerateRequestTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___UnsupportedAccelerateRequestTypes_3), value);
	}

	inline static int32_t get_offset_of_sseKeyHeaders_4() { return static_cast<int32_t>(offsetof(AmazonS3PostMarshallHandler_t3097908696_StaticFields, ___sseKeyHeaders_4)); }
	inline HashSet_1_t362681087 * get_sseKeyHeaders_4() const { return ___sseKeyHeaders_4; }
	inline HashSet_1_t362681087 ** get_address_of_sseKeyHeaders_4() { return &___sseKeyHeaders_4; }
	inline void set_sseKeyHeaders_4(HashSet_1_t362681087 * value)
	{
		___sseKeyHeaders_4 = value;
		Il2CppCodeGenWriteBarrier((&___sseKeyHeaders_4), value);
	}

	inline static int32_t get_offset_of_separators_5() { return static_cast<int32_t>(offsetof(AmazonS3PostMarshallHandler_t3097908696_StaticFields, ___separators_5)); }
	inline CharU5BU5D_t1328083999* get_separators_5() const { return ___separators_5; }
	inline CharU5BU5D_t1328083999** get_address_of_separators_5() { return &___separators_5; }
	inline void set_separators_5(CharU5BU5D_t1328083999* value)
	{
		___separators_5 = value;
		Il2CppCodeGenWriteBarrier((&___separators_5), value);
	}

	inline static int32_t get_offset_of_bucketValidationRegex_6() { return static_cast<int32_t>(offsetof(AmazonS3PostMarshallHandler_t3097908696_StaticFields, ___bucketValidationRegex_6)); }
	inline Regex_t1803876613 * get_bucketValidationRegex_6() const { return ___bucketValidationRegex_6; }
	inline Regex_t1803876613 ** get_address_of_bucketValidationRegex_6() { return &___bucketValidationRegex_6; }
	inline void set_bucketValidationRegex_6(Regex_t1803876613 * value)
	{
		___bucketValidationRegex_6 = value;
		Il2CppCodeGenWriteBarrier((&___bucketValidationRegex_6), value);
	}

	inline static int32_t get_offset_of_dnsValidationRegex1_7() { return static_cast<int32_t>(offsetof(AmazonS3PostMarshallHandler_t3097908696_StaticFields, ___dnsValidationRegex1_7)); }
	inline Regex_t1803876613 * get_dnsValidationRegex1_7() const { return ___dnsValidationRegex1_7; }
	inline Regex_t1803876613 ** get_address_of_dnsValidationRegex1_7() { return &___dnsValidationRegex1_7; }
	inline void set_dnsValidationRegex1_7(Regex_t1803876613 * value)
	{
		___dnsValidationRegex1_7 = value;
		Il2CppCodeGenWriteBarrier((&___dnsValidationRegex1_7), value);
	}

	inline static int32_t get_offset_of_dnsValidationRegex2_8() { return static_cast<int32_t>(offsetof(AmazonS3PostMarshallHandler_t3097908696_StaticFields, ___dnsValidationRegex2_8)); }
	inline Regex_t1803876613 * get_dnsValidationRegex2_8() const { return ___dnsValidationRegex2_8; }
	inline Regex_t1803876613 ** get_address_of_dnsValidationRegex2_8() { return &___dnsValidationRegex2_8; }
	inline void set_dnsValidationRegex2_8(Regex_t1803876613 * value)
	{
		___dnsValidationRegex2_8 = value;
		Il2CppCodeGenWriteBarrier((&___dnsValidationRegex2_8), value);
	}

	inline static int32_t get_offset_of_invalidPatterns_9() { return static_cast<int32_t>(offsetof(AmazonS3PostMarshallHandler_t3097908696_StaticFields, ___invalidPatterns_9)); }
	inline StringU5BU5D_t1642385972* get_invalidPatterns_9() const { return ___invalidPatterns_9; }
	inline StringU5BU5D_t1642385972** get_address_of_invalidPatterns_9() { return &___invalidPatterns_9; }
	inline void set_invalidPatterns_9(StringU5BU5D_t1642385972* value)
	{
		___invalidPatterns_9 = value;
		Il2CppCodeGenWriteBarrier((&___invalidPatterns_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3POSTMARSHALLHANDLER_T3097908696_H
#ifndef AMAZONS3EXCEPTIONHANDLER_T3293121943_H
#define AMAZONS3EXCEPTIONHANDLER_T3293121943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.AmazonS3ExceptionHandler
struct  AmazonS3ExceptionHandler_t3293121943  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3EXCEPTIONHANDLER_T3293121943_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef PUTWITHACLREQUEST_T3114873226_H
#define PUTWITHACLREQUEST_T3114873226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.PutWithACLRequest
struct  PutWithACLRequest_t3114873226  : public AmazonWebServiceRequest_t3384026212
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTWITHACLREQUEST_T3114873226_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef COMPLETEMULTIPARTUPLOADREQUEST_T2028871123_H
#define COMPLETEMULTIPARTUPLOADREQUEST_T2028871123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.CompleteMultipartUploadRequest
struct  CompleteMultipartUploadRequest_t2028871123  : public AmazonWebServiceRequest_t3384026212
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETEMULTIPARTUPLOADREQUEST_T2028871123_H
#ifndef LISTBUCKETSREQUEST_T1957637484_H
#define LISTBUCKETSREQUEST_T1957637484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.ListBucketsRequest
struct  ListBucketsRequest_t1957637484  : public AmazonWebServiceRequest_t3384026212
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTBUCKETSREQUEST_T1957637484_H
#ifndef XMLRESPONSEUNMARSHALLER_T760346204_H
#define XMLRESPONSEUNMARSHALLER_T760346204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.XmlResponseUnmarshaller
struct  XmlResponseUnmarshaller_t760346204  : public ResponseUnmarshaller_t3934041557
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRESPONSEUNMARSHALLER_T760346204_H
#ifndef MATERIALREFERENCE_T2854353496_H
#define MATERIALREFERENCE_T2854353496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t2854353496 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t2530419979 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t193706927 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t193706927 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___fontAsset_1)); }
	inline TMP_FontAsset_t2530419979 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t2530419979 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t2641813093 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t2641813093 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___material_3)); }
	inline Material_t193706927 * get_material_3() const { return ___material_3; }
	inline Material_t193706927 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t193706927 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___fallbackMaterial_6)); }
	inline Material_t193706927 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t193706927 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t193706927 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t2854353496_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t2530419979 * ___fontAsset_1;
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_2;
	Material_t193706927 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t193706927 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t2854353496_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t2530419979 * ___fontAsset_1;
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_2;
	Material_t193706927 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t193706927 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T2854353496_H
#ifndef REDIRECTHANDLER_T32990664_H
#define REDIRECTHANDLER_T32990664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RedirectHandler
struct  RedirectHandler_t32990664  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REDIRECTHANDLER_T32990664_H
#ifndef AMAZONS3RESPONSEHANDLER_T1411073907_H
#define AMAZONS3RESPONSEHANDLER_T1411073907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.AmazonS3ResponseHandler
struct  AmazonS3ResponseHandler_t1411073907  : public PipelineHandler_t1486422324
{
public:

public:
};

struct AmazonS3ResponseHandler_t1411073907_StaticFields
{
public:
	// System.Char[] Amazon.S3.Internal.AmazonS3ResponseHandler::etagTrimChars
	CharU5BU5D_t1328083999* ___etagTrimChars_3;

public:
	inline static int32_t get_offset_of_etagTrimChars_3() { return static_cast<int32_t>(offsetof(AmazonS3ResponseHandler_t1411073907_StaticFields, ___etagTrimChars_3)); }
	inline CharU5BU5D_t1328083999* get_etagTrimChars_3() const { return ___etagTrimChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_etagTrimChars_3() { return &___etagTrimChars_3; }
	inline void set_etagTrimChars_3(CharU5BU5D_t1328083999* value)
	{
		___etagTrimChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___etagTrimChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3RESPONSEHANDLER_T1411073907_H
#ifndef S3SIGNER_T70082942_H
#define S3SIGNER_T70082942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.S3Signer
struct  S3Signer_t70082942  : public AbstractAWSSigner_t2114314031
{
public:
	// System.Boolean Amazon.S3.Internal.S3Signer::_useSigV4
	bool ____useSigV4_1;

public:
	inline static int32_t get_offset_of__useSigV4_1() { return static_cast<int32_t>(offsetof(S3Signer_t70082942, ____useSigV4_1)); }
	inline bool get__useSigV4_1() const { return ____useSigV4_1; }
	inline bool* get_address_of__useSigV4_1() { return &____useSigV4_1; }
	inline void set__useSigV4_1(bool value)
	{
		____useSigV4_1 = value;
	}
};

struct S3Signer_t70082942_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> Amazon.S3.Internal.S3Signer::SignableParameters
	HashSet_1_t362681087 * ___SignableParameters_2;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.S3.Internal.S3Signer::SubResourcesSigningExclusion
	HashSet_1_t362681087 * ___SubResourcesSigningExclusion_3;

public:
	inline static int32_t get_offset_of_SignableParameters_2() { return static_cast<int32_t>(offsetof(S3Signer_t70082942_StaticFields, ___SignableParameters_2)); }
	inline HashSet_1_t362681087 * get_SignableParameters_2() const { return ___SignableParameters_2; }
	inline HashSet_1_t362681087 ** get_address_of_SignableParameters_2() { return &___SignableParameters_2; }
	inline void set_SignableParameters_2(HashSet_1_t362681087 * value)
	{
		___SignableParameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___SignableParameters_2), value);
	}

	inline static int32_t get_offset_of_SubResourcesSigningExclusion_3() { return static_cast<int32_t>(offsetof(S3Signer_t70082942_StaticFields, ___SubResourcesSigningExclusion_3)); }
	inline HashSet_1_t362681087 * get_SubResourcesSigningExclusion_3() const { return ___SubResourcesSigningExclusion_3; }
	inline HashSet_1_t362681087 ** get_address_of_SubResourcesSigningExclusion_3() { return &___SubResourcesSigningExclusion_3; }
	inline void set_SubResourcesSigningExclusion_3(HashSet_1_t362681087 * value)
	{
		___SubResourcesSigningExclusion_3 = value;
		Il2CppCodeGenWriteBarrier((&___SubResourcesSigningExclusion_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3SIGNER_T70082942_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef AMAZONS3HTTPDELETEHANDLER_T3511250707_H
#define AMAZONS3HTTPDELETEHANDLER_T3511250707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.AmazonS3HttpDeleteHandler
struct  AmazonS3HttpDeleteHandler_t3511250707  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3HTTPDELETEHANDLER_T3511250707_H
#ifndef MASKINGTYPES_T259687445_H
#define MASKINGTYPES_T259687445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t259687445 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingTypes_t259687445, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T259687445_H
#ifndef BOUNDS_T3033363703_H
#define BOUNDS_T3033363703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3033363703 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t2243707580  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t2243707580  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Center_0)); }
	inline Vector3_t2243707580  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t2243707580 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t2243707580  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Extents_1)); }
	inline Vector3_t2243707580  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t2243707580 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t2243707580  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3033363703_H
#ifndef TMP_XMLTAGSTACK_1_T3512906015_H
#define TMP_XMLTAGSTACK_1_T3512906015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t3512906015 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t627890505* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t2854353496  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3512906015, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t627890505* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t627890505** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t627890505* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3512906015, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3512906015, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3512906015, ___m_defaultItem_3)); }
	inline MaterialReference_t2854353496  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t2854353496 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t2854353496  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3512906015_H
#ifndef COPYOBJECTREQUEST_T3530746871_H
#define COPYOBJECTREQUEST_T3530746871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.CopyObjectRequest
struct  CopyObjectRequest_t3530746871  : public PutWithACLRequest_t3114873226
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYOBJECTREQUEST_T3530746871_H
#ifndef FONTSTYLES_T3171728781_H
#define FONTSTYLES_T3171728781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3171728781 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3171728781, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3171728781_H
#ifndef COPYPARTREQUEST_T1158344445_H
#define COPYPARTREQUEST_T1158344445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.CopyPartRequest
struct  CopyPartRequest_t1158344445  : public AmazonWebServiceRequest_t3384026212
{
public:
	// System.Nullable`1<System.Int32> Amazon.S3.Model.CopyPartRequest::partNumber
	Nullable_1_t334943763  ___partNumber_4;

public:
	inline static int32_t get_offset_of_partNumber_4() { return static_cast<int32_t>(offsetof(CopyPartRequest_t1158344445, ___partNumber_4)); }
	inline Nullable_1_t334943763  get_partNumber_4() const { return ___partNumber_4; }
	inline Nullable_1_t334943763 * get_address_of_partNumber_4() { return &___partNumber_4; }
	inline void set_partNumber_4(Nullable_1_t334943763  value)
	{
		___partNumber_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYPARTREQUEST_T1158344445_H
#ifndef TMP_TEXTELEMENTTYPE_T1959651783_H
#define TMP_TEXTELEMENTTYPE_T1959651783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1959651783 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1959651783, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1959651783_H
#ifndef TMP_XMLTAGSTACK_1_T1533070037_H
#define TMP_XMLTAGSTACK_1_T1533070037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t1533070037 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t30278651* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t874517518  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1533070037, ___itemStack_0)); }
	inline Color32U5BU5D_t30278651* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t30278651** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t30278651* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1533070037, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1533070037, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1533070037, ___m_defaultItem_3)); }
	inline Color32_t874517518  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t874517518 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t874517518  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1533070037_H
#ifndef AMAZONS3REDIRECTHANDLER_T470844594_H
#define AMAZONS3REDIRECTHANDLER_T470844594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.AmazonS3RedirectHandler
struct  AmazonS3RedirectHandler_t470844594  : public RedirectHandler_t32990664
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3REDIRECTHANDLER_T470844594_H
#ifndef TEXTALIGNMENTOPTIONS_T1466788324_H
#define TEXTALIGNMENTOPTIONS_T1466788324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t1466788324 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t1466788324, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T1466788324_H
#ifndef PUTBUCKETREQUEST_T1453664272_H
#define PUTBUCKETREQUEST_T1453664272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.PutBucketRequest
struct  PutBucketRequest_t1453664272  : public PutWithACLRequest_t3114873226
{
public:
	// Amazon.S3.S3Region Amazon.S3.Model.PutBucketRequest::bucketRegion
	S3Region_t723338532 * ___bucketRegion_4;
	// System.String Amazon.S3.Model.PutBucketRequest::bucketRegionName
	String_t* ___bucketRegionName_5;
	// System.Boolean Amazon.S3.Model.PutBucketRequest::useClientRegion
	bool ___useClientRegion_6;

public:
	inline static int32_t get_offset_of_bucketRegion_4() { return static_cast<int32_t>(offsetof(PutBucketRequest_t1453664272, ___bucketRegion_4)); }
	inline S3Region_t723338532 * get_bucketRegion_4() const { return ___bucketRegion_4; }
	inline S3Region_t723338532 ** get_address_of_bucketRegion_4() { return &___bucketRegion_4; }
	inline void set_bucketRegion_4(S3Region_t723338532 * value)
	{
		___bucketRegion_4 = value;
		Il2CppCodeGenWriteBarrier((&___bucketRegion_4), value);
	}

	inline static int32_t get_offset_of_bucketRegionName_5() { return static_cast<int32_t>(offsetof(PutBucketRequest_t1453664272, ___bucketRegionName_5)); }
	inline String_t* get_bucketRegionName_5() const { return ___bucketRegionName_5; }
	inline String_t** get_address_of_bucketRegionName_5() { return &___bucketRegionName_5; }
	inline void set_bucketRegionName_5(String_t* value)
	{
		___bucketRegionName_5 = value;
		Il2CppCodeGenWriteBarrier((&___bucketRegionName_5), value);
	}

	inline static int32_t get_offset_of_useClientRegion_6() { return static_cast<int32_t>(offsetof(PutBucketRequest_t1453664272, ___useClientRegion_6)); }
	inline bool get_useClientRegion_6() const { return ___useClientRegion_6; }
	inline bool* get_address_of_useClientRegion_6() { return &___useClientRegion_6; }
	inline void set_useClientRegion_6(bool value)
	{
		___useClientRegion_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTBUCKETREQUEST_T1453664272_H
#ifndef RETRYPOLICY_T1476739446_H
#define RETRYPOLICY_T1476739446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.RetryPolicy
struct  RetryPolicy_t1476739446  : public RuntimeObject
{
public:
	// System.Int32 Amazon.Runtime.RetryPolicy::<MaxRetries>k__BackingField
	int32_t ___U3CMaxRetriesU3Ek__BackingField_0;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.RetryPolicy::<Logger>k__BackingField
	RuntimeObject* ___U3CLoggerU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMaxRetriesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446, ___U3CMaxRetriesU3Ek__BackingField_0)); }
	inline int32_t get_U3CMaxRetriesU3Ek__BackingField_0() const { return ___U3CMaxRetriesU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CMaxRetriesU3Ek__BackingField_0() { return &___U3CMaxRetriesU3Ek__BackingField_0; }
	inline void set_U3CMaxRetriesU3Ek__BackingField_0(int32_t value)
	{
		___U3CMaxRetriesU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLoggerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446, ___U3CLoggerU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CLoggerU3Ek__BackingField_1() const { return ___U3CLoggerU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CLoggerU3Ek__BackingField_1() { return &___U3CLoggerU3Ek__BackingField_1; }
	inline void set_U3CLoggerU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CLoggerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggerU3Ek__BackingField_1), value);
	}
};

struct RetryPolicy_t1476739446_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.RetryPolicy::clockSkewErrorCodes
	HashSet_1_t362681087 * ___clockSkewErrorCodes_2;
	// System.TimeSpan Amazon.Runtime.RetryPolicy::clockSkewMaxThreshold
	TimeSpan_t3430258949  ___clockSkewMaxThreshold_3;

public:
	inline static int32_t get_offset_of_clockSkewErrorCodes_2() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446_StaticFields, ___clockSkewErrorCodes_2)); }
	inline HashSet_1_t362681087 * get_clockSkewErrorCodes_2() const { return ___clockSkewErrorCodes_2; }
	inline HashSet_1_t362681087 ** get_address_of_clockSkewErrorCodes_2() { return &___clockSkewErrorCodes_2; }
	inline void set_clockSkewErrorCodes_2(HashSet_1_t362681087 * value)
	{
		___clockSkewErrorCodes_2 = value;
		Il2CppCodeGenWriteBarrier((&___clockSkewErrorCodes_2), value);
	}

	inline static int32_t get_offset_of_clockSkewMaxThreshold_3() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446_StaticFields, ___clockSkewMaxThreshold_3)); }
	inline TimeSpan_t3430258949  get_clockSkewMaxThreshold_3() const { return ___clockSkewMaxThreshold_3; }
	inline TimeSpan_t3430258949 * get_address_of_clockSkewMaxThreshold_3() { return &___clockSkewMaxThreshold_3; }
	inline void set_clockSkewMaxThreshold_3(TimeSpan_t3430258949  value)
	{
		___clockSkewMaxThreshold_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYPOLICY_T1476739446_H
#ifndef TEXTCONTAINERANCHORS_T2796925266_H
#define TEXTCONTAINERANCHORS_T2796925266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainerAnchors
struct  TextContainerAnchors_t2796925266 
{
public:
	// System.Int32 TMPro.TextContainerAnchors::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextContainerAnchors_t2796925266, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINERANCHORS_T2796925266_H
#ifndef LISTOBJECTSREQUEST_T3715455147_H
#define LISTOBJECTSREQUEST_T3715455147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.ListObjectsRequest
struct  ListObjectsRequest_t3715455147  : public AmazonWebServiceRequest_t3384026212
{
public:
	// System.String Amazon.S3.Model.ListObjectsRequest::bucketName
	String_t* ___bucketName_4;
	// System.String Amazon.S3.Model.ListObjectsRequest::delimiter
	String_t* ___delimiter_5;
	// System.String Amazon.S3.Model.ListObjectsRequest::marker
	String_t* ___marker_6;
	// System.Nullable`1<System.Int32> Amazon.S3.Model.ListObjectsRequest::maxKeys
	Nullable_1_t334943763  ___maxKeys_7;
	// System.String Amazon.S3.Model.ListObjectsRequest::prefix
	String_t* ___prefix_8;
	// Amazon.S3.EncodingType Amazon.S3.Model.ListObjectsRequest::encoding
	EncodingType_t159362831 * ___encoding_9;
	// Amazon.S3.RequestPayer Amazon.S3.Model.ListObjectsRequest::requestPayer
	RequestPayer_t3334661492 * ___requestPayer_10;

public:
	inline static int32_t get_offset_of_bucketName_4() { return static_cast<int32_t>(offsetof(ListObjectsRequest_t3715455147, ___bucketName_4)); }
	inline String_t* get_bucketName_4() const { return ___bucketName_4; }
	inline String_t** get_address_of_bucketName_4() { return &___bucketName_4; }
	inline void set_bucketName_4(String_t* value)
	{
		___bucketName_4 = value;
		Il2CppCodeGenWriteBarrier((&___bucketName_4), value);
	}

	inline static int32_t get_offset_of_delimiter_5() { return static_cast<int32_t>(offsetof(ListObjectsRequest_t3715455147, ___delimiter_5)); }
	inline String_t* get_delimiter_5() const { return ___delimiter_5; }
	inline String_t** get_address_of_delimiter_5() { return &___delimiter_5; }
	inline void set_delimiter_5(String_t* value)
	{
		___delimiter_5 = value;
		Il2CppCodeGenWriteBarrier((&___delimiter_5), value);
	}

	inline static int32_t get_offset_of_marker_6() { return static_cast<int32_t>(offsetof(ListObjectsRequest_t3715455147, ___marker_6)); }
	inline String_t* get_marker_6() const { return ___marker_6; }
	inline String_t** get_address_of_marker_6() { return &___marker_6; }
	inline void set_marker_6(String_t* value)
	{
		___marker_6 = value;
		Il2CppCodeGenWriteBarrier((&___marker_6), value);
	}

	inline static int32_t get_offset_of_maxKeys_7() { return static_cast<int32_t>(offsetof(ListObjectsRequest_t3715455147, ___maxKeys_7)); }
	inline Nullable_1_t334943763  get_maxKeys_7() const { return ___maxKeys_7; }
	inline Nullable_1_t334943763 * get_address_of_maxKeys_7() { return &___maxKeys_7; }
	inline void set_maxKeys_7(Nullable_1_t334943763  value)
	{
		___maxKeys_7 = value;
	}

	inline static int32_t get_offset_of_prefix_8() { return static_cast<int32_t>(offsetof(ListObjectsRequest_t3715455147, ___prefix_8)); }
	inline String_t* get_prefix_8() const { return ___prefix_8; }
	inline String_t** get_address_of_prefix_8() { return &___prefix_8; }
	inline void set_prefix_8(String_t* value)
	{
		___prefix_8 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_8), value);
	}

	inline static int32_t get_offset_of_encoding_9() { return static_cast<int32_t>(offsetof(ListObjectsRequest_t3715455147, ___encoding_9)); }
	inline EncodingType_t159362831 * get_encoding_9() const { return ___encoding_9; }
	inline EncodingType_t159362831 ** get_address_of_encoding_9() { return &___encoding_9; }
	inline void set_encoding_9(EncodingType_t159362831 * value)
	{
		___encoding_9 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_9), value);
	}

	inline static int32_t get_offset_of_requestPayer_10() { return static_cast<int32_t>(offsetof(ListObjectsRequest_t3715455147, ___requestPayer_10)); }
	inline RequestPayer_t3334661492 * get_requestPayer_10() const { return ___requestPayer_10; }
	inline RequestPayer_t3334661492 ** get_address_of_requestPayer_10() { return &___requestPayer_10; }
	inline void set_requestPayer_10(RequestPayer_t3334661492 * value)
	{
		___requestPayer_10 = value;
		Il2CppCodeGenWriteBarrier((&___requestPayer_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTOBJECTSREQUEST_T3715455147_H
#ifndef XMLNODETYPE_T739504597_H
#define XMLNODETYPE_T739504597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_t739504597 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeType_t739504597, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_T739504597_H
#ifndef S3REPONSEUNMARSHALLER_T3340520676_H
#define S3REPONSEUNMARSHALLER_T3340520676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.S3ReponseUnmarshaller
struct  S3ReponseUnmarshaller_t3340520676  : public XmlResponseUnmarshaller_t760346204
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3REPONSEUNMARSHALLER_T3340520676_H
#ifndef VERTEXSORTINGORDER_T471281372_H
#define VERTEXSORTINGORDER_T471281372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t471281372 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t471281372, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T471281372_H
#ifndef TEXTINPUTSOURCES_T434791461_H
#define TEXTINPUTSOURCES_T434791461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text/TextInputSources
struct  TextInputSources_t434791461 
{
public:
	// System.Int32 TMPro.TMP_Text/TextInputSources::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextInputSources_t434791461, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T434791461_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef TEXTRENDERFLAGS_T7026704_H
#define TEXTRENDERFLAGS_T7026704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t7026704 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextRenderFlags_t7026704, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T7026704_H
#ifndef TEXTOVERFLOWMODES_T2644609723_H
#define TEXTOVERFLOWMODES_T2644609723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_t2644609723 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextOverflowModes_t2644609723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_T2644609723_H
#ifndef PUTOBJECTREQUEST_T3608576685_H
#define PUTOBJECTREQUEST_T3608576685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.PutObjectRequest
struct  PutObjectRequest_t3608576685  : public PutWithACLRequest_t3114873226
{
public:
	// System.String Amazon.S3.Model.PutObjectRequest::key
	String_t* ___key_4;
	// Amazon.S3.Model.HeadersCollection Amazon.S3.Model.PutObjectRequest::headersCollection
	HeadersCollection_t2711555648 * ___headersCollection_5;
	// System.IO.Stream Amazon.S3.Model.PutObjectRequest::inputStream
	Stream_t3255436806 * ___inputStream_6;
	// System.String Amazon.S3.Model.PutObjectRequest::filePath
	String_t* ___filePath_7;
	// System.String Amazon.S3.Model.PutObjectRequest::contentBody
	String_t* ___contentBody_8;
	// System.Boolean Amazon.S3.Model.PutObjectRequest::autoCloseStream
	bool ___autoCloseStream_9;
	// System.Boolean Amazon.S3.Model.PutObjectRequest::autoResetStreamPosition
	bool ___autoResetStreamPosition_10;

public:
	inline static int32_t get_offset_of_key_4() { return static_cast<int32_t>(offsetof(PutObjectRequest_t3608576685, ___key_4)); }
	inline String_t* get_key_4() const { return ___key_4; }
	inline String_t** get_address_of_key_4() { return &___key_4; }
	inline void set_key_4(String_t* value)
	{
		___key_4 = value;
		Il2CppCodeGenWriteBarrier((&___key_4), value);
	}

	inline static int32_t get_offset_of_headersCollection_5() { return static_cast<int32_t>(offsetof(PutObjectRequest_t3608576685, ___headersCollection_5)); }
	inline HeadersCollection_t2711555648 * get_headersCollection_5() const { return ___headersCollection_5; }
	inline HeadersCollection_t2711555648 ** get_address_of_headersCollection_5() { return &___headersCollection_5; }
	inline void set_headersCollection_5(HeadersCollection_t2711555648 * value)
	{
		___headersCollection_5 = value;
		Il2CppCodeGenWriteBarrier((&___headersCollection_5), value);
	}

	inline static int32_t get_offset_of_inputStream_6() { return static_cast<int32_t>(offsetof(PutObjectRequest_t3608576685, ___inputStream_6)); }
	inline Stream_t3255436806 * get_inputStream_6() const { return ___inputStream_6; }
	inline Stream_t3255436806 ** get_address_of_inputStream_6() { return &___inputStream_6; }
	inline void set_inputStream_6(Stream_t3255436806 * value)
	{
		___inputStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputStream_6), value);
	}

	inline static int32_t get_offset_of_filePath_7() { return static_cast<int32_t>(offsetof(PutObjectRequest_t3608576685, ___filePath_7)); }
	inline String_t* get_filePath_7() const { return ___filePath_7; }
	inline String_t** get_address_of_filePath_7() { return &___filePath_7; }
	inline void set_filePath_7(String_t* value)
	{
		___filePath_7 = value;
		Il2CppCodeGenWriteBarrier((&___filePath_7), value);
	}

	inline static int32_t get_offset_of_contentBody_8() { return static_cast<int32_t>(offsetof(PutObjectRequest_t3608576685, ___contentBody_8)); }
	inline String_t* get_contentBody_8() const { return ___contentBody_8; }
	inline String_t** get_address_of_contentBody_8() { return &___contentBody_8; }
	inline void set_contentBody_8(String_t* value)
	{
		___contentBody_8 = value;
		Il2CppCodeGenWriteBarrier((&___contentBody_8), value);
	}

	inline static int32_t get_offset_of_autoCloseStream_9() { return static_cast<int32_t>(offsetof(PutObjectRequest_t3608576685, ___autoCloseStream_9)); }
	inline bool get_autoCloseStream_9() const { return ___autoCloseStream_9; }
	inline bool* get_address_of_autoCloseStream_9() { return &___autoCloseStream_9; }
	inline void set_autoCloseStream_9(bool value)
	{
		___autoCloseStream_9 = value;
	}

	inline static int32_t get_offset_of_autoResetStreamPosition_10() { return static_cast<int32_t>(offsetof(PutObjectRequest_t3608576685, ___autoResetStreamPosition_10)); }
	inline bool get_autoResetStreamPosition_10() const { return ___autoResetStreamPosition_10; }
	inline bool* get_address_of_autoResetStreamPosition_10() { return &___autoResetStreamPosition_10; }
	inline void set_autoResetStreamPosition_10(bool value)
	{
		___autoResetStreamPosition_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTOBJECTREQUEST_T3608576685_H
#ifndef TEXTUREMAPPINGOPTIONS_T761764377_H
#define TEXTUREMAPPINGOPTIONS_T761764377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_t761764377 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t761764377, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_T761764377_H
#ifndef ERRORTYPE_T1448377524_H
#define ERRORTYPE_T1448377524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ErrorType
struct  ErrorType_t1448377524 
{
public:
	// System.Int32 Amazon.Runtime.ErrorType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ErrorType_t1448377524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORTYPE_T1448377524_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_T878817098_H
#define TMP_VERTEXDATAUPDATEFLAGS_T878817098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_t878817098 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_t878817098, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_T878817098_H
#ifndef TAGTYPE_T1698342214_H
#define TAGTYPE_T1698342214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t1698342214 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagType_t1698342214, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T1698342214_H
#ifndef UPLOADPARTREQUEST_T874148693_H
#define UPLOADPARTREQUEST_T874148693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.UploadPartRequest
struct  UploadPartRequest_t874148693  : public AmazonWebServiceRequest_t3384026212
{
public:
	// System.IO.Stream Amazon.S3.Model.UploadPartRequest::inputStream
	Stream_t3255436806 * ___inputStream_4;
	// System.Nullable`1<System.Int32> Amazon.S3.Model.UploadPartRequest::partNumber
	Nullable_1_t334943763  ___partNumber_5;
	// System.String Amazon.S3.Model.UploadPartRequest::filePath
	String_t* ___filePath_6;
	// System.Nullable`1<System.Int64> Amazon.S3.Model.UploadPartRequest::filePosition
	Nullable_1_t3467111648  ___filePosition_7;

public:
	inline static int32_t get_offset_of_inputStream_4() { return static_cast<int32_t>(offsetof(UploadPartRequest_t874148693, ___inputStream_4)); }
	inline Stream_t3255436806 * get_inputStream_4() const { return ___inputStream_4; }
	inline Stream_t3255436806 ** get_address_of_inputStream_4() { return &___inputStream_4; }
	inline void set_inputStream_4(Stream_t3255436806 * value)
	{
		___inputStream_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputStream_4), value);
	}

	inline static int32_t get_offset_of_partNumber_5() { return static_cast<int32_t>(offsetof(UploadPartRequest_t874148693, ___partNumber_5)); }
	inline Nullable_1_t334943763  get_partNumber_5() const { return ___partNumber_5; }
	inline Nullable_1_t334943763 * get_address_of_partNumber_5() { return &___partNumber_5; }
	inline void set_partNumber_5(Nullable_1_t334943763  value)
	{
		___partNumber_5 = value;
	}

	inline static int32_t get_offset_of_filePath_6() { return static_cast<int32_t>(offsetof(UploadPartRequest_t874148693, ___filePath_6)); }
	inline String_t* get_filePath_6() const { return ___filePath_6; }
	inline String_t** get_address_of_filePath_6() { return &___filePath_6; }
	inline void set_filePath_6(String_t* value)
	{
		___filePath_6 = value;
		Il2CppCodeGenWriteBarrier((&___filePath_6), value);
	}

	inline static int32_t get_offset_of_filePosition_7() { return static_cast<int32_t>(offsetof(UploadPartRequest_t874148693, ___filePosition_7)); }
	inline Nullable_1_t3467111648  get_filePosition_7() const { return ___filePosition_7; }
	inline Nullable_1_t3467111648 * get_address_of_filePosition_7() { return &___filePosition_7; }
	inline void set_filePosition_7(Nullable_1_t3467111648  value)
	{
		___filePosition_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADPARTREQUEST_T874148693_H
#ifndef TMP_VERTEX_T1422870470_H
#define TMP_VERTEX_T1422870470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t1422870470 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t2243707580  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t2243707579  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t2243707579  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t2243707579  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t874517518  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___position_0)); }
	inline Vector3_t2243707580  get_position_0() const { return ___position_0; }
	inline Vector3_t2243707580 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2243707580  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___uv_1)); }
	inline Vector2_t2243707579  get_uv_1() const { return ___uv_1; }
	inline Vector2_t2243707579 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t2243707579  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___uv2_2)); }
	inline Vector2_t2243707579  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t2243707579 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t2243707579  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___uv4_3)); }
	inline Vector2_t2243707579  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t2243707579 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t2243707579  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___color_4)); }
	inline Color32_t874517518  get_color_4() const { return ___color_4; }
	inline Color32_t874517518 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t874517518  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T1422870470_H
#ifndef MESH_EXTENTS_T3515350687_H
#define MESH_EXTENTS_T3515350687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t3515350687 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_t2243707579  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_t2243707579  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3515350687, ___min_0)); }
	inline Vector2_t2243707579  get_min_0() const { return ___min_0; }
	inline Vector2_t2243707579 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2243707579  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t3515350687, ___max_1)); }
	inline Vector2_t2243707579  get_max_1() const { return ___max_1; }
	inline Vector2_t2243707579 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2243707579  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T3515350687_H
#ifndef EXTENTS_T3018556803_H
#define EXTENTS_T3018556803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3018556803 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2243707579  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2243707579  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3018556803, ___min_0)); }
	inline Vector2_t2243707579  get_min_0() const { return ___min_0; }
	inline Vector2_t2243707579 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2243707579  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3018556803, ___max_1)); }
	inline Vector2_t2243707579  get_max_1() const { return ___max_1; }
	inline Vector2_t2243707579 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2243707579  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3018556803_H
#ifndef VERTEXGRADIENT_T1602386880_H
#define VERTEXGRADIENT_T1602386880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t1602386880 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2020392075  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2020392075  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2020392075  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2020392075  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t1602386880, ___topLeft_0)); }
	inline Color_t2020392075  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2020392075 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2020392075  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t1602386880, ___topRight_1)); }
	inline Color_t2020392075  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2020392075 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2020392075  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t1602386880, ___bottomLeft_2)); }
	inline Color_t2020392075  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2020392075 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2020392075  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t1602386880, ___bottomRight_3)); }
	inline Color_t2020392075  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2020392075 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2020392075  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T1602386880_H
#ifndef INITIATEMULTIPARTUPLOADREQUEST_T440630327_H
#define INITIATEMULTIPARTUPLOADREQUEST_T440630327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.InitiateMultipartUploadRequest
struct  InitiateMultipartUploadRequest_t440630327  : public PutWithACLRequest_t3114873226
{
public:
	// System.String Amazon.S3.Model.InitiateMultipartUploadRequest::key
	String_t* ___key_4;
	// Amazon.S3.Model.HeadersCollection Amazon.S3.Model.InitiateMultipartUploadRequest::headersCollection
	HeadersCollection_t2711555648 * ___headersCollection_5;

public:
	inline static int32_t get_offset_of_key_4() { return static_cast<int32_t>(offsetof(InitiateMultipartUploadRequest_t440630327, ___key_4)); }
	inline String_t* get_key_4() const { return ___key_4; }
	inline String_t** get_address_of_key_4() { return &___key_4; }
	inline void set_key_4(String_t* value)
	{
		___key_4 = value;
		Il2CppCodeGenWriteBarrier((&___key_4), value);
	}

	inline static int32_t get_offset_of_headersCollection_5() { return static_cast<int32_t>(offsetof(InitiateMultipartUploadRequest_t440630327, ___headersCollection_5)); }
	inline HeadersCollection_t2711555648 * get_headersCollection_5() const { return ___headersCollection_5; }
	inline HeadersCollection_t2711555648 ** get_address_of_headersCollection_5() { return &___headersCollection_5; }
	inline void set_headersCollection_5(HeadersCollection_t2711555648 * value)
	{
		___headersCollection_5 = value;
		Il2CppCodeGenWriteBarrier((&___headersCollection_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIATEMULTIPARTUPLOADREQUEST_T440630327_H
#ifndef COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T3434557887_H
#define COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T3434557887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DistanceTransform_EventTypes
struct  Compute_DistanceTransform_EventTypes_t3434557887 
{
public:
	// System.Int32 TMPro.Compute_DistanceTransform_EventTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Compute_DistanceTransform_EventTypes_t3434557887, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T3434557887_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef HTTPSTATUSCODE_T1898409641_H
#define HTTPSTATUSCODE_T1898409641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_t1898409641 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t1898409641, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T1898409641_H
#ifndef KERNINGPAIR_T1577753922_H
#define KERNINGPAIR_T1577753922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.KerningPair
struct  KerningPair_t1577753922  : public RuntimeObject
{
public:
	// System.UInt32 TMPro.KerningPair::m_FirstGlyph
	uint32_t ___m_FirstGlyph_0;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_FirstGlyphAdjustments
	GlyphValueRecord_t3876405180  ___m_FirstGlyphAdjustments_1;
	// System.UInt32 TMPro.KerningPair::m_SecondGlyph
	uint32_t ___m_SecondGlyph_2;
	// TMPro.GlyphValueRecord TMPro.KerningPair::m_SecondGlyphAdjustments
	GlyphValueRecord_t3876405180  ___m_SecondGlyphAdjustments_3;
	// System.Single TMPro.KerningPair::xOffset
	float ___xOffset_4;

public:
	inline static int32_t get_offset_of_m_FirstGlyph_0() { return static_cast<int32_t>(offsetof(KerningPair_t1577753922, ___m_FirstGlyph_0)); }
	inline uint32_t get_m_FirstGlyph_0() const { return ___m_FirstGlyph_0; }
	inline uint32_t* get_address_of_m_FirstGlyph_0() { return &___m_FirstGlyph_0; }
	inline void set_m_FirstGlyph_0(uint32_t value)
	{
		___m_FirstGlyph_0 = value;
	}

	inline static int32_t get_offset_of_m_FirstGlyphAdjustments_1() { return static_cast<int32_t>(offsetof(KerningPair_t1577753922, ___m_FirstGlyphAdjustments_1)); }
	inline GlyphValueRecord_t3876405180  get_m_FirstGlyphAdjustments_1() const { return ___m_FirstGlyphAdjustments_1; }
	inline GlyphValueRecord_t3876405180 * get_address_of_m_FirstGlyphAdjustments_1() { return &___m_FirstGlyphAdjustments_1; }
	inline void set_m_FirstGlyphAdjustments_1(GlyphValueRecord_t3876405180  value)
	{
		___m_FirstGlyphAdjustments_1 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyph_2() { return static_cast<int32_t>(offsetof(KerningPair_t1577753922, ___m_SecondGlyph_2)); }
	inline uint32_t get_m_SecondGlyph_2() const { return ___m_SecondGlyph_2; }
	inline uint32_t* get_address_of_m_SecondGlyph_2() { return &___m_SecondGlyph_2; }
	inline void set_m_SecondGlyph_2(uint32_t value)
	{
		___m_SecondGlyph_2 = value;
	}

	inline static int32_t get_offset_of_m_SecondGlyphAdjustments_3() { return static_cast<int32_t>(offsetof(KerningPair_t1577753922, ___m_SecondGlyphAdjustments_3)); }
	inline GlyphValueRecord_t3876405180  get_m_SecondGlyphAdjustments_3() const { return ___m_SecondGlyphAdjustments_3; }
	inline GlyphValueRecord_t3876405180 * get_address_of_m_SecondGlyphAdjustments_3() { return &___m_SecondGlyphAdjustments_3; }
	inline void set_m_SecondGlyphAdjustments_3(GlyphValueRecord_t3876405180  value)
	{
		___m_SecondGlyphAdjustments_3 = value;
	}

	inline static int32_t get_offset_of_xOffset_4() { return static_cast<int32_t>(offsetof(KerningPair_t1577753922, ___xOffset_4)); }
	inline float get_xOffset_4() const { return ___xOffset_4; }
	inline float* get_address_of_xOffset_4() { return &___xOffset_4; }
	inline void set_xOffset_4(float value)
	{
		___xOffset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNINGPAIR_T1577753922_H
#ifndef TMP_MATH_T871511886_H
#define TMP_MATH_T871511886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Math
struct  TMP_Math_t871511886  : public RuntimeObject
{
public:

public:
};

struct TMP_Math_t871511886_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_Math::MAX_16BIT
	Vector2_t2243707579  ___MAX_16BIT_6;
	// UnityEngine.Vector2 TMPro.TMP_Math::MIN_16BIT
	Vector2_t2243707579  ___MIN_16BIT_7;

public:
	inline static int32_t get_offset_of_MAX_16BIT_6() { return static_cast<int32_t>(offsetof(TMP_Math_t871511886_StaticFields, ___MAX_16BIT_6)); }
	inline Vector2_t2243707579  get_MAX_16BIT_6() const { return ___MAX_16BIT_6; }
	inline Vector2_t2243707579 * get_address_of_MAX_16BIT_6() { return &___MAX_16BIT_6; }
	inline void set_MAX_16BIT_6(Vector2_t2243707579  value)
	{
		___MAX_16BIT_6 = value;
	}

	inline static int32_t get_offset_of_MIN_16BIT_7() { return static_cast<int32_t>(offsetof(TMP_Math_t871511886_StaticFields, ___MIN_16BIT_7)); }
	inline Vector2_t2243707579  get_MIN_16BIT_7() const { return ___MIN_16BIT_7; }
	inline Vector2_t2243707579 * get_address_of_MIN_16BIT_7() { return &___MIN_16BIT_7; }
	inline void set_MIN_16BIT_7(Vector2_t2243707579  value)
	{
		___MIN_16BIT_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATH_T871511886_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef TMP_XMLTAGSTACK_1_T2125340843_H
#define TMP_XMLTAGSTACK_1_T2125340843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t2125340843 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t1615060493* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2125340843, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t1615060493* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t1615060493** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t1615060493* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2125340843, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2125340843, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2125340843, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2125340843_H
#ifndef TMP_LINEINFO_T2320418126_H
#define TMP_LINEINFO_T2320418126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t2320418126 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_2;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_3;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_7;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_8;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_9;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_10;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_11;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_12;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_13;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_14;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_15;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_16;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_17;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3018556803  ___lineExtents_18;

public:
	inline static int32_t get_offset_of_characterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___characterCount_0)); }
	inline int32_t get_characterCount_0() const { return ___characterCount_0; }
	inline int32_t* get_address_of_characterCount_0() { return &___characterCount_0; }
	inline void set_characterCount_0(int32_t value)
	{
		___characterCount_0 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___visibleCharacterCount_1)); }
	inline int32_t get_visibleCharacterCount_1() const { return ___visibleCharacterCount_1; }
	inline int32_t* get_address_of_visibleCharacterCount_1() { return &___visibleCharacterCount_1; }
	inline void set_visibleCharacterCount_1(int32_t value)
	{
		___visibleCharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_spaceCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___spaceCount_2)); }
	inline int32_t get_spaceCount_2() const { return ___spaceCount_2; }
	inline int32_t* get_address_of_spaceCount_2() { return &___spaceCount_2; }
	inline void set_spaceCount_2(int32_t value)
	{
		___spaceCount_2 = value;
	}

	inline static int32_t get_offset_of_wordCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___wordCount_3)); }
	inline int32_t get_wordCount_3() const { return ___wordCount_3; }
	inline int32_t* get_address_of_wordCount_3() { return &___wordCount_3; }
	inline void set_wordCount_3(int32_t value)
	{
		___wordCount_3 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___firstCharacterIndex_4)); }
	inline int32_t get_firstCharacterIndex_4() const { return ___firstCharacterIndex_4; }
	inline int32_t* get_address_of_firstCharacterIndex_4() { return &___firstCharacterIndex_4; }
	inline void set_firstCharacterIndex_4(int32_t value)
	{
		___firstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___firstVisibleCharacterIndex_5)); }
	inline int32_t get_firstVisibleCharacterIndex_5() const { return ___firstVisibleCharacterIndex_5; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_5() { return &___firstVisibleCharacterIndex_5; }
	inline void set_firstVisibleCharacterIndex_5(int32_t value)
	{
		___firstVisibleCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___lastCharacterIndex_6)); }
	inline int32_t get_lastCharacterIndex_6() const { return ___lastCharacterIndex_6; }
	inline int32_t* get_address_of_lastCharacterIndex_6() { return &___lastCharacterIndex_6; }
	inline void set_lastCharacterIndex_6(int32_t value)
	{
		___lastCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___lastVisibleCharacterIndex_7)); }
	inline int32_t get_lastVisibleCharacterIndex_7() const { return ___lastVisibleCharacterIndex_7; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_7() { return &___lastVisibleCharacterIndex_7; }
	inline void set_lastVisibleCharacterIndex_7(int32_t value)
	{
		___lastVisibleCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_lineHeight_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___lineHeight_9)); }
	inline float get_lineHeight_9() const { return ___lineHeight_9; }
	inline float* get_address_of_lineHeight_9() { return &___lineHeight_9; }
	inline void set_lineHeight_9(float value)
	{
		___lineHeight_9 = value;
	}

	inline static int32_t get_offset_of_ascender_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___ascender_10)); }
	inline float get_ascender_10() const { return ___ascender_10; }
	inline float* get_address_of_ascender_10() { return &___ascender_10; }
	inline void set_ascender_10(float value)
	{
		___ascender_10 = value;
	}

	inline static int32_t get_offset_of_baseline_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___baseline_11)); }
	inline float get_baseline_11() const { return ___baseline_11; }
	inline float* get_address_of_baseline_11() { return &___baseline_11; }
	inline void set_baseline_11(float value)
	{
		___baseline_11 = value;
	}

	inline static int32_t get_offset_of_descender_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___descender_12)); }
	inline float get_descender_12() const { return ___descender_12; }
	inline float* get_address_of_descender_12() { return &___descender_12; }
	inline void set_descender_12(float value)
	{
		___descender_12 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___maxAdvance_13)); }
	inline float get_maxAdvance_13() const { return ___maxAdvance_13; }
	inline float* get_address_of_maxAdvance_13() { return &___maxAdvance_13; }
	inline void set_maxAdvance_13(float value)
	{
		___maxAdvance_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_marginLeft_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___marginLeft_15)); }
	inline float get_marginLeft_15() const { return ___marginLeft_15; }
	inline float* get_address_of_marginLeft_15() { return &___marginLeft_15; }
	inline void set_marginLeft_15(float value)
	{
		___marginLeft_15 = value;
	}

	inline static int32_t get_offset_of_marginRight_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___marginRight_16)); }
	inline float get_marginRight_16() const { return ___marginRight_16; }
	inline float* get_address_of_marginRight_16() { return &___marginRight_16; }
	inline void set_marginRight_16(float value)
	{
		___marginRight_16 = value;
	}

	inline static int32_t get_offset_of_alignment_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___alignment_17)); }
	inline int32_t get_alignment_17() const { return ___alignment_17; }
	inline int32_t* get_address_of_alignment_17() { return &___alignment_17; }
	inline void set_alignment_17(int32_t value)
	{
		___alignment_17 = value;
	}

	inline static int32_t get_offset_of_lineExtents_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___lineExtents_18)); }
	inline Extents_t3018556803  get_lineExtents_18() const { return ___lineExtents_18; }
	inline Extents_t3018556803 * get_address_of_lineExtents_18() { return &___lineExtents_18; }
	inline void set_lineExtents_18(Extents_t3018556803  value)
	{
		___lineExtents_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T2320418126_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef GETOBJECTRESPONSEUNMARSHALLER_T4197678500_H
#define GETOBJECTRESPONSEUNMARSHALLER_T4197678500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.GetObjectResponseUnmarshaller
struct  GetObjectResponseUnmarshaller_t4197678500  : public S3ReponseUnmarshaller_t3340520676
{
public:

public:
};

struct GetObjectResponseUnmarshaller_t4197678500_StaticFields
{
public:
	// Amazon.S3.Model.Internal.MarshallTransformations.GetObjectResponseUnmarshaller Amazon.S3.Model.Internal.MarshallTransformations.GetObjectResponseUnmarshaller::_instance
	GetObjectResponseUnmarshaller_t4197678500 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(GetObjectResponseUnmarshaller_t4197678500_StaticFields, ____instance_0)); }
	inline GetObjectResponseUnmarshaller_t4197678500 * get__instance_0() const { return ____instance_0; }
	inline GetObjectResponseUnmarshaller_t4197678500 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(GetObjectResponseUnmarshaller_t4197678500 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETOBJECTRESPONSEUNMARSHALLER_T4197678500_H
#ifndef AMAZONWEBSERVICERESPONSE_T529043356_H
#define AMAZONWEBSERVICERESPONSE_T529043356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonWebServiceResponse
struct  AmazonWebServiceResponse_t529043356  : public RuntimeObject
{
public:
	// Amazon.Runtime.ResponseMetadata Amazon.Runtime.AmazonWebServiceResponse::responseMetadataField
	ResponseMetadata_t527027456 * ___responseMetadataField_0;
	// System.Int64 Amazon.Runtime.AmazonWebServiceResponse::contentLength
	int64_t ___contentLength_1;
	// System.Net.HttpStatusCode Amazon.Runtime.AmazonWebServiceResponse::httpStatusCode
	int32_t ___httpStatusCode_2;

public:
	inline static int32_t get_offset_of_responseMetadataField_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t529043356, ___responseMetadataField_0)); }
	inline ResponseMetadata_t527027456 * get_responseMetadataField_0() const { return ___responseMetadataField_0; }
	inline ResponseMetadata_t527027456 ** get_address_of_responseMetadataField_0() { return &___responseMetadataField_0; }
	inline void set_responseMetadataField_0(ResponseMetadata_t527027456 * value)
	{
		___responseMetadataField_0 = value;
		Il2CppCodeGenWriteBarrier((&___responseMetadataField_0), value);
	}

	inline static int32_t get_offset_of_contentLength_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t529043356, ___contentLength_1)); }
	inline int64_t get_contentLength_1() const { return ___contentLength_1; }
	inline int64_t* get_address_of_contentLength_1() { return &___contentLength_1; }
	inline void set_contentLength_1(int64_t value)
	{
		___contentLength_1 = value;
	}

	inline static int32_t get_offset_of_httpStatusCode_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t529043356, ___httpStatusCode_2)); }
	inline int32_t get_httpStatusCode_2() const { return ___httpStatusCode_2; }
	inline int32_t* get_address_of_httpStatusCode_2() { return &___httpStatusCode_2; }
	inline void set_httpStatusCode_2(int32_t value)
	{
		___httpStatusCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONWEBSERVICERESPONSE_T529043356_H
#ifndef TMP_CHARACTERINFO_T1421302791_H
#define TMP_CHARACTERINFO_T1421302791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t1421302791 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_t2285620223 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t2530419979 * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_t193706927 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t1422870470  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t1422870470  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t1422870470  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t1422870470  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_t2243707580  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_t2243707580  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_t2243707580  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_t2243707580  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t874517518  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t874517518  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t874517518  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t874517518  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___textElement_3)); }
	inline TMP_TextElement_t2285620223 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_t2285620223 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_t2285620223 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___fontAsset_4)); }
	inline TMP_FontAsset_t2530419979 * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t2530419979 * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_t2641813093 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_t2641813093 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___material_7)); }
	inline Material_t193706927 * get_material_7() const { return ___material_7; }
	inline Material_t193706927 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t193706927 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___pageNumber_12)); }
	inline int32_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int32_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int32_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertex_TL_14)); }
	inline TMP_Vertex_t1422870470  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t1422870470 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t1422870470  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertex_BL_15)); }
	inline TMP_Vertex_t1422870470  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t1422870470 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t1422870470  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertex_TR_16)); }
	inline TMP_Vertex_t1422870470  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t1422870470 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t1422870470  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertex_BR_17)); }
	inline TMP_Vertex_t1422870470  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t1422870470 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t1422870470  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___topLeft_18)); }
	inline Vector3_t2243707580  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_t2243707580 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_t2243707580  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___bottomLeft_19)); }
	inline Vector3_t2243707580  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_t2243707580 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_t2243707580  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___topRight_20)); }
	inline Vector3_t2243707580  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_t2243707580 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_t2243707580  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___bottomRight_21)); }
	inline Vector3_t2243707580  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_t2243707580 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_t2243707580  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___color_29)); }
	inline Color32_t874517518  get_color_29() const { return ___color_29; }
	inline Color32_t874517518 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t874517518  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___underlineColor_30)); }
	inline Color32_t874517518  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t874517518 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t874517518  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___strikethroughColor_31)); }
	inline Color32_t874517518  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t874517518 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t874517518  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___highlightColor_32)); }
	inline Color32_t874517518  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t874517518 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t874517518  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t1421302791_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t2285620223 * ___textElement_3;
	TMP_FontAsset_t2530419979 * ___fontAsset_4;
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t193706927 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t1422870470  ___vertex_TL_14;
	TMP_Vertex_t1422870470  ___vertex_BL_15;
	TMP_Vertex_t1422870470  ___vertex_TR_16;
	TMP_Vertex_t1422870470  ___vertex_BR_17;
	Vector3_t2243707580  ___topLeft_18;
	Vector3_t2243707580  ___bottomLeft_19;
	Vector3_t2243707580  ___topRight_20;
	Vector3_t2243707580  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t874517518  ___color_29;
	Color32_t874517518  ___underlineColor_30;
	Color32_t874517518  ___strikethroughColor_31;
	Color32_t874517518  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t1421302791_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t2285620223 * ___textElement_3;
	TMP_FontAsset_t2530419979 * ___fontAsset_4;
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t193706927 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t1422870470  ___vertex_TL_14;
	TMP_Vertex_t1422870470  ___vertex_BL_15;
	TMP_Vertex_t1422870470  ___vertex_TR_16;
	TMP_Vertex_t1422870470  ___vertex_BR_17;
	Vector3_t2243707580  ___topLeft_18;
	Vector3_t2243707580  ___bottomLeft_19;
	Vector3_t2243707580  ___topRight_20;
	Vector3_t2243707580  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t874517518  ___color_29;
	Color32_t874517518  ___underlineColor_30;
	Color32_t874517518  ___strikethroughColor_31;
	Color32_t874517518  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T1421302791_H
#ifndef LISTOBJECTSRESPONSEUNMARSHALLER_T1341882655_H
#define LISTOBJECTSRESPONSEUNMARSHALLER_T1341882655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.ListObjectsResponseUnmarshaller
struct  ListObjectsResponseUnmarshaller_t1341882655  : public S3ReponseUnmarshaller_t3340520676
{
public:

public:
};

struct ListObjectsResponseUnmarshaller_t1341882655_StaticFields
{
public:
	// Amazon.S3.Model.Internal.MarshallTransformations.ListObjectsResponseUnmarshaller Amazon.S3.Model.Internal.MarshallTransformations.ListObjectsResponseUnmarshaller::_instance
	ListObjectsResponseUnmarshaller_t1341882655 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(ListObjectsResponseUnmarshaller_t1341882655_StaticFields, ____instance_0)); }
	inline ListObjectsResponseUnmarshaller_t1341882655 * get__instance_0() const { return ____instance_0; }
	inline ListObjectsResponseUnmarshaller_t1341882655 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(ListObjectsResponseUnmarshaller_t1341882655 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTOBJECTSRESPONSEUNMARSHALLER_T1341882655_H
#ifndef XML_TAGATTRIBUTE_T1879784992_H
#define XML_TAGATTRIBUTE_T1879784992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.XML_TagAttribute
struct  XML_TagAttribute_t1879784992 
{
public:
	// System.Int32 TMPro.XML_TagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// TMPro.TagType TMPro.XML_TagAttribute::valueType
	int32_t ___valueType_1;
	// System.Int32 TMPro.XML_TagAttribute::valueStartIndex
	int32_t ___valueStartIndex_2;
	// System.Int32 TMPro.XML_TagAttribute::valueLength
	int32_t ___valueLength_3;
	// System.Int32 TMPro.XML_TagAttribute::valueHashCode
	int32_t ___valueHashCode_4;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1879784992, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueType_1() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1879784992, ___valueType_1)); }
	inline int32_t get_valueType_1() const { return ___valueType_1; }
	inline int32_t* get_address_of_valueType_1() { return &___valueType_1; }
	inline void set_valueType_1(int32_t value)
	{
		___valueType_1 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_2() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1879784992, ___valueStartIndex_2)); }
	inline int32_t get_valueStartIndex_2() const { return ___valueStartIndex_2; }
	inline int32_t* get_address_of_valueStartIndex_2() { return &___valueStartIndex_2; }
	inline void set_valueStartIndex_2(int32_t value)
	{
		___valueStartIndex_2 = value;
	}

	inline static int32_t get_offset_of_valueLength_3() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1879784992, ___valueLength_3)); }
	inline int32_t get_valueLength_3() const { return ___valueLength_3; }
	inline int32_t* get_address_of_valueLength_3() { return &___valueLength_3; }
	inline void set_valueLength_3(int32_t value)
	{
		___valueLength_3 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_4() { return static_cast<int32_t>(offsetof(XML_TagAttribute_t1879784992, ___valueHashCode_4)); }
	inline int32_t get_valueHashCode_4() const { return ___valueHashCode_4; }
	inline int32_t* get_address_of_valueHashCode_4() { return &___valueHashCode_4; }
	inline void set_valueHashCode_4(int32_t value)
	{
		___valueHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XML_TAGATTRIBUTE_T1879784992_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef COMPUTE_DT_EVENTARGS_T4231491594_H
#define COMPUTE_DT_EVENTARGS_T4231491594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DT_EventArgs
struct  Compute_DT_EventArgs_t4231491594  : public RuntimeObject
{
public:
	// TMPro.Compute_DistanceTransform_EventTypes TMPro.Compute_DT_EventArgs::EventType
	int32_t ___EventType_0;
	// System.Single TMPro.Compute_DT_EventArgs::ProgressPercentage
	float ___ProgressPercentage_1;
	// UnityEngine.Color[] TMPro.Compute_DT_EventArgs::Colors
	ColorU5BU5D_t672350442* ___Colors_2;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t4231491594, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}

	inline static int32_t get_offset_of_ProgressPercentage_1() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t4231491594, ___ProgressPercentage_1)); }
	inline float get_ProgressPercentage_1() const { return ___ProgressPercentage_1; }
	inline float* get_address_of_ProgressPercentage_1() { return &___ProgressPercentage_1; }
	inline void set_ProgressPercentage_1(float value)
	{
		___ProgressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_Colors_2() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t4231491594, ___Colors_2)); }
	inline ColorU5BU5D_t672350442* get_Colors_2() const { return ___Colors_2; }
	inline ColorU5BU5D_t672350442** get_address_of_Colors_2() { return &___Colors_2; }
	inline void set_Colors_2(ColorU5BU5D_t672350442* value)
	{
		___Colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DT_EVENTARGS_T4231491594_H
#ifndef DEFAULTRETRYPOLICY_T2207644155_H
#define DEFAULTRETRYPOLICY_T2207644155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.DefaultRetryPolicy
struct  DefaultRetryPolicy_t2207644155  : public RetryPolicy_t1476739446
{
public:
	// System.Int32 Amazon.Runtime.Internal.DefaultRetryPolicy::_maxBackoffInMilliseconds
	int32_t ____maxBackoffInMilliseconds_5;
	// Amazon.Runtime.Internal.RetryCapacity Amazon.Runtime.Internal.DefaultRetryPolicy::_retryCapacity
	RetryCapacity_t2794668894 * ____retryCapacity_6;
	// System.Collections.Generic.ICollection`1<System.Net.HttpStatusCode> Amazon.Runtime.Internal.DefaultRetryPolicy::_httpStatusCodesToRetryOn
	RuntimeObject* ____httpStatusCodesToRetryOn_7;
	// System.Collections.Generic.ICollection`1<System.Net.WebExceptionStatus> Amazon.Runtime.Internal.DefaultRetryPolicy::_webExceptionStatusesToRetryOn
	RuntimeObject* ____webExceptionStatusesToRetryOn_8;
	// System.Collections.Generic.ICollection`1<System.String> Amazon.Runtime.Internal.DefaultRetryPolicy::_errorCodesToRetryOn
	RuntimeObject* ____errorCodesToRetryOn_10;

public:
	inline static int32_t get_offset_of__maxBackoffInMilliseconds_5() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____maxBackoffInMilliseconds_5)); }
	inline int32_t get__maxBackoffInMilliseconds_5() const { return ____maxBackoffInMilliseconds_5; }
	inline int32_t* get_address_of__maxBackoffInMilliseconds_5() { return &____maxBackoffInMilliseconds_5; }
	inline void set__maxBackoffInMilliseconds_5(int32_t value)
	{
		____maxBackoffInMilliseconds_5 = value;
	}

	inline static int32_t get_offset_of__retryCapacity_6() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____retryCapacity_6)); }
	inline RetryCapacity_t2794668894 * get__retryCapacity_6() const { return ____retryCapacity_6; }
	inline RetryCapacity_t2794668894 ** get_address_of__retryCapacity_6() { return &____retryCapacity_6; }
	inline void set__retryCapacity_6(RetryCapacity_t2794668894 * value)
	{
		____retryCapacity_6 = value;
		Il2CppCodeGenWriteBarrier((&____retryCapacity_6), value);
	}

	inline static int32_t get_offset_of__httpStatusCodesToRetryOn_7() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____httpStatusCodesToRetryOn_7)); }
	inline RuntimeObject* get__httpStatusCodesToRetryOn_7() const { return ____httpStatusCodesToRetryOn_7; }
	inline RuntimeObject** get_address_of__httpStatusCodesToRetryOn_7() { return &____httpStatusCodesToRetryOn_7; }
	inline void set__httpStatusCodesToRetryOn_7(RuntimeObject* value)
	{
		____httpStatusCodesToRetryOn_7 = value;
		Il2CppCodeGenWriteBarrier((&____httpStatusCodesToRetryOn_7), value);
	}

	inline static int32_t get_offset_of__webExceptionStatusesToRetryOn_8() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____webExceptionStatusesToRetryOn_8)); }
	inline RuntimeObject* get__webExceptionStatusesToRetryOn_8() const { return ____webExceptionStatusesToRetryOn_8; }
	inline RuntimeObject** get_address_of__webExceptionStatusesToRetryOn_8() { return &____webExceptionStatusesToRetryOn_8; }
	inline void set__webExceptionStatusesToRetryOn_8(RuntimeObject* value)
	{
		____webExceptionStatusesToRetryOn_8 = value;
		Il2CppCodeGenWriteBarrier((&____webExceptionStatusesToRetryOn_8), value);
	}

	inline static int32_t get_offset_of__errorCodesToRetryOn_10() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____errorCodesToRetryOn_10)); }
	inline RuntimeObject* get__errorCodesToRetryOn_10() const { return ____errorCodesToRetryOn_10; }
	inline RuntimeObject** get_address_of__errorCodesToRetryOn_10() { return &____errorCodesToRetryOn_10; }
	inline void set__errorCodesToRetryOn_10(RuntimeObject* value)
	{
		____errorCodesToRetryOn_10 = value;
		Il2CppCodeGenWriteBarrier((&____errorCodesToRetryOn_10), value);
	}
};

struct DefaultRetryPolicy_t2207644155_StaticFields
{
public:
	// Amazon.Runtime.Internal.CapacityManager Amazon.Runtime.Internal.DefaultRetryPolicy::_capacityManagerInstance
	CapacityManager_t2181902141 * ____capacityManagerInstance_4;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.Internal.DefaultRetryPolicy::_coreCLRRetryErrorMessages
	HashSet_1_t362681087 * ____coreCLRRetryErrorMessages_9;

public:
	inline static int32_t get_offset_of__capacityManagerInstance_4() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155_StaticFields, ____capacityManagerInstance_4)); }
	inline CapacityManager_t2181902141 * get__capacityManagerInstance_4() const { return ____capacityManagerInstance_4; }
	inline CapacityManager_t2181902141 ** get_address_of__capacityManagerInstance_4() { return &____capacityManagerInstance_4; }
	inline void set__capacityManagerInstance_4(CapacityManager_t2181902141 * value)
	{
		____capacityManagerInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&____capacityManagerInstance_4), value);
	}

	inline static int32_t get_offset_of__coreCLRRetryErrorMessages_9() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155_StaticFields, ____coreCLRRetryErrorMessages_9)); }
	inline HashSet_1_t362681087 * get__coreCLRRetryErrorMessages_9() const { return ____coreCLRRetryErrorMessages_9; }
	inline HashSet_1_t362681087 ** get_address_of__coreCLRRetryErrorMessages_9() { return &____coreCLRRetryErrorMessages_9; }
	inline void set__coreCLRRetryErrorMessages_9(HashSet_1_t362681087 * value)
	{
		____coreCLRRetryErrorMessages_9 = value;
		Il2CppCodeGenWriteBarrier((&____coreCLRRetryErrorMessages_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTRETRYPOLICY_T2207644155_H
#ifndef ERRORRESPONSE_T3502566035_H
#define ERRORRESPONSE_T3502566035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ErrorResponse
struct  ErrorResponse_t3502566035  : public RuntimeObject
{
public:
	// Amazon.Runtime.ErrorType Amazon.Runtime.Internal.ErrorResponse::type
	int32_t ___type_0;
	// System.String Amazon.Runtime.Internal.ErrorResponse::code
	String_t* ___code_1;
	// System.String Amazon.Runtime.Internal.ErrorResponse::message
	String_t* ___message_2;
	// System.String Amazon.Runtime.Internal.ErrorResponse::requestId
	String_t* ___requestId_3;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_code_1() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___code_1)); }
	inline String_t* get_code_1() const { return ___code_1; }
	inline String_t** get_address_of_code_1() { return &___code_1; }
	inline void set_code_1(String_t* value)
	{
		___code_1 = value;
		Il2CppCodeGenWriteBarrier((&___code_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_requestId_3() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___requestId_3)); }
	inline String_t* get_requestId_3() const { return ___requestId_3; }
	inline String_t** get_address_of_requestId_3() { return &___requestId_3; }
	inline void set_requestId_3(String_t* value)
	{
		___requestId_3 = value;
		Il2CppCodeGenWriteBarrier((&___requestId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORRESPONSE_T3502566035_H
#ifndef XMLUNMARSHALLERCONTEXT_T1179575220_H
#define XMLUNMARSHALLERCONTEXT_T1179575220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct  XmlUnmarshallerContext_t1179575220  : public UnmarshallerContext_t1608322995
{
public:
	// System.IO.StreamReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::streamReader
	StreamReader_t2360341767 * ___streamReader_8;
	// System.Xml.XmlReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::_xmlReader
	XmlReader_t3675626668 * ____xmlReader_9;
	// System.Collections.Generic.Stack`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::stack
	Stack_1_t3116948387 * ___stack_10;
	// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::stackString
	String_t* ___stackString_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeValues
	Dictionary_2_t3943999495 * ___attributeValues_12;
	// System.Collections.Generic.List`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeNames
	List_1_t1398341365 * ___attributeNames_13;
	// System.Collections.Generic.IEnumerator`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeEnumerator
	RuntimeObject* ___attributeEnumerator_14;
	// System.Xml.XmlNodeType Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodeType
	int32_t ___nodeType_15;
	// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodeContent
	String_t* ___nodeContent_16;
	// System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::disposed
	bool ___disposed_17;

public:
	inline static int32_t get_offset_of_streamReader_8() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___streamReader_8)); }
	inline StreamReader_t2360341767 * get_streamReader_8() const { return ___streamReader_8; }
	inline StreamReader_t2360341767 ** get_address_of_streamReader_8() { return &___streamReader_8; }
	inline void set_streamReader_8(StreamReader_t2360341767 * value)
	{
		___streamReader_8 = value;
		Il2CppCodeGenWriteBarrier((&___streamReader_8), value);
	}

	inline static int32_t get_offset_of__xmlReader_9() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ____xmlReader_9)); }
	inline XmlReader_t3675626668 * get__xmlReader_9() const { return ____xmlReader_9; }
	inline XmlReader_t3675626668 ** get_address_of__xmlReader_9() { return &____xmlReader_9; }
	inline void set__xmlReader_9(XmlReader_t3675626668 * value)
	{
		____xmlReader_9 = value;
		Il2CppCodeGenWriteBarrier((&____xmlReader_9), value);
	}

	inline static int32_t get_offset_of_stack_10() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___stack_10)); }
	inline Stack_1_t3116948387 * get_stack_10() const { return ___stack_10; }
	inline Stack_1_t3116948387 ** get_address_of_stack_10() { return &___stack_10; }
	inline void set_stack_10(Stack_1_t3116948387 * value)
	{
		___stack_10 = value;
		Il2CppCodeGenWriteBarrier((&___stack_10), value);
	}

	inline static int32_t get_offset_of_stackString_11() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___stackString_11)); }
	inline String_t* get_stackString_11() const { return ___stackString_11; }
	inline String_t** get_address_of_stackString_11() { return &___stackString_11; }
	inline void set_stackString_11(String_t* value)
	{
		___stackString_11 = value;
		Il2CppCodeGenWriteBarrier((&___stackString_11), value);
	}

	inline static int32_t get_offset_of_attributeValues_12() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___attributeValues_12)); }
	inline Dictionary_2_t3943999495 * get_attributeValues_12() const { return ___attributeValues_12; }
	inline Dictionary_2_t3943999495 ** get_address_of_attributeValues_12() { return &___attributeValues_12; }
	inline void set_attributeValues_12(Dictionary_2_t3943999495 * value)
	{
		___attributeValues_12 = value;
		Il2CppCodeGenWriteBarrier((&___attributeValues_12), value);
	}

	inline static int32_t get_offset_of_attributeNames_13() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___attributeNames_13)); }
	inline List_1_t1398341365 * get_attributeNames_13() const { return ___attributeNames_13; }
	inline List_1_t1398341365 ** get_address_of_attributeNames_13() { return &___attributeNames_13; }
	inline void set_attributeNames_13(List_1_t1398341365 * value)
	{
		___attributeNames_13 = value;
		Il2CppCodeGenWriteBarrier((&___attributeNames_13), value);
	}

	inline static int32_t get_offset_of_attributeEnumerator_14() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___attributeEnumerator_14)); }
	inline RuntimeObject* get_attributeEnumerator_14() const { return ___attributeEnumerator_14; }
	inline RuntimeObject** get_address_of_attributeEnumerator_14() { return &___attributeEnumerator_14; }
	inline void set_attributeEnumerator_14(RuntimeObject* value)
	{
		___attributeEnumerator_14 = value;
		Il2CppCodeGenWriteBarrier((&___attributeEnumerator_14), value);
	}

	inline static int32_t get_offset_of_nodeType_15() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___nodeType_15)); }
	inline int32_t get_nodeType_15() const { return ___nodeType_15; }
	inline int32_t* get_address_of_nodeType_15() { return &___nodeType_15; }
	inline void set_nodeType_15(int32_t value)
	{
		___nodeType_15 = value;
	}

	inline static int32_t get_offset_of_nodeContent_16() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___nodeContent_16)); }
	inline String_t* get_nodeContent_16() const { return ___nodeContent_16; }
	inline String_t** get_address_of_nodeContent_16() { return &___nodeContent_16; }
	inline void set_nodeContent_16(String_t* value)
	{
		___nodeContent_16 = value;
		Il2CppCodeGenWriteBarrier((&___nodeContent_16), value);
	}

	inline static int32_t get_offset_of_disposed_17() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___disposed_17)); }
	inline bool get_disposed_17() const { return ___disposed_17; }
	inline bool* get_address_of_disposed_17() { return &___disposed_17; }
	inline void set_disposed_17(bool value)
	{
		___disposed_17 = value;
	}
};

struct XmlUnmarshallerContext_t1179575220_StaticFields
{
public:
	// System.Xml.XmlReaderSettings Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::READER_SETTINGS
	XmlReaderSettings_t1578612233 * ___READER_SETTINGS_6;
	// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodesToSkip
	HashSet_1_t3367932747 * ___nodesToSkip_7;

public:
	inline static int32_t get_offset_of_READER_SETTINGS_6() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220_StaticFields, ___READER_SETTINGS_6)); }
	inline XmlReaderSettings_t1578612233 * get_READER_SETTINGS_6() const { return ___READER_SETTINGS_6; }
	inline XmlReaderSettings_t1578612233 ** get_address_of_READER_SETTINGS_6() { return &___READER_SETTINGS_6; }
	inline void set_READER_SETTINGS_6(XmlReaderSettings_t1578612233 * value)
	{
		___READER_SETTINGS_6 = value;
		Il2CppCodeGenWriteBarrier((&___READER_SETTINGS_6), value);
	}

	inline static int32_t get_offset_of_nodesToSkip_7() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220_StaticFields, ___nodesToSkip_7)); }
	inline HashSet_1_t3367932747 * get_nodesToSkip_7() const { return ___nodesToSkip_7; }
	inline HashSet_1_t3367932747 ** get_address_of_nodesToSkip_7() { return &___nodesToSkip_7; }
	inline void set_nodesToSkip_7(HashSet_1_t3367932747 * value)
	{
		___nodesToSkip_7 = value;
		Il2CppCodeGenWriteBarrier((&___nodesToSkip_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUNMARSHALLERCONTEXT_T1179575220_H
#ifndef S3ERRORRESPONSE_T623087499_H
#define S3ERRORRESPONSE_T623087499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.S3ErrorResponse
struct  S3ErrorResponse_t623087499  : public ErrorResponse_t3502566035
{
public:
	// System.String Amazon.S3.Model.Internal.MarshallTransformations.S3ErrorResponse::<Region>k__BackingField
	String_t* ___U3CRegionU3Ek__BackingField_4;
	// System.String Amazon.S3.Model.Internal.MarshallTransformations.S3ErrorResponse::<Resource>k__BackingField
	String_t* ___U3CResourceU3Ek__BackingField_5;
	// System.String Amazon.S3.Model.Internal.MarshallTransformations.S3ErrorResponse::<Id2>k__BackingField
	String_t* ___U3CId2U3Ek__BackingField_6;
	// System.String Amazon.S3.Model.Internal.MarshallTransformations.S3ErrorResponse::<AmzCfId>k__BackingField
	String_t* ___U3CAmzCfIdU3Ek__BackingField_7;
	// System.Exception Amazon.S3.Model.Internal.MarshallTransformations.S3ErrorResponse::<ParsingException>k__BackingField
	Exception_t1927440687 * ___U3CParsingExceptionU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(S3ErrorResponse_t623087499, ___U3CRegionU3Ek__BackingField_4)); }
	inline String_t* get_U3CRegionU3Ek__BackingField_4() const { return ___U3CRegionU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CRegionU3Ek__BackingField_4() { return &___U3CRegionU3Ek__BackingField_4; }
	inline void set_U3CRegionU3Ek__BackingField_4(String_t* value)
	{
		___U3CRegionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRegionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CResourceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(S3ErrorResponse_t623087499, ___U3CResourceU3Ek__BackingField_5)); }
	inline String_t* get_U3CResourceU3Ek__BackingField_5() const { return ___U3CResourceU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CResourceU3Ek__BackingField_5() { return &___U3CResourceU3Ek__BackingField_5; }
	inline void set_U3CResourceU3Ek__BackingField_5(String_t* value)
	{
		___U3CResourceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResourceU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CId2U3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(S3ErrorResponse_t623087499, ___U3CId2U3Ek__BackingField_6)); }
	inline String_t* get_U3CId2U3Ek__BackingField_6() const { return ___U3CId2U3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CId2U3Ek__BackingField_6() { return &___U3CId2U3Ek__BackingField_6; }
	inline void set_U3CId2U3Ek__BackingField_6(String_t* value)
	{
		___U3CId2U3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CId2U3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CAmzCfIdU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(S3ErrorResponse_t623087499, ___U3CAmzCfIdU3Ek__BackingField_7)); }
	inline String_t* get_U3CAmzCfIdU3Ek__BackingField_7() const { return ___U3CAmzCfIdU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CAmzCfIdU3Ek__BackingField_7() { return &___U3CAmzCfIdU3Ek__BackingField_7; }
	inline void set_U3CAmzCfIdU3Ek__BackingField_7(String_t* value)
	{
		___U3CAmzCfIdU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAmzCfIdU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CParsingExceptionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(S3ErrorResponse_t623087499, ___U3CParsingExceptionU3Ek__BackingField_8)); }
	inline Exception_t1927440687 * get_U3CParsingExceptionU3Ek__BackingField_8() const { return ___U3CParsingExceptionU3Ek__BackingField_8; }
	inline Exception_t1927440687 ** get_address_of_U3CParsingExceptionU3Ek__BackingField_8() { return &___U3CParsingExceptionU3Ek__BackingField_8; }
	inline void set_U3CParsingExceptionU3Ek__BackingField_8(Exception_t1927440687 * value)
	{
		___U3CParsingExceptionU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParsingExceptionU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3ERRORRESPONSE_T623087499_H
#ifndef S3UNMARSHALLERCONTEXT_T295718887_H
#define S3UNMARSHALLERCONTEXT_T295718887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Internal.MarshallTransformations.S3UnmarshallerContext
struct  S3UnmarshallerContext_t295718887  : public XmlUnmarshallerContext_t1179575220
{
public:
	// System.Boolean Amazon.S3.Model.Internal.MarshallTransformations.S3UnmarshallerContext::_checkedForErrorResponse
	bool ____checkedForErrorResponse_18;

public:
	inline static int32_t get_offset_of__checkedForErrorResponse_18() { return static_cast<int32_t>(offsetof(S3UnmarshallerContext_t295718887, ____checkedForErrorResponse_18)); }
	inline bool get__checkedForErrorResponse_18() const { return ____checkedForErrorResponse_18; }
	inline bool* get_address_of__checkedForErrorResponse_18() { return &____checkedForErrorResponse_18; }
	inline void set__checkedForErrorResponse_18(bool value)
	{
		____checkedForErrorResponse_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3UNMARSHALLERCONTEXT_T295718887_H
#ifndef AMAZONS3RETRYPOLICY_T2252046324_H
#define AMAZONS3RETRYPOLICY_T2252046324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Internal.AmazonS3RetryPolicy
struct  AmazonS3RetryPolicy_t2252046324  : public DefaultRetryPolicy_t2207644155
{
public:

public:
};

struct AmazonS3RetryPolicy_t2252046324_StaticFields
{
public:
	// System.Collections.Generic.ICollection`1<System.Type> Amazon.S3.Internal.AmazonS3RetryPolicy::RequestsWith200Error
	RuntimeObject* ___RequestsWith200Error_11;

public:
	inline static int32_t get_offset_of_RequestsWith200Error_11() { return static_cast<int32_t>(offsetof(AmazonS3RetryPolicy_t2252046324_StaticFields, ___RequestsWith200Error_11)); }
	inline RuntimeObject* get_RequestsWith200Error_11() const { return ___RequestsWith200Error_11; }
	inline RuntimeObject** get_address_of_RequestsWith200Error_11() { return &___RequestsWith200Error_11; }
	inline void set_RequestsWith200Error_11(RuntimeObject* value)
	{
		___RequestsWith200Error_11 = value;
		Il2CppCodeGenWriteBarrier((&___RequestsWith200Error_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONS3RETRYPOLICY_T2252046324_H
#ifndef LISTOBJECTSRESPONSE_T2587169543_H
#define LISTOBJECTSRESPONSE_T2587169543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.ListObjectsResponse
struct  ListObjectsResponse_t2587169543  : public AmazonWebServiceResponse_t529043356
{
public:
	// System.Nullable`1<System.Boolean> Amazon.S3.Model.ListObjectsResponse::isTruncated
	Nullable_1_t2088641033  ___isTruncated_3;
	// System.String Amazon.S3.Model.ListObjectsResponse::nextMarker
	String_t* ___nextMarker_4;
	// System.Collections.Generic.List`1<Amazon.S3.Model.S3Object> Amazon.S3.Model.ListObjectsResponse::contents
	List_1_t1205837151 * ___contents_5;
	// System.String Amazon.S3.Model.ListObjectsResponse::name
	String_t* ___name_6;
	// System.String Amazon.S3.Model.ListObjectsResponse::prefix
	String_t* ___prefix_7;
	// System.Nullable`1<System.Int32> Amazon.S3.Model.ListObjectsResponse::maxKeys
	Nullable_1_t334943763  ___maxKeys_8;
	// System.Collections.Generic.List`1<System.String> Amazon.S3.Model.ListObjectsResponse::commonPrefixes
	List_1_t1398341365 * ___commonPrefixes_9;
	// System.String Amazon.S3.Model.ListObjectsResponse::delimiter
	String_t* ___delimiter_10;

public:
	inline static int32_t get_offset_of_isTruncated_3() { return static_cast<int32_t>(offsetof(ListObjectsResponse_t2587169543, ___isTruncated_3)); }
	inline Nullable_1_t2088641033  get_isTruncated_3() const { return ___isTruncated_3; }
	inline Nullable_1_t2088641033 * get_address_of_isTruncated_3() { return &___isTruncated_3; }
	inline void set_isTruncated_3(Nullable_1_t2088641033  value)
	{
		___isTruncated_3 = value;
	}

	inline static int32_t get_offset_of_nextMarker_4() { return static_cast<int32_t>(offsetof(ListObjectsResponse_t2587169543, ___nextMarker_4)); }
	inline String_t* get_nextMarker_4() const { return ___nextMarker_4; }
	inline String_t** get_address_of_nextMarker_4() { return &___nextMarker_4; }
	inline void set_nextMarker_4(String_t* value)
	{
		___nextMarker_4 = value;
		Il2CppCodeGenWriteBarrier((&___nextMarker_4), value);
	}

	inline static int32_t get_offset_of_contents_5() { return static_cast<int32_t>(offsetof(ListObjectsResponse_t2587169543, ___contents_5)); }
	inline List_1_t1205837151 * get_contents_5() const { return ___contents_5; }
	inline List_1_t1205837151 ** get_address_of_contents_5() { return &___contents_5; }
	inline void set_contents_5(List_1_t1205837151 * value)
	{
		___contents_5 = value;
		Il2CppCodeGenWriteBarrier((&___contents_5), value);
	}

	inline static int32_t get_offset_of_name_6() { return static_cast<int32_t>(offsetof(ListObjectsResponse_t2587169543, ___name_6)); }
	inline String_t* get_name_6() const { return ___name_6; }
	inline String_t** get_address_of_name_6() { return &___name_6; }
	inline void set_name_6(String_t* value)
	{
		___name_6 = value;
		Il2CppCodeGenWriteBarrier((&___name_6), value);
	}

	inline static int32_t get_offset_of_prefix_7() { return static_cast<int32_t>(offsetof(ListObjectsResponse_t2587169543, ___prefix_7)); }
	inline String_t* get_prefix_7() const { return ___prefix_7; }
	inline String_t** get_address_of_prefix_7() { return &___prefix_7; }
	inline void set_prefix_7(String_t* value)
	{
		___prefix_7 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_7), value);
	}

	inline static int32_t get_offset_of_maxKeys_8() { return static_cast<int32_t>(offsetof(ListObjectsResponse_t2587169543, ___maxKeys_8)); }
	inline Nullable_1_t334943763  get_maxKeys_8() const { return ___maxKeys_8; }
	inline Nullable_1_t334943763 * get_address_of_maxKeys_8() { return &___maxKeys_8; }
	inline void set_maxKeys_8(Nullable_1_t334943763  value)
	{
		___maxKeys_8 = value;
	}

	inline static int32_t get_offset_of_commonPrefixes_9() { return static_cast<int32_t>(offsetof(ListObjectsResponse_t2587169543, ___commonPrefixes_9)); }
	inline List_1_t1398341365 * get_commonPrefixes_9() const { return ___commonPrefixes_9; }
	inline List_1_t1398341365 ** get_address_of_commonPrefixes_9() { return &___commonPrefixes_9; }
	inline void set_commonPrefixes_9(List_1_t1398341365 * value)
	{
		___commonPrefixes_9 = value;
		Il2CppCodeGenWriteBarrier((&___commonPrefixes_9), value);
	}

	inline static int32_t get_offset_of_delimiter_10() { return static_cast<int32_t>(offsetof(ListObjectsResponse_t2587169543, ___delimiter_10)); }
	inline String_t* get_delimiter_10() const { return ___delimiter_10; }
	inline String_t** get_address_of_delimiter_10() { return &___delimiter_10; }
	inline void set_delimiter_10(String_t* value)
	{
		___delimiter_10 = value;
		Il2CppCodeGenWriteBarrier((&___delimiter_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTOBJECTSRESPONSE_T2587169543_H
#ifndef STREAMRESPONSE_T2490679653_H
#define STREAMRESPONSE_T2490679653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.StreamResponse
struct  StreamResponse_t2490679653  : public AmazonWebServiceResponse_t529043356
{
public:
	// System.Boolean Amazon.S3.Model.StreamResponse::disposed
	bool ___disposed_3;
	// System.IO.Stream Amazon.S3.Model.StreamResponse::responseStream
	Stream_t3255436806 * ___responseStream_4;

public:
	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(StreamResponse_t2490679653, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}

	inline static int32_t get_offset_of_responseStream_4() { return static_cast<int32_t>(offsetof(StreamResponse_t2490679653, ___responseStream_4)); }
	inline Stream_t3255436806 * get_responseStream_4() const { return ___responseStream_4; }
	inline Stream_t3255436806 ** get_address_of_responseStream_4() { return &___responseStream_4; }
	inline void set_responseStream_4(Stream_t3255436806 * value)
	{
		___responseStream_4 = value;
		Il2CppCodeGenWriteBarrier((&___responseStream_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMRESPONSE_T2490679653_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef PUTOBJECTRESPONSE_T418550917_H
#define PUTOBJECTRESPONSE_T418550917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.PutObjectResponse
struct  PutObjectResponse_t418550917  : public AmazonWebServiceResponse_t529043356
{
public:
	// System.String Amazon.S3.Model.PutObjectResponse::eTag
	String_t* ___eTag_3;

public:
	inline static int32_t get_offset_of_eTag_3() { return static_cast<int32_t>(offsetof(PutObjectResponse_t418550917, ___eTag_3)); }
	inline String_t* get_eTag_3() const { return ___eTag_3; }
	inline String_t** get_address_of_eTag_3() { return &___eTag_3; }
	inline void set_eTag_3(String_t* value)
	{
		___eTag_3 = value;
		Il2CppCodeGenWriteBarrier((&___eTag_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUTOBJECTRESPONSE_T418550917_H
#ifndef UPLOADPARTRESPONSE_T2256813681_H
#define UPLOADPARTRESPONSE_T2256813681_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.UploadPartResponse
struct  UploadPartResponse_t2256813681  : public AmazonWebServiceResponse_t529043356
{
public:
	// System.String Amazon.S3.Model.UploadPartResponse::eTag
	String_t* ___eTag_3;
	// System.Int32 Amazon.S3.Model.UploadPartResponse::partNumber
	int32_t ___partNumber_4;

public:
	inline static int32_t get_offset_of_eTag_3() { return static_cast<int32_t>(offsetof(UploadPartResponse_t2256813681, ___eTag_3)); }
	inline String_t* get_eTag_3() const { return ___eTag_3; }
	inline String_t** get_address_of_eTag_3() { return &___eTag_3; }
	inline void set_eTag_3(String_t* value)
	{
		___eTag_3 = value;
		Il2CppCodeGenWriteBarrier((&___eTag_3), value);
	}

	inline static int32_t get_offset_of_partNumber_4() { return static_cast<int32_t>(offsetof(UploadPartResponse_t2256813681, ___partNumber_4)); }
	inline int32_t get_partNumber_4() const { return ___partNumber_4; }
	inline int32_t* get_address_of_partNumber_4() { return &___partNumber_4; }
	inline void set_partNumber_4(int32_t value)
	{
		___partNumber_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADPARTRESPONSE_T2256813681_H
#ifndef DELETEOBJECTSRESPONSE_T4016776830_H
#define DELETEOBJECTSRESPONSE_T4016776830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.DeleteObjectsResponse
struct  DeleteObjectsResponse_t4016776830  : public AmazonWebServiceResponse_t529043356
{
public:
	// System.Collections.Generic.List`1<Amazon.S3.Model.DeletedObject> Amazon.S3.Model.DeleteObjectsResponse::deleted
	List_1_t2033343350 * ___deleted_3;
	// System.Collections.Generic.List`1<Amazon.S3.Model.DeleteError> Amazon.S3.Model.DeleteObjectsResponse::errors
	List_1_t2768982555 * ___errors_4;
	// Amazon.S3.RequestCharged Amazon.S3.Model.DeleteObjectsResponse::requestCharged
	RequestCharged_t2438105727 * ___requestCharged_5;

public:
	inline static int32_t get_offset_of_deleted_3() { return static_cast<int32_t>(offsetof(DeleteObjectsResponse_t4016776830, ___deleted_3)); }
	inline List_1_t2033343350 * get_deleted_3() const { return ___deleted_3; }
	inline List_1_t2033343350 ** get_address_of_deleted_3() { return &___deleted_3; }
	inline void set_deleted_3(List_1_t2033343350 * value)
	{
		___deleted_3 = value;
		Il2CppCodeGenWriteBarrier((&___deleted_3), value);
	}

	inline static int32_t get_offset_of_errors_4() { return static_cast<int32_t>(offsetof(DeleteObjectsResponse_t4016776830, ___errors_4)); }
	inline List_1_t2768982555 * get_errors_4() const { return ___errors_4; }
	inline List_1_t2768982555 ** get_address_of_errors_4() { return &___errors_4; }
	inline void set_errors_4(List_1_t2768982555 * value)
	{
		___errors_4 = value;
		Il2CppCodeGenWriteBarrier((&___errors_4), value);
	}

	inline static int32_t get_offset_of_requestCharged_5() { return static_cast<int32_t>(offsetof(DeleteObjectsResponse_t4016776830, ___requestCharged_5)); }
	inline RequestCharged_t2438105727 * get_requestCharged_5() const { return ___requestCharged_5; }
	inline RequestCharged_t2438105727 ** get_address_of_requestCharged_5() { return &___requestCharged_5; }
	inline void set_requestCharged_5(RequestCharged_t2438105727 * value)
	{
		___requestCharged_5 = value;
		Il2CppCodeGenWriteBarrier((&___requestCharged_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELETEOBJECTSRESPONSE_T4016776830_H
#ifndef NULLABLE_1_T3251239280_H
#define NULLABLE_1_T3251239280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t3251239280 
{
public:
	// T System.Nullable`1::value
	DateTime_t693205669  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3251239280, ___value_0)); }
	inline DateTime_t693205669  get_value_0() const { return ___value_0; }
	inline DateTime_t693205669 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t693205669  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3251239280, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3251239280_H
#ifndef WORDWRAPSTATE_T433984875_H
#define WORDWRAPSTATE_T433984875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t433984875 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t2849466151 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t2320418126  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t874517518  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t874517518  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t874517518  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t874517518  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t937156555  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t1533070037  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t1533070037  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t1533070037  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t1533070037  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t1818389866  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t2735062451  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t2735062451  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2730429967  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2730429967  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t2735062451  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2730429967  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t3512906015  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t2125340843  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t2530419979 * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t2641813093 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t193706927 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3018556803  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___textInfo_27)); }
	inline TMP_TextInfo_t2849466151 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t2849466151 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lineInfo_28)); }
	inline TMP_LineInfo_t2320418126  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t2320418126 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t2320418126  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___vertexColor_29)); }
	inline Color32_t874517518  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t874517518 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t874517518  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___underlineColor_30)); }
	inline Color32_t874517518  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t874517518 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t874517518  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___strikethroughColor_31)); }
	inline Color32_t874517518  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t874517518 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t874517518  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___highlightColor_32)); }
	inline Color32_t874517518  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t874517518 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t874517518  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t937156555  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t937156555 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t937156555  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t1533070037  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t1533070037  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t1533070037  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t1533070037  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t1533070037  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t1533070037  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t1533070037  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t1533070037  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t1818389866  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t1818389866 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t1818389866  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t2735062451  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t2735062451  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t2735062451  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t2735062451  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2730429967  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2730429967  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2730429967  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2730429967  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t2735062451  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t2735062451  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2730429967  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2730429967  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_t3512906015  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_t3512906015 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_t3512906015  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t2125340843  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t2125340843 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t2125340843  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t2530419979 * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t2530419979 * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_t2641813093 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_t2641813093 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentMaterial_50)); }
	inline Material_t193706927 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_t193706927 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_t193706927 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___meshExtents_52)); }
	inline Extents_t3018556803  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_t3018556803 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_t3018556803  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t433984875_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t2849466151 * ___textInfo_27;
	TMP_LineInfo_t2320418126  ___lineInfo_28;
	Color32_t874517518  ___vertexColor_29;
	Color32_t874517518  ___underlineColor_30;
	Color32_t874517518  ___strikethroughColor_31;
	Color32_t874517518  ___highlightColor_32;
	TMP_BasicXmlTagStack_t937156555  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t1533070037  ___colorStack_34;
	TMP_XmlTagStack_1_t1533070037  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t1533070037  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t1533070037  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t1818389866  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t2735062451  ___sizeStack_39;
	TMP_XmlTagStack_1_t2735062451  ___indentStack_40;
	TMP_XmlTagStack_1_t2730429967  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2730429967  ___styleStack_42;
	TMP_XmlTagStack_1_t2735062451  ___baselineStack_43;
	TMP_XmlTagStack_1_t2730429967  ___actionStack_44;
	TMP_XmlTagStack_1_t3512906015  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t2125340843  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t2530419979 * ___currentFontAsset_48;
	TMP_SpriteAsset_t2641813093 * ___currentSpriteAsset_49;
	Material_t193706927 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3018556803  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t433984875_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t2849466151 * ___textInfo_27;
	TMP_LineInfo_t2320418126  ___lineInfo_28;
	Color32_t874517518  ___vertexColor_29;
	Color32_t874517518  ___underlineColor_30;
	Color32_t874517518  ___strikethroughColor_31;
	Color32_t874517518  ___highlightColor_32;
	TMP_BasicXmlTagStack_t937156555  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t1533070037  ___colorStack_34;
	TMP_XmlTagStack_1_t1533070037  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t1533070037  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t1533070037  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t1818389866  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t2735062451  ___sizeStack_39;
	TMP_XmlTagStack_1_t2735062451  ___indentStack_40;
	TMP_XmlTagStack_1_t2730429967  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2730429967  ___styleStack_42;
	TMP_XmlTagStack_1_t2735062451  ___baselineStack_43;
	TMP_XmlTagStack_1_t2730429967  ___actionStack_44;
	TMP_XmlTagStack_1_t3512906015  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t2125340843  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t2530419979 * ___currentFontAsset_48;
	TMP_SpriteAsset_t2641813093 * ___currentSpriteAsset_49;
	Material_t193706927 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3018556803  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T433984875_H
#ifndef COPYPARTRESPONSE_T2814382569_H
#define COPYPARTRESPONSE_T2814382569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.CopyPartResponse
struct  CopyPartResponse_t2814382569  : public AmazonWebServiceResponse_t529043356
{
public:
	// System.Int32 Amazon.S3.Model.CopyPartResponse::partNumber
	int32_t ___partNumber_3;

public:
	inline static int32_t get_offset_of_partNumber_3() { return static_cast<int32_t>(offsetof(CopyPartResponse_t2814382569, ___partNumber_3)); }
	inline int32_t get_partNumber_3() const { return ___partNumber_3; }
	inline int32_t* get_address_of_partNumber_3() { return &___partNumber_3; }
	inline void set_partNumber_3(int32_t value)
	{
		___partNumber_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYPARTRESPONSE_T2814382569_H
#ifndef TMP_COLORGRADIENT_T1159837347_H
#define TMP_COLORGRADIENT_T1159837347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_ColorGradient
struct  TMP_ColorGradient_t1159837347  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.Color TMPro.TMP_ColorGradient::topLeft
	Color_t2020392075  ___topLeft_2;
	// UnityEngine.Color TMPro.TMP_ColorGradient::topRight
	Color_t2020392075  ___topRight_3;
	// UnityEngine.Color TMPro.TMP_ColorGradient::bottomLeft
	Color_t2020392075  ___bottomLeft_4;
	// UnityEngine.Color TMPro.TMP_ColorGradient::bottomRight
	Color_t2020392075  ___bottomRight_5;

public:
	inline static int32_t get_offset_of_topLeft_2() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_t1159837347, ___topLeft_2)); }
	inline Color_t2020392075  get_topLeft_2() const { return ___topLeft_2; }
	inline Color_t2020392075 * get_address_of_topLeft_2() { return &___topLeft_2; }
	inline void set_topLeft_2(Color_t2020392075  value)
	{
		___topLeft_2 = value;
	}

	inline static int32_t get_offset_of_topRight_3() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_t1159837347, ___topRight_3)); }
	inline Color_t2020392075  get_topRight_3() const { return ___topRight_3; }
	inline Color_t2020392075 * get_address_of_topRight_3() { return &___topRight_3; }
	inline void set_topRight_3(Color_t2020392075  value)
	{
		___topRight_3 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_4() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_t1159837347, ___bottomLeft_4)); }
	inline Color_t2020392075  get_bottomLeft_4() const { return ___bottomLeft_4; }
	inline Color_t2020392075 * get_address_of_bottomLeft_4() { return &___bottomLeft_4; }
	inline void set_bottomLeft_4(Color_t2020392075  value)
	{
		___bottomLeft_4 = value;
	}

	inline static int32_t get_offset_of_bottomRight_5() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_t1159837347, ___bottomRight_5)); }
	inline Color_t2020392075  get_bottomRight_5() const { return ___bottomRight_5; }
	inline Color_t2020392075 * get_address_of_bottomRight_5() { return &___bottomRight_5; }
	inline void set_bottomRight_5(Color_t2020392075  value)
	{
		___bottomRight_5 = value;
	}
};

struct TMP_ColorGradient_t1159837347_StaticFields
{
public:
	// UnityEngine.Color TMPro.TMP_ColorGradient::k_defaultColor
	Color_t2020392075  ___k_defaultColor_6;

public:
	inline static int32_t get_offset_of_k_defaultColor_6() { return static_cast<int32_t>(offsetof(TMP_ColorGradient_t1159837347_StaticFields, ___k_defaultColor_6)); }
	inline Color_t2020392075  get_k_defaultColor_6() const { return ___k_defaultColor_6; }
	inline Color_t2020392075 * get_address_of_k_defaultColor_6() { return &___k_defaultColor_6; }
	inline void set_k_defaultColor_6(Color_t2020392075  value)
	{
		___k_defaultColor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_COLORGRADIENT_T1159837347_H
#ifndef EXPIRATION_T12298299_H
#define EXPIRATION_T12298299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.Expiration
struct  Expiration_t12298299  : public RuntimeObject
{
public:
	// System.DateTime Amazon.S3.Model.Expiration::expiryDate
	DateTime_t693205669  ___expiryDate_0;
	// System.String Amazon.S3.Model.Expiration::ruleId
	String_t* ___ruleId_1;

public:
	inline static int32_t get_offset_of_expiryDate_0() { return static_cast<int32_t>(offsetof(Expiration_t12298299, ___expiryDate_0)); }
	inline DateTime_t693205669  get_expiryDate_0() const { return ___expiryDate_0; }
	inline DateTime_t693205669 * get_address_of_expiryDate_0() { return &___expiryDate_0; }
	inline void set_expiryDate_0(DateTime_t693205669  value)
	{
		___expiryDate_0 = value;
	}

	inline static int32_t get_offset_of_ruleId_1() { return static_cast<int32_t>(offsetof(Expiration_t12298299, ___ruleId_1)); }
	inline String_t* get_ruleId_1() const { return ___ruleId_1; }
	inline String_t** get_address_of_ruleId_1() { return &___ruleId_1; }
	inline void set_ruleId_1(String_t* value)
	{
		___ruleId_1 = value;
		Il2CppCodeGenWriteBarrier((&___ruleId_1), value);
	}
};

struct Expiration_t12298299_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex Amazon.S3.Model.Expiration::expiryRegex
	Regex_t1803876613 * ___expiryRegex_2;
	// System.Text.RegularExpressions.Regex Amazon.S3.Model.Expiration::ruleRegex
	Regex_t1803876613 * ___ruleRegex_3;

public:
	inline static int32_t get_offset_of_expiryRegex_2() { return static_cast<int32_t>(offsetof(Expiration_t12298299_StaticFields, ___expiryRegex_2)); }
	inline Regex_t1803876613 * get_expiryRegex_2() const { return ___expiryRegex_2; }
	inline Regex_t1803876613 ** get_address_of_expiryRegex_2() { return &___expiryRegex_2; }
	inline void set_expiryRegex_2(Regex_t1803876613 * value)
	{
		___expiryRegex_2 = value;
		Il2CppCodeGenWriteBarrier((&___expiryRegex_2), value);
	}

	inline static int32_t get_offset_of_ruleRegex_3() { return static_cast<int32_t>(offsetof(Expiration_t12298299_StaticFields, ___ruleRegex_3)); }
	inline Regex_t1803876613 * get_ruleRegex_3() const { return ___ruleRegex_3; }
	inline Regex_t1803876613 ** get_address_of_ruleRegex_3() { return &___ruleRegex_3; }
	inline void set_ruleRegex_3(Regex_t1803876613 * value)
	{
		___ruleRegex_3 = value;
		Il2CppCodeGenWriteBarrier((&___ruleRegex_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPIRATION_T12298299_H
#ifndef TMP_ASSET_T1084708044_H
#define TMP_ASSET_T1084708044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Asset
struct  TMP_Asset_t1084708044  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 TMPro.TMP_Asset::hashCode
	int32_t ___hashCode_2;
	// UnityEngine.Material TMPro.TMP_Asset::material
	Material_t193706927 * ___material_3;
	// System.Int32 TMPro.TMP_Asset::materialHashCode
	int32_t ___materialHashCode_4;

public:
	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TMP_Asset_t1084708044, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(TMP_Asset_t1084708044, ___material_3)); }
	inline Material_t193706927 * get_material_3() const { return ___material_3; }
	inline Material_t193706927 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t193706927 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_materialHashCode_4() { return static_cast<int32_t>(offsetof(TMP_Asset_t1084708044, ___materialHashCode_4)); }
	inline int32_t get_materialHashCode_4() const { return ___materialHashCode_4; }
	inline int32_t* get_address_of_materialHashCode_4() { return &___materialHashCode_4; }
	inline void set_materialHashCode_4(int32_t value)
	{
		___materialHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_ASSET_T1084708044_H
#ifndef S3OBJECT_T1836716019_H
#define S3OBJECT_T1836716019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.S3Object
struct  S3Object_t1836716019  : public RuntimeObject
{
public:
	// System.String Amazon.S3.Model.S3Object::eTag
	String_t* ___eTag_0;
	// System.String Amazon.S3.Model.S3Object::key
	String_t* ___key_1;
	// System.Nullable`1<System.DateTime> Amazon.S3.Model.S3Object::lastModified
	Nullable_1_t3251239280  ___lastModified_2;
	// Amazon.S3.Model.Owner Amazon.S3.Model.S3Object::owner
	Owner_t97740679 * ___owner_3;
	// System.Nullable`1<System.Int64> Amazon.S3.Model.S3Object::size
	Nullable_1_t3467111648  ___size_4;
	// Amazon.S3.S3StorageClass Amazon.S3.Model.S3Object::storageClass
	S3StorageClass_t454477475 * ___storageClass_5;
	// System.String Amazon.S3.Model.S3Object::bucketName
	String_t* ___bucketName_6;

public:
	inline static int32_t get_offset_of_eTag_0() { return static_cast<int32_t>(offsetof(S3Object_t1836716019, ___eTag_0)); }
	inline String_t* get_eTag_0() const { return ___eTag_0; }
	inline String_t** get_address_of_eTag_0() { return &___eTag_0; }
	inline void set_eTag_0(String_t* value)
	{
		___eTag_0 = value;
		Il2CppCodeGenWriteBarrier((&___eTag_0), value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(S3Object_t1836716019, ___key_1)); }
	inline String_t* get_key_1() const { return ___key_1; }
	inline String_t** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(String_t* value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_lastModified_2() { return static_cast<int32_t>(offsetof(S3Object_t1836716019, ___lastModified_2)); }
	inline Nullable_1_t3251239280  get_lastModified_2() const { return ___lastModified_2; }
	inline Nullable_1_t3251239280 * get_address_of_lastModified_2() { return &___lastModified_2; }
	inline void set_lastModified_2(Nullable_1_t3251239280  value)
	{
		___lastModified_2 = value;
	}

	inline static int32_t get_offset_of_owner_3() { return static_cast<int32_t>(offsetof(S3Object_t1836716019, ___owner_3)); }
	inline Owner_t97740679 * get_owner_3() const { return ___owner_3; }
	inline Owner_t97740679 ** get_address_of_owner_3() { return &___owner_3; }
	inline void set_owner_3(Owner_t97740679 * value)
	{
		___owner_3 = value;
		Il2CppCodeGenWriteBarrier((&___owner_3), value);
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(S3Object_t1836716019, ___size_4)); }
	inline Nullable_1_t3467111648  get_size_4() const { return ___size_4; }
	inline Nullable_1_t3467111648 * get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(Nullable_1_t3467111648  value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_storageClass_5() { return static_cast<int32_t>(offsetof(S3Object_t1836716019, ___storageClass_5)); }
	inline S3StorageClass_t454477475 * get_storageClass_5() const { return ___storageClass_5; }
	inline S3StorageClass_t454477475 ** get_address_of_storageClass_5() { return &___storageClass_5; }
	inline void set_storageClass_5(S3StorageClass_t454477475 * value)
	{
		___storageClass_5 = value;
		Il2CppCodeGenWriteBarrier((&___storageClass_5), value);
	}

	inline static int32_t get_offset_of_bucketName_6() { return static_cast<int32_t>(offsetof(S3Object_t1836716019, ___bucketName_6)); }
	inline String_t* get_bucketName_6() const { return ___bucketName_6; }
	inline String_t** get_address_of_bucketName_6() { return &___bucketName_6; }
	inline void set_bucketName_6(String_t* value)
	{
		___bucketName_6 = value;
		Il2CppCodeGenWriteBarrier((&___bucketName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3OBJECT_T1836716019_H
#ifndef GETOBJECTREQUEST_T109865576_H
#define GETOBJECTREQUEST_T109865576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.GetObjectRequest
struct  GetObjectRequest_t109865576  : public AmazonWebServiceRequest_t3384026212
{
public:
	// System.String Amazon.S3.Model.GetObjectRequest::bucketName
	String_t* ___bucketName_4;
	// System.Nullable`1<System.DateTime> Amazon.S3.Model.GetObjectRequest::modifiedSinceDate
	Nullable_1_t3251239280  ___modifiedSinceDate_5;
	// System.Nullable`1<System.DateTime> Amazon.S3.Model.GetObjectRequest::unmodifiedSinceDate
	Nullable_1_t3251239280  ___unmodifiedSinceDate_6;
	// System.String Amazon.S3.Model.GetObjectRequest::etagToMatch
	String_t* ___etagToMatch_7;
	// System.String Amazon.S3.Model.GetObjectRequest::etagToNotMatch
	String_t* ___etagToNotMatch_8;
	// System.String Amazon.S3.Model.GetObjectRequest::key
	String_t* ___key_9;
	// Amazon.S3.Model.ByteRange Amazon.S3.Model.GetObjectRequest::byteRange
	ByteRange_t286214923 * ___byteRange_10;
	// System.Nullable`1<System.DateTime> Amazon.S3.Model.GetObjectRequest::responseExpires
	Nullable_1_t3251239280  ___responseExpires_11;
	// System.String Amazon.S3.Model.GetObjectRequest::versionId
	String_t* ___versionId_12;
	// Amazon.S3.Model.ResponseHeaderOverrides Amazon.S3.Model.GetObjectRequest::responseHeaders
	ResponseHeaderOverrides_t382636191 * ___responseHeaders_13;
	// Amazon.S3.ServerSideEncryptionCustomerMethod Amazon.S3.Model.GetObjectRequest::serverSideCustomerEncryption
	ServerSideEncryptionCustomerMethod_t3201425490 * ___serverSideCustomerEncryption_14;
	// System.String Amazon.S3.Model.GetObjectRequest::serverSideEncryptionCustomerProvidedKey
	String_t* ___serverSideEncryptionCustomerProvidedKey_15;
	// System.String Amazon.S3.Model.GetObjectRequest::serverSideEncryptionCustomerProvidedKeyMD5
	String_t* ___serverSideEncryptionCustomerProvidedKeyMD5_16;
	// Amazon.S3.RequestPayer Amazon.S3.Model.GetObjectRequest::requestPayer
	RequestPayer_t3334661492 * ___requestPayer_17;
	// System.Nullable`1<System.Int32> Amazon.S3.Model.GetObjectRequest::partNumber
	Nullable_1_t334943763  ___partNumber_18;

public:
	inline static int32_t get_offset_of_bucketName_4() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___bucketName_4)); }
	inline String_t* get_bucketName_4() const { return ___bucketName_4; }
	inline String_t** get_address_of_bucketName_4() { return &___bucketName_4; }
	inline void set_bucketName_4(String_t* value)
	{
		___bucketName_4 = value;
		Il2CppCodeGenWriteBarrier((&___bucketName_4), value);
	}

	inline static int32_t get_offset_of_modifiedSinceDate_5() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___modifiedSinceDate_5)); }
	inline Nullable_1_t3251239280  get_modifiedSinceDate_5() const { return ___modifiedSinceDate_5; }
	inline Nullable_1_t3251239280 * get_address_of_modifiedSinceDate_5() { return &___modifiedSinceDate_5; }
	inline void set_modifiedSinceDate_5(Nullable_1_t3251239280  value)
	{
		___modifiedSinceDate_5 = value;
	}

	inline static int32_t get_offset_of_unmodifiedSinceDate_6() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___unmodifiedSinceDate_6)); }
	inline Nullable_1_t3251239280  get_unmodifiedSinceDate_6() const { return ___unmodifiedSinceDate_6; }
	inline Nullable_1_t3251239280 * get_address_of_unmodifiedSinceDate_6() { return &___unmodifiedSinceDate_6; }
	inline void set_unmodifiedSinceDate_6(Nullable_1_t3251239280  value)
	{
		___unmodifiedSinceDate_6 = value;
	}

	inline static int32_t get_offset_of_etagToMatch_7() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___etagToMatch_7)); }
	inline String_t* get_etagToMatch_7() const { return ___etagToMatch_7; }
	inline String_t** get_address_of_etagToMatch_7() { return &___etagToMatch_7; }
	inline void set_etagToMatch_7(String_t* value)
	{
		___etagToMatch_7 = value;
		Il2CppCodeGenWriteBarrier((&___etagToMatch_7), value);
	}

	inline static int32_t get_offset_of_etagToNotMatch_8() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___etagToNotMatch_8)); }
	inline String_t* get_etagToNotMatch_8() const { return ___etagToNotMatch_8; }
	inline String_t** get_address_of_etagToNotMatch_8() { return &___etagToNotMatch_8; }
	inline void set_etagToNotMatch_8(String_t* value)
	{
		___etagToNotMatch_8 = value;
		Il2CppCodeGenWriteBarrier((&___etagToNotMatch_8), value);
	}

	inline static int32_t get_offset_of_key_9() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___key_9)); }
	inline String_t* get_key_9() const { return ___key_9; }
	inline String_t** get_address_of_key_9() { return &___key_9; }
	inline void set_key_9(String_t* value)
	{
		___key_9 = value;
		Il2CppCodeGenWriteBarrier((&___key_9), value);
	}

	inline static int32_t get_offset_of_byteRange_10() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___byteRange_10)); }
	inline ByteRange_t286214923 * get_byteRange_10() const { return ___byteRange_10; }
	inline ByteRange_t286214923 ** get_address_of_byteRange_10() { return &___byteRange_10; }
	inline void set_byteRange_10(ByteRange_t286214923 * value)
	{
		___byteRange_10 = value;
		Il2CppCodeGenWriteBarrier((&___byteRange_10), value);
	}

	inline static int32_t get_offset_of_responseExpires_11() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___responseExpires_11)); }
	inline Nullable_1_t3251239280  get_responseExpires_11() const { return ___responseExpires_11; }
	inline Nullable_1_t3251239280 * get_address_of_responseExpires_11() { return &___responseExpires_11; }
	inline void set_responseExpires_11(Nullable_1_t3251239280  value)
	{
		___responseExpires_11 = value;
	}

	inline static int32_t get_offset_of_versionId_12() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___versionId_12)); }
	inline String_t* get_versionId_12() const { return ___versionId_12; }
	inline String_t** get_address_of_versionId_12() { return &___versionId_12; }
	inline void set_versionId_12(String_t* value)
	{
		___versionId_12 = value;
		Il2CppCodeGenWriteBarrier((&___versionId_12), value);
	}

	inline static int32_t get_offset_of_responseHeaders_13() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___responseHeaders_13)); }
	inline ResponseHeaderOverrides_t382636191 * get_responseHeaders_13() const { return ___responseHeaders_13; }
	inline ResponseHeaderOverrides_t382636191 ** get_address_of_responseHeaders_13() { return &___responseHeaders_13; }
	inline void set_responseHeaders_13(ResponseHeaderOverrides_t382636191 * value)
	{
		___responseHeaders_13 = value;
		Il2CppCodeGenWriteBarrier((&___responseHeaders_13), value);
	}

	inline static int32_t get_offset_of_serverSideCustomerEncryption_14() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___serverSideCustomerEncryption_14)); }
	inline ServerSideEncryptionCustomerMethod_t3201425490 * get_serverSideCustomerEncryption_14() const { return ___serverSideCustomerEncryption_14; }
	inline ServerSideEncryptionCustomerMethod_t3201425490 ** get_address_of_serverSideCustomerEncryption_14() { return &___serverSideCustomerEncryption_14; }
	inline void set_serverSideCustomerEncryption_14(ServerSideEncryptionCustomerMethod_t3201425490 * value)
	{
		___serverSideCustomerEncryption_14 = value;
		Il2CppCodeGenWriteBarrier((&___serverSideCustomerEncryption_14), value);
	}

	inline static int32_t get_offset_of_serverSideEncryptionCustomerProvidedKey_15() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___serverSideEncryptionCustomerProvidedKey_15)); }
	inline String_t* get_serverSideEncryptionCustomerProvidedKey_15() const { return ___serverSideEncryptionCustomerProvidedKey_15; }
	inline String_t** get_address_of_serverSideEncryptionCustomerProvidedKey_15() { return &___serverSideEncryptionCustomerProvidedKey_15; }
	inline void set_serverSideEncryptionCustomerProvidedKey_15(String_t* value)
	{
		___serverSideEncryptionCustomerProvidedKey_15 = value;
		Il2CppCodeGenWriteBarrier((&___serverSideEncryptionCustomerProvidedKey_15), value);
	}

	inline static int32_t get_offset_of_serverSideEncryptionCustomerProvidedKeyMD5_16() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___serverSideEncryptionCustomerProvidedKeyMD5_16)); }
	inline String_t* get_serverSideEncryptionCustomerProvidedKeyMD5_16() const { return ___serverSideEncryptionCustomerProvidedKeyMD5_16; }
	inline String_t** get_address_of_serverSideEncryptionCustomerProvidedKeyMD5_16() { return &___serverSideEncryptionCustomerProvidedKeyMD5_16; }
	inline void set_serverSideEncryptionCustomerProvidedKeyMD5_16(String_t* value)
	{
		___serverSideEncryptionCustomerProvidedKeyMD5_16 = value;
		Il2CppCodeGenWriteBarrier((&___serverSideEncryptionCustomerProvidedKeyMD5_16), value);
	}

	inline static int32_t get_offset_of_requestPayer_17() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___requestPayer_17)); }
	inline RequestPayer_t3334661492 * get_requestPayer_17() const { return ___requestPayer_17; }
	inline RequestPayer_t3334661492 ** get_address_of_requestPayer_17() { return &___requestPayer_17; }
	inline void set_requestPayer_17(RequestPayer_t3334661492 * value)
	{
		___requestPayer_17 = value;
		Il2CppCodeGenWriteBarrier((&___requestPayer_17), value);
	}

	inline static int32_t get_offset_of_partNumber_18() { return static_cast<int32_t>(offsetof(GetObjectRequest_t109865576, ___partNumber_18)); }
	inline Nullable_1_t334943763  get_partNumber_18() const { return ___partNumber_18; }
	inline Nullable_1_t334943763 * get_address_of_partNumber_18() { return &___partNumber_18; }
	inline void set_partNumber_18(Nullable_1_t334943763  value)
	{
		___partNumber_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETOBJECTREQUEST_T109865576_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef GETOBJECTRESPONSE_T653598534_H
#define GETOBJECTRESPONSE_T653598534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.S3.Model.GetObjectResponse
struct  GetObjectResponse_t653598534  : public StreamResponse_t2490679653
{
public:
	// System.String Amazon.S3.Model.GetObjectResponse::deleteMarker
	String_t* ___deleteMarker_5;
	// System.String Amazon.S3.Model.GetObjectResponse::acceptRanges
	String_t* ___acceptRanges_6;
	// Amazon.S3.Model.Expiration Amazon.S3.Model.GetObjectResponse::expiration
	Expiration_t12298299 * ___expiration_7;
	// System.Nullable`1<System.DateTime> Amazon.S3.Model.GetObjectResponse::restoreExpiration
	Nullable_1_t3251239280  ___restoreExpiration_8;
	// System.Boolean Amazon.S3.Model.GetObjectResponse::restoreInProgress
	bool ___restoreInProgress_9;
	// System.Nullable`1<System.DateTime> Amazon.S3.Model.GetObjectResponse::lastModified
	Nullable_1_t3251239280  ___lastModified_10;
	// System.String Amazon.S3.Model.GetObjectResponse::eTag
	String_t* ___eTag_11;
	// System.Nullable`1<System.Int32> Amazon.S3.Model.GetObjectResponse::missingMeta
	Nullable_1_t334943763  ___missingMeta_12;
	// System.String Amazon.S3.Model.GetObjectResponse::versionId
	String_t* ___versionId_13;
	// System.Nullable`1<System.DateTime> Amazon.S3.Model.GetObjectResponse::expires
	Nullable_1_t3251239280  ___expires_14;
	// System.String Amazon.S3.Model.GetObjectResponse::websiteRedirectLocation
	String_t* ___websiteRedirectLocation_15;
	// Amazon.S3.ServerSideEncryptionMethod Amazon.S3.Model.GetObjectResponse::serverSideEncryption
	ServerSideEncryptionMethod_t608782770 * ___serverSideEncryption_16;
	// Amazon.S3.ServerSideEncryptionCustomerMethod Amazon.S3.Model.GetObjectResponse::serverSideEncryptionCustomerMethod
	ServerSideEncryptionCustomerMethod_t3201425490 * ___serverSideEncryptionCustomerMethod_17;
	// System.String Amazon.S3.Model.GetObjectResponse::serverSideEncryptionKeyManagementServiceKeyId
	String_t* ___serverSideEncryptionKeyManagementServiceKeyId_18;
	// Amazon.S3.Model.HeadersCollection Amazon.S3.Model.GetObjectResponse::headersCollection
	HeadersCollection_t2711555648 * ___headersCollection_19;
	// Amazon.S3.Model.MetadataCollection Amazon.S3.Model.GetObjectResponse::metadataCollection
	MetadataCollection_t3745113723 * ___metadataCollection_20;
	// Amazon.S3.ReplicationStatus Amazon.S3.Model.GetObjectResponse::replicationStatus
	ReplicationStatus_t1991002226 * ___replicationStatus_21;
	// System.Nullable`1<System.Int32> Amazon.S3.Model.GetObjectResponse::partsCount
	Nullable_1_t334943763  ___partsCount_22;
	// Amazon.S3.S3StorageClass Amazon.S3.Model.GetObjectResponse::storageClass
	S3StorageClass_t454477475 * ___storageClass_23;
	// Amazon.S3.RequestCharged Amazon.S3.Model.GetObjectResponse::requestCharged
	RequestCharged_t2438105727 * ___requestCharged_24;
	// System.Nullable`1<System.Int32> Amazon.S3.Model.GetObjectResponse::tagCount
	Nullable_1_t334943763  ___tagCount_25;
	// System.String Amazon.S3.Model.GetObjectResponse::bucketName
	String_t* ___bucketName_26;
	// System.String Amazon.S3.Model.GetObjectResponse::key
	String_t* ___key_27;
	// System.Boolean Amazon.S3.Model.GetObjectResponse::isExpiresUnmarshalled
	bool ___isExpiresUnmarshalled_28;
	// System.String Amazon.S3.Model.GetObjectResponse::<RawExpires>k__BackingField
	String_t* ___U3CRawExpiresU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_deleteMarker_5() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___deleteMarker_5)); }
	inline String_t* get_deleteMarker_5() const { return ___deleteMarker_5; }
	inline String_t** get_address_of_deleteMarker_5() { return &___deleteMarker_5; }
	inline void set_deleteMarker_5(String_t* value)
	{
		___deleteMarker_5 = value;
		Il2CppCodeGenWriteBarrier((&___deleteMarker_5), value);
	}

	inline static int32_t get_offset_of_acceptRanges_6() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___acceptRanges_6)); }
	inline String_t* get_acceptRanges_6() const { return ___acceptRanges_6; }
	inline String_t** get_address_of_acceptRanges_6() { return &___acceptRanges_6; }
	inline void set_acceptRanges_6(String_t* value)
	{
		___acceptRanges_6 = value;
		Il2CppCodeGenWriteBarrier((&___acceptRanges_6), value);
	}

	inline static int32_t get_offset_of_expiration_7() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___expiration_7)); }
	inline Expiration_t12298299 * get_expiration_7() const { return ___expiration_7; }
	inline Expiration_t12298299 ** get_address_of_expiration_7() { return &___expiration_7; }
	inline void set_expiration_7(Expiration_t12298299 * value)
	{
		___expiration_7 = value;
		Il2CppCodeGenWriteBarrier((&___expiration_7), value);
	}

	inline static int32_t get_offset_of_restoreExpiration_8() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___restoreExpiration_8)); }
	inline Nullable_1_t3251239280  get_restoreExpiration_8() const { return ___restoreExpiration_8; }
	inline Nullable_1_t3251239280 * get_address_of_restoreExpiration_8() { return &___restoreExpiration_8; }
	inline void set_restoreExpiration_8(Nullable_1_t3251239280  value)
	{
		___restoreExpiration_8 = value;
	}

	inline static int32_t get_offset_of_restoreInProgress_9() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___restoreInProgress_9)); }
	inline bool get_restoreInProgress_9() const { return ___restoreInProgress_9; }
	inline bool* get_address_of_restoreInProgress_9() { return &___restoreInProgress_9; }
	inline void set_restoreInProgress_9(bool value)
	{
		___restoreInProgress_9 = value;
	}

	inline static int32_t get_offset_of_lastModified_10() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___lastModified_10)); }
	inline Nullable_1_t3251239280  get_lastModified_10() const { return ___lastModified_10; }
	inline Nullable_1_t3251239280 * get_address_of_lastModified_10() { return &___lastModified_10; }
	inline void set_lastModified_10(Nullable_1_t3251239280  value)
	{
		___lastModified_10 = value;
	}

	inline static int32_t get_offset_of_eTag_11() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___eTag_11)); }
	inline String_t* get_eTag_11() const { return ___eTag_11; }
	inline String_t** get_address_of_eTag_11() { return &___eTag_11; }
	inline void set_eTag_11(String_t* value)
	{
		___eTag_11 = value;
		Il2CppCodeGenWriteBarrier((&___eTag_11), value);
	}

	inline static int32_t get_offset_of_missingMeta_12() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___missingMeta_12)); }
	inline Nullable_1_t334943763  get_missingMeta_12() const { return ___missingMeta_12; }
	inline Nullable_1_t334943763 * get_address_of_missingMeta_12() { return &___missingMeta_12; }
	inline void set_missingMeta_12(Nullable_1_t334943763  value)
	{
		___missingMeta_12 = value;
	}

	inline static int32_t get_offset_of_versionId_13() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___versionId_13)); }
	inline String_t* get_versionId_13() const { return ___versionId_13; }
	inline String_t** get_address_of_versionId_13() { return &___versionId_13; }
	inline void set_versionId_13(String_t* value)
	{
		___versionId_13 = value;
		Il2CppCodeGenWriteBarrier((&___versionId_13), value);
	}

	inline static int32_t get_offset_of_expires_14() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___expires_14)); }
	inline Nullable_1_t3251239280  get_expires_14() const { return ___expires_14; }
	inline Nullable_1_t3251239280 * get_address_of_expires_14() { return &___expires_14; }
	inline void set_expires_14(Nullable_1_t3251239280  value)
	{
		___expires_14 = value;
	}

	inline static int32_t get_offset_of_websiteRedirectLocation_15() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___websiteRedirectLocation_15)); }
	inline String_t* get_websiteRedirectLocation_15() const { return ___websiteRedirectLocation_15; }
	inline String_t** get_address_of_websiteRedirectLocation_15() { return &___websiteRedirectLocation_15; }
	inline void set_websiteRedirectLocation_15(String_t* value)
	{
		___websiteRedirectLocation_15 = value;
		Il2CppCodeGenWriteBarrier((&___websiteRedirectLocation_15), value);
	}

	inline static int32_t get_offset_of_serverSideEncryption_16() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___serverSideEncryption_16)); }
	inline ServerSideEncryptionMethod_t608782770 * get_serverSideEncryption_16() const { return ___serverSideEncryption_16; }
	inline ServerSideEncryptionMethod_t608782770 ** get_address_of_serverSideEncryption_16() { return &___serverSideEncryption_16; }
	inline void set_serverSideEncryption_16(ServerSideEncryptionMethod_t608782770 * value)
	{
		___serverSideEncryption_16 = value;
		Il2CppCodeGenWriteBarrier((&___serverSideEncryption_16), value);
	}

	inline static int32_t get_offset_of_serverSideEncryptionCustomerMethod_17() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___serverSideEncryptionCustomerMethod_17)); }
	inline ServerSideEncryptionCustomerMethod_t3201425490 * get_serverSideEncryptionCustomerMethod_17() const { return ___serverSideEncryptionCustomerMethod_17; }
	inline ServerSideEncryptionCustomerMethod_t3201425490 ** get_address_of_serverSideEncryptionCustomerMethod_17() { return &___serverSideEncryptionCustomerMethod_17; }
	inline void set_serverSideEncryptionCustomerMethod_17(ServerSideEncryptionCustomerMethod_t3201425490 * value)
	{
		___serverSideEncryptionCustomerMethod_17 = value;
		Il2CppCodeGenWriteBarrier((&___serverSideEncryptionCustomerMethod_17), value);
	}

	inline static int32_t get_offset_of_serverSideEncryptionKeyManagementServiceKeyId_18() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___serverSideEncryptionKeyManagementServiceKeyId_18)); }
	inline String_t* get_serverSideEncryptionKeyManagementServiceKeyId_18() const { return ___serverSideEncryptionKeyManagementServiceKeyId_18; }
	inline String_t** get_address_of_serverSideEncryptionKeyManagementServiceKeyId_18() { return &___serverSideEncryptionKeyManagementServiceKeyId_18; }
	inline void set_serverSideEncryptionKeyManagementServiceKeyId_18(String_t* value)
	{
		___serverSideEncryptionKeyManagementServiceKeyId_18 = value;
		Il2CppCodeGenWriteBarrier((&___serverSideEncryptionKeyManagementServiceKeyId_18), value);
	}

	inline static int32_t get_offset_of_headersCollection_19() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___headersCollection_19)); }
	inline HeadersCollection_t2711555648 * get_headersCollection_19() const { return ___headersCollection_19; }
	inline HeadersCollection_t2711555648 ** get_address_of_headersCollection_19() { return &___headersCollection_19; }
	inline void set_headersCollection_19(HeadersCollection_t2711555648 * value)
	{
		___headersCollection_19 = value;
		Il2CppCodeGenWriteBarrier((&___headersCollection_19), value);
	}

	inline static int32_t get_offset_of_metadataCollection_20() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___metadataCollection_20)); }
	inline MetadataCollection_t3745113723 * get_metadataCollection_20() const { return ___metadataCollection_20; }
	inline MetadataCollection_t3745113723 ** get_address_of_metadataCollection_20() { return &___metadataCollection_20; }
	inline void set_metadataCollection_20(MetadataCollection_t3745113723 * value)
	{
		___metadataCollection_20 = value;
		Il2CppCodeGenWriteBarrier((&___metadataCollection_20), value);
	}

	inline static int32_t get_offset_of_replicationStatus_21() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___replicationStatus_21)); }
	inline ReplicationStatus_t1991002226 * get_replicationStatus_21() const { return ___replicationStatus_21; }
	inline ReplicationStatus_t1991002226 ** get_address_of_replicationStatus_21() { return &___replicationStatus_21; }
	inline void set_replicationStatus_21(ReplicationStatus_t1991002226 * value)
	{
		___replicationStatus_21 = value;
		Il2CppCodeGenWriteBarrier((&___replicationStatus_21), value);
	}

	inline static int32_t get_offset_of_partsCount_22() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___partsCount_22)); }
	inline Nullable_1_t334943763  get_partsCount_22() const { return ___partsCount_22; }
	inline Nullable_1_t334943763 * get_address_of_partsCount_22() { return &___partsCount_22; }
	inline void set_partsCount_22(Nullable_1_t334943763  value)
	{
		___partsCount_22 = value;
	}

	inline static int32_t get_offset_of_storageClass_23() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___storageClass_23)); }
	inline S3StorageClass_t454477475 * get_storageClass_23() const { return ___storageClass_23; }
	inline S3StorageClass_t454477475 ** get_address_of_storageClass_23() { return &___storageClass_23; }
	inline void set_storageClass_23(S3StorageClass_t454477475 * value)
	{
		___storageClass_23 = value;
		Il2CppCodeGenWriteBarrier((&___storageClass_23), value);
	}

	inline static int32_t get_offset_of_requestCharged_24() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___requestCharged_24)); }
	inline RequestCharged_t2438105727 * get_requestCharged_24() const { return ___requestCharged_24; }
	inline RequestCharged_t2438105727 ** get_address_of_requestCharged_24() { return &___requestCharged_24; }
	inline void set_requestCharged_24(RequestCharged_t2438105727 * value)
	{
		___requestCharged_24 = value;
		Il2CppCodeGenWriteBarrier((&___requestCharged_24), value);
	}

	inline static int32_t get_offset_of_tagCount_25() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___tagCount_25)); }
	inline Nullable_1_t334943763  get_tagCount_25() const { return ___tagCount_25; }
	inline Nullable_1_t334943763 * get_address_of_tagCount_25() { return &___tagCount_25; }
	inline void set_tagCount_25(Nullable_1_t334943763  value)
	{
		___tagCount_25 = value;
	}

	inline static int32_t get_offset_of_bucketName_26() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___bucketName_26)); }
	inline String_t* get_bucketName_26() const { return ___bucketName_26; }
	inline String_t** get_address_of_bucketName_26() { return &___bucketName_26; }
	inline void set_bucketName_26(String_t* value)
	{
		___bucketName_26 = value;
		Il2CppCodeGenWriteBarrier((&___bucketName_26), value);
	}

	inline static int32_t get_offset_of_key_27() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___key_27)); }
	inline String_t* get_key_27() const { return ___key_27; }
	inline String_t** get_address_of_key_27() { return &___key_27; }
	inline void set_key_27(String_t* value)
	{
		___key_27 = value;
		Il2CppCodeGenWriteBarrier((&___key_27), value);
	}

	inline static int32_t get_offset_of_isExpiresUnmarshalled_28() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___isExpiresUnmarshalled_28)); }
	inline bool get_isExpiresUnmarshalled_28() const { return ___isExpiresUnmarshalled_28; }
	inline bool* get_address_of_isExpiresUnmarshalled_28() { return &___isExpiresUnmarshalled_28; }
	inline void set_isExpiresUnmarshalled_28(bool value)
	{
		___isExpiresUnmarshalled_28 = value;
	}

	inline static int32_t get_offset_of_U3CRawExpiresU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(GetObjectResponse_t653598534, ___U3CRawExpiresU3Ek__BackingField_29)); }
	inline String_t* get_U3CRawExpiresU3Ek__BackingField_29() const { return ___U3CRawExpiresU3Ek__BackingField_29; }
	inline String_t** get_address_of_U3CRawExpiresU3Ek__BackingField_29() { return &___U3CRawExpiresU3Ek__BackingField_29; }
	inline void set_U3CRawExpiresU3Ek__BackingField_29(String_t* value)
	{
		___U3CRawExpiresU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawExpiresU3Ek__BackingField_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETOBJECTRESPONSE_T653598534_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef TEXTCONTAINER_T4263764796_H
#define TEXTCONTAINER_T4263764796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextContainer
struct  TextContainer_t4263764796  : public UIBehaviour_t3960014691
{
public:
	// System.Boolean TMPro.TextContainer::m_hasChanged
	bool ___m_hasChanged_2;
	// UnityEngine.Vector2 TMPro.TextContainer::m_pivot
	Vector2_t2243707579  ___m_pivot_3;
	// TMPro.TextContainerAnchors TMPro.TextContainer::m_anchorPosition
	int32_t ___m_anchorPosition_4;
	// UnityEngine.Rect TMPro.TextContainer::m_rect
	Rect_t3681755626  ___m_rect_5;
	// System.Boolean TMPro.TextContainer::m_isDefaultWidth
	bool ___m_isDefaultWidth_6;
	// System.Boolean TMPro.TextContainer::m_isDefaultHeight
	bool ___m_isDefaultHeight_7;
	// System.Boolean TMPro.TextContainer::m_isAutoFitting
	bool ___m_isAutoFitting_8;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_corners
	Vector3U5BU5D_t1172311765* ___m_corners_9;
	// UnityEngine.Vector3[] TMPro.TextContainer::m_worldCorners
	Vector3U5BU5D_t1172311765* ___m_worldCorners_10;
	// UnityEngine.Vector4 TMPro.TextContainer::m_margins
	Vector4_t2243707581  ___m_margins_11;
	// UnityEngine.RectTransform TMPro.TextContainer::m_rectTransform
	RectTransform_t3349966182 * ___m_rectTransform_12;
	// TMPro.TextMeshPro TMPro.TextContainer::m_textMeshPro
	TextMeshPro_t2521834357 * ___m_textMeshPro_14;

public:
	inline static int32_t get_offset_of_m_hasChanged_2() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_hasChanged_2)); }
	inline bool get_m_hasChanged_2() const { return ___m_hasChanged_2; }
	inline bool* get_address_of_m_hasChanged_2() { return &___m_hasChanged_2; }
	inline void set_m_hasChanged_2(bool value)
	{
		___m_hasChanged_2 = value;
	}

	inline static int32_t get_offset_of_m_pivot_3() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_pivot_3)); }
	inline Vector2_t2243707579  get_m_pivot_3() const { return ___m_pivot_3; }
	inline Vector2_t2243707579 * get_address_of_m_pivot_3() { return &___m_pivot_3; }
	inline void set_m_pivot_3(Vector2_t2243707579  value)
	{
		___m_pivot_3 = value;
	}

	inline static int32_t get_offset_of_m_anchorPosition_4() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_anchorPosition_4)); }
	inline int32_t get_m_anchorPosition_4() const { return ___m_anchorPosition_4; }
	inline int32_t* get_address_of_m_anchorPosition_4() { return &___m_anchorPosition_4; }
	inline void set_m_anchorPosition_4(int32_t value)
	{
		___m_anchorPosition_4 = value;
	}

	inline static int32_t get_offset_of_m_rect_5() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_rect_5)); }
	inline Rect_t3681755626  get_m_rect_5() const { return ___m_rect_5; }
	inline Rect_t3681755626 * get_address_of_m_rect_5() { return &___m_rect_5; }
	inline void set_m_rect_5(Rect_t3681755626  value)
	{
		___m_rect_5 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultWidth_6() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_isDefaultWidth_6)); }
	inline bool get_m_isDefaultWidth_6() const { return ___m_isDefaultWidth_6; }
	inline bool* get_address_of_m_isDefaultWidth_6() { return &___m_isDefaultWidth_6; }
	inline void set_m_isDefaultWidth_6(bool value)
	{
		___m_isDefaultWidth_6 = value;
	}

	inline static int32_t get_offset_of_m_isDefaultHeight_7() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_isDefaultHeight_7)); }
	inline bool get_m_isDefaultHeight_7() const { return ___m_isDefaultHeight_7; }
	inline bool* get_address_of_m_isDefaultHeight_7() { return &___m_isDefaultHeight_7; }
	inline void set_m_isDefaultHeight_7(bool value)
	{
		___m_isDefaultHeight_7 = value;
	}

	inline static int32_t get_offset_of_m_isAutoFitting_8() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_isAutoFitting_8)); }
	inline bool get_m_isAutoFitting_8() const { return ___m_isAutoFitting_8; }
	inline bool* get_address_of_m_isAutoFitting_8() { return &___m_isAutoFitting_8; }
	inline void set_m_isAutoFitting_8(bool value)
	{
		___m_isAutoFitting_8 = value;
	}

	inline static int32_t get_offset_of_m_corners_9() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_corners_9)); }
	inline Vector3U5BU5D_t1172311765* get_m_corners_9() const { return ___m_corners_9; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_corners_9() { return &___m_corners_9; }
	inline void set_m_corners_9(Vector3U5BU5D_t1172311765* value)
	{
		___m_corners_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_corners_9), value);
	}

	inline static int32_t get_offset_of_m_worldCorners_10() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_worldCorners_10)); }
	inline Vector3U5BU5D_t1172311765* get_m_worldCorners_10() const { return ___m_worldCorners_10; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_worldCorners_10() { return &___m_worldCorners_10; }
	inline void set_m_worldCorners_10(Vector3U5BU5D_t1172311765* value)
	{
		___m_worldCorners_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_worldCorners_10), value);
	}

	inline static int32_t get_offset_of_m_margins_11() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_margins_11)); }
	inline Vector4_t2243707581  get_m_margins_11() const { return ___m_margins_11; }
	inline Vector4_t2243707581 * get_address_of_m_margins_11() { return &___m_margins_11; }
	inline void set_m_margins_11(Vector4_t2243707581  value)
	{
		___m_margins_11 = value;
	}

	inline static int32_t get_offset_of_m_rectTransform_12() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_rectTransform_12)); }
	inline RectTransform_t3349966182 * get_m_rectTransform_12() const { return ___m_rectTransform_12; }
	inline RectTransform_t3349966182 ** get_address_of_m_rectTransform_12() { return &___m_rectTransform_12; }
	inline void set_m_rectTransform_12(RectTransform_t3349966182 * value)
	{
		___m_rectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_12), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_14() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796, ___m_textMeshPro_14)); }
	inline TextMeshPro_t2521834357 * get_m_textMeshPro_14() const { return ___m_textMeshPro_14; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_textMeshPro_14() { return &___m_textMeshPro_14; }
	inline void set_m_textMeshPro_14(TextMeshPro_t2521834357 * value)
	{
		___m_textMeshPro_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_14), value);
	}
};

struct TextContainer_t4263764796_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TextContainer::k_defaultSize
	Vector2_t2243707579  ___k_defaultSize_13;

public:
	inline static int32_t get_offset_of_k_defaultSize_13() { return static_cast<int32_t>(offsetof(TextContainer_t4263764796_StaticFields, ___k_defaultSize_13)); }
	inline Vector2_t2243707579  get_k_defaultSize_13() const { return ___k_defaultSize_13; }
	inline Vector2_t2243707579 * get_address_of_k_defaultSize_13() { return &___k_defaultSize_13; }
	inline void set_k_defaultSize_13(Vector2_t2243707579  value)
	{
		___k_defaultSize_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCONTAINER_T4263764796_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef TMP_TEXT_T1920000777_H
#define TMP_TEXT_T1920000777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t1920000777  : public MaskableGraphic_t540192618
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_28;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_29;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t2530419979 * ___m_fontAsset_30;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t2530419979 * ___m_currentFontAsset_31;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_32;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t193706927 * ___m_sharedMaterial_33;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t193706927 * ___m_currentMaterial_34;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t627890505* ___m_materialReferences_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t1079703083 * ___m_materialReferenceIndexLookup_36;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_XmlTagStack_1_t3512906015  ___m_materialReferenceStack_37;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_38;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t3123989686* ___m_fontSharedMaterials_39;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t193706927 * ___m_fontMaterial_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t3123989686* ___m_fontMaterials_41;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_42;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t874517518  ___m_fontColor32_43;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t2020392075  ___m_fontColor_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t874517518  ___m_underlineColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t874517518  ___m_strikethroughColor_47;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t874517518  ___m_highlightColor_48;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_49;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t1602386880  ___m_fontColorGradient_50;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t1159837347 * ___m_fontColorGradientPreset_51;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_spriteAsset_52;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_53;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_54;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t874517518  ___m_spriteColor_55;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t874517518  ___m_faceColor_57;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t874517518  ___m_outlineColor_58;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_59;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_60;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_61;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_62;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t2735062451  ___m_sizeStack_63;
	// System.Int32 TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_64;
	// System.Int32 TMPro.TMP_Text::m_fontWeightInternal
	int32_t ___m_fontWeightInternal_65;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_fontWeightStack
	TMP_XmlTagStack_1_t2730429967  ___m_fontWeightStack_66;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_67;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_68;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_69;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_70;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_71;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_72;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_73;
	// TMPro.TMP_BasicXmlTagStack TMPro.TMP_Text::m_fontStyleStack
	TMP_BasicXmlTagStack_t937156555  ___m_fontStyleStack_74;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_75;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_76;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_77;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_XmlTagStack_1_t2125340843  ___m_lineJustificationStack_78;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t1172311765* ___m_textContainerLocalCorners_79;
	// System.Boolean TMPro.TMP_Text::m_isAlignmentEnumConverted
	bool ___m_isAlignmentEnumConverted_80;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_81;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_82;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_83;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_84;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_85;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_86;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_87;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_88;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_89;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_90;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_91;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_92;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_93;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_94;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_95;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_96;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_97;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_98;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t1920000777 * ___m_linkedTextComponent_99;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_100;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_101;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_102;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_103;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_104;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_105;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_106;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_107;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_108;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_109;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_110;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_111;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_112;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_113;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_114;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_115;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_116;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_117;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_118;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_119;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_120;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_121;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_122;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_123;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t2243707581  ___m_margin_124;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_125;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_126;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_127;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_128;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_129;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t2849466151 * ___m_textInfo_130;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_131;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_132;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_t3275118058 * ___m_transform_133;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t3349966182 * ___m_rectTransform_134;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_135;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_136;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t1356156583 * ___m_mesh_137;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_138;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2347923044 * ___m_spriteAnimator_139;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_140;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_141;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_142;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_143;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_144;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_145;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_t2808691390 * ___m_LayoutElement_146;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_147;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_148;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_149;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_150;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_151;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_152;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_153;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_154;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_155;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_156;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_157;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_158;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_159;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_160;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_161;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_162;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_163;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_164;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_165;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t1328083999* ___m_htmlTag_166;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_t573465953* ___m_xmlAttribute_167;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t577127397* ___m_attributeParameterValues_168;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_169;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_170;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t2735062451  ___m_indentStack_171;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_172;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_173;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t2933234003  ___m_FXMatrix_174;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_175;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t3030399641* ___m_char_buffer_176;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t602810366* ___m_internalCharacterInfo_177;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t1328083999* ___m_input_CharArray_178;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_179;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_180;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t433984875  ___m_SavedWordWrapState_181;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t433984875  ___m_SavedLineState_182;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_183;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_184;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_185;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_186;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_187;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_188;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_189;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_190;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_191;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_192;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_193;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_194;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_195;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_196;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_197;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_t3018556803  ___m_meshExtents_198;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t874517518  ___m_htmlColor_199;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t1533070037  ___m_colorStack_200;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_XmlTagStack_1_t1533070037  ___m_underlineColorStack_201;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_XmlTagStack_1_t1533070037  ___m_strikethroughColorStack_202;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_XmlTagStack_1_t1533070037  ___m_highlightColorStack_203;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_t1159837347 * ___m_colorGradientPreset_204;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_XmlTagStack_1_t1818389866  ___m_colorGradientStack_205;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_206;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_207;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2730429967  ___m_styleStack_208;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_XmlTagStack_1_t2730429967  ___m_actionStack_209;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_210;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_211;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_XmlTagStack_1_t2735062451  ___m_baselineOffsetStack_212;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_213;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_214;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t2285620223 * ___m_cached_TextElement_215;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_t909793902 * ___m_cached_Underline_GlyphInfo_216;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Ellipsis_GlyphInfo
	TMP_Glyph_t909793902 * ___m_cached_Ellipsis_GlyphInfo_217;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_defaultSpriteAsset_218;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_currentSpriteAsset_219;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_220;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_221;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_222;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_223;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_t577127397* ___k_Power_224;

public:
	inline static int32_t get_offset_of_m_text_28() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_text_28)); }
	inline String_t* get_m_text_28() const { return ___m_text_28; }
	inline String_t** get_address_of_m_text_28() { return &___m_text_28; }
	inline void set_m_text_28(String_t* value)
	{
		___m_text_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_28), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_29() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isRightToLeft_29)); }
	inline bool get_m_isRightToLeft_29() const { return ___m_isRightToLeft_29; }
	inline bool* get_address_of_m_isRightToLeft_29() { return &___m_isRightToLeft_29; }
	inline void set_m_isRightToLeft_29(bool value)
	{
		___m_isRightToLeft_29 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_30() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontAsset_30)); }
	inline TMP_FontAsset_t2530419979 * get_m_fontAsset_30() const { return ___m_fontAsset_30; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_m_fontAsset_30() { return &___m_fontAsset_30; }
	inline void set_m_fontAsset_30(TMP_FontAsset_t2530419979 * value)
	{
		___m_fontAsset_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_30), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_31() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentFontAsset_31)); }
	inline TMP_FontAsset_t2530419979 * get_m_currentFontAsset_31() const { return ___m_currentFontAsset_31; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_m_currentFontAsset_31() { return &___m_currentFontAsset_31; }
	inline void set_m_currentFontAsset_31(TMP_FontAsset_t2530419979 * value)
	{
		___m_currentFontAsset_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_31), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_32() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isSDFShader_32)); }
	inline bool get_m_isSDFShader_32() const { return ___m_isSDFShader_32; }
	inline bool* get_address_of_m_isSDFShader_32() { return &___m_isSDFShader_32; }
	inline void set_m_isSDFShader_32(bool value)
	{
		___m_isSDFShader_32 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_33() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_sharedMaterial_33)); }
	inline Material_t193706927 * get_m_sharedMaterial_33() const { return ___m_sharedMaterial_33; }
	inline Material_t193706927 ** get_address_of_m_sharedMaterial_33() { return &___m_sharedMaterial_33; }
	inline void set_m_sharedMaterial_33(Material_t193706927 * value)
	{
		___m_sharedMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_34() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentMaterial_34)); }
	inline Material_t193706927 * get_m_currentMaterial_34() const { return ___m_currentMaterial_34; }
	inline Material_t193706927 ** get_address_of_m_currentMaterial_34() { return &___m_currentMaterial_34; }
	inline void set_m_currentMaterial_34(Material_t193706927 * value)
	{
		___m_currentMaterial_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_34), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_35() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_materialReferences_35)); }
	inline MaterialReferenceU5BU5D_t627890505* get_m_materialReferences_35() const { return ___m_materialReferences_35; }
	inline MaterialReferenceU5BU5D_t627890505** get_address_of_m_materialReferences_35() { return &___m_materialReferences_35; }
	inline void set_m_materialReferences_35(MaterialReferenceU5BU5D_t627890505* value)
	{
		___m_materialReferences_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_35), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_36() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_materialReferenceIndexLookup_36)); }
	inline Dictionary_2_t1079703083 * get_m_materialReferenceIndexLookup_36() const { return ___m_materialReferenceIndexLookup_36; }
	inline Dictionary_2_t1079703083 ** get_address_of_m_materialReferenceIndexLookup_36() { return &___m_materialReferenceIndexLookup_36; }
	inline void set_m_materialReferenceIndexLookup_36(Dictionary_2_t1079703083 * value)
	{
		___m_materialReferenceIndexLookup_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_37() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_materialReferenceStack_37)); }
	inline TMP_XmlTagStack_1_t3512906015  get_m_materialReferenceStack_37() const { return ___m_materialReferenceStack_37; }
	inline TMP_XmlTagStack_1_t3512906015 * get_address_of_m_materialReferenceStack_37() { return &___m_materialReferenceStack_37; }
	inline void set_m_materialReferenceStack_37(TMP_XmlTagStack_1_t3512906015  value)
	{
		___m_materialReferenceStack_37 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_38() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentMaterialIndex_38)); }
	inline int32_t get_m_currentMaterialIndex_38() const { return ___m_currentMaterialIndex_38; }
	inline int32_t* get_address_of_m_currentMaterialIndex_38() { return &___m_currentMaterialIndex_38; }
	inline void set_m_currentMaterialIndex_38(int32_t value)
	{
		___m_currentMaterialIndex_38 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_39() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSharedMaterials_39)); }
	inline MaterialU5BU5D_t3123989686* get_m_fontSharedMaterials_39() const { return ___m_fontSharedMaterials_39; }
	inline MaterialU5BU5D_t3123989686** get_address_of_m_fontSharedMaterials_39() { return &___m_fontSharedMaterials_39; }
	inline void set_m_fontSharedMaterials_39(MaterialU5BU5D_t3123989686* value)
	{
		___m_fontSharedMaterials_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_39), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_40() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontMaterial_40)); }
	inline Material_t193706927 * get_m_fontMaterial_40() const { return ___m_fontMaterial_40; }
	inline Material_t193706927 ** get_address_of_m_fontMaterial_40() { return &___m_fontMaterial_40; }
	inline void set_m_fontMaterial_40(Material_t193706927 * value)
	{
		___m_fontMaterial_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_40), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontMaterials_41)); }
	inline MaterialU5BU5D_t3123989686* get_m_fontMaterials_41() const { return ___m_fontMaterials_41; }
	inline MaterialU5BU5D_t3123989686** get_address_of_m_fontMaterials_41() { return &___m_fontMaterials_41; }
	inline void set_m_fontMaterials_41(MaterialU5BU5D_t3123989686* value)
	{
		___m_fontMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_42() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isMaterialDirty_42)); }
	inline bool get_m_isMaterialDirty_42() const { return ___m_isMaterialDirty_42; }
	inline bool* get_address_of_m_isMaterialDirty_42() { return &___m_isMaterialDirty_42; }
	inline void set_m_isMaterialDirty_42(bool value)
	{
		___m_isMaterialDirty_42 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_43() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontColor32_43)); }
	inline Color32_t874517518  get_m_fontColor32_43() const { return ___m_fontColor32_43; }
	inline Color32_t874517518 * get_address_of_m_fontColor32_43() { return &___m_fontColor32_43; }
	inline void set_m_fontColor32_43(Color32_t874517518  value)
	{
		___m_fontColor32_43 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_44() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontColor_44)); }
	inline Color_t2020392075  get_m_fontColor_44() const { return ___m_fontColor_44; }
	inline Color_t2020392075 * get_address_of_m_fontColor_44() { return &___m_fontColor_44; }
	inline void set_m_fontColor_44(Color_t2020392075  value)
	{
		___m_fontColor_44 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_underlineColor_46)); }
	inline Color32_t874517518  get_m_underlineColor_46() const { return ___m_underlineColor_46; }
	inline Color32_t874517518 * get_address_of_m_underlineColor_46() { return &___m_underlineColor_46; }
	inline void set_m_underlineColor_46(Color32_t874517518  value)
	{
		___m_underlineColor_46 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_47() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_strikethroughColor_47)); }
	inline Color32_t874517518  get_m_strikethroughColor_47() const { return ___m_strikethroughColor_47; }
	inline Color32_t874517518 * get_address_of_m_strikethroughColor_47() { return &___m_strikethroughColor_47; }
	inline void set_m_strikethroughColor_47(Color32_t874517518  value)
	{
		___m_strikethroughColor_47 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_highlightColor_48)); }
	inline Color32_t874517518  get_m_highlightColor_48() const { return ___m_highlightColor_48; }
	inline Color32_t874517518 * get_address_of_m_highlightColor_48() { return &___m_highlightColor_48; }
	inline void set_m_highlightColor_48(Color32_t874517518  value)
	{
		___m_highlightColor_48 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_49() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableVertexGradient_49)); }
	inline bool get_m_enableVertexGradient_49() const { return ___m_enableVertexGradient_49; }
	inline bool* get_address_of_m_enableVertexGradient_49() { return &___m_enableVertexGradient_49; }
	inline void set_m_enableVertexGradient_49(bool value)
	{
		___m_enableVertexGradient_49 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_50() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontColorGradient_50)); }
	inline VertexGradient_t1602386880  get_m_fontColorGradient_50() const { return ___m_fontColorGradient_50; }
	inline VertexGradient_t1602386880 * get_address_of_m_fontColorGradient_50() { return &___m_fontColorGradient_50; }
	inline void set_m_fontColorGradient_50(VertexGradient_t1602386880  value)
	{
		___m_fontColorGradient_50 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_51() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontColorGradientPreset_51)); }
	inline TMP_ColorGradient_t1159837347 * get_m_fontColorGradientPreset_51() const { return ___m_fontColorGradientPreset_51; }
	inline TMP_ColorGradient_t1159837347 ** get_address_of_m_fontColorGradientPreset_51() { return &___m_fontColorGradientPreset_51; }
	inline void set_m_fontColorGradientPreset_51(TMP_ColorGradient_t1159837347 * value)
	{
		___m_fontColorGradientPreset_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_51), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_52() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteAsset_52)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_spriteAsset_52() const { return ___m_spriteAsset_52; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_spriteAsset_52() { return &___m_spriteAsset_52; }
	inline void set_m_spriteAsset_52(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_spriteAsset_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_52), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_53() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_tintAllSprites_53)); }
	inline bool get_m_tintAllSprites_53() const { return ___m_tintAllSprites_53; }
	inline bool* get_address_of_m_tintAllSprites_53() { return &___m_tintAllSprites_53; }
	inline void set_m_tintAllSprites_53(bool value)
	{
		___m_tintAllSprites_53 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_54() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_tintSprite_54)); }
	inline bool get_m_tintSprite_54() const { return ___m_tintSprite_54; }
	inline bool* get_address_of_m_tintSprite_54() { return &___m_tintSprite_54; }
	inline void set_m_tintSprite_54(bool value)
	{
		___m_tintSprite_54 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_55() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteColor_55)); }
	inline Color32_t874517518  get_m_spriteColor_55() const { return ___m_spriteColor_55; }
	inline Color32_t874517518 * get_address_of_m_spriteColor_55() { return &___m_spriteColor_55; }
	inline void set_m_spriteColor_55(Color32_t874517518  value)
	{
		___m_spriteColor_55 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_56() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_overrideHtmlColors_56)); }
	inline bool get_m_overrideHtmlColors_56() const { return ___m_overrideHtmlColors_56; }
	inline bool* get_address_of_m_overrideHtmlColors_56() { return &___m_overrideHtmlColors_56; }
	inline void set_m_overrideHtmlColors_56(bool value)
	{
		___m_overrideHtmlColors_56 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_57() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_faceColor_57)); }
	inline Color32_t874517518  get_m_faceColor_57() const { return ___m_faceColor_57; }
	inline Color32_t874517518 * get_address_of_m_faceColor_57() { return &___m_faceColor_57; }
	inline void set_m_faceColor_57(Color32_t874517518  value)
	{
		___m_faceColor_57 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_58() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_outlineColor_58)); }
	inline Color32_t874517518  get_m_outlineColor_58() const { return ___m_outlineColor_58; }
	inline Color32_t874517518 * get_address_of_m_outlineColor_58() { return &___m_outlineColor_58; }
	inline void set_m_outlineColor_58(Color32_t874517518  value)
	{
		___m_outlineColor_58 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_59() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_outlineWidth_59)); }
	inline float get_m_outlineWidth_59() const { return ___m_outlineWidth_59; }
	inline float* get_address_of_m_outlineWidth_59() { return &___m_outlineWidth_59; }
	inline void set_m_outlineWidth_59(float value)
	{
		___m_outlineWidth_59 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_60() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSize_60)); }
	inline float get_m_fontSize_60() const { return ___m_fontSize_60; }
	inline float* get_address_of_m_fontSize_60() { return &___m_fontSize_60; }
	inline void set_m_fontSize_60(float value)
	{
		___m_fontSize_60 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_61() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentFontSize_61)); }
	inline float get_m_currentFontSize_61() const { return ___m_currentFontSize_61; }
	inline float* get_address_of_m_currentFontSize_61() { return &___m_currentFontSize_61; }
	inline void set_m_currentFontSize_61(float value)
	{
		___m_currentFontSize_61 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_62() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSizeBase_62)); }
	inline float get_m_fontSizeBase_62() const { return ___m_fontSizeBase_62; }
	inline float* get_address_of_m_fontSizeBase_62() { return &___m_fontSizeBase_62; }
	inline void set_m_fontSizeBase_62(float value)
	{
		___m_fontSizeBase_62 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_63() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_sizeStack_63)); }
	inline TMP_XmlTagStack_1_t2735062451  get_m_sizeStack_63() const { return ___m_sizeStack_63; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_m_sizeStack_63() { return &___m_sizeStack_63; }
	inline void set_m_sizeStack_63(TMP_XmlTagStack_1_t2735062451  value)
	{
		___m_sizeStack_63 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_64() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontWeight_64)); }
	inline int32_t get_m_fontWeight_64() const { return ___m_fontWeight_64; }
	inline int32_t* get_address_of_m_fontWeight_64() { return &___m_fontWeight_64; }
	inline void set_m_fontWeight_64(int32_t value)
	{
		___m_fontWeight_64 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightInternal_65() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontWeightInternal_65)); }
	inline int32_t get_m_fontWeightInternal_65() const { return ___m_fontWeightInternal_65; }
	inline int32_t* get_address_of_m_fontWeightInternal_65() { return &___m_fontWeightInternal_65; }
	inline void set_m_fontWeightInternal_65(int32_t value)
	{
		___m_fontWeightInternal_65 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightStack_66() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontWeightStack_66)); }
	inline TMP_XmlTagStack_1_t2730429967  get_m_fontWeightStack_66() const { return ___m_fontWeightStack_66; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_m_fontWeightStack_66() { return &___m_fontWeightStack_66; }
	inline void set_m_fontWeightStack_66(TMP_XmlTagStack_1_t2730429967  value)
	{
		___m_fontWeightStack_66 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_67() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableAutoSizing_67)); }
	inline bool get_m_enableAutoSizing_67() const { return ___m_enableAutoSizing_67; }
	inline bool* get_address_of_m_enableAutoSizing_67() { return &___m_enableAutoSizing_67; }
	inline void set_m_enableAutoSizing_67(bool value)
	{
		___m_enableAutoSizing_67 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_68() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxFontSize_68)); }
	inline float get_m_maxFontSize_68() const { return ___m_maxFontSize_68; }
	inline float* get_address_of_m_maxFontSize_68() { return &___m_maxFontSize_68; }
	inline void set_m_maxFontSize_68(float value)
	{
		___m_maxFontSize_68 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_69() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_minFontSize_69)); }
	inline float get_m_minFontSize_69() const { return ___m_minFontSize_69; }
	inline float* get_address_of_m_minFontSize_69() { return &___m_minFontSize_69; }
	inline void set_m_minFontSize_69(float value)
	{
		___m_minFontSize_69 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_70() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSizeMin_70)); }
	inline float get_m_fontSizeMin_70() const { return ___m_fontSizeMin_70; }
	inline float* get_address_of_m_fontSizeMin_70() { return &___m_fontSizeMin_70; }
	inline void set_m_fontSizeMin_70(float value)
	{
		___m_fontSizeMin_70 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_71() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSizeMax_71)); }
	inline float get_m_fontSizeMax_71() const { return ___m_fontSizeMax_71; }
	inline float* get_address_of_m_fontSizeMax_71() { return &___m_fontSizeMax_71; }
	inline void set_m_fontSizeMax_71(float value)
	{
		___m_fontSizeMax_71 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_72() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontStyle_72)); }
	inline int32_t get_m_fontStyle_72() const { return ___m_fontStyle_72; }
	inline int32_t* get_address_of_m_fontStyle_72() { return &___m_fontStyle_72; }
	inline void set_m_fontStyle_72(int32_t value)
	{
		___m_fontStyle_72 = value;
	}

	inline static int32_t get_offset_of_m_style_73() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_style_73)); }
	inline int32_t get_m_style_73() const { return ___m_style_73; }
	inline int32_t* get_address_of_m_style_73() { return &___m_style_73; }
	inline void set_m_style_73(int32_t value)
	{
		___m_style_73 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_74() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontStyleStack_74)); }
	inline TMP_BasicXmlTagStack_t937156555  get_m_fontStyleStack_74() const { return ___m_fontStyleStack_74; }
	inline TMP_BasicXmlTagStack_t937156555 * get_address_of_m_fontStyleStack_74() { return &___m_fontStyleStack_74; }
	inline void set_m_fontStyleStack_74(TMP_BasicXmlTagStack_t937156555  value)
	{
		___m_fontStyleStack_74 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_75() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isUsingBold_75)); }
	inline bool get_m_isUsingBold_75() const { return ___m_isUsingBold_75; }
	inline bool* get_address_of_m_isUsingBold_75() { return &___m_isUsingBold_75; }
	inline void set_m_isUsingBold_75(bool value)
	{
		___m_isUsingBold_75 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_76() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_textAlignment_76)); }
	inline int32_t get_m_textAlignment_76() const { return ___m_textAlignment_76; }
	inline int32_t* get_address_of_m_textAlignment_76() { return &___m_textAlignment_76; }
	inline void set_m_textAlignment_76(int32_t value)
	{
		___m_textAlignment_76 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_77() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineJustification_77)); }
	inline int32_t get_m_lineJustification_77() const { return ___m_lineJustification_77; }
	inline int32_t* get_address_of_m_lineJustification_77() { return &___m_lineJustification_77; }
	inline void set_m_lineJustification_77(int32_t value)
	{
		___m_lineJustification_77 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_78() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineJustificationStack_78)); }
	inline TMP_XmlTagStack_1_t2125340843  get_m_lineJustificationStack_78() const { return ___m_lineJustificationStack_78; }
	inline TMP_XmlTagStack_1_t2125340843 * get_address_of_m_lineJustificationStack_78() { return &___m_lineJustificationStack_78; }
	inline void set_m_lineJustificationStack_78(TMP_XmlTagStack_1_t2125340843  value)
	{
		___m_lineJustificationStack_78 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_79() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_textContainerLocalCorners_79)); }
	inline Vector3U5BU5D_t1172311765* get_m_textContainerLocalCorners_79() const { return ___m_textContainerLocalCorners_79; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_textContainerLocalCorners_79() { return &___m_textContainerLocalCorners_79; }
	inline void set_m_textContainerLocalCorners_79(Vector3U5BU5D_t1172311765* value)
	{
		___m_textContainerLocalCorners_79 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_79), value);
	}

	inline static int32_t get_offset_of_m_isAlignmentEnumConverted_80() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isAlignmentEnumConverted_80)); }
	inline bool get_m_isAlignmentEnumConverted_80() const { return ___m_isAlignmentEnumConverted_80; }
	inline bool* get_address_of_m_isAlignmentEnumConverted_80() { return &___m_isAlignmentEnumConverted_80; }
	inline void set_m_isAlignmentEnumConverted_80(bool value)
	{
		___m_isAlignmentEnumConverted_80 = value;
	}

	inline static int32_t get_offset_of_m_characterSpacing_81() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_characterSpacing_81)); }
	inline float get_m_characterSpacing_81() const { return ___m_characterSpacing_81; }
	inline float* get_address_of_m_characterSpacing_81() { return &___m_characterSpacing_81; }
	inline void set_m_characterSpacing_81(float value)
	{
		___m_characterSpacing_81 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_82() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_cSpacing_82)); }
	inline float get_m_cSpacing_82() const { return ___m_cSpacing_82; }
	inline float* get_address_of_m_cSpacing_82() { return &___m_cSpacing_82; }
	inline void set_m_cSpacing_82(float value)
	{
		___m_cSpacing_82 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_83() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_monoSpacing_83)); }
	inline float get_m_monoSpacing_83() const { return ___m_monoSpacing_83; }
	inline float* get_address_of_m_monoSpacing_83() { return &___m_monoSpacing_83; }
	inline void set_m_monoSpacing_83(float value)
	{
		___m_monoSpacing_83 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_wordSpacing_84)); }
	inline float get_m_wordSpacing_84() const { return ___m_wordSpacing_84; }
	inline float* get_address_of_m_wordSpacing_84() { return &___m_wordSpacing_84; }
	inline void set_m_wordSpacing_84(float value)
	{
		___m_wordSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineSpacing_85)); }
	inline float get_m_lineSpacing_85() const { return ___m_lineSpacing_85; }
	inline float* get_address_of_m_lineSpacing_85() { return &___m_lineSpacing_85; }
	inline void set_m_lineSpacing_85(float value)
	{
		___m_lineSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_86() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineSpacingDelta_86)); }
	inline float get_m_lineSpacingDelta_86() const { return ___m_lineSpacingDelta_86; }
	inline float* get_address_of_m_lineSpacingDelta_86() { return &___m_lineSpacingDelta_86; }
	inline void set_m_lineSpacingDelta_86(float value)
	{
		___m_lineSpacingDelta_86 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_87() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineHeight_87)); }
	inline float get_m_lineHeight_87() const { return ___m_lineHeight_87; }
	inline float* get_address_of_m_lineHeight_87() { return &___m_lineHeight_87; }
	inline void set_m_lineHeight_87(float value)
	{
		___m_lineHeight_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_88() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineSpacingMax_88)); }
	inline float get_m_lineSpacingMax_88() const { return ___m_lineSpacingMax_88; }
	inline float* get_address_of_m_lineSpacingMax_88() { return &___m_lineSpacingMax_88; }
	inline void set_m_lineSpacingMax_88(float value)
	{
		___m_lineSpacingMax_88 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_89() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_paragraphSpacing_89)); }
	inline float get_m_paragraphSpacing_89() const { return ___m_paragraphSpacing_89; }
	inline float* get_address_of_m_paragraphSpacing_89() { return &___m_paragraphSpacing_89; }
	inline void set_m_paragraphSpacing_89(float value)
	{
		___m_paragraphSpacing_89 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_90() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_charWidthMaxAdj_90)); }
	inline float get_m_charWidthMaxAdj_90() const { return ___m_charWidthMaxAdj_90; }
	inline float* get_address_of_m_charWidthMaxAdj_90() { return &___m_charWidthMaxAdj_90; }
	inline void set_m_charWidthMaxAdj_90(float value)
	{
		___m_charWidthMaxAdj_90 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_91() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_charWidthAdjDelta_91)); }
	inline float get_m_charWidthAdjDelta_91() const { return ___m_charWidthAdjDelta_91; }
	inline float* get_address_of_m_charWidthAdjDelta_91() { return &___m_charWidthAdjDelta_91; }
	inline void set_m_charWidthAdjDelta_91(float value)
	{
		___m_charWidthAdjDelta_91 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_92() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableWordWrapping_92)); }
	inline bool get_m_enableWordWrapping_92() const { return ___m_enableWordWrapping_92; }
	inline bool* get_address_of_m_enableWordWrapping_92() { return &___m_enableWordWrapping_92; }
	inline void set_m_enableWordWrapping_92(bool value)
	{
		___m_enableWordWrapping_92 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_93() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isCharacterWrappingEnabled_93)); }
	inline bool get_m_isCharacterWrappingEnabled_93() const { return ___m_isCharacterWrappingEnabled_93; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_93() { return &___m_isCharacterWrappingEnabled_93; }
	inline void set_m_isCharacterWrappingEnabled_93(bool value)
	{
		___m_isCharacterWrappingEnabled_93 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_94() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isNonBreakingSpace_94)); }
	inline bool get_m_isNonBreakingSpace_94() const { return ___m_isNonBreakingSpace_94; }
	inline bool* get_address_of_m_isNonBreakingSpace_94() { return &___m_isNonBreakingSpace_94; }
	inline void set_m_isNonBreakingSpace_94(bool value)
	{
		___m_isNonBreakingSpace_94 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_95() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isIgnoringAlignment_95)); }
	inline bool get_m_isIgnoringAlignment_95() const { return ___m_isIgnoringAlignment_95; }
	inline bool* get_address_of_m_isIgnoringAlignment_95() { return &___m_isIgnoringAlignment_95; }
	inline void set_m_isIgnoringAlignment_95(bool value)
	{
		___m_isIgnoringAlignment_95 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_96() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_wordWrappingRatios_96)); }
	inline float get_m_wordWrappingRatios_96() const { return ___m_wordWrappingRatios_96; }
	inline float* get_address_of_m_wordWrappingRatios_96() { return &___m_wordWrappingRatios_96; }
	inline void set_m_wordWrappingRatios_96(float value)
	{
		___m_wordWrappingRatios_96 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_97() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_overflowMode_97)); }
	inline int32_t get_m_overflowMode_97() const { return ___m_overflowMode_97; }
	inline int32_t* get_address_of_m_overflowMode_97() { return &___m_overflowMode_97; }
	inline void set_m_overflowMode_97(int32_t value)
	{
		___m_overflowMode_97 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_98() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_firstOverflowCharacterIndex_98)); }
	inline int32_t get_m_firstOverflowCharacterIndex_98() const { return ___m_firstOverflowCharacterIndex_98; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_98() { return &___m_firstOverflowCharacterIndex_98; }
	inline void set_m_firstOverflowCharacterIndex_98(int32_t value)
	{
		___m_firstOverflowCharacterIndex_98 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_99() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_linkedTextComponent_99)); }
	inline TMP_Text_t1920000777 * get_m_linkedTextComponent_99() const { return ___m_linkedTextComponent_99; }
	inline TMP_Text_t1920000777 ** get_address_of_m_linkedTextComponent_99() { return &___m_linkedTextComponent_99; }
	inline void set_m_linkedTextComponent_99(TMP_Text_t1920000777 * value)
	{
		___m_linkedTextComponent_99 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_99), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_100() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isLinkedTextComponent_100)); }
	inline bool get_m_isLinkedTextComponent_100() const { return ___m_isLinkedTextComponent_100; }
	inline bool* get_address_of_m_isLinkedTextComponent_100() { return &___m_isLinkedTextComponent_100; }
	inline void set_m_isLinkedTextComponent_100(bool value)
	{
		___m_isLinkedTextComponent_100 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_101() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isTextTruncated_101)); }
	inline bool get_m_isTextTruncated_101() const { return ___m_isTextTruncated_101; }
	inline bool* get_address_of_m_isTextTruncated_101() { return &___m_isTextTruncated_101; }
	inline void set_m_isTextTruncated_101(bool value)
	{
		___m_isTextTruncated_101 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_102() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableKerning_102)); }
	inline bool get_m_enableKerning_102() const { return ___m_enableKerning_102; }
	inline bool* get_address_of_m_enableKerning_102() { return &___m_enableKerning_102; }
	inline void set_m_enableKerning_102(bool value)
	{
		___m_enableKerning_102 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_103() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableExtraPadding_103)); }
	inline bool get_m_enableExtraPadding_103() const { return ___m_enableExtraPadding_103; }
	inline bool* get_address_of_m_enableExtraPadding_103() { return &___m_enableExtraPadding_103; }
	inline void set_m_enableExtraPadding_103(bool value)
	{
		___m_enableExtraPadding_103 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_104() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___checkPaddingRequired_104)); }
	inline bool get_checkPaddingRequired_104() const { return ___checkPaddingRequired_104; }
	inline bool* get_address_of_checkPaddingRequired_104() { return &___checkPaddingRequired_104; }
	inline void set_checkPaddingRequired_104(bool value)
	{
		___checkPaddingRequired_104 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_105() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isRichText_105)); }
	inline bool get_m_isRichText_105() const { return ___m_isRichText_105; }
	inline bool* get_address_of_m_isRichText_105() { return &___m_isRichText_105; }
	inline void set_m_isRichText_105(bool value)
	{
		___m_isRichText_105 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_106() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_parseCtrlCharacters_106)); }
	inline bool get_m_parseCtrlCharacters_106() const { return ___m_parseCtrlCharacters_106; }
	inline bool* get_address_of_m_parseCtrlCharacters_106() { return &___m_parseCtrlCharacters_106; }
	inline void set_m_parseCtrlCharacters_106(bool value)
	{
		___m_parseCtrlCharacters_106 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_107() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isOverlay_107)); }
	inline bool get_m_isOverlay_107() const { return ___m_isOverlay_107; }
	inline bool* get_address_of_m_isOverlay_107() { return &___m_isOverlay_107; }
	inline void set_m_isOverlay_107(bool value)
	{
		___m_isOverlay_107 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_108() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isOrthographic_108)); }
	inline bool get_m_isOrthographic_108() const { return ___m_isOrthographic_108; }
	inline bool* get_address_of_m_isOrthographic_108() { return &___m_isOrthographic_108; }
	inline void set_m_isOrthographic_108(bool value)
	{
		___m_isOrthographic_108 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_109() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isCullingEnabled_109)); }
	inline bool get_m_isCullingEnabled_109() const { return ___m_isCullingEnabled_109; }
	inline bool* get_address_of_m_isCullingEnabled_109() { return &___m_isCullingEnabled_109; }
	inline void set_m_isCullingEnabled_109(bool value)
	{
		___m_isCullingEnabled_109 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_110() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_ignoreRectMaskCulling_110)); }
	inline bool get_m_ignoreRectMaskCulling_110() const { return ___m_ignoreRectMaskCulling_110; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_110() { return &___m_ignoreRectMaskCulling_110; }
	inline void set_m_ignoreRectMaskCulling_110(bool value)
	{
		___m_ignoreRectMaskCulling_110 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_111() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_ignoreCulling_111)); }
	inline bool get_m_ignoreCulling_111() const { return ___m_ignoreCulling_111; }
	inline bool* get_address_of_m_ignoreCulling_111() { return &___m_ignoreCulling_111; }
	inline void set_m_ignoreCulling_111(bool value)
	{
		___m_ignoreCulling_111 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_112() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_horizontalMapping_112)); }
	inline int32_t get_m_horizontalMapping_112() const { return ___m_horizontalMapping_112; }
	inline int32_t* get_address_of_m_horizontalMapping_112() { return &___m_horizontalMapping_112; }
	inline void set_m_horizontalMapping_112(int32_t value)
	{
		___m_horizontalMapping_112 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_113() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_verticalMapping_113)); }
	inline int32_t get_m_verticalMapping_113() const { return ___m_verticalMapping_113; }
	inline int32_t* get_address_of_m_verticalMapping_113() { return &___m_verticalMapping_113; }
	inline void set_m_verticalMapping_113(int32_t value)
	{
		___m_verticalMapping_113 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_114() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_uvLineOffset_114)); }
	inline float get_m_uvLineOffset_114() const { return ___m_uvLineOffset_114; }
	inline float* get_address_of_m_uvLineOffset_114() { return &___m_uvLineOffset_114; }
	inline void set_m_uvLineOffset_114(float value)
	{
		___m_uvLineOffset_114 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_115() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_renderMode_115)); }
	inline int32_t get_m_renderMode_115() const { return ___m_renderMode_115; }
	inline int32_t* get_address_of_m_renderMode_115() { return &___m_renderMode_115; }
	inline void set_m_renderMode_115(int32_t value)
	{
		___m_renderMode_115 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_116() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_geometrySortingOrder_116)); }
	inline int32_t get_m_geometrySortingOrder_116() const { return ___m_geometrySortingOrder_116; }
	inline int32_t* get_address_of_m_geometrySortingOrder_116() { return &___m_geometrySortingOrder_116; }
	inline void set_m_geometrySortingOrder_116(int32_t value)
	{
		___m_geometrySortingOrder_116 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_117() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_firstVisibleCharacter_117)); }
	inline int32_t get_m_firstVisibleCharacter_117() const { return ___m_firstVisibleCharacter_117; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_117() { return &___m_firstVisibleCharacter_117; }
	inline void set_m_firstVisibleCharacter_117(int32_t value)
	{
		___m_firstVisibleCharacter_117 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_118() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxVisibleCharacters_118)); }
	inline int32_t get_m_maxVisibleCharacters_118() const { return ___m_maxVisibleCharacters_118; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_118() { return &___m_maxVisibleCharacters_118; }
	inline void set_m_maxVisibleCharacters_118(int32_t value)
	{
		___m_maxVisibleCharacters_118 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_119() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxVisibleWords_119)); }
	inline int32_t get_m_maxVisibleWords_119() const { return ___m_maxVisibleWords_119; }
	inline int32_t* get_address_of_m_maxVisibleWords_119() { return &___m_maxVisibleWords_119; }
	inline void set_m_maxVisibleWords_119(int32_t value)
	{
		___m_maxVisibleWords_119 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_120() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxVisibleLines_120)); }
	inline int32_t get_m_maxVisibleLines_120() const { return ___m_maxVisibleLines_120; }
	inline int32_t* get_address_of_m_maxVisibleLines_120() { return &___m_maxVisibleLines_120; }
	inline void set_m_maxVisibleLines_120(int32_t value)
	{
		___m_maxVisibleLines_120 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_121() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_useMaxVisibleDescender_121)); }
	inline bool get_m_useMaxVisibleDescender_121() const { return ___m_useMaxVisibleDescender_121; }
	inline bool* get_address_of_m_useMaxVisibleDescender_121() { return &___m_useMaxVisibleDescender_121; }
	inline void set_m_useMaxVisibleDescender_121(bool value)
	{
		___m_useMaxVisibleDescender_121 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_122() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_pageToDisplay_122)); }
	inline int32_t get_m_pageToDisplay_122() const { return ___m_pageToDisplay_122; }
	inline int32_t* get_address_of_m_pageToDisplay_122() { return &___m_pageToDisplay_122; }
	inline void set_m_pageToDisplay_122(int32_t value)
	{
		___m_pageToDisplay_122 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_123() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isNewPage_123)); }
	inline bool get_m_isNewPage_123() const { return ___m_isNewPage_123; }
	inline bool* get_address_of_m_isNewPage_123() { return &___m_isNewPage_123; }
	inline void set_m_isNewPage_123(bool value)
	{
		___m_isNewPage_123 = value;
	}

	inline static int32_t get_offset_of_m_margin_124() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_margin_124)); }
	inline Vector4_t2243707581  get_m_margin_124() const { return ___m_margin_124; }
	inline Vector4_t2243707581 * get_address_of_m_margin_124() { return &___m_margin_124; }
	inline void set_m_margin_124(Vector4_t2243707581  value)
	{
		___m_margin_124 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_125() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_marginLeft_125)); }
	inline float get_m_marginLeft_125() const { return ___m_marginLeft_125; }
	inline float* get_address_of_m_marginLeft_125() { return &___m_marginLeft_125; }
	inline void set_m_marginLeft_125(float value)
	{
		___m_marginLeft_125 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_126() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_marginRight_126)); }
	inline float get_m_marginRight_126() const { return ___m_marginRight_126; }
	inline float* get_address_of_m_marginRight_126() { return &___m_marginRight_126; }
	inline void set_m_marginRight_126(float value)
	{
		___m_marginRight_126 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_127() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_marginWidth_127)); }
	inline float get_m_marginWidth_127() const { return ___m_marginWidth_127; }
	inline float* get_address_of_m_marginWidth_127() { return &___m_marginWidth_127; }
	inline void set_m_marginWidth_127(float value)
	{
		___m_marginWidth_127 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_128() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_marginHeight_128)); }
	inline float get_m_marginHeight_128() const { return ___m_marginHeight_128; }
	inline float* get_address_of_m_marginHeight_128() { return &___m_marginHeight_128; }
	inline void set_m_marginHeight_128(float value)
	{
		___m_marginHeight_128 = value;
	}

	inline static int32_t get_offset_of_m_width_129() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_width_129)); }
	inline float get_m_width_129() const { return ___m_width_129; }
	inline float* get_address_of_m_width_129() { return &___m_width_129; }
	inline void set_m_width_129(float value)
	{
		___m_width_129 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_130() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_textInfo_130)); }
	inline TMP_TextInfo_t2849466151 * get_m_textInfo_130() const { return ___m_textInfo_130; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_m_textInfo_130() { return &___m_textInfo_130; }
	inline void set_m_textInfo_130(TMP_TextInfo_t2849466151 * value)
	{
		___m_textInfo_130 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_130), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_131() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_havePropertiesChanged_131)); }
	inline bool get_m_havePropertiesChanged_131() const { return ___m_havePropertiesChanged_131; }
	inline bool* get_address_of_m_havePropertiesChanged_131() { return &___m_havePropertiesChanged_131; }
	inline void set_m_havePropertiesChanged_131(bool value)
	{
		___m_havePropertiesChanged_131 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_132() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isUsingLegacyAnimationComponent_132)); }
	inline bool get_m_isUsingLegacyAnimationComponent_132() const { return ___m_isUsingLegacyAnimationComponent_132; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_132() { return &___m_isUsingLegacyAnimationComponent_132; }
	inline void set_m_isUsingLegacyAnimationComponent_132(bool value)
	{
		___m_isUsingLegacyAnimationComponent_132 = value;
	}

	inline static int32_t get_offset_of_m_transform_133() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_transform_133)); }
	inline Transform_t3275118058 * get_m_transform_133() const { return ___m_transform_133; }
	inline Transform_t3275118058 ** get_address_of_m_transform_133() { return &___m_transform_133; }
	inline void set_m_transform_133(Transform_t3275118058 * value)
	{
		___m_transform_133 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_133), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_134() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_rectTransform_134)); }
	inline RectTransform_t3349966182 * get_m_rectTransform_134() const { return ___m_rectTransform_134; }
	inline RectTransform_t3349966182 ** get_address_of_m_rectTransform_134() { return &___m_rectTransform_134; }
	inline void set_m_rectTransform_134(RectTransform_t3349966182 * value)
	{
		___m_rectTransform_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_134), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___U3CautoSizeTextContainerU3Ek__BackingField_135)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_135() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return &___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_135(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_135 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_136() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_autoSizeTextContainer_136)); }
	inline bool get_m_autoSizeTextContainer_136() const { return ___m_autoSizeTextContainer_136; }
	inline bool* get_address_of_m_autoSizeTextContainer_136() { return &___m_autoSizeTextContainer_136; }
	inline void set_m_autoSizeTextContainer_136(bool value)
	{
		___m_autoSizeTextContainer_136 = value;
	}

	inline static int32_t get_offset_of_m_mesh_137() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_mesh_137)); }
	inline Mesh_t1356156583 * get_m_mesh_137() const { return ___m_mesh_137; }
	inline Mesh_t1356156583 ** get_address_of_m_mesh_137() { return &___m_mesh_137; }
	inline void set_m_mesh_137(Mesh_t1356156583 * value)
	{
		___m_mesh_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_137), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_138() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isVolumetricText_138)); }
	inline bool get_m_isVolumetricText_138() const { return ___m_isVolumetricText_138; }
	inline bool* get_address_of_m_isVolumetricText_138() { return &___m_isVolumetricText_138; }
	inline void set_m_isVolumetricText_138(bool value)
	{
		___m_isVolumetricText_138 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_139() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteAnimator_139)); }
	inline TMP_SpriteAnimator_t2347923044 * get_m_spriteAnimator_139() const { return ___m_spriteAnimator_139; }
	inline TMP_SpriteAnimator_t2347923044 ** get_address_of_m_spriteAnimator_139() { return &___m_spriteAnimator_139; }
	inline void set_m_spriteAnimator_139(TMP_SpriteAnimator_t2347923044 * value)
	{
		___m_spriteAnimator_139 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_139), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_140() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_flexibleHeight_140)); }
	inline float get_m_flexibleHeight_140() const { return ___m_flexibleHeight_140; }
	inline float* get_address_of_m_flexibleHeight_140() { return &___m_flexibleHeight_140; }
	inline void set_m_flexibleHeight_140(float value)
	{
		___m_flexibleHeight_140 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_141() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_flexibleWidth_141)); }
	inline float get_m_flexibleWidth_141() const { return ___m_flexibleWidth_141; }
	inline float* get_address_of_m_flexibleWidth_141() { return &___m_flexibleWidth_141; }
	inline void set_m_flexibleWidth_141(float value)
	{
		___m_flexibleWidth_141 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_142() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_minWidth_142)); }
	inline float get_m_minWidth_142() const { return ___m_minWidth_142; }
	inline float* get_address_of_m_minWidth_142() { return &___m_minWidth_142; }
	inline void set_m_minWidth_142(float value)
	{
		___m_minWidth_142 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_143() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_minHeight_143)); }
	inline float get_m_minHeight_143() const { return ___m_minHeight_143; }
	inline float* get_address_of_m_minHeight_143() { return &___m_minHeight_143; }
	inline void set_m_minHeight_143(float value)
	{
		___m_minHeight_143 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_144() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxWidth_144)); }
	inline float get_m_maxWidth_144() const { return ___m_maxWidth_144; }
	inline float* get_address_of_m_maxWidth_144() { return &___m_maxWidth_144; }
	inline void set_m_maxWidth_144(float value)
	{
		___m_maxWidth_144 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_145() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxHeight_145)); }
	inline float get_m_maxHeight_145() const { return ___m_maxHeight_145; }
	inline float* get_address_of_m_maxHeight_145() { return &___m_maxHeight_145; }
	inline void set_m_maxHeight_145(float value)
	{
		___m_maxHeight_145 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_146() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_LayoutElement_146)); }
	inline LayoutElement_t2808691390 * get_m_LayoutElement_146() const { return ___m_LayoutElement_146; }
	inline LayoutElement_t2808691390 ** get_address_of_m_LayoutElement_146() { return &___m_LayoutElement_146; }
	inline void set_m_LayoutElement_146(LayoutElement_t2808691390 * value)
	{
		___m_LayoutElement_146 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_146), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_147() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_preferredWidth_147)); }
	inline float get_m_preferredWidth_147() const { return ___m_preferredWidth_147; }
	inline float* get_address_of_m_preferredWidth_147() { return &___m_preferredWidth_147; }
	inline void set_m_preferredWidth_147(float value)
	{
		___m_preferredWidth_147 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_148() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_renderedWidth_148)); }
	inline float get_m_renderedWidth_148() const { return ___m_renderedWidth_148; }
	inline float* get_address_of_m_renderedWidth_148() { return &___m_renderedWidth_148; }
	inline void set_m_renderedWidth_148(float value)
	{
		___m_renderedWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_149() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isPreferredWidthDirty_149)); }
	inline bool get_m_isPreferredWidthDirty_149() const { return ___m_isPreferredWidthDirty_149; }
	inline bool* get_address_of_m_isPreferredWidthDirty_149() { return &___m_isPreferredWidthDirty_149; }
	inline void set_m_isPreferredWidthDirty_149(bool value)
	{
		___m_isPreferredWidthDirty_149 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_150() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_preferredHeight_150)); }
	inline float get_m_preferredHeight_150() const { return ___m_preferredHeight_150; }
	inline float* get_address_of_m_preferredHeight_150() { return &___m_preferredHeight_150; }
	inline void set_m_preferredHeight_150(float value)
	{
		___m_preferredHeight_150 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_151() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_renderedHeight_151)); }
	inline float get_m_renderedHeight_151() const { return ___m_renderedHeight_151; }
	inline float* get_address_of_m_renderedHeight_151() { return &___m_renderedHeight_151; }
	inline void set_m_renderedHeight_151(float value)
	{
		___m_renderedHeight_151 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_152() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isPreferredHeightDirty_152)); }
	inline bool get_m_isPreferredHeightDirty_152() const { return ___m_isPreferredHeightDirty_152; }
	inline bool* get_address_of_m_isPreferredHeightDirty_152() { return &___m_isPreferredHeightDirty_152; }
	inline void set_m_isPreferredHeightDirty_152(bool value)
	{
		___m_isPreferredHeightDirty_152 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_153() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isCalculatingPreferredValues_153)); }
	inline bool get_m_isCalculatingPreferredValues_153() const { return ___m_isCalculatingPreferredValues_153; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_153() { return &___m_isCalculatingPreferredValues_153; }
	inline void set_m_isCalculatingPreferredValues_153(bool value)
	{
		___m_isCalculatingPreferredValues_153 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_154() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_recursiveCount_154)); }
	inline int32_t get_m_recursiveCount_154() const { return ___m_recursiveCount_154; }
	inline int32_t* get_address_of_m_recursiveCount_154() { return &___m_recursiveCount_154; }
	inline void set_m_recursiveCount_154(int32_t value)
	{
		___m_recursiveCount_154 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_155() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_layoutPriority_155)); }
	inline int32_t get_m_layoutPriority_155() const { return ___m_layoutPriority_155; }
	inline int32_t* get_address_of_m_layoutPriority_155() { return &___m_layoutPriority_155; }
	inline void set_m_layoutPriority_155(int32_t value)
	{
		___m_layoutPriority_155 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_156() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isCalculateSizeRequired_156)); }
	inline bool get_m_isCalculateSizeRequired_156() const { return ___m_isCalculateSizeRequired_156; }
	inline bool* get_address_of_m_isCalculateSizeRequired_156() { return &___m_isCalculateSizeRequired_156; }
	inline void set_m_isCalculateSizeRequired_156(bool value)
	{
		___m_isCalculateSizeRequired_156 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_157() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isLayoutDirty_157)); }
	inline bool get_m_isLayoutDirty_157() const { return ___m_isLayoutDirty_157; }
	inline bool* get_address_of_m_isLayoutDirty_157() { return &___m_isLayoutDirty_157; }
	inline void set_m_isLayoutDirty_157(bool value)
	{
		___m_isLayoutDirty_157 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_158() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_verticesAlreadyDirty_158)); }
	inline bool get_m_verticesAlreadyDirty_158() const { return ___m_verticesAlreadyDirty_158; }
	inline bool* get_address_of_m_verticesAlreadyDirty_158() { return &___m_verticesAlreadyDirty_158; }
	inline void set_m_verticesAlreadyDirty_158(bool value)
	{
		___m_verticesAlreadyDirty_158 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_159() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_layoutAlreadyDirty_159)); }
	inline bool get_m_layoutAlreadyDirty_159() const { return ___m_layoutAlreadyDirty_159; }
	inline bool* get_address_of_m_layoutAlreadyDirty_159() { return &___m_layoutAlreadyDirty_159; }
	inline void set_m_layoutAlreadyDirty_159(bool value)
	{
		___m_layoutAlreadyDirty_159 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_160() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isAwake_160)); }
	inline bool get_m_isAwake_160() const { return ___m_isAwake_160; }
	inline bool* get_address_of_m_isAwake_160() { return &___m_isAwake_160; }
	inline void set_m_isAwake_160(bool value)
	{
		___m_isAwake_160 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_161() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isInputParsingRequired_161)); }
	inline bool get_m_isInputParsingRequired_161() const { return ___m_isInputParsingRequired_161; }
	inline bool* get_address_of_m_isInputParsingRequired_161() { return &___m_isInputParsingRequired_161; }
	inline void set_m_isInputParsingRequired_161(bool value)
	{
		___m_isInputParsingRequired_161 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_162() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_inputSource_162)); }
	inline int32_t get_m_inputSource_162() const { return ___m_inputSource_162; }
	inline int32_t* get_address_of_m_inputSource_162() { return &___m_inputSource_162; }
	inline void set_m_inputSource_162(int32_t value)
	{
		___m_inputSource_162 = value;
	}

	inline static int32_t get_offset_of_old_text_163() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___old_text_163)); }
	inline String_t* get_old_text_163() const { return ___old_text_163; }
	inline String_t** get_address_of_old_text_163() { return &___old_text_163; }
	inline void set_old_text_163(String_t* value)
	{
		___old_text_163 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_163), value);
	}

	inline static int32_t get_offset_of_m_fontScale_164() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontScale_164)); }
	inline float get_m_fontScale_164() const { return ___m_fontScale_164; }
	inline float* get_address_of_m_fontScale_164() { return &___m_fontScale_164; }
	inline void set_m_fontScale_164(float value)
	{
		___m_fontScale_164 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_165() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontScaleMultiplier_165)); }
	inline float get_m_fontScaleMultiplier_165() const { return ___m_fontScaleMultiplier_165; }
	inline float* get_address_of_m_fontScaleMultiplier_165() { return &___m_fontScaleMultiplier_165; }
	inline void set_m_fontScaleMultiplier_165(float value)
	{
		___m_fontScaleMultiplier_165 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_166() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_htmlTag_166)); }
	inline CharU5BU5D_t1328083999* get_m_htmlTag_166() const { return ___m_htmlTag_166; }
	inline CharU5BU5D_t1328083999** get_address_of_m_htmlTag_166() { return &___m_htmlTag_166; }
	inline void set_m_htmlTag_166(CharU5BU5D_t1328083999* value)
	{
		___m_htmlTag_166 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_166), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_167() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_xmlAttribute_167)); }
	inline XML_TagAttributeU5BU5D_t573465953* get_m_xmlAttribute_167() const { return ___m_xmlAttribute_167; }
	inline XML_TagAttributeU5BU5D_t573465953** get_address_of_m_xmlAttribute_167() { return &___m_xmlAttribute_167; }
	inline void set_m_xmlAttribute_167(XML_TagAttributeU5BU5D_t573465953* value)
	{
		___m_xmlAttribute_167 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_167), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_168() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_attributeParameterValues_168)); }
	inline SingleU5BU5D_t577127397* get_m_attributeParameterValues_168() const { return ___m_attributeParameterValues_168; }
	inline SingleU5BU5D_t577127397** get_address_of_m_attributeParameterValues_168() { return &___m_attributeParameterValues_168; }
	inline void set_m_attributeParameterValues_168(SingleU5BU5D_t577127397* value)
	{
		___m_attributeParameterValues_168 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_168), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_169() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___tag_LineIndent_169)); }
	inline float get_tag_LineIndent_169() const { return ___tag_LineIndent_169; }
	inline float* get_address_of_tag_LineIndent_169() { return &___tag_LineIndent_169; }
	inline void set_tag_LineIndent_169(float value)
	{
		___tag_LineIndent_169 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_170() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___tag_Indent_170)); }
	inline float get_tag_Indent_170() const { return ___tag_Indent_170; }
	inline float* get_address_of_tag_Indent_170() { return &___tag_Indent_170; }
	inline void set_tag_Indent_170(float value)
	{
		___tag_Indent_170 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_171() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_indentStack_171)); }
	inline TMP_XmlTagStack_1_t2735062451  get_m_indentStack_171() const { return ___m_indentStack_171; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_m_indentStack_171() { return &___m_indentStack_171; }
	inline void set_m_indentStack_171(TMP_XmlTagStack_1_t2735062451  value)
	{
		___m_indentStack_171 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_172() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___tag_NoParsing_172)); }
	inline bool get_tag_NoParsing_172() const { return ___tag_NoParsing_172; }
	inline bool* get_address_of_tag_NoParsing_172() { return &___tag_NoParsing_172; }
	inline void set_tag_NoParsing_172(bool value)
	{
		___tag_NoParsing_172 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_173() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isParsingText_173)); }
	inline bool get_m_isParsingText_173() const { return ___m_isParsingText_173; }
	inline bool* get_address_of_m_isParsingText_173() { return &___m_isParsingText_173; }
	inline void set_m_isParsingText_173(bool value)
	{
		___m_isParsingText_173 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_174() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_FXMatrix_174)); }
	inline Matrix4x4_t2933234003  get_m_FXMatrix_174() const { return ___m_FXMatrix_174; }
	inline Matrix4x4_t2933234003 * get_address_of_m_FXMatrix_174() { return &___m_FXMatrix_174; }
	inline void set_m_FXMatrix_174(Matrix4x4_t2933234003  value)
	{
		___m_FXMatrix_174 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_175() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isFXMatrixSet_175)); }
	inline bool get_m_isFXMatrixSet_175() const { return ___m_isFXMatrixSet_175; }
	inline bool* get_address_of_m_isFXMatrixSet_175() { return &___m_isFXMatrixSet_175; }
	inline void set_m_isFXMatrixSet_175(bool value)
	{
		___m_isFXMatrixSet_175 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_176() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_char_buffer_176)); }
	inline Int32U5BU5D_t3030399641* get_m_char_buffer_176() const { return ___m_char_buffer_176; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_char_buffer_176() { return &___m_char_buffer_176; }
	inline void set_m_char_buffer_176(Int32U5BU5D_t3030399641* value)
	{
		___m_char_buffer_176 = value;
		Il2CppCodeGenWriteBarrier((&___m_char_buffer_176), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_177() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_internalCharacterInfo_177)); }
	inline TMP_CharacterInfoU5BU5D_t602810366* get_m_internalCharacterInfo_177() const { return ___m_internalCharacterInfo_177; }
	inline TMP_CharacterInfoU5BU5D_t602810366** get_address_of_m_internalCharacterInfo_177() { return &___m_internalCharacterInfo_177; }
	inline void set_m_internalCharacterInfo_177(TMP_CharacterInfoU5BU5D_t602810366* value)
	{
		___m_internalCharacterInfo_177 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_177), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_178() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_input_CharArray_178)); }
	inline CharU5BU5D_t1328083999* get_m_input_CharArray_178() const { return ___m_input_CharArray_178; }
	inline CharU5BU5D_t1328083999** get_address_of_m_input_CharArray_178() { return &___m_input_CharArray_178; }
	inline void set_m_input_CharArray_178(CharU5BU5D_t1328083999* value)
	{
		___m_input_CharArray_178 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_178), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_179() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_charArray_Length_179)); }
	inline int32_t get_m_charArray_Length_179() const { return ___m_charArray_Length_179; }
	inline int32_t* get_address_of_m_charArray_Length_179() { return &___m_charArray_Length_179; }
	inline void set_m_charArray_Length_179(int32_t value)
	{
		___m_charArray_Length_179 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_180() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_totalCharacterCount_180)); }
	inline int32_t get_m_totalCharacterCount_180() const { return ___m_totalCharacterCount_180; }
	inline int32_t* get_address_of_m_totalCharacterCount_180() { return &___m_totalCharacterCount_180; }
	inline void set_m_totalCharacterCount_180(int32_t value)
	{
		___m_totalCharacterCount_180 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_181() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_SavedWordWrapState_181)); }
	inline WordWrapState_t433984875  get_m_SavedWordWrapState_181() const { return ___m_SavedWordWrapState_181; }
	inline WordWrapState_t433984875 * get_address_of_m_SavedWordWrapState_181() { return &___m_SavedWordWrapState_181; }
	inline void set_m_SavedWordWrapState_181(WordWrapState_t433984875  value)
	{
		___m_SavedWordWrapState_181 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_182() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_SavedLineState_182)); }
	inline WordWrapState_t433984875  get_m_SavedLineState_182() const { return ___m_SavedLineState_182; }
	inline WordWrapState_t433984875 * get_address_of_m_SavedLineState_182() { return &___m_SavedLineState_182; }
	inline void set_m_SavedLineState_182(WordWrapState_t433984875  value)
	{
		___m_SavedLineState_182 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_183() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_characterCount_183)); }
	inline int32_t get_m_characterCount_183() const { return ___m_characterCount_183; }
	inline int32_t* get_address_of_m_characterCount_183() { return &___m_characterCount_183; }
	inline void set_m_characterCount_183(int32_t value)
	{
		___m_characterCount_183 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_184() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_firstCharacterOfLine_184)); }
	inline int32_t get_m_firstCharacterOfLine_184() const { return ___m_firstCharacterOfLine_184; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_184() { return &___m_firstCharacterOfLine_184; }
	inline void set_m_firstCharacterOfLine_184(int32_t value)
	{
		___m_firstCharacterOfLine_184 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_185() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_firstVisibleCharacterOfLine_185)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_185() const { return ___m_firstVisibleCharacterOfLine_185; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_185() { return &___m_firstVisibleCharacterOfLine_185; }
	inline void set_m_firstVisibleCharacterOfLine_185(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_185 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_186() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lastCharacterOfLine_186)); }
	inline int32_t get_m_lastCharacterOfLine_186() const { return ___m_lastCharacterOfLine_186; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_186() { return &___m_lastCharacterOfLine_186; }
	inline void set_m_lastCharacterOfLine_186(int32_t value)
	{
		___m_lastCharacterOfLine_186 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_187() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lastVisibleCharacterOfLine_187)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_187() const { return ___m_lastVisibleCharacterOfLine_187; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_187() { return &___m_lastVisibleCharacterOfLine_187; }
	inline void set_m_lastVisibleCharacterOfLine_187(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_187 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_188() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineNumber_188)); }
	inline int32_t get_m_lineNumber_188() const { return ___m_lineNumber_188; }
	inline int32_t* get_address_of_m_lineNumber_188() { return &___m_lineNumber_188; }
	inline void set_m_lineNumber_188(int32_t value)
	{
		___m_lineNumber_188 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_189() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineVisibleCharacterCount_189)); }
	inline int32_t get_m_lineVisibleCharacterCount_189() const { return ___m_lineVisibleCharacterCount_189; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_189() { return &___m_lineVisibleCharacterCount_189; }
	inline void set_m_lineVisibleCharacterCount_189(int32_t value)
	{
		___m_lineVisibleCharacterCount_189 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_190() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_pageNumber_190)); }
	inline int32_t get_m_pageNumber_190() const { return ___m_pageNumber_190; }
	inline int32_t* get_address_of_m_pageNumber_190() { return &___m_pageNumber_190; }
	inline void set_m_pageNumber_190(int32_t value)
	{
		___m_pageNumber_190 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_191() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxAscender_191)); }
	inline float get_m_maxAscender_191() const { return ___m_maxAscender_191; }
	inline float* get_address_of_m_maxAscender_191() { return &___m_maxAscender_191; }
	inline void set_m_maxAscender_191(float value)
	{
		___m_maxAscender_191 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_192() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxCapHeight_192)); }
	inline float get_m_maxCapHeight_192() const { return ___m_maxCapHeight_192; }
	inline float* get_address_of_m_maxCapHeight_192() { return &___m_maxCapHeight_192; }
	inline void set_m_maxCapHeight_192(float value)
	{
		___m_maxCapHeight_192 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_193() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxDescender_193)); }
	inline float get_m_maxDescender_193() const { return ___m_maxDescender_193; }
	inline float* get_address_of_m_maxDescender_193() { return &___m_maxDescender_193; }
	inline void set_m_maxDescender_193(float value)
	{
		___m_maxDescender_193 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_194() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxLineAscender_194)); }
	inline float get_m_maxLineAscender_194() const { return ___m_maxLineAscender_194; }
	inline float* get_address_of_m_maxLineAscender_194() { return &___m_maxLineAscender_194; }
	inline void set_m_maxLineAscender_194(float value)
	{
		___m_maxLineAscender_194 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_195() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxLineDescender_195)); }
	inline float get_m_maxLineDescender_195() const { return ___m_maxLineDescender_195; }
	inline float* get_address_of_m_maxLineDescender_195() { return &___m_maxLineDescender_195; }
	inline void set_m_maxLineDescender_195(float value)
	{
		___m_maxLineDescender_195 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_startOfLineAscender_196)); }
	inline float get_m_startOfLineAscender_196() const { return ___m_startOfLineAscender_196; }
	inline float* get_address_of_m_startOfLineAscender_196() { return &___m_startOfLineAscender_196; }
	inline void set_m_startOfLineAscender_196(float value)
	{
		___m_startOfLineAscender_196 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_197() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineOffset_197)); }
	inline float get_m_lineOffset_197() const { return ___m_lineOffset_197; }
	inline float* get_address_of_m_lineOffset_197() { return &___m_lineOffset_197; }
	inline void set_m_lineOffset_197(float value)
	{
		___m_lineOffset_197 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_198() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_meshExtents_198)); }
	inline Extents_t3018556803  get_m_meshExtents_198() const { return ___m_meshExtents_198; }
	inline Extents_t3018556803 * get_address_of_m_meshExtents_198() { return &___m_meshExtents_198; }
	inline void set_m_meshExtents_198(Extents_t3018556803  value)
	{
		___m_meshExtents_198 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_199() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_htmlColor_199)); }
	inline Color32_t874517518  get_m_htmlColor_199() const { return ___m_htmlColor_199; }
	inline Color32_t874517518 * get_address_of_m_htmlColor_199() { return &___m_htmlColor_199; }
	inline void set_m_htmlColor_199(Color32_t874517518  value)
	{
		___m_htmlColor_199 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_200() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_colorStack_200)); }
	inline TMP_XmlTagStack_1_t1533070037  get_m_colorStack_200() const { return ___m_colorStack_200; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_m_colorStack_200() { return &___m_colorStack_200; }
	inline void set_m_colorStack_200(TMP_XmlTagStack_1_t1533070037  value)
	{
		___m_colorStack_200 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_201() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_underlineColorStack_201)); }
	inline TMP_XmlTagStack_1_t1533070037  get_m_underlineColorStack_201() const { return ___m_underlineColorStack_201; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_m_underlineColorStack_201() { return &___m_underlineColorStack_201; }
	inline void set_m_underlineColorStack_201(TMP_XmlTagStack_1_t1533070037  value)
	{
		___m_underlineColorStack_201 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_202() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_strikethroughColorStack_202)); }
	inline TMP_XmlTagStack_1_t1533070037  get_m_strikethroughColorStack_202() const { return ___m_strikethroughColorStack_202; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_m_strikethroughColorStack_202() { return &___m_strikethroughColorStack_202; }
	inline void set_m_strikethroughColorStack_202(TMP_XmlTagStack_1_t1533070037  value)
	{
		___m_strikethroughColorStack_202 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_203() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_highlightColorStack_203)); }
	inline TMP_XmlTagStack_1_t1533070037  get_m_highlightColorStack_203() const { return ___m_highlightColorStack_203; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_m_highlightColorStack_203() { return &___m_highlightColorStack_203; }
	inline void set_m_highlightColorStack_203(TMP_XmlTagStack_1_t1533070037  value)
	{
		___m_highlightColorStack_203 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_204() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_colorGradientPreset_204)); }
	inline TMP_ColorGradient_t1159837347 * get_m_colorGradientPreset_204() const { return ___m_colorGradientPreset_204; }
	inline TMP_ColorGradient_t1159837347 ** get_address_of_m_colorGradientPreset_204() { return &___m_colorGradientPreset_204; }
	inline void set_m_colorGradientPreset_204(TMP_ColorGradient_t1159837347 * value)
	{
		___m_colorGradientPreset_204 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_204), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_colorGradientStack_205)); }
	inline TMP_XmlTagStack_1_t1818389866  get_m_colorGradientStack_205() const { return ___m_colorGradientStack_205; }
	inline TMP_XmlTagStack_1_t1818389866 * get_address_of_m_colorGradientStack_205() { return &___m_colorGradientStack_205; }
	inline void set_m_colorGradientStack_205(TMP_XmlTagStack_1_t1818389866  value)
	{
		___m_colorGradientStack_205 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_206() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_tabSpacing_206)); }
	inline float get_m_tabSpacing_206() const { return ___m_tabSpacing_206; }
	inline float* get_address_of_m_tabSpacing_206() { return &___m_tabSpacing_206; }
	inline void set_m_tabSpacing_206(float value)
	{
		___m_tabSpacing_206 = value;
	}

	inline static int32_t get_offset_of_m_spacing_207() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spacing_207)); }
	inline float get_m_spacing_207() const { return ___m_spacing_207; }
	inline float* get_address_of_m_spacing_207() { return &___m_spacing_207; }
	inline void set_m_spacing_207(float value)
	{
		___m_spacing_207 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_styleStack_208)); }
	inline TMP_XmlTagStack_1_t2730429967  get_m_styleStack_208() const { return ___m_styleStack_208; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_m_styleStack_208() { return &___m_styleStack_208; }
	inline void set_m_styleStack_208(TMP_XmlTagStack_1_t2730429967  value)
	{
		___m_styleStack_208 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_209() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_actionStack_209)); }
	inline TMP_XmlTagStack_1_t2730429967  get_m_actionStack_209() const { return ___m_actionStack_209; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_m_actionStack_209() { return &___m_actionStack_209; }
	inline void set_m_actionStack_209(TMP_XmlTagStack_1_t2730429967  value)
	{
		___m_actionStack_209 = value;
	}

	inline static int32_t get_offset_of_m_padding_210() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_padding_210)); }
	inline float get_m_padding_210() const { return ___m_padding_210; }
	inline float* get_address_of_m_padding_210() { return &___m_padding_210; }
	inline void set_m_padding_210(float value)
	{
		___m_padding_210 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_211() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_baselineOffset_211)); }
	inline float get_m_baselineOffset_211() const { return ___m_baselineOffset_211; }
	inline float* get_address_of_m_baselineOffset_211() { return &___m_baselineOffset_211; }
	inline void set_m_baselineOffset_211(float value)
	{
		___m_baselineOffset_211 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_212() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_baselineOffsetStack_212)); }
	inline TMP_XmlTagStack_1_t2735062451  get_m_baselineOffsetStack_212() const { return ___m_baselineOffsetStack_212; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_m_baselineOffsetStack_212() { return &___m_baselineOffsetStack_212; }
	inline void set_m_baselineOffsetStack_212(TMP_XmlTagStack_1_t2735062451  value)
	{
		___m_baselineOffsetStack_212 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_213() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_xAdvance_213)); }
	inline float get_m_xAdvance_213() const { return ___m_xAdvance_213; }
	inline float* get_address_of_m_xAdvance_213() { return &___m_xAdvance_213; }
	inline void set_m_xAdvance_213(float value)
	{
		___m_xAdvance_213 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_214() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_textElementType_214)); }
	inline int32_t get_m_textElementType_214() const { return ___m_textElementType_214; }
	inline int32_t* get_address_of_m_textElementType_214() { return &___m_textElementType_214; }
	inline void set_m_textElementType_214(int32_t value)
	{
		___m_textElementType_214 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_215() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_cached_TextElement_215)); }
	inline TMP_TextElement_t2285620223 * get_m_cached_TextElement_215() const { return ___m_cached_TextElement_215; }
	inline TMP_TextElement_t2285620223 ** get_address_of_m_cached_TextElement_215() { return &___m_cached_TextElement_215; }
	inline void set_m_cached_TextElement_215(TMP_TextElement_t2285620223 * value)
	{
		___m_cached_TextElement_215 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_215), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_216() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_cached_Underline_GlyphInfo_216)); }
	inline TMP_Glyph_t909793902 * get_m_cached_Underline_GlyphInfo_216() const { return ___m_cached_Underline_GlyphInfo_216; }
	inline TMP_Glyph_t909793902 ** get_address_of_m_cached_Underline_GlyphInfo_216() { return &___m_cached_Underline_GlyphInfo_216; }
	inline void set_m_cached_Underline_GlyphInfo_216(TMP_Glyph_t909793902 * value)
	{
		___m_cached_Underline_GlyphInfo_216 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_GlyphInfo_216), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_GlyphInfo_217() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_cached_Ellipsis_GlyphInfo_217)); }
	inline TMP_Glyph_t909793902 * get_m_cached_Ellipsis_GlyphInfo_217() const { return ___m_cached_Ellipsis_GlyphInfo_217; }
	inline TMP_Glyph_t909793902 ** get_address_of_m_cached_Ellipsis_GlyphInfo_217() { return &___m_cached_Ellipsis_GlyphInfo_217; }
	inline void set_m_cached_Ellipsis_GlyphInfo_217(TMP_Glyph_t909793902 * value)
	{
		___m_cached_Ellipsis_GlyphInfo_217 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_GlyphInfo_217), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_218() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_defaultSpriteAsset_218)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_defaultSpriteAsset_218() const { return ___m_defaultSpriteAsset_218; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_defaultSpriteAsset_218() { return &___m_defaultSpriteAsset_218; }
	inline void set_m_defaultSpriteAsset_218(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_defaultSpriteAsset_218 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_218), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_219() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentSpriteAsset_219)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_currentSpriteAsset_219() const { return ___m_currentSpriteAsset_219; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_currentSpriteAsset_219() { return &___m_currentSpriteAsset_219; }
	inline void set_m_currentSpriteAsset_219(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_currentSpriteAsset_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_219), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_220() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteCount_220)); }
	inline int32_t get_m_spriteCount_220() const { return ___m_spriteCount_220; }
	inline int32_t* get_address_of_m_spriteCount_220() { return &___m_spriteCount_220; }
	inline void set_m_spriteCount_220(int32_t value)
	{
		___m_spriteCount_220 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_221() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteIndex_221)); }
	inline int32_t get_m_spriteIndex_221() const { return ___m_spriteIndex_221; }
	inline int32_t* get_address_of_m_spriteIndex_221() { return &___m_spriteIndex_221; }
	inline void set_m_spriteIndex_221(int32_t value)
	{
		___m_spriteIndex_221 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_222() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteAnimationID_222)); }
	inline int32_t get_m_spriteAnimationID_222() const { return ___m_spriteAnimationID_222; }
	inline int32_t* get_address_of_m_spriteAnimationID_222() { return &___m_spriteAnimationID_222; }
	inline void set_m_spriteAnimationID_222(int32_t value)
	{
		___m_spriteAnimationID_222 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_223() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_ignoreActiveState_223)); }
	inline bool get_m_ignoreActiveState_223() const { return ___m_ignoreActiveState_223; }
	inline bool* get_address_of_m_ignoreActiveState_223() { return &___m_ignoreActiveState_223; }
	inline void set_m_ignoreActiveState_223(bool value)
	{
		___m_ignoreActiveState_223 = value;
	}

	inline static int32_t get_offset_of_k_Power_224() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___k_Power_224)); }
	inline SingleU5BU5D_t577127397* get_k_Power_224() const { return ___k_Power_224; }
	inline SingleU5BU5D_t577127397** get_address_of_k_Power_224() { return &___k_Power_224; }
	inline void set_k_Power_224(SingleU5BU5D_t577127397* value)
	{
		___k_Power_224 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_224), value);
	}
};

struct TMP_Text_t1920000777_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t874517518  ___s_colorWhite_45;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t2243707579  ___k_LargePositiveVector2_225;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t2243707579  ___k_LargeNegativeVector2_226;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_227;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_228;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_229;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_230;

public:
	inline static int32_t get_offset_of_s_colorWhite_45() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___s_colorWhite_45)); }
	inline Color32_t874517518  get_s_colorWhite_45() const { return ___s_colorWhite_45; }
	inline Color32_t874517518 * get_address_of_s_colorWhite_45() { return &___s_colorWhite_45; }
	inline void set_s_colorWhite_45(Color32_t874517518  value)
	{
		___s_colorWhite_45 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_225() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargePositiveVector2_225)); }
	inline Vector2_t2243707579  get_k_LargePositiveVector2_225() const { return ___k_LargePositiveVector2_225; }
	inline Vector2_t2243707579 * get_address_of_k_LargePositiveVector2_225() { return &___k_LargePositiveVector2_225; }
	inline void set_k_LargePositiveVector2_225(Vector2_t2243707579  value)
	{
		___k_LargePositiveVector2_225 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_226() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargeNegativeVector2_226)); }
	inline Vector2_t2243707579  get_k_LargeNegativeVector2_226() const { return ___k_LargeNegativeVector2_226; }
	inline Vector2_t2243707579 * get_address_of_k_LargeNegativeVector2_226() { return &___k_LargeNegativeVector2_226; }
	inline void set_k_LargeNegativeVector2_226(Vector2_t2243707579  value)
	{
		___k_LargeNegativeVector2_226 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_227() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargePositiveFloat_227)); }
	inline float get_k_LargePositiveFloat_227() const { return ___k_LargePositiveFloat_227; }
	inline float* get_address_of_k_LargePositiveFloat_227() { return &___k_LargePositiveFloat_227; }
	inline void set_k_LargePositiveFloat_227(float value)
	{
		___k_LargePositiveFloat_227 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_228() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargeNegativeFloat_228)); }
	inline float get_k_LargeNegativeFloat_228() const { return ___k_LargeNegativeFloat_228; }
	inline float* get_address_of_k_LargeNegativeFloat_228() { return &___k_LargeNegativeFloat_228; }
	inline void set_k_LargeNegativeFloat_228(float value)
	{
		___k_LargeNegativeFloat_228 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_229() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargePositiveInt_229)); }
	inline int32_t get_k_LargePositiveInt_229() const { return ___k_LargePositiveInt_229; }
	inline int32_t* get_address_of_k_LargePositiveInt_229() { return &___k_LargePositiveInt_229; }
	inline void set_k_LargePositiveInt_229(int32_t value)
	{
		___k_LargePositiveInt_229 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_230() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargeNegativeInt_230)); }
	inline int32_t get_k_LargeNegativeInt_230() const { return ___k_LargeNegativeInt_230; }
	inline int32_t* get_address_of_k_LargeNegativeInt_230() { return &___k_LargeNegativeInt_230; }
	inline void set_k_LargeNegativeInt_230(int32_t value)
	{
		___k_LargeNegativeInt_230 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T1920000777_H
#ifndef TEXTMESHPROUGUI_T934157183_H
#define TEXTMESHPROUGUI_T934157183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshProUGUI
struct  TextMeshProUGUI_t934157183  : public TMP_Text_t1920000777
{
public:
	// System.Boolean TMPro.TextMeshProUGUI::m_isRebuildingLayout
	bool ___m_isRebuildingLayout_231;
	// System.Boolean TMPro.TextMeshProUGUI::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_232;
	// TMPro.TMP_SubMeshUI[] TMPro.TextMeshProUGUI::m_subTextObjects
	TMP_SubMeshUIU5BU5D_t2798123870* ___m_subTextObjects_233;
	// System.Single TMPro.TextMeshProUGUI::m_previousLossyScaleY
	float ___m_previousLossyScaleY_234;
	// UnityEngine.Vector3[] TMPro.TextMeshProUGUI::m_RectTransformCorners
	Vector3U5BU5D_t1172311765* ___m_RectTransformCorners_235;
	// UnityEngine.CanvasRenderer TMPro.TextMeshProUGUI::m_canvasRenderer
	CanvasRenderer_t261436805 * ___m_canvasRenderer_236;
	// UnityEngine.Canvas TMPro.TextMeshProUGUI::m_canvas
	Canvas_t209405766 * ___m_canvas_237;
	// System.Boolean TMPro.TextMeshProUGUI::m_isFirstAllocation
	bool ___m_isFirstAllocation_238;
	// System.Int32 TMPro.TextMeshProUGUI::m_max_characters
	int32_t ___m_max_characters_239;
	// System.Boolean TMPro.TextMeshProUGUI::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_240;
	// UnityEngine.Material TMPro.TextMeshProUGUI::m_baseMaterial
	Material_t193706927 * ___m_baseMaterial_241;
	// System.Boolean TMPro.TextMeshProUGUI::m_isScrollRegionSet
	bool ___m_isScrollRegionSet_242;
	// System.Int32 TMPro.TextMeshProUGUI::m_stencilID
	int32_t ___m_stencilID_243;
	// UnityEngine.Vector4 TMPro.TextMeshProUGUI::m_maskOffset
	Vector4_t2243707581  ___m_maskOffset_244;
	// UnityEngine.Matrix4x4 TMPro.TextMeshProUGUI::m_EnvMapMatrix
	Matrix4x4_t2933234003  ___m_EnvMapMatrix_245;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_246;
	// System.Int32 TMPro.TextMeshProUGUI::m_recursiveCountA
	int32_t ___m_recursiveCountA_247;
	// System.Int32 TMPro.TextMeshProUGUI::loopCountA
	int32_t ___loopCountA_248;

public:
	inline static int32_t get_offset_of_m_isRebuildingLayout_231() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_isRebuildingLayout_231)); }
	inline bool get_m_isRebuildingLayout_231() const { return ___m_isRebuildingLayout_231; }
	inline bool* get_address_of_m_isRebuildingLayout_231() { return &___m_isRebuildingLayout_231; }
	inline void set_m_isRebuildingLayout_231(bool value)
	{
		___m_isRebuildingLayout_231 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_232() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_hasFontAssetChanged_232)); }
	inline bool get_m_hasFontAssetChanged_232() const { return ___m_hasFontAssetChanged_232; }
	inline bool* get_address_of_m_hasFontAssetChanged_232() { return &___m_hasFontAssetChanged_232; }
	inline void set_m_hasFontAssetChanged_232(bool value)
	{
		___m_hasFontAssetChanged_232 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_233() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_subTextObjects_233)); }
	inline TMP_SubMeshUIU5BU5D_t2798123870* get_m_subTextObjects_233() const { return ___m_subTextObjects_233; }
	inline TMP_SubMeshUIU5BU5D_t2798123870** get_address_of_m_subTextObjects_233() { return &___m_subTextObjects_233; }
	inline void set_m_subTextObjects_233(TMP_SubMeshUIU5BU5D_t2798123870* value)
	{
		___m_subTextObjects_233 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_233), value);
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_234() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_previousLossyScaleY_234)); }
	inline float get_m_previousLossyScaleY_234() const { return ___m_previousLossyScaleY_234; }
	inline float* get_address_of_m_previousLossyScaleY_234() { return &___m_previousLossyScaleY_234; }
	inline void set_m_previousLossyScaleY_234(float value)
	{
		___m_previousLossyScaleY_234 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_235() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_RectTransformCorners_235)); }
	inline Vector3U5BU5D_t1172311765* get_m_RectTransformCorners_235() const { return ___m_RectTransformCorners_235; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_RectTransformCorners_235() { return &___m_RectTransformCorners_235; }
	inline void set_m_RectTransformCorners_235(Vector3U5BU5D_t1172311765* value)
	{
		___m_RectTransformCorners_235 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_235), value);
	}

	inline static int32_t get_offset_of_m_canvasRenderer_236() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_canvasRenderer_236)); }
	inline CanvasRenderer_t261436805 * get_m_canvasRenderer_236() const { return ___m_canvasRenderer_236; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_canvasRenderer_236() { return &___m_canvasRenderer_236; }
	inline void set_m_canvasRenderer_236(CanvasRenderer_t261436805 * value)
	{
		___m_canvasRenderer_236 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_236), value);
	}

	inline static int32_t get_offset_of_m_canvas_237() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_canvas_237)); }
	inline Canvas_t209405766 * get_m_canvas_237() const { return ___m_canvas_237; }
	inline Canvas_t209405766 ** get_address_of_m_canvas_237() { return &___m_canvas_237; }
	inline void set_m_canvas_237(Canvas_t209405766 * value)
	{
		___m_canvas_237 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvas_237), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_238() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_isFirstAllocation_238)); }
	inline bool get_m_isFirstAllocation_238() const { return ___m_isFirstAllocation_238; }
	inline bool* get_address_of_m_isFirstAllocation_238() { return &___m_isFirstAllocation_238; }
	inline void set_m_isFirstAllocation_238(bool value)
	{
		___m_isFirstAllocation_238 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_239() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_max_characters_239)); }
	inline int32_t get_m_max_characters_239() const { return ___m_max_characters_239; }
	inline int32_t* get_address_of_m_max_characters_239() { return &___m_max_characters_239; }
	inline void set_m_max_characters_239(int32_t value)
	{
		___m_max_characters_239 = value;
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_240() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_isMaskingEnabled_240)); }
	inline bool get_m_isMaskingEnabled_240() const { return ___m_isMaskingEnabled_240; }
	inline bool* get_address_of_m_isMaskingEnabled_240() { return &___m_isMaskingEnabled_240; }
	inline void set_m_isMaskingEnabled_240(bool value)
	{
		___m_isMaskingEnabled_240 = value;
	}

	inline static int32_t get_offset_of_m_baseMaterial_241() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_baseMaterial_241)); }
	inline Material_t193706927 * get_m_baseMaterial_241() const { return ___m_baseMaterial_241; }
	inline Material_t193706927 ** get_address_of_m_baseMaterial_241() { return &___m_baseMaterial_241; }
	inline void set_m_baseMaterial_241(Material_t193706927 * value)
	{
		___m_baseMaterial_241 = value;
		Il2CppCodeGenWriteBarrier((&___m_baseMaterial_241), value);
	}

	inline static int32_t get_offset_of_m_isScrollRegionSet_242() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_isScrollRegionSet_242)); }
	inline bool get_m_isScrollRegionSet_242() const { return ___m_isScrollRegionSet_242; }
	inline bool* get_address_of_m_isScrollRegionSet_242() { return &___m_isScrollRegionSet_242; }
	inline void set_m_isScrollRegionSet_242(bool value)
	{
		___m_isScrollRegionSet_242 = value;
	}

	inline static int32_t get_offset_of_m_stencilID_243() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_stencilID_243)); }
	inline int32_t get_m_stencilID_243() const { return ___m_stencilID_243; }
	inline int32_t* get_address_of_m_stencilID_243() { return &___m_stencilID_243; }
	inline void set_m_stencilID_243(int32_t value)
	{
		___m_stencilID_243 = value;
	}

	inline static int32_t get_offset_of_m_maskOffset_244() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_maskOffset_244)); }
	inline Vector4_t2243707581  get_m_maskOffset_244() const { return ___m_maskOffset_244; }
	inline Vector4_t2243707581 * get_address_of_m_maskOffset_244() { return &___m_maskOffset_244; }
	inline void set_m_maskOffset_244(Vector4_t2243707581  value)
	{
		___m_maskOffset_244 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_245() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_EnvMapMatrix_245)); }
	inline Matrix4x4_t2933234003  get_m_EnvMapMatrix_245() const { return ___m_EnvMapMatrix_245; }
	inline Matrix4x4_t2933234003 * get_address_of_m_EnvMapMatrix_245() { return &___m_EnvMapMatrix_245; }
	inline void set_m_EnvMapMatrix_245(Matrix4x4_t2933234003  value)
	{
		___m_EnvMapMatrix_245 = value;
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_246() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_isRegisteredForEvents_246)); }
	inline bool get_m_isRegisteredForEvents_246() const { return ___m_isRegisteredForEvents_246; }
	inline bool* get_address_of_m_isRegisteredForEvents_246() { return &___m_isRegisteredForEvents_246; }
	inline void set_m_isRegisteredForEvents_246(bool value)
	{
		___m_isRegisteredForEvents_246 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCountA_247() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___m_recursiveCountA_247)); }
	inline int32_t get_m_recursiveCountA_247() const { return ___m_recursiveCountA_247; }
	inline int32_t* get_address_of_m_recursiveCountA_247() { return &___m_recursiveCountA_247; }
	inline void set_m_recursiveCountA_247(int32_t value)
	{
		___m_recursiveCountA_247 = value;
	}

	inline static int32_t get_offset_of_loopCountA_248() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t934157183, ___loopCountA_248)); }
	inline int32_t get_loopCountA_248() const { return ___loopCountA_248; }
	inline int32_t* get_address_of_loopCountA_248() { return &___loopCountA_248; }
	inline void set_loopCountA_248(int32_t value)
	{
		___loopCountA_248 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPROUGUI_T934157183_H
#ifndef TEXTMESHPRO_T2521834357_H
#define TEXTMESHPRO_T2521834357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshPro
struct  TextMeshPro_t2521834357  : public TMP_Text_t1920000777
{
public:
	// System.Boolean TMPro.TextMeshPro::m_currentAutoSizeMode
	bool ___m_currentAutoSizeMode_231;
	// System.Boolean TMPro.TextMeshPro::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_232;
	// System.Single TMPro.TextMeshPro::m_previousLossyScaleY
	float ___m_previousLossyScaleY_233;
	// UnityEngine.Renderer TMPro.TextMeshPro::m_renderer
	Renderer_t257310565 * ___m_renderer_234;
	// UnityEngine.MeshFilter TMPro.TextMeshPro::m_meshFilter
	MeshFilter_t3026937449 * ___m_meshFilter_235;
	// System.Boolean TMPro.TextMeshPro::m_isFirstAllocation
	bool ___m_isFirstAllocation_236;
	// System.Int32 TMPro.TextMeshPro::m_max_characters
	int32_t ___m_max_characters_237;
	// System.Int32 TMPro.TextMeshPro::m_max_numberOfLines
	int32_t ___m_max_numberOfLines_238;
	// UnityEngine.Bounds TMPro.TextMeshPro::m_default_bounds
	Bounds_t3033363703  ___m_default_bounds_239;
	// TMPro.TMP_SubMesh[] TMPro.TextMeshPro::m_subTextObjects
	TMP_SubMeshU5BU5D_t4206981150* ___m_subTextObjects_240;
	// System.Boolean TMPro.TextMeshPro::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_241;
	// System.Boolean TMPro.TextMeshPro::isMaskUpdateRequired
	bool ___isMaskUpdateRequired_242;
	// TMPro.MaskingTypes TMPro.TextMeshPro::m_maskType
	int32_t ___m_maskType_243;
	// UnityEngine.Matrix4x4 TMPro.TextMeshPro::m_EnvMapMatrix
	Matrix4x4_t2933234003  ___m_EnvMapMatrix_244;
	// UnityEngine.Vector3[] TMPro.TextMeshPro::m_RectTransformCorners
	Vector3U5BU5D_t1172311765* ___m_RectTransformCorners_245;
	// System.Boolean TMPro.TextMeshPro::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_246;
	// System.Int32 TMPro.TextMeshPro::loopCountA
	int32_t ___loopCountA_247;

public:
	inline static int32_t get_offset_of_m_currentAutoSizeMode_231() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_currentAutoSizeMode_231)); }
	inline bool get_m_currentAutoSizeMode_231() const { return ___m_currentAutoSizeMode_231; }
	inline bool* get_address_of_m_currentAutoSizeMode_231() { return &___m_currentAutoSizeMode_231; }
	inline void set_m_currentAutoSizeMode_231(bool value)
	{
		___m_currentAutoSizeMode_231 = value;
	}

	inline static int32_t get_offset_of_m_hasFontAssetChanged_232() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_hasFontAssetChanged_232)); }
	inline bool get_m_hasFontAssetChanged_232() const { return ___m_hasFontAssetChanged_232; }
	inline bool* get_address_of_m_hasFontAssetChanged_232() { return &___m_hasFontAssetChanged_232; }
	inline void set_m_hasFontAssetChanged_232(bool value)
	{
		___m_hasFontAssetChanged_232 = value;
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_233() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_previousLossyScaleY_233)); }
	inline float get_m_previousLossyScaleY_233() const { return ___m_previousLossyScaleY_233; }
	inline float* get_address_of_m_previousLossyScaleY_233() { return &___m_previousLossyScaleY_233; }
	inline void set_m_previousLossyScaleY_233(float value)
	{
		___m_previousLossyScaleY_233 = value;
	}

	inline static int32_t get_offset_of_m_renderer_234() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_renderer_234)); }
	inline Renderer_t257310565 * get_m_renderer_234() const { return ___m_renderer_234; }
	inline Renderer_t257310565 ** get_address_of_m_renderer_234() { return &___m_renderer_234; }
	inline void set_m_renderer_234(Renderer_t257310565 * value)
	{
		___m_renderer_234 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_234), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_235() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_meshFilter_235)); }
	inline MeshFilter_t3026937449 * get_m_meshFilter_235() const { return ___m_meshFilter_235; }
	inline MeshFilter_t3026937449 ** get_address_of_m_meshFilter_235() { return &___m_meshFilter_235; }
	inline void set_m_meshFilter_235(MeshFilter_t3026937449 * value)
	{
		___m_meshFilter_235 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_235), value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_236() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_isFirstAllocation_236)); }
	inline bool get_m_isFirstAllocation_236() const { return ___m_isFirstAllocation_236; }
	inline bool* get_address_of_m_isFirstAllocation_236() { return &___m_isFirstAllocation_236; }
	inline void set_m_isFirstAllocation_236(bool value)
	{
		___m_isFirstAllocation_236 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_237() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_max_characters_237)); }
	inline int32_t get_m_max_characters_237() const { return ___m_max_characters_237; }
	inline int32_t* get_address_of_m_max_characters_237() { return &___m_max_characters_237; }
	inline void set_m_max_characters_237(int32_t value)
	{
		___m_max_characters_237 = value;
	}

	inline static int32_t get_offset_of_m_max_numberOfLines_238() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_max_numberOfLines_238)); }
	inline int32_t get_m_max_numberOfLines_238() const { return ___m_max_numberOfLines_238; }
	inline int32_t* get_address_of_m_max_numberOfLines_238() { return &___m_max_numberOfLines_238; }
	inline void set_m_max_numberOfLines_238(int32_t value)
	{
		___m_max_numberOfLines_238 = value;
	}

	inline static int32_t get_offset_of_m_default_bounds_239() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_default_bounds_239)); }
	inline Bounds_t3033363703  get_m_default_bounds_239() const { return ___m_default_bounds_239; }
	inline Bounds_t3033363703 * get_address_of_m_default_bounds_239() { return &___m_default_bounds_239; }
	inline void set_m_default_bounds_239(Bounds_t3033363703  value)
	{
		___m_default_bounds_239 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_240() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_subTextObjects_240)); }
	inline TMP_SubMeshU5BU5D_t4206981150* get_m_subTextObjects_240() const { return ___m_subTextObjects_240; }
	inline TMP_SubMeshU5BU5D_t4206981150** get_address_of_m_subTextObjects_240() { return &___m_subTextObjects_240; }
	inline void set_m_subTextObjects_240(TMP_SubMeshU5BU5D_t4206981150* value)
	{
		___m_subTextObjects_240 = value;
		Il2CppCodeGenWriteBarrier((&___m_subTextObjects_240), value);
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_241() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_isMaskingEnabled_241)); }
	inline bool get_m_isMaskingEnabled_241() const { return ___m_isMaskingEnabled_241; }
	inline bool* get_address_of_m_isMaskingEnabled_241() { return &___m_isMaskingEnabled_241; }
	inline void set_m_isMaskingEnabled_241(bool value)
	{
		___m_isMaskingEnabled_241 = value;
	}

	inline static int32_t get_offset_of_isMaskUpdateRequired_242() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___isMaskUpdateRequired_242)); }
	inline bool get_isMaskUpdateRequired_242() const { return ___isMaskUpdateRequired_242; }
	inline bool* get_address_of_isMaskUpdateRequired_242() { return &___isMaskUpdateRequired_242; }
	inline void set_isMaskUpdateRequired_242(bool value)
	{
		___isMaskUpdateRequired_242 = value;
	}

	inline static int32_t get_offset_of_m_maskType_243() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_maskType_243)); }
	inline int32_t get_m_maskType_243() const { return ___m_maskType_243; }
	inline int32_t* get_address_of_m_maskType_243() { return &___m_maskType_243; }
	inline void set_m_maskType_243(int32_t value)
	{
		___m_maskType_243 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_244() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_EnvMapMatrix_244)); }
	inline Matrix4x4_t2933234003  get_m_EnvMapMatrix_244() const { return ___m_EnvMapMatrix_244; }
	inline Matrix4x4_t2933234003 * get_address_of_m_EnvMapMatrix_244() { return &___m_EnvMapMatrix_244; }
	inline void set_m_EnvMapMatrix_244(Matrix4x4_t2933234003  value)
	{
		___m_EnvMapMatrix_244 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_245() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_RectTransformCorners_245)); }
	inline Vector3U5BU5D_t1172311765* get_m_RectTransformCorners_245() const { return ___m_RectTransformCorners_245; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_RectTransformCorners_245() { return &___m_RectTransformCorners_245; }
	inline void set_m_RectTransformCorners_245(Vector3U5BU5D_t1172311765* value)
	{
		___m_RectTransformCorners_245 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransformCorners_245), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_246() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___m_isRegisteredForEvents_246)); }
	inline bool get_m_isRegisteredForEvents_246() const { return ___m_isRegisteredForEvents_246; }
	inline bool* get_address_of_m_isRegisteredForEvents_246() { return &___m_isRegisteredForEvents_246; }
	inline void set_m_isRegisteredForEvents_246(bool value)
	{
		___m_isRegisteredForEvents_246 = value;
	}

	inline static int32_t get_offset_of_loopCountA_247() { return static_cast<int32_t>(offsetof(TextMeshPro_t2521834357, ___loopCountA_247)); }
	inline int32_t get_loopCountA_247() const { return ___loopCountA_247; }
	inline int32_t* get_address_of_loopCountA_247() { return &___loopCountA_247; }
	inline void set_loopCountA_247(int32_t value)
	{
		___loopCountA_247 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESHPRO_T2521834357_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (AmazonS3Uri_t3833005522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2601[4] = 
{
	AmazonS3Uri_t3833005522::get_offset_of_U3CIsPathStyleU3Ek__BackingField_0(),
	AmazonS3Uri_t3833005522::get_offset_of_U3CBucketU3Ek__BackingField_1(),
	AmazonS3Uri_t3833005522::get_offset_of_U3CKeyU3Ek__BackingField_2(),
	AmazonS3Uri_t3833005522::get_offset_of_U3CRegionU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (AmazonS3Util_t2934979980), -1, sizeof(AmazonS3Util_t2934979980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2602[1] = 
{
	AmazonS3Util_t2934979980_StaticFields::get_offset_of_extensionToMime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (BucketRegionDetector_t3645418826), -1, sizeof(BucketRegionDetector_t3645418826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2603[1] = 
{
	BucketRegionDetector_t3645418826_StaticFields::get_offset_of_U3CBucketRegionCacheU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (S3Constants_t195587261), -1, sizeof(S3Constants_t195587261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2604[26] = 
{
	S3Constants_t195587261_StaticFields::get_offset_of_MinPartSize_0(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataObjectKey_1(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataAcl_2(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataRedirect_3(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataStatus_4(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataContentType_5(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataMetaPrefix_6(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataXAmzPrefix_7(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataAccessKeyId_8(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataPolicy_9(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataSignature_10(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataXAmzSignature_11(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataXAmzAlgorithm_12(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataXAmzCredential_13(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataXAmzDate_14(),
	S3Constants_t195587261_StaticFields::get_offset_of_PostFormDataSecurityToken_15(),
	S3Constants_t195587261_StaticFields::get_offset_of_AmzHeaderMultipartPartsCount_16(),
	S3Constants_t195587261_StaticFields::get_offset_of_AmzHeaderRequestPayer_17(),
	S3Constants_t195587261_StaticFields::get_offset_of_AmzHeaderRequestCharged_18(),
	S3Constants_t195587261_StaticFields::get_offset_of_AmzHeaderTagging_19(),
	S3Constants_t195587261_StaticFields::get_offset_of_AmzHeaderTaggingDirective_20(),
	S3Constants_t195587261_StaticFields::get_offset_of_AmzHeaderTaggingCount_21(),
	S3Constants_t195587261_StaticFields::get_offset_of_AmzHeaderRestoreOutputPath_22(),
	S3Constants_t195587261_StaticFields::get_offset_of_BucketVersions_23(),
	S3Constants_t195587261_StaticFields::get_offset_of_MetadataDirectives_24(),
	S3Constants_t195587261_StaticFields::get_offset_of_GetObjectExtraSubResources_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (ByteRange_t286214923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2605[2] = 
{
	ByteRange_t286214923::get_offset_of_U3CStartU3Ek__BackingField_0(),
	ByteRange_t286214923::get_offset_of_U3CEndU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (CompleteMultipartUploadRequest_t2028871123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (CopyObjectRequest_t3530746871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (CopyPartRequest_t1158344445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[1] = 
{
	CopyPartRequest_t1158344445::get_offset_of_partNumber_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (CopyPartResponse_t2814382569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2609[1] = 
{
	CopyPartResponse_t2814382569::get_offset_of_partNumber_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (DeleteBucketRequest_t3161332728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2610[2] = 
{
	DeleteBucketRequest_t3161332728::get_offset_of_bucketRegion_4(),
	DeleteBucketRequest_t3161332728::get_offset_of_useClientRegion_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (DeletedObject_t2664222218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (DeleteError_t3399861423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (DeleteObjectsResponse_t4016776830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[3] = 
{
	DeleteObjectsResponse_t4016776830::get_offset_of_deleted_3(),
	DeleteObjectsResponse_t4016776830::get_offset_of_errors_4(),
	DeleteObjectsResponse_t4016776830::get_offset_of_requestCharged_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (Expiration_t12298299), -1, sizeof(Expiration_t12298299_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2614[4] = 
{
	Expiration_t12298299::get_offset_of_expiryDate_0(),
	Expiration_t12298299::get_offset_of_ruleId_1(),
	Expiration_t12298299_StaticFields::get_offset_of_expiryRegex_2(),
	Expiration_t12298299_StaticFields::get_offset_of_ruleRegex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (GetObjectRequest_t109865576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2615[15] = 
{
	GetObjectRequest_t109865576::get_offset_of_bucketName_4(),
	GetObjectRequest_t109865576::get_offset_of_modifiedSinceDate_5(),
	GetObjectRequest_t109865576::get_offset_of_unmodifiedSinceDate_6(),
	GetObjectRequest_t109865576::get_offset_of_etagToMatch_7(),
	GetObjectRequest_t109865576::get_offset_of_etagToNotMatch_8(),
	GetObjectRequest_t109865576::get_offset_of_key_9(),
	GetObjectRequest_t109865576::get_offset_of_byteRange_10(),
	GetObjectRequest_t109865576::get_offset_of_responseExpires_11(),
	GetObjectRequest_t109865576::get_offset_of_versionId_12(),
	GetObjectRequest_t109865576::get_offset_of_responseHeaders_13(),
	GetObjectRequest_t109865576::get_offset_of_serverSideCustomerEncryption_14(),
	GetObjectRequest_t109865576::get_offset_of_serverSideEncryptionCustomerProvidedKey_15(),
	GetObjectRequest_t109865576::get_offset_of_serverSideEncryptionCustomerProvidedKeyMD5_16(),
	GetObjectRequest_t109865576::get_offset_of_requestPayer_17(),
	GetObjectRequest_t109865576::get_offset_of_partNumber_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (GetObjectResponse_t653598534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[25] = 
{
	GetObjectResponse_t653598534::get_offset_of_deleteMarker_5(),
	GetObjectResponse_t653598534::get_offset_of_acceptRanges_6(),
	GetObjectResponse_t653598534::get_offset_of_expiration_7(),
	GetObjectResponse_t653598534::get_offset_of_restoreExpiration_8(),
	GetObjectResponse_t653598534::get_offset_of_restoreInProgress_9(),
	GetObjectResponse_t653598534::get_offset_of_lastModified_10(),
	GetObjectResponse_t653598534::get_offset_of_eTag_11(),
	GetObjectResponse_t653598534::get_offset_of_missingMeta_12(),
	GetObjectResponse_t653598534::get_offset_of_versionId_13(),
	GetObjectResponse_t653598534::get_offset_of_expires_14(),
	GetObjectResponse_t653598534::get_offset_of_websiteRedirectLocation_15(),
	GetObjectResponse_t653598534::get_offset_of_serverSideEncryption_16(),
	GetObjectResponse_t653598534::get_offset_of_serverSideEncryptionCustomerMethod_17(),
	GetObjectResponse_t653598534::get_offset_of_serverSideEncryptionKeyManagementServiceKeyId_18(),
	GetObjectResponse_t653598534::get_offset_of_headersCollection_19(),
	GetObjectResponse_t653598534::get_offset_of_metadataCollection_20(),
	GetObjectResponse_t653598534::get_offset_of_replicationStatus_21(),
	GetObjectResponse_t653598534::get_offset_of_partsCount_22(),
	GetObjectResponse_t653598534::get_offset_of_storageClass_23(),
	GetObjectResponse_t653598534::get_offset_of_requestCharged_24(),
	GetObjectResponse_t653598534::get_offset_of_tagCount_25(),
	GetObjectResponse_t653598534::get_offset_of_bucketName_26(),
	GetObjectResponse_t653598534::get_offset_of_key_27(),
	GetObjectResponse_t653598534::get_offset_of_isExpiresUnmarshalled_28(),
	GetObjectResponse_t653598534::get_offset_of_U3CRawExpiresU3Ek__BackingField_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (HeadersCollection_t2711555648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[1] = 
{
	HeadersCollection_t2711555648::get_offset_of__values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (InitiateMultipartUploadRequest_t440630327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2618[2] = 
{
	InitiateMultipartUploadRequest_t440630327::get_offset_of_key_4(),
	InitiateMultipartUploadRequest_t440630327::get_offset_of_headersCollection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (ListBucketsRequest_t1957637484), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (ListObjectsRequest_t3715455147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2620[7] = 
{
	ListObjectsRequest_t3715455147::get_offset_of_bucketName_4(),
	ListObjectsRequest_t3715455147::get_offset_of_delimiter_5(),
	ListObjectsRequest_t3715455147::get_offset_of_marker_6(),
	ListObjectsRequest_t3715455147::get_offset_of_maxKeys_7(),
	ListObjectsRequest_t3715455147::get_offset_of_prefix_8(),
	ListObjectsRequest_t3715455147::get_offset_of_encoding_9(),
	ListObjectsRequest_t3715455147::get_offset_of_requestPayer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { sizeof (ListObjectsResponse_t2587169543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2621[8] = 
{
	ListObjectsResponse_t2587169543::get_offset_of_isTruncated_3(),
	ListObjectsResponse_t2587169543::get_offset_of_nextMarker_4(),
	ListObjectsResponse_t2587169543::get_offset_of_contents_5(),
	ListObjectsResponse_t2587169543::get_offset_of_name_6(),
	ListObjectsResponse_t2587169543::get_offset_of_prefix_7(),
	ListObjectsResponse_t2587169543::get_offset_of_maxKeys_8(),
	ListObjectsResponse_t2587169543::get_offset_of_commonPrefixes_9(),
	ListObjectsResponse_t2587169543::get_offset_of_delimiter_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { sizeof (MetadataCollection_t3745113723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2622[2] = 
{
	0,
	MetadataCollection_t3745113723::get_offset_of_values_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { sizeof (Owner_t97740679), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2623[2] = 
{
	Owner_t97740679::get_offset_of_U3CDisplayNameU3Ek__BackingField_0(),
	Owner_t97740679::get_offset_of_U3CIdU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { sizeof (PutBucketRequest_t1453664272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2624[3] = 
{
	PutBucketRequest_t1453664272::get_offset_of_bucketRegion_4(),
	PutBucketRequest_t1453664272::get_offset_of_bucketRegionName_5(),
	PutBucketRequest_t1453664272::get_offset_of_useClientRegion_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { sizeof (PutObjectRequest_t3608576685), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2625[7] = 
{
	PutObjectRequest_t3608576685::get_offset_of_key_4(),
	PutObjectRequest_t3608576685::get_offset_of_headersCollection_5(),
	PutObjectRequest_t3608576685::get_offset_of_inputStream_6(),
	PutObjectRequest_t3608576685::get_offset_of_filePath_7(),
	PutObjectRequest_t3608576685::get_offset_of_contentBody_8(),
	PutObjectRequest_t3608576685::get_offset_of_autoCloseStream_9(),
	PutObjectRequest_t3608576685::get_offset_of_autoResetStreamPosition_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { sizeof (PutObjectResponse_t418550917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2626[1] = 
{
	PutObjectResponse_t418550917::get_offset_of_eTag_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { sizeof (PutWithACLRequest_t3114873226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { sizeof (ResponseHeaderOverrides_t382636191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2628[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	ResponseHeaderOverrides_t382636191::get_offset_of__contentType_6(),
	ResponseHeaderOverrides_t382636191::get_offset_of__contentLanguage_7(),
	ResponseHeaderOverrides_t382636191::get_offset_of__expires_8(),
	ResponseHeaderOverrides_t382636191::get_offset_of__cacheControl_9(),
	ResponseHeaderOverrides_t382636191::get_offset_of__contentDisposition_10(),
	ResponseHeaderOverrides_t382636191::get_offset_of__contentEncoding_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { sizeof (S3Object_t1836716019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2629[7] = 
{
	S3Object_t1836716019::get_offset_of_eTag_0(),
	S3Object_t1836716019::get_offset_of_key_1(),
	S3Object_t1836716019::get_offset_of_lastModified_2(),
	S3Object_t1836716019::get_offset_of_owner_3(),
	S3Object_t1836716019::get_offset_of_size_4(),
	S3Object_t1836716019::get_offset_of_storageClass_5(),
	S3Object_t1836716019::get_offset_of_bucketName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { sizeof (StreamResponse_t2490679653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2630[2] = 
{
	StreamResponse_t2490679653::get_offset_of_disposed_3(),
	StreamResponse_t2490679653::get_offset_of_responseStream_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { sizeof (UploadPartRequest_t874148693), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2631[4] = 
{
	UploadPartRequest_t874148693::get_offset_of_inputStream_4(),
	UploadPartRequest_t874148693::get_offset_of_partNumber_5(),
	UploadPartRequest_t874148693::get_offset_of_filePath_6(),
	UploadPartRequest_t874148693::get_offset_of_filePosition_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { sizeof (UploadPartResponse_t2256813681), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2632[2] = 
{
	UploadPartResponse_t2256813681::get_offset_of_eTag_3(),
	UploadPartResponse_t2256813681::get_offset_of_partNumber_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { sizeof (CommonPrefixesItemUnmarshaller_t2768491974), -1, sizeof(CommonPrefixesItemUnmarshaller_t2768491974_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2633[1] = 
{
	CommonPrefixesItemUnmarshaller_t2768491974_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { sizeof (ContentsItemUnmarshaller_t2864009311), -1, sizeof(ContentsItemUnmarshaller_t2864009311_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2634[1] = 
{
	ContentsItemUnmarshaller_t2864009311_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { sizeof (GetObjectRequestMarshaller_t1832281661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { sizeof (GetObjectResponseUnmarshaller_t4197678500), -1, sizeof(GetObjectResponseUnmarshaller_t4197678500_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2636[1] = 
{
	GetObjectResponseUnmarshaller_t4197678500_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { sizeof (ListObjectsRequestMarshaller_t3330065254), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { sizeof (ListObjectsResponseUnmarshaller_t1341882655), -1, sizeof(ListObjectsResponseUnmarshaller_t1341882655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2638[1] = 
{
	ListObjectsResponseUnmarshaller_t1341882655_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { sizeof (OwnerUnmarshaller_t3479873017), -1, sizeof(OwnerUnmarshaller_t3479873017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2639[1] = 
{
	OwnerUnmarshaller_t3479873017_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { sizeof (S3ErrorResponseUnmarshaller_t140617651), -1, sizeof(S3ErrorResponseUnmarshaller_t140617651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2640[1] = 
{
	S3ErrorResponseUnmarshaller_t140617651_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { sizeof (S3ErrorResponse_t623087499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2641[5] = 
{
	S3ErrorResponse_t623087499::get_offset_of_U3CRegionU3Ek__BackingField_4(),
	S3ErrorResponse_t623087499::get_offset_of_U3CResourceU3Ek__BackingField_5(),
	S3ErrorResponse_t623087499::get_offset_of_U3CId2U3Ek__BackingField_6(),
	S3ErrorResponse_t623087499::get_offset_of_U3CAmzCfIdU3Ek__BackingField_7(),
	S3ErrorResponse_t623087499::get_offset_of_U3CParsingExceptionU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { sizeof (S3ReponseUnmarshaller_t3340520676), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { sizeof (S3Transforms_t1521396015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { sizeof (S3UnmarshallerContext_t295718887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2644[1] = 
{
	S3UnmarshallerContext_t295718887::get_offset_of__checkedForErrorResponse_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { sizeof (AmazonS3ExceptionHandler_t3293121943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { sizeof (AmazonS3KmsHandler_t1831047167), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (AmazonS3PostMarshallHandler_t3097908696), -1, sizeof(AmazonS3PostMarshallHandler_t3097908696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2647[7] = 
{
	AmazonS3PostMarshallHandler_t3097908696_StaticFields::get_offset_of_UnsupportedAccelerateRequestTypes_3(),
	AmazonS3PostMarshallHandler_t3097908696_StaticFields::get_offset_of_sseKeyHeaders_4(),
	AmazonS3PostMarshallHandler_t3097908696_StaticFields::get_offset_of_separators_5(),
	AmazonS3PostMarshallHandler_t3097908696_StaticFields::get_offset_of_bucketValidationRegex_6(),
	AmazonS3PostMarshallHandler_t3097908696_StaticFields::get_offset_of_dnsValidationRegex1_7(),
	AmazonS3PostMarshallHandler_t3097908696_StaticFields::get_offset_of_dnsValidationRegex2_8(),
	AmazonS3PostMarshallHandler_t3097908696_StaticFields::get_offset_of_invalidPatterns_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (U3CU3Ec_t3596783049), -1, sizeof(U3CU3Ec_t3596783049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2648[3] = 
{
	U3CU3Ec_t3596783049_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3596783049_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
	U3CU3Ec_t3596783049_StaticFields::get_offset_of_U3CU3E9__8_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (AmazonS3PreMarshallHandler_t3687553405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (AmazonS3RedirectHandler_t470844594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (AmazonS3ResponseHandler_t1411073907), -1, sizeof(AmazonS3ResponseHandler_t1411073907_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2651[1] = 
{
	AmazonS3ResponseHandler_t1411073907_StaticFields::get_offset_of_etagTrimChars_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (AmazonS3RetryPolicy_t2252046324), -1, sizeof(AmazonS3RetryPolicy_t2252046324_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2652[1] = 
{
	AmazonS3RetryPolicy_t2252046324_StaticFields::get_offset_of_RequestsWith200Error_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (S3Signer_t70082942), -1, sizeof(S3Signer_t70082942_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2653[3] = 
{
	S3Signer_t70082942::get_offset_of__useSigV4_1(),
	S3Signer_t70082942_StaticFields::get_offset_of_SignableParameters_2(),
	S3Signer_t70082942_StaticFields::get_offset_of_SubResourcesSigningExclusion_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (U3CU3Ec_t4103665279), -1, sizeof(U3CU3Ec_t4103665279_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2654[3] = 
{
	U3CU3Ec_t4103665279_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4103665279_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
	U3CU3Ec_t4103665279_StaticFields::get_offset_of_U3CU3E9__10_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (AmazonS3HttpDeleteHandler_t3511250707), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (U3CModuleU3E_t3783534238), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (FastAction_t1302743882), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2657[2] = 
{
	FastAction_t1302743882::get_offset_of_delegates_0(),
	FastAction_t1302743882::get_offset_of_lookup_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2659[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2660[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (MaterialReferenceManager_t1374850133), -1, sizeof(MaterialReferenceManager_t1374850133_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2661[5] = 
{
	MaterialReferenceManager_t1374850133_StaticFields::get_offset_of_s_Instance_0(),
	MaterialReferenceManager_t1374850133::get_offset_of_m_FontMaterialReferenceLookup_1(),
	MaterialReferenceManager_t1374850133::get_offset_of_m_FontAssetReferenceLookup_2(),
	MaterialReferenceManager_t1374850133::get_offset_of_m_SpriteAssetReferenceLookup_3(),
	MaterialReferenceManager_t1374850133::get_offset_of_m_ColorGradientReferenceLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (MaterialReference_t2854353496)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2662[9] = 
{
	MaterialReference_t2854353496::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t2854353496::get_offset_of_fontAsset_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t2854353496::get_offset_of_spriteAsset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t2854353496::get_offset_of_material_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t2854353496::get_offset_of_isDefaultMaterial_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t2854353496::get_offset_of_isFallbackMaterial_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t2854353496::get_offset_of_fallbackMaterial_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t2854353496::get_offset_of_padding_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialReference_t2854353496::get_offset_of_referenceCount_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (TextContainerAnchors_t2796925266)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2663[11] = 
{
	TextContainerAnchors_t2796925266::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (TextContainer_t4263764796), -1, sizeof(TextContainer_t4263764796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2664[13] = 
{
	TextContainer_t4263764796::get_offset_of_m_hasChanged_2(),
	TextContainer_t4263764796::get_offset_of_m_pivot_3(),
	TextContainer_t4263764796::get_offset_of_m_anchorPosition_4(),
	TextContainer_t4263764796::get_offset_of_m_rect_5(),
	TextContainer_t4263764796::get_offset_of_m_isDefaultWidth_6(),
	TextContainer_t4263764796::get_offset_of_m_isDefaultHeight_7(),
	TextContainer_t4263764796::get_offset_of_m_isAutoFitting_8(),
	TextContainer_t4263764796::get_offset_of_m_corners_9(),
	TextContainer_t4263764796::get_offset_of_m_worldCorners_10(),
	TextContainer_t4263764796::get_offset_of_m_margins_11(),
	TextContainer_t4263764796::get_offset_of_m_rectTransform_12(),
	TextContainer_t4263764796_StaticFields::get_offset_of_k_defaultSize_13(),
	TextContainer_t4263764796::get_offset_of_m_textMeshPro_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (TextMeshPro_t2521834357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2665[17] = 
{
	TextMeshPro_t2521834357::get_offset_of_m_currentAutoSizeMode_231(),
	TextMeshPro_t2521834357::get_offset_of_m_hasFontAssetChanged_232(),
	TextMeshPro_t2521834357::get_offset_of_m_previousLossyScaleY_233(),
	TextMeshPro_t2521834357::get_offset_of_m_renderer_234(),
	TextMeshPro_t2521834357::get_offset_of_m_meshFilter_235(),
	TextMeshPro_t2521834357::get_offset_of_m_isFirstAllocation_236(),
	TextMeshPro_t2521834357::get_offset_of_m_max_characters_237(),
	TextMeshPro_t2521834357::get_offset_of_m_max_numberOfLines_238(),
	TextMeshPro_t2521834357::get_offset_of_m_default_bounds_239(),
	TextMeshPro_t2521834357::get_offset_of_m_subTextObjects_240(),
	TextMeshPro_t2521834357::get_offset_of_m_isMaskingEnabled_241(),
	TextMeshPro_t2521834357::get_offset_of_isMaskUpdateRequired_242(),
	TextMeshPro_t2521834357::get_offset_of_m_maskType_243(),
	TextMeshPro_t2521834357::get_offset_of_m_EnvMapMatrix_244(),
	TextMeshPro_t2521834357::get_offset_of_m_RectTransformCorners_245(),
	TextMeshPro_t2521834357::get_offset_of_m_isRegisteredForEvents_246(),
	TextMeshPro_t2521834357::get_offset_of_loopCountA_247(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (TextMeshProUGUI_t934157183), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2666[18] = 
{
	TextMeshProUGUI_t934157183::get_offset_of_m_isRebuildingLayout_231(),
	TextMeshProUGUI_t934157183::get_offset_of_m_hasFontAssetChanged_232(),
	TextMeshProUGUI_t934157183::get_offset_of_m_subTextObjects_233(),
	TextMeshProUGUI_t934157183::get_offset_of_m_previousLossyScaleY_234(),
	TextMeshProUGUI_t934157183::get_offset_of_m_RectTransformCorners_235(),
	TextMeshProUGUI_t934157183::get_offset_of_m_canvasRenderer_236(),
	TextMeshProUGUI_t934157183::get_offset_of_m_canvas_237(),
	TextMeshProUGUI_t934157183::get_offset_of_m_isFirstAllocation_238(),
	TextMeshProUGUI_t934157183::get_offset_of_m_max_characters_239(),
	TextMeshProUGUI_t934157183::get_offset_of_m_isMaskingEnabled_240(),
	TextMeshProUGUI_t934157183::get_offset_of_m_baseMaterial_241(),
	TextMeshProUGUI_t934157183::get_offset_of_m_isScrollRegionSet_242(),
	TextMeshProUGUI_t934157183::get_offset_of_m_stencilID_243(),
	TextMeshProUGUI_t934157183::get_offset_of_m_maskOffset_244(),
	TextMeshProUGUI_t934157183::get_offset_of_m_EnvMapMatrix_245(),
	TextMeshProUGUI_t934157183::get_offset_of_m_isRegisteredForEvents_246(),
	TextMeshProUGUI_t934157183::get_offset_of_m_recursiveCountA_247(),
	TextMeshProUGUI_t934157183::get_offset_of_loopCountA_248(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (Compute_DistanceTransform_EventTypes_t3434557887)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2667[3] = 
{
	Compute_DistanceTransform_EventTypes_t3434557887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (TMPro_EventManager_t528455910), -1, sizeof(TMPro_EventManager_t528455910_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2668[12] = 
{
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_COMPUTE_DT_EVENT_0(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_MATERIAL_PROPERTY_EVENT_1(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_FONT_PROPERTY_EVENT_2(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_OnPreRenderObject_Event_10(),
	TMPro_EventManager_t528455910_StaticFields::get_offset_of_TEXT_CHANGED_EVENT_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (Compute_DT_EventArgs_t4231491594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[3] = 
{
	Compute_DT_EventArgs_t4231491594::get_offset_of_EventType_0(),
	Compute_DT_EventArgs_t4231491594::get_offset_of_ProgressPercentage_1(),
	Compute_DT_EventArgs_t4231491594::get_offset_of_Colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (TMPro_ExtensionMethods_t3178223924), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (TMP_Math_t871511886), -1, sizeof(TMP_Math_t871511886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2671[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TMP_Math_t871511886_StaticFields::get_offset_of_MAX_16BIT_6(),
	TMP_Math_t871511886_StaticFields::get_offset_of_MIN_16BIT_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (FaceInfo_t3239700425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[21] = 
{
	FaceInfo_t3239700425::get_offset_of_Name_0(),
	FaceInfo_t3239700425::get_offset_of_PointSize_1(),
	FaceInfo_t3239700425::get_offset_of_Scale_2(),
	FaceInfo_t3239700425::get_offset_of_CharacterCount_3(),
	FaceInfo_t3239700425::get_offset_of_LineHeight_4(),
	FaceInfo_t3239700425::get_offset_of_Baseline_5(),
	FaceInfo_t3239700425::get_offset_of_Ascender_6(),
	FaceInfo_t3239700425::get_offset_of_CapHeight_7(),
	FaceInfo_t3239700425::get_offset_of_Descender_8(),
	FaceInfo_t3239700425::get_offset_of_CenterLine_9(),
	FaceInfo_t3239700425::get_offset_of_SuperscriptOffset_10(),
	FaceInfo_t3239700425::get_offset_of_SubscriptOffset_11(),
	FaceInfo_t3239700425::get_offset_of_SubSize_12(),
	FaceInfo_t3239700425::get_offset_of_Underline_13(),
	FaceInfo_t3239700425::get_offset_of_UnderlineThickness_14(),
	FaceInfo_t3239700425::get_offset_of_strikethrough_15(),
	FaceInfo_t3239700425::get_offset_of_strikethroughThickness_16(),
	FaceInfo_t3239700425::get_offset_of_TabWidth_17(),
	FaceInfo_t3239700425::get_offset_of_Padding_18(),
	FaceInfo_t3239700425::get_offset_of_AtlasWidth_19(),
	FaceInfo_t3239700425::get_offset_of_AtlasHeight_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (TMP_Glyph_t909793902), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (FontCreationSetting_t1093397046)+ sizeof (RuntimeObject), sizeof(FontCreationSetting_t1093397046_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2674[12] = 
{
	FontCreationSetting_t1093397046::get_offset_of_fontSourcePath_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontSizingMode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontPadding_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontPackingMode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontAtlasWidth_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontAtlasHeight_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontCharacterSet_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontStyle_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontStlyeModifier_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontRenderMode_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FontCreationSetting_t1093397046::get_offset_of_fontKerning_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (KerningPairKey_t3915473665)+ sizeof (RuntimeObject), sizeof(KerningPairKey_t3915473665 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2675[3] = 
{
	KerningPairKey_t3915473665::get_offset_of_ascii_Left_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t3915473665::get_offset_of_ascii_Right_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	KerningPairKey_t3915473665::get_offset_of_key_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (GlyphValueRecord_t3876405180)+ sizeof (RuntimeObject), sizeof(GlyphValueRecord_t3876405180 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2676[4] = 
{
	GlyphValueRecord_t3876405180::get_offset_of_xPlacement_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t3876405180::get_offset_of_yPlacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t3876405180::get_offset_of_xAdvance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	GlyphValueRecord_t3876405180::get_offset_of_yAdvance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (KerningPair_t1577753922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2677[5] = 
{
	KerningPair_t1577753922::get_offset_of_m_FirstGlyph_0(),
	KerningPair_t1577753922::get_offset_of_m_FirstGlyphAdjustments_1(),
	KerningPair_t1577753922::get_offset_of_m_SecondGlyph_2(),
	KerningPair_t1577753922::get_offset_of_m_SecondGlyphAdjustments_3(),
	KerningPair_t1577753922::get_offset_of_xOffset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (KerningTable_t2970824110), -1, sizeof(KerningTable_t2970824110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2678[3] = 
{
	KerningTable_t2970824110::get_offset_of_kerningPairs_0(),
	KerningTable_t2970824110_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
	KerningTable_t2970824110_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (U3CAddKerningPairU3Ec__AnonStorey0_t2114388639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2679[2] = 
{
	U3CAddKerningPairU3Ec__AnonStorey0_t2114388639::get_offset_of_first_0(),
	U3CAddKerningPairU3Ec__AnonStorey0_t2114388639::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t2874277708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2680[2] = 
{
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t2874277708::get_offset_of_first_0(),
	U3CAddGlyphPairAdjustmentRecordU3Ec__AnonStorey1_t2874277708::get_offset_of_second_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (U3CRemoveKerningPairU3Ec__AnonStorey2_t702943500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[2] = 
{
	U3CRemoveKerningPairU3Ec__AnonStorey2_t702943500::get_offset_of_left_0(),
	U3CRemoveKerningPairU3Ec__AnonStorey2_t702943500::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (TMP_FontUtilities_t1411032097), -1, sizeof(TMP_FontUtilities_t1411032097_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2682[1] = 
{
	TMP_FontUtilities_t1411032097_StaticFields::get_offset_of_k_searchedFontAssets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (TMP_VertexDataUpdateFlags_t878817098)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2683[8] = 
{
	TMP_VertexDataUpdateFlags_t878817098::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (TMP_CharacterInfo_t1421302791)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2684[35] = 
{
	TMP_CharacterInfo_t1421302791::get_offset_of_character_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_index_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_elementType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_textElement_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_fontAsset_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_spriteAsset_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_spriteIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_material_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_materialReferenceIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_isUsingAlternateTypeface_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_pointSize_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_lineNumber_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_pageNumber_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_vertexIndex_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_vertex_TL_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_vertex_TR_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_vertex_BR_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_topLeft_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_bottomLeft_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_topRight_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_bottomRight_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_origin_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_ascender_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_baseLine_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_descender_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_xAdvance_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_aspectRatio_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_scale_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_color_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_style_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_CharacterInfo_t1421302791::get_offset_of_isVisible_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (TMP_Vertex_t1422870470)+ sizeof (RuntimeObject), sizeof(TMP_Vertex_t1422870470 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2685[5] = 
{
	TMP_Vertex_t1422870470::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t1422870470::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t1422870470::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t1422870470::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_Vertex_t1422870470::get_offset_of_color_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (VertexGradient_t1602386880)+ sizeof (RuntimeObject), sizeof(VertexGradient_t1602386880 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2686[4] = 
{
	VertexGradient_t1602386880::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t1602386880::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t1602386880::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_t1602386880::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (TMP_PageInfo_t3845132337)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t3845132337 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2687[5] = 
{
	TMP_PageInfo_t3845132337::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t3845132337::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t3845132337::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t3845132337::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t3845132337::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (TMP_LinkInfo_t3626894960)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[7] = 
{
	TMP_LinkInfo_t3626894960::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t3626894960::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t3626894960::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t3626894960::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t3626894960::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t3626894960::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t3626894960::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (TMP_WordInfo_t3807457612)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2689[4] = 
{
	TMP_WordInfo_t3807457612::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3807457612::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3807457612::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t3807457612::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (TMP_SpriteInfo_t124011303)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t124011303 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2690[3] = 
{
	TMP_SpriteInfo_t124011303::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t124011303::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t124011303::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (Extents_t3018556803)+ sizeof (RuntimeObject), sizeof(Extents_t3018556803 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2691[2] = 
{
	Extents_t3018556803::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_t3018556803::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (Mesh_Extents_t3515350687)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t3515350687 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2692[2] = 
{
	Mesh_Extents_t3515350687::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t3515350687::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (WordWrapState_t433984875)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2693[55] = 
{
	WordWrapState_t433984875::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_colorGradientStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_sizeStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_indentStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_fontWeightStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_styleStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_baselineStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_actionStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_materialReferenceStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_lineJustificationStack_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_spriteAnimationID_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_currentFontAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_currentSpriteAsset_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_currentMaterial_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_currentMaterialIndex_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_meshExtents_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_tagNoParsing_53() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t433984875::get_offset_of_isNonBreakingSpace_54() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (TagAttribute_t1488719162)+ sizeof (RuntimeObject), sizeof(TagAttribute_t1488719162 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2694[3] = 
{
	TagAttribute_t1488719162::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t1488719162::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t1488719162::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (XML_TagAttribute_t1879784992)+ sizeof (RuntimeObject), sizeof(XML_TagAttribute_t1879784992 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2695[5] = 
{
	XML_TagAttribute_t1879784992::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1879784992::get_offset_of_valueType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1879784992::get_offset_of_valueStartIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1879784992::get_offset_of_valueLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XML_TagAttribute_t1879784992::get_offset_of_valueHashCode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (ShaderUtilities_t2519527413), -1, sizeof(ShaderUtilities_t2519527413_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2696[59] = 
{
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_GradientScale_16(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_ScaleX_17(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_ScaleY_18(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_PerspectiveFilter_19(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_TextureWidth_20(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_TextureHeight_21(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_BevelAmount_22(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_GlowColor_23(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_GlowOffset_24(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_GlowPower_25(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_GlowOuter_26(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_LightAngle_27(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_EnvMap_28(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_EnvMatrix_29(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_EnvMatrixRotation_30(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_MaskCoord_31(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_ClipRect_32(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_MaskSoftnessX_33(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_MaskSoftnessY_34(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_VertexOffsetX_35(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_VertexOffsetY_36(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_UseClipRect_37(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_StencilID_38(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_StencilOp_39(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_StencilComp_40(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_StencilReadMask_41(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_StencilWriteMask_42(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_ShaderFlags_43(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_ScaleRatio_A_44(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_ScaleRatio_B_45(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ID_ScaleRatio_C_46(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_Keyword_Bevel_47(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_Keyword_Glow_48(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_Keyword_Underlay_49(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_Keyword_Ratios_50(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_Keyword_MASK_SOFT_51(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_Keyword_MASK_HARD_52(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_Keyword_MASK_TEX_53(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_Keyword_Outline_54(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ShaderTag_ZTestMode_55(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_ShaderTag_CullMode_56(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_m_clamp_57(),
	ShaderUtilities_t2519527413_StaticFields::get_offset_of_isInitialized_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (TMP_Asset_t1084708044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2697[3] = 
{
	TMP_Asset_t1084708044::get_offset_of_hashCode_2(),
	TMP_Asset_t1084708044::get_offset_of_material_3(),
	TMP_Asset_t1084708044::get_offset_of_materialHashCode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (TMP_ColorGradient_t1159837347), -1, sizeof(TMP_ColorGradient_t1159837347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2698[5] = 
{
	TMP_ColorGradient_t1159837347::get_offset_of_topLeft_2(),
	TMP_ColorGradient_t1159837347::get_offset_of_topRight_3(),
	TMP_ColorGradient_t1159837347::get_offset_of_bottomLeft_4(),
	TMP_ColorGradient_t1159837347::get_offset_of_bottomRight_5(),
	TMP_ColorGradient_t1159837347_StaticFields::get_offset_of_k_defaultColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (TMP_Compatibility_t3260547480), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
