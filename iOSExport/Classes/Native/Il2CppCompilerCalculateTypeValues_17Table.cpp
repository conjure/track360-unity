﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t313318308;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t290043810;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.String
struct String_t;
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t2962278982;
// System.UriParser
struct UriParser_t1012511323;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t1548133672;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t1578612233;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.String[]
struct StringU5BU5D_t1642385972;
// Mono.Xml.Schema.XmlSchemaUri
struct XmlSchemaUri_t1295878664;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2510243513;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_t3063656491;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;
// System.Xml.XmlNameTable
struct XmlNameTable_t1345805268;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t3364835593;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t2971213394;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t248156492;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t4015859774;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t2433337156;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// System.Xml.XmlNode[]
struct XmlNodeU5BU5D_t2118142256;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t395083109;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t880472818;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_t2400301303;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t287209776;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t1096449895;
// Mono.Xml.Schema.XsdString
struct XsdString_t263933896;
// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_t2132216291;
// Mono.Xml.Schema.XsdToken
struct XsdToken_t2902215462;
// Mono.Xml.Schema.XsdLanguage
struct XsdLanguage_t3897851897;
// Mono.Xml.Schema.XsdNMToken
struct XsdNMToken_t547135877;
// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_t1753890866;
// Mono.Xml.Schema.XsdName
struct XsdName_t1409945702;
// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t414304927;
// Mono.Xml.Schema.XsdID
struct XsdID_t1067193160;
// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_t1377311049;
// Mono.Xml.Schema.XsdIDRefs
struct XsdIDRefs_t763119546;
// Mono.Xml.Schema.XsdEntity
struct XsdEntity_t2664305022;
// Mono.Xml.Schema.XsdEntities
struct XsdEntities_t1103053540;
// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t720379093;
// Mono.Xml.Schema.XsdDecimal
struct XsdDecimal_t2932251550;
// Mono.Xml.Schema.XsdInteger
struct XsdInteger_t1330502157;
// Mono.Xml.Schema.XsdLong
struct XsdLong_t4179235589;
// Mono.Xml.Schema.XsdInt
struct XsdInt_t1488443894;
// Mono.Xml.Schema.XsdShort
struct XsdShort_t1778530041;
// Mono.Xml.Schema.XsdByte
struct XsdByte_t1120972221;
// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t3587933853;
// Mono.Xml.Schema.XsdPositiveInteger
struct XsdPositiveInteger_t1896481288;
// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t137890294;
// Mono.Xml.Schema.XsdUnsignedInt
struct XsdUnsignedInt_t2956447959;
// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_t3693774826;
// Mono.Xml.Schema.XsdUnsignedByte
struct XsdUnsignedByte_t3216355454;
// Mono.Xml.Schema.XsdNonPositiveInteger
struct XsdNonPositiveInteger_t409343009;
// Mono.Xml.Schema.XsdNegativeInteger
struct XsdNegativeInteger_t399444136;
// Mono.Xml.Schema.XsdFloat
struct XsdFloat_t386143221;
// Mono.Xml.Schema.XsdDouble
struct XsdDouble_t2510112208;
// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t1094704629;
// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_t4126353587;
// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t2527983239;
// Mono.Xml.Schema.XsdDuration
struct XsdDuration_t1605638443;
// Mono.Xml.Schema.XsdDateTime
struct XsdDateTime_t1344468684;
// Mono.Xml.Schema.XsdDate
struct XsdDate_t919459387;
// Mono.Xml.Schema.XsdTime
struct XsdTime_t4165680512;
// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t3496718151;
// Mono.Xml.Schema.XsdQName
struct XsdQName_t930779123;
// Mono.Xml.Schema.XsdGYearMonth
struct XsdGYearMonth_t1363357165;
// Mono.Xml.Schema.XsdGMonthDay
struct XsdGMonthDay_t3859978294;
// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t3810382607;
// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t4076673358;
// Mono.Xml.Schema.XsdGDay
struct XsdGDay_t1914244270;
// Mono.Xml.Schema.XdtAnyAtomicType
struct XdtAnyAtomicType_t3359210273;
// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t2904699188;
// Mono.Xml.Schema.XdtDayTimeDuration
struct XdtDayTimeDuration_t2797717973;
// Mono.Xml.Schema.XdtYearMonthDuration
struct XdtYearMonthDuration_t1764328599;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Xml.Schema.XmlSchemaException
struct XmlSchemaException_t4082200141;
// System.Xml.Schema.XmlSchemaXPath
struct XmlSchemaXPath_t604820427;
// Mono.Xml.Schema.XsdIdentitySelector
struct XsdIdentitySelector_t185499482;
// Mono.Xml.Schema.XsdWildcard
struct XsdWildcard_t625524157;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t1195946242;
// System.Xml.Schema.XmlSchemaGroupBase
struct XmlSchemaGroupBase_t3811767860;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t530453212;
// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct XmlSchemaSimpleTypeContent_t1606103299;
// System.Text.RegularExpressions.Regex[]
struct RegexU5BU5D_t3677892936;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t717347117;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Xml.Schema.XmlSchemaSimpleType[]
struct XmlSchemaSimpleTypeU5BU5D_t192177157;
// System.Xml.Schema.XmlSchemaContent
struct XmlSchemaContent_t3733871217;
// System.Xml.Schema.XmlSchemaContentModel
struct XmlSchemaContentModel_t907989596;
// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_t3365045970;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_t1058613623;
// System.Xml.Schema.XmlSchemaGroup
struct XmlSchemaGroup_t4189650927;
// System.Byte[]
struct ByteU5BU5D_t3397334013;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef XMLSCHEMACOLLECTION_T3518500204_H
#define XMLSCHEMACOLLECTION_T3518500204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCollection
struct  XmlSchemaCollection_t3518500204  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaSet System.Xml.Schema.XmlSchemaCollection::schemaSet
	XmlSchemaSet_t313318308 * ___schemaSet_0;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaCollection::ValidationEventHandler
	ValidationEventHandler_t1580700381 * ___ValidationEventHandler_1;

public:
	inline static int32_t get_offset_of_schemaSet_0() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___schemaSet_0)); }
	inline XmlSchemaSet_t313318308 * get_schemaSet_0() const { return ___schemaSet_0; }
	inline XmlSchemaSet_t313318308 ** get_address_of_schemaSet_0() { return &___schemaSet_0; }
	inline void set_schemaSet_0(XmlSchemaSet_t313318308 * value)
	{
		___schemaSet_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaSet_0), value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_1() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t3518500204, ___ValidationEventHandler_1)); }
	inline ValidationEventHandler_t1580700381 * get_ValidationEventHandler_1() const { return ___ValidationEventHandler_1; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_ValidationEventHandler_1() { return &___ValidationEventHandler_1; }
	inline void set_ValidationEventHandler_1(ValidationEventHandler_t1580700381 * value)
	{
		___ValidationEventHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOLLECTION_T3518500204_H
#ifndef VALIDATIONHANDLER_T3342756761_H
#define VALIDATIONHANDLER_T3342756761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationHandler
struct  ValidationHandler_t3342756761  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONHANDLER_T3342756761_H
#ifndef XPATHITEM_T3130801258_H
#define XPATHITEM_T3130801258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XPath.XPathItem
struct  XPathItem_t3130801258  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XPATHITEM_T3130801258_H
#ifndef XMLSCHEMACOLLECTIONENUMERATOR_T1538181312_H
#define XMLSCHEMACOLLECTIONENUMERATOR_T1538181312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCollectionEnumerator
struct  XmlSchemaCollectionEnumerator_t1538181312  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Xml.Schema.XmlSchemaCollectionEnumerator::xenum
	RuntimeObject* ___xenum_0;

public:
	inline static int32_t get_offset_of_xenum_0() { return static_cast<int32_t>(offsetof(XmlSchemaCollectionEnumerator_t1538181312, ___xenum_0)); }
	inline RuntimeObject* get_xenum_0() const { return ___xenum_0; }
	inline RuntimeObject** get_address_of_xenum_0() { return &___xenum_0; }
	inline void set_xenum_0(RuntimeObject* value)
	{
		___xenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___xenum_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOLLECTIONENUMERATOR_T1538181312_H
#ifndef XMLSCHEMAOBJECTENUMERATOR_T2354997415_H
#define XMLSCHEMAOBJECTENUMERATOR_T2354997415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectEnumerator
struct  XmlSchemaObjectEnumerator_t2354997415  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Xml.Schema.XmlSchemaObjectEnumerator::ienum
	RuntimeObject* ___ienum_0;

public:
	inline static int32_t get_offset_of_ienum_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectEnumerator_t2354997415, ___ienum_0)); }
	inline RuntimeObject* get_ienum_0() const { return ___ienum_0; }
	inline RuntimeObject** get_address_of_ienum_0() { return &___ienum_0; }
	inline void set_ienum_0(RuntimeObject* value)
	{
		___ienum_0 = value;
		Il2CppCodeGenWriteBarrier((&___ienum_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTENUMERATOR_T2354997415_H
#ifndef XMLSCHEMAOBJECTTABLE_T3364835593_H
#define XMLSCHEMAOBJECTTABLE_T3364835593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable
struct  XmlSchemaObjectTable_t3364835593  : public RuntimeObject
{
public:
	// System.Collections.Specialized.HybridDictionary System.Xml.Schema.XmlSchemaObjectTable::table
	HybridDictionary_t290043810 * ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTable_t3364835593, ___table_0)); }
	inline HybridDictionary_t290043810 * get_table_0() const { return ___table_0; }
	inline HybridDictionary_t290043810 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(HybridDictionary_t290043810 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTTABLE_T3364835593_H
#ifndef XMLSCHEMAOBJECTTABLEENUMERATOR_T2908487920_H
#define XMLSCHEMAOBJECTTABLEENUMERATOR_T2908487920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectTableEnumerator
struct  XmlSchemaObjectTableEnumerator_t2908487920  : public RuntimeObject
{
public:
	// System.Collections.IDictionaryEnumerator System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectTableEnumerator::xenum
	RuntimeObject* ___xenum_0;
	// System.Collections.IEnumerable System.Xml.Schema.XmlSchemaObjectTable/XmlSchemaObjectTableEnumerator::tmp
	RuntimeObject* ___tmp_1;

public:
	inline static int32_t get_offset_of_xenum_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTableEnumerator_t2908487920, ___xenum_0)); }
	inline RuntimeObject* get_xenum_0() const { return ___xenum_0; }
	inline RuntimeObject** get_address_of_xenum_0() { return &___xenum_0; }
	inline void set_xenum_0(RuntimeObject* value)
	{
		___xenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___xenum_0), value);
	}

	inline static int32_t get_offset_of_tmp_1() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTableEnumerator_t2908487920, ___tmp_1)); }
	inline RuntimeObject* get_tmp_1() const { return ___tmp_1; }
	inline RuntimeObject** get_address_of_tmp_1() { return &___tmp_1; }
	inline void set_tmp_1(RuntimeObject* value)
	{
		___tmp_1 = value;
		Il2CppCodeGenWriteBarrier((&___tmp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTTABLEENUMERATOR_T2908487920_H
#ifndef XMLSCHEMACOMPILATIONSETTINGS_T2971213394_H
#define XMLSCHEMACOMPILATIONSETTINGS_T2971213394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCompilationSettings
struct  XmlSchemaCompilationSettings_t2971213394  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaCompilationSettings::enable_upa_check
	bool ___enable_upa_check_0;

public:
	inline static int32_t get_offset_of_enable_upa_check_0() { return static_cast<int32_t>(offsetof(XmlSchemaCompilationSettings_t2971213394, ___enable_upa_check_0)); }
	inline bool get_enable_upa_check_0() const { return ___enable_upa_check_0; }
	inline bool* get_address_of_enable_upa_check_0() { return &___enable_upa_check_0; }
	inline void set_enable_upa_check_0(bool value)
	{
		___enable_upa_check_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPILATIONSETTINGS_T2971213394_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef URI_T19570940_H
#define URI_T19570940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t19570940  : public RuntimeObject
{
public:
	// System.Boolean System.Uri::isUnixFilePath
	bool ___isUnixFilePath_0;
	// System.String System.Uri::source
	String_t* ___source_1;
	// System.String System.Uri::scheme
	String_t* ___scheme_2;
	// System.String System.Uri::host
	String_t* ___host_3;
	// System.Int32 System.Uri::port
	int32_t ___port_4;
	// System.String System.Uri::path
	String_t* ___path_5;
	// System.String System.Uri::query
	String_t* ___query_6;
	// System.String System.Uri::fragment
	String_t* ___fragment_7;
	// System.String System.Uri::userinfo
	String_t* ___userinfo_8;
	// System.Boolean System.Uri::isUnc
	bool ___isUnc_9;
	// System.Boolean System.Uri::isOpaquePart
	bool ___isOpaquePart_10;
	// System.Boolean System.Uri::isAbsoluteUri
	bool ___isAbsoluteUri_11;
	// System.Boolean System.Uri::userEscaped
	bool ___userEscaped_12;
	// System.String System.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_13;
	// System.String System.Uri::cachedToString
	String_t* ___cachedToString_14;
	// System.String System.Uri::cachedLocalPath
	String_t* ___cachedLocalPath_15;
	// System.Int32 System.Uri::cachedHashCode
	int32_t ___cachedHashCode_16;
	// System.UriParser System.Uri::parser
	UriParser_t1012511323 * ___parser_30;

public:
	inline static int32_t get_offset_of_isUnixFilePath_0() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___isUnixFilePath_0)); }
	inline bool get_isUnixFilePath_0() const { return ___isUnixFilePath_0; }
	inline bool* get_address_of_isUnixFilePath_0() { return &___isUnixFilePath_0; }
	inline void set_isUnixFilePath_0(bool value)
	{
		___isUnixFilePath_0 = value;
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___source_1)); }
	inline String_t* get_source_1() const { return ___source_1; }
	inline String_t** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(String_t* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_scheme_2() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___scheme_2)); }
	inline String_t* get_scheme_2() const { return ___scheme_2; }
	inline String_t** get_address_of_scheme_2() { return &___scheme_2; }
	inline void set_scheme_2(String_t* value)
	{
		___scheme_2 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_2), value);
	}

	inline static int32_t get_offset_of_host_3() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___host_3)); }
	inline String_t* get_host_3() const { return ___host_3; }
	inline String_t** get_address_of_host_3() { return &___host_3; }
	inline void set_host_3(String_t* value)
	{
		___host_3 = value;
		Il2CppCodeGenWriteBarrier((&___host_3), value);
	}

	inline static int32_t get_offset_of_port_4() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___port_4)); }
	inline int32_t get_port_4() const { return ___port_4; }
	inline int32_t* get_address_of_port_4() { return &___port_4; }
	inline void set_port_4(int32_t value)
	{
		___port_4 = value;
	}

	inline static int32_t get_offset_of_path_5() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___path_5)); }
	inline String_t* get_path_5() const { return ___path_5; }
	inline String_t** get_address_of_path_5() { return &___path_5; }
	inline void set_path_5(String_t* value)
	{
		___path_5 = value;
		Il2CppCodeGenWriteBarrier((&___path_5), value);
	}

	inline static int32_t get_offset_of_query_6() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___query_6)); }
	inline String_t* get_query_6() const { return ___query_6; }
	inline String_t** get_address_of_query_6() { return &___query_6; }
	inline void set_query_6(String_t* value)
	{
		___query_6 = value;
		Il2CppCodeGenWriteBarrier((&___query_6), value);
	}

	inline static int32_t get_offset_of_fragment_7() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___fragment_7)); }
	inline String_t* get_fragment_7() const { return ___fragment_7; }
	inline String_t** get_address_of_fragment_7() { return &___fragment_7; }
	inline void set_fragment_7(String_t* value)
	{
		___fragment_7 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_7), value);
	}

	inline static int32_t get_offset_of_userinfo_8() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___userinfo_8)); }
	inline String_t* get_userinfo_8() const { return ___userinfo_8; }
	inline String_t** get_address_of_userinfo_8() { return &___userinfo_8; }
	inline void set_userinfo_8(String_t* value)
	{
		___userinfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___userinfo_8), value);
	}

	inline static int32_t get_offset_of_isUnc_9() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___isUnc_9)); }
	inline bool get_isUnc_9() const { return ___isUnc_9; }
	inline bool* get_address_of_isUnc_9() { return &___isUnc_9; }
	inline void set_isUnc_9(bool value)
	{
		___isUnc_9 = value;
	}

	inline static int32_t get_offset_of_isOpaquePart_10() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___isOpaquePart_10)); }
	inline bool get_isOpaquePart_10() const { return ___isOpaquePart_10; }
	inline bool* get_address_of_isOpaquePart_10() { return &___isOpaquePart_10; }
	inline void set_isOpaquePart_10(bool value)
	{
		___isOpaquePart_10 = value;
	}

	inline static int32_t get_offset_of_isAbsoluteUri_11() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___isAbsoluteUri_11)); }
	inline bool get_isAbsoluteUri_11() const { return ___isAbsoluteUri_11; }
	inline bool* get_address_of_isAbsoluteUri_11() { return &___isAbsoluteUri_11; }
	inline void set_isAbsoluteUri_11(bool value)
	{
		___isAbsoluteUri_11 = value;
	}

	inline static int32_t get_offset_of_userEscaped_12() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___userEscaped_12)); }
	inline bool get_userEscaped_12() const { return ___userEscaped_12; }
	inline bool* get_address_of_userEscaped_12() { return &___userEscaped_12; }
	inline void set_userEscaped_12(bool value)
	{
		___userEscaped_12 = value;
	}

	inline static int32_t get_offset_of_cachedAbsoluteUri_13() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___cachedAbsoluteUri_13)); }
	inline String_t* get_cachedAbsoluteUri_13() const { return ___cachedAbsoluteUri_13; }
	inline String_t** get_address_of_cachedAbsoluteUri_13() { return &___cachedAbsoluteUri_13; }
	inline void set_cachedAbsoluteUri_13(String_t* value)
	{
		___cachedAbsoluteUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAbsoluteUri_13), value);
	}

	inline static int32_t get_offset_of_cachedToString_14() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___cachedToString_14)); }
	inline String_t* get_cachedToString_14() const { return ___cachedToString_14; }
	inline String_t** get_address_of_cachedToString_14() { return &___cachedToString_14; }
	inline void set_cachedToString_14(String_t* value)
	{
		___cachedToString_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedToString_14), value);
	}

	inline static int32_t get_offset_of_cachedLocalPath_15() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___cachedLocalPath_15)); }
	inline String_t* get_cachedLocalPath_15() const { return ___cachedLocalPath_15; }
	inline String_t** get_address_of_cachedLocalPath_15() { return &___cachedLocalPath_15; }
	inline void set_cachedLocalPath_15(String_t* value)
	{
		___cachedLocalPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedLocalPath_15), value);
	}

	inline static int32_t get_offset_of_cachedHashCode_16() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___cachedHashCode_16)); }
	inline int32_t get_cachedHashCode_16() const { return ___cachedHashCode_16; }
	inline int32_t* get_address_of_cachedHashCode_16() { return &___cachedHashCode_16; }
	inline void set_cachedHashCode_16(int32_t value)
	{
		___cachedHashCode_16 = value;
	}

	inline static int32_t get_offset_of_parser_30() { return static_cast<int32_t>(offsetof(Uri_t19570940, ___parser_30)); }
	inline UriParser_t1012511323 * get_parser_30() const { return ___parser_30; }
	inline UriParser_t1012511323 ** get_address_of_parser_30() { return &___parser_30; }
	inline void set_parser_30(UriParser_t1012511323 * value)
	{
		___parser_30 = value;
		Il2CppCodeGenWriteBarrier((&___parser_30), value);
	}
};

struct Uri_t19570940_StaticFields
{
public:
	// System.String System.Uri::hexUpperChars
	String_t* ___hexUpperChars_17;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_18;
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_19;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_20;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_21;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_22;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_23;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_24;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_25;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_26;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_27;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_28;
	// System.Uri/UriScheme[] System.Uri::schemes
	UriSchemeU5BU5D_t2962278982* ___schemes_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map12
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map12_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map13
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map13_32;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map14
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map14_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map15
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map15_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map16
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map16_35;

public:
	inline static int32_t get_offset_of_hexUpperChars_17() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___hexUpperChars_17)); }
	inline String_t* get_hexUpperChars_17() const { return ___hexUpperChars_17; }
	inline String_t** get_address_of_hexUpperChars_17() { return &___hexUpperChars_17; }
	inline void set_hexUpperChars_17(String_t* value)
	{
		___hexUpperChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___hexUpperChars_17), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_18() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___SchemeDelimiter_18)); }
	inline String_t* get_SchemeDelimiter_18() const { return ___SchemeDelimiter_18; }
	inline String_t** get_address_of_SchemeDelimiter_18() { return &___SchemeDelimiter_18; }
	inline void set_SchemeDelimiter_18(String_t* value)
	{
		___SchemeDelimiter_18 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_18), value);
	}

	inline static int32_t get_offset_of_UriSchemeFile_19() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeFile_19)); }
	inline String_t* get_UriSchemeFile_19() const { return ___UriSchemeFile_19; }
	inline String_t** get_address_of_UriSchemeFile_19() { return &___UriSchemeFile_19; }
	inline void set_UriSchemeFile_19(String_t* value)
	{
		___UriSchemeFile_19 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_19), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_20() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeFtp_20)); }
	inline String_t* get_UriSchemeFtp_20() const { return ___UriSchemeFtp_20; }
	inline String_t** get_address_of_UriSchemeFtp_20() { return &___UriSchemeFtp_20; }
	inline void set_UriSchemeFtp_20(String_t* value)
	{
		___UriSchemeFtp_20 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_20), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_21() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeGopher_21)); }
	inline String_t* get_UriSchemeGopher_21() const { return ___UriSchemeGopher_21; }
	inline String_t** get_address_of_UriSchemeGopher_21() { return &___UriSchemeGopher_21; }
	inline void set_UriSchemeGopher_21(String_t* value)
	{
		___UriSchemeGopher_21 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_21), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_22() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeHttp_22)); }
	inline String_t* get_UriSchemeHttp_22() const { return ___UriSchemeHttp_22; }
	inline String_t** get_address_of_UriSchemeHttp_22() { return &___UriSchemeHttp_22; }
	inline void set_UriSchemeHttp_22(String_t* value)
	{
		___UriSchemeHttp_22 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_22), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_23() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeHttps_23)); }
	inline String_t* get_UriSchemeHttps_23() const { return ___UriSchemeHttps_23; }
	inline String_t** get_address_of_UriSchemeHttps_23() { return &___UriSchemeHttps_23; }
	inline void set_UriSchemeHttps_23(String_t* value)
	{
		___UriSchemeHttps_23 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_23), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_24() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeMailto_24)); }
	inline String_t* get_UriSchemeMailto_24() const { return ___UriSchemeMailto_24; }
	inline String_t** get_address_of_UriSchemeMailto_24() { return &___UriSchemeMailto_24; }
	inline void set_UriSchemeMailto_24(String_t* value)
	{
		___UriSchemeMailto_24 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_24), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_25() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeNews_25)); }
	inline String_t* get_UriSchemeNews_25() const { return ___UriSchemeNews_25; }
	inline String_t** get_address_of_UriSchemeNews_25() { return &___UriSchemeNews_25; }
	inline void set_UriSchemeNews_25(String_t* value)
	{
		___UriSchemeNews_25 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_25), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_26() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeNntp_26)); }
	inline String_t* get_UriSchemeNntp_26() const { return ___UriSchemeNntp_26; }
	inline String_t** get_address_of_UriSchemeNntp_26() { return &___UriSchemeNntp_26; }
	inline void set_UriSchemeNntp_26(String_t* value)
	{
		___UriSchemeNntp_26 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_26), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_27() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeNetPipe_27)); }
	inline String_t* get_UriSchemeNetPipe_27() const { return ___UriSchemeNetPipe_27; }
	inline String_t** get_address_of_UriSchemeNetPipe_27() { return &___UriSchemeNetPipe_27; }
	inline void set_UriSchemeNetPipe_27(String_t* value)
	{
		___UriSchemeNetPipe_27 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_27), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_28() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___UriSchemeNetTcp_28)); }
	inline String_t* get_UriSchemeNetTcp_28() const { return ___UriSchemeNetTcp_28; }
	inline String_t** get_address_of_UriSchemeNetTcp_28() { return &___UriSchemeNetTcp_28; }
	inline void set_UriSchemeNetTcp_28(String_t* value)
	{
		___UriSchemeNetTcp_28 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_28), value);
	}

	inline static int32_t get_offset_of_schemes_29() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___schemes_29)); }
	inline UriSchemeU5BU5D_t2962278982* get_schemes_29() const { return ___schemes_29; }
	inline UriSchemeU5BU5D_t2962278982** get_address_of_schemes_29() { return &___schemes_29; }
	inline void set_schemes_29(UriSchemeU5BU5D_t2962278982* value)
	{
		___schemes_29 = value;
		Il2CppCodeGenWriteBarrier((&___schemes_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map12_31() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___U3CU3Ef__switchU24map12_31)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map12_31() const { return ___U3CU3Ef__switchU24map12_31; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map12_31() { return &___U3CU3Ef__switchU24map12_31; }
	inline void set_U3CU3Ef__switchU24map12_31(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map12_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map12_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_32() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___U3CU3Ef__switchU24map13_32)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map13_32() const { return ___U3CU3Ef__switchU24map13_32; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map13_32() { return &___U3CU3Ef__switchU24map13_32; }
	inline void set_U3CU3Ef__switchU24map13_32(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map13_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map13_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map14_33() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___U3CU3Ef__switchU24map14_33)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map14_33() const { return ___U3CU3Ef__switchU24map14_33; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map14_33() { return &___U3CU3Ef__switchU24map14_33; }
	inline void set_U3CU3Ef__switchU24map14_33(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map14_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map14_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_34() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___U3CU3Ef__switchU24map15_34)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map15_34() const { return ___U3CU3Ef__switchU24map15_34; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map15_34() { return &___U3CU3Ef__switchU24map15_34; }
	inline void set_U3CU3Ef__switchU24map15_34(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map15_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map15_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map16_35() { return static_cast<int32_t>(offsetof(Uri_t19570940_StaticFields, ___U3CU3Ef__switchU24map16_35)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map16_35() const { return ___U3CU3Ef__switchU24map16_35; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map16_35() { return &___U3CU3Ef__switchU24map16_35; }
	inline void set_U3CU3Ef__switchU24map16_35(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map16_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map16_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T19570940_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef XMLREADER_T3675626668_H
#define XMLREADER_T3675626668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3675626668  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t1548133672 * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t1578612233 * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_t3675626668, ___binary_0)); }
	inline XmlReaderBinarySupport_t1548133672 * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_t1548133672 ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_t1548133672 * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_t3675626668, ___settings_1)); }
	inline XmlReaderSettings_t1578612233 * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_t1578612233 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_t1578612233 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3675626668_H
#ifndef COLLECTIONBASE_T1101587467_H
#define COLLECTIONBASE_T1101587467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_t1101587467  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t4252133567 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_t1101587467, ___list_0)); }
	inline ArrayList_t4252133567 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4252133567 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4252133567 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_T1101587467_H
#ifndef XMLSCHEMAREADER_T3786681597_H
#define XMLSCHEMAREADER_T3786681597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaReader
struct  XmlSchemaReader_t3786681597  : public XmlReader_t3675626668
{
public:
	// System.Xml.XmlReader System.Xml.Schema.XmlSchemaReader::reader
	XmlReader_t3675626668 * ___reader_2;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaReader::handler
	ValidationEventHandler_t1580700381 * ___handler_3;
	// System.Boolean System.Xml.Schema.XmlSchemaReader::hasLineInfo
	bool ___hasLineInfo_4;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(XmlSchemaReader_t3786681597, ___reader_2)); }
	inline XmlReader_t3675626668 * get_reader_2() const { return ___reader_2; }
	inline XmlReader_t3675626668 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(XmlReader_t3675626668 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_handler_3() { return static_cast<int32_t>(offsetof(XmlSchemaReader_t3786681597, ___handler_3)); }
	inline ValidationEventHandler_t1580700381 * get_handler_3() const { return ___handler_3; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_handler_3() { return &___handler_3; }
	inline void set_handler_3(ValidationEventHandler_t1580700381 * value)
	{
		___handler_3 = value;
		Il2CppCodeGenWriteBarrier((&___handler_3), value);
	}

	inline static int32_t get_offset_of_hasLineInfo_4() { return static_cast<int32_t>(offsetof(XmlSchemaReader_t3786681597, ___hasLineInfo_4)); }
	inline bool get_hasLineInfo_4() const { return ___hasLineInfo_4; }
	inline bool* get_address_of_hasLineInfo_4() { return &___hasLineInfo_4; }
	inline void set_hasLineInfo_4(bool value)
	{
		___hasLineInfo_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAREADER_T3786681597_H
#ifndef STRINGARRAYVALUETYPE_T1731700877_H
#define STRINGARRAYVALUETYPE_T1731700877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StringArrayValueType
struct  StringArrayValueType_t1731700877 
{
public:
	// System.String[] System.Xml.Schema.StringArrayValueType::value
	StringU5BU5D_t1642385972* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(StringArrayValueType_t1731700877, ___value_0)); }
	inline StringU5BU5D_t1642385972* get_value_0() const { return ___value_0; }
	inline StringU5BU5D_t1642385972** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(StringU5BU5D_t1642385972* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.StringArrayValueType
struct StringArrayValueType_t1731700877_marshaled_pinvoke
{
	char** ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.StringArrayValueType
struct StringArrayValueType_t1731700877_marshaled_com
{
	Il2CppChar** ___value_0;
};
#endif // STRINGARRAYVALUETYPE_T1731700877_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef XMLSCHEMAURI_T1295878664_H
#define XMLSCHEMAURI_T1295878664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaUri
struct  XmlSchemaUri_t1295878664  : public Uri_t19570940
{
public:
	// System.String Mono.Xml.Schema.XmlSchemaUri::value
	String_t* ___value_36;

public:
	inline static int32_t get_offset_of_value_36() { return static_cast<int32_t>(offsetof(XmlSchemaUri_t1295878664, ___value_36)); }
	inline String_t* get_value_36() const { return ___value_36; }
	inline String_t** get_address_of_value_36() { return &___value_36; }
	inline void set_value_36(String_t* value)
	{
		___value_36 = value;
		Il2CppCodeGenWriteBarrier((&___value_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAURI_T1295878664_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef URIVALUETYPE_T1626089757_H
#define URIVALUETYPE_T1626089757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.UriValueType
struct  UriValueType_t1626089757 
{
public:
	// Mono.Xml.Schema.XmlSchemaUri System.Xml.Schema.UriValueType::value
	XmlSchemaUri_t1295878664 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(UriValueType_t1626089757, ___value_0)); }
	inline XmlSchemaUri_t1295878664 * get_value_0() const { return ___value_0; }
	inline XmlSchemaUri_t1295878664 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(XmlSchemaUri_t1295878664 * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.UriValueType
struct UriValueType_t1626089757_marshaled_pinvoke
{
	XmlSchemaUri_t1295878664 * ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.UriValueType
struct UriValueType_t1626089757_marshaled_com
{
	XmlSchemaUri_t1295878664 * ___value_0;
};
#endif // URIVALUETYPE_T1626089757_H
#ifndef XMLSCHEMAOBJECTCOLLECTION_T395083109_H
#define XMLSCHEMAOBJECTCOLLECTION_T395083109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectCollection
struct  XmlSchemaObjectCollection_t395083109  : public CollectionBase_t1101587467
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTCOLLECTION_T395083109_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2510243513 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2510243513 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2510243513 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2510243513 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef DECIMAL_T724701077_H
#define DECIMAL_T724701077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t724701077 
{
public:
	// System.UInt32 System.Decimal::flags
	uint32_t ___flags_5;
	// System.UInt32 System.Decimal::hi
	uint32_t ___hi_6;
	// System.UInt32 System.Decimal::lo
	uint32_t ___lo_7;
	// System.UInt32 System.Decimal::mid
	uint32_t ___mid_8;

public:
	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___flags_5)); }
	inline uint32_t get_flags_5() const { return ___flags_5; }
	inline uint32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(uint32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_hi_6() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___hi_6)); }
	inline uint32_t get_hi_6() const { return ___hi_6; }
	inline uint32_t* get_address_of_hi_6() { return &___hi_6; }
	inline void set_hi_6(uint32_t value)
	{
		___hi_6 = value;
	}

	inline static int32_t get_offset_of_lo_7() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___lo_7)); }
	inline uint32_t get_lo_7() const { return ___lo_7; }
	inline uint32_t* get_address_of_lo_7() { return &___lo_7; }
	inline void set_lo_7(uint32_t value)
	{
		___lo_7 = value;
	}

	inline static int32_t get_offset_of_mid_8() { return static_cast<int32_t>(offsetof(Decimal_t724701077, ___mid_8)); }
	inline uint32_t get_mid_8() const { return ___mid_8; }
	inline uint32_t* get_address_of_mid_8() { return &___mid_8; }
	inline void set_mid_8(uint32_t value)
	{
		___mid_8 = value;
	}
};

struct Decimal_t724701077_StaticFields
{
public:
	// System.Decimal System.Decimal::MinValue
	Decimal_t724701077  ___MinValue_0;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t724701077  ___MaxValue_1;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t724701077  ___MinusOne_2;
	// System.Decimal System.Decimal::One
	Decimal_t724701077  ___One_3;
	// System.Decimal System.Decimal::MaxValueDiv10
	Decimal_t724701077  ___MaxValueDiv10_4;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MinValue_0)); }
	inline Decimal_t724701077  get_MinValue_0() const { return ___MinValue_0; }
	inline Decimal_t724701077 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(Decimal_t724701077  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MaxValue_1)); }
	inline Decimal_t724701077  get_MaxValue_1() const { return ___MaxValue_1; }
	inline Decimal_t724701077 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(Decimal_t724701077  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinusOne_2() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MinusOne_2)); }
	inline Decimal_t724701077  get_MinusOne_2() const { return ___MinusOne_2; }
	inline Decimal_t724701077 * get_address_of_MinusOne_2() { return &___MinusOne_2; }
	inline void set_MinusOne_2(Decimal_t724701077  value)
	{
		___MinusOne_2 = value;
	}

	inline static int32_t get_offset_of_One_3() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___One_3)); }
	inline Decimal_t724701077  get_One_3() const { return ___One_3; }
	inline Decimal_t724701077 * get_address_of_One_3() { return &___One_3; }
	inline void set_One_3(Decimal_t724701077  value)
	{
		___One_3 = value;
	}

	inline static int32_t get_offset_of_MaxValueDiv10_4() { return static_cast<int32_t>(offsetof(Decimal_t724701077_StaticFields, ___MaxValueDiv10_4)); }
	inline Decimal_t724701077  get_MaxValueDiv10_4() const { return ___MaxValueDiv10_4; }
	inline Decimal_t724701077 * get_address_of_MaxValueDiv10_4() { return &___MaxValueDiv10_4; }
	inline void set_MaxValueDiv10_4(Decimal_t724701077  value)
	{
		___MaxValueDiv10_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T724701077_H
#ifndef STRINGVALUETYPE_T652312500_H
#define STRINGVALUETYPE_T652312500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StringValueType
struct  StringValueType_t652312500 
{
public:
	// System.String System.Xml.Schema.StringValueType::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(StringValueType_t652312500, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.StringValueType
struct StringValueType_t652312500_marshaled_pinvoke
{
	char* ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.StringValueType
struct StringValueType_t652312500_marshaled_com
{
	Il2CppChar* ___value_0;
};
#endif // STRINGVALUETYPE_T652312500_H
#ifndef QNAMEVALUETYPE_T2109511131_H
#define QNAMEVALUETYPE_T2109511131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.QNameValueType
struct  QNameValueType_t2109511131 
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.QNameValueType::value
	XmlQualifiedName_t1944712516 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(QNameValueType_t2109511131, ___value_0)); }
	inline XmlQualifiedName_t1944712516 * get_value_0() const { return ___value_0; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(XmlQualifiedName_t1944712516 * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.QNameValueType
struct QNameValueType_t2109511131_marshaled_pinvoke
{
	XmlQualifiedName_t1944712516 * ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.QNameValueType
struct QNameValueType_t2109511131_marshaled_com
{
	XmlQualifiedName_t1944712516 * ___value_0;
};
#endif // QNAMEVALUETYPE_T2109511131_H
#ifndef XMLSCHEMAOBJECT_T2050913741_H
#define XMLSCHEMAOBJECT_T2050913741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObject
struct  XmlSchemaObject_t2050913741  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaObject::lineNumber
	int32_t ___lineNumber_0;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::linePosition
	int32_t ___linePosition_1;
	// System.String System.Xml.Schema.XmlSchemaObject::sourceUri
	String_t* ___sourceUri_2;
	// System.Xml.Serialization.XmlSerializerNamespaces System.Xml.Schema.XmlSchemaObject::namespaces
	XmlSerializerNamespaces_t3063656491 * ___namespaces_3;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaObject::unhandledAttributeList
	ArrayList_t4252133567 * ___unhandledAttributeList_4;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isCompiled
	bool ___isCompiled_5;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::errorCount
	int32_t ___errorCount_6;
	// System.Guid System.Xml.Schema.XmlSchemaObject::CompilationId
	Guid_t  ___CompilationId_7;
	// System.Guid System.Xml.Schema.XmlSchemaObject::ValidationId
	Guid_t  ___ValidationId_8;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isRedefineChild
	bool ___isRedefineChild_9;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isRedefinedComponent
	bool ___isRedefinedComponent_10;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::redefinedObject
	XmlSchemaObject_t2050913741 * ___redefinedObject_11;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::parent
	XmlSchemaObject_t2050913741 * ___parent_12;

public:
	inline static int32_t get_offset_of_lineNumber_0() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___lineNumber_0)); }
	inline int32_t get_lineNumber_0() const { return ___lineNumber_0; }
	inline int32_t* get_address_of_lineNumber_0() { return &___lineNumber_0; }
	inline void set_lineNumber_0(int32_t value)
	{
		___lineNumber_0 = value;
	}

	inline static int32_t get_offset_of_linePosition_1() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___linePosition_1)); }
	inline int32_t get_linePosition_1() const { return ___linePosition_1; }
	inline int32_t* get_address_of_linePosition_1() { return &___linePosition_1; }
	inline void set_linePosition_1(int32_t value)
	{
		___linePosition_1 = value;
	}

	inline static int32_t get_offset_of_sourceUri_2() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___sourceUri_2)); }
	inline String_t* get_sourceUri_2() const { return ___sourceUri_2; }
	inline String_t** get_address_of_sourceUri_2() { return &___sourceUri_2; }
	inline void set_sourceUri_2(String_t* value)
	{
		___sourceUri_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_2), value);
	}

	inline static int32_t get_offset_of_namespaces_3() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___namespaces_3)); }
	inline XmlSerializerNamespaces_t3063656491 * get_namespaces_3() const { return ___namespaces_3; }
	inline XmlSerializerNamespaces_t3063656491 ** get_address_of_namespaces_3() { return &___namespaces_3; }
	inline void set_namespaces_3(XmlSerializerNamespaces_t3063656491 * value)
	{
		___namespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_3), value);
	}

	inline static int32_t get_offset_of_unhandledAttributeList_4() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___unhandledAttributeList_4)); }
	inline ArrayList_t4252133567 * get_unhandledAttributeList_4() const { return ___unhandledAttributeList_4; }
	inline ArrayList_t4252133567 ** get_address_of_unhandledAttributeList_4() { return &___unhandledAttributeList_4; }
	inline void set_unhandledAttributeList_4(ArrayList_t4252133567 * value)
	{
		___unhandledAttributeList_4 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributeList_4), value);
	}

	inline static int32_t get_offset_of_isCompiled_5() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___isCompiled_5)); }
	inline bool get_isCompiled_5() const { return ___isCompiled_5; }
	inline bool* get_address_of_isCompiled_5() { return &___isCompiled_5; }
	inline void set_isCompiled_5(bool value)
	{
		___isCompiled_5 = value;
	}

	inline static int32_t get_offset_of_errorCount_6() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___errorCount_6)); }
	inline int32_t get_errorCount_6() const { return ___errorCount_6; }
	inline int32_t* get_address_of_errorCount_6() { return &___errorCount_6; }
	inline void set_errorCount_6(int32_t value)
	{
		___errorCount_6 = value;
	}

	inline static int32_t get_offset_of_CompilationId_7() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___CompilationId_7)); }
	inline Guid_t  get_CompilationId_7() const { return ___CompilationId_7; }
	inline Guid_t * get_address_of_CompilationId_7() { return &___CompilationId_7; }
	inline void set_CompilationId_7(Guid_t  value)
	{
		___CompilationId_7 = value;
	}

	inline static int32_t get_offset_of_ValidationId_8() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___ValidationId_8)); }
	inline Guid_t  get_ValidationId_8() const { return ___ValidationId_8; }
	inline Guid_t * get_address_of_ValidationId_8() { return &___ValidationId_8; }
	inline void set_ValidationId_8(Guid_t  value)
	{
		___ValidationId_8 = value;
	}

	inline static int32_t get_offset_of_isRedefineChild_9() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___isRedefineChild_9)); }
	inline bool get_isRedefineChild_9() const { return ___isRedefineChild_9; }
	inline bool* get_address_of_isRedefineChild_9() { return &___isRedefineChild_9; }
	inline void set_isRedefineChild_9(bool value)
	{
		___isRedefineChild_9 = value;
	}

	inline static int32_t get_offset_of_isRedefinedComponent_10() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___isRedefinedComponent_10)); }
	inline bool get_isRedefinedComponent_10() const { return ___isRedefinedComponent_10; }
	inline bool* get_address_of_isRedefinedComponent_10() { return &___isRedefinedComponent_10; }
	inline void set_isRedefinedComponent_10(bool value)
	{
		___isRedefinedComponent_10 = value;
	}

	inline static int32_t get_offset_of_redefinedObject_11() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___redefinedObject_11)); }
	inline XmlSchemaObject_t2050913741 * get_redefinedObject_11() const { return ___redefinedObject_11; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_redefinedObject_11() { return &___redefinedObject_11; }
	inline void set_redefinedObject_11(XmlSchemaObject_t2050913741 * value)
	{
		___redefinedObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___redefinedObject_11), value);
	}

	inline static int32_t get_offset_of_parent_12() { return static_cast<int32_t>(offsetof(XmlSchemaObject_t2050913741, ___parent_12)); }
	inline XmlSchemaObject_t2050913741 * get_parent_12() const { return ___parent_12; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_parent_12() { return &___parent_12; }
	inline void set_parent_12(XmlSchemaObject_t2050913741 * value)
	{
		___parent_12 = value;
		Il2CppCodeGenWriteBarrier((&___parent_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECT_T2050913741_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef XMLSCHEMADERIVATIONMETHOD_T3165007540_H
#define XMLSCHEMADERIVATIONMETHOD_T3165007540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDerivationMethod
struct  XmlSchemaDerivationMethod_t3165007540 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDerivationMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaDerivationMethod_t3165007540, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADERIVATIONMETHOD_T3165007540_H
#ifndef NUMBERSTYLES_T3408984435_H
#define NUMBERSTYLES_T3408984435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberStyles
struct  NumberStyles_t3408984435 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NumberStyles_t3408984435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERSTYLES_T3408984435_H
#ifndef XMLSCHEMAEXCEPTION_T4082200141_H
#define XMLSCHEMAEXCEPTION_T4082200141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaException
struct  XmlSchemaException_t4082200141  : public SystemException_t3877406272
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaException::hasLineInfo
	bool ___hasLineInfo_11;
	// System.Int32 System.Xml.Schema.XmlSchemaException::lineNumber
	int32_t ___lineNumber_12;
	// System.Int32 System.Xml.Schema.XmlSchemaException::linePosition
	int32_t ___linePosition_13;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaException::sourceObj
	XmlSchemaObject_t2050913741 * ___sourceObj_14;
	// System.String System.Xml.Schema.XmlSchemaException::sourceUri
	String_t* ___sourceUri_15;

public:
	inline static int32_t get_offset_of_hasLineInfo_11() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___hasLineInfo_11)); }
	inline bool get_hasLineInfo_11() const { return ___hasLineInfo_11; }
	inline bool* get_address_of_hasLineInfo_11() { return &___hasLineInfo_11; }
	inline void set_hasLineInfo_11(bool value)
	{
		___hasLineInfo_11 = value;
	}

	inline static int32_t get_offset_of_lineNumber_12() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___lineNumber_12)); }
	inline int32_t get_lineNumber_12() const { return ___lineNumber_12; }
	inline int32_t* get_address_of_lineNumber_12() { return &___lineNumber_12; }
	inline void set_lineNumber_12(int32_t value)
	{
		___lineNumber_12 = value;
	}

	inline static int32_t get_offset_of_linePosition_13() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___linePosition_13)); }
	inline int32_t get_linePosition_13() const { return ___linePosition_13; }
	inline int32_t* get_address_of_linePosition_13() { return &___linePosition_13; }
	inline void set_linePosition_13(int32_t value)
	{
		___linePosition_13 = value;
	}

	inline static int32_t get_offset_of_sourceObj_14() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___sourceObj_14)); }
	inline XmlSchemaObject_t2050913741 * get_sourceObj_14() const { return ___sourceObj_14; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_sourceObj_14() { return &___sourceObj_14; }
	inline void set_sourceObj_14(XmlSchemaObject_t2050913741 * value)
	{
		___sourceObj_14 = value;
		Il2CppCodeGenWriteBarrier((&___sourceObj_14), value);
	}

	inline static int32_t get_offset_of_sourceUri_15() { return static_cast<int32_t>(offsetof(XmlSchemaException_t4082200141, ___sourceUri_15)); }
	inline String_t* get_sourceUri_15() const { return ___sourceUri_15; }
	inline String_t** get_address_of_sourceUri_15() { return &___sourceUri_15; }
	inline void set_sourceUri_15(String_t* value)
	{
		___sourceUri_15 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXCEPTION_T4082200141_H
#ifndef FACET_T3019654938_H
#define FACET_T3019654938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet/Facet
struct  Facet_t3019654938 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaFacet/Facet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Facet_t3019654938, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACET_T3019654938_H
#ifndef XMLSCHEMAFORM_T1143227640_H
#define XMLSCHEMAFORM_T1143227640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaForm
struct  XmlSchemaForm_t1143227640 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaForm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaForm_t1143227640, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFORM_T1143227640_H
#ifndef XMLSCHEMACONTENTPROCESSING_T74226324_H
#define XMLSCHEMACONTENTPROCESSING_T74226324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_t74226324 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_t74226324, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_T74226324_H
#ifndef XMLSCHEMACONTENTTYPE_T2874429441_H
#define XMLSCHEMACONTENTTYPE_T2874429441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentType
struct  XmlSchemaContentType_t2874429441 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaContentType_t2874429441, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTTYPE_T2874429441_H
#ifndef XMLSCHEMASET_T313318308_H
#define XMLSCHEMASET_T313318308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSet
struct  XmlSchemaSet_t313318308  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchemaSet::nameTable
	XmlNameTable_t1345805268 * ___nameTable_0;
	// System.Xml.XmlResolver System.Xml.Schema.XmlSchemaSet::xmlResolver
	XmlResolver_t2024571559 * ___xmlResolver_1;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaSet::schemas
	ArrayList_t4252133567 * ___schemas_2;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::attributes
	XmlSchemaObjectTable_t3364835593 * ___attributes_3;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::elements
	XmlSchemaObjectTable_t3364835593 * ___elements_4;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::types
	XmlSchemaObjectTable_t3364835593 * ___types_5;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaSet::idCollection
	Hashtable_t909839986 * ___idCollection_6;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::namedIdentities
	XmlSchemaObjectTable_t3364835593 * ___namedIdentities_7;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.XmlSchemaSet::settings
	XmlSchemaCompilationSettings_t2971213394 * ___settings_8;
	// System.Boolean System.Xml.Schema.XmlSchemaSet::isCompiled
	bool ___isCompiled_9;
	// System.Guid System.Xml.Schema.XmlSchemaSet::CompilationId
	Guid_t  ___CompilationId_10;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaSet::ValidationEventHandler
	ValidationEventHandler_t1580700381 * ___ValidationEventHandler_11;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___nameTable_0)); }
	inline XmlNameTable_t1345805268 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t1345805268 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_xmlResolver_1() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___xmlResolver_1)); }
	inline XmlResolver_t2024571559 * get_xmlResolver_1() const { return ___xmlResolver_1; }
	inline XmlResolver_t2024571559 ** get_address_of_xmlResolver_1() { return &___xmlResolver_1; }
	inline void set_xmlResolver_1(XmlResolver_t2024571559 * value)
	{
		___xmlResolver_1 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_1), value);
	}

	inline static int32_t get_offset_of_schemas_2() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___schemas_2)); }
	inline ArrayList_t4252133567 * get_schemas_2() const { return ___schemas_2; }
	inline ArrayList_t4252133567 ** get_address_of_schemas_2() { return &___schemas_2; }
	inline void set_schemas_2(ArrayList_t4252133567 * value)
	{
		___schemas_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_2), value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___attributes_3)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributes_3() const { return ___attributes_3; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_3), value);
	}

	inline static int32_t get_offset_of_elements_4() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___elements_4)); }
	inline XmlSchemaObjectTable_t3364835593 * get_elements_4() const { return ___elements_4; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_elements_4() { return &___elements_4; }
	inline void set_elements_4(XmlSchemaObjectTable_t3364835593 * value)
	{
		___elements_4 = value;
		Il2CppCodeGenWriteBarrier((&___elements_4), value);
	}

	inline static int32_t get_offset_of_types_5() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___types_5)); }
	inline XmlSchemaObjectTable_t3364835593 * get_types_5() const { return ___types_5; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_types_5() { return &___types_5; }
	inline void set_types_5(XmlSchemaObjectTable_t3364835593 * value)
	{
		___types_5 = value;
		Il2CppCodeGenWriteBarrier((&___types_5), value);
	}

	inline static int32_t get_offset_of_idCollection_6() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___idCollection_6)); }
	inline Hashtable_t909839986 * get_idCollection_6() const { return ___idCollection_6; }
	inline Hashtable_t909839986 ** get_address_of_idCollection_6() { return &___idCollection_6; }
	inline void set_idCollection_6(Hashtable_t909839986 * value)
	{
		___idCollection_6 = value;
		Il2CppCodeGenWriteBarrier((&___idCollection_6), value);
	}

	inline static int32_t get_offset_of_namedIdentities_7() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___namedIdentities_7)); }
	inline XmlSchemaObjectTable_t3364835593 * get_namedIdentities_7() const { return ___namedIdentities_7; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_namedIdentities_7() { return &___namedIdentities_7; }
	inline void set_namedIdentities_7(XmlSchemaObjectTable_t3364835593 * value)
	{
		___namedIdentities_7 = value;
		Il2CppCodeGenWriteBarrier((&___namedIdentities_7), value);
	}

	inline static int32_t get_offset_of_settings_8() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___settings_8)); }
	inline XmlSchemaCompilationSettings_t2971213394 * get_settings_8() const { return ___settings_8; }
	inline XmlSchemaCompilationSettings_t2971213394 ** get_address_of_settings_8() { return &___settings_8; }
	inline void set_settings_8(XmlSchemaCompilationSettings_t2971213394 * value)
	{
		___settings_8 = value;
		Il2CppCodeGenWriteBarrier((&___settings_8), value);
	}

	inline static int32_t get_offset_of_isCompiled_9() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___isCompiled_9)); }
	inline bool get_isCompiled_9() const { return ___isCompiled_9; }
	inline bool* get_address_of_isCompiled_9() { return &___isCompiled_9; }
	inline void set_isCompiled_9(bool value)
	{
		___isCompiled_9 = value;
	}

	inline static int32_t get_offset_of_CompilationId_10() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___CompilationId_10)); }
	inline Guid_t  get_CompilationId_10() const { return ___CompilationId_10; }
	inline Guid_t * get_address_of_CompilationId_10() { return &___CompilationId_10; }
	inline void set_CompilationId_10(Guid_t  value)
	{
		___CompilationId_10 = value;
	}

	inline static int32_t get_offset_of_ValidationEventHandler_11() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t313318308, ___ValidationEventHandler_11)); }
	inline ValidationEventHandler_t1580700381 * get_ValidationEventHandler_11() const { return ___ValidationEventHandler_11; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_ValidationEventHandler_11() { return &___ValidationEventHandler_11; }
	inline void set_ValidationEventHandler_11(ValidationEventHandler_t1580700381 * value)
	{
		___ValidationEventHandler_11 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASET_T313318308_H
#ifndef XMLSCHEMAVALIDITY_T995929432_H
#define XMLSCHEMAVALIDITY_T995929432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidity
struct  XmlSchemaValidity_t995929432 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaValidity_t995929432, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDITY_T995929432_H
#ifndef XSDWHITESPACEFACET_T2758097827_H
#define XSDWHITESPACEFACET_T2758097827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWhitespaceFacet
struct  XsdWhitespaceFacet_t2758097827 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdWhitespaceFacet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdWhitespaceFacet_t2758097827, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWHITESPACEFACET_T2758097827_H
#ifndef XMLTYPECODE_T58293802_H
#define XMLTYPECODE_T58293802_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlTypeCode
struct  XmlTypeCode_t58293802 
{
public:
	// System.Int32 System.Xml.Schema.XmlTypeCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlTypeCode_t58293802, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPECODE_T58293802_H
#ifndef XMLSEVERITYTYPE_T3547578624_H
#define XMLSEVERITYTYPE_T3547578624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSeverityType
struct  XmlSeverityType_t3547578624 
{
public:
	// System.Int32 System.Xml.Schema.XmlSeverityType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSeverityType_t3547578624, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSEVERITYTYPE_T3547578624_H
#ifndef XMLSCHEMAUSE_T3553149267_H
#define XMLSCHEMAUSE_T3553149267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUse
struct  XmlSchemaUse_t3553149267 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaUse::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaUse_t3553149267, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUSE_T3553149267_H
#ifndef XMLSCHEMAINFO_T2864028808_H
#define XMLSCHEMAINFO_T2864028808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInfo
struct  XmlSchemaInfo_t2864028808  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isDefault
	bool ___isDefault_0;
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isNil
	bool ___isNil_1;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaInfo::memberType
	XmlSchemaSimpleType_t248156492 * ___memberType_2;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaInfo::attr
	XmlSchemaAttribute_t4015859774 * ___attr_3;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaInfo::elem
	XmlSchemaElement_t2433337156 * ___elem_4;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaInfo::type
	XmlSchemaType_t1795078578 * ___type_5;
	// System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.XmlSchemaInfo::validity
	int32_t ___validity_6;

public:
	inline static int32_t get_offset_of_isDefault_0() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___isDefault_0)); }
	inline bool get_isDefault_0() const { return ___isDefault_0; }
	inline bool* get_address_of_isDefault_0() { return &___isDefault_0; }
	inline void set_isDefault_0(bool value)
	{
		___isDefault_0 = value;
	}

	inline static int32_t get_offset_of_isNil_1() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___isNil_1)); }
	inline bool get_isNil_1() const { return ___isNil_1; }
	inline bool* get_address_of_isNil_1() { return &___isNil_1; }
	inline void set_isNil_1(bool value)
	{
		___isNil_1 = value;
	}

	inline static int32_t get_offset_of_memberType_2() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___memberType_2)); }
	inline XmlSchemaSimpleType_t248156492 * get_memberType_2() const { return ___memberType_2; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_memberType_2() { return &___memberType_2; }
	inline void set_memberType_2(XmlSchemaSimpleType_t248156492 * value)
	{
		___memberType_2 = value;
		Il2CppCodeGenWriteBarrier((&___memberType_2), value);
	}

	inline static int32_t get_offset_of_attr_3() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___attr_3)); }
	inline XmlSchemaAttribute_t4015859774 * get_attr_3() const { return ___attr_3; }
	inline XmlSchemaAttribute_t4015859774 ** get_address_of_attr_3() { return &___attr_3; }
	inline void set_attr_3(XmlSchemaAttribute_t4015859774 * value)
	{
		___attr_3 = value;
		Il2CppCodeGenWriteBarrier((&___attr_3), value);
	}

	inline static int32_t get_offset_of_elem_4() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___elem_4)); }
	inline XmlSchemaElement_t2433337156 * get_elem_4() const { return ___elem_4; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_elem_4() { return &___elem_4; }
	inline void set_elem_4(XmlSchemaElement_t2433337156 * value)
	{
		___elem_4 = value;
		Il2CppCodeGenWriteBarrier((&___elem_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___type_5)); }
	inline XmlSchemaType_t1795078578 * get_type_5() const { return ___type_5; }
	inline XmlSchemaType_t1795078578 ** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(XmlSchemaType_t1795078578 * value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier((&___type_5), value);
	}

	inline static int32_t get_offset_of_validity_6() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t2864028808, ___validity_6)); }
	inline int32_t get_validity_6() const { return ___validity_6; }
	inline int32_t* get_address_of_validity_6() { return &___validity_6; }
	inline void set_validity_6(int32_t value)
	{
		___validity_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAINFO_T2864028808_H
#ifndef XMLSCHEMADOCUMENTATION_T3832803992_H
#define XMLSCHEMADOCUMENTATION_T3832803992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDocumentation
struct  XmlSchemaDocumentation_t3832803992  : public XmlSchemaObject_t2050913741
{
public:
	// System.String System.Xml.Schema.XmlSchemaDocumentation::language
	String_t* ___language_13;
	// System.Xml.XmlNode[] System.Xml.Schema.XmlSchemaDocumentation::markup
	XmlNodeU5BU5D_t2118142256* ___markup_14;
	// System.String System.Xml.Schema.XmlSchemaDocumentation::source
	String_t* ___source_15;

public:
	inline static int32_t get_offset_of_language_13() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t3832803992, ___language_13)); }
	inline String_t* get_language_13() const { return ___language_13; }
	inline String_t** get_address_of_language_13() { return &___language_13; }
	inline void set_language_13(String_t* value)
	{
		___language_13 = value;
		Il2CppCodeGenWriteBarrier((&___language_13), value);
	}

	inline static int32_t get_offset_of_markup_14() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t3832803992, ___markup_14)); }
	inline XmlNodeU5BU5D_t2118142256* get_markup_14() const { return ___markup_14; }
	inline XmlNodeU5BU5D_t2118142256** get_address_of_markup_14() { return &___markup_14; }
	inline void set_markup_14(XmlNodeU5BU5D_t2118142256* value)
	{
		___markup_14 = value;
		Il2CppCodeGenWriteBarrier((&___markup_14), value);
	}

	inline static int32_t get_offset_of_source_15() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_t3832803992, ___source_15)); }
	inline String_t* get_source_15() const { return ___source_15; }
	inline String_t** get_address_of_source_15() { return &___source_15; }
	inline void set_source_15(String_t* value)
	{
		___source_15 = value;
		Il2CppCodeGenWriteBarrier((&___source_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADOCUMENTATION_T3832803992_H
#ifndef XMLSCHEMA_T880472818_H
#define XMLSCHEMA_T880472818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchema
struct  XmlSchema_t880472818  : public XmlSchemaObject_t2050913741
{
public:
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchema::attributeFormDefault
	int32_t ___attributeFormDefault_13;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::attributeGroups
	XmlSchemaObjectTable_t3364835593 * ___attributeGroups_14;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::attributes
	XmlSchemaObjectTable_t3364835593 * ___attributes_15;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchema::blockDefault
	int32_t ___blockDefault_16;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchema::elementFormDefault
	int32_t ___elementFormDefault_17;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::elements
	XmlSchemaObjectTable_t3364835593 * ___elements_18;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchema::finalDefault
	int32_t ___finalDefault_19;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::groups
	XmlSchemaObjectTable_t3364835593 * ___groups_20;
	// System.String System.Xml.Schema.XmlSchema::id
	String_t* ___id_21;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::includes
	XmlSchemaObjectCollection_t395083109 * ___includes_22;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::items
	XmlSchemaObjectCollection_t395083109 * ___items_23;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::notations
	XmlSchemaObjectTable_t3364835593 * ___notations_24;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::schemaTypes
	XmlSchemaObjectTable_t3364835593 * ___schemaTypes_25;
	// System.String System.Xml.Schema.XmlSchema::targetNamespace
	String_t* ___targetNamespace_26;
	// System.String System.Xml.Schema.XmlSchema::version
	String_t* ___version_27;
	// System.Xml.Schema.XmlSchemaSet System.Xml.Schema.XmlSchema::schemas
	XmlSchemaSet_t313318308 * ___schemas_28;
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchema::nameTable
	XmlNameTable_t1345805268 * ___nameTable_29;
	// System.Boolean System.Xml.Schema.XmlSchema::missedSubComponents
	bool ___missedSubComponents_30;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::compilationItems
	XmlSchemaObjectCollection_t395083109 * ___compilationItems_31;

public:
	inline static int32_t get_offset_of_attributeFormDefault_13() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___attributeFormDefault_13)); }
	inline int32_t get_attributeFormDefault_13() const { return ___attributeFormDefault_13; }
	inline int32_t* get_address_of_attributeFormDefault_13() { return &___attributeFormDefault_13; }
	inline void set_attributeFormDefault_13(int32_t value)
	{
		___attributeFormDefault_13 = value;
	}

	inline static int32_t get_offset_of_attributeGroups_14() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___attributeGroups_14)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributeGroups_14() const { return ___attributeGroups_14; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributeGroups_14() { return &___attributeGroups_14; }
	inline void set_attributeGroups_14(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributeGroups_14 = value;
		Il2CppCodeGenWriteBarrier((&___attributeGroups_14), value);
	}

	inline static int32_t get_offset_of_attributes_15() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___attributes_15)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributes_15() const { return ___attributes_15; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributes_15() { return &___attributes_15; }
	inline void set_attributes_15(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributes_15 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_15), value);
	}

	inline static int32_t get_offset_of_blockDefault_16() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___blockDefault_16)); }
	inline int32_t get_blockDefault_16() const { return ___blockDefault_16; }
	inline int32_t* get_address_of_blockDefault_16() { return &___blockDefault_16; }
	inline void set_blockDefault_16(int32_t value)
	{
		___blockDefault_16 = value;
	}

	inline static int32_t get_offset_of_elementFormDefault_17() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___elementFormDefault_17)); }
	inline int32_t get_elementFormDefault_17() const { return ___elementFormDefault_17; }
	inline int32_t* get_address_of_elementFormDefault_17() { return &___elementFormDefault_17; }
	inline void set_elementFormDefault_17(int32_t value)
	{
		___elementFormDefault_17 = value;
	}

	inline static int32_t get_offset_of_elements_18() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___elements_18)); }
	inline XmlSchemaObjectTable_t3364835593 * get_elements_18() const { return ___elements_18; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_elements_18() { return &___elements_18; }
	inline void set_elements_18(XmlSchemaObjectTable_t3364835593 * value)
	{
		___elements_18 = value;
		Il2CppCodeGenWriteBarrier((&___elements_18), value);
	}

	inline static int32_t get_offset_of_finalDefault_19() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___finalDefault_19)); }
	inline int32_t get_finalDefault_19() const { return ___finalDefault_19; }
	inline int32_t* get_address_of_finalDefault_19() { return &___finalDefault_19; }
	inline void set_finalDefault_19(int32_t value)
	{
		___finalDefault_19 = value;
	}

	inline static int32_t get_offset_of_groups_20() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___groups_20)); }
	inline XmlSchemaObjectTable_t3364835593 * get_groups_20() const { return ___groups_20; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_groups_20() { return &___groups_20; }
	inline void set_groups_20(XmlSchemaObjectTable_t3364835593 * value)
	{
		___groups_20 = value;
		Il2CppCodeGenWriteBarrier((&___groups_20), value);
	}

	inline static int32_t get_offset_of_id_21() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___id_21)); }
	inline String_t* get_id_21() const { return ___id_21; }
	inline String_t** get_address_of_id_21() { return &___id_21; }
	inline void set_id_21(String_t* value)
	{
		___id_21 = value;
		Il2CppCodeGenWriteBarrier((&___id_21), value);
	}

	inline static int32_t get_offset_of_includes_22() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___includes_22)); }
	inline XmlSchemaObjectCollection_t395083109 * get_includes_22() const { return ___includes_22; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_includes_22() { return &___includes_22; }
	inline void set_includes_22(XmlSchemaObjectCollection_t395083109 * value)
	{
		___includes_22 = value;
		Il2CppCodeGenWriteBarrier((&___includes_22), value);
	}

	inline static int32_t get_offset_of_items_23() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___items_23)); }
	inline XmlSchemaObjectCollection_t395083109 * get_items_23() const { return ___items_23; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_items_23() { return &___items_23; }
	inline void set_items_23(XmlSchemaObjectCollection_t395083109 * value)
	{
		___items_23 = value;
		Il2CppCodeGenWriteBarrier((&___items_23), value);
	}

	inline static int32_t get_offset_of_notations_24() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___notations_24)); }
	inline XmlSchemaObjectTable_t3364835593 * get_notations_24() const { return ___notations_24; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_notations_24() { return &___notations_24; }
	inline void set_notations_24(XmlSchemaObjectTable_t3364835593 * value)
	{
		___notations_24 = value;
		Il2CppCodeGenWriteBarrier((&___notations_24), value);
	}

	inline static int32_t get_offset_of_schemaTypes_25() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___schemaTypes_25)); }
	inline XmlSchemaObjectTable_t3364835593 * get_schemaTypes_25() const { return ___schemaTypes_25; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_schemaTypes_25() { return &___schemaTypes_25; }
	inline void set_schemaTypes_25(XmlSchemaObjectTable_t3364835593 * value)
	{
		___schemaTypes_25 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypes_25), value);
	}

	inline static int32_t get_offset_of_targetNamespace_26() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___targetNamespace_26)); }
	inline String_t* get_targetNamespace_26() const { return ___targetNamespace_26; }
	inline String_t** get_address_of_targetNamespace_26() { return &___targetNamespace_26; }
	inline void set_targetNamespace_26(String_t* value)
	{
		___targetNamespace_26 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespace_26), value);
	}

	inline static int32_t get_offset_of_version_27() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___version_27)); }
	inline String_t* get_version_27() const { return ___version_27; }
	inline String_t** get_address_of_version_27() { return &___version_27; }
	inline void set_version_27(String_t* value)
	{
		___version_27 = value;
		Il2CppCodeGenWriteBarrier((&___version_27), value);
	}

	inline static int32_t get_offset_of_schemas_28() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___schemas_28)); }
	inline XmlSchemaSet_t313318308 * get_schemas_28() const { return ___schemas_28; }
	inline XmlSchemaSet_t313318308 ** get_address_of_schemas_28() { return &___schemas_28; }
	inline void set_schemas_28(XmlSchemaSet_t313318308 * value)
	{
		___schemas_28 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_28), value);
	}

	inline static int32_t get_offset_of_nameTable_29() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___nameTable_29)); }
	inline XmlNameTable_t1345805268 * get_nameTable_29() const { return ___nameTable_29; }
	inline XmlNameTable_t1345805268 ** get_address_of_nameTable_29() { return &___nameTable_29; }
	inline void set_nameTable_29(XmlNameTable_t1345805268 * value)
	{
		___nameTable_29 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_29), value);
	}

	inline static int32_t get_offset_of_missedSubComponents_30() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___missedSubComponents_30)); }
	inline bool get_missedSubComponents_30() const { return ___missedSubComponents_30; }
	inline bool* get_address_of_missedSubComponents_30() { return &___missedSubComponents_30; }
	inline void set_missedSubComponents_30(bool value)
	{
		___missedSubComponents_30 = value;
	}

	inline static int32_t get_offset_of_compilationItems_31() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818, ___compilationItems_31)); }
	inline XmlSchemaObjectCollection_t395083109 * get_compilationItems_31() const { return ___compilationItems_31; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_compilationItems_31() { return &___compilationItems_31; }
	inline void set_compilationItems_31(XmlSchemaObjectCollection_t395083109 * value)
	{
		___compilationItems_31 = value;
		Il2CppCodeGenWriteBarrier((&___compilationItems_31), value);
	}
};

struct XmlSchema_t880472818_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchema::<>f__switch$map2D
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2D_32;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2D_32() { return static_cast<int32_t>(offsetof(XmlSchema_t880472818_StaticFields, ___U3CU3Ef__switchU24map2D_32)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2D_32() const { return ___U3CU3Ef__switchU24map2D_32; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2D_32() { return &___U3CU3Ef__switchU24map2D_32; }
	inline void set_U3CU3Ef__switchU24map2D_32(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2D_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2D_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMA_T880472818_H
#ifndef XMLSCHEMAEXTERNAL_T3943748629_H
#define XMLSCHEMAEXTERNAL_T3943748629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaExternal
struct  XmlSchemaExternal_t3943748629  : public XmlSchemaObject_t2050913741
{
public:
	// System.String System.Xml.Schema.XmlSchemaExternal::id
	String_t* ___id_13;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaExternal::schema
	XmlSchema_t880472818 * ___schema_14;
	// System.String System.Xml.Schema.XmlSchemaExternal::location
	String_t* ___location_15;

public:
	inline static int32_t get_offset_of_id_13() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_t3943748629, ___id_13)); }
	inline String_t* get_id_13() const { return ___id_13; }
	inline String_t** get_address_of_id_13() { return &___id_13; }
	inline void set_id_13(String_t* value)
	{
		___id_13 = value;
		Il2CppCodeGenWriteBarrier((&___id_13), value);
	}

	inline static int32_t get_offset_of_schema_14() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_t3943748629, ___schema_14)); }
	inline XmlSchema_t880472818 * get_schema_14() const { return ___schema_14; }
	inline XmlSchema_t880472818 ** get_address_of_schema_14() { return &___schema_14; }
	inline void set_schema_14(XmlSchema_t880472818 * value)
	{
		___schema_14 = value;
		Il2CppCodeGenWriteBarrier((&___schema_14), value);
	}

	inline static int32_t get_offset_of_location_15() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_t3943748629, ___location_15)); }
	inline String_t* get_location_15() const { return ___location_15; }
	inline String_t** get_address_of_location_15() { return &___location_15; }
	inline void set_location_15(String_t* value)
	{
		___location_15 = value;
		Il2CppCodeGenWriteBarrier((&___location_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXTERNAL_T3943748629_H
#ifndef XMLSCHEMAANNOTATED_T2082486936_H
#define XMLSCHEMAANNOTATED_T2082486936_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotated
struct  XmlSchemaAnnotated_t2082486936  : public XmlSchemaObject_t2050913741
{
public:
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaAnnotated::annotation
	XmlSchemaAnnotation_t2400301303 * ___annotation_13;
	// System.String System.Xml.Schema.XmlSchemaAnnotated::id
	String_t* ___id_14;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchemaAnnotated::unhandledAttributes
	XmlAttributeU5BU5D_t287209776* ___unhandledAttributes_15;

public:
	inline static int32_t get_offset_of_annotation_13() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t2082486936, ___annotation_13)); }
	inline XmlSchemaAnnotation_t2400301303 * get_annotation_13() const { return ___annotation_13; }
	inline XmlSchemaAnnotation_t2400301303 ** get_address_of_annotation_13() { return &___annotation_13; }
	inline void set_annotation_13(XmlSchemaAnnotation_t2400301303 * value)
	{
		___annotation_13 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_13), value);
	}

	inline static int32_t get_offset_of_id_14() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t2082486936, ___id_14)); }
	inline String_t* get_id_14() const { return ___id_14; }
	inline String_t** get_address_of_id_14() { return &___id_14; }
	inline void set_id_14(String_t* value)
	{
		___id_14 = value;
		Il2CppCodeGenWriteBarrier((&___id_14), value);
	}

	inline static int32_t get_offset_of_unhandledAttributes_15() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t2082486936, ___unhandledAttributes_15)); }
	inline XmlAttributeU5BU5D_t287209776* get_unhandledAttributes_15() const { return ___unhandledAttributes_15; }
	inline XmlAttributeU5BU5D_t287209776** get_address_of_unhandledAttributes_15() { return &___unhandledAttributes_15; }
	inline void set_unhandledAttributes_15(XmlAttributeU5BU5D_t287209776* value)
	{
		___unhandledAttributes_15 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributes_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANNOTATED_T2082486936_H
#ifndef XMLSCHEMADATATYPE_T1195946242_H
#define XMLSCHEMADATATYPE_T1195946242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t1195946242  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdWhitespaceFacet System.Xml.Schema.XmlSchemaDatatype::WhitespaceValue
	int32_t ___WhitespaceValue_0;
	// System.Text.StringBuilder System.Xml.Schema.XmlSchemaDatatype::sb
	StringBuilder_t1221177846 * ___sb_2;

public:
	inline static int32_t get_offset_of_WhitespaceValue_0() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242, ___WhitespaceValue_0)); }
	inline int32_t get_WhitespaceValue_0() const { return ___WhitespaceValue_0; }
	inline int32_t* get_address_of_WhitespaceValue_0() { return &___WhitespaceValue_0; }
	inline void set_WhitespaceValue_0(int32_t value)
	{
		___WhitespaceValue_0 = value;
	}

	inline static int32_t get_offset_of_sb_2() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242, ___sb_2)); }
	inline StringBuilder_t1221177846 * get_sb_2() const { return ___sb_2; }
	inline StringBuilder_t1221177846 ** get_address_of_sb_2() { return &___sb_2; }
	inline void set_sb_2(StringBuilder_t1221177846 * value)
	{
		___sb_2 = value;
		Il2CppCodeGenWriteBarrier((&___sb_2), value);
	}
};

struct XmlSchemaDatatype_t1195946242_StaticFields
{
public:
	// System.Char[] System.Xml.Schema.XmlSchemaDatatype::wsChars
	CharU5BU5D_t1328083999* ___wsChars_1;
	// Mono.Xml.Schema.XsdAnySimpleType System.Xml.Schema.XmlSchemaDatatype::datatypeAnySimpleType
	XsdAnySimpleType_t1096449895 * ___datatypeAnySimpleType_3;
	// Mono.Xml.Schema.XsdString System.Xml.Schema.XmlSchemaDatatype::datatypeString
	XsdString_t263933896 * ___datatypeString_4;
	// Mono.Xml.Schema.XsdNormalizedString System.Xml.Schema.XmlSchemaDatatype::datatypeNormalizedString
	XsdNormalizedString_t2132216291 * ___datatypeNormalizedString_5;
	// Mono.Xml.Schema.XsdToken System.Xml.Schema.XmlSchemaDatatype::datatypeToken
	XsdToken_t2902215462 * ___datatypeToken_6;
	// Mono.Xml.Schema.XsdLanguage System.Xml.Schema.XmlSchemaDatatype::datatypeLanguage
	XsdLanguage_t3897851897 * ___datatypeLanguage_7;
	// Mono.Xml.Schema.XsdNMToken System.Xml.Schema.XmlSchemaDatatype::datatypeNMToken
	XsdNMToken_t547135877 * ___datatypeNMToken_8;
	// Mono.Xml.Schema.XsdNMTokens System.Xml.Schema.XmlSchemaDatatype::datatypeNMTokens
	XsdNMTokens_t1753890866 * ___datatypeNMTokens_9;
	// Mono.Xml.Schema.XsdName System.Xml.Schema.XmlSchemaDatatype::datatypeName
	XsdName_t1409945702 * ___datatypeName_10;
	// Mono.Xml.Schema.XsdNCName System.Xml.Schema.XmlSchemaDatatype::datatypeNCName
	XsdNCName_t414304927 * ___datatypeNCName_11;
	// Mono.Xml.Schema.XsdID System.Xml.Schema.XmlSchemaDatatype::datatypeID
	XsdID_t1067193160 * ___datatypeID_12;
	// Mono.Xml.Schema.XsdIDRef System.Xml.Schema.XmlSchemaDatatype::datatypeIDRef
	XsdIDRef_t1377311049 * ___datatypeIDRef_13;
	// Mono.Xml.Schema.XsdIDRefs System.Xml.Schema.XmlSchemaDatatype::datatypeIDRefs
	XsdIDRefs_t763119546 * ___datatypeIDRefs_14;
	// Mono.Xml.Schema.XsdEntity System.Xml.Schema.XmlSchemaDatatype::datatypeEntity
	XsdEntity_t2664305022 * ___datatypeEntity_15;
	// Mono.Xml.Schema.XsdEntities System.Xml.Schema.XmlSchemaDatatype::datatypeEntities
	XsdEntities_t1103053540 * ___datatypeEntities_16;
	// Mono.Xml.Schema.XsdNotation System.Xml.Schema.XmlSchemaDatatype::datatypeNotation
	XsdNotation_t720379093 * ___datatypeNotation_17;
	// Mono.Xml.Schema.XsdDecimal System.Xml.Schema.XmlSchemaDatatype::datatypeDecimal
	XsdDecimal_t2932251550 * ___datatypeDecimal_18;
	// Mono.Xml.Schema.XsdInteger System.Xml.Schema.XmlSchemaDatatype::datatypeInteger
	XsdInteger_t1330502157 * ___datatypeInteger_19;
	// Mono.Xml.Schema.XsdLong System.Xml.Schema.XmlSchemaDatatype::datatypeLong
	XsdLong_t4179235589 * ___datatypeLong_20;
	// Mono.Xml.Schema.XsdInt System.Xml.Schema.XmlSchemaDatatype::datatypeInt
	XsdInt_t1488443894 * ___datatypeInt_21;
	// Mono.Xml.Schema.XsdShort System.Xml.Schema.XmlSchemaDatatype::datatypeShort
	XsdShort_t1778530041 * ___datatypeShort_22;
	// Mono.Xml.Schema.XsdByte System.Xml.Schema.XmlSchemaDatatype::datatypeByte
	XsdByte_t1120972221 * ___datatypeByte_23;
	// Mono.Xml.Schema.XsdNonNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonNegativeInteger
	XsdNonNegativeInteger_t3587933853 * ___datatypeNonNegativeInteger_24;
	// Mono.Xml.Schema.XsdPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypePositiveInteger
	XsdPositiveInteger_t1896481288 * ___datatypePositiveInteger_25;
	// Mono.Xml.Schema.XsdUnsignedLong System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedLong
	XsdUnsignedLong_t137890294 * ___datatypeUnsignedLong_26;
	// Mono.Xml.Schema.XsdUnsignedInt System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedInt
	XsdUnsignedInt_t2956447959 * ___datatypeUnsignedInt_27;
	// Mono.Xml.Schema.XsdUnsignedShort System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedShort
	XsdUnsignedShort_t3693774826 * ___datatypeUnsignedShort_28;
	// Mono.Xml.Schema.XsdUnsignedByte System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedByte
	XsdUnsignedByte_t3216355454 * ___datatypeUnsignedByte_29;
	// Mono.Xml.Schema.XsdNonPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonPositiveInteger
	XsdNonPositiveInteger_t409343009 * ___datatypeNonPositiveInteger_30;
	// Mono.Xml.Schema.XsdNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNegativeInteger
	XsdNegativeInteger_t399444136 * ___datatypeNegativeInteger_31;
	// Mono.Xml.Schema.XsdFloat System.Xml.Schema.XmlSchemaDatatype::datatypeFloat
	XsdFloat_t386143221 * ___datatypeFloat_32;
	// Mono.Xml.Schema.XsdDouble System.Xml.Schema.XmlSchemaDatatype::datatypeDouble
	XsdDouble_t2510112208 * ___datatypeDouble_33;
	// Mono.Xml.Schema.XsdBase64Binary System.Xml.Schema.XmlSchemaDatatype::datatypeBase64Binary
	XsdBase64Binary_t1094704629 * ___datatypeBase64Binary_34;
	// Mono.Xml.Schema.XsdBoolean System.Xml.Schema.XmlSchemaDatatype::datatypeBoolean
	XsdBoolean_t4126353587 * ___datatypeBoolean_35;
	// Mono.Xml.Schema.XsdAnyURI System.Xml.Schema.XmlSchemaDatatype::datatypeAnyURI
	XsdAnyURI_t2527983239 * ___datatypeAnyURI_36;
	// Mono.Xml.Schema.XsdDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDuration
	XsdDuration_t1605638443 * ___datatypeDuration_37;
	// Mono.Xml.Schema.XsdDateTime System.Xml.Schema.XmlSchemaDatatype::datatypeDateTime
	XsdDateTime_t1344468684 * ___datatypeDateTime_38;
	// Mono.Xml.Schema.XsdDate System.Xml.Schema.XmlSchemaDatatype::datatypeDate
	XsdDate_t919459387 * ___datatypeDate_39;
	// Mono.Xml.Schema.XsdTime System.Xml.Schema.XmlSchemaDatatype::datatypeTime
	XsdTime_t4165680512 * ___datatypeTime_40;
	// Mono.Xml.Schema.XsdHexBinary System.Xml.Schema.XmlSchemaDatatype::datatypeHexBinary
	XsdHexBinary_t3496718151 * ___datatypeHexBinary_41;
	// Mono.Xml.Schema.XsdQName System.Xml.Schema.XmlSchemaDatatype::datatypeQName
	XsdQName_t930779123 * ___datatypeQName_42;
	// Mono.Xml.Schema.XsdGYearMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGYearMonth
	XsdGYearMonth_t1363357165 * ___datatypeGYearMonth_43;
	// Mono.Xml.Schema.XsdGMonthDay System.Xml.Schema.XmlSchemaDatatype::datatypeGMonthDay
	XsdGMonthDay_t3859978294 * ___datatypeGMonthDay_44;
	// Mono.Xml.Schema.XsdGYear System.Xml.Schema.XmlSchemaDatatype::datatypeGYear
	XsdGYear_t3810382607 * ___datatypeGYear_45;
	// Mono.Xml.Schema.XsdGMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGMonth
	XsdGMonth_t4076673358 * ___datatypeGMonth_46;
	// Mono.Xml.Schema.XsdGDay System.Xml.Schema.XmlSchemaDatatype::datatypeGDay
	XsdGDay_t1914244270 * ___datatypeGDay_47;
	// Mono.Xml.Schema.XdtAnyAtomicType System.Xml.Schema.XmlSchemaDatatype::datatypeAnyAtomicType
	XdtAnyAtomicType_t3359210273 * ___datatypeAnyAtomicType_48;
	// Mono.Xml.Schema.XdtUntypedAtomic System.Xml.Schema.XmlSchemaDatatype::datatypeUntypedAtomic
	XdtUntypedAtomic_t2904699188 * ___datatypeUntypedAtomic_49;
	// Mono.Xml.Schema.XdtDayTimeDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDayTimeDuration
	XdtDayTimeDuration_t2797717973 * ___datatypeDayTimeDuration_50;
	// Mono.Xml.Schema.XdtYearMonthDuration System.Xml.Schema.XmlSchemaDatatype::datatypeYearMonthDuration
	XdtYearMonthDuration_t1764328599 * ___datatypeYearMonthDuration_51;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2A
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2A_52;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2B
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2B_53;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2C
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2C_54;

public:
	inline static int32_t get_offset_of_wsChars_1() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___wsChars_1)); }
	inline CharU5BU5D_t1328083999* get_wsChars_1() const { return ___wsChars_1; }
	inline CharU5BU5D_t1328083999** get_address_of_wsChars_1() { return &___wsChars_1; }
	inline void set_wsChars_1(CharU5BU5D_t1328083999* value)
	{
		___wsChars_1 = value;
		Il2CppCodeGenWriteBarrier((&___wsChars_1), value);
	}

	inline static int32_t get_offset_of_datatypeAnySimpleType_3() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeAnySimpleType_3)); }
	inline XsdAnySimpleType_t1096449895 * get_datatypeAnySimpleType_3() const { return ___datatypeAnySimpleType_3; }
	inline XsdAnySimpleType_t1096449895 ** get_address_of_datatypeAnySimpleType_3() { return &___datatypeAnySimpleType_3; }
	inline void set_datatypeAnySimpleType_3(XsdAnySimpleType_t1096449895 * value)
	{
		___datatypeAnySimpleType_3 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnySimpleType_3), value);
	}

	inline static int32_t get_offset_of_datatypeString_4() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeString_4)); }
	inline XsdString_t263933896 * get_datatypeString_4() const { return ___datatypeString_4; }
	inline XsdString_t263933896 ** get_address_of_datatypeString_4() { return &___datatypeString_4; }
	inline void set_datatypeString_4(XsdString_t263933896 * value)
	{
		___datatypeString_4 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeString_4), value);
	}

	inline static int32_t get_offset_of_datatypeNormalizedString_5() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNormalizedString_5)); }
	inline XsdNormalizedString_t2132216291 * get_datatypeNormalizedString_5() const { return ___datatypeNormalizedString_5; }
	inline XsdNormalizedString_t2132216291 ** get_address_of_datatypeNormalizedString_5() { return &___datatypeNormalizedString_5; }
	inline void set_datatypeNormalizedString_5(XsdNormalizedString_t2132216291 * value)
	{
		___datatypeNormalizedString_5 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNormalizedString_5), value);
	}

	inline static int32_t get_offset_of_datatypeToken_6() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeToken_6)); }
	inline XsdToken_t2902215462 * get_datatypeToken_6() const { return ___datatypeToken_6; }
	inline XsdToken_t2902215462 ** get_address_of_datatypeToken_6() { return &___datatypeToken_6; }
	inline void set_datatypeToken_6(XsdToken_t2902215462 * value)
	{
		___datatypeToken_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeToken_6), value);
	}

	inline static int32_t get_offset_of_datatypeLanguage_7() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeLanguage_7)); }
	inline XsdLanguage_t3897851897 * get_datatypeLanguage_7() const { return ___datatypeLanguage_7; }
	inline XsdLanguage_t3897851897 ** get_address_of_datatypeLanguage_7() { return &___datatypeLanguage_7; }
	inline void set_datatypeLanguage_7(XsdLanguage_t3897851897 * value)
	{
		___datatypeLanguage_7 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLanguage_7), value);
	}

	inline static int32_t get_offset_of_datatypeNMToken_8() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNMToken_8)); }
	inline XsdNMToken_t547135877 * get_datatypeNMToken_8() const { return ___datatypeNMToken_8; }
	inline XsdNMToken_t547135877 ** get_address_of_datatypeNMToken_8() { return &___datatypeNMToken_8; }
	inline void set_datatypeNMToken_8(XsdNMToken_t547135877 * value)
	{
		___datatypeNMToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMToken_8), value);
	}

	inline static int32_t get_offset_of_datatypeNMTokens_9() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNMTokens_9)); }
	inline XsdNMTokens_t1753890866 * get_datatypeNMTokens_9() const { return ___datatypeNMTokens_9; }
	inline XsdNMTokens_t1753890866 ** get_address_of_datatypeNMTokens_9() { return &___datatypeNMTokens_9; }
	inline void set_datatypeNMTokens_9(XsdNMTokens_t1753890866 * value)
	{
		___datatypeNMTokens_9 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMTokens_9), value);
	}

	inline static int32_t get_offset_of_datatypeName_10() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeName_10)); }
	inline XsdName_t1409945702 * get_datatypeName_10() const { return ___datatypeName_10; }
	inline XsdName_t1409945702 ** get_address_of_datatypeName_10() { return &___datatypeName_10; }
	inline void set_datatypeName_10(XsdName_t1409945702 * value)
	{
		___datatypeName_10 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeName_10), value);
	}

	inline static int32_t get_offset_of_datatypeNCName_11() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNCName_11)); }
	inline XsdNCName_t414304927 * get_datatypeNCName_11() const { return ___datatypeNCName_11; }
	inline XsdNCName_t414304927 ** get_address_of_datatypeNCName_11() { return &___datatypeNCName_11; }
	inline void set_datatypeNCName_11(XsdNCName_t414304927 * value)
	{
		___datatypeNCName_11 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNCName_11), value);
	}

	inline static int32_t get_offset_of_datatypeID_12() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeID_12)); }
	inline XsdID_t1067193160 * get_datatypeID_12() const { return ___datatypeID_12; }
	inline XsdID_t1067193160 ** get_address_of_datatypeID_12() { return &___datatypeID_12; }
	inline void set_datatypeID_12(XsdID_t1067193160 * value)
	{
		___datatypeID_12 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeID_12), value);
	}

	inline static int32_t get_offset_of_datatypeIDRef_13() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeIDRef_13)); }
	inline XsdIDRef_t1377311049 * get_datatypeIDRef_13() const { return ___datatypeIDRef_13; }
	inline XsdIDRef_t1377311049 ** get_address_of_datatypeIDRef_13() { return &___datatypeIDRef_13; }
	inline void set_datatypeIDRef_13(XsdIDRef_t1377311049 * value)
	{
		___datatypeIDRef_13 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRef_13), value);
	}

	inline static int32_t get_offset_of_datatypeIDRefs_14() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeIDRefs_14)); }
	inline XsdIDRefs_t763119546 * get_datatypeIDRefs_14() const { return ___datatypeIDRefs_14; }
	inline XsdIDRefs_t763119546 ** get_address_of_datatypeIDRefs_14() { return &___datatypeIDRefs_14; }
	inline void set_datatypeIDRefs_14(XsdIDRefs_t763119546 * value)
	{
		___datatypeIDRefs_14 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRefs_14), value);
	}

	inline static int32_t get_offset_of_datatypeEntity_15() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeEntity_15)); }
	inline XsdEntity_t2664305022 * get_datatypeEntity_15() const { return ___datatypeEntity_15; }
	inline XsdEntity_t2664305022 ** get_address_of_datatypeEntity_15() { return &___datatypeEntity_15; }
	inline void set_datatypeEntity_15(XsdEntity_t2664305022 * value)
	{
		___datatypeEntity_15 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntity_15), value);
	}

	inline static int32_t get_offset_of_datatypeEntities_16() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeEntities_16)); }
	inline XsdEntities_t1103053540 * get_datatypeEntities_16() const { return ___datatypeEntities_16; }
	inline XsdEntities_t1103053540 ** get_address_of_datatypeEntities_16() { return &___datatypeEntities_16; }
	inline void set_datatypeEntities_16(XsdEntities_t1103053540 * value)
	{
		___datatypeEntities_16 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntities_16), value);
	}

	inline static int32_t get_offset_of_datatypeNotation_17() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNotation_17)); }
	inline XsdNotation_t720379093 * get_datatypeNotation_17() const { return ___datatypeNotation_17; }
	inline XsdNotation_t720379093 ** get_address_of_datatypeNotation_17() { return &___datatypeNotation_17; }
	inline void set_datatypeNotation_17(XsdNotation_t720379093 * value)
	{
		___datatypeNotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNotation_17), value);
	}

	inline static int32_t get_offset_of_datatypeDecimal_18() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDecimal_18)); }
	inline XsdDecimal_t2932251550 * get_datatypeDecimal_18() const { return ___datatypeDecimal_18; }
	inline XsdDecimal_t2932251550 ** get_address_of_datatypeDecimal_18() { return &___datatypeDecimal_18; }
	inline void set_datatypeDecimal_18(XsdDecimal_t2932251550 * value)
	{
		___datatypeDecimal_18 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDecimal_18), value);
	}

	inline static int32_t get_offset_of_datatypeInteger_19() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeInteger_19)); }
	inline XsdInteger_t1330502157 * get_datatypeInteger_19() const { return ___datatypeInteger_19; }
	inline XsdInteger_t1330502157 ** get_address_of_datatypeInteger_19() { return &___datatypeInteger_19; }
	inline void set_datatypeInteger_19(XsdInteger_t1330502157 * value)
	{
		___datatypeInteger_19 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInteger_19), value);
	}

	inline static int32_t get_offset_of_datatypeLong_20() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeLong_20)); }
	inline XsdLong_t4179235589 * get_datatypeLong_20() const { return ___datatypeLong_20; }
	inline XsdLong_t4179235589 ** get_address_of_datatypeLong_20() { return &___datatypeLong_20; }
	inline void set_datatypeLong_20(XsdLong_t4179235589 * value)
	{
		___datatypeLong_20 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLong_20), value);
	}

	inline static int32_t get_offset_of_datatypeInt_21() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeInt_21)); }
	inline XsdInt_t1488443894 * get_datatypeInt_21() const { return ___datatypeInt_21; }
	inline XsdInt_t1488443894 ** get_address_of_datatypeInt_21() { return &___datatypeInt_21; }
	inline void set_datatypeInt_21(XsdInt_t1488443894 * value)
	{
		___datatypeInt_21 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInt_21), value);
	}

	inline static int32_t get_offset_of_datatypeShort_22() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeShort_22)); }
	inline XsdShort_t1778530041 * get_datatypeShort_22() const { return ___datatypeShort_22; }
	inline XsdShort_t1778530041 ** get_address_of_datatypeShort_22() { return &___datatypeShort_22; }
	inline void set_datatypeShort_22(XsdShort_t1778530041 * value)
	{
		___datatypeShort_22 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeShort_22), value);
	}

	inline static int32_t get_offset_of_datatypeByte_23() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeByte_23)); }
	inline XsdByte_t1120972221 * get_datatypeByte_23() const { return ___datatypeByte_23; }
	inline XsdByte_t1120972221 ** get_address_of_datatypeByte_23() { return &___datatypeByte_23; }
	inline void set_datatypeByte_23(XsdByte_t1120972221 * value)
	{
		___datatypeByte_23 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeByte_23), value);
	}

	inline static int32_t get_offset_of_datatypeNonNegativeInteger_24() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNonNegativeInteger_24)); }
	inline XsdNonNegativeInteger_t3587933853 * get_datatypeNonNegativeInteger_24() const { return ___datatypeNonNegativeInteger_24; }
	inline XsdNonNegativeInteger_t3587933853 ** get_address_of_datatypeNonNegativeInteger_24() { return &___datatypeNonNegativeInteger_24; }
	inline void set_datatypeNonNegativeInteger_24(XsdNonNegativeInteger_t3587933853 * value)
	{
		___datatypeNonNegativeInteger_24 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonNegativeInteger_24), value);
	}

	inline static int32_t get_offset_of_datatypePositiveInteger_25() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypePositiveInteger_25)); }
	inline XsdPositiveInteger_t1896481288 * get_datatypePositiveInteger_25() const { return ___datatypePositiveInteger_25; }
	inline XsdPositiveInteger_t1896481288 ** get_address_of_datatypePositiveInteger_25() { return &___datatypePositiveInteger_25; }
	inline void set_datatypePositiveInteger_25(XsdPositiveInteger_t1896481288 * value)
	{
		___datatypePositiveInteger_25 = value;
		Il2CppCodeGenWriteBarrier((&___datatypePositiveInteger_25), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedLong_26() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUnsignedLong_26)); }
	inline XsdUnsignedLong_t137890294 * get_datatypeUnsignedLong_26() const { return ___datatypeUnsignedLong_26; }
	inline XsdUnsignedLong_t137890294 ** get_address_of_datatypeUnsignedLong_26() { return &___datatypeUnsignedLong_26; }
	inline void set_datatypeUnsignedLong_26(XsdUnsignedLong_t137890294 * value)
	{
		___datatypeUnsignedLong_26 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedLong_26), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedInt_27() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUnsignedInt_27)); }
	inline XsdUnsignedInt_t2956447959 * get_datatypeUnsignedInt_27() const { return ___datatypeUnsignedInt_27; }
	inline XsdUnsignedInt_t2956447959 ** get_address_of_datatypeUnsignedInt_27() { return &___datatypeUnsignedInt_27; }
	inline void set_datatypeUnsignedInt_27(XsdUnsignedInt_t2956447959 * value)
	{
		___datatypeUnsignedInt_27 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedInt_27), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedShort_28() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUnsignedShort_28)); }
	inline XsdUnsignedShort_t3693774826 * get_datatypeUnsignedShort_28() const { return ___datatypeUnsignedShort_28; }
	inline XsdUnsignedShort_t3693774826 ** get_address_of_datatypeUnsignedShort_28() { return &___datatypeUnsignedShort_28; }
	inline void set_datatypeUnsignedShort_28(XsdUnsignedShort_t3693774826 * value)
	{
		___datatypeUnsignedShort_28 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedShort_28), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedByte_29() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUnsignedByte_29)); }
	inline XsdUnsignedByte_t3216355454 * get_datatypeUnsignedByte_29() const { return ___datatypeUnsignedByte_29; }
	inline XsdUnsignedByte_t3216355454 ** get_address_of_datatypeUnsignedByte_29() { return &___datatypeUnsignedByte_29; }
	inline void set_datatypeUnsignedByte_29(XsdUnsignedByte_t3216355454 * value)
	{
		___datatypeUnsignedByte_29 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedByte_29), value);
	}

	inline static int32_t get_offset_of_datatypeNonPositiveInteger_30() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNonPositiveInteger_30)); }
	inline XsdNonPositiveInteger_t409343009 * get_datatypeNonPositiveInteger_30() const { return ___datatypeNonPositiveInteger_30; }
	inline XsdNonPositiveInteger_t409343009 ** get_address_of_datatypeNonPositiveInteger_30() { return &___datatypeNonPositiveInteger_30; }
	inline void set_datatypeNonPositiveInteger_30(XsdNonPositiveInteger_t409343009 * value)
	{
		___datatypeNonPositiveInteger_30 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonPositiveInteger_30), value);
	}

	inline static int32_t get_offset_of_datatypeNegativeInteger_31() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNegativeInteger_31)); }
	inline XsdNegativeInteger_t399444136 * get_datatypeNegativeInteger_31() const { return ___datatypeNegativeInteger_31; }
	inline XsdNegativeInteger_t399444136 ** get_address_of_datatypeNegativeInteger_31() { return &___datatypeNegativeInteger_31; }
	inline void set_datatypeNegativeInteger_31(XsdNegativeInteger_t399444136 * value)
	{
		___datatypeNegativeInteger_31 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNegativeInteger_31), value);
	}

	inline static int32_t get_offset_of_datatypeFloat_32() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeFloat_32)); }
	inline XsdFloat_t386143221 * get_datatypeFloat_32() const { return ___datatypeFloat_32; }
	inline XsdFloat_t386143221 ** get_address_of_datatypeFloat_32() { return &___datatypeFloat_32; }
	inline void set_datatypeFloat_32(XsdFloat_t386143221 * value)
	{
		___datatypeFloat_32 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeFloat_32), value);
	}

	inline static int32_t get_offset_of_datatypeDouble_33() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDouble_33)); }
	inline XsdDouble_t2510112208 * get_datatypeDouble_33() const { return ___datatypeDouble_33; }
	inline XsdDouble_t2510112208 ** get_address_of_datatypeDouble_33() { return &___datatypeDouble_33; }
	inline void set_datatypeDouble_33(XsdDouble_t2510112208 * value)
	{
		___datatypeDouble_33 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDouble_33), value);
	}

	inline static int32_t get_offset_of_datatypeBase64Binary_34() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeBase64Binary_34)); }
	inline XsdBase64Binary_t1094704629 * get_datatypeBase64Binary_34() const { return ___datatypeBase64Binary_34; }
	inline XsdBase64Binary_t1094704629 ** get_address_of_datatypeBase64Binary_34() { return &___datatypeBase64Binary_34; }
	inline void set_datatypeBase64Binary_34(XsdBase64Binary_t1094704629 * value)
	{
		___datatypeBase64Binary_34 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBase64Binary_34), value);
	}

	inline static int32_t get_offset_of_datatypeBoolean_35() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeBoolean_35)); }
	inline XsdBoolean_t4126353587 * get_datatypeBoolean_35() const { return ___datatypeBoolean_35; }
	inline XsdBoolean_t4126353587 ** get_address_of_datatypeBoolean_35() { return &___datatypeBoolean_35; }
	inline void set_datatypeBoolean_35(XsdBoolean_t4126353587 * value)
	{
		___datatypeBoolean_35 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBoolean_35), value);
	}

	inline static int32_t get_offset_of_datatypeAnyURI_36() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeAnyURI_36)); }
	inline XsdAnyURI_t2527983239 * get_datatypeAnyURI_36() const { return ___datatypeAnyURI_36; }
	inline XsdAnyURI_t2527983239 ** get_address_of_datatypeAnyURI_36() { return &___datatypeAnyURI_36; }
	inline void set_datatypeAnyURI_36(XsdAnyURI_t2527983239 * value)
	{
		___datatypeAnyURI_36 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyURI_36), value);
	}

	inline static int32_t get_offset_of_datatypeDuration_37() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDuration_37)); }
	inline XsdDuration_t1605638443 * get_datatypeDuration_37() const { return ___datatypeDuration_37; }
	inline XsdDuration_t1605638443 ** get_address_of_datatypeDuration_37() { return &___datatypeDuration_37; }
	inline void set_datatypeDuration_37(XsdDuration_t1605638443 * value)
	{
		___datatypeDuration_37 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDuration_37), value);
	}

	inline static int32_t get_offset_of_datatypeDateTime_38() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDateTime_38)); }
	inline XsdDateTime_t1344468684 * get_datatypeDateTime_38() const { return ___datatypeDateTime_38; }
	inline XsdDateTime_t1344468684 ** get_address_of_datatypeDateTime_38() { return &___datatypeDateTime_38; }
	inline void set_datatypeDateTime_38(XsdDateTime_t1344468684 * value)
	{
		___datatypeDateTime_38 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDateTime_38), value);
	}

	inline static int32_t get_offset_of_datatypeDate_39() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDate_39)); }
	inline XsdDate_t919459387 * get_datatypeDate_39() const { return ___datatypeDate_39; }
	inline XsdDate_t919459387 ** get_address_of_datatypeDate_39() { return &___datatypeDate_39; }
	inline void set_datatypeDate_39(XsdDate_t919459387 * value)
	{
		___datatypeDate_39 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDate_39), value);
	}

	inline static int32_t get_offset_of_datatypeTime_40() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeTime_40)); }
	inline XsdTime_t4165680512 * get_datatypeTime_40() const { return ___datatypeTime_40; }
	inline XsdTime_t4165680512 ** get_address_of_datatypeTime_40() { return &___datatypeTime_40; }
	inline void set_datatypeTime_40(XsdTime_t4165680512 * value)
	{
		___datatypeTime_40 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeTime_40), value);
	}

	inline static int32_t get_offset_of_datatypeHexBinary_41() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeHexBinary_41)); }
	inline XsdHexBinary_t3496718151 * get_datatypeHexBinary_41() const { return ___datatypeHexBinary_41; }
	inline XsdHexBinary_t3496718151 ** get_address_of_datatypeHexBinary_41() { return &___datatypeHexBinary_41; }
	inline void set_datatypeHexBinary_41(XsdHexBinary_t3496718151 * value)
	{
		___datatypeHexBinary_41 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeHexBinary_41), value);
	}

	inline static int32_t get_offset_of_datatypeQName_42() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeQName_42)); }
	inline XsdQName_t930779123 * get_datatypeQName_42() const { return ___datatypeQName_42; }
	inline XsdQName_t930779123 ** get_address_of_datatypeQName_42() { return &___datatypeQName_42; }
	inline void set_datatypeQName_42(XsdQName_t930779123 * value)
	{
		___datatypeQName_42 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeQName_42), value);
	}

	inline static int32_t get_offset_of_datatypeGYearMonth_43() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGYearMonth_43)); }
	inline XsdGYearMonth_t1363357165 * get_datatypeGYearMonth_43() const { return ___datatypeGYearMonth_43; }
	inline XsdGYearMonth_t1363357165 ** get_address_of_datatypeGYearMonth_43() { return &___datatypeGYearMonth_43; }
	inline void set_datatypeGYearMonth_43(XsdGYearMonth_t1363357165 * value)
	{
		___datatypeGYearMonth_43 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYearMonth_43), value);
	}

	inline static int32_t get_offset_of_datatypeGMonthDay_44() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGMonthDay_44)); }
	inline XsdGMonthDay_t3859978294 * get_datatypeGMonthDay_44() const { return ___datatypeGMonthDay_44; }
	inline XsdGMonthDay_t3859978294 ** get_address_of_datatypeGMonthDay_44() { return &___datatypeGMonthDay_44; }
	inline void set_datatypeGMonthDay_44(XsdGMonthDay_t3859978294 * value)
	{
		___datatypeGMonthDay_44 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonthDay_44), value);
	}

	inline static int32_t get_offset_of_datatypeGYear_45() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGYear_45)); }
	inline XsdGYear_t3810382607 * get_datatypeGYear_45() const { return ___datatypeGYear_45; }
	inline XsdGYear_t3810382607 ** get_address_of_datatypeGYear_45() { return &___datatypeGYear_45; }
	inline void set_datatypeGYear_45(XsdGYear_t3810382607 * value)
	{
		___datatypeGYear_45 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYear_45), value);
	}

	inline static int32_t get_offset_of_datatypeGMonth_46() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGMonth_46)); }
	inline XsdGMonth_t4076673358 * get_datatypeGMonth_46() const { return ___datatypeGMonth_46; }
	inline XsdGMonth_t4076673358 ** get_address_of_datatypeGMonth_46() { return &___datatypeGMonth_46; }
	inline void set_datatypeGMonth_46(XsdGMonth_t4076673358 * value)
	{
		___datatypeGMonth_46 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonth_46), value);
	}

	inline static int32_t get_offset_of_datatypeGDay_47() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGDay_47)); }
	inline XsdGDay_t1914244270 * get_datatypeGDay_47() const { return ___datatypeGDay_47; }
	inline XsdGDay_t1914244270 ** get_address_of_datatypeGDay_47() { return &___datatypeGDay_47; }
	inline void set_datatypeGDay_47(XsdGDay_t1914244270 * value)
	{
		___datatypeGDay_47 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGDay_47), value);
	}

	inline static int32_t get_offset_of_datatypeAnyAtomicType_48() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeAnyAtomicType_48)); }
	inline XdtAnyAtomicType_t3359210273 * get_datatypeAnyAtomicType_48() const { return ___datatypeAnyAtomicType_48; }
	inline XdtAnyAtomicType_t3359210273 ** get_address_of_datatypeAnyAtomicType_48() { return &___datatypeAnyAtomicType_48; }
	inline void set_datatypeAnyAtomicType_48(XdtAnyAtomicType_t3359210273 * value)
	{
		___datatypeAnyAtomicType_48 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyAtomicType_48), value);
	}

	inline static int32_t get_offset_of_datatypeUntypedAtomic_49() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUntypedAtomic_49)); }
	inline XdtUntypedAtomic_t2904699188 * get_datatypeUntypedAtomic_49() const { return ___datatypeUntypedAtomic_49; }
	inline XdtUntypedAtomic_t2904699188 ** get_address_of_datatypeUntypedAtomic_49() { return &___datatypeUntypedAtomic_49; }
	inline void set_datatypeUntypedAtomic_49(XdtUntypedAtomic_t2904699188 * value)
	{
		___datatypeUntypedAtomic_49 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUntypedAtomic_49), value);
	}

	inline static int32_t get_offset_of_datatypeDayTimeDuration_50() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDayTimeDuration_50)); }
	inline XdtDayTimeDuration_t2797717973 * get_datatypeDayTimeDuration_50() const { return ___datatypeDayTimeDuration_50; }
	inline XdtDayTimeDuration_t2797717973 ** get_address_of_datatypeDayTimeDuration_50() { return &___datatypeDayTimeDuration_50; }
	inline void set_datatypeDayTimeDuration_50(XdtDayTimeDuration_t2797717973 * value)
	{
		___datatypeDayTimeDuration_50 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDayTimeDuration_50), value);
	}

	inline static int32_t get_offset_of_datatypeYearMonthDuration_51() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeYearMonthDuration_51)); }
	inline XdtYearMonthDuration_t1764328599 * get_datatypeYearMonthDuration_51() const { return ___datatypeYearMonthDuration_51; }
	inline XdtYearMonthDuration_t1764328599 ** get_address_of_datatypeYearMonthDuration_51() { return &___datatypeYearMonthDuration_51; }
	inline void set_datatypeYearMonthDuration_51(XdtYearMonthDuration_t1764328599 * value)
	{
		___datatypeYearMonthDuration_51 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeYearMonthDuration_51), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2A_52() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___U3CU3Ef__switchU24map2A_52)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2A_52() const { return ___U3CU3Ef__switchU24map2A_52; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2A_52() { return &___U3CU3Ef__switchU24map2A_52; }
	inline void set_U3CU3Ef__switchU24map2A_52(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2A_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2A_52), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2B_53() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___U3CU3Ef__switchU24map2B_53)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2B_53() const { return ___U3CU3Ef__switchU24map2B_53; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2B_53() { return &___U3CU3Ef__switchU24map2B_53; }
	inline void set_U3CU3Ef__switchU24map2B_53(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2B_53 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2B_53), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2C_54() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___U3CU3Ef__switchU24map2C_54)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2C_54() const { return ___U3CU3Ef__switchU24map2C_54; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2C_54() { return &___U3CU3Ef__switchU24map2C_54; }
	inline void set_U3CU3Ef__switchU24map2C_54(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2C_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2C_54), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T1195946242_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef XMLSCHEMAAPPINFO_T2033489551_H
#define XMLSCHEMAAPPINFO_T2033489551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAppInfo
struct  XmlSchemaAppInfo_t2033489551  : public XmlSchemaObject_t2050913741
{
public:
	// System.Xml.XmlNode[] System.Xml.Schema.XmlSchemaAppInfo::markup
	XmlNodeU5BU5D_t2118142256* ___markup_13;
	// System.String System.Xml.Schema.XmlSchemaAppInfo::source
	String_t* ___source_14;

public:
	inline static int32_t get_offset_of_markup_13() { return static_cast<int32_t>(offsetof(XmlSchemaAppInfo_t2033489551, ___markup_13)); }
	inline XmlNodeU5BU5D_t2118142256* get_markup_13() const { return ___markup_13; }
	inline XmlNodeU5BU5D_t2118142256** get_address_of_markup_13() { return &___markup_13; }
	inline void set_markup_13(XmlNodeU5BU5D_t2118142256* value)
	{
		___markup_13 = value;
		Il2CppCodeGenWriteBarrier((&___markup_13), value);
	}

	inline static int32_t get_offset_of_source_14() { return static_cast<int32_t>(offsetof(XmlSchemaAppInfo_t2033489551, ___source_14)); }
	inline String_t* get_source_14() const { return ___source_14; }
	inline String_t** get_address_of_source_14() { return &___source_14; }
	inline void set_source_14(String_t* value)
	{
		___source_14 = value;
		Il2CppCodeGenWriteBarrier((&___source_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAAPPINFO_T2033489551_H
#ifndef XMLSCHEMAANNOTATION_T2400301303_H
#define XMLSCHEMAANNOTATION_T2400301303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotation
struct  XmlSchemaAnnotation_t2400301303  : public XmlSchemaObject_t2050913741
{
public:
	// System.String System.Xml.Schema.XmlSchemaAnnotation::id
	String_t* ___id_13;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAnnotation::items
	XmlSchemaObjectCollection_t395083109 * ___items_14;

public:
	inline static int32_t get_offset_of_id_13() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotation_t2400301303, ___id_13)); }
	inline String_t* get_id_13() const { return ___id_13; }
	inline String_t** get_address_of_id_13() { return &___id_13; }
	inline void set_id_13(String_t* value)
	{
		___id_13 = value;
		Il2CppCodeGenWriteBarrier((&___id_13), value);
	}

	inline static int32_t get_offset_of_items_14() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotation_t2400301303, ___items_14)); }
	inline XmlSchemaObjectCollection_t395083109 * get_items_14() const { return ___items_14; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_items_14() { return &___items_14; }
	inline void set_items_14(XmlSchemaObjectCollection_t395083109 * value)
	{
		___items_14 = value;
		Il2CppCodeGenWriteBarrier((&___items_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANNOTATION_T2400301303_H
#ifndef VALIDATIONEVENTARGS_T1577905814_H
#define VALIDATIONEVENTARGS_T1577905814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationEventArgs
struct  ValidationEventArgs_t1577905814  : public EventArgs_t3289624707
{
public:
	// System.Xml.Schema.XmlSchemaException System.Xml.Schema.ValidationEventArgs::exception
	XmlSchemaException_t4082200141 * ___exception_1;
	// System.String System.Xml.Schema.ValidationEventArgs::message
	String_t* ___message_2;
	// System.Xml.Schema.XmlSeverityType System.Xml.Schema.ValidationEventArgs::severity
	int32_t ___severity_3;

public:
	inline static int32_t get_offset_of_exception_1() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t1577905814, ___exception_1)); }
	inline XmlSchemaException_t4082200141 * get_exception_1() const { return ___exception_1; }
	inline XmlSchemaException_t4082200141 ** get_address_of_exception_1() { return &___exception_1; }
	inline void set_exception_1(XmlSchemaException_t4082200141 * value)
	{
		___exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t1577905814, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_severity_3() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t1577905814, ___severity_3)); }
	inline int32_t get_severity_3() const { return ___severity_3; }
	inline int32_t* get_address_of_severity_3() { return &___severity_3; }
	inline void set_severity_3(int32_t value)
	{
		___severity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONEVENTARGS_T1577905814_H
#ifndef XMLSCHEMAINCLUDE_T2752556710_H
#define XMLSCHEMAINCLUDE_T2752556710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInclude
struct  XmlSchemaInclude_t2752556710  : public XmlSchemaExternal_t3943748629
{
public:
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaInclude::annotation
	XmlSchemaAnnotation_t2400301303 * ___annotation_16;

public:
	inline static int32_t get_offset_of_annotation_16() { return static_cast<int32_t>(offsetof(XmlSchemaInclude_t2752556710, ___annotation_16)); }
	inline XmlSchemaAnnotation_t2400301303 * get_annotation_16() const { return ___annotation_16; }
	inline XmlSchemaAnnotation_t2400301303 ** get_address_of_annotation_16() { return &___annotation_16; }
	inline void set_annotation_16(XmlSchemaAnnotation_t2400301303 * value)
	{
		___annotation_16 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAINCLUDE_T2752556710_H
#ifndef XMLSCHEMASIMPLETYPECONTENT_T1606103299_H
#define XMLSCHEMASIMPLETYPECONTENT_T1606103299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct  XmlSchemaSimpleTypeContent_t1606103299  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeContent::OwnerType
	XmlSchemaSimpleType_t248156492 * ___OwnerType_16;

public:
	inline static int32_t get_offset_of_OwnerType_16() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeContent_t1606103299, ___OwnerType_16)); }
	inline XmlSchemaSimpleType_t248156492 * get_OwnerType_16() const { return ___OwnerType_16; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_OwnerType_16() { return &___OwnerType_16; }
	inline void set_OwnerType_16(XmlSchemaSimpleType_t248156492 * value)
	{
		___OwnerType_16 = value;
		Il2CppCodeGenWriteBarrier((&___OwnerType_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPECONTENT_T1606103299_H
#ifndef XMLSCHEMAIDENTITYCONSTRAINT_T1058613623_H
#define XMLSCHEMAIDENTITYCONSTRAINT_T1058613623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaIdentityConstraint
struct  XmlSchemaIdentityConstraint_t1058613623  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaIdentityConstraint::fields
	XmlSchemaObjectCollection_t395083109 * ___fields_16;
	// System.String System.Xml.Schema.XmlSchemaIdentityConstraint::name
	String_t* ___name_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaIdentityConstraint::qName
	XmlQualifiedName_t1944712516 * ___qName_18;
	// System.Xml.Schema.XmlSchemaXPath System.Xml.Schema.XmlSchemaIdentityConstraint::selector
	XmlSchemaXPath_t604820427 * ___selector_19;
	// Mono.Xml.Schema.XsdIdentitySelector System.Xml.Schema.XmlSchemaIdentityConstraint::compiledSelector
	XsdIdentitySelector_t185499482 * ___compiledSelector_20;

public:
	inline static int32_t get_offset_of_fields_16() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___fields_16)); }
	inline XmlSchemaObjectCollection_t395083109 * get_fields_16() const { return ___fields_16; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_fields_16() { return &___fields_16; }
	inline void set_fields_16(XmlSchemaObjectCollection_t395083109 * value)
	{
		___fields_16 = value;
		Il2CppCodeGenWriteBarrier((&___fields_16), value);
	}

	inline static int32_t get_offset_of_name_17() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___name_17)); }
	inline String_t* get_name_17() const { return ___name_17; }
	inline String_t** get_address_of_name_17() { return &___name_17; }
	inline void set_name_17(String_t* value)
	{
		___name_17 = value;
		Il2CppCodeGenWriteBarrier((&___name_17), value);
	}

	inline static int32_t get_offset_of_qName_18() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___qName_18)); }
	inline XmlQualifiedName_t1944712516 * get_qName_18() const { return ___qName_18; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qName_18() { return &___qName_18; }
	inline void set_qName_18(XmlQualifiedName_t1944712516 * value)
	{
		___qName_18 = value;
		Il2CppCodeGenWriteBarrier((&___qName_18), value);
	}

	inline static int32_t get_offset_of_selector_19() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___selector_19)); }
	inline XmlSchemaXPath_t604820427 * get_selector_19() const { return ___selector_19; }
	inline XmlSchemaXPath_t604820427 ** get_address_of_selector_19() { return &___selector_19; }
	inline void set_selector_19(XmlSchemaXPath_t604820427 * value)
	{
		___selector_19 = value;
		Il2CppCodeGenWriteBarrier((&___selector_19), value);
	}

	inline static int32_t get_offset_of_compiledSelector_20() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_t1058613623, ___compiledSelector_20)); }
	inline XsdIdentitySelector_t185499482 * get_compiledSelector_20() const { return ___compiledSelector_20; }
	inline XsdIdentitySelector_t185499482 ** get_address_of_compiledSelector_20() { return &___compiledSelector_20; }
	inline void set_compiledSelector_20(XsdIdentitySelector_t185499482 * value)
	{
		___compiledSelector_20 = value;
		Il2CppCodeGenWriteBarrier((&___compiledSelector_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAIDENTITYCONSTRAINT_T1058613623_H
#ifndef XMLSCHEMANOTATION_T346281646_H
#define XMLSCHEMANOTATION_T346281646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaNotation
struct  XmlSchemaNotation_t346281646  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaNotation::name
	String_t* ___name_16;
	// System.String System.Xml.Schema.XmlSchemaNotation::pub
	String_t* ___pub_17;
	// System.String System.Xml.Schema.XmlSchemaNotation::system
	String_t* ___system_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaNotation::qualifiedName
	XmlQualifiedName_t1944712516 * ___qualifiedName_19;

public:
	inline static int32_t get_offset_of_name_16() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t346281646, ___name_16)); }
	inline String_t* get_name_16() const { return ___name_16; }
	inline String_t** get_address_of_name_16() { return &___name_16; }
	inline void set_name_16(String_t* value)
	{
		___name_16 = value;
		Il2CppCodeGenWriteBarrier((&___name_16), value);
	}

	inline static int32_t get_offset_of_pub_17() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t346281646, ___pub_17)); }
	inline String_t* get_pub_17() const { return ___pub_17; }
	inline String_t** get_address_of_pub_17() { return &___pub_17; }
	inline void set_pub_17(String_t* value)
	{
		___pub_17 = value;
		Il2CppCodeGenWriteBarrier((&___pub_17), value);
	}

	inline static int32_t get_offset_of_system_18() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t346281646, ___system_18)); }
	inline String_t* get_system_18() const { return ___system_18; }
	inline String_t** get_address_of_system_18() { return &___system_18; }
	inline void set_system_18(String_t* value)
	{
		___system_18 = value;
		Il2CppCodeGenWriteBarrier((&___system_18), value);
	}

	inline static int32_t get_offset_of_qualifiedName_19() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t346281646, ___qualifiedName_19)); }
	inline XmlQualifiedName_t1944712516 * get_qualifiedName_19() const { return ___qualifiedName_19; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qualifiedName_19() { return &___qualifiedName_19; }
	inline void set_qualifiedName_19(XmlQualifiedName_t1944712516 * value)
	{
		___qualifiedName_19 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMANOTATION_T346281646_H
#ifndef XMLSCHEMAPARTICLE_T3365045970_H
#define XMLSCHEMAPARTICLE_T3365045970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle
struct  XmlSchemaParticle_t3365045970  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::minOccurs
	Decimal_t724701077  ___minOccurs_16;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::maxOccurs
	Decimal_t724701077  ___maxOccurs_17;
	// System.String System.Xml.Schema.XmlSchemaParticle::minstr
	String_t* ___minstr_18;
	// System.String System.Xml.Schema.XmlSchemaParticle::maxstr
	String_t* ___maxstr_19;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::validatedMinOccurs
	Decimal_t724701077  ___validatedMinOccurs_21;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::validatedMaxOccurs
	Decimal_t724701077  ___validatedMaxOccurs_22;
	// System.Int32 System.Xml.Schema.XmlSchemaParticle::recursionDepth
	int32_t ___recursionDepth_23;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::minEffectiveTotalRange
	Decimal_t724701077  ___minEffectiveTotalRange_24;
	// System.Boolean System.Xml.Schema.XmlSchemaParticle::parentIsGroupDefinition
	bool ___parentIsGroupDefinition_25;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaParticle::OptimizedParticle
	XmlSchemaParticle_t3365045970 * ___OptimizedParticle_26;

public:
	inline static int32_t get_offset_of_minOccurs_16() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___minOccurs_16)); }
	inline Decimal_t724701077  get_minOccurs_16() const { return ___minOccurs_16; }
	inline Decimal_t724701077 * get_address_of_minOccurs_16() { return &___minOccurs_16; }
	inline void set_minOccurs_16(Decimal_t724701077  value)
	{
		___minOccurs_16 = value;
	}

	inline static int32_t get_offset_of_maxOccurs_17() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___maxOccurs_17)); }
	inline Decimal_t724701077  get_maxOccurs_17() const { return ___maxOccurs_17; }
	inline Decimal_t724701077 * get_address_of_maxOccurs_17() { return &___maxOccurs_17; }
	inline void set_maxOccurs_17(Decimal_t724701077  value)
	{
		___maxOccurs_17 = value;
	}

	inline static int32_t get_offset_of_minstr_18() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___minstr_18)); }
	inline String_t* get_minstr_18() const { return ___minstr_18; }
	inline String_t** get_address_of_minstr_18() { return &___minstr_18; }
	inline void set_minstr_18(String_t* value)
	{
		___minstr_18 = value;
		Il2CppCodeGenWriteBarrier((&___minstr_18), value);
	}

	inline static int32_t get_offset_of_maxstr_19() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___maxstr_19)); }
	inline String_t* get_maxstr_19() const { return ___maxstr_19; }
	inline String_t** get_address_of_maxstr_19() { return &___maxstr_19; }
	inline void set_maxstr_19(String_t* value)
	{
		___maxstr_19 = value;
		Il2CppCodeGenWriteBarrier((&___maxstr_19), value);
	}

	inline static int32_t get_offset_of_validatedMinOccurs_21() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___validatedMinOccurs_21)); }
	inline Decimal_t724701077  get_validatedMinOccurs_21() const { return ___validatedMinOccurs_21; }
	inline Decimal_t724701077 * get_address_of_validatedMinOccurs_21() { return &___validatedMinOccurs_21; }
	inline void set_validatedMinOccurs_21(Decimal_t724701077  value)
	{
		___validatedMinOccurs_21 = value;
	}

	inline static int32_t get_offset_of_validatedMaxOccurs_22() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___validatedMaxOccurs_22)); }
	inline Decimal_t724701077  get_validatedMaxOccurs_22() const { return ___validatedMaxOccurs_22; }
	inline Decimal_t724701077 * get_address_of_validatedMaxOccurs_22() { return &___validatedMaxOccurs_22; }
	inline void set_validatedMaxOccurs_22(Decimal_t724701077  value)
	{
		___validatedMaxOccurs_22 = value;
	}

	inline static int32_t get_offset_of_recursionDepth_23() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___recursionDepth_23)); }
	inline int32_t get_recursionDepth_23() const { return ___recursionDepth_23; }
	inline int32_t* get_address_of_recursionDepth_23() { return &___recursionDepth_23; }
	inline void set_recursionDepth_23(int32_t value)
	{
		___recursionDepth_23 = value;
	}

	inline static int32_t get_offset_of_minEffectiveTotalRange_24() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___minEffectiveTotalRange_24)); }
	inline Decimal_t724701077  get_minEffectiveTotalRange_24() const { return ___minEffectiveTotalRange_24; }
	inline Decimal_t724701077 * get_address_of_minEffectiveTotalRange_24() { return &___minEffectiveTotalRange_24; }
	inline void set_minEffectiveTotalRange_24(Decimal_t724701077  value)
	{
		___minEffectiveTotalRange_24 = value;
	}

	inline static int32_t get_offset_of_parentIsGroupDefinition_25() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___parentIsGroupDefinition_25)); }
	inline bool get_parentIsGroupDefinition_25() const { return ___parentIsGroupDefinition_25; }
	inline bool* get_address_of_parentIsGroupDefinition_25() { return &___parentIsGroupDefinition_25; }
	inline void set_parentIsGroupDefinition_25(bool value)
	{
		___parentIsGroupDefinition_25 = value;
	}

	inline static int32_t get_offset_of_OptimizedParticle_26() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970, ___OptimizedParticle_26)); }
	inline XmlSchemaParticle_t3365045970 * get_OptimizedParticle_26() const { return ___OptimizedParticle_26; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_OptimizedParticle_26() { return &___OptimizedParticle_26; }
	inline void set_OptimizedParticle_26(XmlSchemaParticle_t3365045970 * value)
	{
		___OptimizedParticle_26 = value;
		Il2CppCodeGenWriteBarrier((&___OptimizedParticle_26), value);
	}
};

struct XmlSchemaParticle_t3365045970_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaParticle::empty
	XmlSchemaParticle_t3365045970 * ___empty_20;

public:
	inline static int32_t get_offset_of_empty_20() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t3365045970_StaticFields, ___empty_20)); }
	inline XmlSchemaParticle_t3365045970 * get_empty_20() const { return ___empty_20; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_empty_20() { return &___empty_20; }
	inline void set_empty_20(XmlSchemaParticle_t3365045970 * value)
	{
		___empty_20 = value;
		Il2CppCodeGenWriteBarrier((&___empty_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPARTICLE_T3365045970_H
#ifndef XMLSCHEMAIMPORT_T250324363_H
#define XMLSCHEMAIMPORT_T250324363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaImport
struct  XmlSchemaImport_t250324363  : public XmlSchemaExternal_t3943748629
{
public:
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaImport::annotation
	XmlSchemaAnnotation_t2400301303 * ___annotation_16;
	// System.String System.Xml.Schema.XmlSchemaImport::nameSpace
	String_t* ___nameSpace_17;

public:
	inline static int32_t get_offset_of_annotation_16() { return static_cast<int32_t>(offsetof(XmlSchemaImport_t250324363, ___annotation_16)); }
	inline XmlSchemaAnnotation_t2400301303 * get_annotation_16() const { return ___annotation_16; }
	inline XmlSchemaAnnotation_t2400301303 ** get_address_of_annotation_16() { return &___annotation_16; }
	inline void set_annotation_16(XmlSchemaAnnotation_t2400301303 * value)
	{
		___annotation_16 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_16), value);
	}

	inline static int32_t get_offset_of_nameSpace_17() { return static_cast<int32_t>(offsetof(XmlSchemaImport_t250324363, ___nameSpace_17)); }
	inline String_t* get_nameSpace_17() const { return ___nameSpace_17; }
	inline String_t** get_address_of_nameSpace_17() { return &___nameSpace_17; }
	inline void set_nameSpace_17(String_t* value)
	{
		___nameSpace_17 = value;
		Il2CppCodeGenWriteBarrier((&___nameSpace_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAIMPORT_T250324363_H
#ifndef XMLATOMICVALUE_T752869371_H
#define XMLATOMICVALUE_T752869371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlAtomicValue
struct  XmlAtomicValue_t752869371  : public XPathItem_t3130801258
{
public:
	// System.Boolean System.Xml.Schema.XmlAtomicValue::booleanValue
	bool ___booleanValue_0;
	// System.DateTime System.Xml.Schema.XmlAtomicValue::dateTimeValue
	DateTime_t693205669  ___dateTimeValue_1;
	// System.Decimal System.Xml.Schema.XmlAtomicValue::decimalValue
	Decimal_t724701077  ___decimalValue_2;
	// System.Double System.Xml.Schema.XmlAtomicValue::doubleValue
	double ___doubleValue_3;
	// System.Int32 System.Xml.Schema.XmlAtomicValue::intValue
	int32_t ___intValue_4;
	// System.Int64 System.Xml.Schema.XmlAtomicValue::longValue
	int64_t ___longValue_5;
	// System.Object System.Xml.Schema.XmlAtomicValue::objectValue
	RuntimeObject * ___objectValue_6;
	// System.Single System.Xml.Schema.XmlAtomicValue::floatValue
	float ___floatValue_7;
	// System.String System.Xml.Schema.XmlAtomicValue::stringValue
	String_t* ___stringValue_8;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlAtomicValue::schemaType
	XmlSchemaType_t1795078578 * ___schemaType_9;
	// System.Xml.Schema.XmlTypeCode System.Xml.Schema.XmlAtomicValue::xmlTypeCode
	int32_t ___xmlTypeCode_10;

public:
	inline static int32_t get_offset_of_booleanValue_0() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___booleanValue_0)); }
	inline bool get_booleanValue_0() const { return ___booleanValue_0; }
	inline bool* get_address_of_booleanValue_0() { return &___booleanValue_0; }
	inline void set_booleanValue_0(bool value)
	{
		___booleanValue_0 = value;
	}

	inline static int32_t get_offset_of_dateTimeValue_1() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___dateTimeValue_1)); }
	inline DateTime_t693205669  get_dateTimeValue_1() const { return ___dateTimeValue_1; }
	inline DateTime_t693205669 * get_address_of_dateTimeValue_1() { return &___dateTimeValue_1; }
	inline void set_dateTimeValue_1(DateTime_t693205669  value)
	{
		___dateTimeValue_1 = value;
	}

	inline static int32_t get_offset_of_decimalValue_2() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___decimalValue_2)); }
	inline Decimal_t724701077  get_decimalValue_2() const { return ___decimalValue_2; }
	inline Decimal_t724701077 * get_address_of_decimalValue_2() { return &___decimalValue_2; }
	inline void set_decimalValue_2(Decimal_t724701077  value)
	{
		___decimalValue_2 = value;
	}

	inline static int32_t get_offset_of_doubleValue_3() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___doubleValue_3)); }
	inline double get_doubleValue_3() const { return ___doubleValue_3; }
	inline double* get_address_of_doubleValue_3() { return &___doubleValue_3; }
	inline void set_doubleValue_3(double value)
	{
		___doubleValue_3 = value;
	}

	inline static int32_t get_offset_of_intValue_4() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___intValue_4)); }
	inline int32_t get_intValue_4() const { return ___intValue_4; }
	inline int32_t* get_address_of_intValue_4() { return &___intValue_4; }
	inline void set_intValue_4(int32_t value)
	{
		___intValue_4 = value;
	}

	inline static int32_t get_offset_of_longValue_5() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___longValue_5)); }
	inline int64_t get_longValue_5() const { return ___longValue_5; }
	inline int64_t* get_address_of_longValue_5() { return &___longValue_5; }
	inline void set_longValue_5(int64_t value)
	{
		___longValue_5 = value;
	}

	inline static int32_t get_offset_of_objectValue_6() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___objectValue_6)); }
	inline RuntimeObject * get_objectValue_6() const { return ___objectValue_6; }
	inline RuntimeObject ** get_address_of_objectValue_6() { return &___objectValue_6; }
	inline void set_objectValue_6(RuntimeObject * value)
	{
		___objectValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___objectValue_6), value);
	}

	inline static int32_t get_offset_of_floatValue_7() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___floatValue_7)); }
	inline float get_floatValue_7() const { return ___floatValue_7; }
	inline float* get_address_of_floatValue_7() { return &___floatValue_7; }
	inline void set_floatValue_7(float value)
	{
		___floatValue_7 = value;
	}

	inline static int32_t get_offset_of_stringValue_8() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___stringValue_8)); }
	inline String_t* get_stringValue_8() const { return ___stringValue_8; }
	inline String_t** get_address_of_stringValue_8() { return &___stringValue_8; }
	inline void set_stringValue_8(String_t* value)
	{
		___stringValue_8 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_8), value);
	}

	inline static int32_t get_offset_of_schemaType_9() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___schemaType_9)); }
	inline XmlSchemaType_t1795078578 * get_schemaType_9() const { return ___schemaType_9; }
	inline XmlSchemaType_t1795078578 ** get_address_of_schemaType_9() { return &___schemaType_9; }
	inline void set_schemaType_9(XmlSchemaType_t1795078578 * value)
	{
		___schemaType_9 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_9), value);
	}

	inline static int32_t get_offset_of_xmlTypeCode_10() { return static_cast<int32_t>(offsetof(XmlAtomicValue_t752869371, ___xmlTypeCode_10)); }
	inline int32_t get_xmlTypeCode_10() const { return ___xmlTypeCode_10; }
	inline int32_t* get_address_of_xmlTypeCode_10() { return &___xmlTypeCode_10; }
	inline void set_xmlTypeCode_10(int32_t value)
	{
		___xmlTypeCode_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATOMICVALUE_T752869371_H
#ifndef XMLSCHEMAREDEFINE_T3478619248_H
#define XMLSCHEMAREDEFINE_T3478619248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaRedefine
struct  XmlSchemaRedefine_t3478619248  : public XmlSchemaExternal_t3943748629
{
public:
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::attributeGroups
	XmlSchemaObjectTable_t3364835593 * ___attributeGroups_16;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::groups
	XmlSchemaObjectTable_t3364835593 * ___groups_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaRedefine::items
	XmlSchemaObjectCollection_t395083109 * ___items_18;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::schemaTypes
	XmlSchemaObjectTable_t3364835593 * ___schemaTypes_19;

public:
	inline static int32_t get_offset_of_attributeGroups_16() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t3478619248, ___attributeGroups_16)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributeGroups_16() const { return ___attributeGroups_16; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributeGroups_16() { return &___attributeGroups_16; }
	inline void set_attributeGroups_16(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributeGroups_16 = value;
		Il2CppCodeGenWriteBarrier((&___attributeGroups_16), value);
	}

	inline static int32_t get_offset_of_groups_17() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t3478619248, ___groups_17)); }
	inline XmlSchemaObjectTable_t3364835593 * get_groups_17() const { return ___groups_17; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_groups_17() { return &___groups_17; }
	inline void set_groups_17(XmlSchemaObjectTable_t3364835593 * value)
	{
		___groups_17 = value;
		Il2CppCodeGenWriteBarrier((&___groups_17), value);
	}

	inline static int32_t get_offset_of_items_18() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t3478619248, ___items_18)); }
	inline XmlSchemaObjectCollection_t395083109 * get_items_18() const { return ___items_18; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_items_18() { return &___items_18; }
	inline void set_items_18(XmlSchemaObjectCollection_t395083109 * value)
	{
		___items_18 = value;
		Il2CppCodeGenWriteBarrier((&___items_18), value);
	}

	inline static int32_t get_offset_of_schemaTypes_19() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t3478619248, ___schemaTypes_19)); }
	inline XmlSchemaObjectTable_t3364835593 * get_schemaTypes_19() const { return ___schemaTypes_19; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_schemaTypes_19() { return &___schemaTypes_19; }
	inline void set_schemaTypes_19(XmlSchemaObjectTable_t3364835593 * value)
	{
		___schemaTypes_19 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypes_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAREDEFINE_T3478619248_H
#ifndef XSDANYSIMPLETYPE_T1096449895_H
#define XSDANYSIMPLETYPE_T1096449895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnySimpleType
struct  XsdAnySimpleType_t1096449895  : public XmlSchemaDatatype_t1195946242
{
public:

public:
};

struct XsdAnySimpleType_t1096449895_StaticFields
{
public:
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdAnySimpleType::instance
	XsdAnySimpleType_t1096449895 * ___instance_55;
	// System.Char[] Mono.Xml.Schema.XsdAnySimpleType::whitespaceArray
	CharU5BU5D_t1328083999* ___whitespaceArray_56;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::booleanAllowedFacets
	int32_t ___booleanAllowedFacets_57;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::decimalAllowedFacets
	int32_t ___decimalAllowedFacets_58;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::durationAllowedFacets
	int32_t ___durationAllowedFacets_59;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::stringAllowedFacets
	int32_t ___stringAllowedFacets_60;

public:
	inline static int32_t get_offset_of_instance_55() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___instance_55)); }
	inline XsdAnySimpleType_t1096449895 * get_instance_55() const { return ___instance_55; }
	inline XsdAnySimpleType_t1096449895 ** get_address_of_instance_55() { return &___instance_55; }
	inline void set_instance_55(XsdAnySimpleType_t1096449895 * value)
	{
		___instance_55 = value;
		Il2CppCodeGenWriteBarrier((&___instance_55), value);
	}

	inline static int32_t get_offset_of_whitespaceArray_56() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___whitespaceArray_56)); }
	inline CharU5BU5D_t1328083999* get_whitespaceArray_56() const { return ___whitespaceArray_56; }
	inline CharU5BU5D_t1328083999** get_address_of_whitespaceArray_56() { return &___whitespaceArray_56; }
	inline void set_whitespaceArray_56(CharU5BU5D_t1328083999* value)
	{
		___whitespaceArray_56 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceArray_56), value);
	}

	inline static int32_t get_offset_of_booleanAllowedFacets_57() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___booleanAllowedFacets_57)); }
	inline int32_t get_booleanAllowedFacets_57() const { return ___booleanAllowedFacets_57; }
	inline int32_t* get_address_of_booleanAllowedFacets_57() { return &___booleanAllowedFacets_57; }
	inline void set_booleanAllowedFacets_57(int32_t value)
	{
		___booleanAllowedFacets_57 = value;
	}

	inline static int32_t get_offset_of_decimalAllowedFacets_58() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___decimalAllowedFacets_58)); }
	inline int32_t get_decimalAllowedFacets_58() const { return ___decimalAllowedFacets_58; }
	inline int32_t* get_address_of_decimalAllowedFacets_58() { return &___decimalAllowedFacets_58; }
	inline void set_decimalAllowedFacets_58(int32_t value)
	{
		___decimalAllowedFacets_58 = value;
	}

	inline static int32_t get_offset_of_durationAllowedFacets_59() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___durationAllowedFacets_59)); }
	inline int32_t get_durationAllowedFacets_59() const { return ___durationAllowedFacets_59; }
	inline int32_t* get_address_of_durationAllowedFacets_59() { return &___durationAllowedFacets_59; }
	inline void set_durationAllowedFacets_59(int32_t value)
	{
		___durationAllowedFacets_59 = value;
	}

	inline static int32_t get_offset_of_stringAllowedFacets_60() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___stringAllowedFacets_60)); }
	inline int32_t get_stringAllowedFacets_60() const { return ___stringAllowedFacets_60; }
	inline int32_t* get_address_of_stringAllowedFacets_60() { return &___stringAllowedFacets_60; }
	inline void set_stringAllowedFacets_60(int32_t value)
	{
		___stringAllowedFacets_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYSIMPLETYPE_T1096449895_H
#ifndef XMLSCHEMAANYATTRIBUTE_T530453212_H
#define XMLSCHEMAANYATTRIBUTE_T530453212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnyAttribute
struct  XmlSchemaAnyAttribute_t530453212  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaAnyAttribute::nameSpace
	String_t* ___nameSpace_16;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XmlSchemaAnyAttribute::processing
	int32_t ___processing_17;
	// Mono.Xml.Schema.XsdWildcard System.Xml.Schema.XmlSchemaAnyAttribute::wildcard
	XsdWildcard_t625524157 * ___wildcard_18;

public:
	inline static int32_t get_offset_of_nameSpace_16() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t530453212, ___nameSpace_16)); }
	inline String_t* get_nameSpace_16() const { return ___nameSpace_16; }
	inline String_t** get_address_of_nameSpace_16() { return &___nameSpace_16; }
	inline void set_nameSpace_16(String_t* value)
	{
		___nameSpace_16 = value;
		Il2CppCodeGenWriteBarrier((&___nameSpace_16), value);
	}

	inline static int32_t get_offset_of_processing_17() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t530453212, ___processing_17)); }
	inline int32_t get_processing_17() const { return ___processing_17; }
	inline int32_t* get_address_of_processing_17() { return &___processing_17; }
	inline void set_processing_17(int32_t value)
	{
		___processing_17 = value;
	}

	inline static int32_t get_offset_of_wildcard_18() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t530453212, ___wildcard_18)); }
	inline XsdWildcard_t625524157 * get_wildcard_18() const { return ___wildcard_18; }
	inline XsdWildcard_t625524157 ** get_address_of_wildcard_18() { return &___wildcard_18; }
	inline void set_wildcard_18(XsdWildcard_t625524157 * value)
	{
		___wildcard_18 = value;
		Il2CppCodeGenWriteBarrier((&___wildcard_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANYATTRIBUTE_T530453212_H
#ifndef XMLSCHEMACONTENT_T3733871217_H
#define XMLSCHEMACONTENT_T3733871217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContent
struct  XmlSchemaContent_t3733871217  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Object System.Xml.Schema.XmlSchemaContent::actualBaseSchemaType
	RuntimeObject * ___actualBaseSchemaType_16;

public:
	inline static int32_t get_offset_of_actualBaseSchemaType_16() { return static_cast<int32_t>(offsetof(XmlSchemaContent_t3733871217, ___actualBaseSchemaType_16)); }
	inline RuntimeObject * get_actualBaseSchemaType_16() const { return ___actualBaseSchemaType_16; }
	inline RuntimeObject ** get_address_of_actualBaseSchemaType_16() { return &___actualBaseSchemaType_16; }
	inline void set_actualBaseSchemaType_16(RuntimeObject * value)
	{
		___actualBaseSchemaType_16 = value;
		Il2CppCodeGenWriteBarrier((&___actualBaseSchemaType_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENT_T3733871217_H
#ifndef XMLSCHEMACONTENTMODEL_T907989596_H
#define XMLSCHEMACONTENTMODEL_T907989596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentModel
struct  XmlSchemaContentModel_t907989596  : public XmlSchemaAnnotated_t2082486936
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTMODEL_T907989596_H
#ifndef XMLSCHEMATYPE_T1795078578_H
#define XMLSCHEMATYPE_T1795078578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaType
struct  XmlSchemaType_t1795078578  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::final
	int32_t ___final_16;
	// System.Boolean System.Xml.Schema.XmlSchemaType::isMixed
	bool ___isMixed_17;
	// System.String System.Xml.Schema.XmlSchemaType::name
	String_t* ___name_18;
	// System.Boolean System.Xml.Schema.XmlSchemaType::recursed
	bool ___recursed_19;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::BaseSchemaTypeName
	XmlQualifiedName_t1944712516 * ___BaseSchemaTypeName_20;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaType::BaseXmlSchemaTypeInternal
	XmlSchemaType_t1795078578 * ___BaseXmlSchemaTypeInternal_21;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaType::DatatypeInternal
	XmlSchemaDatatype_t1195946242 * ___DatatypeInternal_22;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::resolvedDerivedBy
	int32_t ___resolvedDerivedBy_23;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::finalResolved
	int32_t ___finalResolved_24;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::QNameInternal
	XmlQualifiedName_t1944712516 * ___QNameInternal_25;

public:
	inline static int32_t get_offset_of_final_16() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___final_16)); }
	inline int32_t get_final_16() const { return ___final_16; }
	inline int32_t* get_address_of_final_16() { return &___final_16; }
	inline void set_final_16(int32_t value)
	{
		___final_16 = value;
	}

	inline static int32_t get_offset_of_isMixed_17() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___isMixed_17)); }
	inline bool get_isMixed_17() const { return ___isMixed_17; }
	inline bool* get_address_of_isMixed_17() { return &___isMixed_17; }
	inline void set_isMixed_17(bool value)
	{
		___isMixed_17 = value;
	}

	inline static int32_t get_offset_of_name_18() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___name_18)); }
	inline String_t* get_name_18() const { return ___name_18; }
	inline String_t** get_address_of_name_18() { return &___name_18; }
	inline void set_name_18(String_t* value)
	{
		___name_18 = value;
		Il2CppCodeGenWriteBarrier((&___name_18), value);
	}

	inline static int32_t get_offset_of_recursed_19() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___recursed_19)); }
	inline bool get_recursed_19() const { return ___recursed_19; }
	inline bool* get_address_of_recursed_19() { return &___recursed_19; }
	inline void set_recursed_19(bool value)
	{
		___recursed_19 = value;
	}

	inline static int32_t get_offset_of_BaseSchemaTypeName_20() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___BaseSchemaTypeName_20)); }
	inline XmlQualifiedName_t1944712516 * get_BaseSchemaTypeName_20() const { return ___BaseSchemaTypeName_20; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_BaseSchemaTypeName_20() { return &___BaseSchemaTypeName_20; }
	inline void set_BaseSchemaTypeName_20(XmlQualifiedName_t1944712516 * value)
	{
		___BaseSchemaTypeName_20 = value;
		Il2CppCodeGenWriteBarrier((&___BaseSchemaTypeName_20), value);
	}

	inline static int32_t get_offset_of_BaseXmlSchemaTypeInternal_21() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___BaseXmlSchemaTypeInternal_21)); }
	inline XmlSchemaType_t1795078578 * get_BaseXmlSchemaTypeInternal_21() const { return ___BaseXmlSchemaTypeInternal_21; }
	inline XmlSchemaType_t1795078578 ** get_address_of_BaseXmlSchemaTypeInternal_21() { return &___BaseXmlSchemaTypeInternal_21; }
	inline void set_BaseXmlSchemaTypeInternal_21(XmlSchemaType_t1795078578 * value)
	{
		___BaseXmlSchemaTypeInternal_21 = value;
		Il2CppCodeGenWriteBarrier((&___BaseXmlSchemaTypeInternal_21), value);
	}

	inline static int32_t get_offset_of_DatatypeInternal_22() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___DatatypeInternal_22)); }
	inline XmlSchemaDatatype_t1195946242 * get_DatatypeInternal_22() const { return ___DatatypeInternal_22; }
	inline XmlSchemaDatatype_t1195946242 ** get_address_of_DatatypeInternal_22() { return &___DatatypeInternal_22; }
	inline void set_DatatypeInternal_22(XmlSchemaDatatype_t1195946242 * value)
	{
		___DatatypeInternal_22 = value;
		Il2CppCodeGenWriteBarrier((&___DatatypeInternal_22), value);
	}

	inline static int32_t get_offset_of_resolvedDerivedBy_23() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___resolvedDerivedBy_23)); }
	inline int32_t get_resolvedDerivedBy_23() const { return ___resolvedDerivedBy_23; }
	inline int32_t* get_address_of_resolvedDerivedBy_23() { return &___resolvedDerivedBy_23; }
	inline void set_resolvedDerivedBy_23(int32_t value)
	{
		___resolvedDerivedBy_23 = value;
	}

	inline static int32_t get_offset_of_finalResolved_24() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___finalResolved_24)); }
	inline int32_t get_finalResolved_24() const { return ___finalResolved_24; }
	inline int32_t* get_address_of_finalResolved_24() { return &___finalResolved_24; }
	inline void set_finalResolved_24(int32_t value)
	{
		___finalResolved_24 = value;
	}

	inline static int32_t get_offset_of_QNameInternal_25() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578, ___QNameInternal_25)); }
	inline XmlQualifiedName_t1944712516 * get_QNameInternal_25() const { return ___QNameInternal_25; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_QNameInternal_25() { return &___QNameInternal_25; }
	inline void set_QNameInternal_25(XmlQualifiedName_t1944712516 * value)
	{
		___QNameInternal_25 = value;
		Il2CppCodeGenWriteBarrier((&___QNameInternal_25), value);
	}
};

struct XmlSchemaType_t1795078578_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switch$map2E
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2E_26;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switch$map2F
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2F_27;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2E_26() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578_StaticFields, ___U3CU3Ef__switchU24map2E_26)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2E_26() const { return ___U3CU3Ef__switchU24map2E_26; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2E_26() { return &___U3CU3Ef__switchU24map2E_26; }
	inline void set_U3CU3Ef__switchU24map2E_26(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2E_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2E_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2F_27() { return static_cast<int32_t>(offsetof(XmlSchemaType_t1795078578_StaticFields, ___U3CU3Ef__switchU24map2F_27)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2F_27() const { return ___U3CU3Ef__switchU24map2F_27; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2F_27() { return &___U3CU3Ef__switchU24map2F_27; }
	inline void set_U3CU3Ef__switchU24map2F_27(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2F_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2F_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMATYPE_T1795078578_H
#ifndef XMLSCHEMAATTRIBUTE_T4015859774_H
#define XMLSCHEMAATTRIBUTE_T4015859774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttribute
struct  XmlSchemaAttribute_t4015859774  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Object System.Xml.Schema.XmlSchemaAttribute::attributeType
	RuntimeObject * ___attributeType_16;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaAttribute::attributeSchemaType
	XmlSchemaSimpleType_t248156492 * ___attributeSchemaType_17;
	// System.String System.Xml.Schema.XmlSchemaAttribute::defaultValue
	String_t* ___defaultValue_18;
	// System.String System.Xml.Schema.XmlSchemaAttribute::fixedValue
	String_t* ___fixedValue_19;
	// System.String System.Xml.Schema.XmlSchemaAttribute::validatedDefaultValue
	String_t* ___validatedDefaultValue_20;
	// System.String System.Xml.Schema.XmlSchemaAttribute::validatedFixedValue
	String_t* ___validatedFixedValue_21;
	// System.Object System.Xml.Schema.XmlSchemaAttribute::validatedFixedTypedValue
	RuntimeObject * ___validatedFixedTypedValue_22;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchemaAttribute::form
	int32_t ___form_23;
	// System.String System.Xml.Schema.XmlSchemaAttribute::name
	String_t* ___name_24;
	// System.String System.Xml.Schema.XmlSchemaAttribute::targetNamespace
	String_t* ___targetNamespace_25;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::qualifiedName
	XmlQualifiedName_t1944712516 * ___qualifiedName_26;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::refName
	XmlQualifiedName_t1944712516 * ___refName_27;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaAttribute::schemaType
	XmlSchemaSimpleType_t248156492 * ___schemaType_28;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::schemaTypeName
	XmlQualifiedName_t1944712516 * ___schemaTypeName_29;
	// System.Xml.Schema.XmlSchemaUse System.Xml.Schema.XmlSchemaAttribute::use
	int32_t ___use_30;
	// System.Xml.Schema.XmlSchemaUse System.Xml.Schema.XmlSchemaAttribute::validatedUse
	int32_t ___validatedUse_31;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaAttribute::referencedAttribute
	XmlSchemaAttribute_t4015859774 * ___referencedAttribute_32;

public:
	inline static int32_t get_offset_of_attributeType_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___attributeType_16)); }
	inline RuntimeObject * get_attributeType_16() const { return ___attributeType_16; }
	inline RuntimeObject ** get_address_of_attributeType_16() { return &___attributeType_16; }
	inline void set_attributeType_16(RuntimeObject * value)
	{
		___attributeType_16 = value;
		Il2CppCodeGenWriteBarrier((&___attributeType_16), value);
	}

	inline static int32_t get_offset_of_attributeSchemaType_17() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___attributeSchemaType_17)); }
	inline XmlSchemaSimpleType_t248156492 * get_attributeSchemaType_17() const { return ___attributeSchemaType_17; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_attributeSchemaType_17() { return &___attributeSchemaType_17; }
	inline void set_attributeSchemaType_17(XmlSchemaSimpleType_t248156492 * value)
	{
		___attributeSchemaType_17 = value;
		Il2CppCodeGenWriteBarrier((&___attributeSchemaType_17), value);
	}

	inline static int32_t get_offset_of_defaultValue_18() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___defaultValue_18)); }
	inline String_t* get_defaultValue_18() const { return ___defaultValue_18; }
	inline String_t** get_address_of_defaultValue_18() { return &___defaultValue_18; }
	inline void set_defaultValue_18(String_t* value)
	{
		___defaultValue_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_18), value);
	}

	inline static int32_t get_offset_of_fixedValue_19() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___fixedValue_19)); }
	inline String_t* get_fixedValue_19() const { return ___fixedValue_19; }
	inline String_t** get_address_of_fixedValue_19() { return &___fixedValue_19; }
	inline void set_fixedValue_19(String_t* value)
	{
		___fixedValue_19 = value;
		Il2CppCodeGenWriteBarrier((&___fixedValue_19), value);
	}

	inline static int32_t get_offset_of_validatedDefaultValue_20() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___validatedDefaultValue_20)); }
	inline String_t* get_validatedDefaultValue_20() const { return ___validatedDefaultValue_20; }
	inline String_t** get_address_of_validatedDefaultValue_20() { return &___validatedDefaultValue_20; }
	inline void set_validatedDefaultValue_20(String_t* value)
	{
		___validatedDefaultValue_20 = value;
		Il2CppCodeGenWriteBarrier((&___validatedDefaultValue_20), value);
	}

	inline static int32_t get_offset_of_validatedFixedValue_21() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___validatedFixedValue_21)); }
	inline String_t* get_validatedFixedValue_21() const { return ___validatedFixedValue_21; }
	inline String_t** get_address_of_validatedFixedValue_21() { return &___validatedFixedValue_21; }
	inline void set_validatedFixedValue_21(String_t* value)
	{
		___validatedFixedValue_21 = value;
		Il2CppCodeGenWriteBarrier((&___validatedFixedValue_21), value);
	}

	inline static int32_t get_offset_of_validatedFixedTypedValue_22() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___validatedFixedTypedValue_22)); }
	inline RuntimeObject * get_validatedFixedTypedValue_22() const { return ___validatedFixedTypedValue_22; }
	inline RuntimeObject ** get_address_of_validatedFixedTypedValue_22() { return &___validatedFixedTypedValue_22; }
	inline void set_validatedFixedTypedValue_22(RuntimeObject * value)
	{
		___validatedFixedTypedValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___validatedFixedTypedValue_22), value);
	}

	inline static int32_t get_offset_of_form_23() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___form_23)); }
	inline int32_t get_form_23() const { return ___form_23; }
	inline int32_t* get_address_of_form_23() { return &___form_23; }
	inline void set_form_23(int32_t value)
	{
		___form_23 = value;
	}

	inline static int32_t get_offset_of_name_24() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___name_24)); }
	inline String_t* get_name_24() const { return ___name_24; }
	inline String_t** get_address_of_name_24() { return &___name_24; }
	inline void set_name_24(String_t* value)
	{
		___name_24 = value;
		Il2CppCodeGenWriteBarrier((&___name_24), value);
	}

	inline static int32_t get_offset_of_targetNamespace_25() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___targetNamespace_25)); }
	inline String_t* get_targetNamespace_25() const { return ___targetNamespace_25; }
	inline String_t** get_address_of_targetNamespace_25() { return &___targetNamespace_25; }
	inline void set_targetNamespace_25(String_t* value)
	{
		___targetNamespace_25 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespace_25), value);
	}

	inline static int32_t get_offset_of_qualifiedName_26() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___qualifiedName_26)); }
	inline XmlQualifiedName_t1944712516 * get_qualifiedName_26() const { return ___qualifiedName_26; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qualifiedName_26() { return &___qualifiedName_26; }
	inline void set_qualifiedName_26(XmlQualifiedName_t1944712516 * value)
	{
		___qualifiedName_26 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_26), value);
	}

	inline static int32_t get_offset_of_refName_27() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___refName_27)); }
	inline XmlQualifiedName_t1944712516 * get_refName_27() const { return ___refName_27; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refName_27() { return &___refName_27; }
	inline void set_refName_27(XmlQualifiedName_t1944712516 * value)
	{
		___refName_27 = value;
		Il2CppCodeGenWriteBarrier((&___refName_27), value);
	}

	inline static int32_t get_offset_of_schemaType_28() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___schemaType_28)); }
	inline XmlSchemaSimpleType_t248156492 * get_schemaType_28() const { return ___schemaType_28; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_schemaType_28() { return &___schemaType_28; }
	inline void set_schemaType_28(XmlSchemaSimpleType_t248156492 * value)
	{
		___schemaType_28 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_28), value);
	}

	inline static int32_t get_offset_of_schemaTypeName_29() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___schemaTypeName_29)); }
	inline XmlQualifiedName_t1944712516 * get_schemaTypeName_29() const { return ___schemaTypeName_29; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_schemaTypeName_29() { return &___schemaTypeName_29; }
	inline void set_schemaTypeName_29(XmlQualifiedName_t1944712516 * value)
	{
		___schemaTypeName_29 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypeName_29), value);
	}

	inline static int32_t get_offset_of_use_30() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___use_30)); }
	inline int32_t get_use_30() const { return ___use_30; }
	inline int32_t* get_address_of_use_30() { return &___use_30; }
	inline void set_use_30(int32_t value)
	{
		___use_30 = value;
	}

	inline static int32_t get_offset_of_validatedUse_31() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___validatedUse_31)); }
	inline int32_t get_validatedUse_31() const { return ___validatedUse_31; }
	inline int32_t* get_address_of_validatedUse_31() { return &___validatedUse_31; }
	inline void set_validatedUse_31(int32_t value)
	{
		___validatedUse_31 = value;
	}

	inline static int32_t get_offset_of_referencedAttribute_32() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t4015859774, ___referencedAttribute_32)); }
	inline XmlSchemaAttribute_t4015859774 * get_referencedAttribute_32() const { return ___referencedAttribute_32; }
	inline XmlSchemaAttribute_t4015859774 ** get_address_of_referencedAttribute_32() { return &___referencedAttribute_32; }
	inline void set_referencedAttribute_32(XmlSchemaAttribute_t4015859774 * value)
	{
		___referencedAttribute_32 = value;
		Il2CppCodeGenWriteBarrier((&___referencedAttribute_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTE_T4015859774_H
#ifndef XMLSCHEMAGROUP_T4189650927_H
#define XMLSCHEMAGROUP_T4189650927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroup
struct  XmlSchemaGroup_t4189650927  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.String System.Xml.Schema.XmlSchemaGroup::name
	String_t* ___name_16;
	// System.Xml.Schema.XmlSchemaGroupBase System.Xml.Schema.XmlSchemaGroup::particle
	XmlSchemaGroupBase_t3811767860 * ___particle_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaGroup::qualifiedName
	XmlQualifiedName_t1944712516 * ___qualifiedName_18;
	// System.Boolean System.Xml.Schema.XmlSchemaGroup::isCircularDefinition
	bool ___isCircularDefinition_19;

public:
	inline static int32_t get_offset_of_name_16() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___name_16)); }
	inline String_t* get_name_16() const { return ___name_16; }
	inline String_t** get_address_of_name_16() { return &___name_16; }
	inline void set_name_16(String_t* value)
	{
		___name_16 = value;
		Il2CppCodeGenWriteBarrier((&___name_16), value);
	}

	inline static int32_t get_offset_of_particle_17() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___particle_17)); }
	inline XmlSchemaGroupBase_t3811767860 * get_particle_17() const { return ___particle_17; }
	inline XmlSchemaGroupBase_t3811767860 ** get_address_of_particle_17() { return &___particle_17; }
	inline void set_particle_17(XmlSchemaGroupBase_t3811767860 * value)
	{
		___particle_17 = value;
		Il2CppCodeGenWriteBarrier((&___particle_17), value);
	}

	inline static int32_t get_offset_of_qualifiedName_18() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___qualifiedName_18)); }
	inline XmlQualifiedName_t1944712516 * get_qualifiedName_18() const { return ___qualifiedName_18; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qualifiedName_18() { return &___qualifiedName_18; }
	inline void set_qualifiedName_18(XmlQualifiedName_t1944712516 * value)
	{
		___qualifiedName_18 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_18), value);
	}

	inline static int32_t get_offset_of_isCircularDefinition_19() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_t4189650927, ___isCircularDefinition_19)); }
	inline bool get_isCircularDefinition_19() const { return ___isCircularDefinition_19; }
	inline bool* get_address_of_isCircularDefinition_19() { return &___isCircularDefinition_19; }
	inline void set_isCircularDefinition_19(bool value)
	{
		___isCircularDefinition_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUP_T4189650927_H
#ifndef XMLSCHEMAATTRIBUTEGROUPREF_T825996660_H
#define XMLSCHEMAATTRIBUTEGROUPREF_T825996660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttributeGroupRef
struct  XmlSchemaAttributeGroupRef_t825996660  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttributeGroupRef::refName
	XmlQualifiedName_t1944712516 * ___refName_16;

public:
	inline static int32_t get_offset_of_refName_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroupRef_t825996660, ___refName_16)); }
	inline XmlQualifiedName_t1944712516 * get_refName_16() const { return ___refName_16; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refName_16() { return &___refName_16; }
	inline void set_refName_16(XmlQualifiedName_t1944712516 * value)
	{
		___refName_16 = value;
		Il2CppCodeGenWriteBarrier((&___refName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTEGROUPREF_T825996660_H
#ifndef XMLSCHEMAFACET_T614309579_H
#define XMLSCHEMAFACET_T614309579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet
struct  XmlSchemaFacet_t614309579  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaFacet::isFixed
	bool ___isFixed_17;
	// System.String System.Xml.Schema.XmlSchemaFacet::val
	String_t* ___val_18;

public:
	inline static int32_t get_offset_of_isFixed_17() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t614309579, ___isFixed_17)); }
	inline bool get_isFixed_17() const { return ___isFixed_17; }
	inline bool* get_address_of_isFixed_17() { return &___isFixed_17; }
	inline void set_isFixed_17(bool value)
	{
		___isFixed_17 = value;
	}

	inline static int32_t get_offset_of_val_18() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t614309579, ___val_18)); }
	inline String_t* get_val_18() const { return ___val_18; }
	inline String_t** get_address_of_val_18() { return &___val_18; }
	inline void set_val_18(String_t* value)
	{
		___val_18 = value;
		Il2CppCodeGenWriteBarrier((&___val_18), value);
	}
};

struct XmlSchemaFacet_t614309579_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaFacet/Facet System.Xml.Schema.XmlSchemaFacet::AllFacets
	int32_t ___AllFacets_16;

public:
	inline static int32_t get_offset_of_AllFacets_16() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t614309579_StaticFields, ___AllFacets_16)); }
	inline int32_t get_AllFacets_16() const { return ___AllFacets_16; }
	inline int32_t* get_address_of_AllFacets_16() { return &___AllFacets_16; }
	inline void set_AllFacets_16(int32_t value)
	{
		___AllFacets_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFACET_T614309579_H
#ifndef XMLSCHEMAATTRIBUTEGROUP_T491156493_H
#define XMLSCHEMAATTRIBUTEGROUP_T491156493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttributeGroup
struct  XmlSchemaAttributeGroup_t491156493  : public XmlSchemaAnnotated_t2082486936
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaAttributeGroup::anyAttribute
	XmlSchemaAnyAttribute_t530453212 * ___anyAttribute_16;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAttributeGroup::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_17;
	// System.String System.Xml.Schema.XmlSchemaAttributeGroup::name
	String_t* ___name_18;
	// System.Xml.Schema.XmlSchemaAttributeGroup System.Xml.Schema.XmlSchemaAttributeGroup::redefined
	XmlSchemaAttributeGroup_t491156493 * ___redefined_19;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttributeGroup::qualifiedName
	XmlQualifiedName_t1944712516 * ___qualifiedName_20;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaAttributeGroup::attributeUses
	XmlSchemaObjectTable_t3364835593 * ___attributeUses_21;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaAttributeGroup::anyAttributeUse
	XmlSchemaAnyAttribute_t530453212 * ___anyAttributeUse_22;
	// System.Boolean System.Xml.Schema.XmlSchemaAttributeGroup::AttributeGroupRecursionCheck
	bool ___AttributeGroupRecursionCheck_23;

public:
	inline static int32_t get_offset_of_anyAttribute_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___anyAttribute_16)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttribute_16() const { return ___anyAttribute_16; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttribute_16() { return &___anyAttribute_16; }
	inline void set_anyAttribute_16(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttribute_16 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_16), value);
	}

	inline static int32_t get_offset_of_attributes_17() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___attributes_17)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_17() const { return ___attributes_17; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_17() { return &___attributes_17; }
	inline void set_attributes_17(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_17 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_17), value);
	}

	inline static int32_t get_offset_of_name_18() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___name_18)); }
	inline String_t* get_name_18() const { return ___name_18; }
	inline String_t** get_address_of_name_18() { return &___name_18; }
	inline void set_name_18(String_t* value)
	{
		___name_18 = value;
		Il2CppCodeGenWriteBarrier((&___name_18), value);
	}

	inline static int32_t get_offset_of_redefined_19() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___redefined_19)); }
	inline XmlSchemaAttributeGroup_t491156493 * get_redefined_19() const { return ___redefined_19; }
	inline XmlSchemaAttributeGroup_t491156493 ** get_address_of_redefined_19() { return &___redefined_19; }
	inline void set_redefined_19(XmlSchemaAttributeGroup_t491156493 * value)
	{
		___redefined_19 = value;
		Il2CppCodeGenWriteBarrier((&___redefined_19), value);
	}

	inline static int32_t get_offset_of_qualifiedName_20() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___qualifiedName_20)); }
	inline XmlQualifiedName_t1944712516 * get_qualifiedName_20() const { return ___qualifiedName_20; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qualifiedName_20() { return &___qualifiedName_20; }
	inline void set_qualifiedName_20(XmlQualifiedName_t1944712516 * value)
	{
		___qualifiedName_20 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_20), value);
	}

	inline static int32_t get_offset_of_attributeUses_21() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___attributeUses_21)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributeUses_21() const { return ___attributeUses_21; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributeUses_21() { return &___attributeUses_21; }
	inline void set_attributeUses_21(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributeUses_21 = value;
		Il2CppCodeGenWriteBarrier((&___attributeUses_21), value);
	}

	inline static int32_t get_offset_of_anyAttributeUse_22() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___anyAttributeUse_22)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttributeUse_22() const { return ___anyAttributeUse_22; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttributeUse_22() { return &___anyAttributeUse_22; }
	inline void set_anyAttributeUse_22(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttributeUse_22 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttributeUse_22), value);
	}

	inline static int32_t get_offset_of_AttributeGroupRecursionCheck_23() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t491156493, ___AttributeGroupRecursionCheck_23)); }
	inline bool get_AttributeGroupRecursionCheck_23() const { return ___AttributeGroupRecursionCheck_23; }
	inline bool* get_address_of_AttributeGroupRecursionCheck_23() { return &___AttributeGroupRecursionCheck_23; }
	inline void set_AttributeGroupRecursionCheck_23(bool value)
	{
		___AttributeGroupRecursionCheck_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTEGROUP_T491156493_H
#ifndef EMPTYPARTICLE_T446815059_H
#define EMPTYPARTICLE_T446815059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle/EmptyParticle
struct  EmptyParticle_t446815059  : public XmlSchemaParticle_t3365045970
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYPARTICLE_T446815059_H
#ifndef XMLSCHEMASIMPLETYPELIST_T2170323082_H
#define XMLSCHEMASIMPLETYPELIST_T2170323082_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeList
struct  XmlSchemaSimpleTypeList_t2170323082  : public XmlSchemaSimpleTypeContent_t1606103299
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeList::itemType
	XmlSchemaSimpleType_t248156492 * ___itemType_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleTypeList::itemTypeName
	XmlQualifiedName_t1944712516 * ___itemTypeName_18;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeList::validatedListItemType
	RuntimeObject * ___validatedListItemType_19;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeList::validatedListItemSchemaType
	XmlSchemaSimpleType_t248156492 * ___validatedListItemSchemaType_20;

public:
	inline static int32_t get_offset_of_itemType_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_t2170323082, ___itemType_17)); }
	inline XmlSchemaSimpleType_t248156492 * get_itemType_17() const { return ___itemType_17; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_itemType_17() { return &___itemType_17; }
	inline void set_itemType_17(XmlSchemaSimpleType_t248156492 * value)
	{
		___itemType_17 = value;
		Il2CppCodeGenWriteBarrier((&___itemType_17), value);
	}

	inline static int32_t get_offset_of_itemTypeName_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_t2170323082, ___itemTypeName_18)); }
	inline XmlQualifiedName_t1944712516 * get_itemTypeName_18() const { return ___itemTypeName_18; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_itemTypeName_18() { return &___itemTypeName_18; }
	inline void set_itemTypeName_18(XmlQualifiedName_t1944712516 * value)
	{
		___itemTypeName_18 = value;
		Il2CppCodeGenWriteBarrier((&___itemTypeName_18), value);
	}

	inline static int32_t get_offset_of_validatedListItemType_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_t2170323082, ___validatedListItemType_19)); }
	inline RuntimeObject * get_validatedListItemType_19() const { return ___validatedListItemType_19; }
	inline RuntimeObject ** get_address_of_validatedListItemType_19() { return &___validatedListItemType_19; }
	inline void set_validatedListItemType_19(RuntimeObject * value)
	{
		___validatedListItemType_19 = value;
		Il2CppCodeGenWriteBarrier((&___validatedListItemType_19), value);
	}

	inline static int32_t get_offset_of_validatedListItemSchemaType_20() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_t2170323082, ___validatedListItemSchemaType_20)); }
	inline XmlSchemaSimpleType_t248156492 * get_validatedListItemSchemaType_20() const { return ___validatedListItemSchemaType_20; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_validatedListItemSchemaType_20() { return &___validatedListItemSchemaType_20; }
	inline void set_validatedListItemSchemaType_20(XmlSchemaSimpleType_t248156492 * value)
	{
		___validatedListItemSchemaType_20 = value;
		Il2CppCodeGenWriteBarrier((&___validatedListItemSchemaType_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPELIST_T2170323082_H
#ifndef XSDSTRING_T263933896_H
#define XSDSTRING_T263933896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdString
struct  XsdString_t263933896  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSTRING_T263933896_H
#ifndef XMLSCHEMASIMPLETYPE_T248156492_H
#define XMLSCHEMASIMPLETYPE_T248156492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleType
struct  XmlSchemaSimpleType_t248156492  : public XmlSchemaType_t1795078578
{
public:
	// System.Xml.Schema.XmlSchemaSimpleTypeContent System.Xml.Schema.XmlSchemaSimpleType::content
	XmlSchemaSimpleTypeContent_t1606103299 * ___content_29;
	// System.Boolean System.Xml.Schema.XmlSchemaSimpleType::islocal
	bool ___islocal_30;
	// System.Boolean System.Xml.Schema.XmlSchemaSimpleType::recursed
	bool ___recursed_31;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaSimpleType::variety
	int32_t ___variety_32;

public:
	inline static int32_t get_offset_of_content_29() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492, ___content_29)); }
	inline XmlSchemaSimpleTypeContent_t1606103299 * get_content_29() const { return ___content_29; }
	inline XmlSchemaSimpleTypeContent_t1606103299 ** get_address_of_content_29() { return &___content_29; }
	inline void set_content_29(XmlSchemaSimpleTypeContent_t1606103299 * value)
	{
		___content_29 = value;
		Il2CppCodeGenWriteBarrier((&___content_29), value);
	}

	inline static int32_t get_offset_of_islocal_30() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492, ___islocal_30)); }
	inline bool get_islocal_30() const { return ___islocal_30; }
	inline bool* get_address_of_islocal_30() { return &___islocal_30; }
	inline void set_islocal_30(bool value)
	{
		___islocal_30 = value;
	}

	inline static int32_t get_offset_of_recursed_31() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492, ___recursed_31)); }
	inline bool get_recursed_31() const { return ___recursed_31; }
	inline bool* get_address_of_recursed_31() { return &___recursed_31; }
	inline void set_recursed_31(bool value)
	{
		___recursed_31 = value;
	}

	inline static int32_t get_offset_of_variety_32() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492, ___variety_32)); }
	inline int32_t get_variety_32() const { return ___variety_32; }
	inline int32_t* get_address_of_variety_32() { return &___variety_32; }
	inline void set_variety_32(int32_t value)
	{
		___variety_32 = value;
	}
};

struct XmlSchemaSimpleType_t248156492_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::schemaLocationType
	XmlSchemaSimpleType_t248156492 * ___schemaLocationType_28;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsAnySimpleType
	XmlSchemaSimpleType_t248156492 * ___XsAnySimpleType_33;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsString
	XmlSchemaSimpleType_t248156492 * ___XsString_34;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsBoolean
	XmlSchemaSimpleType_t248156492 * ___XsBoolean_35;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDecimal
	XmlSchemaSimpleType_t248156492 * ___XsDecimal_36;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsFloat
	XmlSchemaSimpleType_t248156492 * ___XsFloat_37;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDouble
	XmlSchemaSimpleType_t248156492 * ___XsDouble_38;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDuration
	XmlSchemaSimpleType_t248156492 * ___XsDuration_39;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDateTime
	XmlSchemaSimpleType_t248156492 * ___XsDateTime_40;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsTime
	XmlSchemaSimpleType_t248156492 * ___XsTime_41;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDate
	XmlSchemaSimpleType_t248156492 * ___XsDate_42;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGYearMonth
	XmlSchemaSimpleType_t248156492 * ___XsGYearMonth_43;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGYear
	XmlSchemaSimpleType_t248156492 * ___XsGYear_44;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGMonthDay
	XmlSchemaSimpleType_t248156492 * ___XsGMonthDay_45;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGDay
	XmlSchemaSimpleType_t248156492 * ___XsGDay_46;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGMonth
	XmlSchemaSimpleType_t248156492 * ___XsGMonth_47;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsHexBinary
	XmlSchemaSimpleType_t248156492 * ___XsHexBinary_48;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsBase64Binary
	XmlSchemaSimpleType_t248156492 * ___XsBase64Binary_49;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsAnyUri
	XmlSchemaSimpleType_t248156492 * ___XsAnyUri_50;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsQName
	XmlSchemaSimpleType_t248156492 * ___XsQName_51;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNotation
	XmlSchemaSimpleType_t248156492 * ___XsNotation_52;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNormalizedString
	XmlSchemaSimpleType_t248156492 * ___XsNormalizedString_53;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsToken
	XmlSchemaSimpleType_t248156492 * ___XsToken_54;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsLanguage
	XmlSchemaSimpleType_t248156492 * ___XsLanguage_55;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNMToken
	XmlSchemaSimpleType_t248156492 * ___XsNMToken_56;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNMTokens
	XmlSchemaSimpleType_t248156492 * ___XsNMTokens_57;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsName
	XmlSchemaSimpleType_t248156492 * ___XsName_58;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNCName
	XmlSchemaSimpleType_t248156492 * ___XsNCName_59;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsID
	XmlSchemaSimpleType_t248156492 * ___XsID_60;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsIDRef
	XmlSchemaSimpleType_t248156492 * ___XsIDRef_61;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsIDRefs
	XmlSchemaSimpleType_t248156492 * ___XsIDRefs_62;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsEntity
	XmlSchemaSimpleType_t248156492 * ___XsEntity_63;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsEntities
	XmlSchemaSimpleType_t248156492 * ___XsEntities_64;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsInteger
	XmlSchemaSimpleType_t248156492 * ___XsInteger_65;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNonPositiveInteger
	XmlSchemaSimpleType_t248156492 * ___XsNonPositiveInteger_66;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNegativeInteger
	XmlSchemaSimpleType_t248156492 * ___XsNegativeInteger_67;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsLong
	XmlSchemaSimpleType_t248156492 * ___XsLong_68;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsInt
	XmlSchemaSimpleType_t248156492 * ___XsInt_69;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsShort
	XmlSchemaSimpleType_t248156492 * ___XsShort_70;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsByte
	XmlSchemaSimpleType_t248156492 * ___XsByte_71;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNonNegativeInteger
	XmlSchemaSimpleType_t248156492 * ___XsNonNegativeInteger_72;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedLong
	XmlSchemaSimpleType_t248156492 * ___XsUnsignedLong_73;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedInt
	XmlSchemaSimpleType_t248156492 * ___XsUnsignedInt_74;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedShort
	XmlSchemaSimpleType_t248156492 * ___XsUnsignedShort_75;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedByte
	XmlSchemaSimpleType_t248156492 * ___XsUnsignedByte_76;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsPositiveInteger
	XmlSchemaSimpleType_t248156492 * ___XsPositiveInteger_77;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtUntypedAtomic
	XmlSchemaSimpleType_t248156492 * ___XdtUntypedAtomic_78;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtAnyAtomicType
	XmlSchemaSimpleType_t248156492 * ___XdtAnyAtomicType_79;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtYearMonthDuration
	XmlSchemaSimpleType_t248156492 * ___XdtYearMonthDuration_80;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtDayTimeDuration
	XmlSchemaSimpleType_t248156492 * ___XdtDayTimeDuration_81;

public:
	inline static int32_t get_offset_of_schemaLocationType_28() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___schemaLocationType_28)); }
	inline XmlSchemaSimpleType_t248156492 * get_schemaLocationType_28() const { return ___schemaLocationType_28; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_schemaLocationType_28() { return &___schemaLocationType_28; }
	inline void set_schemaLocationType_28(XmlSchemaSimpleType_t248156492 * value)
	{
		___schemaLocationType_28 = value;
		Il2CppCodeGenWriteBarrier((&___schemaLocationType_28), value);
	}

	inline static int32_t get_offset_of_XsAnySimpleType_33() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsAnySimpleType_33)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsAnySimpleType_33() const { return ___XsAnySimpleType_33; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsAnySimpleType_33() { return &___XsAnySimpleType_33; }
	inline void set_XsAnySimpleType_33(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsAnySimpleType_33 = value;
		Il2CppCodeGenWriteBarrier((&___XsAnySimpleType_33), value);
	}

	inline static int32_t get_offset_of_XsString_34() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsString_34)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsString_34() const { return ___XsString_34; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsString_34() { return &___XsString_34; }
	inline void set_XsString_34(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsString_34 = value;
		Il2CppCodeGenWriteBarrier((&___XsString_34), value);
	}

	inline static int32_t get_offset_of_XsBoolean_35() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsBoolean_35)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsBoolean_35() const { return ___XsBoolean_35; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsBoolean_35() { return &___XsBoolean_35; }
	inline void set_XsBoolean_35(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsBoolean_35 = value;
		Il2CppCodeGenWriteBarrier((&___XsBoolean_35), value);
	}

	inline static int32_t get_offset_of_XsDecimal_36() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsDecimal_36)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsDecimal_36() const { return ___XsDecimal_36; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsDecimal_36() { return &___XsDecimal_36; }
	inline void set_XsDecimal_36(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsDecimal_36 = value;
		Il2CppCodeGenWriteBarrier((&___XsDecimal_36), value);
	}

	inline static int32_t get_offset_of_XsFloat_37() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsFloat_37)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsFloat_37() const { return ___XsFloat_37; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsFloat_37() { return &___XsFloat_37; }
	inline void set_XsFloat_37(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsFloat_37 = value;
		Il2CppCodeGenWriteBarrier((&___XsFloat_37), value);
	}

	inline static int32_t get_offset_of_XsDouble_38() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsDouble_38)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsDouble_38() const { return ___XsDouble_38; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsDouble_38() { return &___XsDouble_38; }
	inline void set_XsDouble_38(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsDouble_38 = value;
		Il2CppCodeGenWriteBarrier((&___XsDouble_38), value);
	}

	inline static int32_t get_offset_of_XsDuration_39() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsDuration_39)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsDuration_39() const { return ___XsDuration_39; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsDuration_39() { return &___XsDuration_39; }
	inline void set_XsDuration_39(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsDuration_39 = value;
		Il2CppCodeGenWriteBarrier((&___XsDuration_39), value);
	}

	inline static int32_t get_offset_of_XsDateTime_40() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsDateTime_40)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsDateTime_40() const { return ___XsDateTime_40; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsDateTime_40() { return &___XsDateTime_40; }
	inline void set_XsDateTime_40(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsDateTime_40 = value;
		Il2CppCodeGenWriteBarrier((&___XsDateTime_40), value);
	}

	inline static int32_t get_offset_of_XsTime_41() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsTime_41)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsTime_41() const { return ___XsTime_41; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsTime_41() { return &___XsTime_41; }
	inline void set_XsTime_41(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsTime_41 = value;
		Il2CppCodeGenWriteBarrier((&___XsTime_41), value);
	}

	inline static int32_t get_offset_of_XsDate_42() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsDate_42)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsDate_42() const { return ___XsDate_42; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsDate_42() { return &___XsDate_42; }
	inline void set_XsDate_42(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsDate_42 = value;
		Il2CppCodeGenWriteBarrier((&___XsDate_42), value);
	}

	inline static int32_t get_offset_of_XsGYearMonth_43() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsGYearMonth_43)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsGYearMonth_43() const { return ___XsGYearMonth_43; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsGYearMonth_43() { return &___XsGYearMonth_43; }
	inline void set_XsGYearMonth_43(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsGYearMonth_43 = value;
		Il2CppCodeGenWriteBarrier((&___XsGYearMonth_43), value);
	}

	inline static int32_t get_offset_of_XsGYear_44() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsGYear_44)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsGYear_44() const { return ___XsGYear_44; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsGYear_44() { return &___XsGYear_44; }
	inline void set_XsGYear_44(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsGYear_44 = value;
		Il2CppCodeGenWriteBarrier((&___XsGYear_44), value);
	}

	inline static int32_t get_offset_of_XsGMonthDay_45() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsGMonthDay_45)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsGMonthDay_45() const { return ___XsGMonthDay_45; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsGMonthDay_45() { return &___XsGMonthDay_45; }
	inline void set_XsGMonthDay_45(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsGMonthDay_45 = value;
		Il2CppCodeGenWriteBarrier((&___XsGMonthDay_45), value);
	}

	inline static int32_t get_offset_of_XsGDay_46() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsGDay_46)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsGDay_46() const { return ___XsGDay_46; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsGDay_46() { return &___XsGDay_46; }
	inline void set_XsGDay_46(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsGDay_46 = value;
		Il2CppCodeGenWriteBarrier((&___XsGDay_46), value);
	}

	inline static int32_t get_offset_of_XsGMonth_47() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsGMonth_47)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsGMonth_47() const { return ___XsGMonth_47; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsGMonth_47() { return &___XsGMonth_47; }
	inline void set_XsGMonth_47(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsGMonth_47 = value;
		Il2CppCodeGenWriteBarrier((&___XsGMonth_47), value);
	}

	inline static int32_t get_offset_of_XsHexBinary_48() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsHexBinary_48)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsHexBinary_48() const { return ___XsHexBinary_48; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsHexBinary_48() { return &___XsHexBinary_48; }
	inline void set_XsHexBinary_48(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsHexBinary_48 = value;
		Il2CppCodeGenWriteBarrier((&___XsHexBinary_48), value);
	}

	inline static int32_t get_offset_of_XsBase64Binary_49() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsBase64Binary_49)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsBase64Binary_49() const { return ___XsBase64Binary_49; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsBase64Binary_49() { return &___XsBase64Binary_49; }
	inline void set_XsBase64Binary_49(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsBase64Binary_49 = value;
		Il2CppCodeGenWriteBarrier((&___XsBase64Binary_49), value);
	}

	inline static int32_t get_offset_of_XsAnyUri_50() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsAnyUri_50)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsAnyUri_50() const { return ___XsAnyUri_50; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsAnyUri_50() { return &___XsAnyUri_50; }
	inline void set_XsAnyUri_50(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsAnyUri_50 = value;
		Il2CppCodeGenWriteBarrier((&___XsAnyUri_50), value);
	}

	inline static int32_t get_offset_of_XsQName_51() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsQName_51)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsQName_51() const { return ___XsQName_51; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsQName_51() { return &___XsQName_51; }
	inline void set_XsQName_51(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsQName_51 = value;
		Il2CppCodeGenWriteBarrier((&___XsQName_51), value);
	}

	inline static int32_t get_offset_of_XsNotation_52() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsNotation_52)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsNotation_52() const { return ___XsNotation_52; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsNotation_52() { return &___XsNotation_52; }
	inline void set_XsNotation_52(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsNotation_52 = value;
		Il2CppCodeGenWriteBarrier((&___XsNotation_52), value);
	}

	inline static int32_t get_offset_of_XsNormalizedString_53() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsNormalizedString_53)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsNormalizedString_53() const { return ___XsNormalizedString_53; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsNormalizedString_53() { return &___XsNormalizedString_53; }
	inline void set_XsNormalizedString_53(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsNormalizedString_53 = value;
		Il2CppCodeGenWriteBarrier((&___XsNormalizedString_53), value);
	}

	inline static int32_t get_offset_of_XsToken_54() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsToken_54)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsToken_54() const { return ___XsToken_54; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsToken_54() { return &___XsToken_54; }
	inline void set_XsToken_54(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsToken_54 = value;
		Il2CppCodeGenWriteBarrier((&___XsToken_54), value);
	}

	inline static int32_t get_offset_of_XsLanguage_55() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsLanguage_55)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsLanguage_55() const { return ___XsLanguage_55; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsLanguage_55() { return &___XsLanguage_55; }
	inline void set_XsLanguage_55(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsLanguage_55 = value;
		Il2CppCodeGenWriteBarrier((&___XsLanguage_55), value);
	}

	inline static int32_t get_offset_of_XsNMToken_56() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsNMToken_56)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsNMToken_56() const { return ___XsNMToken_56; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsNMToken_56() { return &___XsNMToken_56; }
	inline void set_XsNMToken_56(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsNMToken_56 = value;
		Il2CppCodeGenWriteBarrier((&___XsNMToken_56), value);
	}

	inline static int32_t get_offset_of_XsNMTokens_57() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsNMTokens_57)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsNMTokens_57() const { return ___XsNMTokens_57; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsNMTokens_57() { return &___XsNMTokens_57; }
	inline void set_XsNMTokens_57(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsNMTokens_57 = value;
		Il2CppCodeGenWriteBarrier((&___XsNMTokens_57), value);
	}

	inline static int32_t get_offset_of_XsName_58() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsName_58)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsName_58() const { return ___XsName_58; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsName_58() { return &___XsName_58; }
	inline void set_XsName_58(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsName_58 = value;
		Il2CppCodeGenWriteBarrier((&___XsName_58), value);
	}

	inline static int32_t get_offset_of_XsNCName_59() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsNCName_59)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsNCName_59() const { return ___XsNCName_59; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsNCName_59() { return &___XsNCName_59; }
	inline void set_XsNCName_59(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsNCName_59 = value;
		Il2CppCodeGenWriteBarrier((&___XsNCName_59), value);
	}

	inline static int32_t get_offset_of_XsID_60() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsID_60)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsID_60() const { return ___XsID_60; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsID_60() { return &___XsID_60; }
	inline void set_XsID_60(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsID_60 = value;
		Il2CppCodeGenWriteBarrier((&___XsID_60), value);
	}

	inline static int32_t get_offset_of_XsIDRef_61() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsIDRef_61)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsIDRef_61() const { return ___XsIDRef_61; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsIDRef_61() { return &___XsIDRef_61; }
	inline void set_XsIDRef_61(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsIDRef_61 = value;
		Il2CppCodeGenWriteBarrier((&___XsIDRef_61), value);
	}

	inline static int32_t get_offset_of_XsIDRefs_62() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsIDRefs_62)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsIDRefs_62() const { return ___XsIDRefs_62; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsIDRefs_62() { return &___XsIDRefs_62; }
	inline void set_XsIDRefs_62(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsIDRefs_62 = value;
		Il2CppCodeGenWriteBarrier((&___XsIDRefs_62), value);
	}

	inline static int32_t get_offset_of_XsEntity_63() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsEntity_63)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsEntity_63() const { return ___XsEntity_63; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsEntity_63() { return &___XsEntity_63; }
	inline void set_XsEntity_63(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsEntity_63 = value;
		Il2CppCodeGenWriteBarrier((&___XsEntity_63), value);
	}

	inline static int32_t get_offset_of_XsEntities_64() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsEntities_64)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsEntities_64() const { return ___XsEntities_64; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsEntities_64() { return &___XsEntities_64; }
	inline void set_XsEntities_64(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsEntities_64 = value;
		Il2CppCodeGenWriteBarrier((&___XsEntities_64), value);
	}

	inline static int32_t get_offset_of_XsInteger_65() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsInteger_65)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsInteger_65() const { return ___XsInteger_65; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsInteger_65() { return &___XsInteger_65; }
	inline void set_XsInteger_65(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsInteger_65 = value;
		Il2CppCodeGenWriteBarrier((&___XsInteger_65), value);
	}

	inline static int32_t get_offset_of_XsNonPositiveInteger_66() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsNonPositiveInteger_66)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsNonPositiveInteger_66() const { return ___XsNonPositiveInteger_66; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsNonPositiveInteger_66() { return &___XsNonPositiveInteger_66; }
	inline void set_XsNonPositiveInteger_66(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsNonPositiveInteger_66 = value;
		Il2CppCodeGenWriteBarrier((&___XsNonPositiveInteger_66), value);
	}

	inline static int32_t get_offset_of_XsNegativeInteger_67() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsNegativeInteger_67)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsNegativeInteger_67() const { return ___XsNegativeInteger_67; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsNegativeInteger_67() { return &___XsNegativeInteger_67; }
	inline void set_XsNegativeInteger_67(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsNegativeInteger_67 = value;
		Il2CppCodeGenWriteBarrier((&___XsNegativeInteger_67), value);
	}

	inline static int32_t get_offset_of_XsLong_68() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsLong_68)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsLong_68() const { return ___XsLong_68; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsLong_68() { return &___XsLong_68; }
	inline void set_XsLong_68(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsLong_68 = value;
		Il2CppCodeGenWriteBarrier((&___XsLong_68), value);
	}

	inline static int32_t get_offset_of_XsInt_69() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsInt_69)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsInt_69() const { return ___XsInt_69; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsInt_69() { return &___XsInt_69; }
	inline void set_XsInt_69(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsInt_69 = value;
		Il2CppCodeGenWriteBarrier((&___XsInt_69), value);
	}

	inline static int32_t get_offset_of_XsShort_70() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsShort_70)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsShort_70() const { return ___XsShort_70; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsShort_70() { return &___XsShort_70; }
	inline void set_XsShort_70(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsShort_70 = value;
		Il2CppCodeGenWriteBarrier((&___XsShort_70), value);
	}

	inline static int32_t get_offset_of_XsByte_71() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsByte_71)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsByte_71() const { return ___XsByte_71; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsByte_71() { return &___XsByte_71; }
	inline void set_XsByte_71(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsByte_71 = value;
		Il2CppCodeGenWriteBarrier((&___XsByte_71), value);
	}

	inline static int32_t get_offset_of_XsNonNegativeInteger_72() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsNonNegativeInteger_72)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsNonNegativeInteger_72() const { return ___XsNonNegativeInteger_72; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsNonNegativeInteger_72() { return &___XsNonNegativeInteger_72; }
	inline void set_XsNonNegativeInteger_72(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsNonNegativeInteger_72 = value;
		Il2CppCodeGenWriteBarrier((&___XsNonNegativeInteger_72), value);
	}

	inline static int32_t get_offset_of_XsUnsignedLong_73() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsUnsignedLong_73)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsUnsignedLong_73() const { return ___XsUnsignedLong_73; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsUnsignedLong_73() { return &___XsUnsignedLong_73; }
	inline void set_XsUnsignedLong_73(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsUnsignedLong_73 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedLong_73), value);
	}

	inline static int32_t get_offset_of_XsUnsignedInt_74() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsUnsignedInt_74)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsUnsignedInt_74() const { return ___XsUnsignedInt_74; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsUnsignedInt_74() { return &___XsUnsignedInt_74; }
	inline void set_XsUnsignedInt_74(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsUnsignedInt_74 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedInt_74), value);
	}

	inline static int32_t get_offset_of_XsUnsignedShort_75() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsUnsignedShort_75)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsUnsignedShort_75() const { return ___XsUnsignedShort_75; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsUnsignedShort_75() { return &___XsUnsignedShort_75; }
	inline void set_XsUnsignedShort_75(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsUnsignedShort_75 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedShort_75), value);
	}

	inline static int32_t get_offset_of_XsUnsignedByte_76() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsUnsignedByte_76)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsUnsignedByte_76() const { return ___XsUnsignedByte_76; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsUnsignedByte_76() { return &___XsUnsignedByte_76; }
	inline void set_XsUnsignedByte_76(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsUnsignedByte_76 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedByte_76), value);
	}

	inline static int32_t get_offset_of_XsPositiveInteger_77() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XsPositiveInteger_77)); }
	inline XmlSchemaSimpleType_t248156492 * get_XsPositiveInteger_77() const { return ___XsPositiveInteger_77; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XsPositiveInteger_77() { return &___XsPositiveInteger_77; }
	inline void set_XsPositiveInteger_77(XmlSchemaSimpleType_t248156492 * value)
	{
		___XsPositiveInteger_77 = value;
		Il2CppCodeGenWriteBarrier((&___XsPositiveInteger_77), value);
	}

	inline static int32_t get_offset_of_XdtUntypedAtomic_78() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XdtUntypedAtomic_78)); }
	inline XmlSchemaSimpleType_t248156492 * get_XdtUntypedAtomic_78() const { return ___XdtUntypedAtomic_78; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XdtUntypedAtomic_78() { return &___XdtUntypedAtomic_78; }
	inline void set_XdtUntypedAtomic_78(XmlSchemaSimpleType_t248156492 * value)
	{
		___XdtUntypedAtomic_78 = value;
		Il2CppCodeGenWriteBarrier((&___XdtUntypedAtomic_78), value);
	}

	inline static int32_t get_offset_of_XdtAnyAtomicType_79() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XdtAnyAtomicType_79)); }
	inline XmlSchemaSimpleType_t248156492 * get_XdtAnyAtomicType_79() const { return ___XdtAnyAtomicType_79; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XdtAnyAtomicType_79() { return &___XdtAnyAtomicType_79; }
	inline void set_XdtAnyAtomicType_79(XmlSchemaSimpleType_t248156492 * value)
	{
		___XdtAnyAtomicType_79 = value;
		Il2CppCodeGenWriteBarrier((&___XdtAnyAtomicType_79), value);
	}

	inline static int32_t get_offset_of_XdtYearMonthDuration_80() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XdtYearMonthDuration_80)); }
	inline XmlSchemaSimpleType_t248156492 * get_XdtYearMonthDuration_80() const { return ___XdtYearMonthDuration_80; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XdtYearMonthDuration_80() { return &___XdtYearMonthDuration_80; }
	inline void set_XdtYearMonthDuration_80(XmlSchemaSimpleType_t248156492 * value)
	{
		___XdtYearMonthDuration_80 = value;
		Il2CppCodeGenWriteBarrier((&___XdtYearMonthDuration_80), value);
	}

	inline static int32_t get_offset_of_XdtDayTimeDuration_81() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t248156492_StaticFields, ___XdtDayTimeDuration_81)); }
	inline XmlSchemaSimpleType_t248156492 * get_XdtDayTimeDuration_81() const { return ___XdtDayTimeDuration_81; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_XdtDayTimeDuration_81() { return &___XdtDayTimeDuration_81; }
	inline void set_XdtDayTimeDuration_81(XmlSchemaSimpleType_t248156492 * value)
	{
		___XdtDayTimeDuration_81 = value;
		Il2CppCodeGenWriteBarrier((&___XdtDayTimeDuration_81), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPE_T248156492_H
#ifndef XMLSCHEMASIMPLECONTENTRESTRICTION_T2728776481_H
#define XMLSCHEMASIMPLECONTENTRESTRICTION_T2728776481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContentRestriction
struct  XmlSchemaSimpleContentRestriction_t2728776481  : public XmlSchemaContent_t3733871217
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaSimpleContentRestriction::any
	XmlSchemaAnyAttribute_t530453212 * ___any_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentRestriction::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_18;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleContentRestriction::baseType
	XmlSchemaSimpleType_t248156492 * ___baseType_19;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleContentRestriction::baseTypeName
	XmlQualifiedName_t1944712516 * ___baseTypeName_20;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentRestriction::facets
	XmlSchemaObjectCollection_t395083109 * ___facets_21;

public:
	inline static int32_t get_offset_of_any_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___any_17)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_any_17() const { return ___any_17; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_any_17() { return &___any_17; }
	inline void set_any_17(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___any_17 = value;
		Il2CppCodeGenWriteBarrier((&___any_17), value);
	}

	inline static int32_t get_offset_of_attributes_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___attributes_18)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_18() const { return ___attributes_18; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_18() { return &___attributes_18; }
	inline void set_attributes_18(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_18 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_18), value);
	}

	inline static int32_t get_offset_of_baseType_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___baseType_19)); }
	inline XmlSchemaSimpleType_t248156492 * get_baseType_19() const { return ___baseType_19; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_baseType_19() { return &___baseType_19; }
	inline void set_baseType_19(XmlSchemaSimpleType_t248156492 * value)
	{
		___baseType_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseType_19), value);
	}

	inline static int32_t get_offset_of_baseTypeName_20() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___baseTypeName_20)); }
	inline XmlQualifiedName_t1944712516 * get_baseTypeName_20() const { return ___baseTypeName_20; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_baseTypeName_20() { return &___baseTypeName_20; }
	inline void set_baseTypeName_20(XmlQualifiedName_t1944712516 * value)
	{
		___baseTypeName_20 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_20), value);
	}

	inline static int32_t get_offset_of_facets_21() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t2728776481, ___facets_21)); }
	inline XmlSchemaObjectCollection_t395083109 * get_facets_21() const { return ___facets_21; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_facets_21() { return &___facets_21; }
	inline void set_facets_21(XmlSchemaObjectCollection_t395083109 * value)
	{
		___facets_21 = value;
		Il2CppCodeGenWriteBarrier((&___facets_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLECONTENTRESTRICTION_T2728776481_H
#ifndef XMLSCHEMASIMPLETYPERESTRICTION_T1099506232_H
#define XMLSCHEMASIMPLETYPERESTRICTION_T1099506232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeRestriction
struct  XmlSchemaSimpleTypeRestriction_t1099506232  : public XmlSchemaSimpleTypeContent_t1606103299
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeRestriction::baseType
	XmlSchemaSimpleType_t248156492 * ___baseType_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleTypeRestriction::baseTypeName
	XmlQualifiedName_t1944712516 * ___baseTypeName_18;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleTypeRestriction::facets
	XmlSchemaObjectCollection_t395083109 * ___facets_19;
	// System.String[] System.Xml.Schema.XmlSchemaSimpleTypeRestriction::enumarationFacetValues
	StringU5BU5D_t1642385972* ___enumarationFacetValues_20;
	// System.String[] System.Xml.Schema.XmlSchemaSimpleTypeRestriction::patternFacetValues
	StringU5BU5D_t1642385972* ___patternFacetValues_21;
	// System.Text.RegularExpressions.Regex[] System.Xml.Schema.XmlSchemaSimpleTypeRestriction::rexPatterns
	RegexU5BU5D_t3677892936* ___rexPatterns_22;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::lengthFacet
	Decimal_t724701077  ___lengthFacet_23;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::maxLengthFacet
	Decimal_t724701077  ___maxLengthFacet_24;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::minLengthFacet
	Decimal_t724701077  ___minLengthFacet_25;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::fractionDigitsFacet
	Decimal_t724701077  ___fractionDigitsFacet_26;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::totalDigitsFacet
	Decimal_t724701077  ___totalDigitsFacet_27;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeRestriction::maxInclusiveFacet
	RuntimeObject * ___maxInclusiveFacet_28;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeRestriction::maxExclusiveFacet
	RuntimeObject * ___maxExclusiveFacet_29;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeRestriction::minInclusiveFacet
	RuntimeObject * ___minInclusiveFacet_30;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeRestriction::minExclusiveFacet
	RuntimeObject * ___minExclusiveFacet_31;
	// System.Xml.Schema.XmlSchemaFacet/Facet System.Xml.Schema.XmlSchemaSimpleTypeRestriction::fixedFacets
	int32_t ___fixedFacets_32;

public:
	inline static int32_t get_offset_of_baseType_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___baseType_17)); }
	inline XmlSchemaSimpleType_t248156492 * get_baseType_17() const { return ___baseType_17; }
	inline XmlSchemaSimpleType_t248156492 ** get_address_of_baseType_17() { return &___baseType_17; }
	inline void set_baseType_17(XmlSchemaSimpleType_t248156492 * value)
	{
		___baseType_17 = value;
		Il2CppCodeGenWriteBarrier((&___baseType_17), value);
	}

	inline static int32_t get_offset_of_baseTypeName_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___baseTypeName_18)); }
	inline XmlQualifiedName_t1944712516 * get_baseTypeName_18() const { return ___baseTypeName_18; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_baseTypeName_18() { return &___baseTypeName_18; }
	inline void set_baseTypeName_18(XmlQualifiedName_t1944712516 * value)
	{
		___baseTypeName_18 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_18), value);
	}

	inline static int32_t get_offset_of_facets_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___facets_19)); }
	inline XmlSchemaObjectCollection_t395083109 * get_facets_19() const { return ___facets_19; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_facets_19() { return &___facets_19; }
	inline void set_facets_19(XmlSchemaObjectCollection_t395083109 * value)
	{
		___facets_19 = value;
		Il2CppCodeGenWriteBarrier((&___facets_19), value);
	}

	inline static int32_t get_offset_of_enumarationFacetValues_20() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___enumarationFacetValues_20)); }
	inline StringU5BU5D_t1642385972* get_enumarationFacetValues_20() const { return ___enumarationFacetValues_20; }
	inline StringU5BU5D_t1642385972** get_address_of_enumarationFacetValues_20() { return &___enumarationFacetValues_20; }
	inline void set_enumarationFacetValues_20(StringU5BU5D_t1642385972* value)
	{
		___enumarationFacetValues_20 = value;
		Il2CppCodeGenWriteBarrier((&___enumarationFacetValues_20), value);
	}

	inline static int32_t get_offset_of_patternFacetValues_21() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___patternFacetValues_21)); }
	inline StringU5BU5D_t1642385972* get_patternFacetValues_21() const { return ___patternFacetValues_21; }
	inline StringU5BU5D_t1642385972** get_address_of_patternFacetValues_21() { return &___patternFacetValues_21; }
	inline void set_patternFacetValues_21(StringU5BU5D_t1642385972* value)
	{
		___patternFacetValues_21 = value;
		Il2CppCodeGenWriteBarrier((&___patternFacetValues_21), value);
	}

	inline static int32_t get_offset_of_rexPatterns_22() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___rexPatterns_22)); }
	inline RegexU5BU5D_t3677892936* get_rexPatterns_22() const { return ___rexPatterns_22; }
	inline RegexU5BU5D_t3677892936** get_address_of_rexPatterns_22() { return &___rexPatterns_22; }
	inline void set_rexPatterns_22(RegexU5BU5D_t3677892936* value)
	{
		___rexPatterns_22 = value;
		Il2CppCodeGenWriteBarrier((&___rexPatterns_22), value);
	}

	inline static int32_t get_offset_of_lengthFacet_23() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___lengthFacet_23)); }
	inline Decimal_t724701077  get_lengthFacet_23() const { return ___lengthFacet_23; }
	inline Decimal_t724701077 * get_address_of_lengthFacet_23() { return &___lengthFacet_23; }
	inline void set_lengthFacet_23(Decimal_t724701077  value)
	{
		___lengthFacet_23 = value;
	}

	inline static int32_t get_offset_of_maxLengthFacet_24() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___maxLengthFacet_24)); }
	inline Decimal_t724701077  get_maxLengthFacet_24() const { return ___maxLengthFacet_24; }
	inline Decimal_t724701077 * get_address_of_maxLengthFacet_24() { return &___maxLengthFacet_24; }
	inline void set_maxLengthFacet_24(Decimal_t724701077  value)
	{
		___maxLengthFacet_24 = value;
	}

	inline static int32_t get_offset_of_minLengthFacet_25() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___minLengthFacet_25)); }
	inline Decimal_t724701077  get_minLengthFacet_25() const { return ___minLengthFacet_25; }
	inline Decimal_t724701077 * get_address_of_minLengthFacet_25() { return &___minLengthFacet_25; }
	inline void set_minLengthFacet_25(Decimal_t724701077  value)
	{
		___minLengthFacet_25 = value;
	}

	inline static int32_t get_offset_of_fractionDigitsFacet_26() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___fractionDigitsFacet_26)); }
	inline Decimal_t724701077  get_fractionDigitsFacet_26() const { return ___fractionDigitsFacet_26; }
	inline Decimal_t724701077 * get_address_of_fractionDigitsFacet_26() { return &___fractionDigitsFacet_26; }
	inline void set_fractionDigitsFacet_26(Decimal_t724701077  value)
	{
		___fractionDigitsFacet_26 = value;
	}

	inline static int32_t get_offset_of_totalDigitsFacet_27() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___totalDigitsFacet_27)); }
	inline Decimal_t724701077  get_totalDigitsFacet_27() const { return ___totalDigitsFacet_27; }
	inline Decimal_t724701077 * get_address_of_totalDigitsFacet_27() { return &___totalDigitsFacet_27; }
	inline void set_totalDigitsFacet_27(Decimal_t724701077  value)
	{
		___totalDigitsFacet_27 = value;
	}

	inline static int32_t get_offset_of_maxInclusiveFacet_28() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___maxInclusiveFacet_28)); }
	inline RuntimeObject * get_maxInclusiveFacet_28() const { return ___maxInclusiveFacet_28; }
	inline RuntimeObject ** get_address_of_maxInclusiveFacet_28() { return &___maxInclusiveFacet_28; }
	inline void set_maxInclusiveFacet_28(RuntimeObject * value)
	{
		___maxInclusiveFacet_28 = value;
		Il2CppCodeGenWriteBarrier((&___maxInclusiveFacet_28), value);
	}

	inline static int32_t get_offset_of_maxExclusiveFacet_29() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___maxExclusiveFacet_29)); }
	inline RuntimeObject * get_maxExclusiveFacet_29() const { return ___maxExclusiveFacet_29; }
	inline RuntimeObject ** get_address_of_maxExclusiveFacet_29() { return &___maxExclusiveFacet_29; }
	inline void set_maxExclusiveFacet_29(RuntimeObject * value)
	{
		___maxExclusiveFacet_29 = value;
		Il2CppCodeGenWriteBarrier((&___maxExclusiveFacet_29), value);
	}

	inline static int32_t get_offset_of_minInclusiveFacet_30() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___minInclusiveFacet_30)); }
	inline RuntimeObject * get_minInclusiveFacet_30() const { return ___minInclusiveFacet_30; }
	inline RuntimeObject ** get_address_of_minInclusiveFacet_30() { return &___minInclusiveFacet_30; }
	inline void set_minInclusiveFacet_30(RuntimeObject * value)
	{
		___minInclusiveFacet_30 = value;
		Il2CppCodeGenWriteBarrier((&___minInclusiveFacet_30), value);
	}

	inline static int32_t get_offset_of_minExclusiveFacet_31() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___minExclusiveFacet_31)); }
	inline RuntimeObject * get_minExclusiveFacet_31() const { return ___minExclusiveFacet_31; }
	inline RuntimeObject ** get_address_of_minExclusiveFacet_31() { return &___minExclusiveFacet_31; }
	inline void set_minExclusiveFacet_31(RuntimeObject * value)
	{
		___minExclusiveFacet_31 = value;
		Il2CppCodeGenWriteBarrier((&___minExclusiveFacet_31), value);
	}

	inline static int32_t get_offset_of_fixedFacets_32() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232, ___fixedFacets_32)); }
	inline int32_t get_fixedFacets_32() const { return ___fixedFacets_32; }
	inline int32_t* get_address_of_fixedFacets_32() { return &___fixedFacets_32; }
	inline void set_fixedFacets_32(int32_t value)
	{
		___fixedFacets_32 = value;
	}
};

struct XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields
{
public:
	// System.Globalization.NumberStyles System.Xml.Schema.XmlSchemaSimpleTypeRestriction::lengthStyle
	int32_t ___lengthStyle_33;
	// System.Xml.Schema.XmlSchemaFacet/Facet System.Xml.Schema.XmlSchemaSimpleTypeRestriction::listFacets
	int32_t ___listFacets_34;

public:
	inline static int32_t get_offset_of_lengthStyle_33() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields, ___lengthStyle_33)); }
	inline int32_t get_lengthStyle_33() const { return ___lengthStyle_33; }
	inline int32_t* get_address_of_lengthStyle_33() { return &___lengthStyle_33; }
	inline void set_lengthStyle_33(int32_t value)
	{
		___lengthStyle_33 = value;
	}

	inline static int32_t get_offset_of_listFacets_34() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields, ___listFacets_34)); }
	inline int32_t get_listFacets_34() const { return ___listFacets_34; }
	inline int32_t* get_address_of_listFacets_34() { return &___listFacets_34; }
	inline void set_listFacets_34(int32_t value)
	{
		___listFacets_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPERESTRICTION_T1099506232_H
#ifndef XMLSCHEMASIMPLETYPEUNION_T91327365_H
#define XMLSCHEMASIMPLETYPEUNION_T91327365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeUnion
struct  XmlSchemaSimpleTypeUnion_t91327365  : public XmlSchemaSimpleTypeContent_t1606103299
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleTypeUnion::baseTypes
	XmlSchemaObjectCollection_t395083109 * ___baseTypes_17;
	// System.Xml.XmlQualifiedName[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::memberTypes
	XmlQualifiedNameU5BU5D_t717347117* ___memberTypes_18;
	// System.Object[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::validatedTypes
	ObjectU5BU5D_t3614634134* ___validatedTypes_19;
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::validatedSchemaTypes
	XmlSchemaSimpleTypeU5BU5D_t192177157* ___validatedSchemaTypes_20;

public:
	inline static int32_t get_offset_of_baseTypes_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t91327365, ___baseTypes_17)); }
	inline XmlSchemaObjectCollection_t395083109 * get_baseTypes_17() const { return ___baseTypes_17; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_baseTypes_17() { return &___baseTypes_17; }
	inline void set_baseTypes_17(XmlSchemaObjectCollection_t395083109 * value)
	{
		___baseTypes_17 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypes_17), value);
	}

	inline static int32_t get_offset_of_memberTypes_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t91327365, ___memberTypes_18)); }
	inline XmlQualifiedNameU5BU5D_t717347117* get_memberTypes_18() const { return ___memberTypes_18; }
	inline XmlQualifiedNameU5BU5D_t717347117** get_address_of_memberTypes_18() { return &___memberTypes_18; }
	inline void set_memberTypes_18(XmlQualifiedNameU5BU5D_t717347117* value)
	{
		___memberTypes_18 = value;
		Il2CppCodeGenWriteBarrier((&___memberTypes_18), value);
	}

	inline static int32_t get_offset_of_validatedTypes_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t91327365, ___validatedTypes_19)); }
	inline ObjectU5BU5D_t3614634134* get_validatedTypes_19() const { return ___validatedTypes_19; }
	inline ObjectU5BU5D_t3614634134** get_address_of_validatedTypes_19() { return &___validatedTypes_19; }
	inline void set_validatedTypes_19(ObjectU5BU5D_t3614634134* value)
	{
		___validatedTypes_19 = value;
		Il2CppCodeGenWriteBarrier((&___validatedTypes_19), value);
	}

	inline static int32_t get_offset_of_validatedSchemaTypes_20() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t91327365, ___validatedSchemaTypes_20)); }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157* get_validatedSchemaTypes_20() const { return ___validatedSchemaTypes_20; }
	inline XmlSchemaSimpleTypeU5BU5D_t192177157** get_address_of_validatedSchemaTypes_20() { return &___validatedSchemaTypes_20; }
	inline void set_validatedSchemaTypes_20(XmlSchemaSimpleTypeU5BU5D_t192177157* value)
	{
		___validatedSchemaTypes_20 = value;
		Il2CppCodeGenWriteBarrier((&___validatedSchemaTypes_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPEUNION_T91327365_H
#ifndef XMLSCHEMAUNIQUE_T403169513_H
#define XMLSCHEMAUNIQUE_T403169513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUnique
struct  XmlSchemaUnique_t403169513  : public XmlSchemaIdentityConstraint_t1058613623
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUNIQUE_T403169513_H
#ifndef XMLSCHEMAPATTERNFACET_T2024909611_H
#define XMLSCHEMAPATTERNFACET_T2024909611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaPatternFacet
struct  XmlSchemaPatternFacet_t2024909611  : public XmlSchemaFacet_t614309579
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPATTERNFACET_T2024909611_H
#ifndef XMLSCHEMASIMPLECONTENTEXTENSION_T3718357176_H
#define XMLSCHEMASIMPLECONTENTEXTENSION_T3718357176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContentExtension
struct  XmlSchemaSimpleContentExtension_t3718357176  : public XmlSchemaContent_t3733871217
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaSimpleContentExtension::any
	XmlSchemaAnyAttribute_t530453212 * ___any_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentExtension::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleContentExtension::baseTypeName
	XmlQualifiedName_t1944712516 * ___baseTypeName_19;

public:
	inline static int32_t get_offset_of_any_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t3718357176, ___any_17)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_any_17() const { return ___any_17; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_any_17() { return &___any_17; }
	inline void set_any_17(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___any_17 = value;
		Il2CppCodeGenWriteBarrier((&___any_17), value);
	}

	inline static int32_t get_offset_of_attributes_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t3718357176, ___attributes_18)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_18() const { return ___attributes_18; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_18() { return &___attributes_18; }
	inline void set_attributes_18(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_18 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_18), value);
	}

	inline static int32_t get_offset_of_baseTypeName_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t3718357176, ___baseTypeName_19)); }
	inline XmlQualifiedName_t1944712516 * get_baseTypeName_19() const { return ___baseTypeName_19; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_baseTypeName_19() { return &___baseTypeName_19; }
	inline void set_baseTypeName_19(XmlQualifiedName_t1944712516 * value)
	{
		___baseTypeName_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLECONTENTEXTENSION_T3718357176_H
#ifndef XMLSCHEMASIMPLECONTENT_T2303138587_H
#define XMLSCHEMASIMPLECONTENT_T2303138587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContent
struct  XmlSchemaSimpleContent_t2303138587  : public XmlSchemaContentModel_t907989596
{
public:
	// System.Xml.Schema.XmlSchemaContent System.Xml.Schema.XmlSchemaSimpleContent::content
	XmlSchemaContent_t3733871217 * ___content_16;

public:
	inline static int32_t get_offset_of_content_16() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContent_t2303138587, ___content_16)); }
	inline XmlSchemaContent_t3733871217 * get_content_16() const { return ___content_16; }
	inline XmlSchemaContent_t3733871217 ** get_address_of_content_16() { return &___content_16; }
	inline void set_content_16(XmlSchemaContent_t3733871217 * value)
	{
		___content_16 = value;
		Il2CppCodeGenWriteBarrier((&___content_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLECONTENT_T2303138587_H
#ifndef XMLSCHEMAANY_T3277730824_H
#define XMLSCHEMAANY_T3277730824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAny
struct  XmlSchemaAny_t3277730824  : public XmlSchemaParticle_t3365045970
{
public:
	// System.String System.Xml.Schema.XmlSchemaAny::nameSpace
	String_t* ___nameSpace_28;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XmlSchemaAny::processing
	int32_t ___processing_29;
	// Mono.Xml.Schema.XsdWildcard System.Xml.Schema.XmlSchemaAny::wildcard
	XsdWildcard_t625524157 * ___wildcard_30;

public:
	inline static int32_t get_offset_of_nameSpace_28() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t3277730824, ___nameSpace_28)); }
	inline String_t* get_nameSpace_28() const { return ___nameSpace_28; }
	inline String_t** get_address_of_nameSpace_28() { return &___nameSpace_28; }
	inline void set_nameSpace_28(String_t* value)
	{
		___nameSpace_28 = value;
		Il2CppCodeGenWriteBarrier((&___nameSpace_28), value);
	}

	inline static int32_t get_offset_of_processing_29() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t3277730824, ___processing_29)); }
	inline int32_t get_processing_29() const { return ___processing_29; }
	inline int32_t* get_address_of_processing_29() { return &___processing_29; }
	inline void set_processing_29(int32_t value)
	{
		___processing_29 = value;
	}

	inline static int32_t get_offset_of_wildcard_30() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t3277730824, ___wildcard_30)); }
	inline XsdWildcard_t625524157 * get_wildcard_30() const { return ___wildcard_30; }
	inline XsdWildcard_t625524157 ** get_address_of_wildcard_30() { return &___wildcard_30; }
	inline void set_wildcard_30(XsdWildcard_t625524157 * value)
	{
		___wildcard_30 = value;
		Il2CppCodeGenWriteBarrier((&___wildcard_30), value);
	}
};

struct XmlSchemaAny_t3277730824_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAny System.Xml.Schema.XmlSchemaAny::anyTypeContent
	XmlSchemaAny_t3277730824 * ___anyTypeContent_27;

public:
	inline static int32_t get_offset_of_anyTypeContent_27() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t3277730824_StaticFields, ___anyTypeContent_27)); }
	inline XmlSchemaAny_t3277730824 * get_anyTypeContent_27() const { return ___anyTypeContent_27; }
	inline XmlSchemaAny_t3277730824 ** get_address_of_anyTypeContent_27() { return &___anyTypeContent_27; }
	inline void set_anyTypeContent_27(XmlSchemaAny_t3277730824 * value)
	{
		___anyTypeContent_27 = value;
		Il2CppCodeGenWriteBarrier((&___anyTypeContent_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANY_T3277730824_H
#ifndef XMLSCHEMACOMPLEXCONTENT_T2065934415_H
#define XMLSCHEMACOMPLEXCONTENT_T2065934415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContent
struct  XmlSchemaComplexContent_t2065934415  : public XmlSchemaContentModel_t907989596
{
public:
	// System.Xml.Schema.XmlSchemaContent System.Xml.Schema.XmlSchemaComplexContent::content
	XmlSchemaContent_t3733871217 * ___content_16;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexContent::isMixed
	bool ___isMixed_17;

public:
	inline static int32_t get_offset_of_content_16() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContent_t2065934415, ___content_16)); }
	inline XmlSchemaContent_t3733871217 * get_content_16() const { return ___content_16; }
	inline XmlSchemaContent_t3733871217 ** get_address_of_content_16() { return &___content_16; }
	inline void set_content_16(XmlSchemaContent_t3733871217 * value)
	{
		___content_16 = value;
		Il2CppCodeGenWriteBarrier((&___content_16), value);
	}

	inline static int32_t get_offset_of_isMixed_17() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContent_t2065934415, ___isMixed_17)); }
	inline bool get_isMixed_17() const { return ___isMixed_17; }
	inline bool* get_address_of_isMixed_17() { return &___isMixed_17; }
	inline void set_isMixed_17(bool value)
	{
		___isMixed_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXCONTENT_T2065934415_H
#ifndef XSDGMONTH_T4076673358_H
#define XSDGMONTH_T4076673358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonth
struct  XsdGMonth_t4076673358  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTH_T4076673358_H
#ifndef XSDGDAY_T1914244270_H
#define XSDGDAY_T1914244270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGDay
struct  XsdGDay_t1914244270  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGDAY_T1914244270_H
#ifndef XMLSCHEMACOMPLEXTYPE_T4086789226_H
#define XMLSCHEMACOMPLEXTYPE_T4086789226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexType
struct  XmlSchemaComplexType_t4086789226  : public XmlSchemaType_t1795078578
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexType::anyAttribute
	XmlSchemaAnyAttribute_t530453212 * ___anyAttribute_28;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexType::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_29;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaComplexType::attributeUses
	XmlSchemaObjectTable_t3364835593 * ___attributeUses_30;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexType::attributeWildcard
	XmlSchemaAnyAttribute_t530453212 * ___attributeWildcard_31;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaComplexType::block
	int32_t ___block_32;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaComplexType::blockResolved
	int32_t ___blockResolved_33;
	// System.Xml.Schema.XmlSchemaContentModel System.Xml.Schema.XmlSchemaComplexType::contentModel
	XmlSchemaContentModel_t907989596 * ___contentModel_34;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::validatableParticle
	XmlSchemaParticle_t3365045970 * ___validatableParticle_35;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::contentTypeParticle
	XmlSchemaParticle_t3365045970 * ___contentTypeParticle_36;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexType::isAbstract
	bool ___isAbstract_37;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexType::isMixed
	bool ___isMixed_38;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::particle
	XmlSchemaParticle_t3365045970 * ___particle_39;
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.XmlSchemaComplexType::resolvedContentType
	int32_t ___resolvedContentType_40;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexType::ValidatedIsAbstract
	bool ___ValidatedIsAbstract_41;
	// System.Guid System.Xml.Schema.XmlSchemaComplexType::CollectProcessId
	Guid_t  ___CollectProcessId_44;

public:
	inline static int32_t get_offset_of_anyAttribute_28() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___anyAttribute_28)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_anyAttribute_28() const { return ___anyAttribute_28; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_anyAttribute_28() { return &___anyAttribute_28; }
	inline void set_anyAttribute_28(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___anyAttribute_28 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_28), value);
	}

	inline static int32_t get_offset_of_attributes_29() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___attributes_29)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_29() const { return ___attributes_29; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_29() { return &___attributes_29; }
	inline void set_attributes_29(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_29 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_29), value);
	}

	inline static int32_t get_offset_of_attributeUses_30() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___attributeUses_30)); }
	inline XmlSchemaObjectTable_t3364835593 * get_attributeUses_30() const { return ___attributeUses_30; }
	inline XmlSchemaObjectTable_t3364835593 ** get_address_of_attributeUses_30() { return &___attributeUses_30; }
	inline void set_attributeUses_30(XmlSchemaObjectTable_t3364835593 * value)
	{
		___attributeUses_30 = value;
		Il2CppCodeGenWriteBarrier((&___attributeUses_30), value);
	}

	inline static int32_t get_offset_of_attributeWildcard_31() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___attributeWildcard_31)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_attributeWildcard_31() const { return ___attributeWildcard_31; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_attributeWildcard_31() { return &___attributeWildcard_31; }
	inline void set_attributeWildcard_31(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___attributeWildcard_31 = value;
		Il2CppCodeGenWriteBarrier((&___attributeWildcard_31), value);
	}

	inline static int32_t get_offset_of_block_32() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___block_32)); }
	inline int32_t get_block_32() const { return ___block_32; }
	inline int32_t* get_address_of_block_32() { return &___block_32; }
	inline void set_block_32(int32_t value)
	{
		___block_32 = value;
	}

	inline static int32_t get_offset_of_blockResolved_33() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___blockResolved_33)); }
	inline int32_t get_blockResolved_33() const { return ___blockResolved_33; }
	inline int32_t* get_address_of_blockResolved_33() { return &___blockResolved_33; }
	inline void set_blockResolved_33(int32_t value)
	{
		___blockResolved_33 = value;
	}

	inline static int32_t get_offset_of_contentModel_34() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___contentModel_34)); }
	inline XmlSchemaContentModel_t907989596 * get_contentModel_34() const { return ___contentModel_34; }
	inline XmlSchemaContentModel_t907989596 ** get_address_of_contentModel_34() { return &___contentModel_34; }
	inline void set_contentModel_34(XmlSchemaContentModel_t907989596 * value)
	{
		___contentModel_34 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_34), value);
	}

	inline static int32_t get_offset_of_validatableParticle_35() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___validatableParticle_35)); }
	inline XmlSchemaParticle_t3365045970 * get_validatableParticle_35() const { return ___validatableParticle_35; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_validatableParticle_35() { return &___validatableParticle_35; }
	inline void set_validatableParticle_35(XmlSchemaParticle_t3365045970 * value)
	{
		___validatableParticle_35 = value;
		Il2CppCodeGenWriteBarrier((&___validatableParticle_35), value);
	}

	inline static int32_t get_offset_of_contentTypeParticle_36() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___contentTypeParticle_36)); }
	inline XmlSchemaParticle_t3365045970 * get_contentTypeParticle_36() const { return ___contentTypeParticle_36; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_contentTypeParticle_36() { return &___contentTypeParticle_36; }
	inline void set_contentTypeParticle_36(XmlSchemaParticle_t3365045970 * value)
	{
		___contentTypeParticle_36 = value;
		Il2CppCodeGenWriteBarrier((&___contentTypeParticle_36), value);
	}

	inline static int32_t get_offset_of_isAbstract_37() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___isAbstract_37)); }
	inline bool get_isAbstract_37() const { return ___isAbstract_37; }
	inline bool* get_address_of_isAbstract_37() { return &___isAbstract_37; }
	inline void set_isAbstract_37(bool value)
	{
		___isAbstract_37 = value;
	}

	inline static int32_t get_offset_of_isMixed_38() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___isMixed_38)); }
	inline bool get_isMixed_38() const { return ___isMixed_38; }
	inline bool* get_address_of_isMixed_38() { return &___isMixed_38; }
	inline void set_isMixed_38(bool value)
	{
		___isMixed_38 = value;
	}

	inline static int32_t get_offset_of_particle_39() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___particle_39)); }
	inline XmlSchemaParticle_t3365045970 * get_particle_39() const { return ___particle_39; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_particle_39() { return &___particle_39; }
	inline void set_particle_39(XmlSchemaParticle_t3365045970 * value)
	{
		___particle_39 = value;
		Il2CppCodeGenWriteBarrier((&___particle_39), value);
	}

	inline static int32_t get_offset_of_resolvedContentType_40() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___resolvedContentType_40)); }
	inline int32_t get_resolvedContentType_40() const { return ___resolvedContentType_40; }
	inline int32_t* get_address_of_resolvedContentType_40() { return &___resolvedContentType_40; }
	inline void set_resolvedContentType_40(int32_t value)
	{
		___resolvedContentType_40 = value;
	}

	inline static int32_t get_offset_of_ValidatedIsAbstract_41() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___ValidatedIsAbstract_41)); }
	inline bool get_ValidatedIsAbstract_41() const { return ___ValidatedIsAbstract_41; }
	inline bool* get_address_of_ValidatedIsAbstract_41() { return &___ValidatedIsAbstract_41; }
	inline void set_ValidatedIsAbstract_41(bool value)
	{
		___ValidatedIsAbstract_41 = value;
	}

	inline static int32_t get_offset_of_CollectProcessId_44() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226, ___CollectProcessId_44)); }
	inline Guid_t  get_CollectProcessId_44() const { return ___CollectProcessId_44; }
	inline Guid_t * get_address_of_CollectProcessId_44() { return &___CollectProcessId_44; }
	inline void set_CollectProcessId_44(Guid_t  value)
	{
		___CollectProcessId_44 = value;
	}
};

struct XmlSchemaComplexType_t4086789226_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XmlSchemaComplexType::anyType
	XmlSchemaComplexType_t4086789226 * ___anyType_42;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaComplexType::AnyTypeName
	XmlQualifiedName_t1944712516 * ___AnyTypeName_43;

public:
	inline static int32_t get_offset_of_anyType_42() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226_StaticFields, ___anyType_42)); }
	inline XmlSchemaComplexType_t4086789226 * get_anyType_42() const { return ___anyType_42; }
	inline XmlSchemaComplexType_t4086789226 ** get_address_of_anyType_42() { return &___anyType_42; }
	inline void set_anyType_42(XmlSchemaComplexType_t4086789226 * value)
	{
		___anyType_42 = value;
		Il2CppCodeGenWriteBarrier((&___anyType_42), value);
	}

	inline static int32_t get_offset_of_AnyTypeName_43() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t4086789226_StaticFields, ___AnyTypeName_43)); }
	inline XmlQualifiedName_t1944712516 * get_AnyTypeName_43() const { return ___AnyTypeName_43; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_AnyTypeName_43() { return &___AnyTypeName_43; }
	inline void set_AnyTypeName_43(XmlQualifiedName_t1944712516 * value)
	{
		___AnyTypeName_43 = value;
		Il2CppCodeGenWriteBarrier((&___AnyTypeName_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXTYPE_T4086789226_H
#ifndef XMLSCHEMAELEMENT_T2433337156_H
#define XMLSCHEMAELEMENT_T2433337156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaElement
struct  XmlSchemaElement_t2433337156  : public XmlSchemaParticle_t3365045970
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::block
	int32_t ___block_27;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaElement::constraints
	XmlSchemaObjectCollection_t395083109 * ___constraints_28;
	// System.String System.Xml.Schema.XmlSchemaElement::defaultValue
	String_t* ___defaultValue_29;
	// System.Object System.Xml.Schema.XmlSchemaElement::elementType
	RuntimeObject * ___elementType_30;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaElement::elementSchemaType
	XmlSchemaType_t1795078578 * ___elementSchemaType_31;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::final
	int32_t ___final_32;
	// System.String System.Xml.Schema.XmlSchemaElement::fixedValue
	String_t* ___fixedValue_33;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchemaElement::form
	int32_t ___form_34;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isAbstract
	bool ___isAbstract_35;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isNillable
	bool ___isNillable_36;
	// System.String System.Xml.Schema.XmlSchemaElement::name
	String_t* ___name_37;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::refName
	XmlQualifiedName_t1944712516 * ___refName_38;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaElement::schemaType
	XmlSchemaType_t1795078578 * ___schemaType_39;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::schemaTypeName
	XmlQualifiedName_t1944712516 * ___schemaTypeName_40;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::substitutionGroup
	XmlQualifiedName_t1944712516 * ___substitutionGroup_41;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaElement::schema
	XmlSchema_t880472818 * ___schema_42;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::parentIsSchema
	bool ___parentIsSchema_43;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::qName
	XmlQualifiedName_t1944712516 * ___qName_44;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::blockResolved
	int32_t ___blockResolved_45;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::finalResolved
	int32_t ___finalResolved_46;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaElement::referencedElement
	XmlSchemaElement_t2433337156 * ___referencedElement_47;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaElement::substitutingElements
	ArrayList_t4252133567 * ___substitutingElements_48;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaElement::substitutionGroupElement
	XmlSchemaElement_t2433337156 * ___substitutionGroupElement_49;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::actualIsAbstract
	bool ___actualIsAbstract_50;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::actualIsNillable
	bool ___actualIsNillable_51;
	// System.String System.Xml.Schema.XmlSchemaElement::validatedDefaultValue
	String_t* ___validatedDefaultValue_52;
	// System.String System.Xml.Schema.XmlSchemaElement::validatedFixedValue
	String_t* ___validatedFixedValue_53;

public:
	inline static int32_t get_offset_of_block_27() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___block_27)); }
	inline int32_t get_block_27() const { return ___block_27; }
	inline int32_t* get_address_of_block_27() { return &___block_27; }
	inline void set_block_27(int32_t value)
	{
		___block_27 = value;
	}

	inline static int32_t get_offset_of_constraints_28() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___constraints_28)); }
	inline XmlSchemaObjectCollection_t395083109 * get_constraints_28() const { return ___constraints_28; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_constraints_28() { return &___constraints_28; }
	inline void set_constraints_28(XmlSchemaObjectCollection_t395083109 * value)
	{
		___constraints_28 = value;
		Il2CppCodeGenWriteBarrier((&___constraints_28), value);
	}

	inline static int32_t get_offset_of_defaultValue_29() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___defaultValue_29)); }
	inline String_t* get_defaultValue_29() const { return ___defaultValue_29; }
	inline String_t** get_address_of_defaultValue_29() { return &___defaultValue_29; }
	inline void set_defaultValue_29(String_t* value)
	{
		___defaultValue_29 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_29), value);
	}

	inline static int32_t get_offset_of_elementType_30() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___elementType_30)); }
	inline RuntimeObject * get_elementType_30() const { return ___elementType_30; }
	inline RuntimeObject ** get_address_of_elementType_30() { return &___elementType_30; }
	inline void set_elementType_30(RuntimeObject * value)
	{
		___elementType_30 = value;
		Il2CppCodeGenWriteBarrier((&___elementType_30), value);
	}

	inline static int32_t get_offset_of_elementSchemaType_31() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___elementSchemaType_31)); }
	inline XmlSchemaType_t1795078578 * get_elementSchemaType_31() const { return ___elementSchemaType_31; }
	inline XmlSchemaType_t1795078578 ** get_address_of_elementSchemaType_31() { return &___elementSchemaType_31; }
	inline void set_elementSchemaType_31(XmlSchemaType_t1795078578 * value)
	{
		___elementSchemaType_31 = value;
		Il2CppCodeGenWriteBarrier((&___elementSchemaType_31), value);
	}

	inline static int32_t get_offset_of_final_32() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___final_32)); }
	inline int32_t get_final_32() const { return ___final_32; }
	inline int32_t* get_address_of_final_32() { return &___final_32; }
	inline void set_final_32(int32_t value)
	{
		___final_32 = value;
	}

	inline static int32_t get_offset_of_fixedValue_33() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___fixedValue_33)); }
	inline String_t* get_fixedValue_33() const { return ___fixedValue_33; }
	inline String_t** get_address_of_fixedValue_33() { return &___fixedValue_33; }
	inline void set_fixedValue_33(String_t* value)
	{
		___fixedValue_33 = value;
		Il2CppCodeGenWriteBarrier((&___fixedValue_33), value);
	}

	inline static int32_t get_offset_of_form_34() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___form_34)); }
	inline int32_t get_form_34() const { return ___form_34; }
	inline int32_t* get_address_of_form_34() { return &___form_34; }
	inline void set_form_34(int32_t value)
	{
		___form_34 = value;
	}

	inline static int32_t get_offset_of_isAbstract_35() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___isAbstract_35)); }
	inline bool get_isAbstract_35() const { return ___isAbstract_35; }
	inline bool* get_address_of_isAbstract_35() { return &___isAbstract_35; }
	inline void set_isAbstract_35(bool value)
	{
		___isAbstract_35 = value;
	}

	inline static int32_t get_offset_of_isNillable_36() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___isNillable_36)); }
	inline bool get_isNillable_36() const { return ___isNillable_36; }
	inline bool* get_address_of_isNillable_36() { return &___isNillable_36; }
	inline void set_isNillable_36(bool value)
	{
		___isNillable_36 = value;
	}

	inline static int32_t get_offset_of_name_37() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___name_37)); }
	inline String_t* get_name_37() const { return ___name_37; }
	inline String_t** get_address_of_name_37() { return &___name_37; }
	inline void set_name_37(String_t* value)
	{
		___name_37 = value;
		Il2CppCodeGenWriteBarrier((&___name_37), value);
	}

	inline static int32_t get_offset_of_refName_38() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___refName_38)); }
	inline XmlQualifiedName_t1944712516 * get_refName_38() const { return ___refName_38; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refName_38() { return &___refName_38; }
	inline void set_refName_38(XmlQualifiedName_t1944712516 * value)
	{
		___refName_38 = value;
		Il2CppCodeGenWriteBarrier((&___refName_38), value);
	}

	inline static int32_t get_offset_of_schemaType_39() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___schemaType_39)); }
	inline XmlSchemaType_t1795078578 * get_schemaType_39() const { return ___schemaType_39; }
	inline XmlSchemaType_t1795078578 ** get_address_of_schemaType_39() { return &___schemaType_39; }
	inline void set_schemaType_39(XmlSchemaType_t1795078578 * value)
	{
		___schemaType_39 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_39), value);
	}

	inline static int32_t get_offset_of_schemaTypeName_40() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___schemaTypeName_40)); }
	inline XmlQualifiedName_t1944712516 * get_schemaTypeName_40() const { return ___schemaTypeName_40; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_schemaTypeName_40() { return &___schemaTypeName_40; }
	inline void set_schemaTypeName_40(XmlQualifiedName_t1944712516 * value)
	{
		___schemaTypeName_40 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypeName_40), value);
	}

	inline static int32_t get_offset_of_substitutionGroup_41() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___substitutionGroup_41)); }
	inline XmlQualifiedName_t1944712516 * get_substitutionGroup_41() const { return ___substitutionGroup_41; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_substitutionGroup_41() { return &___substitutionGroup_41; }
	inline void set_substitutionGroup_41(XmlQualifiedName_t1944712516 * value)
	{
		___substitutionGroup_41 = value;
		Il2CppCodeGenWriteBarrier((&___substitutionGroup_41), value);
	}

	inline static int32_t get_offset_of_schema_42() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___schema_42)); }
	inline XmlSchema_t880472818 * get_schema_42() const { return ___schema_42; }
	inline XmlSchema_t880472818 ** get_address_of_schema_42() { return &___schema_42; }
	inline void set_schema_42(XmlSchema_t880472818 * value)
	{
		___schema_42 = value;
		Il2CppCodeGenWriteBarrier((&___schema_42), value);
	}

	inline static int32_t get_offset_of_parentIsSchema_43() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___parentIsSchema_43)); }
	inline bool get_parentIsSchema_43() const { return ___parentIsSchema_43; }
	inline bool* get_address_of_parentIsSchema_43() { return &___parentIsSchema_43; }
	inline void set_parentIsSchema_43(bool value)
	{
		___parentIsSchema_43 = value;
	}

	inline static int32_t get_offset_of_qName_44() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___qName_44)); }
	inline XmlQualifiedName_t1944712516 * get_qName_44() const { return ___qName_44; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qName_44() { return &___qName_44; }
	inline void set_qName_44(XmlQualifiedName_t1944712516 * value)
	{
		___qName_44 = value;
		Il2CppCodeGenWriteBarrier((&___qName_44), value);
	}

	inline static int32_t get_offset_of_blockResolved_45() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___blockResolved_45)); }
	inline int32_t get_blockResolved_45() const { return ___blockResolved_45; }
	inline int32_t* get_address_of_blockResolved_45() { return &___blockResolved_45; }
	inline void set_blockResolved_45(int32_t value)
	{
		___blockResolved_45 = value;
	}

	inline static int32_t get_offset_of_finalResolved_46() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___finalResolved_46)); }
	inline int32_t get_finalResolved_46() const { return ___finalResolved_46; }
	inline int32_t* get_address_of_finalResolved_46() { return &___finalResolved_46; }
	inline void set_finalResolved_46(int32_t value)
	{
		___finalResolved_46 = value;
	}

	inline static int32_t get_offset_of_referencedElement_47() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___referencedElement_47)); }
	inline XmlSchemaElement_t2433337156 * get_referencedElement_47() const { return ___referencedElement_47; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_referencedElement_47() { return &___referencedElement_47; }
	inline void set_referencedElement_47(XmlSchemaElement_t2433337156 * value)
	{
		___referencedElement_47 = value;
		Il2CppCodeGenWriteBarrier((&___referencedElement_47), value);
	}

	inline static int32_t get_offset_of_substitutingElements_48() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___substitutingElements_48)); }
	inline ArrayList_t4252133567 * get_substitutingElements_48() const { return ___substitutingElements_48; }
	inline ArrayList_t4252133567 ** get_address_of_substitutingElements_48() { return &___substitutingElements_48; }
	inline void set_substitutingElements_48(ArrayList_t4252133567 * value)
	{
		___substitutingElements_48 = value;
		Il2CppCodeGenWriteBarrier((&___substitutingElements_48), value);
	}

	inline static int32_t get_offset_of_substitutionGroupElement_49() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___substitutionGroupElement_49)); }
	inline XmlSchemaElement_t2433337156 * get_substitutionGroupElement_49() const { return ___substitutionGroupElement_49; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_substitutionGroupElement_49() { return &___substitutionGroupElement_49; }
	inline void set_substitutionGroupElement_49(XmlSchemaElement_t2433337156 * value)
	{
		___substitutionGroupElement_49 = value;
		Il2CppCodeGenWriteBarrier((&___substitutionGroupElement_49), value);
	}

	inline static int32_t get_offset_of_actualIsAbstract_50() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___actualIsAbstract_50)); }
	inline bool get_actualIsAbstract_50() const { return ___actualIsAbstract_50; }
	inline bool* get_address_of_actualIsAbstract_50() { return &___actualIsAbstract_50; }
	inline void set_actualIsAbstract_50(bool value)
	{
		___actualIsAbstract_50 = value;
	}

	inline static int32_t get_offset_of_actualIsNillable_51() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___actualIsNillable_51)); }
	inline bool get_actualIsNillable_51() const { return ___actualIsNillable_51; }
	inline bool* get_address_of_actualIsNillable_51() { return &___actualIsNillable_51; }
	inline void set_actualIsNillable_51(bool value)
	{
		___actualIsNillable_51 = value;
	}

	inline static int32_t get_offset_of_validatedDefaultValue_52() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___validatedDefaultValue_52)); }
	inline String_t* get_validatedDefaultValue_52() const { return ___validatedDefaultValue_52; }
	inline String_t** get_address_of_validatedDefaultValue_52() { return &___validatedDefaultValue_52; }
	inline void set_validatedDefaultValue_52(String_t* value)
	{
		___validatedDefaultValue_52 = value;
		Il2CppCodeGenWriteBarrier((&___validatedDefaultValue_52), value);
	}

	inline static int32_t get_offset_of_validatedFixedValue_53() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t2433337156, ___validatedFixedValue_53)); }
	inline String_t* get_validatedFixedValue_53() const { return ___validatedFixedValue_53; }
	inline String_t** get_address_of_validatedFixedValue_53() { return &___validatedFixedValue_53; }
	inline void set_validatedFixedValue_53(String_t* value)
	{
		___validatedFixedValue_53 = value;
		Il2CppCodeGenWriteBarrier((&___validatedFixedValue_53), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAELEMENT_T2433337156_H
#ifndef XMLSCHEMACOMPLEXCONTENTEXTENSION_T655218998_H
#define XMLSCHEMACOMPLEXCONTENTEXTENSION_T655218998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContentExtension
struct  XmlSchemaComplexContentExtension_t655218998  : public XmlSchemaContent_t3733871217
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexContentExtension::any
	XmlSchemaAnyAttribute_t530453212 * ___any_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexContentExtension::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaComplexContentExtension::baseTypeName
	XmlQualifiedName_t1944712516 * ___baseTypeName_19;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexContentExtension::particle
	XmlSchemaParticle_t3365045970 * ___particle_20;

public:
	inline static int32_t get_offset_of_any_17() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t655218998, ___any_17)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_any_17() const { return ___any_17; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_any_17() { return &___any_17; }
	inline void set_any_17(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___any_17 = value;
		Il2CppCodeGenWriteBarrier((&___any_17), value);
	}

	inline static int32_t get_offset_of_attributes_18() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t655218998, ___attributes_18)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_18() const { return ___attributes_18; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_18() { return &___attributes_18; }
	inline void set_attributes_18(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_18 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_18), value);
	}

	inline static int32_t get_offset_of_baseTypeName_19() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t655218998, ___baseTypeName_19)); }
	inline XmlQualifiedName_t1944712516 * get_baseTypeName_19() const { return ___baseTypeName_19; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_baseTypeName_19() { return &___baseTypeName_19; }
	inline void set_baseTypeName_19(XmlQualifiedName_t1944712516 * value)
	{
		___baseTypeName_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_19), value);
	}

	inline static int32_t get_offset_of_particle_20() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t655218998, ___particle_20)); }
	inline XmlSchemaParticle_t3365045970 * get_particle_20() const { return ___particle_20; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_particle_20() { return &___particle_20; }
	inline void set_particle_20(XmlSchemaParticle_t3365045970 * value)
	{
		___particle_20 = value;
		Il2CppCodeGenWriteBarrier((&___particle_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXCONTENTEXTENSION_T655218998_H
#ifndef XMLSCHEMACOMPLEXCONTENTRESTRICTION_T1722137421_H
#define XMLSCHEMACOMPLEXCONTENTRESTRICTION_T1722137421_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContentRestriction
struct  XmlSchemaComplexContentRestriction_t1722137421  : public XmlSchemaContent_t3733871217
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexContentRestriction::any
	XmlSchemaAnyAttribute_t530453212 * ___any_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexContentRestriction::attributes
	XmlSchemaObjectCollection_t395083109 * ___attributes_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaComplexContentRestriction::baseTypeName
	XmlQualifiedName_t1944712516 * ___baseTypeName_19;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexContentRestriction::particle
	XmlSchemaParticle_t3365045970 * ___particle_20;

public:
	inline static int32_t get_offset_of_any_17() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t1722137421, ___any_17)); }
	inline XmlSchemaAnyAttribute_t530453212 * get_any_17() const { return ___any_17; }
	inline XmlSchemaAnyAttribute_t530453212 ** get_address_of_any_17() { return &___any_17; }
	inline void set_any_17(XmlSchemaAnyAttribute_t530453212 * value)
	{
		___any_17 = value;
		Il2CppCodeGenWriteBarrier((&___any_17), value);
	}

	inline static int32_t get_offset_of_attributes_18() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t1722137421, ___attributes_18)); }
	inline XmlSchemaObjectCollection_t395083109 * get_attributes_18() const { return ___attributes_18; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_attributes_18() { return &___attributes_18; }
	inline void set_attributes_18(XmlSchemaObjectCollection_t395083109 * value)
	{
		___attributes_18 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_18), value);
	}

	inline static int32_t get_offset_of_baseTypeName_19() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t1722137421, ___baseTypeName_19)); }
	inline XmlQualifiedName_t1944712516 * get_baseTypeName_19() const { return ___baseTypeName_19; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_baseTypeName_19() { return &___baseTypeName_19; }
	inline void set_baseTypeName_19(XmlQualifiedName_t1944712516 * value)
	{
		___baseTypeName_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_19), value);
	}

	inline static int32_t get_offset_of_particle_20() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_t1722137421, ___particle_20)); }
	inline XmlSchemaParticle_t3365045970 * get_particle_20() const { return ___particle_20; }
	inline XmlSchemaParticle_t3365045970 ** get_address_of_particle_20() { return &___particle_20; }
	inline void set_particle_20(XmlSchemaParticle_t3365045970 * value)
	{
		___particle_20 = value;
		Il2CppCodeGenWriteBarrier((&___particle_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXCONTENTRESTRICTION_T1722137421_H
#ifndef XSDGYEAR_T3810382607_H
#define XSDGYEAR_T3810382607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYear
struct  XsdGYear_t3810382607  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEAR_T3810382607_H
#ifndef XSDDURATION_T1605638443_H
#define XSDDURATION_T1605638443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDuration
struct  XsdDuration_t1605638443  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDURATION_T1605638443_H
#ifndef XSDDATETIME_T1344468684_H
#define XSDDATETIME_T1344468684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDateTime
struct  XsdDateTime_t1344468684  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIME_T1344468684_H
#ifndef XSDHEXBINARY_T3496718151_H
#define XSDHEXBINARY_T3496718151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdHexBinary
struct  XsdHexBinary_t3496718151  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDHEXBINARY_T3496718151_H
#ifndef XSDBOOLEAN_T4126353587_H
#define XSDBOOLEAN_T4126353587_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBoolean
struct  XsdBoolean_t4126353587  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBOOLEAN_T4126353587_H
#ifndef XSDGYEARMONTH_T1363357165_H
#define XSDGYEARMONTH_T1363357165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYearMonth
struct  XsdGYearMonth_t1363357165  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEARMONTH_T1363357165_H
#ifndef XSDGMONTHDAY_T3859978294_H
#define XSDGMONTHDAY_T3859978294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonthDay
struct  XsdGMonthDay_t3859978294  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTHDAY_T3859978294_H
#ifndef XSDDATE_T919459387_H
#define XSDDATE_T919459387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDate
struct  XsdDate_t919459387  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATE_T919459387_H
#ifndef XSDTIME_T4165680512_H
#define XSDTIME_T4165680512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdTime
struct  XsdTime_t4165680512  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

struct XsdTime_t4165680512_StaticFields
{
public:
	// System.String[] Mono.Xml.Schema.XsdTime::timeFormats
	StringU5BU5D_t1642385972* ___timeFormats_61;

public:
	inline static int32_t get_offset_of_timeFormats_61() { return static_cast<int32_t>(offsetof(XsdTime_t4165680512_StaticFields, ___timeFormats_61)); }
	inline StringU5BU5D_t1642385972* get_timeFormats_61() const { return ___timeFormats_61; }
	inline StringU5BU5D_t1642385972** get_address_of_timeFormats_61() { return &___timeFormats_61; }
	inline void set_timeFormats_61(StringU5BU5D_t1642385972* value)
	{
		___timeFormats_61 = value;
		Il2CppCodeGenWriteBarrier((&___timeFormats_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTIME_T4165680512_H
#ifndef XMLSCHEMAMAXEXCLUSIVEFACET_T2593226405_H
#define XMLSCHEMAMAXEXCLUSIVEFACET_T2593226405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMaxExclusiveFacet
struct  XmlSchemaMaxExclusiveFacet_t2593226405  : public XmlSchemaFacet_t614309579
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMAXEXCLUSIVEFACET_T2593226405_H
#ifndef XMLSCHEMAKEYREF_T1894386400_H
#define XMLSCHEMAKEYREF_T1894386400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaKeyref
struct  XmlSchemaKeyref_t1894386400  : public XmlSchemaIdentityConstraint_t1058613623
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaKeyref::refer
	XmlQualifiedName_t1944712516 * ___refer_21;
	// System.Xml.Schema.XmlSchemaIdentityConstraint System.Xml.Schema.XmlSchemaKeyref::target
	XmlSchemaIdentityConstraint_t1058613623 * ___target_22;

public:
	inline static int32_t get_offset_of_refer_21() { return static_cast<int32_t>(offsetof(XmlSchemaKeyref_t1894386400, ___refer_21)); }
	inline XmlQualifiedName_t1944712516 * get_refer_21() const { return ___refer_21; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refer_21() { return &___refer_21; }
	inline void set_refer_21(XmlQualifiedName_t1944712516 * value)
	{
		___refer_21 = value;
		Il2CppCodeGenWriteBarrier((&___refer_21), value);
	}

	inline static int32_t get_offset_of_target_22() { return static_cast<int32_t>(offsetof(XmlSchemaKeyref_t1894386400, ___target_22)); }
	inline XmlSchemaIdentityConstraint_t1058613623 * get_target_22() const { return ___target_22; }
	inline XmlSchemaIdentityConstraint_t1058613623 ** get_address_of_target_22() { return &___target_22; }
	inline void set_target_22(XmlSchemaIdentityConstraint_t1058613623 * value)
	{
		___target_22 = value;
		Il2CppCodeGenWriteBarrier((&___target_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAKEYREF_T1894386400_H
#ifndef XMLSCHEMAKEY_T1946917723_H
#define XMLSCHEMAKEY_T1946917723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaKey
struct  XmlSchemaKey_t1946917723  : public XmlSchemaIdentityConstraint_t1058613623
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAKEY_T1946917723_H
#ifndef XMLSCHEMAMAXINCLUSIVEFACET_T663510947_H
#define XMLSCHEMAMAXINCLUSIVEFACET_T663510947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMaxInclusiveFacet
struct  XmlSchemaMaxInclusiveFacet_t663510947  : public XmlSchemaFacet_t614309579
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMAXINCLUSIVEFACET_T663510947_H
#ifndef XMLSCHEMANUMERICFACET_T3887766756_H
#define XMLSCHEMANUMERICFACET_T3887766756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaNumericFacet
struct  XmlSchemaNumericFacet_t3887766756  : public XmlSchemaFacet_t614309579
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMANUMERICFACET_T3887766756_H
#ifndef XMLSCHEMAMININCLUSIVEFACET_T708563817_H
#define XMLSCHEMAMININCLUSIVEFACET_T708563817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMinInclusiveFacet
struct  XmlSchemaMinInclusiveFacet_t708563817  : public XmlSchemaFacet_t614309579
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMININCLUSIVEFACET_T708563817_H
#ifndef XMLSCHEMAMINEXCLUSIVEFACET_T3225721695_H
#define XMLSCHEMAMINEXCLUSIVEFACET_T3225721695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMinExclusiveFacet
struct  XmlSchemaMinExclusiveFacet_t3225721695  : public XmlSchemaFacet_t614309579
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMINEXCLUSIVEFACET_T3225721695_H
#ifndef XSDDOUBLE_T2510112208_H
#define XSDDOUBLE_T2510112208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDouble
struct  XsdDouble_t2510112208  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDOUBLE_T2510112208_H
#ifndef XMLSCHEMAGROUPREF_T3082205844_H
#define XMLSCHEMAGROUPREF_T3082205844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroupRef
struct  XmlSchemaGroupRef_t3082205844  : public XmlSchemaParticle_t3365045970
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaGroupRef::schema
	XmlSchema_t880472818 * ___schema_27;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaGroupRef::refName
	XmlQualifiedName_t1944712516 * ___refName_28;
	// System.Xml.Schema.XmlSchemaGroup System.Xml.Schema.XmlSchemaGroupRef::referencedGroup
	XmlSchemaGroup_t4189650927 * ___referencedGroup_29;
	// System.Boolean System.Xml.Schema.XmlSchemaGroupRef::busy
	bool ___busy_30;

public:
	inline static int32_t get_offset_of_schema_27() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t3082205844, ___schema_27)); }
	inline XmlSchema_t880472818 * get_schema_27() const { return ___schema_27; }
	inline XmlSchema_t880472818 ** get_address_of_schema_27() { return &___schema_27; }
	inline void set_schema_27(XmlSchema_t880472818 * value)
	{
		___schema_27 = value;
		Il2CppCodeGenWriteBarrier((&___schema_27), value);
	}

	inline static int32_t get_offset_of_refName_28() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t3082205844, ___refName_28)); }
	inline XmlQualifiedName_t1944712516 * get_refName_28() const { return ___refName_28; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refName_28() { return &___refName_28; }
	inline void set_refName_28(XmlQualifiedName_t1944712516 * value)
	{
		___refName_28 = value;
		Il2CppCodeGenWriteBarrier((&___refName_28), value);
	}

	inline static int32_t get_offset_of_referencedGroup_29() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t3082205844, ___referencedGroup_29)); }
	inline XmlSchemaGroup_t4189650927 * get_referencedGroup_29() const { return ___referencedGroup_29; }
	inline XmlSchemaGroup_t4189650927 ** get_address_of_referencedGroup_29() { return &___referencedGroup_29; }
	inline void set_referencedGroup_29(XmlSchemaGroup_t4189650927 * value)
	{
		___referencedGroup_29 = value;
		Il2CppCodeGenWriteBarrier((&___referencedGroup_29), value);
	}

	inline static int32_t get_offset_of_busy_30() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t3082205844, ___busy_30)); }
	inline bool get_busy_30() const { return ___busy_30; }
	inline bool* get_address_of_busy_30() { return &___busy_30; }
	inline void set_busy_30(bool value)
	{
		___busy_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUPREF_T3082205844_H
#ifndef XMLSCHEMAENUMERATIONFACET_T3989738874_H
#define XMLSCHEMAENUMERATIONFACET_T3989738874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaEnumerationFacet
struct  XmlSchemaEnumerationFacet_t3989738874  : public XmlSchemaFacet_t614309579
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAENUMERATIONFACET_T3989738874_H
#ifndef XMLSCHEMAGROUPBASE_T3811767860_H
#define XMLSCHEMAGROUPBASE_T3811767860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroupBase
struct  XmlSchemaGroupBase_t3811767860  : public XmlSchemaParticle_t3365045970
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaGroupBase::compiledItems
	XmlSchemaObjectCollection_t395083109 * ___compiledItems_27;

public:
	inline static int32_t get_offset_of_compiledItems_27() { return static_cast<int32_t>(offsetof(XmlSchemaGroupBase_t3811767860, ___compiledItems_27)); }
	inline XmlSchemaObjectCollection_t395083109 * get_compiledItems_27() const { return ___compiledItems_27; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_compiledItems_27() { return &___compiledItems_27; }
	inline void set_compiledItems_27(XmlSchemaObjectCollection_t395083109 * value)
	{
		___compiledItems_27 = value;
		Il2CppCodeGenWriteBarrier((&___compiledItems_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUPBASE_T3811767860_H
#ifndef XSDANYURI_T2527983239_H
#define XSDANYURI_T2527983239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnyURI
struct  XsdAnyURI_t2527983239  : public XsdString_t263933896
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYURI_T2527983239_H
#ifndef XMLSCHEMATOTALDIGITSFACET_T2939281405_H
#define XMLSCHEMATOTALDIGITSFACET_T2939281405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaTotalDigitsFacet
struct  XmlSchemaTotalDigitsFacet_t2939281405  : public XmlSchemaNumericFacet_t3887766756
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMATOTALDIGITSFACET_T2939281405_H
#ifndef XDTDAYTIMEDURATION_T2797717973_H
#define XDTDAYTIMEDURATION_T2797717973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtDayTimeDuration
struct  XdtDayTimeDuration_t2797717973  : public XsdDuration_t1605638443
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTDAYTIMEDURATION_T2797717973_H
#ifndef XSDBASE64BINARY_T1094704629_H
#define XSDBASE64BINARY_T1094704629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBase64Binary
struct  XsdBase64Binary_t1094704629  : public XsdString_t263933896
{
public:

public:
};

struct XsdBase64Binary_t1094704629_StaticFields
{
public:
	// System.String Mono.Xml.Schema.XsdBase64Binary::ALPHABET
	String_t* ___ALPHABET_61;
	// System.Byte[] Mono.Xml.Schema.XsdBase64Binary::decodeTable
	ByteU5BU5D_t3397334013* ___decodeTable_62;

public:
	inline static int32_t get_offset_of_ALPHABET_61() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t1094704629_StaticFields, ___ALPHABET_61)); }
	inline String_t* get_ALPHABET_61() const { return ___ALPHABET_61; }
	inline String_t** get_address_of_ALPHABET_61() { return &___ALPHABET_61; }
	inline void set_ALPHABET_61(String_t* value)
	{
		___ALPHABET_61 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_61), value);
	}

	inline static int32_t get_offset_of_decodeTable_62() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t1094704629_StaticFields, ___decodeTable_62)); }
	inline ByteU5BU5D_t3397334013* get_decodeTable_62() const { return ___decodeTable_62; }
	inline ByteU5BU5D_t3397334013** get_address_of_decodeTable_62() { return &___decodeTable_62; }
	inline void set_decodeTable_62(ByteU5BU5D_t3397334013* value)
	{
		___decodeTable_62 = value;
		Il2CppCodeGenWriteBarrier((&___decodeTable_62), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBASE64BINARY_T1094704629_H
#ifndef XSDNORMALIZEDSTRING_T2132216291_H
#define XSDNORMALIZEDSTRING_T2132216291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNormalizedString
struct  XsdNormalizedString_t2132216291  : public XsdString_t263933896
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNORMALIZEDSTRING_T2132216291_H
#ifndef XMLSCHEMAMINLENGTHFACET_T1871324785_H
#define XMLSCHEMAMINLENGTHFACET_T1871324785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMinLengthFacet
struct  XmlSchemaMinLengthFacet_t1871324785  : public XmlSchemaNumericFacet_t3887766756
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMINLENGTHFACET_T1871324785_H
#ifndef XDTYEARMONTHDURATION_T1764328599_H
#define XDTYEARMONTHDURATION_T1764328599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtYearMonthDuration
struct  XdtYearMonthDuration_t1764328599  : public XsdDuration_t1605638443
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTYEARMONTHDURATION_T1764328599_H
#ifndef XMLSCHEMAALL_T1805755215_H
#define XMLSCHEMAALL_T1805755215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAll
struct  XmlSchemaAll_t1805755215  : public XmlSchemaGroupBase_t3811767860
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaAll::schema
	XmlSchema_t880472818 * ___schema_28;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAll::items
	XmlSchemaObjectCollection_t395083109 * ___items_29;
	// System.Boolean System.Xml.Schema.XmlSchemaAll::emptiable
	bool ___emptiable_30;

public:
	inline static int32_t get_offset_of_schema_28() { return static_cast<int32_t>(offsetof(XmlSchemaAll_t1805755215, ___schema_28)); }
	inline XmlSchema_t880472818 * get_schema_28() const { return ___schema_28; }
	inline XmlSchema_t880472818 ** get_address_of_schema_28() { return &___schema_28; }
	inline void set_schema_28(XmlSchema_t880472818 * value)
	{
		___schema_28 = value;
		Il2CppCodeGenWriteBarrier((&___schema_28), value);
	}

	inline static int32_t get_offset_of_items_29() { return static_cast<int32_t>(offsetof(XmlSchemaAll_t1805755215, ___items_29)); }
	inline XmlSchemaObjectCollection_t395083109 * get_items_29() const { return ___items_29; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_items_29() { return &___items_29; }
	inline void set_items_29(XmlSchemaObjectCollection_t395083109 * value)
	{
		___items_29 = value;
		Il2CppCodeGenWriteBarrier((&___items_29), value);
	}

	inline static int32_t get_offset_of_emptiable_30() { return static_cast<int32_t>(offsetof(XmlSchemaAll_t1805755215, ___emptiable_30)); }
	inline bool get_emptiable_30() const { return ___emptiable_30; }
	inline bool* get_address_of_emptiable_30() { return &___emptiable_30; }
	inline void set_emptiable_30(bool value)
	{
		___emptiable_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAALL_T1805755215_H
#ifndef XMLSCHEMALENGTHFACET_T430394943_H
#define XMLSCHEMALENGTHFACET_T430394943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaLengthFacet
struct  XmlSchemaLengthFacet_t430394943  : public XmlSchemaNumericFacet_t3887766756
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMALENGTHFACET_T430394943_H
#ifndef XMLSCHEMACHOICE_T654568461_H
#define XMLSCHEMACHOICE_T654568461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaChoice
struct  XmlSchemaChoice_t654568461  : public XmlSchemaGroupBase_t3811767860
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaChoice::items
	XmlSchemaObjectCollection_t395083109 * ___items_28;
	// System.Decimal System.Xml.Schema.XmlSchemaChoice::minEffectiveTotalRange
	Decimal_t724701077  ___minEffectiveTotalRange_29;

public:
	inline static int32_t get_offset_of_items_28() { return static_cast<int32_t>(offsetof(XmlSchemaChoice_t654568461, ___items_28)); }
	inline XmlSchemaObjectCollection_t395083109 * get_items_28() const { return ___items_28; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_items_28() { return &___items_28; }
	inline void set_items_28(XmlSchemaObjectCollection_t395083109 * value)
	{
		___items_28 = value;
		Il2CppCodeGenWriteBarrier((&___items_28), value);
	}

	inline static int32_t get_offset_of_minEffectiveTotalRange_29() { return static_cast<int32_t>(offsetof(XmlSchemaChoice_t654568461, ___minEffectiveTotalRange_29)); }
	inline Decimal_t724701077  get_minEffectiveTotalRange_29() const { return ___minEffectiveTotalRange_29; }
	inline Decimal_t724701077 * get_address_of_minEffectiveTotalRange_29() { return &___minEffectiveTotalRange_29; }
	inline void set_minEffectiveTotalRange_29(Decimal_t724701077  value)
	{
		___minEffectiveTotalRange_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACHOICE_T654568461_H
#ifndef XMLSCHEMAFRACTIONDIGITSFACET_T2708215647_H
#define XMLSCHEMAFRACTIONDIGITSFACET_T2708215647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFractionDigitsFacet
struct  XmlSchemaFractionDigitsFacet_t2708215647  : public XmlSchemaNumericFacet_t3887766756
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFRACTIONDIGITSFACET_T2708215647_H
#ifndef XMLSCHEMAMAXLENGTHFACET_T3225833099_H
#define XMLSCHEMAMAXLENGTHFACET_T3225833099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMaxLengthFacet
struct  XmlSchemaMaxLengthFacet_t3225833099  : public XmlSchemaNumericFacet_t3887766756
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMAXLENGTHFACET_T3225833099_H
#ifndef XMLSCHEMASEQUENCE_T728414063_H
#define XMLSCHEMASEQUENCE_T728414063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSequence
struct  XmlSchemaSequence_t728414063  : public XmlSchemaGroupBase_t3811767860
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSequence::items
	XmlSchemaObjectCollection_t395083109 * ___items_28;

public:
	inline static int32_t get_offset_of_items_28() { return static_cast<int32_t>(offsetof(XmlSchemaSequence_t728414063, ___items_28)); }
	inline XmlSchemaObjectCollection_t395083109 * get_items_28() const { return ___items_28; }
	inline XmlSchemaObjectCollection_t395083109 ** get_address_of_items_28() { return &___items_28; }
	inline void set_items_28(XmlSchemaObjectCollection_t395083109 * value)
	{
		___items_28 = value;
		Il2CppCodeGenWriteBarrier((&___items_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASEQUENCE_T728414063_H
#ifndef XSDTOKEN_T2902215462_H
#define XSDTOKEN_T2902215462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdToken
struct  XsdToken_t2902215462  : public XsdNormalizedString_t2132216291
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTOKEN_T2902215462_H
#ifndef XSDNAME_T1409945702_H
#define XSDNAME_T1409945702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdName
struct  XsdName_t1409945702  : public XsdToken_t2902215462
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNAME_T1409945702_H
#ifndef XSDQNAME_T930779123_H
#define XSDQNAME_T930779123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdQName
struct  XsdQName_t930779123  : public XsdName_t1409945702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDQNAME_T930779123_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (XsdDouble_t2510112208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (XsdBase64Binary_t1094704629), -1, sizeof(XsdBase64Binary_t1094704629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1701[2] = 
{
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t1094704629_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (XsdHexBinary_t3496718151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (XsdQName_t930779123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (XsdBoolean_t4126353587), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (XsdAnyURI_t2527983239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (XmlSchemaUri_t1295878664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[1] = 
{
	XmlSchemaUri_t1295878664::get_offset_of_value_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (XsdDuration_t1605638443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (XdtDayTimeDuration_t2797717973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (XdtYearMonthDuration_t1764328599), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (XsdDateTime_t1344468684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (XsdDate_t919459387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (XsdTime_t4165680512), -1, sizeof(XsdTime_t4165680512_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1712[1] = 
{
	XsdTime_t4165680512_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (XsdGYearMonth_t1363357165), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (XsdGMonthDay_t3859978294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (XsdGYear_t3810382607), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (XsdGMonth_t4076673358), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (XsdGDay_t1914244270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (QNameValueType_t2109511131)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[1] = 
{
	QNameValueType_t2109511131::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (StringValueType_t652312500)+ sizeof (RuntimeObject), sizeof(StringValueType_t652312500_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[1] = 
{
	StringValueType_t652312500::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (UriValueType_t1626089757)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[1] = 
{
	UriValueType_t1626089757::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (StringArrayValueType_t1731700877)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[1] = 
{
	StringArrayValueType_t1731700877::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (ValidationEventArgs_t1577905814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[3] = 
{
	ValidationEventArgs_t1577905814::get_offset_of_exception_1(),
	ValidationEventArgs_t1577905814::get_offset_of_message_2(),
	ValidationEventArgs_t1577905814::get_offset_of_severity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (ValidationHandler_t3342756761), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (XmlAtomicValue_t752869371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[11] = 
{
	XmlAtomicValue_t752869371::get_offset_of_booleanValue_0(),
	XmlAtomicValue_t752869371::get_offset_of_dateTimeValue_1(),
	XmlAtomicValue_t752869371::get_offset_of_decimalValue_2(),
	XmlAtomicValue_t752869371::get_offset_of_doubleValue_3(),
	XmlAtomicValue_t752869371::get_offset_of_intValue_4(),
	XmlAtomicValue_t752869371::get_offset_of_longValue_5(),
	XmlAtomicValue_t752869371::get_offset_of_objectValue_6(),
	XmlAtomicValue_t752869371::get_offset_of_floatValue_7(),
	XmlAtomicValue_t752869371::get_offset_of_stringValue_8(),
	XmlAtomicValue_t752869371::get_offset_of_schemaType_9(),
	XmlAtomicValue_t752869371::get_offset_of_xmlTypeCode_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (XmlSchema_t880472818), -1, sizeof(XmlSchema_t880472818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1726[20] = 
{
	XmlSchema_t880472818::get_offset_of_attributeFormDefault_13(),
	XmlSchema_t880472818::get_offset_of_attributeGroups_14(),
	XmlSchema_t880472818::get_offset_of_attributes_15(),
	XmlSchema_t880472818::get_offset_of_blockDefault_16(),
	XmlSchema_t880472818::get_offset_of_elementFormDefault_17(),
	XmlSchema_t880472818::get_offset_of_elements_18(),
	XmlSchema_t880472818::get_offset_of_finalDefault_19(),
	XmlSchema_t880472818::get_offset_of_groups_20(),
	XmlSchema_t880472818::get_offset_of_id_21(),
	XmlSchema_t880472818::get_offset_of_includes_22(),
	XmlSchema_t880472818::get_offset_of_items_23(),
	XmlSchema_t880472818::get_offset_of_notations_24(),
	XmlSchema_t880472818::get_offset_of_schemaTypes_25(),
	XmlSchema_t880472818::get_offset_of_targetNamespace_26(),
	XmlSchema_t880472818::get_offset_of_version_27(),
	XmlSchema_t880472818::get_offset_of_schemas_28(),
	XmlSchema_t880472818::get_offset_of_nameTable_29(),
	XmlSchema_t880472818::get_offset_of_missedSubComponents_30(),
	XmlSchema_t880472818::get_offset_of_compilationItems_31(),
	XmlSchema_t880472818_StaticFields::get_offset_of_U3CU3Ef__switchU24map2D_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (XmlSchemaAll_t1805755215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[3] = 
{
	XmlSchemaAll_t1805755215::get_offset_of_schema_28(),
	XmlSchemaAll_t1805755215::get_offset_of_items_29(),
	XmlSchemaAll_t1805755215::get_offset_of_emptiable_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (XmlSchemaAnnotated_t2082486936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[3] = 
{
	XmlSchemaAnnotated_t2082486936::get_offset_of_annotation_13(),
	XmlSchemaAnnotated_t2082486936::get_offset_of_id_14(),
	XmlSchemaAnnotated_t2082486936::get_offset_of_unhandledAttributes_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (XmlSchemaAnnotation_t2400301303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[2] = 
{
	XmlSchemaAnnotation_t2400301303::get_offset_of_id_13(),
	XmlSchemaAnnotation_t2400301303::get_offset_of_items_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (XmlSchemaAny_t3277730824), -1, sizeof(XmlSchemaAny_t3277730824_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1730[4] = 
{
	XmlSchemaAny_t3277730824_StaticFields::get_offset_of_anyTypeContent_27(),
	XmlSchemaAny_t3277730824::get_offset_of_nameSpace_28(),
	XmlSchemaAny_t3277730824::get_offset_of_processing_29(),
	XmlSchemaAny_t3277730824::get_offset_of_wildcard_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (XmlSchemaAnyAttribute_t530453212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[3] = 
{
	XmlSchemaAnyAttribute_t530453212::get_offset_of_nameSpace_16(),
	XmlSchemaAnyAttribute_t530453212::get_offset_of_processing_17(),
	XmlSchemaAnyAttribute_t530453212::get_offset_of_wildcard_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (XmlSchemaAppInfo_t2033489551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[2] = 
{
	XmlSchemaAppInfo_t2033489551::get_offset_of_markup_13(),
	XmlSchemaAppInfo_t2033489551::get_offset_of_source_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (XmlSchemaAttribute_t4015859774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[17] = 
{
	XmlSchemaAttribute_t4015859774::get_offset_of_attributeType_16(),
	XmlSchemaAttribute_t4015859774::get_offset_of_attributeSchemaType_17(),
	XmlSchemaAttribute_t4015859774::get_offset_of_defaultValue_18(),
	XmlSchemaAttribute_t4015859774::get_offset_of_fixedValue_19(),
	XmlSchemaAttribute_t4015859774::get_offset_of_validatedDefaultValue_20(),
	XmlSchemaAttribute_t4015859774::get_offset_of_validatedFixedValue_21(),
	XmlSchemaAttribute_t4015859774::get_offset_of_validatedFixedTypedValue_22(),
	XmlSchemaAttribute_t4015859774::get_offset_of_form_23(),
	XmlSchemaAttribute_t4015859774::get_offset_of_name_24(),
	XmlSchemaAttribute_t4015859774::get_offset_of_targetNamespace_25(),
	XmlSchemaAttribute_t4015859774::get_offset_of_qualifiedName_26(),
	XmlSchemaAttribute_t4015859774::get_offset_of_refName_27(),
	XmlSchemaAttribute_t4015859774::get_offset_of_schemaType_28(),
	XmlSchemaAttribute_t4015859774::get_offset_of_schemaTypeName_29(),
	XmlSchemaAttribute_t4015859774::get_offset_of_use_30(),
	XmlSchemaAttribute_t4015859774::get_offset_of_validatedUse_31(),
	XmlSchemaAttribute_t4015859774::get_offset_of_referencedAttribute_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (XmlSchemaAttributeGroup_t491156493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[8] = 
{
	XmlSchemaAttributeGroup_t491156493::get_offset_of_anyAttribute_16(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_attributes_17(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_name_18(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_redefined_19(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_qualifiedName_20(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_attributeUses_21(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_anyAttributeUse_22(),
	XmlSchemaAttributeGroup_t491156493::get_offset_of_AttributeGroupRecursionCheck_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (XmlSchemaAttributeGroupRef_t825996660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[1] = 
{
	XmlSchemaAttributeGroupRef_t825996660::get_offset_of_refName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (XmlSchemaChoice_t654568461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[2] = 
{
	XmlSchemaChoice_t654568461::get_offset_of_items_28(),
	XmlSchemaChoice_t654568461::get_offset_of_minEffectiveTotalRange_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (XmlSchemaCollection_t3518500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[2] = 
{
	XmlSchemaCollection_t3518500204::get_offset_of_schemaSet_0(),
	XmlSchemaCollection_t3518500204::get_offset_of_ValidationEventHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (XmlSchemaCollectionEnumerator_t1538181312), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[1] = 
{
	XmlSchemaCollectionEnumerator_t1538181312::get_offset_of_xenum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (XmlSchemaCompilationSettings_t2971213394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[1] = 
{
	XmlSchemaCompilationSettings_t2971213394::get_offset_of_enable_upa_check_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (XmlSchemaComplexContent_t2065934415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[2] = 
{
	XmlSchemaComplexContent_t2065934415::get_offset_of_content_16(),
	XmlSchemaComplexContent_t2065934415::get_offset_of_isMixed_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (XmlSchemaComplexContentExtension_t655218998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[4] = 
{
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_any_17(),
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_attributes_18(),
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentExtension_t655218998::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (XmlSchemaComplexContentRestriction_t1722137421), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1742[4] = 
{
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_any_17(),
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_attributes_18(),
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentRestriction_t1722137421::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (XmlSchemaComplexType_t4086789226), -1, sizeof(XmlSchemaComplexType_t4086789226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1743[17] = 
{
	XmlSchemaComplexType_t4086789226::get_offset_of_anyAttribute_28(),
	XmlSchemaComplexType_t4086789226::get_offset_of_attributes_29(),
	XmlSchemaComplexType_t4086789226::get_offset_of_attributeUses_30(),
	XmlSchemaComplexType_t4086789226::get_offset_of_attributeWildcard_31(),
	XmlSchemaComplexType_t4086789226::get_offset_of_block_32(),
	XmlSchemaComplexType_t4086789226::get_offset_of_blockResolved_33(),
	XmlSchemaComplexType_t4086789226::get_offset_of_contentModel_34(),
	XmlSchemaComplexType_t4086789226::get_offset_of_validatableParticle_35(),
	XmlSchemaComplexType_t4086789226::get_offset_of_contentTypeParticle_36(),
	XmlSchemaComplexType_t4086789226::get_offset_of_isAbstract_37(),
	XmlSchemaComplexType_t4086789226::get_offset_of_isMixed_38(),
	XmlSchemaComplexType_t4086789226::get_offset_of_particle_39(),
	XmlSchemaComplexType_t4086789226::get_offset_of_resolvedContentType_40(),
	XmlSchemaComplexType_t4086789226::get_offset_of_ValidatedIsAbstract_41(),
	XmlSchemaComplexType_t4086789226_StaticFields::get_offset_of_anyType_42(),
	XmlSchemaComplexType_t4086789226_StaticFields::get_offset_of_AnyTypeName_43(),
	XmlSchemaComplexType_t4086789226::get_offset_of_CollectProcessId_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (XmlSchemaContent_t3733871217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1744[1] = 
{
	XmlSchemaContent_t3733871217::get_offset_of_actualBaseSchemaType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (XmlSchemaContentModel_t907989596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (XmlSchemaContentProcessing_t74226324)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1746[5] = 
{
	XmlSchemaContentProcessing_t74226324::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (XmlSchemaContentType_t2874429441)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1747[5] = 
{
	XmlSchemaContentType_t2874429441::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (XmlSchemaDatatype_t1195946242), -1, sizeof(XmlSchemaDatatype_t1195946242_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1748[55] = 
{
	XmlSchemaDatatype_t1195946242::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t1195946242::get_offset_of_sb_2(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_52(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_53(),
	XmlSchemaDatatype_t1195946242_StaticFields::get_offset_of_U3CU3Ef__switchU24map2C_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (XmlSchemaDerivationMethod_t3165007540)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1749[9] = 
{
	XmlSchemaDerivationMethod_t3165007540::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (XmlSchemaDocumentation_t3832803992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[3] = 
{
	XmlSchemaDocumentation_t3832803992::get_offset_of_language_13(),
	XmlSchemaDocumentation_t3832803992::get_offset_of_markup_14(),
	XmlSchemaDocumentation_t3832803992::get_offset_of_source_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (XmlSchemaElement_t2433337156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[27] = 
{
	XmlSchemaElement_t2433337156::get_offset_of_block_27(),
	XmlSchemaElement_t2433337156::get_offset_of_constraints_28(),
	XmlSchemaElement_t2433337156::get_offset_of_defaultValue_29(),
	XmlSchemaElement_t2433337156::get_offset_of_elementType_30(),
	XmlSchemaElement_t2433337156::get_offset_of_elementSchemaType_31(),
	XmlSchemaElement_t2433337156::get_offset_of_final_32(),
	XmlSchemaElement_t2433337156::get_offset_of_fixedValue_33(),
	XmlSchemaElement_t2433337156::get_offset_of_form_34(),
	XmlSchemaElement_t2433337156::get_offset_of_isAbstract_35(),
	XmlSchemaElement_t2433337156::get_offset_of_isNillable_36(),
	XmlSchemaElement_t2433337156::get_offset_of_name_37(),
	XmlSchemaElement_t2433337156::get_offset_of_refName_38(),
	XmlSchemaElement_t2433337156::get_offset_of_schemaType_39(),
	XmlSchemaElement_t2433337156::get_offset_of_schemaTypeName_40(),
	XmlSchemaElement_t2433337156::get_offset_of_substitutionGroup_41(),
	XmlSchemaElement_t2433337156::get_offset_of_schema_42(),
	XmlSchemaElement_t2433337156::get_offset_of_parentIsSchema_43(),
	XmlSchemaElement_t2433337156::get_offset_of_qName_44(),
	XmlSchemaElement_t2433337156::get_offset_of_blockResolved_45(),
	XmlSchemaElement_t2433337156::get_offset_of_finalResolved_46(),
	XmlSchemaElement_t2433337156::get_offset_of_referencedElement_47(),
	XmlSchemaElement_t2433337156::get_offset_of_substitutingElements_48(),
	XmlSchemaElement_t2433337156::get_offset_of_substitutionGroupElement_49(),
	XmlSchemaElement_t2433337156::get_offset_of_actualIsAbstract_50(),
	XmlSchemaElement_t2433337156::get_offset_of_actualIsNillable_51(),
	XmlSchemaElement_t2433337156::get_offset_of_validatedDefaultValue_52(),
	XmlSchemaElement_t2433337156::get_offset_of_validatedFixedValue_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (XmlSchemaEnumerationFacet_t3989738874), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (XmlSchemaException_t4082200141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1753[5] = 
{
	XmlSchemaException_t4082200141::get_offset_of_hasLineInfo_11(),
	XmlSchemaException_t4082200141::get_offset_of_lineNumber_12(),
	XmlSchemaException_t4082200141::get_offset_of_linePosition_13(),
	XmlSchemaException_t4082200141::get_offset_of_sourceObj_14(),
	XmlSchemaException_t4082200141::get_offset_of_sourceUri_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (XmlSchemaExternal_t3943748629), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[3] = 
{
	XmlSchemaExternal_t3943748629::get_offset_of_id_13(),
	XmlSchemaExternal_t3943748629::get_offset_of_schema_14(),
	XmlSchemaExternal_t3943748629::get_offset_of_location_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (XmlSchemaFacet_t614309579), -1, sizeof(XmlSchemaFacet_t614309579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1755[3] = 
{
	XmlSchemaFacet_t614309579_StaticFields::get_offset_of_AllFacets_16(),
	XmlSchemaFacet_t614309579::get_offset_of_isFixed_17(),
	XmlSchemaFacet_t614309579::get_offset_of_val_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (Facet_t3019654938)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1756[14] = 
{
	Facet_t3019654938::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (XmlSchemaForm_t1143227640)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1757[4] = 
{
	XmlSchemaForm_t1143227640::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (XmlSchemaFractionDigitsFacet_t2708215647), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (XmlSchemaGroup_t4189650927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[4] = 
{
	XmlSchemaGroup_t4189650927::get_offset_of_name_16(),
	XmlSchemaGroup_t4189650927::get_offset_of_particle_17(),
	XmlSchemaGroup_t4189650927::get_offset_of_qualifiedName_18(),
	XmlSchemaGroup_t4189650927::get_offset_of_isCircularDefinition_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (XmlSchemaGroupBase_t3811767860), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[1] = 
{
	XmlSchemaGroupBase_t3811767860::get_offset_of_compiledItems_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (XmlSchemaGroupRef_t3082205844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[4] = 
{
	XmlSchemaGroupRef_t3082205844::get_offset_of_schema_27(),
	XmlSchemaGroupRef_t3082205844::get_offset_of_refName_28(),
	XmlSchemaGroupRef_t3082205844::get_offset_of_referencedGroup_29(),
	XmlSchemaGroupRef_t3082205844::get_offset_of_busy_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (XmlSchemaIdentityConstraint_t1058613623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[5] = 
{
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_fields_16(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_name_17(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_qName_18(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_selector_19(),
	XmlSchemaIdentityConstraint_t1058613623::get_offset_of_compiledSelector_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (XmlSchemaImport_t250324363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[2] = 
{
	XmlSchemaImport_t250324363::get_offset_of_annotation_16(),
	XmlSchemaImport_t250324363::get_offset_of_nameSpace_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (XmlSchemaInclude_t2752556710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[1] = 
{
	XmlSchemaInclude_t2752556710::get_offset_of_annotation_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (XmlSchemaInfo_t2864028808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[7] = 
{
	XmlSchemaInfo_t2864028808::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t2864028808::get_offset_of_isNil_1(),
	XmlSchemaInfo_t2864028808::get_offset_of_memberType_2(),
	XmlSchemaInfo_t2864028808::get_offset_of_attr_3(),
	XmlSchemaInfo_t2864028808::get_offset_of_elem_4(),
	XmlSchemaInfo_t2864028808::get_offset_of_type_5(),
	XmlSchemaInfo_t2864028808::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (XmlSchemaKey_t1946917723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (XmlSchemaKeyref_t1894386400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[2] = 
{
	XmlSchemaKeyref_t1894386400::get_offset_of_refer_21(),
	XmlSchemaKeyref_t1894386400::get_offset_of_target_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (XmlSchemaLengthFacet_t430394943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (XmlSchemaMaxExclusiveFacet_t2593226405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (XmlSchemaMaxInclusiveFacet_t663510947), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (XmlSchemaMaxLengthFacet_t3225833099), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (XmlSchemaMinExclusiveFacet_t3225721695), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (XmlSchemaMinInclusiveFacet_t708563817), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (XmlSchemaMinLengthFacet_t1871324785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (XmlSchemaNotation_t346281646), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[4] = 
{
	XmlSchemaNotation_t346281646::get_offset_of_name_16(),
	XmlSchemaNotation_t346281646::get_offset_of_pub_17(),
	XmlSchemaNotation_t346281646::get_offset_of_system_18(),
	XmlSchemaNotation_t346281646::get_offset_of_qualifiedName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (XmlSchemaNumericFacet_t3887766756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (XmlSchemaObject_t2050913741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[13] = 
{
	XmlSchemaObject_t2050913741::get_offset_of_lineNumber_0(),
	XmlSchemaObject_t2050913741::get_offset_of_linePosition_1(),
	XmlSchemaObject_t2050913741::get_offset_of_sourceUri_2(),
	XmlSchemaObject_t2050913741::get_offset_of_namespaces_3(),
	XmlSchemaObject_t2050913741::get_offset_of_unhandledAttributeList_4(),
	XmlSchemaObject_t2050913741::get_offset_of_isCompiled_5(),
	XmlSchemaObject_t2050913741::get_offset_of_errorCount_6(),
	XmlSchemaObject_t2050913741::get_offset_of_CompilationId_7(),
	XmlSchemaObject_t2050913741::get_offset_of_ValidationId_8(),
	XmlSchemaObject_t2050913741::get_offset_of_isRedefineChild_9(),
	XmlSchemaObject_t2050913741::get_offset_of_isRedefinedComponent_10(),
	XmlSchemaObject_t2050913741::get_offset_of_redefinedObject_11(),
	XmlSchemaObject_t2050913741::get_offset_of_parent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (XmlSchemaObjectCollection_t395083109), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (XmlSchemaObjectEnumerator_t2354997415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[1] = 
{
	XmlSchemaObjectEnumerator_t2354997415::get_offset_of_ienum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (XmlSchemaObjectTable_t3364835593), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[1] = 
{
	XmlSchemaObjectTable_t3364835593::get_offset_of_table_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (XmlSchemaObjectTableEnumerator_t2908487920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[2] = 
{
	XmlSchemaObjectTableEnumerator_t2908487920::get_offset_of_xenum_0(),
	XmlSchemaObjectTableEnumerator_t2908487920::get_offset_of_tmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (XmlSchemaParticle_t3365045970), -1, sizeof(XmlSchemaParticle_t3365045970_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1782[11] = 
{
	XmlSchemaParticle_t3365045970::get_offset_of_minOccurs_16(),
	XmlSchemaParticle_t3365045970::get_offset_of_maxOccurs_17(),
	XmlSchemaParticle_t3365045970::get_offset_of_minstr_18(),
	XmlSchemaParticle_t3365045970::get_offset_of_maxstr_19(),
	XmlSchemaParticle_t3365045970_StaticFields::get_offset_of_empty_20(),
	XmlSchemaParticle_t3365045970::get_offset_of_validatedMinOccurs_21(),
	XmlSchemaParticle_t3365045970::get_offset_of_validatedMaxOccurs_22(),
	XmlSchemaParticle_t3365045970::get_offset_of_recursionDepth_23(),
	XmlSchemaParticle_t3365045970::get_offset_of_minEffectiveTotalRange_24(),
	XmlSchemaParticle_t3365045970::get_offset_of_parentIsGroupDefinition_25(),
	XmlSchemaParticle_t3365045970::get_offset_of_OptimizedParticle_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (EmptyParticle_t446815059), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (XmlSchemaPatternFacet_t2024909611), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (XmlSchemaReader_t3786681597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[3] = 
{
	XmlSchemaReader_t3786681597::get_offset_of_reader_2(),
	XmlSchemaReader_t3786681597::get_offset_of_handler_3(),
	XmlSchemaReader_t3786681597::get_offset_of_hasLineInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (XmlSchemaRedefine_t3478619248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[4] = 
{
	XmlSchemaRedefine_t3478619248::get_offset_of_attributeGroups_16(),
	XmlSchemaRedefine_t3478619248::get_offset_of_groups_17(),
	XmlSchemaRedefine_t3478619248::get_offset_of_items_18(),
	XmlSchemaRedefine_t3478619248::get_offset_of_schemaTypes_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (XmlSchemaSequence_t728414063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[1] = 
{
	XmlSchemaSequence_t728414063::get_offset_of_items_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (XmlSchemaSet_t313318308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1788[12] = 
{
	XmlSchemaSet_t313318308::get_offset_of_nameTable_0(),
	XmlSchemaSet_t313318308::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t313318308::get_offset_of_schemas_2(),
	XmlSchemaSet_t313318308::get_offset_of_attributes_3(),
	XmlSchemaSet_t313318308::get_offset_of_elements_4(),
	XmlSchemaSet_t313318308::get_offset_of_types_5(),
	XmlSchemaSet_t313318308::get_offset_of_idCollection_6(),
	XmlSchemaSet_t313318308::get_offset_of_namedIdentities_7(),
	XmlSchemaSet_t313318308::get_offset_of_settings_8(),
	XmlSchemaSet_t313318308::get_offset_of_isCompiled_9(),
	XmlSchemaSet_t313318308::get_offset_of_CompilationId_10(),
	XmlSchemaSet_t313318308::get_offset_of_ValidationEventHandler_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (XmlSchemaSimpleContent_t2303138587), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[1] = 
{
	XmlSchemaSimpleContent_t2303138587::get_offset_of_content_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (XmlSchemaSimpleContentExtension_t3718357176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1790[3] = 
{
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_any_17(),
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentExtension_t3718357176::get_offset_of_baseTypeName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (XmlSchemaSimpleContentRestriction_t2728776481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[5] = 
{
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_any_17(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_baseType_19(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_baseTypeName_20(),
	XmlSchemaSimpleContentRestriction_t2728776481::get_offset_of_facets_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (XmlSchemaSimpleType_t248156492), -1, sizeof(XmlSchemaSimpleType_t248156492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1792[54] = 
{
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_schemaLocationType_28(),
	XmlSchemaSimpleType_t248156492::get_offset_of_content_29(),
	XmlSchemaSimpleType_t248156492::get_offset_of_islocal_30(),
	XmlSchemaSimpleType_t248156492::get_offset_of_recursed_31(),
	XmlSchemaSimpleType_t248156492::get_offset_of_variety_32(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnySimpleType_33(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsString_34(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBoolean_35(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDecimal_36(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsFloat_37(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDouble_38(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDuration_39(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDateTime_40(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsTime_41(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsDate_42(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYearMonth_43(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGYear_44(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonthDay_45(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGDay_46(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsGMonth_47(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsHexBinary_48(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsBase64Binary_49(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsAnyUri_50(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsQName_51(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNotation_52(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNormalizedString_53(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsToken_54(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLanguage_55(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMToken_56(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNMTokens_57(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsName_58(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNCName_59(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsID_60(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRef_61(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsIDRefs_62(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntity_63(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsEntities_64(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInteger_65(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonPositiveInteger_66(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNegativeInteger_67(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsLong_68(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsInt_69(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsShort_70(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsByte_71(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsNonNegativeInteger_72(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedLong_73(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedInt_74(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedShort_75(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsUnsignedByte_76(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XsPositiveInteger_77(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtUntypedAtomic_78(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtAnyAtomicType_79(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtYearMonthDuration_80(),
	XmlSchemaSimpleType_t248156492_StaticFields::get_offset_of_XdtDayTimeDuration_81(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (XmlSchemaSimpleTypeContent_t1606103299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[1] = 
{
	XmlSchemaSimpleTypeContent_t1606103299::get_offset_of_OwnerType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (XmlSchemaSimpleTypeList_t2170323082), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[4] = 
{
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemType_17(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_itemTypeName_18(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_validatedListItemType_19(),
	XmlSchemaSimpleTypeList_t2170323082::get_offset_of_validatedListItemSchemaType_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (XmlSchemaSimpleTypeRestriction_t1099506232), -1, sizeof(XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1795[18] = 
{
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_baseType_17(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_baseTypeName_18(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_facets_19(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_enumarationFacetValues_20(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_patternFacetValues_21(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_rexPatterns_22(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_lengthFacet_23(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_maxLengthFacet_24(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_minLengthFacet_25(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_fractionDigitsFacet_26(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_totalDigitsFacet_27(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_maxInclusiveFacet_28(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_maxExclusiveFacet_29(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_minInclusiveFacet_30(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_minExclusiveFacet_31(),
	XmlSchemaSimpleTypeRestriction_t1099506232::get_offset_of_fixedFacets_32(),
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_lengthStyle_33(),
	XmlSchemaSimpleTypeRestriction_t1099506232_StaticFields::get_offset_of_listFacets_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (XmlSchemaSimpleTypeUnion_t91327365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[4] = 
{
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_baseTypes_17(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_memberTypes_18(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_validatedTypes_19(),
	XmlSchemaSimpleTypeUnion_t91327365::get_offset_of_validatedSchemaTypes_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (XmlSchemaTotalDigitsFacet_t2939281405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (XmlSchemaType_t1795078578), -1, sizeof(XmlSchemaType_t1795078578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1798[12] = 
{
	XmlSchemaType_t1795078578::get_offset_of_final_16(),
	XmlSchemaType_t1795078578::get_offset_of_isMixed_17(),
	XmlSchemaType_t1795078578::get_offset_of_name_18(),
	XmlSchemaType_t1795078578::get_offset_of_recursed_19(),
	XmlSchemaType_t1795078578::get_offset_of_BaseSchemaTypeName_20(),
	XmlSchemaType_t1795078578::get_offset_of_BaseXmlSchemaTypeInternal_21(),
	XmlSchemaType_t1795078578::get_offset_of_DatatypeInternal_22(),
	XmlSchemaType_t1795078578::get_offset_of_resolvedDerivedBy_23(),
	XmlSchemaType_t1795078578::get_offset_of_finalResolved_24(),
	XmlSchemaType_t1795078578::get_offset_of_QNameInternal_25(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2E_26(),
	XmlSchemaType_t1795078578_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (XmlSchemaUnique_t403169513), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
