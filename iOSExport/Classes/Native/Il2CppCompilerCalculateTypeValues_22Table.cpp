﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Amazon.Runtime.IRequestContext
struct IRequestContext_t1620878341;
// Amazon.Runtime.IResponseContext
struct IResponseContext_t898521739;
// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct AbstractAWSSigner_t2114314031;
// Amazon.Runtime.Internal.IRequest
struct IRequest_t2400804350;
// Amazon.Runtime.Internal.Util.RequestMetrics
struct RequestMetrics_t218029284;
// Amazon.Runtime.IClientConfig
struct IClientConfig_t4078933728;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest>
struct IMarshaller_2_t3817076711;
// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct ResponseUnmarshaller_t3934041557;
// Amazon.Runtime.ImmutableCredentials
struct ImmutableCredentials_t282353664;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// Amazon.Runtime.IAsyncResponseContext
struct IAsyncResponseContext_t340181993;
// Amazon.Runtime.IAsyncRequestContext
struct IAsyncRequestContext_t1452803323;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>>
struct Dictionary_2_t3557729749;
// System.String
struct String_t;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// Amazon.Runtime.RequestEventHandler
struct RequestEventHandler_t2213783891;
// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs>
struct EventHandler_1_t1230945235;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t309261261;
// System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.Internal.RetryCapacity>
struct Dictionary_2_t414480860;
// System.Threading.ReaderWriterLockSlim
struct ReaderWriterLockSlim_t3961242320;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t1943082916;
// Amazon.Runtime.Internal.ParameterCollection
struct ParameterCollection_t311980373;
// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;
// System.Collections.Generic.IList`1<Amazon.Runtime.Internal.IRuntimePipelineCustomizer>
struct IList_1_t1763753295;
// System.Func`2<System.IO.Stream,System.Boolean>
struct Func_2_t4109217329;
// System.Uri
struct Uri_t19570940;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.IO.Stream
struct Stream_t3255436806;
// Amazon.RegionEndpoint
struct RegionEndpoint_t661522805;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct AWS4SigningResult_t430803065;
// Amazon.Util.Internal.ElementInformation
struct ElementInformation_t3988909444;
// Amazon.Util.LoggingConfig
struct LoggingConfig_t4162907495;
// Amazon.Util.ProxyConfig
struct ProxyConfig_t2693849256;
// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement>
struct IDictionary_2_t467683733;
// System.Type
struct Type_t;
// Amazon.Util.Internal.ITypeInfo[]
struct ITypeInfoU5BU5D_t39398432;
// System.Func`2<System.Reflection.MethodInfo,System.Boolean>
struct Func_2_t499475658;
// System.Collections.Generic.RBTree
struct RBTree_t1544615604;
// System.Collections.Generic.SortedDictionary`2/NodeHelper<System.String,Amazon.Runtime.ParameterValue>
struct NodeHelper_t2648599067;
// Amazon.Runtime.Internal.RuntimePipeline
struct RuntimePipeline_t3355992556;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.Runtime.PreRequestEventHandler
struct PreRequestEventHandler_t345425304;
// Amazon.Runtime.ResponseEventHandler
struct ResponseEventHandler_t3870676125;
// Amazon.Runtime.ExceptionEventHandler
struct ExceptionEventHandler_t3236465969;
// System.Collections.Generic.IDictionary`2<System.Type,System.Type>
struct IDictionary_2_t1240244544;
// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel>
struct IDictionary_2_t1569248696;
// System.Collections.Generic.IDictionary`2<System.Type,System.Object>
struct IDictionary_2_t2625890613;
// System.EventHandler`1<Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs>
struct EventHandler_1_t198474825;
// Amazon.Runtime.ParameterValue
struct ParameterValue_t2426009594;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;
// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>
struct Action_4_t897279368;
// Amazon.Runtime.Internal.RuntimeAsyncResult
struct RuntimeAsyncResult_t4279356013;
// System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node>
struct Stack_1_t3586864480;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Void
struct Void_t1841601450;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.Internal.ParametersDictionaryFacade
struct ParametersDictionaryFacade_t4039108328;
// System.Collections.Generic.IDictionary`2<System.String,System.TimeSpan>
struct IDictionary_2_t3344121632;
// Amazon.Runtime.StringListParameterValue
struct StringListParameterValue_t1474210831;
// Amazon.Runtime.ResponseMetadata
struct ResponseMetadata_t527027456;
// Amazon.Runtime.ResponseEventArgs
struct ResponseEventArgs_t4056063878;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// Amazon.Runtime.PreRequestEventArgs
struct PreRequestEventArgs_t776850383;
// Amazon.Runtime.ExceptionEventArgs
struct ExceptionEventArgs_t154100464;
// Amazon.Runtime.RequestEventArgs
struct RequestEventArgs_t434225820;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef PARAMETERVALUE_T2426009594_H
#define PARAMETERVALUE_T2426009594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ParameterValue
struct  ParameterValue_t2426009594  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERVALUE_T2426009594_H
#ifndef EXECUTIONCONTEXT_T2943675185_H
#define EXECUTIONCONTEXT_T2943675185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ExecutionContext
struct  ExecutionContext_t2943675185  : public RuntimeObject
{
public:
	// Amazon.Runtime.IRequestContext Amazon.Runtime.Internal.ExecutionContext::<RequestContext>k__BackingField
	RuntimeObject* ___U3CRequestContextU3Ek__BackingField_0;
	// Amazon.Runtime.IResponseContext Amazon.Runtime.Internal.ExecutionContext::<ResponseContext>k__BackingField
	RuntimeObject* ___U3CResponseContextU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CRequestContextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ExecutionContext_t2943675185, ___U3CRequestContextU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CRequestContextU3Ek__BackingField_0() const { return ___U3CRequestContextU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CRequestContextU3Ek__BackingField_0() { return &___U3CRequestContextU3Ek__BackingField_0; }
	inline void set_U3CRequestContextU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CRequestContextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestContextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CResponseContextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExecutionContext_t2943675185, ___U3CResponseContextU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CResponseContextU3Ek__BackingField_1() const { return ___U3CResponseContextU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CResponseContextU3Ek__BackingField_1() { return &___U3CResponseContextU3Ek__BackingField_1; }
	inline void set_U3CResponseContextU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CResponseContextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseContextU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTIONCONTEXT_T2943675185_H
#ifndef REQUESTCONTEXT_T2912551040_H
#define REQUESTCONTEXT_T2912551040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RequestContext
struct  RequestContext_t2912551040  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.Internal.RequestContext::clientSigner
	AbstractAWSSigner_t2114314031 * ___clientSigner_0;
	// Amazon.Runtime.Internal.IRequest Amazon.Runtime.Internal.RequestContext::<Request>k__BackingField
	RuntimeObject* ___U3CRequestU3Ek__BackingField_1;
	// Amazon.Runtime.Internal.Util.RequestMetrics Amazon.Runtime.Internal.RequestContext::<Metrics>k__BackingField
	RequestMetrics_t218029284 * ___U3CMetricsU3Ek__BackingField_2;
	// Amazon.Runtime.IClientConfig Amazon.Runtime.Internal.RequestContext::<ClientConfig>k__BackingField
	RuntimeObject* ___U3CClientConfigU3Ek__BackingField_3;
	// System.Int32 Amazon.Runtime.Internal.RequestContext::<Retries>k__BackingField
	int32_t ___U3CRetriesU3Ek__BackingField_4;
	// System.Boolean Amazon.Runtime.Internal.RequestContext::<IsSigned>k__BackingField
	bool ___U3CIsSignedU3Ek__BackingField_5;
	// System.Boolean Amazon.Runtime.Internal.RequestContext::<IsAsync>k__BackingField
	bool ___U3CIsAsyncU3Ek__BackingField_6;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.RequestContext::<OriginalRequest>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3COriginalRequestU3Ek__BackingField_7;
	// Amazon.Runtime.Internal.Transform.IMarshaller`2<Amazon.Runtime.Internal.IRequest,Amazon.Runtime.AmazonWebServiceRequest> Amazon.Runtime.Internal.RequestContext::<Marshaller>k__BackingField
	RuntimeObject* ___U3CMarshallerU3Ek__BackingField_8;
	// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller Amazon.Runtime.Internal.RequestContext::<Unmarshaller>k__BackingField
	ResponseUnmarshaller_t3934041557 * ___U3CUnmarshallerU3Ek__BackingField_9;
	// Amazon.Runtime.ImmutableCredentials Amazon.Runtime.Internal.RequestContext::<ImmutableCredentials>k__BackingField
	ImmutableCredentials_t282353664 * ___U3CImmutableCredentialsU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_clientSigner_0() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___clientSigner_0)); }
	inline AbstractAWSSigner_t2114314031 * get_clientSigner_0() const { return ___clientSigner_0; }
	inline AbstractAWSSigner_t2114314031 ** get_address_of_clientSigner_0() { return &___clientSigner_0; }
	inline void set_clientSigner_0(AbstractAWSSigner_t2114314031 * value)
	{
		___clientSigner_0 = value;
		Il2CppCodeGenWriteBarrier((&___clientSigner_0), value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CRequestU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CRequestU3Ek__BackingField_1() const { return ___U3CRequestU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CRequestU3Ek__BackingField_1() { return &___U3CRequestU3Ek__BackingField_1; }
	inline void set_U3CRequestU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CRequestU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CMetricsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CMetricsU3Ek__BackingField_2)); }
	inline RequestMetrics_t218029284 * get_U3CMetricsU3Ek__BackingField_2() const { return ___U3CMetricsU3Ek__BackingField_2; }
	inline RequestMetrics_t218029284 ** get_address_of_U3CMetricsU3Ek__BackingField_2() { return &___U3CMetricsU3Ek__BackingField_2; }
	inline void set_U3CMetricsU3Ek__BackingField_2(RequestMetrics_t218029284 * value)
	{
		___U3CMetricsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMetricsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CClientConfigU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CClientConfigU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CClientConfigU3Ek__BackingField_3() const { return ___U3CClientConfigU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CClientConfigU3Ek__BackingField_3() { return &___U3CClientConfigU3Ek__BackingField_3; }
	inline void set_U3CClientConfigU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CClientConfigU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientConfigU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CRetriesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CRetriesU3Ek__BackingField_4)); }
	inline int32_t get_U3CRetriesU3Ek__BackingField_4() const { return ___U3CRetriesU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CRetriesU3Ek__BackingField_4() { return &___U3CRetriesU3Ek__BackingField_4; }
	inline void set_U3CRetriesU3Ek__BackingField_4(int32_t value)
	{
		___U3CRetriesU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIsSignedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CIsSignedU3Ek__BackingField_5)); }
	inline bool get_U3CIsSignedU3Ek__BackingField_5() const { return ___U3CIsSignedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsSignedU3Ek__BackingField_5() { return &___U3CIsSignedU3Ek__BackingField_5; }
	inline void set_U3CIsSignedU3Ek__BackingField_5(bool value)
	{
		___U3CIsSignedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsAsyncU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CIsAsyncU3Ek__BackingField_6)); }
	inline bool get_U3CIsAsyncU3Ek__BackingField_6() const { return ___U3CIsAsyncU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsAsyncU3Ek__BackingField_6() { return &___U3CIsAsyncU3Ek__BackingField_6; }
	inline void set_U3CIsAsyncU3Ek__BackingField_6(bool value)
	{
		___U3CIsAsyncU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3COriginalRequestU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3COriginalRequestU3Ek__BackingField_7)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3COriginalRequestU3Ek__BackingField_7() const { return ___U3COriginalRequestU3Ek__BackingField_7; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3COriginalRequestU3Ek__BackingField_7() { return &___U3COriginalRequestU3Ek__BackingField_7; }
	inline void set_U3COriginalRequestU3Ek__BackingField_7(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3COriginalRequestU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3COriginalRequestU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CMarshallerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CMarshallerU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CMarshallerU3Ek__BackingField_8() const { return ___U3CMarshallerU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CMarshallerU3Ek__BackingField_8() { return &___U3CMarshallerU3Ek__BackingField_8; }
	inline void set_U3CMarshallerU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CMarshallerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMarshallerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CUnmarshallerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CUnmarshallerU3Ek__BackingField_9)); }
	inline ResponseUnmarshaller_t3934041557 * get_U3CUnmarshallerU3Ek__BackingField_9() const { return ___U3CUnmarshallerU3Ek__BackingField_9; }
	inline ResponseUnmarshaller_t3934041557 ** get_address_of_U3CUnmarshallerU3Ek__BackingField_9() { return &___U3CUnmarshallerU3Ek__BackingField_9; }
	inline void set_U3CUnmarshallerU3Ek__BackingField_9(ResponseUnmarshaller_t3934041557 * value)
	{
		___U3CUnmarshallerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnmarshallerU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CImmutableCredentialsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RequestContext_t2912551040, ___U3CImmutableCredentialsU3Ek__BackingField_10)); }
	inline ImmutableCredentials_t282353664 * get_U3CImmutableCredentialsU3Ek__BackingField_10() const { return ___U3CImmutableCredentialsU3Ek__BackingField_10; }
	inline ImmutableCredentials_t282353664 ** get_address_of_U3CImmutableCredentialsU3Ek__BackingField_10() { return &___U3CImmutableCredentialsU3Ek__BackingField_10; }
	inline void set_U3CImmutableCredentialsU3Ek__BackingField_10(ImmutableCredentials_t282353664 * value)
	{
		___U3CImmutableCredentialsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImmutableCredentialsU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTCONTEXT_T2912551040_H
#ifndef RESPONSECONTEXT_T3850805926_H
#define RESPONSECONTEXT_T3850805926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ResponseContext
struct  ResponseContext_t3850805926  : public RuntimeObject
{
public:
	// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.ResponseContext::<Response>k__BackingField
	AmazonWebServiceResponse_t529043356 * ___U3CResponseU3Ek__BackingField_0;
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.ResponseContext::<HttpResponse>k__BackingField
	RuntimeObject* ___U3CHttpResponseU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResponseContext_t3850805926, ___U3CResponseU3Ek__BackingField_0)); }
	inline AmazonWebServiceResponse_t529043356 * get_U3CResponseU3Ek__BackingField_0() const { return ___U3CResponseU3Ek__BackingField_0; }
	inline AmazonWebServiceResponse_t529043356 ** get_address_of_U3CResponseU3Ek__BackingField_0() { return &___U3CResponseU3Ek__BackingField_0; }
	inline void set_U3CResponseU3Ek__BackingField_0(AmazonWebServiceResponse_t529043356 * value)
	{
		___U3CResponseU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CHttpResponseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResponseContext_t3850805926, ___U3CHttpResponseU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CHttpResponseU3Ek__BackingField_1() const { return ___U3CHttpResponseU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CHttpResponseU3Ek__BackingField_1() { return &___U3CHttpResponseU3Ek__BackingField_1; }
	inline void set_U3CHttpResponseU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CHttpResponseU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHttpResponseU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSECONTEXT_T3850805926_H
#ifndef ASYNCEXECUTIONCONTEXT_T3009477291_H
#define ASYNCEXECUTIONCONTEXT_T3009477291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.AsyncExecutionContext
struct  AsyncExecutionContext_t3009477291  : public RuntimeObject
{
public:
	// Amazon.Runtime.IAsyncResponseContext Amazon.Runtime.Internal.AsyncExecutionContext::<ResponseContext>k__BackingField
	RuntimeObject* ___U3CResponseContextU3Ek__BackingField_0;
	// Amazon.Runtime.IAsyncRequestContext Amazon.Runtime.Internal.AsyncExecutionContext::<RequestContext>k__BackingField
	RuntimeObject* ___U3CRequestContextU3Ek__BackingField_1;
	// System.Object Amazon.Runtime.Internal.AsyncExecutionContext::<RuntimeState>k__BackingField
	RuntimeObject * ___U3CRuntimeStateU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CResponseContextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncExecutionContext_t3009477291, ___U3CResponseContextU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CResponseContextU3Ek__BackingField_0() const { return ___U3CResponseContextU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CResponseContextU3Ek__BackingField_0() { return &___U3CResponseContextU3Ek__BackingField_0; }
	inline void set_U3CResponseContextU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CResponseContextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseContextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRequestContextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AsyncExecutionContext_t3009477291, ___U3CRequestContextU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CRequestContextU3Ek__BackingField_1() const { return ___U3CRequestContextU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CRequestContextU3Ek__BackingField_1() { return &___U3CRequestContextU3Ek__BackingField_1; }
	inline void set_U3CRequestContextU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CRequestContextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestContextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRuntimeStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AsyncExecutionContext_t3009477291, ___U3CRuntimeStateU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CRuntimeStateU3Ek__BackingField_2() const { return ___U3CRuntimeStateU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CRuntimeStateU3Ek__BackingField_2() { return &___U3CRuntimeStateU3Ek__BackingField_2; }
	inline void set_U3CRuntimeStateU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CRuntimeStateU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRuntimeStateU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCEXECUTIONCONTEXT_T3009477291_H
#ifndef CONSTANTCLASS_T4000559886_H
#define CONSTANTCLASS_T4000559886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ConstantClass
struct  ConstantClass_t4000559886  : public RuntimeObject
{
public:
	// System.String Amazon.Runtime.ConstantClass::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConstantClass_t4000559886, ___U3CValueU3Ek__BackingField_2)); }
	inline String_t* get_U3CValueU3Ek__BackingField_2() const { return ___U3CValueU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_2() { return &___U3CValueU3Ek__BackingField_2; }
	inline void set_U3CValueU3Ek__BackingField_2(String_t* value)
	{
		___U3CValueU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_2), value);
	}
};

struct ConstantClass_t4000559886_StaticFields
{
public:
	// System.Object Amazon.Runtime.ConstantClass::staticFieldsLock
	RuntimeObject * ___staticFieldsLock_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.ConstantClass>> Amazon.Runtime.ConstantClass::staticFields
	Dictionary_2_t3557729749 * ___staticFields_1;

public:
	inline static int32_t get_offset_of_staticFieldsLock_0() { return static_cast<int32_t>(offsetof(ConstantClass_t4000559886_StaticFields, ___staticFieldsLock_0)); }
	inline RuntimeObject * get_staticFieldsLock_0() const { return ___staticFieldsLock_0; }
	inline RuntimeObject ** get_address_of_staticFieldsLock_0() { return &___staticFieldsLock_0; }
	inline void set_staticFieldsLock_0(RuntimeObject * value)
	{
		___staticFieldsLock_0 = value;
		Il2CppCodeGenWriteBarrier((&___staticFieldsLock_0), value);
	}

	inline static int32_t get_offset_of_staticFields_1() { return static_cast<int32_t>(offsetof(ConstantClass_t4000559886_StaticFields, ___staticFields_1)); }
	inline Dictionary_2_t3557729749 * get_staticFields_1() const { return ___staticFields_1; }
	inline Dictionary_2_t3557729749 ** get_address_of_staticFields_1() { return &___staticFields_1; }
	inline void set_staticFields_1(Dictionary_2_t3557729749 * value)
	{
		___staticFields_1 = value;
		Il2CppCodeGenWriteBarrier((&___staticFields_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTCLASS_T4000559886_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef AMAZONWEBSERVICEREQUEST_T3384026212_H
#define AMAZONWEBSERVICEREQUEST_T3384026212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonWebServiceRequest
struct  AmazonWebServiceRequest_t3384026212  : public RuntimeObject
{
public:
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonWebServiceRequest::mBeforeRequestEvent
	RequestEventHandler_t2213783891 * ___mBeforeRequestEvent_0;
	// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.StreamUploadProgressCallback>k__BackingField
	EventHandler_1_t1230945235 * ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Amazon.Runtime.AmazonWebServiceRequest::requestState
	Dictionary_2_t309261261 * ___requestState_2;
	// System.Boolean Amazon.Runtime.AmazonWebServiceRequest::<Amazon.Runtime.Internal.IAmazonWebServiceRequest.UseSigV4>k__BackingField
	bool ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_mBeforeRequestEvent_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___mBeforeRequestEvent_0)); }
	inline RequestEventHandler_t2213783891 * get_mBeforeRequestEvent_0() const { return ___mBeforeRequestEvent_0; }
	inline RequestEventHandler_t2213783891 ** get_address_of_mBeforeRequestEvent_0() { return &___mBeforeRequestEvent_0; }
	inline void set_mBeforeRequestEvent_0(RequestEventHandler_t2213783891 * value)
	{
		___mBeforeRequestEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBeforeRequestEvent_0), value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1)); }
	inline EventHandler_1_t1230945235 * get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline EventHandler_1_t1230945235 ** get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1(EventHandler_1_t1230945235 * value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_requestState_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___requestState_2)); }
	inline Dictionary_2_t309261261 * get_requestState_2() const { return ___requestState_2; }
	inline Dictionary_2_t309261261 ** get_address_of_requestState_2() { return &___requestState_2; }
	inline void set_requestState_2(Dictionary_2_t309261261 * value)
	{
		___requestState_2 = value;
		Il2CppCodeGenWriteBarrier((&___requestState_2), value);
	}

	inline static int32_t get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AmazonWebServiceRequest_t3384026212, ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3)); }
	inline bool get_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3() const { return ___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3; }
	inline bool* get_address_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3() { return &___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3; }
	inline void set_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3(bool value)
	{
		___U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONWEBSERVICEREQUEST_T3384026212_H
#ifndef EVENTARGS_T3289624707_H
#define EVENTARGS_T3289624707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3289624707  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3289624707_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3289624707 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3289624707_StaticFields, ___Empty_0)); }
	inline EventArgs_t3289624707 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3289624707 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3289624707 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3289624707_H
#ifndef STREAMREADTRACKER_T1958363340_H
#define STREAMREADTRACKER_T1958363340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.StreamReadTracker
struct  StreamReadTracker_t1958363340  : public RuntimeObject
{
public:
	// System.Object Amazon.Runtime.Internal.StreamReadTracker::sender
	RuntimeObject * ___sender_0;
	// System.EventHandler`1<Amazon.Runtime.StreamTransferProgressArgs> Amazon.Runtime.Internal.StreamReadTracker::callback
	EventHandler_1_t1230945235 * ___callback_1;
	// System.Int64 Amazon.Runtime.Internal.StreamReadTracker::contentLength
	int64_t ___contentLength_2;
	// System.Int64 Amazon.Runtime.Internal.StreamReadTracker::totalBytesRead
	int64_t ___totalBytesRead_3;
	// System.Int64 Amazon.Runtime.Internal.StreamReadTracker::totalIncrementTransferred
	int64_t ___totalIncrementTransferred_4;
	// System.Int64 Amazon.Runtime.Internal.StreamReadTracker::progressUpdateInterval
	int64_t ___progressUpdateInterval_5;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___sender_0)); }
	inline RuntimeObject * get_sender_0() const { return ___sender_0; }
	inline RuntimeObject ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(RuntimeObject * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier((&___sender_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___callback_1)); }
	inline EventHandler_1_t1230945235 * get_callback_1() const { return ___callback_1; }
	inline EventHandler_1_t1230945235 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(EventHandler_1_t1230945235 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}

	inline static int32_t get_offset_of_contentLength_2() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___contentLength_2)); }
	inline int64_t get_contentLength_2() const { return ___contentLength_2; }
	inline int64_t* get_address_of_contentLength_2() { return &___contentLength_2; }
	inline void set_contentLength_2(int64_t value)
	{
		___contentLength_2 = value;
	}

	inline static int32_t get_offset_of_totalBytesRead_3() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___totalBytesRead_3)); }
	inline int64_t get_totalBytesRead_3() const { return ___totalBytesRead_3; }
	inline int64_t* get_address_of_totalBytesRead_3() { return &___totalBytesRead_3; }
	inline void set_totalBytesRead_3(int64_t value)
	{
		___totalBytesRead_3 = value;
	}

	inline static int32_t get_offset_of_totalIncrementTransferred_4() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___totalIncrementTransferred_4)); }
	inline int64_t get_totalIncrementTransferred_4() const { return ___totalIncrementTransferred_4; }
	inline int64_t* get_address_of_totalIncrementTransferred_4() { return &___totalIncrementTransferred_4; }
	inline void set_totalIncrementTransferred_4(int64_t value)
	{
		___totalIncrementTransferred_4 = value;
	}

	inline static int32_t get_offset_of_progressUpdateInterval_5() { return static_cast<int32_t>(offsetof(StreamReadTracker_t1958363340, ___progressUpdateInterval_5)); }
	inline int64_t get_progressUpdateInterval_5() const { return ___progressUpdateInterval_5; }
	inline int64_t* get_address_of_progressUpdateInterval_5() { return &___progressUpdateInterval_5; }
	inline void set_progressUpdateInterval_5(int64_t value)
	{
		___progressUpdateInterval_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMREADTRACKER_T1958363340_H
#ifndef CAPACITYMANAGER_T2181902141_H
#define CAPACITYMANAGER_T2181902141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.CapacityManager
struct  CapacityManager_t2181902141  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.CapacityManager::_disposed
	bool ____disposed_0;
	// System.Threading.ReaderWriterLockSlim Amazon.Runtime.Internal.CapacityManager::_rwlock
	ReaderWriterLockSlim_t3961242320 * ____rwlock_2;
	// System.Int32 Amazon.Runtime.Internal.CapacityManager::THROTTLE_RETRY_REQUEST_COST
	int32_t ___THROTTLE_RETRY_REQUEST_COST_3;
	// System.Int32 Amazon.Runtime.Internal.CapacityManager::THROTTLED_RETRIES
	int32_t ___THROTTLED_RETRIES_4;
	// System.Int32 Amazon.Runtime.Internal.CapacityManager::THROTTLE_REQUEST_COST
	int32_t ___THROTTLE_REQUEST_COST_5;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__rwlock_2() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ____rwlock_2)); }
	inline ReaderWriterLockSlim_t3961242320 * get__rwlock_2() const { return ____rwlock_2; }
	inline ReaderWriterLockSlim_t3961242320 ** get_address_of__rwlock_2() { return &____rwlock_2; }
	inline void set__rwlock_2(ReaderWriterLockSlim_t3961242320 * value)
	{
		____rwlock_2 = value;
		Il2CppCodeGenWriteBarrier((&____rwlock_2), value);
	}

	inline static int32_t get_offset_of_THROTTLE_RETRY_REQUEST_COST_3() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ___THROTTLE_RETRY_REQUEST_COST_3)); }
	inline int32_t get_THROTTLE_RETRY_REQUEST_COST_3() const { return ___THROTTLE_RETRY_REQUEST_COST_3; }
	inline int32_t* get_address_of_THROTTLE_RETRY_REQUEST_COST_3() { return &___THROTTLE_RETRY_REQUEST_COST_3; }
	inline void set_THROTTLE_RETRY_REQUEST_COST_3(int32_t value)
	{
		___THROTTLE_RETRY_REQUEST_COST_3 = value;
	}

	inline static int32_t get_offset_of_THROTTLED_RETRIES_4() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ___THROTTLED_RETRIES_4)); }
	inline int32_t get_THROTTLED_RETRIES_4() const { return ___THROTTLED_RETRIES_4; }
	inline int32_t* get_address_of_THROTTLED_RETRIES_4() { return &___THROTTLED_RETRIES_4; }
	inline void set_THROTTLED_RETRIES_4(int32_t value)
	{
		___THROTTLED_RETRIES_4 = value;
	}

	inline static int32_t get_offset_of_THROTTLE_REQUEST_COST_5() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141, ___THROTTLE_REQUEST_COST_5)); }
	inline int32_t get_THROTTLE_REQUEST_COST_5() const { return ___THROTTLE_REQUEST_COST_5; }
	inline int32_t* get_address_of_THROTTLE_REQUEST_COST_5() { return &___THROTTLE_REQUEST_COST_5; }
	inline void set_THROTTLE_REQUEST_COST_5(int32_t value)
	{
		___THROTTLE_REQUEST_COST_5 = value;
	}
};

struct CapacityManager_t2181902141_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.Runtime.Internal.RetryCapacity> Amazon.Runtime.Internal.CapacityManager::_serviceUrlToCapacityMap
	Dictionary_2_t414480860 * ____serviceUrlToCapacityMap_1;

public:
	inline static int32_t get_offset_of__serviceUrlToCapacityMap_1() { return static_cast<int32_t>(offsetof(CapacityManager_t2181902141_StaticFields, ____serviceUrlToCapacityMap_1)); }
	inline Dictionary_2_t414480860 * get__serviceUrlToCapacityMap_1() const { return ____serviceUrlToCapacityMap_1; }
	inline Dictionary_2_t414480860 ** get_address_of__serviceUrlToCapacityMap_1() { return &____serviceUrlToCapacityMap_1; }
	inline void set__serviceUrlToCapacityMap_1(Dictionary_2_t414480860 * value)
	{
		____serviceUrlToCapacityMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____serviceUrlToCapacityMap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPACITYMANAGER_T2181902141_H
#ifndef RESPONSEMETADATA_T527027456_H
#define RESPONSEMETADATA_T527027456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ResponseMetadata
struct  ResponseMetadata_t527027456  : public RuntimeObject
{
public:
	// System.String Amazon.Runtime.ResponseMetadata::requestIdField
	String_t* ___requestIdField_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.ResponseMetadata::_metadata
	RuntimeObject* ____metadata_1;

public:
	inline static int32_t get_offset_of_requestIdField_0() { return static_cast<int32_t>(offsetof(ResponseMetadata_t527027456, ___requestIdField_0)); }
	inline String_t* get_requestIdField_0() const { return ___requestIdField_0; }
	inline String_t** get_address_of_requestIdField_0() { return &___requestIdField_0; }
	inline void set_requestIdField_0(String_t* value)
	{
		___requestIdField_0 = value;
		Il2CppCodeGenWriteBarrier((&___requestIdField_0), value);
	}

	inline static int32_t get_offset_of__metadata_1() { return static_cast<int32_t>(offsetof(ResponseMetadata_t527027456, ____metadata_1)); }
	inline RuntimeObject* get__metadata_1() const { return ____metadata_1; }
	inline RuntimeObject** get_address_of__metadata_1() { return &____metadata_1; }
	inline void set__metadata_1(RuntimeObject* value)
	{
		____metadata_1 = value;
		Il2CppCodeGenWriteBarrier((&____metadata_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEMETADATA_T527027456_H
#ifndef ASYNCOPTIONS_T558351272_H
#define ASYNCOPTIONS_T558351272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AsyncOptions
struct  AsyncOptions_t558351272  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.AsyncOptions::<ExecuteCallbackOnMainThread>k__BackingField
	bool ___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0;
	// System.Object Amazon.Runtime.AsyncOptions::<State>k__BackingField
	RuntimeObject * ___U3CStateU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AsyncOptions_t558351272, ___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0)); }
	inline bool get_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0() const { return ___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0() { return &___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0; }
	inline void set_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0(bool value)
	{
		___U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AsyncOptions_t558351272, ___U3CStateU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CStateU3Ek__BackingField_1() const { return ___U3CStateU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CStateU3Ek__BackingField_1() { return &___U3CStateU3Ek__BackingField_1; }
	inline void set_U3CStateU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CStateU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCOPTIONS_T558351272_H
#ifndef IMMUTABLECREDENTIALS_T282353664_H
#define IMMUTABLECREDENTIALS_T282353664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ImmutableCredentials
struct  ImmutableCredentials_t282353664  : public RuntimeObject
{
public:
	// System.String Amazon.Runtime.ImmutableCredentials::<AccessKey>k__BackingField
	String_t* ___U3CAccessKeyU3Ek__BackingField_0;
	// System.String Amazon.Runtime.ImmutableCredentials::<SecretKey>k__BackingField
	String_t* ___U3CSecretKeyU3Ek__BackingField_1;
	// System.String Amazon.Runtime.ImmutableCredentials::<Token>k__BackingField
	String_t* ___U3CTokenU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAccessKeyU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ImmutableCredentials_t282353664, ___U3CAccessKeyU3Ek__BackingField_0)); }
	inline String_t* get_U3CAccessKeyU3Ek__BackingField_0() const { return ___U3CAccessKeyU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAccessKeyU3Ek__BackingField_0() { return &___U3CAccessKeyU3Ek__BackingField_0; }
	inline void set_U3CAccessKeyU3Ek__BackingField_0(String_t* value)
	{
		___U3CAccessKeyU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAccessKeyU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSecretKeyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ImmutableCredentials_t282353664, ___U3CSecretKeyU3Ek__BackingField_1)); }
	inline String_t* get_U3CSecretKeyU3Ek__BackingField_1() const { return ___U3CSecretKeyU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSecretKeyU3Ek__BackingField_1() { return &___U3CSecretKeyU3Ek__BackingField_1; }
	inline void set_U3CSecretKeyU3Ek__BackingField_1(String_t* value)
	{
		___U3CSecretKeyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSecretKeyU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ImmutableCredentials_t282353664, ___U3CTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CTokenU3Ek__BackingField_2() const { return ___U3CTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTokenU3Ek__BackingField_2() { return &___U3CTokenU3Ek__BackingField_2; }
	inline void set_U3CTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLECREDENTIALS_T282353664_H
#ifndef AWSCREDENTIALS_T3583921007_H
#define AWSCREDENTIALS_T3583921007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AWSCredentials
struct  AWSCredentials_t3583921007  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AWSCREDENTIALS_T3583921007_H
#ifndef PARAMETERSDICTIONARYFACADE_T4039108328_H
#define PARAMETERSDICTIONARYFACADE_T4039108328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ParametersDictionaryFacade
struct  ParametersDictionaryFacade_t4039108328  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.ParameterCollection Amazon.Runtime.Internal.ParametersDictionaryFacade::_parameterCollection
	ParameterCollection_t311980373 * ____parameterCollection_0;

public:
	inline static int32_t get_offset_of__parameterCollection_0() { return static_cast<int32_t>(offsetof(ParametersDictionaryFacade_t4039108328, ____parameterCollection_0)); }
	inline ParameterCollection_t311980373 * get__parameterCollection_0() const { return ____parameterCollection_0; }
	inline ParameterCollection_t311980373 ** get_address_of__parameterCollection_0() { return &____parameterCollection_0; }
	inline void set__parameterCollection_0(ParameterCollection_t311980373 * value)
	{
		____parameterCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&____parameterCollection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERSDICTIONARYFACADE_T4039108328_H
#ifndef RUNTIMEPIPELINECUSTOMIZERREGISTRY_T1556540342_H
#define RUNTIMEPIPELINECUSTOMIZERREGISTRY_T1556540342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry
struct  RuntimePipelineCustomizerRegistry_t1556540342  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::_logger
	Logger_t2262497814 * ____logger_1;
	// System.Threading.ReaderWriterLockSlim Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::_rwlock
	ReaderWriterLockSlim_t3961242320 * ____rwlock_2;
	// System.Collections.Generic.IList`1<Amazon.Runtime.Internal.IRuntimePipelineCustomizer> Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::_customizers
	RuntimeObject* ____customizers_3;

public:
	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(RuntimePipelineCustomizerRegistry_t1556540342, ____logger_1)); }
	inline Logger_t2262497814 * get__logger_1() const { return ____logger_1; }
	inline Logger_t2262497814 ** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(Logger_t2262497814 * value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier((&____logger_1), value);
	}

	inline static int32_t get_offset_of__rwlock_2() { return static_cast<int32_t>(offsetof(RuntimePipelineCustomizerRegistry_t1556540342, ____rwlock_2)); }
	inline ReaderWriterLockSlim_t3961242320 * get__rwlock_2() const { return ____rwlock_2; }
	inline ReaderWriterLockSlim_t3961242320 ** get_address_of__rwlock_2() { return &____rwlock_2; }
	inline void set__rwlock_2(ReaderWriterLockSlim_t3961242320 * value)
	{
		____rwlock_2 = value;
		Il2CppCodeGenWriteBarrier((&____rwlock_2), value);
	}

	inline static int32_t get_offset_of__customizers_3() { return static_cast<int32_t>(offsetof(RuntimePipelineCustomizerRegistry_t1556540342, ____customizers_3)); }
	inline RuntimeObject* get__customizers_3() const { return ____customizers_3; }
	inline RuntimeObject** get_address_of__customizers_3() { return &____customizers_3; }
	inline void set__customizers_3(RuntimeObject* value)
	{
		____customizers_3 = value;
		Il2CppCodeGenWriteBarrier((&____customizers_3), value);
	}
};

struct RuntimePipelineCustomizerRegistry_t1556540342_StaticFields
{
public:
	// Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry Amazon.Runtime.Internal.RuntimePipelineCustomizerRegistry::<Instance>k__BackingField
	RuntimePipelineCustomizerRegistry_t1556540342 * ___U3CInstanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RuntimePipelineCustomizerRegistry_t1556540342_StaticFields, ___U3CInstanceU3Ek__BackingField_0)); }
	inline RuntimePipelineCustomizerRegistry_t1556540342 * get_U3CInstanceU3Ek__BackingField_0() const { return ___U3CInstanceU3Ek__BackingField_0; }
	inline RuntimePipelineCustomizerRegistry_t1556540342 ** get_address_of_U3CInstanceU3Ek__BackingField_0() { return &___U3CInstanceU3Ek__BackingField_0; }
	inline void set_U3CInstanceU3Ek__BackingField_0(RuntimePipelineCustomizerRegistry_t1556540342 * value)
	{
		___U3CInstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPIPELINECUSTOMIZERREGISTRY_T1556540342_H
#ifndef U3CU3EC_T12014549_H
#define U3CU3EC_T12014549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.DefaultRequest/<>c
struct  U3CU3Ec_t12014549  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t12014549_StaticFields
{
public:
	// Amazon.Runtime.Internal.DefaultRequest/<>c Amazon.Runtime.Internal.DefaultRequest/<>c::<>9
	U3CU3Ec_t12014549 * ___U3CU3E9_0;
	// System.Func`2<System.IO.Stream,System.Boolean> Amazon.Runtime.Internal.DefaultRequest/<>c::<>9__60_0
	Func_2_t4109217329 * ___U3CU3E9__60_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t12014549_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t12014549 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t12014549 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t12014549 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__60_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t12014549_StaticFields, ___U3CU3E9__60_0_1)); }
	inline Func_2_t4109217329 * get_U3CU3E9__60_0_1() const { return ___U3CU3E9__60_0_1; }
	inline Func_2_t4109217329 ** get_address_of_U3CU3E9__60_0_1() { return &___U3CU3E9__60_0_1; }
	inline void set_U3CU3E9__60_0_1(Func_2_t4109217329 * value)
	{
		___U3CU3E9__60_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__60_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T12014549_H
#ifndef RETRYCAPACITY_T2794668894_H
#define RETRYCAPACITY_T2794668894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RetryCapacity
struct  RetryCapacity_t2794668894  : public RuntimeObject
{
public:
	// System.Int32 Amazon.Runtime.Internal.RetryCapacity::_maxCapacity
	int32_t ____maxCapacity_0;
	// System.Int32 Amazon.Runtime.Internal.RetryCapacity::<AvailableCapacity>k__BackingField
	int32_t ___U3CAvailableCapacityU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of__maxCapacity_0() { return static_cast<int32_t>(offsetof(RetryCapacity_t2794668894, ____maxCapacity_0)); }
	inline int32_t get__maxCapacity_0() const { return ____maxCapacity_0; }
	inline int32_t* get_address_of__maxCapacity_0() { return &____maxCapacity_0; }
	inline void set__maxCapacity_0(int32_t value)
	{
		____maxCapacity_0 = value;
	}

	inline static int32_t get_offset_of_U3CAvailableCapacityU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RetryCapacity_t2794668894, ___U3CAvailableCapacityU3Ek__BackingField_1)); }
	inline int32_t get_U3CAvailableCapacityU3Ek__BackingField_1() const { return ___U3CAvailableCapacityU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CAvailableCapacityU3Ek__BackingField_1() { return &___U3CAvailableCapacityU3Ek__BackingField_1; }
	inline void set_U3CAvailableCapacityU3Ek__BackingField_1(int32_t value)
	{
		___U3CAvailableCapacityU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYCAPACITY_T2794668894_H
#ifndef DEFAULTREQUEST_T3080757440_H
#define DEFAULTREQUEST_T3080757440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.DefaultRequest
struct  DefaultRequest_t3080757440  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.ParameterCollection Amazon.Runtime.Internal.DefaultRequest::parametersCollection
	ParameterCollection_t311980373 * ___parametersCollection_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::parametersFacade
	RuntimeObject* ___parametersFacade_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::headers
	RuntimeObject* ___headers_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.Internal.DefaultRequest::subResources
	RuntimeObject* ___subResources_3;
	// System.Uri Amazon.Runtime.Internal.DefaultRequest::endpoint
	Uri_t19570940 * ___endpoint_4;
	// System.String Amazon.Runtime.Internal.DefaultRequest::resourcePath
	String_t* ___resourcePath_5;
	// System.String Amazon.Runtime.Internal.DefaultRequest::serviceName
	String_t* ___serviceName_6;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.DefaultRequest::originalRequest
	AmazonWebServiceRequest_t3384026212 * ___originalRequest_7;
	// System.Byte[] Amazon.Runtime.Internal.DefaultRequest::content
	ByteU5BU5D_t3397334013* ___content_8;
	// System.IO.Stream Amazon.Runtime.Internal.DefaultRequest::contentStream
	Stream_t3255436806 * ___contentStream_9;
	// System.String Amazon.Runtime.Internal.DefaultRequest::contentStreamHash
	String_t* ___contentStreamHash_10;
	// System.String Amazon.Runtime.Internal.DefaultRequest::httpMethod
	String_t* ___httpMethod_11;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::useQueryString
	bool ___useQueryString_12;
	// System.String Amazon.Runtime.Internal.DefaultRequest::requestName
	String_t* ___requestName_13;
	// Amazon.RegionEndpoint Amazon.Runtime.Internal.DefaultRequest::alternateRegion
	RegionEndpoint_t661522805 * ___alternateRegion_14;
	// System.Int64 Amazon.Runtime.Internal.DefaultRequest::originalStreamLength
	int64_t ___originalStreamLength_15;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<SetContentFromParameters>k__BackingField
	bool ___U3CSetContentFromParametersU3Ek__BackingField_16;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<Suppress404Exceptions>k__BackingField
	bool ___U3CSuppress404ExceptionsU3Ek__BackingField_17;
	// Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.DefaultRequest::<AWS4SignerResult>k__BackingField
	AWS4SigningResult_t430803065 * ___U3CAWS4SignerResultU3Ek__BackingField_18;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<UseChunkEncoding>k__BackingField
	bool ___U3CUseChunkEncodingU3Ek__BackingField_19;
	// System.String Amazon.Runtime.Internal.DefaultRequest::<CanonicalResourcePrefix>k__BackingField
	String_t* ___U3CCanonicalResourcePrefixU3Ek__BackingField_20;
	// System.Boolean Amazon.Runtime.Internal.DefaultRequest::<UseSigV4>k__BackingField
	bool ___U3CUseSigV4U3Ek__BackingField_21;
	// System.String Amazon.Runtime.Internal.DefaultRequest::<AuthenticationRegion>k__BackingField
	String_t* ___U3CAuthenticationRegionU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_parametersCollection_0() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___parametersCollection_0)); }
	inline ParameterCollection_t311980373 * get_parametersCollection_0() const { return ___parametersCollection_0; }
	inline ParameterCollection_t311980373 ** get_address_of_parametersCollection_0() { return &___parametersCollection_0; }
	inline void set_parametersCollection_0(ParameterCollection_t311980373 * value)
	{
		___parametersCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&___parametersCollection_0), value);
	}

	inline static int32_t get_offset_of_parametersFacade_1() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___parametersFacade_1)); }
	inline RuntimeObject* get_parametersFacade_1() const { return ___parametersFacade_1; }
	inline RuntimeObject** get_address_of_parametersFacade_1() { return &___parametersFacade_1; }
	inline void set_parametersFacade_1(RuntimeObject* value)
	{
		___parametersFacade_1 = value;
		Il2CppCodeGenWriteBarrier((&___parametersFacade_1), value);
	}

	inline static int32_t get_offset_of_headers_2() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___headers_2)); }
	inline RuntimeObject* get_headers_2() const { return ___headers_2; }
	inline RuntimeObject** get_address_of_headers_2() { return &___headers_2; }
	inline void set_headers_2(RuntimeObject* value)
	{
		___headers_2 = value;
		Il2CppCodeGenWriteBarrier((&___headers_2), value);
	}

	inline static int32_t get_offset_of_subResources_3() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___subResources_3)); }
	inline RuntimeObject* get_subResources_3() const { return ___subResources_3; }
	inline RuntimeObject** get_address_of_subResources_3() { return &___subResources_3; }
	inline void set_subResources_3(RuntimeObject* value)
	{
		___subResources_3 = value;
		Il2CppCodeGenWriteBarrier((&___subResources_3), value);
	}

	inline static int32_t get_offset_of_endpoint_4() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___endpoint_4)); }
	inline Uri_t19570940 * get_endpoint_4() const { return ___endpoint_4; }
	inline Uri_t19570940 ** get_address_of_endpoint_4() { return &___endpoint_4; }
	inline void set_endpoint_4(Uri_t19570940 * value)
	{
		___endpoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___endpoint_4), value);
	}

	inline static int32_t get_offset_of_resourcePath_5() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___resourcePath_5)); }
	inline String_t* get_resourcePath_5() const { return ___resourcePath_5; }
	inline String_t** get_address_of_resourcePath_5() { return &___resourcePath_5; }
	inline void set_resourcePath_5(String_t* value)
	{
		___resourcePath_5 = value;
		Il2CppCodeGenWriteBarrier((&___resourcePath_5), value);
	}

	inline static int32_t get_offset_of_serviceName_6() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___serviceName_6)); }
	inline String_t* get_serviceName_6() const { return ___serviceName_6; }
	inline String_t** get_address_of_serviceName_6() { return &___serviceName_6; }
	inline void set_serviceName_6(String_t* value)
	{
		___serviceName_6 = value;
		Il2CppCodeGenWriteBarrier((&___serviceName_6), value);
	}

	inline static int32_t get_offset_of_originalRequest_7() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___originalRequest_7)); }
	inline AmazonWebServiceRequest_t3384026212 * get_originalRequest_7() const { return ___originalRequest_7; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_originalRequest_7() { return &___originalRequest_7; }
	inline void set_originalRequest_7(AmazonWebServiceRequest_t3384026212 * value)
	{
		___originalRequest_7 = value;
		Il2CppCodeGenWriteBarrier((&___originalRequest_7), value);
	}

	inline static int32_t get_offset_of_content_8() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___content_8)); }
	inline ByteU5BU5D_t3397334013* get_content_8() const { return ___content_8; }
	inline ByteU5BU5D_t3397334013** get_address_of_content_8() { return &___content_8; }
	inline void set_content_8(ByteU5BU5D_t3397334013* value)
	{
		___content_8 = value;
		Il2CppCodeGenWriteBarrier((&___content_8), value);
	}

	inline static int32_t get_offset_of_contentStream_9() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___contentStream_9)); }
	inline Stream_t3255436806 * get_contentStream_9() const { return ___contentStream_9; }
	inline Stream_t3255436806 ** get_address_of_contentStream_9() { return &___contentStream_9; }
	inline void set_contentStream_9(Stream_t3255436806 * value)
	{
		___contentStream_9 = value;
		Il2CppCodeGenWriteBarrier((&___contentStream_9), value);
	}

	inline static int32_t get_offset_of_contentStreamHash_10() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___contentStreamHash_10)); }
	inline String_t* get_contentStreamHash_10() const { return ___contentStreamHash_10; }
	inline String_t** get_address_of_contentStreamHash_10() { return &___contentStreamHash_10; }
	inline void set_contentStreamHash_10(String_t* value)
	{
		___contentStreamHash_10 = value;
		Il2CppCodeGenWriteBarrier((&___contentStreamHash_10), value);
	}

	inline static int32_t get_offset_of_httpMethod_11() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___httpMethod_11)); }
	inline String_t* get_httpMethod_11() const { return ___httpMethod_11; }
	inline String_t** get_address_of_httpMethod_11() { return &___httpMethod_11; }
	inline void set_httpMethod_11(String_t* value)
	{
		___httpMethod_11 = value;
		Il2CppCodeGenWriteBarrier((&___httpMethod_11), value);
	}

	inline static int32_t get_offset_of_useQueryString_12() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___useQueryString_12)); }
	inline bool get_useQueryString_12() const { return ___useQueryString_12; }
	inline bool* get_address_of_useQueryString_12() { return &___useQueryString_12; }
	inline void set_useQueryString_12(bool value)
	{
		___useQueryString_12 = value;
	}

	inline static int32_t get_offset_of_requestName_13() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___requestName_13)); }
	inline String_t* get_requestName_13() const { return ___requestName_13; }
	inline String_t** get_address_of_requestName_13() { return &___requestName_13; }
	inline void set_requestName_13(String_t* value)
	{
		___requestName_13 = value;
		Il2CppCodeGenWriteBarrier((&___requestName_13), value);
	}

	inline static int32_t get_offset_of_alternateRegion_14() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___alternateRegion_14)); }
	inline RegionEndpoint_t661522805 * get_alternateRegion_14() const { return ___alternateRegion_14; }
	inline RegionEndpoint_t661522805 ** get_address_of_alternateRegion_14() { return &___alternateRegion_14; }
	inline void set_alternateRegion_14(RegionEndpoint_t661522805 * value)
	{
		___alternateRegion_14 = value;
		Il2CppCodeGenWriteBarrier((&___alternateRegion_14), value);
	}

	inline static int32_t get_offset_of_originalStreamLength_15() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___originalStreamLength_15)); }
	inline int64_t get_originalStreamLength_15() const { return ___originalStreamLength_15; }
	inline int64_t* get_address_of_originalStreamLength_15() { return &___originalStreamLength_15; }
	inline void set_originalStreamLength_15(int64_t value)
	{
		___originalStreamLength_15 = value;
	}

	inline static int32_t get_offset_of_U3CSetContentFromParametersU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CSetContentFromParametersU3Ek__BackingField_16)); }
	inline bool get_U3CSetContentFromParametersU3Ek__BackingField_16() const { return ___U3CSetContentFromParametersU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CSetContentFromParametersU3Ek__BackingField_16() { return &___U3CSetContentFromParametersU3Ek__BackingField_16; }
	inline void set_U3CSetContentFromParametersU3Ek__BackingField_16(bool value)
	{
		___U3CSetContentFromParametersU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CSuppress404ExceptionsU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CSuppress404ExceptionsU3Ek__BackingField_17)); }
	inline bool get_U3CSuppress404ExceptionsU3Ek__BackingField_17() const { return ___U3CSuppress404ExceptionsU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CSuppress404ExceptionsU3Ek__BackingField_17() { return &___U3CSuppress404ExceptionsU3Ek__BackingField_17; }
	inline void set_U3CSuppress404ExceptionsU3Ek__BackingField_17(bool value)
	{
		___U3CSuppress404ExceptionsU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CAWS4SignerResultU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CAWS4SignerResultU3Ek__BackingField_18)); }
	inline AWS4SigningResult_t430803065 * get_U3CAWS4SignerResultU3Ek__BackingField_18() const { return ___U3CAWS4SignerResultU3Ek__BackingField_18; }
	inline AWS4SigningResult_t430803065 ** get_address_of_U3CAWS4SignerResultU3Ek__BackingField_18() { return &___U3CAWS4SignerResultU3Ek__BackingField_18; }
	inline void set_U3CAWS4SignerResultU3Ek__BackingField_18(AWS4SigningResult_t430803065 * value)
	{
		___U3CAWS4SignerResultU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAWS4SignerResultU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CUseChunkEncodingU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CUseChunkEncodingU3Ek__BackingField_19)); }
	inline bool get_U3CUseChunkEncodingU3Ek__BackingField_19() const { return ___U3CUseChunkEncodingU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CUseChunkEncodingU3Ek__BackingField_19() { return &___U3CUseChunkEncodingU3Ek__BackingField_19; }
	inline void set_U3CUseChunkEncodingU3Ek__BackingField_19(bool value)
	{
		___U3CUseChunkEncodingU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CCanonicalResourcePrefixU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CCanonicalResourcePrefixU3Ek__BackingField_20)); }
	inline String_t* get_U3CCanonicalResourcePrefixU3Ek__BackingField_20() const { return ___U3CCanonicalResourcePrefixU3Ek__BackingField_20; }
	inline String_t** get_address_of_U3CCanonicalResourcePrefixU3Ek__BackingField_20() { return &___U3CCanonicalResourcePrefixU3Ek__BackingField_20; }
	inline void set_U3CCanonicalResourcePrefixU3Ek__BackingField_20(String_t* value)
	{
		___U3CCanonicalResourcePrefixU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCanonicalResourcePrefixU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CUseSigV4U3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CUseSigV4U3Ek__BackingField_21)); }
	inline bool get_U3CUseSigV4U3Ek__BackingField_21() const { return ___U3CUseSigV4U3Ek__BackingField_21; }
	inline bool* get_address_of_U3CUseSigV4U3Ek__BackingField_21() { return &___U3CUseSigV4U3Ek__BackingField_21; }
	inline void set_U3CUseSigV4U3Ek__BackingField_21(bool value)
	{
		___U3CUseSigV4U3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CAuthenticationRegionU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(DefaultRequest_t3080757440, ___U3CAuthenticationRegionU3Ek__BackingField_22)); }
	inline String_t* get_U3CAuthenticationRegionU3Ek__BackingField_22() const { return ___U3CAuthenticationRegionU3Ek__BackingField_22; }
	inline String_t** get_address_of_U3CAuthenticationRegionU3Ek__BackingField_22() { return &___U3CAuthenticationRegionU3Ek__BackingField_22; }
	inline void set_U3CAuthenticationRegionU3Ek__BackingField_22(String_t* value)
	{
		___U3CAuthenticationRegionU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthenticationRegionU3Ek__BackingField_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTREQUEST_T3080757440_H
#ifndef CONFIGURATIONELEMENT_T1996355484_H
#define CONFIGURATIONELEMENT_T1996355484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.ConfigurationElement
struct  ConfigurationElement_t1996355484  : public RuntimeObject
{
public:
	// Amazon.Util.Internal.ElementInformation Amazon.Util.Internal.ConfigurationElement::<ElementInformation>k__BackingField
	ElementInformation_t3988909444 * ___U3CElementInformationU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CElementInformationU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConfigurationElement_t1996355484, ___U3CElementInformationU3Ek__BackingField_0)); }
	inline ElementInformation_t3988909444 * get_U3CElementInformationU3Ek__BackingField_0() const { return ___U3CElementInformationU3Ek__BackingField_0; }
	inline ElementInformation_t3988909444 ** get_address_of_U3CElementInformationU3Ek__BackingField_0() { return &___U3CElementInformationU3Ek__BackingField_0; }
	inline void set_U3CElementInformationU3Ek__BackingField_0(ElementInformation_t3988909444 * value)
	{
		___U3CElementInformationU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CElementInformationU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENT_T1996355484_H
#ifndef AMAZONHOOKEDPLATFORMINFO_T2168027349_H
#define AMAZONHOOKEDPLATFORMINFO_T2168027349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.AmazonHookedPlatformInfo
struct  AmazonHookedPlatformInfo_t2168027349  : public RuntimeObject
{
public:
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_platform
	String_t* ___device_platform_2;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_model
	String_t* ___device_model_3;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_make
	String_t* ___device_make_4;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_platformVersion
	String_t* ___device_platformVersion_5;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::device_locale
	String_t* ___device_locale_6;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::app_version_name
	String_t* ___app_version_name_7;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::app_version_code
	String_t* ___app_version_code_8;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::app_package_name
	String_t* ___app_package_name_9;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::app_title
	String_t* ___app_title_10;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::<PersistentDataPath>k__BackingField
	String_t* ___U3CPersistentDataPathU3Ek__BackingField_11;
	// System.String Amazon.Util.Internal.AmazonHookedPlatformInfo::<UnityVersion>k__BackingField
	String_t* ___U3CUnityVersionU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_device_platform_2() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_platform_2)); }
	inline String_t* get_device_platform_2() const { return ___device_platform_2; }
	inline String_t** get_address_of_device_platform_2() { return &___device_platform_2; }
	inline void set_device_platform_2(String_t* value)
	{
		___device_platform_2 = value;
		Il2CppCodeGenWriteBarrier((&___device_platform_2), value);
	}

	inline static int32_t get_offset_of_device_model_3() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_model_3)); }
	inline String_t* get_device_model_3() const { return ___device_model_3; }
	inline String_t** get_address_of_device_model_3() { return &___device_model_3; }
	inline void set_device_model_3(String_t* value)
	{
		___device_model_3 = value;
		Il2CppCodeGenWriteBarrier((&___device_model_3), value);
	}

	inline static int32_t get_offset_of_device_make_4() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_make_4)); }
	inline String_t* get_device_make_4() const { return ___device_make_4; }
	inline String_t** get_address_of_device_make_4() { return &___device_make_4; }
	inline void set_device_make_4(String_t* value)
	{
		___device_make_4 = value;
		Il2CppCodeGenWriteBarrier((&___device_make_4), value);
	}

	inline static int32_t get_offset_of_device_platformVersion_5() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_platformVersion_5)); }
	inline String_t* get_device_platformVersion_5() const { return ___device_platformVersion_5; }
	inline String_t** get_address_of_device_platformVersion_5() { return &___device_platformVersion_5; }
	inline void set_device_platformVersion_5(String_t* value)
	{
		___device_platformVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___device_platformVersion_5), value);
	}

	inline static int32_t get_offset_of_device_locale_6() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___device_locale_6)); }
	inline String_t* get_device_locale_6() const { return ___device_locale_6; }
	inline String_t** get_address_of_device_locale_6() { return &___device_locale_6; }
	inline void set_device_locale_6(String_t* value)
	{
		___device_locale_6 = value;
		Il2CppCodeGenWriteBarrier((&___device_locale_6), value);
	}

	inline static int32_t get_offset_of_app_version_name_7() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___app_version_name_7)); }
	inline String_t* get_app_version_name_7() const { return ___app_version_name_7; }
	inline String_t** get_address_of_app_version_name_7() { return &___app_version_name_7; }
	inline void set_app_version_name_7(String_t* value)
	{
		___app_version_name_7 = value;
		Il2CppCodeGenWriteBarrier((&___app_version_name_7), value);
	}

	inline static int32_t get_offset_of_app_version_code_8() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___app_version_code_8)); }
	inline String_t* get_app_version_code_8() const { return ___app_version_code_8; }
	inline String_t** get_address_of_app_version_code_8() { return &___app_version_code_8; }
	inline void set_app_version_code_8(String_t* value)
	{
		___app_version_code_8 = value;
		Il2CppCodeGenWriteBarrier((&___app_version_code_8), value);
	}

	inline static int32_t get_offset_of_app_package_name_9() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___app_package_name_9)); }
	inline String_t* get_app_package_name_9() const { return ___app_package_name_9; }
	inline String_t** get_address_of_app_package_name_9() { return &___app_package_name_9; }
	inline void set_app_package_name_9(String_t* value)
	{
		___app_package_name_9 = value;
		Il2CppCodeGenWriteBarrier((&___app_package_name_9), value);
	}

	inline static int32_t get_offset_of_app_title_10() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___app_title_10)); }
	inline String_t* get_app_title_10() const { return ___app_title_10; }
	inline String_t** get_address_of_app_title_10() { return &___app_title_10; }
	inline void set_app_title_10(String_t* value)
	{
		___app_title_10 = value;
		Il2CppCodeGenWriteBarrier((&___app_title_10), value);
	}

	inline static int32_t get_offset_of_U3CPersistentDataPathU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___U3CPersistentDataPathU3Ek__BackingField_11)); }
	inline String_t* get_U3CPersistentDataPathU3Ek__BackingField_11() const { return ___U3CPersistentDataPathU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CPersistentDataPathU3Ek__BackingField_11() { return &___U3CPersistentDataPathU3Ek__BackingField_11; }
	inline void set_U3CPersistentDataPathU3Ek__BackingField_11(String_t* value)
	{
		___U3CPersistentDataPathU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPersistentDataPathU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CUnityVersionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349, ___U3CUnityVersionU3Ek__BackingField_12)); }
	inline String_t* get_U3CUnityVersionU3Ek__BackingField_12() const { return ___U3CUnityVersionU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CUnityVersionU3Ek__BackingField_12() { return &___U3CUnityVersionU3Ek__BackingField_12; }
	inline void set_U3CUnityVersionU3Ek__BackingField_12(String_t* value)
	{
		___U3CUnityVersionU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnityVersionU3Ek__BackingField_12), value);
	}
};

struct AmazonHookedPlatformInfo_t2168027349_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.Logger Amazon.Util.Internal.AmazonHookedPlatformInfo::_logger
	Logger_t2262497814 * ____logger_0;
	// Amazon.Util.Internal.AmazonHookedPlatformInfo Amazon.Util.Internal.AmazonHookedPlatformInfo::instance
	AmazonHookedPlatformInfo_t2168027349 * ___instance_1;

public:
	inline static int32_t get_offset_of__logger_0() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349_StaticFields, ____logger_0)); }
	inline Logger_t2262497814 * get__logger_0() const { return ____logger_0; }
	inline Logger_t2262497814 ** get_address_of__logger_0() { return &____logger_0; }
	inline void set__logger_0(Logger_t2262497814 * value)
	{
		____logger_0 = value;
		Il2CppCodeGenWriteBarrier((&____logger_0), value);
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(AmazonHookedPlatformInfo_t2168027349_StaticFields, ___instance_1)); }
	inline AmazonHookedPlatformInfo_t2168027349 * get_instance_1() const { return ___instance_1; }
	inline AmazonHookedPlatformInfo_t2168027349 ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(AmazonHookedPlatformInfo_t2168027349 * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONHOOKEDPLATFORMINFO_T2168027349_H
#ifndef ELEMENTINFORMATION_T3988909444_H
#define ELEMENTINFORMATION_T3988909444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.ElementInformation
struct  ElementInformation_t3988909444  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Util.Internal.ElementInformation::<IsPresent>k__BackingField
	bool ___U3CIsPresentU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsPresentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ElementInformation_t3988909444, ___U3CIsPresentU3Ek__BackingField_0)); }
	inline bool get_U3CIsPresentU3Ek__BackingField_0() const { return ___U3CIsPresentU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsPresentU3Ek__BackingField_0() { return &___U3CIsPresentU3Ek__BackingField_0; }
	inline void set_U3CIsPresentU3Ek__BackingField_0(bool value)
	{
		___U3CIsPresentU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTINFORMATION_T3988909444_H
#ifndef ROOTCONFIG_T4046630774_H
#define ROOTCONFIG_T4046630774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.RootConfig
struct  RootConfig_t4046630774  : public RuntimeObject
{
public:
	// Amazon.Util.LoggingConfig Amazon.Util.Internal.RootConfig::<Logging>k__BackingField
	LoggingConfig_t4162907495 * ___U3CLoggingU3Ek__BackingField_0;
	// Amazon.Util.ProxyConfig Amazon.Util.Internal.RootConfig::<Proxy>k__BackingField
	ProxyConfig_t2693849256 * ___U3CProxyU3Ek__BackingField_1;
	// System.String Amazon.Util.Internal.RootConfig::<EndpointDefinition>k__BackingField
	String_t* ___U3CEndpointDefinitionU3Ek__BackingField_2;
	// System.String Amazon.Util.Internal.RootConfig::<Region>k__BackingField
	String_t* ___U3CRegionU3Ek__BackingField_3;
	// System.String Amazon.Util.Internal.RootConfig::<ProfileName>k__BackingField
	String_t* ___U3CProfileNameU3Ek__BackingField_4;
	// System.String Amazon.Util.Internal.RootConfig::<ProfilesLocation>k__BackingField
	String_t* ___U3CProfilesLocationU3Ek__BackingField_5;
	// System.Boolean Amazon.Util.Internal.RootConfig::<UseSdkCache>k__BackingField
	bool ___U3CUseSdkCacheU3Ek__BackingField_6;
	// System.Boolean Amazon.Util.Internal.RootConfig::<CorrectForClockSkew>k__BackingField
	bool ___U3CCorrectForClockSkewU3Ek__BackingField_7;
	// System.String Amazon.Util.Internal.RootConfig::<ApplicationName>k__BackingField
	String_t* ___U3CApplicationNameU3Ek__BackingField_8;
	// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement> Amazon.Util.Internal.RootConfig::<ServiceSections>k__BackingField
	RuntimeObject* ___U3CServiceSectionsU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CLoggingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CLoggingU3Ek__BackingField_0)); }
	inline LoggingConfig_t4162907495 * get_U3CLoggingU3Ek__BackingField_0() const { return ___U3CLoggingU3Ek__BackingField_0; }
	inline LoggingConfig_t4162907495 ** get_address_of_U3CLoggingU3Ek__BackingField_0() { return &___U3CLoggingU3Ek__BackingField_0; }
	inline void set_U3CLoggingU3Ek__BackingField_0(LoggingConfig_t4162907495 * value)
	{
		___U3CLoggingU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggingU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CProxyU3Ek__BackingField_1)); }
	inline ProxyConfig_t2693849256 * get_U3CProxyU3Ek__BackingField_1() const { return ___U3CProxyU3Ek__BackingField_1; }
	inline ProxyConfig_t2693849256 ** get_address_of_U3CProxyU3Ek__BackingField_1() { return &___U3CProxyU3Ek__BackingField_1; }
	inline void set_U3CProxyU3Ek__BackingField_1(ProxyConfig_t2693849256 * value)
	{
		___U3CProxyU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CEndpointDefinitionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CEndpointDefinitionU3Ek__BackingField_2)); }
	inline String_t* get_U3CEndpointDefinitionU3Ek__BackingField_2() const { return ___U3CEndpointDefinitionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CEndpointDefinitionU3Ek__BackingField_2() { return &___U3CEndpointDefinitionU3Ek__BackingField_2; }
	inline void set_U3CEndpointDefinitionU3Ek__BackingField_2(String_t* value)
	{
		___U3CEndpointDefinitionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEndpointDefinitionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CRegionU3Ek__BackingField_3)); }
	inline String_t* get_U3CRegionU3Ek__BackingField_3() const { return ___U3CRegionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CRegionU3Ek__BackingField_3() { return &___U3CRegionU3Ek__BackingField_3; }
	inline void set_U3CRegionU3Ek__BackingField_3(String_t* value)
	{
		___U3CRegionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRegionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CProfileNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CProfileNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CProfileNameU3Ek__BackingField_4() const { return ___U3CProfileNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CProfileNameU3Ek__BackingField_4() { return &___U3CProfileNameU3Ek__BackingField_4; }
	inline void set_U3CProfileNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CProfileNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProfileNameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CProfilesLocationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CProfilesLocationU3Ek__BackingField_5)); }
	inline String_t* get_U3CProfilesLocationU3Ek__BackingField_5() const { return ___U3CProfilesLocationU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CProfilesLocationU3Ek__BackingField_5() { return &___U3CProfilesLocationU3Ek__BackingField_5; }
	inline void set_U3CProfilesLocationU3Ek__BackingField_5(String_t* value)
	{
		___U3CProfilesLocationU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProfilesLocationU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CUseSdkCacheU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CUseSdkCacheU3Ek__BackingField_6)); }
	inline bool get_U3CUseSdkCacheU3Ek__BackingField_6() const { return ___U3CUseSdkCacheU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CUseSdkCacheU3Ek__BackingField_6() { return &___U3CUseSdkCacheU3Ek__BackingField_6; }
	inline void set_U3CUseSdkCacheU3Ek__BackingField_6(bool value)
	{
		___U3CUseSdkCacheU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CCorrectForClockSkewU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CCorrectForClockSkewU3Ek__BackingField_7)); }
	inline bool get_U3CCorrectForClockSkewU3Ek__BackingField_7() const { return ___U3CCorrectForClockSkewU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CCorrectForClockSkewU3Ek__BackingField_7() { return &___U3CCorrectForClockSkewU3Ek__BackingField_7; }
	inline void set_U3CCorrectForClockSkewU3Ek__BackingField_7(bool value)
	{
		___U3CCorrectForClockSkewU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CApplicationNameU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CApplicationNameU3Ek__BackingField_8)); }
	inline String_t* get_U3CApplicationNameU3Ek__BackingField_8() const { return ___U3CApplicationNameU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CApplicationNameU3Ek__BackingField_8() { return &___U3CApplicationNameU3Ek__BackingField_8; }
	inline void set_U3CApplicationNameU3Ek__BackingField_8(String_t* value)
	{
		___U3CApplicationNameU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationNameU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CServiceSectionsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RootConfig_t4046630774, ___U3CServiceSectionsU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CServiceSectionsU3Ek__BackingField_9() const { return ___U3CServiceSectionsU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CServiceSectionsU3Ek__BackingField_9() { return &___U3CServiceSectionsU3Ek__BackingField_9; }
	inline void set_U3CServiceSectionsU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CServiceSectionsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServiceSectionsU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROOTCONFIG_T4046630774_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef ABSTRACTTYPEINFO_T3397773112_H
#define ABSTRACTTYPEINFO_T3397773112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.TypeFactory/AbstractTypeInfo
struct  AbstractTypeInfo_t3397773112  : public RuntimeObject
{
public:
	// System.Type Amazon.Util.Internal.TypeFactory/AbstractTypeInfo::_type
	Type_t * ____type_0;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(AbstractTypeInfo_t3397773112, ____type_0)); }
	inline Type_t * get__type_0() const { return ____type_0; }
	inline Type_t ** get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(Type_t * value)
	{
		____type_0 = value;
		Il2CppCodeGenWriteBarrier((&____type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTYPEINFO_T3397773112_H
#ifndef TYPEFACTORY_T695630734_H
#define TYPEFACTORY_T695630734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.TypeFactory
struct  TypeFactory_t695630734  : public RuntimeObject
{
public:

public:
};

struct TypeFactory_t695630734_StaticFields
{
public:
	// Amazon.Util.Internal.ITypeInfo[] Amazon.Util.Internal.TypeFactory::EmptyTypes
	ITypeInfoU5BU5D_t39398432* ___EmptyTypes_0;

public:
	inline static int32_t get_offset_of_EmptyTypes_0() { return static_cast<int32_t>(offsetof(TypeFactory_t695630734_StaticFields, ___EmptyTypes_0)); }
	inline ITypeInfoU5BU5D_t39398432* get_EmptyTypes_0() const { return ___EmptyTypes_0; }
	inline ITypeInfoU5BU5D_t39398432** get_address_of_EmptyTypes_0() { return &___EmptyTypes_0; }
	inline void set_EmptyTypes_0(ITypeInfoU5BU5D_t39398432* value)
	{
		___EmptyTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEFACTORY_T695630734_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T2372916748_H
#define U3CU3EC__DISPLAYCLASS4_0_T2372916748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.TypeFactory/TypeInfoWrapper/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t2372916748  : public RuntimeObject
{
public:
	// System.String Amazon.Util.Internal.TypeFactory/TypeInfoWrapper/<>c__DisplayClass4_0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t2372916748, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T2372916748_H
#ifndef U3CU3EC_T22341567_H
#define U3CU3EC_T22341567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.AndroidInterop/<>c
struct  U3CU3Ec_t22341567  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t22341567_StaticFields
{
public:
	// Amazon.Util.Internal.AndroidInterop/<>c Amazon.Util.Internal.AndroidInterop/<>c::<>9
	U3CU3Ec_t22341567 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c::<>9__1_0
	Func_2_t499475658 * ___U3CU3E9__1_0_1;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c::<>9__1_1
	Func_2_t499475658 * ___U3CU3E9__1_1_2;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c::<>9__3_0
	Func_2_t499475658 * ___U3CU3E9__3_0_3;
	// System.Func`2<System.Reflection.MethodInfo,System.Boolean> Amazon.Util.Internal.AndroidInterop/<>c::<>9__3_1
	Func_2_t499475658 * ___U3CU3E9__3_1_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t22341567 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t22341567 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t22341567 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9__1_0_1)); }
	inline Func_2_t499475658 * get_U3CU3E9__1_0_1() const { return ___U3CU3E9__1_0_1; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__1_0_1() { return &___U3CU3E9__1_0_1; }
	inline void set_U3CU3E9__1_0_1(Func_2_t499475658 * value)
	{
		___U3CU3E9__1_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9__1_1_2)); }
	inline Func_2_t499475658 * get_U3CU3E9__1_1_2() const { return ___U3CU3E9__1_1_2; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__1_1_2() { return &___U3CU3E9__1_1_2; }
	inline void set_U3CU3E9__1_1_2(Func_2_t499475658 * value)
	{
		___U3CU3E9__1_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9__3_0_3)); }
	inline Func_2_t499475658 * get_U3CU3E9__3_0_3() const { return ___U3CU3E9__3_0_3; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__3_0_3() { return &___U3CU3E9__3_0_3; }
	inline void set_U3CU3E9__3_0_3(Func_2_t499475658 * value)
	{
		___U3CU3E9__3_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t22341567_StaticFields, ___U3CU3E9__3_1_4)); }
	inline Func_2_t499475658 * get_U3CU3E9__3_1_4() const { return ___U3CU3E9__3_1_4; }
	inline Func_2_t499475658 ** get_address_of_U3CU3E9__3_1_4() { return &___U3CU3E9__3_1_4; }
	inline void set_U3CU3E9__3_1_4(Func_2_t499475658 * value)
	{
		___U3CU3E9__3_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T22341567_H
#ifndef ANDROIDINTEROP_T1929019630_H
#define ANDROIDINTEROP_T1929019630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.AndroidInterop
struct  AndroidInterop_t1929019630  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDINTEROP_T1929019630_H
#ifndef APPLICATIONINFO_T3929346268_H
#define APPLICATIONINFO_T3929346268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.ApplicationInfo
struct  ApplicationInfo_t3929346268  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONINFO_T3929346268_H
#ifndef SORTEDDICTIONARY_2_T1695433449_H
#define SORTEDDICTIONARY_2_T1695433449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2<System.String,Amazon.Runtime.ParameterValue>
struct  SortedDictionary_2_t1695433449  : public RuntimeObject
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.SortedDictionary`2::tree
	RBTree_t1544615604 * ___tree_0;
	// System.Collections.Generic.SortedDictionary`2/NodeHelper<TKey,TValue> System.Collections.Generic.SortedDictionary`2::hlp
	NodeHelper_t2648599067 * ___hlp_1;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t1695433449, ___tree_0)); }
	inline RBTree_t1544615604 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t1544615604 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t1544615604 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_hlp_1() { return static_cast<int32_t>(offsetof(SortedDictionary_2_t1695433449, ___hlp_1)); }
	inline NodeHelper_t2648599067 * get_hlp_1() const { return ___hlp_1; }
	inline NodeHelper_t2648599067 ** get_address_of_hlp_1() { return &___hlp_1; }
	inline void set_hlp_1(NodeHelper_t2648599067 * value)
	{
		___hlp_1 = value;
		Il2CppCodeGenWriteBarrier((&___hlp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SORTEDDICTIONARY_2_T1695433449_H
#ifndef ENVIRONMENTINFO_T4123618435_H
#define ENVIRONMENTINFO_T4123618435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.EnvironmentInfo
struct  EnvironmentInfo_t4123618435  : public RuntimeObject
{
public:
	// System.String Amazon.Util.Internal.PlatformServices.EnvironmentInfo::<FrameworkUserAgent>k__BackingField
	String_t* ___U3CFrameworkUserAgentU3Ek__BackingField_0;
	// System.String Amazon.Util.Internal.PlatformServices.EnvironmentInfo::<PlatformUserAgent>k__BackingField
	String_t* ___U3CPlatformUserAgentU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFrameworkUserAgentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(EnvironmentInfo_t4123618435, ___U3CFrameworkUserAgentU3Ek__BackingField_0)); }
	inline String_t* get_U3CFrameworkUserAgentU3Ek__BackingField_0() const { return ___U3CFrameworkUserAgentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFrameworkUserAgentU3Ek__BackingField_0() { return &___U3CFrameworkUserAgentU3Ek__BackingField_0; }
	inline void set_U3CFrameworkUserAgentU3Ek__BackingField_0(String_t* value)
	{
		___U3CFrameworkUserAgentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFrameworkUserAgentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPlatformUserAgentU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EnvironmentInfo_t4123618435, ___U3CPlatformUserAgentU3Ek__BackingField_1)); }
	inline String_t* get_U3CPlatformUserAgentU3Ek__BackingField_1() const { return ___U3CPlatformUserAgentU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CPlatformUserAgentU3Ek__BackingField_1() { return &___U3CPlatformUserAgentU3Ek__BackingField_1; }
	inline void set_U3CPlatformUserAgentU3Ek__BackingField_1(String_t* value)
	{
		___U3CPlatformUserAgentU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlatformUserAgentU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVIRONMENTINFO_T4123618435_H
#ifndef AMAZONSERVICECLIENT_T3583134838_H
#define AMAZONSERVICECLIENT_T3583134838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonServiceClient
struct  AmazonServiceClient_t3583134838  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.AmazonServiceClient::_disposed
	bool ____disposed_0;
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.AmazonServiceClient::_logger
	Logger_t2262497814 * ____logger_1;
	// Amazon.Runtime.Internal.RuntimePipeline Amazon.Runtime.AmazonServiceClient::<RuntimePipeline>k__BackingField
	RuntimePipeline_t3355992556 * ___U3CRuntimePipelineU3Ek__BackingField_2;
	// Amazon.Runtime.AWSCredentials Amazon.Runtime.AmazonServiceClient::<Credentials>k__BackingField
	AWSCredentials_t3583921007 * ___U3CCredentialsU3Ek__BackingField_3;
	// Amazon.Runtime.IClientConfig Amazon.Runtime.AmazonServiceClient::<Config>k__BackingField
	RuntimeObject* ___U3CConfigU3Ek__BackingField_4;
	// Amazon.Runtime.PreRequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeMarshallingEvent
	PreRequestEventHandler_t345425304 * ___mBeforeMarshallingEvent_5;
	// Amazon.Runtime.RequestEventHandler Amazon.Runtime.AmazonServiceClient::mBeforeRequestEvent
	RequestEventHandler_t2213783891 * ___mBeforeRequestEvent_6;
	// Amazon.Runtime.ResponseEventHandler Amazon.Runtime.AmazonServiceClient::mAfterResponseEvent
	ResponseEventHandler_t3870676125 * ___mAfterResponseEvent_7;
	// Amazon.Runtime.ExceptionEventHandler Amazon.Runtime.AmazonServiceClient::mExceptionEvent
	ExceptionEventHandler_t3236465969 * ___mExceptionEvent_8;
	// Amazon.Runtime.Internal.Auth.AbstractAWSSigner Amazon.Runtime.AmazonServiceClient::<Signer>k__BackingField
	AbstractAWSSigner_t2114314031 * ___U3CSignerU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ____logger_1)); }
	inline Logger_t2262497814 * get__logger_1() const { return ____logger_1; }
	inline Logger_t2262497814 ** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(Logger_t2262497814 * value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier((&____logger_1), value);
	}

	inline static int32_t get_offset_of_U3CRuntimePipelineU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CRuntimePipelineU3Ek__BackingField_2)); }
	inline RuntimePipeline_t3355992556 * get_U3CRuntimePipelineU3Ek__BackingField_2() const { return ___U3CRuntimePipelineU3Ek__BackingField_2; }
	inline RuntimePipeline_t3355992556 ** get_address_of_U3CRuntimePipelineU3Ek__BackingField_2() { return &___U3CRuntimePipelineU3Ek__BackingField_2; }
	inline void set_U3CRuntimePipelineU3Ek__BackingField_2(RuntimePipeline_t3355992556 * value)
	{
		___U3CRuntimePipelineU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRuntimePipelineU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CCredentialsU3Ek__BackingField_3)); }
	inline AWSCredentials_t3583921007 * get_U3CCredentialsU3Ek__BackingField_3() const { return ___U3CCredentialsU3Ek__BackingField_3; }
	inline AWSCredentials_t3583921007 ** get_address_of_U3CCredentialsU3Ek__BackingField_3() { return &___U3CCredentialsU3Ek__BackingField_3; }
	inline void set_U3CCredentialsU3Ek__BackingField_3(AWSCredentials_t3583921007 * value)
	{
		___U3CCredentialsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCredentialsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CConfigU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CConfigU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CConfigU3Ek__BackingField_4() const { return ___U3CConfigU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CConfigU3Ek__BackingField_4() { return &___U3CConfigU3Ek__BackingField_4; }
	inline void set_U3CConfigU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CConfigU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_mBeforeMarshallingEvent_5() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mBeforeMarshallingEvent_5)); }
	inline PreRequestEventHandler_t345425304 * get_mBeforeMarshallingEvent_5() const { return ___mBeforeMarshallingEvent_5; }
	inline PreRequestEventHandler_t345425304 ** get_address_of_mBeforeMarshallingEvent_5() { return &___mBeforeMarshallingEvent_5; }
	inline void set_mBeforeMarshallingEvent_5(PreRequestEventHandler_t345425304 * value)
	{
		___mBeforeMarshallingEvent_5 = value;
		Il2CppCodeGenWriteBarrier((&___mBeforeMarshallingEvent_5), value);
	}

	inline static int32_t get_offset_of_mBeforeRequestEvent_6() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mBeforeRequestEvent_6)); }
	inline RequestEventHandler_t2213783891 * get_mBeforeRequestEvent_6() const { return ___mBeforeRequestEvent_6; }
	inline RequestEventHandler_t2213783891 ** get_address_of_mBeforeRequestEvent_6() { return &___mBeforeRequestEvent_6; }
	inline void set_mBeforeRequestEvent_6(RequestEventHandler_t2213783891 * value)
	{
		___mBeforeRequestEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBeforeRequestEvent_6), value);
	}

	inline static int32_t get_offset_of_mAfterResponseEvent_7() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mAfterResponseEvent_7)); }
	inline ResponseEventHandler_t3870676125 * get_mAfterResponseEvent_7() const { return ___mAfterResponseEvent_7; }
	inline ResponseEventHandler_t3870676125 ** get_address_of_mAfterResponseEvent_7() { return &___mAfterResponseEvent_7; }
	inline void set_mAfterResponseEvent_7(ResponseEventHandler_t3870676125 * value)
	{
		___mAfterResponseEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___mAfterResponseEvent_7), value);
	}

	inline static int32_t get_offset_of_mExceptionEvent_8() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___mExceptionEvent_8)); }
	inline ExceptionEventHandler_t3236465969 * get_mExceptionEvent_8() const { return ___mExceptionEvent_8; }
	inline ExceptionEventHandler_t3236465969 ** get_address_of_mExceptionEvent_8() { return &___mExceptionEvent_8; }
	inline void set_mExceptionEvent_8(ExceptionEventHandler_t3236465969 * value)
	{
		___mExceptionEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___mExceptionEvent_8), value);
	}

	inline static int32_t get_offset_of_U3CSignerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AmazonServiceClient_t3583134838, ___U3CSignerU3Ek__BackingField_9)); }
	inline AbstractAWSSigner_t2114314031 * get_U3CSignerU3Ek__BackingField_9() const { return ___U3CSignerU3Ek__BackingField_9; }
	inline AbstractAWSSigner_t2114314031 ** get_address_of_U3CSignerU3Ek__BackingField_9() { return &___U3CSignerU3Ek__BackingField_9; }
	inline void set_U3CSignerU3Ek__BackingField_9(AbstractAWSSigner_t2114314031 * value)
	{
		___U3CSignerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSignerU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONSERVICECLIENT_T3583134838_H
#ifndef SERVICEFACTORY_T589839913_H
#define SERVICEFACTORY_T589839913_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.ServiceFactory
struct  ServiceFactory_t589839913  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel> Amazon.Util.Internal.PlatformServices.ServiceFactory::_instantationMappings
	RuntimeObject* ____instantationMappings_3;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Object> Amazon.Util.Internal.PlatformServices.ServiceFactory::_singletonServices
	RuntimeObject* ____singletonServices_4;

public:
	inline static int32_t get_offset_of__instantationMappings_3() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913, ____instantationMappings_3)); }
	inline RuntimeObject* get__instantationMappings_3() const { return ____instantationMappings_3; }
	inline RuntimeObject** get_address_of__instantationMappings_3() { return &____instantationMappings_3; }
	inline void set__instantationMappings_3(RuntimeObject* value)
	{
		____instantationMappings_3 = value;
		Il2CppCodeGenWriteBarrier((&____instantationMappings_3), value);
	}

	inline static int32_t get_offset_of__singletonServices_4() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913, ____singletonServices_4)); }
	inline RuntimeObject* get__singletonServices_4() const { return ____singletonServices_4; }
	inline RuntimeObject** get_address_of__singletonServices_4() { return &____singletonServices_4; }
	inline void set__singletonServices_4(RuntimeObject* value)
	{
		____singletonServices_4 = value;
		Il2CppCodeGenWriteBarrier((&____singletonServices_4), value);
	}
};

struct ServiceFactory_t589839913_StaticFields
{
public:
	// System.Object Amazon.Util.Internal.PlatformServices.ServiceFactory::_lock
	RuntimeObject * ____lock_0;
	// System.Boolean Amazon.Util.Internal.PlatformServices.ServiceFactory::_factoryInitialized
	bool ____factoryInitialized_1;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Type> Amazon.Util.Internal.PlatformServices.ServiceFactory::_mappings
	RuntimeObject* ____mappings_2;
	// Amazon.Util.Internal.PlatformServices.ServiceFactory Amazon.Util.Internal.PlatformServices.ServiceFactory::Instance
	ServiceFactory_t589839913 * ___Instance_5;

public:
	inline static int32_t get_offset_of__lock_0() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913_StaticFields, ____lock_0)); }
	inline RuntimeObject * get__lock_0() const { return ____lock_0; }
	inline RuntimeObject ** get_address_of__lock_0() { return &____lock_0; }
	inline void set__lock_0(RuntimeObject * value)
	{
		____lock_0 = value;
		Il2CppCodeGenWriteBarrier((&____lock_0), value);
	}

	inline static int32_t get_offset_of__factoryInitialized_1() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913_StaticFields, ____factoryInitialized_1)); }
	inline bool get__factoryInitialized_1() const { return ____factoryInitialized_1; }
	inline bool* get_address_of__factoryInitialized_1() { return &____factoryInitialized_1; }
	inline void set__factoryInitialized_1(bool value)
	{
		____factoryInitialized_1 = value;
	}

	inline static int32_t get_offset_of__mappings_2() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913_StaticFields, ____mappings_2)); }
	inline RuntimeObject* get__mappings_2() const { return ____mappings_2; }
	inline RuntimeObject** get_address_of__mappings_2() { return &____mappings_2; }
	inline void set__mappings_2(RuntimeObject* value)
	{
		____mappings_2 = value;
		Il2CppCodeGenWriteBarrier((&____mappings_2), value);
	}

	inline static int32_t get_offset_of_Instance_5() { return static_cast<int32_t>(offsetof(ServiceFactory_t589839913_StaticFields, ___Instance_5)); }
	inline ServiceFactory_t589839913 * get_Instance_5() const { return ___Instance_5; }
	inline ServiceFactory_t589839913 ** get_address_of_Instance_5() { return &___Instance_5; }
	inline void set_Instance_5(ServiceFactory_t589839913 * value)
	{
		___Instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEFACTORY_T589839913_H
#ifndef NETWORKREACHABILITY_T3059923765_H
#define NETWORKREACHABILITY_T3059923765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.NetworkReachability
struct  NetworkReachability_t3059923765  : public RuntimeObject
{
public:
	// System.EventHandler`1<Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs> Amazon.Util.Internal.PlatformServices.NetworkReachability::mNetworkReachabilityChanged
	EventHandler_1_t198474825 * ___mNetworkReachabilityChanged_0;

public:
	inline static int32_t get_offset_of_mNetworkReachabilityChanged_0() { return static_cast<int32_t>(offsetof(NetworkReachability_t3059923765, ___mNetworkReachabilityChanged_0)); }
	inline EventHandler_1_t198474825 * get_mNetworkReachabilityChanged_0() const { return ___mNetworkReachabilityChanged_0; }
	inline EventHandler_1_t198474825 ** get_address_of_mNetworkReachabilityChanged_0() { return &___mNetworkReachabilityChanged_0; }
	inline void set_mNetworkReachabilityChanged_0(EventHandler_1_t198474825 * value)
	{
		___mNetworkReachabilityChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___mNetworkReachabilityChanged_0), value);
	}
};

struct NetworkReachability_t3059923765_StaticFields
{
public:
	// System.Object Amazon.Util.Internal.PlatformServices.NetworkReachability::reachabilityChangedLock
	RuntimeObject * ___reachabilityChangedLock_1;

public:
	inline static int32_t get_offset_of_reachabilityChangedLock_1() { return static_cast<int32_t>(offsetof(NetworkReachability_t3059923765_StaticFields, ___reachabilityChangedLock_1)); }
	inline RuntimeObject * get_reachabilityChangedLock_1() const { return ___reachabilityChangedLock_1; }
	inline RuntimeObject ** get_address_of_reachabilityChangedLock_1() { return &___reachabilityChangedLock_1; }
	inline void set_reachabilityChangedLock_1(RuntimeObject * value)
	{
		___reachabilityChangedLock_1 = value;
		Il2CppCodeGenWriteBarrier((&___reachabilityChangedLock_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKREACHABILITY_T3059923765_H
#ifndef APPLICATIONSETTINGS_T3429773411_H
#define APPLICATIONSETTINGS_T3429773411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.ApplicationSettings
struct  ApplicationSettings_t3429773411  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLICATIONSETTINGS_T3429773411_H
#ifndef PARAMETERCOLLECTION_T311980373_H
#define PARAMETERCOLLECTION_T311980373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ParameterCollection
struct  ParameterCollection_t311980373  : public SortedDictionary_2_t1695433449
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERCOLLECTION_T311980373_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef KEYVALUEPAIR_2_T2098134078_H
#define KEYVALUEPAIR_2_T2098134078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Amazon.Runtime.ParameterValue>
struct  KeyValuePair_2_t2098134078 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ParameterValue_t2426009594 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2098134078, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2098134078, ___value_1)); }
	inline ParameterValue_t2426009594 * get_value_1() const { return ___value_1; }
	inline ParameterValue_t2426009594 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ParameterValue_t2426009594 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2098134078_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef HTTPERRORRESPONSEEXCEPTION_T3191555406_H
#define HTTPERRORRESPONSEEXCEPTION_T3191555406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.HttpErrorResponseException
struct  HttpErrorResponseException_t3191555406  : public Exception_t1927440687
{
public:
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.HttpErrorResponseException::<Response>k__BackingField
	RuntimeObject* ___U3CResponseU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HttpErrorResponseException_t3191555406, ___U3CResponseU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CResponseU3Ek__BackingField_11() const { return ___U3CResponseU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CResponseU3Ek__BackingField_11() { return &___U3CResponseU3Ek__BackingField_11; }
	inline void set_U3CResponseU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CResponseU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPERRORRESPONSEEXCEPTION_T3191555406_H
#ifndef ASYNCREQUESTCONTEXT_T3705567694_H
#define ASYNCREQUESTCONTEXT_T3705567694_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.AsyncRequestContext
struct  AsyncRequestContext_t3705567694  : public RequestContext_t2912551040
{
public:
	// System.AsyncCallback Amazon.Runtime.Internal.AsyncRequestContext::<Callback>k__BackingField
	AsyncCallback_t163412349 * ___U3CCallbackU3Ek__BackingField_11;
	// System.Object Amazon.Runtime.Internal.AsyncRequestContext::<State>k__BackingField
	RuntimeObject * ___U3CStateU3Ek__BackingField_12;
	// Amazon.Runtime.AsyncOptions Amazon.Runtime.Internal.AsyncRequestContext::<AsyncOptions>k__BackingField
	AsyncOptions_t558351272 * ___U3CAsyncOptionsU3Ek__BackingField_13;
	// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions> Amazon.Runtime.Internal.AsyncRequestContext::<Action>k__BackingField
	Action_4_t897279368 * ___U3CActionU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(AsyncRequestContext_t3705567694, ___U3CCallbackU3Ek__BackingField_11)); }
	inline AsyncCallback_t163412349 * get_U3CCallbackU3Ek__BackingField_11() const { return ___U3CCallbackU3Ek__BackingField_11; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CCallbackU3Ek__BackingField_11() { return &___U3CCallbackU3Ek__BackingField_11; }
	inline void set_U3CCallbackU3Ek__BackingField_11(AsyncCallback_t163412349 * value)
	{
		___U3CCallbackU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(AsyncRequestContext_t3705567694, ___U3CStateU3Ek__BackingField_12)); }
	inline RuntimeObject * get_U3CStateU3Ek__BackingField_12() const { return ___U3CStateU3Ek__BackingField_12; }
	inline RuntimeObject ** get_address_of_U3CStateU3Ek__BackingField_12() { return &___U3CStateU3Ek__BackingField_12; }
	inline void set_U3CStateU3Ek__BackingField_12(RuntimeObject * value)
	{
		___U3CStateU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStateU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CAsyncOptionsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(AsyncRequestContext_t3705567694, ___U3CAsyncOptionsU3Ek__BackingField_13)); }
	inline AsyncOptions_t558351272 * get_U3CAsyncOptionsU3Ek__BackingField_13() const { return ___U3CAsyncOptionsU3Ek__BackingField_13; }
	inline AsyncOptions_t558351272 ** get_address_of_U3CAsyncOptionsU3Ek__BackingField_13() { return &___U3CAsyncOptionsU3Ek__BackingField_13; }
	inline void set_U3CAsyncOptionsU3Ek__BackingField_13(AsyncOptions_t558351272 * value)
	{
		___U3CAsyncOptionsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAsyncOptionsU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AsyncRequestContext_t3705567694, ___U3CActionU3Ek__BackingField_14)); }
	inline Action_4_t897279368 * get_U3CActionU3Ek__BackingField_14() const { return ___U3CActionU3Ek__BackingField_14; }
	inline Action_4_t897279368 ** get_address_of_U3CActionU3Ek__BackingField_14() { return &___U3CActionU3Ek__BackingField_14; }
	inline void set_U3CActionU3Ek__BackingField_14(Action_4_t897279368 * value)
	{
		___U3CActionU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActionU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREQUESTCONTEXT_T3705567694_H
#ifndef ASYNCRESPONSECONTEXT_T210983900_H
#define ASYNCRESPONSECONTEXT_T210983900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.AsyncResponseContext
struct  AsyncResponseContext_t210983900  : public ResponseContext_t3850805926
{
public:
	// Amazon.Runtime.Internal.RuntimeAsyncResult Amazon.Runtime.Internal.AsyncResponseContext::<AsyncResult>k__BackingField
	RuntimeAsyncResult_t4279356013 * ___U3CAsyncResultU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAsyncResultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AsyncResponseContext_t210983900, ___U3CAsyncResultU3Ek__BackingField_2)); }
	inline RuntimeAsyncResult_t4279356013 * get_U3CAsyncResultU3Ek__BackingField_2() const { return ___U3CAsyncResultU3Ek__BackingField_2; }
	inline RuntimeAsyncResult_t4279356013 ** get_address_of_U3CAsyncResultU3Ek__BackingField_2() { return &___U3CAsyncResultU3Ek__BackingField_2; }
	inline void set_U3CAsyncResultU3Ek__BackingField_2(RuntimeAsyncResult_t4279356013 * value)
	{
		___U3CAsyncResultU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAsyncResultU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCRESPONSECONTEXT_T210983900_H
#ifndef NODEENUMERATOR_T648190100_H
#define NODEENUMERATOR_T648190100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.RBTree/NodeEnumerator
struct  NodeEnumerator_t648190100 
{
public:
	// System.Collections.Generic.RBTree System.Collections.Generic.RBTree/NodeEnumerator::tree
	RBTree_t1544615604 * ___tree_0;
	// System.UInt32 System.Collections.Generic.RBTree/NodeEnumerator::version
	uint32_t ___version_1;
	// System.Collections.Generic.Stack`1<System.Collections.Generic.RBTree/Node> System.Collections.Generic.RBTree/NodeEnumerator::pennants
	Stack_1_t3586864480 * ___pennants_2;

public:
	inline static int32_t get_offset_of_tree_0() { return static_cast<int32_t>(offsetof(NodeEnumerator_t648190100, ___tree_0)); }
	inline RBTree_t1544615604 * get_tree_0() const { return ___tree_0; }
	inline RBTree_t1544615604 ** get_address_of_tree_0() { return &___tree_0; }
	inline void set_tree_0(RBTree_t1544615604 * value)
	{
		___tree_0 = value;
		Il2CppCodeGenWriteBarrier((&___tree_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(NodeEnumerator_t648190100, ___version_1)); }
	inline uint32_t get_version_1() const { return ___version_1; }
	inline uint32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(uint32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_pennants_2() { return static_cast<int32_t>(offsetof(NodeEnumerator_t648190100, ___pennants_2)); }
	inline Stack_1_t3586864480 * get_pennants_2() const { return ___pennants_2; }
	inline Stack_1_t3586864480 ** get_address_of_pennants_2() { return &___pennants_2; }
	inline void set_pennants_2(Stack_1_t3586864480 * value)
	{
		___pennants_2 = value;
		Il2CppCodeGenWriteBarrier((&___pennants_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Collections.Generic.RBTree/NodeEnumerator
struct NodeEnumerator_t648190100_marshaled_pinvoke
{
	RBTree_t1544615604 * ___tree_0;
	uint32_t ___version_1;
	Stack_1_t3586864480 * ___pennants_2;
};
// Native definition for COM marshalling of System.Collections.Generic.RBTree/NodeEnumerator
struct NodeEnumerator_t648190100_marshaled_com
{
	RBTree_t1544615604 * ___tree_0;
	uint32_t ___version_1;
	Stack_1_t3586864480 * ___pennants_2;
};
#endif // NODEENUMERATOR_T648190100_H
#ifndef KEYVALUEPAIR_2_T1701344717_H
#define KEYVALUEPAIR_2_T1701344717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct  KeyValuePair_2_t1701344717 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1701344717, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1701344717, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1701344717_H
#ifndef ENUMERATOR_T933071039_H
#define ENUMERATOR_T933071039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.String>
struct  Enumerator_t933071039 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1398341365 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___l_0)); }
	inline List_1_t1398341365 * get_l_0() const { return ___l_0; }
	inline List_1_t1398341365 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1398341365 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t933071039, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T933071039_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef STRINGLISTPARAMETERVALUE_T1474210831_H
#define STRINGLISTPARAMETERVALUE_T1474210831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.StringListParameterValue
struct  StringListParameterValue_t1474210831  : public ParameterValue_t2426009594
{
public:
	// System.Collections.Generic.List`1<System.String> Amazon.Runtime.StringListParameterValue::<Value>k__BackingField
	List_1_t1398341365 * ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StringListParameterValue_t1474210831, ___U3CValueU3Ek__BackingField_0)); }
	inline List_1_t1398341365 * get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline List_1_t1398341365 ** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(List_1_t1398341365 * value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGLISTPARAMETERVALUE_T1474210831_H
#ifndef PREREQUESTEVENTARGS_T776850383_H
#define PREREQUESTEVENTARGS_T776850383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.PreRequestEventArgs
struct  PreRequestEventArgs_t776850383  : public EventArgs_t3289624707
{
public:
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.PreRequestEventArgs::<Request>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3CRequestU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PreRequestEventArgs_t776850383, ___U3CRequestU3Ek__BackingField_1)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3CRequestU3Ek__BackingField_1() const { return ___U3CRequestU3Ek__BackingField_1; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3CRequestU3Ek__BackingField_1() { return &___U3CRequestU3Ek__BackingField_1; }
	inline void set_U3CRequestU3Ek__BackingField_1(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3CRequestU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREREQUESTEVENTARGS_T776850383_H
#ifndef REQUESTEVENTARGS_T434225820_H
#define REQUESTEVENTARGS_T434225820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.RequestEventArgs
struct  RequestEventArgs_t434225820  : public EventArgs_t3289624707
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTEVENTARGS_T434225820_H
#ifndef STRINGPARAMETERVALUE_T1318048037_H
#define STRINGPARAMETERVALUE_T1318048037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.StringParameterValue
struct  StringParameterValue_t1318048037  : public ParameterValue_t2426009594
{
public:
	// System.String Amazon.Runtime.StringParameterValue::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StringParameterValue_t1318048037, ___U3CValueU3Ek__BackingField_0)); }
	inline String_t* get_U3CValueU3Ek__BackingField_0() const { return ___U3CValueU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_0() { return &___U3CValueU3Ek__BackingField_0; }
	inline void set_U3CValueU3Ek__BackingField_0(String_t* value)
	{
		___U3CValueU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPARAMETERVALUE_T1318048037_H
#ifndef TYPEINFOWRAPPER_T3766999367_H
#define TYPEINFOWRAPPER_T3766999367_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.TypeFactory/TypeInfoWrapper
struct  TypeInfoWrapper_t3766999367  : public AbstractTypeInfo_t3397773112
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFOWRAPPER_T3766999367_H
#ifndef AMAZONCLIENTEXCEPTION_T332426366_H
#define AMAZONCLIENTEXCEPTION_T332426366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonClientException
struct  AmazonClientException_t332426366  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONCLIENTEXCEPTION_T332426366_H
#ifndef EXCEPTIONEVENTARGS_T154100464_H
#define EXCEPTIONEVENTARGS_T154100464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ExceptionEventArgs
struct  ExceptionEventArgs_t154100464  : public EventArgs_t3289624707
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONEVENTARGS_T154100464_H
#ifndef RESPONSEEVENTARGS_T4056063878_H
#define RESPONSEEVENTARGS_T4056063878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ResponseEventArgs
struct  ResponseEventArgs_t4056063878  : public EventArgs_t3289624707
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEEVENTARGS_T4056063878_H
#ifndef STREAMTRANSFERPROGRESSARGS_T2639638063_H
#define STREAMTRANSFERPROGRESSARGS_T2639638063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.StreamTransferProgressArgs
struct  StreamTransferProgressArgs_t2639638063  : public EventArgs_t3289624707
{
public:
	// System.Int64 Amazon.Runtime.StreamTransferProgressArgs::_incrementTransferred
	int64_t ____incrementTransferred_1;
	// System.Int64 Amazon.Runtime.StreamTransferProgressArgs::_total
	int64_t ____total_2;
	// System.Int64 Amazon.Runtime.StreamTransferProgressArgs::_transferred
	int64_t ____transferred_3;

public:
	inline static int32_t get_offset_of__incrementTransferred_1() { return static_cast<int32_t>(offsetof(StreamTransferProgressArgs_t2639638063, ____incrementTransferred_1)); }
	inline int64_t get__incrementTransferred_1() const { return ____incrementTransferred_1; }
	inline int64_t* get_address_of__incrementTransferred_1() { return &____incrementTransferred_1; }
	inline void set__incrementTransferred_1(int64_t value)
	{
		____incrementTransferred_1 = value;
	}

	inline static int32_t get_offset_of__total_2() { return static_cast<int32_t>(offsetof(StreamTransferProgressArgs_t2639638063, ____total_2)); }
	inline int64_t get__total_2() const { return ____total_2; }
	inline int64_t* get_address_of__total_2() { return &____total_2; }
	inline void set__total_2(int64_t value)
	{
		____total_2 = value;
	}

	inline static int32_t get_offset_of__transferred_3() { return static_cast<int32_t>(offsetof(StreamTransferProgressArgs_t2639638063, ____transferred_3)); }
	inline int64_t get__transferred_3() const { return ____transferred_3; }
	inline int64_t* get_address_of__transferred_3() { return &____transferred_3; }
	inline void set__transferred_3(int64_t value)
	{
		____transferred_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMTRANSFERPROGRESSARGS_T2639638063_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef BASICAWSCREDENTIALS_T3665160937_H
#define BASICAWSCREDENTIALS_T3665160937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.BasicAWSCredentials
struct  BasicAWSCredentials_t3665160937  : public AWSCredentials_t3583921007
{
public:
	// Amazon.Runtime.ImmutableCredentials Amazon.Runtime.BasicAWSCredentials::_credentials
	ImmutableCredentials_t282353664 * ____credentials_0;

public:
	inline static int32_t get_offset_of__credentials_0() { return static_cast<int32_t>(offsetof(BasicAWSCredentials_t3665160937, ____credentials_0)); }
	inline ImmutableCredentials_t282353664 * get__credentials_0() const { return ____credentials_0; }
	inline ImmutableCredentials_t282353664 ** get_address_of__credentials_0() { return &____credentials_0; }
	inline void set__credentials_0(ImmutableCredentials_t282353664 * value)
	{
		____credentials_0 = value;
		Il2CppCodeGenWriteBarrier((&____credentials_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICAWSCREDENTIALS_T3665160937_H
#ifndef ANONYMOUSAWSCREDENTIALS_T3877662854_H
#define ANONYMOUSAWSCREDENTIALS_T3877662854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AnonymousAWSCredentials
struct  AnonymousAWSCredentials_t3877662854  : public AWSCredentials_t3583921007
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANONYMOUSAWSCREDENTIALS_T3877662854_H
#ifndef WEBSERVICERESPONSEEVENTARGS_T1104338495_H
#define WEBSERVICERESPONSEEVENTARGS_T1104338495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.WebServiceResponseEventArgs
struct  WebServiceResponseEventArgs_t1104338495  : public ResponseEventArgs_t4056063878
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceResponseEventArgs::<RequestHeaders>k__BackingField
	RuntimeObject* ___U3CRequestHeadersU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceResponseEventArgs::<ResponseHeaders>k__BackingField
	RuntimeObject* ___U3CResponseHeadersU3Ek__BackingField_2;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceResponseEventArgs::<Parameters>k__BackingField
	RuntimeObject* ___U3CParametersU3Ek__BackingField_3;
	// System.String Amazon.Runtime.WebServiceResponseEventArgs::<ServiceName>k__BackingField
	String_t* ___U3CServiceNameU3Ek__BackingField_4;
	// System.Uri Amazon.Runtime.WebServiceResponseEventArgs::<Endpoint>k__BackingField
	Uri_t19570940 * ___U3CEndpointU3Ek__BackingField_5;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.WebServiceResponseEventArgs::<Request>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3CRequestU3Ek__BackingField_6;
	// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.WebServiceResponseEventArgs::<Response>k__BackingField
	AmazonWebServiceResponse_t529043356 * ___U3CResponseU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CRequestHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CRequestHeadersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CRequestHeadersU3Ek__BackingField_1() const { return ___U3CRequestHeadersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CRequestHeadersU3Ek__BackingField_1() { return &___U3CRequestHeadersU3Ek__BackingField_1; }
	inline void set_U3CRequestHeadersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CRequestHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestHeadersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CResponseHeadersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CResponseHeadersU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CResponseHeadersU3Ek__BackingField_2() const { return ___U3CResponseHeadersU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CResponseHeadersU3Ek__BackingField_2() { return &___U3CResponseHeadersU3Ek__BackingField_2; }
	inline void set_U3CResponseHeadersU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CResponseHeadersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseHeadersU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CParametersU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CParametersU3Ek__BackingField_3() const { return ___U3CParametersU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CParametersU3Ek__BackingField_3() { return &___U3CParametersU3Ek__BackingField_3; }
	inline void set_U3CParametersU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CParametersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CServiceNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CServiceNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CServiceNameU3Ek__BackingField_4() const { return ___U3CServiceNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CServiceNameU3Ek__BackingField_4() { return &___U3CServiceNameU3Ek__BackingField_4; }
	inline void set_U3CServiceNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CServiceNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServiceNameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CEndpointU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CEndpointU3Ek__BackingField_5)); }
	inline Uri_t19570940 * get_U3CEndpointU3Ek__BackingField_5() const { return ___U3CEndpointU3Ek__BackingField_5; }
	inline Uri_t19570940 ** get_address_of_U3CEndpointU3Ek__BackingField_5() { return &___U3CEndpointU3Ek__BackingField_5; }
	inline void set_U3CEndpointU3Ek__BackingField_5(Uri_t19570940 * value)
	{
		___U3CEndpointU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEndpointU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CRequestU3Ek__BackingField_6)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3CRequestU3Ek__BackingField_6() const { return ___U3CRequestU3Ek__BackingField_6; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3CRequestU3Ek__BackingField_6() { return &___U3CRequestU3Ek__BackingField_6; }
	inline void set_U3CRequestU3Ek__BackingField_6(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3CRequestU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WebServiceResponseEventArgs_t1104338495, ___U3CResponseU3Ek__BackingField_7)); }
	inline AmazonWebServiceResponse_t529043356 * get_U3CResponseU3Ek__BackingField_7() const { return ___U3CResponseU3Ek__BackingField_7; }
	inline AmazonWebServiceResponse_t529043356 ** get_address_of_U3CResponseU3Ek__BackingField_7() { return &___U3CResponseU3Ek__BackingField_7; }
	inline void set_U3CResponseU3Ek__BackingField_7(AmazonWebServiceResponse_t529043356 * value)
	{
		___U3CResponseU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSERVICERESPONSEEVENTARGS_T1104338495_H
#ifndef ERRORTYPE_T1448377524_H
#define ERRORTYPE_T1448377524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ErrorType
struct  ErrorType_t1448377524 
{
public:
	// System.Int32 Amazon.Runtime.ErrorType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ErrorType_t1448377524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORTYPE_T1448377524_H
#ifndef HTTPSTATUSCODE_T1898409641_H
#define HTTPSTATUSCODE_T1898409641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_t1898409641 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t1898409641, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T1898409641_H
#ifndef INSTANTIATIONMODEL_T1632807378_H
#define INSTANTIATIONMODEL_T1632807378_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel
struct  InstantiationModel_t1632807378 
{
public:
	// System.Int32 Amazon.Util.Internal.PlatformServices.ServiceFactory/InstantiationModel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstantiationModel_t1632807378, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTIATIONMODEL_T1632807378_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef NETWORKSTATUS_T879095062_H
#define NETWORKSTATUS_T879095062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.NetworkStatus
struct  NetworkStatus_t879095062 
{
public:
	// System.Int32 Amazon.Util.Internal.PlatformServices.NetworkStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NetworkStatus_t879095062, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSTATUS_T879095062_H
#ifndef NULLABLE_1_T1693325264_H
#define NULLABLE_1_T1693325264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.TimeSpan>
struct  Nullable_1_t1693325264 
{
public:
	// T System.Nullable`1::value
	TimeSpan_t3430258949  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1693325264, ___value_0)); }
	inline TimeSpan_t3430258949  get_value_0() const { return ___value_0; }
	inline TimeSpan_t3430258949 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_t3430258949  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1693325264, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1693325264_H
#ifndef RETRYPOLICY_T1476739446_H
#define RETRYPOLICY_T1476739446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.RetryPolicy
struct  RetryPolicy_t1476739446  : public RuntimeObject
{
public:
	// System.Int32 Amazon.Runtime.RetryPolicy::<MaxRetries>k__BackingField
	int32_t ___U3CMaxRetriesU3Ek__BackingField_0;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.RetryPolicy::<Logger>k__BackingField
	RuntimeObject* ___U3CLoggerU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMaxRetriesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446, ___U3CMaxRetriesU3Ek__BackingField_0)); }
	inline int32_t get_U3CMaxRetriesU3Ek__BackingField_0() const { return ___U3CMaxRetriesU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CMaxRetriesU3Ek__BackingField_0() { return &___U3CMaxRetriesU3Ek__BackingField_0; }
	inline void set_U3CMaxRetriesU3Ek__BackingField_0(int32_t value)
	{
		___U3CMaxRetriesU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLoggerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446, ___U3CLoggerU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CLoggerU3Ek__BackingField_1() const { return ___U3CLoggerU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CLoggerU3Ek__BackingField_1() { return &___U3CLoggerU3Ek__BackingField_1; }
	inline void set_U3CLoggerU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CLoggerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggerU3Ek__BackingField_1), value);
	}
};

struct RetryPolicy_t1476739446_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.RetryPolicy::clockSkewErrorCodes
	HashSet_1_t362681087 * ___clockSkewErrorCodes_2;
	// System.TimeSpan Amazon.Runtime.RetryPolicy::clockSkewMaxThreshold
	TimeSpan_t3430258949  ___clockSkewMaxThreshold_3;

public:
	inline static int32_t get_offset_of_clockSkewErrorCodes_2() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446_StaticFields, ___clockSkewErrorCodes_2)); }
	inline HashSet_1_t362681087 * get_clockSkewErrorCodes_2() const { return ___clockSkewErrorCodes_2; }
	inline HashSet_1_t362681087 ** get_address_of_clockSkewErrorCodes_2() { return &___clockSkewErrorCodes_2; }
	inline void set_clockSkewErrorCodes_2(HashSet_1_t362681087 * value)
	{
		___clockSkewErrorCodes_2 = value;
		Il2CppCodeGenWriteBarrier((&___clockSkewErrorCodes_2), value);
	}

	inline static int32_t get_offset_of_clockSkewMaxThreshold_3() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446_StaticFields, ___clockSkewMaxThreshold_3)); }
	inline TimeSpan_t3430258949  get_clockSkewMaxThreshold_3() const { return ___clockSkewMaxThreshold_3; }
	inline TimeSpan_t3430258949 * get_address_of_clockSkewMaxThreshold_3() { return &___clockSkewMaxThreshold_3; }
	inline void set_clockSkewMaxThreshold_3(TimeSpan_t3430258949  value)
	{
		___clockSkewMaxThreshold_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYPOLICY_T1476739446_H
#ifndef ENUMERATOR_T3063994949_H
#define ENUMERATOR_T3063994949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,Amazon.Runtime.ParameterValue>
struct  Enumerator_t3063994949 
{
public:
	// System.Collections.Generic.RBTree/NodeEnumerator System.Collections.Generic.SortedDictionary`2/Enumerator::host
	NodeEnumerator_t648190100  ___host_0;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.SortedDictionary`2/Enumerator::current
	KeyValuePair_2_t2098134078  ___current_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(Enumerator_t3063994949, ___host_0)); }
	inline NodeEnumerator_t648190100  get_host_0() const { return ___host_0; }
	inline NodeEnumerator_t648190100 * get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(NodeEnumerator_t648190100  value)
	{
		___host_0 = value;
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Enumerator_t3063994949, ___current_1)); }
	inline KeyValuePair_2_t2098134078  get_current_1() const { return ___current_1; }
	inline KeyValuePair_2_t2098134078 * get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(KeyValuePair_2_t2098134078  value)
	{
		___current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3063994949_H
#ifndef METRIC_T3273440202_H
#define METRIC_T3273440202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Metric
struct  Metric_t3273440202 
{
public:
	// System.Int32 Amazon.Runtime.Metric::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Metric_t3273440202, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METRIC_T3273440202_H
#ifndef SIGNINGALGORITHM_T3740229458_H
#define SIGNINGALGORITHM_T3740229458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.SigningAlgorithm
struct  SigningAlgorithm_t3740229458 
{
public:
	// System.Int32 Amazon.Runtime.SigningAlgorithm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SigningAlgorithm_t3740229458, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNINGALGORITHM_T3740229458_H
#ifndef WEBSERVICEEXCEPTIONEVENTARGS_T3226266439_H
#define WEBSERVICEEXCEPTIONEVENTARGS_T3226266439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.WebServiceExceptionEventArgs
struct  WebServiceExceptionEventArgs_t3226266439  : public ExceptionEventArgs_t154100464
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceExceptionEventArgs::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceExceptionEventArgs::<Parameters>k__BackingField
	RuntimeObject* ___U3CParametersU3Ek__BackingField_2;
	// System.String Amazon.Runtime.WebServiceExceptionEventArgs::<ServiceName>k__BackingField
	String_t* ___U3CServiceNameU3Ek__BackingField_3;
	// System.Uri Amazon.Runtime.WebServiceExceptionEventArgs::<Endpoint>k__BackingField
	Uri_t19570940 * ___U3CEndpointU3Ek__BackingField_4;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.WebServiceExceptionEventArgs::<Request>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3CRequestU3Ek__BackingField_5;
	// System.Exception Amazon.Runtime.WebServiceExceptionEventArgs::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebServiceExceptionEventArgs_t3226266439, ___U3CHeadersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_1() const { return ___U3CHeadersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_1() { return &___U3CHeadersU3Ek__BackingField_1; }
	inline void set_U3CHeadersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebServiceExceptionEventArgs_t3226266439, ___U3CParametersU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CParametersU3Ek__BackingField_2() const { return ___U3CParametersU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CParametersU3Ek__BackingField_2() { return &___U3CParametersU3Ek__BackingField_2; }
	inline void set_U3CParametersU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CParametersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CServiceNameU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebServiceExceptionEventArgs_t3226266439, ___U3CServiceNameU3Ek__BackingField_3)); }
	inline String_t* get_U3CServiceNameU3Ek__BackingField_3() const { return ___U3CServiceNameU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CServiceNameU3Ek__BackingField_3() { return &___U3CServiceNameU3Ek__BackingField_3; }
	inline void set_U3CServiceNameU3Ek__BackingField_3(String_t* value)
	{
		___U3CServiceNameU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServiceNameU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CEndpointU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebServiceExceptionEventArgs_t3226266439, ___U3CEndpointU3Ek__BackingField_4)); }
	inline Uri_t19570940 * get_U3CEndpointU3Ek__BackingField_4() const { return ___U3CEndpointU3Ek__BackingField_4; }
	inline Uri_t19570940 ** get_address_of_U3CEndpointU3Ek__BackingField_4() { return &___U3CEndpointU3Ek__BackingField_4; }
	inline void set_U3CEndpointU3Ek__BackingField_4(Uri_t19570940 * value)
	{
		___U3CEndpointU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEndpointU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WebServiceExceptionEventArgs_t3226266439, ___U3CRequestU3Ek__BackingField_5)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3CRequestU3Ek__BackingField_5() const { return ___U3CRequestU3Ek__BackingField_5; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3CRequestU3Ek__BackingField_5() { return &___U3CRequestU3Ek__BackingField_5; }
	inline void set_U3CRequestU3Ek__BackingField_5(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3CRequestU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WebServiceExceptionEventArgs_t3226266439, ___U3CExceptionU3Ek__BackingField_6)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_6() const { return ___U3CExceptionU3Ek__BackingField_6; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_6() { return &___U3CExceptionU3Ek__BackingField_6; }
	inline void set_U3CExceptionU3Ek__BackingField_6(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSERVICEEXCEPTIONEVENTARGS_T3226266439_H
#ifndef WEBSERVICEREQUESTEVENTARGS_T1089733597_H
#define WEBSERVICEREQUESTEVENTARGS_T1089733597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.WebServiceRequestEventArgs
struct  WebServiceRequestEventArgs_t1089733597  : public RequestEventArgs_t434225820
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceRequestEventArgs::<Headers>k__BackingField
	RuntimeObject* ___U3CHeadersU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Amazon.Runtime.WebServiceRequestEventArgs::<Parameters>k__BackingField
	RuntimeObject* ___U3CParametersU3Ek__BackingField_2;
	// Amazon.Runtime.Internal.ParameterCollection Amazon.Runtime.WebServiceRequestEventArgs::<ParameterCollection>k__BackingField
	ParameterCollection_t311980373 * ___U3CParameterCollectionU3Ek__BackingField_3;
	// System.String Amazon.Runtime.WebServiceRequestEventArgs::<ServiceName>k__BackingField
	String_t* ___U3CServiceNameU3Ek__BackingField_4;
	// System.Uri Amazon.Runtime.WebServiceRequestEventArgs::<Endpoint>k__BackingField
	Uri_t19570940 * ___U3CEndpointU3Ek__BackingField_5;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.WebServiceRequestEventArgs::<Request>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3CRequestU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CHeadersU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CHeadersU3Ek__BackingField_1() const { return ___U3CHeadersU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CHeadersU3Ek__BackingField_1() { return &___U3CHeadersU3Ek__BackingField_1; }
	inline void set_U3CHeadersU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CHeadersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CParametersU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CParametersU3Ek__BackingField_2() const { return ___U3CParametersU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CParametersU3Ek__BackingField_2() { return &___U3CParametersU3Ek__BackingField_2; }
	inline void set_U3CParametersU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CParametersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CParameterCollectionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CParameterCollectionU3Ek__BackingField_3)); }
	inline ParameterCollection_t311980373 * get_U3CParameterCollectionU3Ek__BackingField_3() const { return ___U3CParameterCollectionU3Ek__BackingField_3; }
	inline ParameterCollection_t311980373 ** get_address_of_U3CParameterCollectionU3Ek__BackingField_3() { return &___U3CParameterCollectionU3Ek__BackingField_3; }
	inline void set_U3CParameterCollectionU3Ek__BackingField_3(ParameterCollection_t311980373 * value)
	{
		___U3CParameterCollectionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParameterCollectionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CServiceNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CServiceNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CServiceNameU3Ek__BackingField_4() const { return ___U3CServiceNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CServiceNameU3Ek__BackingField_4() { return &___U3CServiceNameU3Ek__BackingField_4; }
	inline void set_U3CServiceNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CServiceNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CServiceNameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CEndpointU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CEndpointU3Ek__BackingField_5)); }
	inline Uri_t19570940 * get_U3CEndpointU3Ek__BackingField_5() const { return ___U3CEndpointU3Ek__BackingField_5; }
	inline Uri_t19570940 ** get_address_of_U3CEndpointU3Ek__BackingField_5() { return &___U3CEndpointU3Ek__BackingField_5; }
	inline void set_U3CEndpointU3Ek__BackingField_5(Uri_t19570940 * value)
	{
		___U3CEndpointU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEndpointU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WebServiceRequestEventArgs_t1089733597, ___U3CRequestU3Ek__BackingField_6)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3CRequestU3Ek__BackingField_6() const { return ___U3CRequestU3Ek__BackingField_6; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3CRequestU3Ek__BackingField_6() { return &___U3CRequestU3Ek__BackingField_6; }
	inline void set_U3CRequestU3Ek__BackingField_6(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3CRequestU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSERVICEREQUESTEVENTARGS_T1089733597_H
#ifndef ERRORRESPONSE_T3502566035_H
#define ERRORRESPONSE_T3502566035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ErrorResponse
struct  ErrorResponse_t3502566035  : public RuntimeObject
{
public:
	// Amazon.Runtime.ErrorType Amazon.Runtime.Internal.ErrorResponse::type
	int32_t ___type_0;
	// System.String Amazon.Runtime.Internal.ErrorResponse::code
	String_t* ___code_1;
	// System.String Amazon.Runtime.Internal.ErrorResponse::message
	String_t* ___message_2;
	// System.String Amazon.Runtime.Internal.ErrorResponse::requestId
	String_t* ___requestId_3;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_code_1() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___code_1)); }
	inline String_t* get_code_1() const { return ___code_1; }
	inline String_t** get_address_of_code_1() { return &___code_1; }
	inline void set_code_1(String_t* value)
	{
		___code_1 = value;
		Il2CppCodeGenWriteBarrier((&___code_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_requestId_3() { return static_cast<int32_t>(offsetof(ErrorResponse_t3502566035, ___requestId_3)); }
	inline String_t* get_requestId_3() const { return ___requestId_3; }
	inline String_t** get_address_of_requestId_3() { return &___requestId_3; }
	inline void set_requestId_3(String_t* value)
	{
		___requestId_3 = value;
		Il2CppCodeGenWriteBarrier((&___requestId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORRESPONSE_T3502566035_H
#ifndef AMAZONSERVICEEXCEPTION_T3748559634_H
#define AMAZONSERVICEEXCEPTION_T3748559634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonServiceException
struct  AmazonServiceException_t3748559634  : public Exception_t1927440687
{
public:
	// Amazon.Runtime.ErrorType Amazon.Runtime.AmazonServiceException::errorType
	int32_t ___errorType_11;
	// System.String Amazon.Runtime.AmazonServiceException::errorCode
	String_t* ___errorCode_12;
	// System.String Amazon.Runtime.AmazonServiceException::requestId
	String_t* ___requestId_13;
	// System.Net.HttpStatusCode Amazon.Runtime.AmazonServiceException::statusCode
	int32_t ___statusCode_14;

public:
	inline static int32_t get_offset_of_errorType_11() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___errorType_11)); }
	inline int32_t get_errorType_11() const { return ___errorType_11; }
	inline int32_t* get_address_of_errorType_11() { return &___errorType_11; }
	inline void set_errorType_11(int32_t value)
	{
		___errorType_11 = value;
	}

	inline static int32_t get_offset_of_errorCode_12() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___errorCode_12)); }
	inline String_t* get_errorCode_12() const { return ___errorCode_12; }
	inline String_t** get_address_of_errorCode_12() { return &___errorCode_12; }
	inline void set_errorCode_12(String_t* value)
	{
		___errorCode_12 = value;
		Il2CppCodeGenWriteBarrier((&___errorCode_12), value);
	}

	inline static int32_t get_offset_of_requestId_13() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___requestId_13)); }
	inline String_t* get_requestId_13() const { return ___requestId_13; }
	inline String_t** get_address_of_requestId_13() { return &___requestId_13; }
	inline void set_requestId_13(String_t* value)
	{
		___requestId_13 = value;
		Il2CppCodeGenWriteBarrier((&___requestId_13), value);
	}

	inline static int32_t get_offset_of_statusCode_14() { return static_cast<int32_t>(offsetof(AmazonServiceException_t3748559634, ___statusCode_14)); }
	inline int32_t get_statusCode_14() const { return ___statusCode_14; }
	inline int32_t* get_address_of_statusCode_14() { return &___statusCode_14; }
	inline void set_statusCode_14(int32_t value)
	{
		___statusCode_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONSERVICEEXCEPTION_T3748559634_H
#ifndef U3CGETENUMERATORU3ED__23_T2866349317_H
#define U3CGETENUMERATORU3ED__23_T2866349317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23
struct  U3CGetEnumeratorU3Ed__23_t2866349317  : public RuntimeObject
{
public:
	// System.Int32 Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.String> Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::<>2__current
	KeyValuePair_2_t1701344717  ___U3CU3E2__current_1;
	// Amazon.Runtime.Internal.ParametersDictionaryFacade Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::<>4__this
	ParametersDictionaryFacade_t4039108328 * ___U3CU3E4__this_2;
	// System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,Amazon.Runtime.ParameterValue> Amazon.Runtime.Internal.ParametersDictionaryFacade/<GetEnumerator>d__23::<>7__wrap1
	Enumerator_t3063994949  ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__23_t2866349317, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__23_t2866349317, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t1701344717  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t1701344717 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t1701344717  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__23_t2866349317, ___U3CU3E4__this_2)); }
	inline ParametersDictionaryFacade_t4039108328 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ParametersDictionaryFacade_t4039108328 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ParametersDictionaryFacade_t4039108328 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__23_t2866349317, ___U3CU3E7__wrap1_3)); }
	inline Enumerator_t3063994949  get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline Enumerator_t3063994949 * get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(Enumerator_t3063994949  value)
	{
		___U3CU3E7__wrap1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__23_T2866349317_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef CORRECTCLOCKSKEW_T2265946392_H
#define CORRECTCLOCKSKEW_T2265946392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.CorrectClockSkew
struct  CorrectClockSkew_t2265946392  : public RuntimeObject
{
public:

public:
};

struct CorrectClockSkew_t2265946392_StaticFields
{
public:
	// System.Nullable`1<System.TimeSpan> Amazon.Runtime.CorrectClockSkew::manualClockCorrection
	Nullable_1_t1693325264  ___manualClockCorrection_0;
	// System.Threading.ReaderWriterLockSlim Amazon.Runtime.CorrectClockSkew::manualClockCorrectionLock
	ReaderWriterLockSlim_t3961242320 * ___manualClockCorrectionLock_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.TimeSpan> Amazon.Runtime.CorrectClockSkew::clockCorrectionDictionary
	RuntimeObject* ___clockCorrectionDictionary_2;
	// System.Threading.ReaderWriterLockSlim Amazon.Runtime.CorrectClockSkew::clockCorrectionDictionaryLock
	ReaderWriterLockSlim_t3961242320 * ___clockCorrectionDictionaryLock_3;

public:
	inline static int32_t get_offset_of_manualClockCorrection_0() { return static_cast<int32_t>(offsetof(CorrectClockSkew_t2265946392_StaticFields, ___manualClockCorrection_0)); }
	inline Nullable_1_t1693325264  get_manualClockCorrection_0() const { return ___manualClockCorrection_0; }
	inline Nullable_1_t1693325264 * get_address_of_manualClockCorrection_0() { return &___manualClockCorrection_0; }
	inline void set_manualClockCorrection_0(Nullable_1_t1693325264  value)
	{
		___manualClockCorrection_0 = value;
	}

	inline static int32_t get_offset_of_manualClockCorrectionLock_1() { return static_cast<int32_t>(offsetof(CorrectClockSkew_t2265946392_StaticFields, ___manualClockCorrectionLock_1)); }
	inline ReaderWriterLockSlim_t3961242320 * get_manualClockCorrectionLock_1() const { return ___manualClockCorrectionLock_1; }
	inline ReaderWriterLockSlim_t3961242320 ** get_address_of_manualClockCorrectionLock_1() { return &___manualClockCorrectionLock_1; }
	inline void set_manualClockCorrectionLock_1(ReaderWriterLockSlim_t3961242320 * value)
	{
		___manualClockCorrectionLock_1 = value;
		Il2CppCodeGenWriteBarrier((&___manualClockCorrectionLock_1), value);
	}

	inline static int32_t get_offset_of_clockCorrectionDictionary_2() { return static_cast<int32_t>(offsetof(CorrectClockSkew_t2265946392_StaticFields, ___clockCorrectionDictionary_2)); }
	inline RuntimeObject* get_clockCorrectionDictionary_2() const { return ___clockCorrectionDictionary_2; }
	inline RuntimeObject** get_address_of_clockCorrectionDictionary_2() { return &___clockCorrectionDictionary_2; }
	inline void set_clockCorrectionDictionary_2(RuntimeObject* value)
	{
		___clockCorrectionDictionary_2 = value;
		Il2CppCodeGenWriteBarrier((&___clockCorrectionDictionary_2), value);
	}

	inline static int32_t get_offset_of_clockCorrectionDictionaryLock_3() { return static_cast<int32_t>(offsetof(CorrectClockSkew_t2265946392_StaticFields, ___clockCorrectionDictionaryLock_3)); }
	inline ReaderWriterLockSlim_t3961242320 * get_clockCorrectionDictionaryLock_3() const { return ___clockCorrectionDictionaryLock_3; }
	inline ReaderWriterLockSlim_t3961242320 ** get_address_of_clockCorrectionDictionaryLock_3() { return &___clockCorrectionDictionaryLock_3; }
	inline void set_clockCorrectionDictionaryLock_3(ReaderWriterLockSlim_t3961242320 * value)
	{
		___clockCorrectionDictionaryLock_3 = value;
		Il2CppCodeGenWriteBarrier((&___clockCorrectionDictionaryLock_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORRECTCLOCKSKEW_T2265946392_H
#ifndef U3CGETPARAMETERSENUMERABLEU3ED__4_T1433792641_H
#define U3CGETPARAMETERSENUMERABLEU3ED__4_T1433792641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4
struct  U3CGetParametersEnumerableU3Ed__4_t1433792641  : public RuntimeObject
{
public:
	// System.Int32 Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.String> Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<>2__current
	KeyValuePair_2_t1701344717  ___U3CU3E2__current_1;
	// System.Int32 Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Amazon.Runtime.Internal.ParameterCollection Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<>4__this
	ParameterCollection_t311980373 * ___U3CU3E4__this_3;
	// Amazon.Runtime.StringListParameterValue Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<slpv>5__1
	StringListParameterValue_t1474210831 * ___U3CslpvU3E5__1_4;
	// System.String Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<name>5__2
	String_t* ___U3CnameU3E5__2_5;
	// Amazon.Runtime.ParameterValue Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<value>5__3
	ParameterValue_t2426009594 * ___U3CvalueU3E5__3_6;
	// System.Collections.Generic.SortedDictionary`2/Enumerator<System.String,Amazon.Runtime.ParameterValue> Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<>7__wrap1
	Enumerator_t3063994949  ___U3CU3E7__wrap1_7;
	// System.Collections.Generic.List`1/Enumerator<System.String> Amazon.Runtime.Internal.ParameterCollection/<GetParametersEnumerable>d__4::<>7__wrap2
	Enumerator_t933071039  ___U3CU3E7__wrap2_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetParametersEnumerableU3Ed__4_t1433792641, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetParametersEnumerableU3Ed__4_t1433792641, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t1701344717  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t1701344717 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t1701344717  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetParametersEnumerableU3Ed__4_t1433792641, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetParametersEnumerableU3Ed__4_t1433792641, ___U3CU3E4__this_3)); }
	inline ParameterCollection_t311980373 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ParameterCollection_t311980373 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ParameterCollection_t311980373 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CslpvU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CGetParametersEnumerableU3Ed__4_t1433792641, ___U3CslpvU3E5__1_4)); }
	inline StringListParameterValue_t1474210831 * get_U3CslpvU3E5__1_4() const { return ___U3CslpvU3E5__1_4; }
	inline StringListParameterValue_t1474210831 ** get_address_of_U3CslpvU3E5__1_4() { return &___U3CslpvU3E5__1_4; }
	inline void set_U3CslpvU3E5__1_4(StringListParameterValue_t1474210831 * value)
	{
		___U3CslpvU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CslpvU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_U3CnameU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetParametersEnumerableU3Ed__4_t1433792641, ___U3CnameU3E5__2_5)); }
	inline String_t* get_U3CnameU3E5__2_5() const { return ___U3CnameU3E5__2_5; }
	inline String_t** get_address_of_U3CnameU3E5__2_5() { return &___U3CnameU3E5__2_5; }
	inline void set_U3CnameU3E5__2_5(String_t* value)
	{
		___U3CnameU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CvalueU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetParametersEnumerableU3Ed__4_t1433792641, ___U3CvalueU3E5__3_6)); }
	inline ParameterValue_t2426009594 * get_U3CvalueU3E5__3_6() const { return ___U3CvalueU3E5__3_6; }
	inline ParameterValue_t2426009594 ** get_address_of_U3CvalueU3E5__3_6() { return &___U3CvalueU3E5__3_6; }
	inline void set_U3CvalueU3E5__3_6(ParameterValue_t2426009594 * value)
	{
		___U3CvalueU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CvalueU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_7() { return static_cast<int32_t>(offsetof(U3CGetParametersEnumerableU3Ed__4_t1433792641, ___U3CU3E7__wrap1_7)); }
	inline Enumerator_t3063994949  get_U3CU3E7__wrap1_7() const { return ___U3CU3E7__wrap1_7; }
	inline Enumerator_t3063994949 * get_address_of_U3CU3E7__wrap1_7() { return &___U3CU3E7__wrap1_7; }
	inline void set_U3CU3E7__wrap1_7(Enumerator_t3063994949  value)
	{
		___U3CU3E7__wrap1_7 = value;
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap2_8() { return static_cast<int32_t>(offsetof(U3CGetParametersEnumerableU3Ed__4_t1433792641, ___U3CU3E7__wrap2_8)); }
	inline Enumerator_t933071039  get_U3CU3E7__wrap2_8() const { return ___U3CU3E7__wrap2_8; }
	inline Enumerator_t933071039 * get_address_of_U3CU3E7__wrap2_8() { return &___U3CU3E7__wrap2_8; }
	inline void set_U3CU3E7__wrap2_8(Enumerator_t933071039  value)
	{
		___U3CU3E7__wrap2_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETPARAMETERSENUMERABLEU3ED__4_T1433792641_H
#ifndef CLIENTCONFIG_T3664713661_H
#define CLIENTCONFIG_T3664713661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ClientConfig
struct  ClientConfig_t3664713661  : public RuntimeObject
{
public:
	// Amazon.RegionEndpoint Amazon.Runtime.ClientConfig::regionEndpoint
	RegionEndpoint_t661522805 * ___regionEndpoint_2;
	// System.Boolean Amazon.Runtime.ClientConfig::probeForRegionEndpoint
	bool ___probeForRegionEndpoint_3;
	// System.Boolean Amazon.Runtime.ClientConfig::throttleRetries
	bool ___throttleRetries_4;
	// System.Boolean Amazon.Runtime.ClientConfig::useHttp
	bool ___useHttp_5;
	// System.String Amazon.Runtime.ClientConfig::serviceURL
	String_t* ___serviceURL_6;
	// System.String Amazon.Runtime.ClientConfig::authRegion
	String_t* ___authRegion_7;
	// System.String Amazon.Runtime.ClientConfig::authServiceName
	String_t* ___authServiceName_8;
	// System.String Amazon.Runtime.ClientConfig::signatureVersion
	String_t* ___signatureVersion_9;
	// Amazon.Runtime.SigningAlgorithm Amazon.Runtime.ClientConfig::signatureMethod
	int32_t ___signatureMethod_10;
	// System.Int32 Amazon.Runtime.ClientConfig::maxErrorRetry
	int32_t ___maxErrorRetry_11;
	// System.Boolean Amazon.Runtime.ClientConfig::logResponse
	bool ___logResponse_12;
	// System.Int32 Amazon.Runtime.ClientConfig::bufferSize
	int32_t ___bufferSize_13;
	// System.Int64 Amazon.Runtime.ClientConfig::progressUpdateInterval
	int64_t ___progressUpdateInterval_14;
	// System.Boolean Amazon.Runtime.ClientConfig::resignRetries
	bool ___resignRetries_15;
	// System.Boolean Amazon.Runtime.ClientConfig::logMetrics
	bool ___logMetrics_16;
	// System.Boolean Amazon.Runtime.ClientConfig::disableLogging
	bool ___disableLogging_17;
	// System.Boolean Amazon.Runtime.ClientConfig::allowAutoRedirect
	bool ___allowAutoRedirect_18;
	// System.Boolean Amazon.Runtime.ClientConfig::useDualstackEndpoint
	bool ___useDualstackEndpoint_19;
	// System.Int32 Amazon.Runtime.ClientConfig::proxyPort
	int32_t ___proxyPort_20;

public:
	inline static int32_t get_offset_of_regionEndpoint_2() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___regionEndpoint_2)); }
	inline RegionEndpoint_t661522805 * get_regionEndpoint_2() const { return ___regionEndpoint_2; }
	inline RegionEndpoint_t661522805 ** get_address_of_regionEndpoint_2() { return &___regionEndpoint_2; }
	inline void set_regionEndpoint_2(RegionEndpoint_t661522805 * value)
	{
		___regionEndpoint_2 = value;
		Il2CppCodeGenWriteBarrier((&___regionEndpoint_2), value);
	}

	inline static int32_t get_offset_of_probeForRegionEndpoint_3() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___probeForRegionEndpoint_3)); }
	inline bool get_probeForRegionEndpoint_3() const { return ___probeForRegionEndpoint_3; }
	inline bool* get_address_of_probeForRegionEndpoint_3() { return &___probeForRegionEndpoint_3; }
	inline void set_probeForRegionEndpoint_3(bool value)
	{
		___probeForRegionEndpoint_3 = value;
	}

	inline static int32_t get_offset_of_throttleRetries_4() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___throttleRetries_4)); }
	inline bool get_throttleRetries_4() const { return ___throttleRetries_4; }
	inline bool* get_address_of_throttleRetries_4() { return &___throttleRetries_4; }
	inline void set_throttleRetries_4(bool value)
	{
		___throttleRetries_4 = value;
	}

	inline static int32_t get_offset_of_useHttp_5() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___useHttp_5)); }
	inline bool get_useHttp_5() const { return ___useHttp_5; }
	inline bool* get_address_of_useHttp_5() { return &___useHttp_5; }
	inline void set_useHttp_5(bool value)
	{
		___useHttp_5 = value;
	}

	inline static int32_t get_offset_of_serviceURL_6() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___serviceURL_6)); }
	inline String_t* get_serviceURL_6() const { return ___serviceURL_6; }
	inline String_t** get_address_of_serviceURL_6() { return &___serviceURL_6; }
	inline void set_serviceURL_6(String_t* value)
	{
		___serviceURL_6 = value;
		Il2CppCodeGenWriteBarrier((&___serviceURL_6), value);
	}

	inline static int32_t get_offset_of_authRegion_7() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___authRegion_7)); }
	inline String_t* get_authRegion_7() const { return ___authRegion_7; }
	inline String_t** get_address_of_authRegion_7() { return &___authRegion_7; }
	inline void set_authRegion_7(String_t* value)
	{
		___authRegion_7 = value;
		Il2CppCodeGenWriteBarrier((&___authRegion_7), value);
	}

	inline static int32_t get_offset_of_authServiceName_8() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___authServiceName_8)); }
	inline String_t* get_authServiceName_8() const { return ___authServiceName_8; }
	inline String_t** get_address_of_authServiceName_8() { return &___authServiceName_8; }
	inline void set_authServiceName_8(String_t* value)
	{
		___authServiceName_8 = value;
		Il2CppCodeGenWriteBarrier((&___authServiceName_8), value);
	}

	inline static int32_t get_offset_of_signatureVersion_9() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___signatureVersion_9)); }
	inline String_t* get_signatureVersion_9() const { return ___signatureVersion_9; }
	inline String_t** get_address_of_signatureVersion_9() { return &___signatureVersion_9; }
	inline void set_signatureVersion_9(String_t* value)
	{
		___signatureVersion_9 = value;
		Il2CppCodeGenWriteBarrier((&___signatureVersion_9), value);
	}

	inline static int32_t get_offset_of_signatureMethod_10() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___signatureMethod_10)); }
	inline int32_t get_signatureMethod_10() const { return ___signatureMethod_10; }
	inline int32_t* get_address_of_signatureMethod_10() { return &___signatureMethod_10; }
	inline void set_signatureMethod_10(int32_t value)
	{
		___signatureMethod_10 = value;
	}

	inline static int32_t get_offset_of_maxErrorRetry_11() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___maxErrorRetry_11)); }
	inline int32_t get_maxErrorRetry_11() const { return ___maxErrorRetry_11; }
	inline int32_t* get_address_of_maxErrorRetry_11() { return &___maxErrorRetry_11; }
	inline void set_maxErrorRetry_11(int32_t value)
	{
		___maxErrorRetry_11 = value;
	}

	inline static int32_t get_offset_of_logResponse_12() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___logResponse_12)); }
	inline bool get_logResponse_12() const { return ___logResponse_12; }
	inline bool* get_address_of_logResponse_12() { return &___logResponse_12; }
	inline void set_logResponse_12(bool value)
	{
		___logResponse_12 = value;
	}

	inline static int32_t get_offset_of_bufferSize_13() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___bufferSize_13)); }
	inline int32_t get_bufferSize_13() const { return ___bufferSize_13; }
	inline int32_t* get_address_of_bufferSize_13() { return &___bufferSize_13; }
	inline void set_bufferSize_13(int32_t value)
	{
		___bufferSize_13 = value;
	}

	inline static int32_t get_offset_of_progressUpdateInterval_14() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___progressUpdateInterval_14)); }
	inline int64_t get_progressUpdateInterval_14() const { return ___progressUpdateInterval_14; }
	inline int64_t* get_address_of_progressUpdateInterval_14() { return &___progressUpdateInterval_14; }
	inline void set_progressUpdateInterval_14(int64_t value)
	{
		___progressUpdateInterval_14 = value;
	}

	inline static int32_t get_offset_of_resignRetries_15() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___resignRetries_15)); }
	inline bool get_resignRetries_15() const { return ___resignRetries_15; }
	inline bool* get_address_of_resignRetries_15() { return &___resignRetries_15; }
	inline void set_resignRetries_15(bool value)
	{
		___resignRetries_15 = value;
	}

	inline static int32_t get_offset_of_logMetrics_16() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___logMetrics_16)); }
	inline bool get_logMetrics_16() const { return ___logMetrics_16; }
	inline bool* get_address_of_logMetrics_16() { return &___logMetrics_16; }
	inline void set_logMetrics_16(bool value)
	{
		___logMetrics_16 = value;
	}

	inline static int32_t get_offset_of_disableLogging_17() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___disableLogging_17)); }
	inline bool get_disableLogging_17() const { return ___disableLogging_17; }
	inline bool* get_address_of_disableLogging_17() { return &___disableLogging_17; }
	inline void set_disableLogging_17(bool value)
	{
		___disableLogging_17 = value;
	}

	inline static int32_t get_offset_of_allowAutoRedirect_18() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___allowAutoRedirect_18)); }
	inline bool get_allowAutoRedirect_18() const { return ___allowAutoRedirect_18; }
	inline bool* get_address_of_allowAutoRedirect_18() { return &___allowAutoRedirect_18; }
	inline void set_allowAutoRedirect_18(bool value)
	{
		___allowAutoRedirect_18 = value;
	}

	inline static int32_t get_offset_of_useDualstackEndpoint_19() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___useDualstackEndpoint_19)); }
	inline bool get_useDualstackEndpoint_19() const { return ___useDualstackEndpoint_19; }
	inline bool* get_address_of_useDualstackEndpoint_19() { return &___useDualstackEndpoint_19; }
	inline void set_useDualstackEndpoint_19(bool value)
	{
		___useDualstackEndpoint_19 = value;
	}

	inline static int32_t get_offset_of_proxyPort_20() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661, ___proxyPort_20)); }
	inline int32_t get_proxyPort_20() const { return ___proxyPort_20; }
	inline int32_t* get_address_of_proxyPort_20() { return &___proxyPort_20; }
	inline void set_proxyPort_20(int32_t value)
	{
		___proxyPort_20 = value;
	}
};

struct ClientConfig_t3664713661_StaticFields
{
public:
	// System.TimeSpan Amazon.Runtime.ClientConfig::InfiniteTimeout
	TimeSpan_t3430258949  ___InfiniteTimeout_0;
	// System.TimeSpan Amazon.Runtime.ClientConfig::MaxTimeout
	TimeSpan_t3430258949  ___MaxTimeout_1;

public:
	inline static int32_t get_offset_of_InfiniteTimeout_0() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661_StaticFields, ___InfiniteTimeout_0)); }
	inline TimeSpan_t3430258949  get_InfiniteTimeout_0() const { return ___InfiniteTimeout_0; }
	inline TimeSpan_t3430258949 * get_address_of_InfiniteTimeout_0() { return &___InfiniteTimeout_0; }
	inline void set_InfiniteTimeout_0(TimeSpan_t3430258949  value)
	{
		___InfiniteTimeout_0 = value;
	}

	inline static int32_t get_offset_of_MaxTimeout_1() { return static_cast<int32_t>(offsetof(ClientConfig_t3664713661_StaticFields, ___MaxTimeout_1)); }
	inline TimeSpan_t3430258949  get_MaxTimeout_1() const { return ___MaxTimeout_1; }
	inline TimeSpan_t3430258949 * get_address_of_MaxTimeout_1() { return &___MaxTimeout_1; }
	inline void set_MaxTimeout_1(TimeSpan_t3430258949  value)
	{
		___MaxTimeout_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCONFIG_T3664713661_H
#ifndef NETWORKSTATUSEVENTARGS_T1607167653_H
#define NETWORKSTATUSEVENTARGS_T1607167653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs
struct  NetworkStatusEventArgs_t1607167653  : public EventArgs_t3289624707
{
public:
	// Amazon.Util.Internal.PlatformServices.NetworkStatus Amazon.Util.Internal.PlatformServices.NetworkStatusEventArgs::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NetworkStatusEventArgs_t1607167653, ___U3CStatusU3Ek__BackingField_1)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_1() const { return ___U3CStatusU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_1() { return &___U3CStatusU3Ek__BackingField_1; }
	inline void set_U3CStatusU3Ek__BackingField_1(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSTATUSEVENTARGS_T1607167653_H
#ifndef AMAZONWEBSERVICERESPONSE_T529043356_H
#define AMAZONWEBSERVICERESPONSE_T529043356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonWebServiceResponse
struct  AmazonWebServiceResponse_t529043356  : public RuntimeObject
{
public:
	// Amazon.Runtime.ResponseMetadata Amazon.Runtime.AmazonWebServiceResponse::responseMetadataField
	ResponseMetadata_t527027456 * ___responseMetadataField_0;
	// System.Int64 Amazon.Runtime.AmazonWebServiceResponse::contentLength
	int64_t ___contentLength_1;
	// System.Net.HttpStatusCode Amazon.Runtime.AmazonWebServiceResponse::httpStatusCode
	int32_t ___httpStatusCode_2;

public:
	inline static int32_t get_offset_of_responseMetadataField_0() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t529043356, ___responseMetadataField_0)); }
	inline ResponseMetadata_t527027456 * get_responseMetadataField_0() const { return ___responseMetadataField_0; }
	inline ResponseMetadata_t527027456 ** get_address_of_responseMetadataField_0() { return &___responseMetadataField_0; }
	inline void set_responseMetadataField_0(ResponseMetadata_t527027456 * value)
	{
		___responseMetadataField_0 = value;
		Il2CppCodeGenWriteBarrier((&___responseMetadataField_0), value);
	}

	inline static int32_t get_offset_of_contentLength_1() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t529043356, ___contentLength_1)); }
	inline int64_t get_contentLength_1() const { return ___contentLength_1; }
	inline int64_t* get_address_of_contentLength_1() { return &___contentLength_1; }
	inline void set_contentLength_1(int64_t value)
	{
		___contentLength_1 = value;
	}

	inline static int32_t get_offset_of_httpStatusCode_2() { return static_cast<int32_t>(offsetof(AmazonWebServiceResponse_t529043356, ___httpStatusCode_2)); }
	inline int32_t get_httpStatusCode_2() const { return ___httpStatusCode_2; }
	inline int32_t* get_address_of_httpStatusCode_2() { return &___httpStatusCode_2; }
	inline void set_httpStatusCode_2(int32_t value)
	{
		___httpStatusCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONWEBSERVICERESPONSE_T529043356_H
#ifndef RESPONSEEVENTHANDLER_T3870676125_H
#define RESPONSEEVENTHANDLER_T3870676125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ResponseEventHandler
struct  ResponseEventHandler_t3870676125  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEEVENTHANDLER_T3870676125_H
#ifndef PREREQUESTEVENTHANDLER_T345425304_H
#define PREREQUESTEVENTHANDLER_T345425304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.PreRequestEventHandler
struct  PreRequestEventHandler_t345425304  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREREQUESTEVENTHANDLER_T345425304_H
#ifndef EXCEPTIONEVENTHANDLER_T3236465969_H
#define EXCEPTIONEVENTHANDLER_T3236465969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.ExceptionEventHandler
struct  ExceptionEventHandler_t3236465969  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONEVENTHANDLER_T3236465969_H
#ifndef REQUESTEVENTHANDLER_T2213783891_H
#define REQUESTEVENTHANDLER_T2213783891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.RequestEventHandler
struct  RequestEventHandler_t2213783891  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTEVENTHANDLER_T2213783891_H
#ifndef AMAZONUNMARSHALLINGEXCEPTION_T1083472470_H
#define AMAZONUNMARSHALLINGEXCEPTION_T1083472470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.AmazonUnmarshallingException
struct  AmazonUnmarshallingException_t1083472470  : public AmazonServiceException_t3748559634
{
public:
	// System.String Amazon.Runtime.AmazonUnmarshallingException::<LastKnownLocation>k__BackingField
	String_t* ___U3CLastKnownLocationU3Ek__BackingField_15;
	// System.String Amazon.Runtime.AmazonUnmarshallingException::<ResponseBody>k__BackingField
	String_t* ___U3CResponseBodyU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CLastKnownLocationU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(AmazonUnmarshallingException_t1083472470, ___U3CLastKnownLocationU3Ek__BackingField_15)); }
	inline String_t* get_U3CLastKnownLocationU3Ek__BackingField_15() const { return ___U3CLastKnownLocationU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CLastKnownLocationU3Ek__BackingField_15() { return &___U3CLastKnownLocationU3Ek__BackingField_15; }
	inline void set_U3CLastKnownLocationU3Ek__BackingField_15(String_t* value)
	{
		___U3CLastKnownLocationU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastKnownLocationU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CResponseBodyU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(AmazonUnmarshallingException_t1083472470, ___U3CResponseBodyU3Ek__BackingField_16)); }
	inline String_t* get_U3CResponseBodyU3Ek__BackingField_16() const { return ___U3CResponseBodyU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CResponseBodyU3Ek__BackingField_16() { return &___U3CResponseBodyU3Ek__BackingField_16; }
	inline void set_U3CResponseBodyU3Ek__BackingField_16(String_t* value)
	{
		___U3CResponseBodyU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseBodyU3Ek__BackingField_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONUNMARSHALLINGEXCEPTION_T1083472470_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (RootConfig_t4046630774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[10] = 
{
	RootConfig_t4046630774::get_offset_of_U3CLoggingU3Ek__BackingField_0(),
	RootConfig_t4046630774::get_offset_of_U3CProxyU3Ek__BackingField_1(),
	RootConfig_t4046630774::get_offset_of_U3CEndpointDefinitionU3Ek__BackingField_2(),
	RootConfig_t4046630774::get_offset_of_U3CRegionU3Ek__BackingField_3(),
	RootConfig_t4046630774::get_offset_of_U3CProfileNameU3Ek__BackingField_4(),
	RootConfig_t4046630774::get_offset_of_U3CProfilesLocationU3Ek__BackingField_5(),
	RootConfig_t4046630774::get_offset_of_U3CUseSdkCacheU3Ek__BackingField_6(),
	RootConfig_t4046630774::get_offset_of_U3CCorrectForClockSkewU3Ek__BackingField_7(),
	RootConfig_t4046630774::get_offset_of_U3CApplicationNameU3Ek__BackingField_8(),
	RootConfig_t4046630774::get_offset_of_U3CServiceSectionsU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (TypeFactory_t695630734), -1, sizeof(TypeFactory_t695630734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2202[1] = 
{
	TypeFactory_t695630734_StaticFields::get_offset_of_EmptyTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (AbstractTypeInfo_t3397773112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[1] = 
{
	AbstractTypeInfo_t3397773112::get_offset_of__type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (TypeInfoWrapper_t3766999367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (U3CU3Ec__DisplayClass4_0_t2372916748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[1] = 
{
	U3CU3Ec__DisplayClass4_0_t2372916748::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (AndroidInterop_t1929019630), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (U3CU3Ec_t22341567), -1, sizeof(U3CU3Ec_t22341567_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2207[5] = 
{
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9__1_0_1(),
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9__1_1_2(),
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9__3_0_3(),
	U3CU3Ec_t22341567_StaticFields::get_offset_of_U3CU3E9__3_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (AmazonHookedPlatformInfo_t2168027349), -1, sizeof(AmazonHookedPlatformInfo_t2168027349_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2210[13] = 
{
	AmazonHookedPlatformInfo_t2168027349_StaticFields::get_offset_of__logger_0(),
	AmazonHookedPlatformInfo_t2168027349_StaticFields::get_offset_of_instance_1(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_platform_2(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_model_3(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_make_4(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_platformVersion_5(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_device_locale_6(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_app_version_name_7(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_app_version_code_8(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_app_package_name_9(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_app_title_10(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_U3CPersistentDataPathU3Ek__BackingField_11(),
	AmazonHookedPlatformInfo_t2168027349::get_offset_of_U3CUnityVersionU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (ConfigurationElement_t1996355484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[1] = 
{
	ConfigurationElement_t1996355484::get_offset_of_U3CElementInformationU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (ElementInformation_t3988909444), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2212[1] = 
{
	ElementInformation_t3988909444::get_offset_of_U3CIsPresentU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (NetworkStatus_t879095062)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2216[4] = 
{
	NetworkStatus_t879095062::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (NetworkStatusEventArgs_t1607167653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2217[1] = 
{
	NetworkStatusEventArgs_t1607167653::get_offset_of_U3CStatusU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (ServiceFactory_t589839913), -1, sizeof(ServiceFactory_t589839913_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2219[6] = 
{
	ServiceFactory_t589839913_StaticFields::get_offset_of__lock_0(),
	ServiceFactory_t589839913_StaticFields::get_offset_of__factoryInitialized_1(),
	ServiceFactory_t589839913_StaticFields::get_offset_of__mappings_2(),
	ServiceFactory_t589839913::get_offset_of__instantationMappings_3(),
	ServiceFactory_t589839913::get_offset_of__singletonServices_4(),
	ServiceFactory_t589839913_StaticFields::get_offset_of_Instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (InstantiationModel_t1632807378)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2220[3] = 
{
	InstantiationModel_t1632807378::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (ApplicationInfo_t3929346268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (ApplicationSettings_t3429773411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (EnvironmentInfo_t4123618435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[2] = 
{
	EnvironmentInfo_t4123618435::get_offset_of_U3CFrameworkUserAgentU3Ek__BackingField_0(),
	EnvironmentInfo_t4123618435::get_offset_of_U3CPlatformUserAgentU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (NetworkReachability_t3059923765), -1, sizeof(NetworkReachability_t3059923765_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2224[2] = 
{
	NetworkReachability_t3059923765::get_offset_of_mNetworkReachabilityChanged_0(),
	NetworkReachability_t3059923765_StaticFields::get_offset_of_reachabilityChangedLock_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (AmazonClientException_t332426366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (AmazonServiceClient_t3583134838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[10] = 
{
	AmazonServiceClient_t3583134838::get_offset_of__disposed_0(),
	AmazonServiceClient_t3583134838::get_offset_of__logger_1(),
	AmazonServiceClient_t3583134838::get_offset_of_U3CRuntimePipelineU3Ek__BackingField_2(),
	AmazonServiceClient_t3583134838::get_offset_of_U3CCredentialsU3Ek__BackingField_3(),
	AmazonServiceClient_t3583134838::get_offset_of_U3CConfigU3Ek__BackingField_4(),
	AmazonServiceClient_t3583134838::get_offset_of_mBeforeMarshallingEvent_5(),
	AmazonServiceClient_t3583134838::get_offset_of_mBeforeRequestEvent_6(),
	AmazonServiceClient_t3583134838::get_offset_of_mAfterResponseEvent_7(),
	AmazonServiceClient_t3583134838::get_offset_of_mExceptionEvent_8(),
	AmazonServiceClient_t3583134838::get_offset_of_U3CSignerU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (AmazonServiceException_t3748559634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[4] = 
{
	AmazonServiceException_t3748559634::get_offset_of_errorType_11(),
	AmazonServiceException_t3748559634::get_offset_of_errorCode_12(),
	AmazonServiceException_t3748559634::get_offset_of_requestId_13(),
	AmazonServiceException_t3748559634::get_offset_of_statusCode_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (AmazonUnmarshallingException_t1083472470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[2] = 
{
	AmazonUnmarshallingException_t1083472470::get_offset_of_U3CLastKnownLocationU3Ek__BackingField_15(),
	AmazonUnmarshallingException_t1083472470::get_offset_of_U3CResponseBodyU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (AmazonWebServiceRequest_t3384026212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[4] = 
{
	AmazonWebServiceRequest_t3384026212::get_offset_of_mBeforeRequestEvent_0(),
	AmazonWebServiceRequest_t3384026212::get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_StreamUploadProgressCallbackU3Ek__BackingField_1(),
	AmazonWebServiceRequest_t3384026212::get_offset_of_requestState_2(),
	AmazonWebServiceRequest_t3384026212::get_offset_of_U3CAmazon_Runtime_Internal_IAmazonWebServiceRequest_UseSigV4U3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (AmazonWebServiceResponse_t529043356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[3] = 
{
	AmazonWebServiceResponse_t529043356::get_offset_of_responseMetadataField_0(),
	AmazonWebServiceResponse_t529043356::get_offset_of_contentLength_1(),
	AmazonWebServiceResponse_t529043356::get_offset_of_httpStatusCode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (ClientConfig_t3664713661), -1, sizeof(ClientConfig_t3664713661_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2231[21] = 
{
	ClientConfig_t3664713661_StaticFields::get_offset_of_InfiniteTimeout_0(),
	ClientConfig_t3664713661_StaticFields::get_offset_of_MaxTimeout_1(),
	ClientConfig_t3664713661::get_offset_of_regionEndpoint_2(),
	ClientConfig_t3664713661::get_offset_of_probeForRegionEndpoint_3(),
	ClientConfig_t3664713661::get_offset_of_throttleRetries_4(),
	ClientConfig_t3664713661::get_offset_of_useHttp_5(),
	ClientConfig_t3664713661::get_offset_of_serviceURL_6(),
	ClientConfig_t3664713661::get_offset_of_authRegion_7(),
	ClientConfig_t3664713661::get_offset_of_authServiceName_8(),
	ClientConfig_t3664713661::get_offset_of_signatureVersion_9(),
	ClientConfig_t3664713661::get_offset_of_signatureMethod_10(),
	ClientConfig_t3664713661::get_offset_of_maxErrorRetry_11(),
	ClientConfig_t3664713661::get_offset_of_logResponse_12(),
	ClientConfig_t3664713661::get_offset_of_bufferSize_13(),
	ClientConfig_t3664713661::get_offset_of_progressUpdateInterval_14(),
	ClientConfig_t3664713661::get_offset_of_resignRetries_15(),
	ClientConfig_t3664713661::get_offset_of_logMetrics_16(),
	ClientConfig_t3664713661::get_offset_of_disableLogging_17(),
	ClientConfig_t3664713661::get_offset_of_allowAutoRedirect_18(),
	ClientConfig_t3664713661::get_offset_of_useDualstackEndpoint_19(),
	ClientConfig_t3664713661::get_offset_of_proxyPort_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (ConstantClass_t4000559886), -1, sizeof(ConstantClass_t4000559886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2232[3] = 
{
	ConstantClass_t4000559886_StaticFields::get_offset_of_staticFieldsLock_0(),
	ConstantClass_t4000559886_StaticFields::get_offset_of_staticFields_1(),
	ConstantClass_t4000559886::get_offset_of_U3CValueU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (CorrectClockSkew_t2265946392), -1, sizeof(CorrectClockSkew_t2265946392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2233[4] = 
{
	CorrectClockSkew_t2265946392_StaticFields::get_offset_of_manualClockCorrection_0(),
	CorrectClockSkew_t2265946392_StaticFields::get_offset_of_manualClockCorrectionLock_1(),
	CorrectClockSkew_t2265946392_StaticFields::get_offset_of_clockCorrectionDictionary_2(),
	CorrectClockSkew_t2265946392_StaticFields::get_offset_of_clockCorrectionDictionaryLock_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (SigningAlgorithm_t3740229458)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2234[3] = 
{
	SigningAlgorithm_t3740229458::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (ErrorType_t1448377524)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2235[4] = 
{
	ErrorType_t1448377524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (ExceptionEventHandler_t3236465969), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (ExceptionEventArgs_t154100464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (WebServiceExceptionEventArgs_t3226266439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[6] = 
{
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CHeadersU3Ek__BackingField_1(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CParametersU3Ek__BackingField_2(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CServiceNameU3Ek__BackingField_3(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CEndpointU3Ek__BackingField_4(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CRequestU3Ek__BackingField_5(),
	WebServiceExceptionEventArgs_t3226266439::get_offset_of_U3CExceptionU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (ParameterValue_t2426009594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (StringParameterValue_t1318048037), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[1] = 
{
	StringParameterValue_t1318048037::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (StringListParameterValue_t1474210831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[1] = 
{
	StringListParameterValue_t1474210831::get_offset_of_U3CValueU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (PreRequestEventArgs_t776850383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[1] = 
{
	PreRequestEventArgs_t776850383::get_offset_of_U3CRequestU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (PreRequestEventHandler_t345425304), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (RequestEventArgs_t434225820), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (WebServiceRequestEventArgs_t1089733597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[6] = 
{
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CHeadersU3Ek__BackingField_1(),
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CParametersU3Ek__BackingField_2(),
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CParameterCollectionU3Ek__BackingField_3(),
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CServiceNameU3Ek__BackingField_4(),
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CEndpointU3Ek__BackingField_5(),
	WebServiceRequestEventArgs_t1089733597::get_offset_of_U3CRequestU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (RequestEventHandler_t2213783891), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (Metric_t3273440202)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2253[27] = 
{
	Metric_t3273440202::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (ResponseEventHandler_t3870676125), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (ResponseEventArgs_t4056063878), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (WebServiceResponseEventArgs_t1104338495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[7] = 
{
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CRequestHeadersU3Ek__BackingField_1(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CResponseHeadersU3Ek__BackingField_2(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CParametersU3Ek__BackingField_3(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CServiceNameU3Ek__BackingField_4(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CEndpointU3Ek__BackingField_5(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CRequestU3Ek__BackingField_6(),
	WebServiceResponseEventArgs_t1104338495::get_offset_of_U3CResponseU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (ResponseMetadata_t527027456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[2] = 
{
	ResponseMetadata_t527027456::get_offset_of_requestIdField_0(),
	ResponseMetadata_t527027456::get_offset_of__metadata_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (StreamTransferProgressArgs_t2639638063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[3] = 
{
	StreamTransferProgressArgs_t2639638063::get_offset_of__incrementTransferred_1(),
	StreamTransferProgressArgs_t2639638063::get_offset_of__total_2(),
	StreamTransferProgressArgs_t2639638063::get_offset_of__transferred_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (AnonymousAWSCredentials_t3877662854), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (AWSCredentials_t3583921007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (BasicAWSCredentials_t3665160937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[1] = 
{
	BasicAWSCredentials_t3665160937::get_offset_of__credentials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (ImmutableCredentials_t282353664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[3] = 
{
	ImmutableCredentials_t282353664::get_offset_of_U3CAccessKeyU3Ek__BackingField_0(),
	ImmutableCredentials_t282353664::get_offset_of_U3CSecretKeyU3Ek__BackingField_1(),
	ImmutableCredentials_t282353664::get_offset_of_U3CTokenU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (RetryPolicy_t1476739446), -1, sizeof(RetryPolicy_t1476739446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2275[4] = 
{
	RetryPolicy_t1476739446::get_offset_of_U3CMaxRetriesU3Ek__BackingField_0(),
	RetryPolicy_t1476739446::get_offset_of_U3CLoggerU3Ek__BackingField_1(),
	RetryPolicy_t1476739446_StaticFields::get_offset_of_clockSkewErrorCodes_2(),
	RetryPolicy_t1476739446_StaticFields::get_offset_of_clockSkewMaxThreshold_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (AsyncOptions_t558351272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[2] = 
{
	AsyncOptions_t558351272::get_offset_of_U3CExecuteCallbackOnMainThreadU3Ek__BackingField_0(),
	AsyncOptions_t558351272::get_offset_of_U3CStateU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (CapacityManager_t2181902141), -1, sizeof(CapacityManager_t2181902141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2279[6] = 
{
	CapacityManager_t2181902141::get_offset_of__disposed_0(),
	CapacityManager_t2181902141_StaticFields::get_offset_of__serviceUrlToCapacityMap_1(),
	CapacityManager_t2181902141::get_offset_of__rwlock_2(),
	CapacityManager_t2181902141::get_offset_of_THROTTLE_RETRY_REQUEST_COST_3(),
	CapacityManager_t2181902141::get_offset_of_THROTTLED_RETRIES_4(),
	CapacityManager_t2181902141::get_offset_of_THROTTLE_REQUEST_COST_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (RetryCapacity_t2794668894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[2] = 
{
	RetryCapacity_t2794668894::get_offset_of__maxCapacity_0(),
	RetryCapacity_t2794668894::get_offset_of_U3CAvailableCapacityU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (ParameterCollection_t311980373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (U3CGetParametersEnumerableU3Ed__4_t1433792641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[9] = 
{
	U3CGetParametersEnumerableU3Ed__4_t1433792641::get_offset_of_U3CU3E1__state_0(),
	U3CGetParametersEnumerableU3Ed__4_t1433792641::get_offset_of_U3CU3E2__current_1(),
	U3CGetParametersEnumerableU3Ed__4_t1433792641::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetParametersEnumerableU3Ed__4_t1433792641::get_offset_of_U3CU3E4__this_3(),
	U3CGetParametersEnumerableU3Ed__4_t1433792641::get_offset_of_U3CslpvU3E5__1_4(),
	U3CGetParametersEnumerableU3Ed__4_t1433792641::get_offset_of_U3CnameU3E5__2_5(),
	U3CGetParametersEnumerableU3Ed__4_t1433792641::get_offset_of_U3CvalueU3E5__3_6(),
	U3CGetParametersEnumerableU3Ed__4_t1433792641::get_offset_of_U3CU3E7__wrap1_7(),
	U3CGetParametersEnumerableU3Ed__4_t1433792641::get_offset_of_U3CU3E7__wrap2_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (DefaultRequest_t3080757440), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2283[23] = 
{
	DefaultRequest_t3080757440::get_offset_of_parametersCollection_0(),
	DefaultRequest_t3080757440::get_offset_of_parametersFacade_1(),
	DefaultRequest_t3080757440::get_offset_of_headers_2(),
	DefaultRequest_t3080757440::get_offset_of_subResources_3(),
	DefaultRequest_t3080757440::get_offset_of_endpoint_4(),
	DefaultRequest_t3080757440::get_offset_of_resourcePath_5(),
	DefaultRequest_t3080757440::get_offset_of_serviceName_6(),
	DefaultRequest_t3080757440::get_offset_of_originalRequest_7(),
	DefaultRequest_t3080757440::get_offset_of_content_8(),
	DefaultRequest_t3080757440::get_offset_of_contentStream_9(),
	DefaultRequest_t3080757440::get_offset_of_contentStreamHash_10(),
	DefaultRequest_t3080757440::get_offset_of_httpMethod_11(),
	DefaultRequest_t3080757440::get_offset_of_useQueryString_12(),
	DefaultRequest_t3080757440::get_offset_of_requestName_13(),
	DefaultRequest_t3080757440::get_offset_of_alternateRegion_14(),
	DefaultRequest_t3080757440::get_offset_of_originalStreamLength_15(),
	DefaultRequest_t3080757440::get_offset_of_U3CSetContentFromParametersU3Ek__BackingField_16(),
	DefaultRequest_t3080757440::get_offset_of_U3CSuppress404ExceptionsU3Ek__BackingField_17(),
	DefaultRequest_t3080757440::get_offset_of_U3CAWS4SignerResultU3Ek__BackingField_18(),
	DefaultRequest_t3080757440::get_offset_of_U3CUseChunkEncodingU3Ek__BackingField_19(),
	DefaultRequest_t3080757440::get_offset_of_U3CCanonicalResourcePrefixU3Ek__BackingField_20(),
	DefaultRequest_t3080757440::get_offset_of_U3CUseSigV4U3Ek__BackingField_21(),
	DefaultRequest_t3080757440::get_offset_of_U3CAuthenticationRegionU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (U3CU3Ec_t12014549), -1, sizeof(U3CU3Ec_t12014549_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2284[2] = 
{
	U3CU3Ec_t12014549_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t12014549_StaticFields::get_offset_of_U3CU3E9__60_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (ErrorResponse_t3502566035), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[4] = 
{
	ErrorResponse_t3502566035::get_offset_of_type_0(),
	ErrorResponse_t3502566035::get_offset_of_code_1(),
	ErrorResponse_t3502566035::get_offset_of_message_2(),
	ErrorResponse_t3502566035::get_offset_of_requestId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (ParametersDictionaryFacade_t4039108328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[1] = 
{
	ParametersDictionaryFacade_t4039108328::get_offset_of__parameterCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (U3CGetEnumeratorU3Ed__23_t2866349317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[4] = 
{
	U3CGetEnumeratorU3Ed__23_t2866349317::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__23_t2866349317::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__23_t2866349317::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__23_t2866349317::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (RuntimePipelineCustomizerRegistry_t1556540342), -1, sizeof(RuntimePipelineCustomizerRegistry_t1556540342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2290[4] = 
{
	RuntimePipelineCustomizerRegistry_t1556540342_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_0(),
	RuntimePipelineCustomizerRegistry_t1556540342::get_offset_of__logger_1(),
	RuntimePipelineCustomizerRegistry_t1556540342::get_offset_of__rwlock_2(),
	RuntimePipelineCustomizerRegistry_t1556540342::get_offset_of__customizers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (StreamReadTracker_t1958363340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2292[6] = 
{
	StreamReadTracker_t1958363340::get_offset_of_sender_0(),
	StreamReadTracker_t1958363340::get_offset_of_callback_1(),
	StreamReadTracker_t1958363340::get_offset_of_contentLength_2(),
	StreamReadTracker_t1958363340::get_offset_of_totalBytesRead_3(),
	StreamReadTracker_t1958363340::get_offset_of_totalIncrementTransferred_4(),
	StreamReadTracker_t1958363340::get_offset_of_progressUpdateInterval_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (RequestContext_t2912551040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[11] = 
{
	RequestContext_t2912551040::get_offset_of_clientSigner_0(),
	RequestContext_t2912551040::get_offset_of_U3CRequestU3Ek__BackingField_1(),
	RequestContext_t2912551040::get_offset_of_U3CMetricsU3Ek__BackingField_2(),
	RequestContext_t2912551040::get_offset_of_U3CClientConfigU3Ek__BackingField_3(),
	RequestContext_t2912551040::get_offset_of_U3CRetriesU3Ek__BackingField_4(),
	RequestContext_t2912551040::get_offset_of_U3CIsSignedU3Ek__BackingField_5(),
	RequestContext_t2912551040::get_offset_of_U3CIsAsyncU3Ek__BackingField_6(),
	RequestContext_t2912551040::get_offset_of_U3COriginalRequestU3Ek__BackingField_7(),
	RequestContext_t2912551040::get_offset_of_U3CMarshallerU3Ek__BackingField_8(),
	RequestContext_t2912551040::get_offset_of_U3CUnmarshallerU3Ek__BackingField_9(),
	RequestContext_t2912551040::get_offset_of_U3CImmutableCredentialsU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (AsyncRequestContext_t3705567694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[4] = 
{
	AsyncRequestContext_t3705567694::get_offset_of_U3CCallbackU3Ek__BackingField_11(),
	AsyncRequestContext_t3705567694::get_offset_of_U3CStateU3Ek__BackingField_12(),
	AsyncRequestContext_t3705567694::get_offset_of_U3CAsyncOptionsU3Ek__BackingField_13(),
	AsyncRequestContext_t3705567694::get_offset_of_U3CActionU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (ResponseContext_t3850805926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[2] = 
{
	ResponseContext_t3850805926::get_offset_of_U3CResponseU3Ek__BackingField_0(),
	ResponseContext_t3850805926::get_offset_of_U3CHttpResponseU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (AsyncResponseContext_t210983900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[1] = 
{
	AsyncResponseContext_t210983900::get_offset_of_U3CAsyncResultU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (ExecutionContext_t2943675185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[2] = 
{
	ExecutionContext_t2943675185::get_offset_of_U3CRequestContextU3Ek__BackingField_0(),
	ExecutionContext_t2943675185::get_offset_of_U3CResponseContextU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (AsyncExecutionContext_t3009477291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[3] = 
{
	AsyncExecutionContext_t3009477291::get_offset_of_U3CResponseContextU3Ek__BackingField_0(),
	AsyncExecutionContext_t3009477291::get_offset_of_U3CRequestContextU3Ek__BackingField_1(),
	AsyncExecutionContext_t3009477291::get_offset_of_U3CRuntimeStateU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (HttpErrorResponseException_t3191555406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[1] = 
{
	HttpErrorResponseException_t3191555406::get_offset_of_U3CResponseU3Ek__BackingField_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
