﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t1578612233;
// Mono.Xml.Schema.XmlSchemaValidatingReader
struct XmlSchemaValidatingReader_t3861297856;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_t1548133672;
// Mono.Xml.Schema.XsdIdentityPath[]
struct XsdIdentityPathU5BU5D_t526781831;
// System.Collections.ArrayList
struct ArrayList_t4252133567;
// Mono.Xml.Schema.XsdIdentityField[]
struct XsdIdentityFieldU5BU5D_t3429900132;
// Mono.Xml.Schema.XsdInvalidValidationState
struct XsdInvalidValidationState_t1112885736;
// Mono.Xml.Schema.XsdParticleStateManager
struct XsdParticleStateManager_t4119977941;
// Mono.Xml.Schema.XsdIdentitySelector
struct XsdIdentitySelector_t185499482;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_t1058613623;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t1944712516;
// Mono.Xml.Schema.XsdKeyEntryCollection
struct XsdKeyEntryCollection_t1714053544;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.String
struct String_t;
// Mono.Xml.Schema.XsdValidationState
struct XsdValidationState_t3545965403;
// System.Collections.Stack
struct Stack_t1043988394;
// Mono.Xml.Schema.XsdKeyEntryFieldCollection
struct XsdKeyEntryFieldCollection_t2946985218;
// Mono.Xml.Schema.XsdKeyTable
struct XsdKeyTable_t2980902100;
// Mono.Xml.Schema.XsdIdentityStep[]
struct XsdIdentityStepU5BU5D_t1247465330;
// Mono.Xml.Schema.XsdKeyEntry
struct XsdKeyEntry_t3532015222;
// Mono.Xml.Schema.XsdIdentityField
struct XsdIdentityField_t2563516441;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t1096449895;
// Mono.Xml.Schema.XsdIdentityPath
struct XsdIdentityPath_t2037874;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_t4166282325;
// System.Xml.Schema.XmlSchemaChoice
struct XmlSchemaChoice_t654568461;
// System.Xml.Schema.XmlSchemaSequence
struct XmlSchemaSequence_t728414063;
// System.Xml.Schema.XmlSchemaAny
struct XmlSchemaAny_t3277730824;
// System.Xml.Schema.XmlSchemaAll
struct XmlSchemaAll_t1805755215;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t2433337156;
// System.Void
struct Void_t1841601450;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t135184468;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_t3616431661;
// UnityEngine.AudioExtensionDefinition
struct AudioExtensionDefinition_t1867593088;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// Mono.Xml.Schema.XsdString
struct XsdString_t263933896;
// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_t2132216291;
// Mono.Xml.Schema.XsdToken
struct XsdToken_t2902215462;
// Mono.Xml.Schema.XsdLanguage
struct XsdLanguage_t3897851897;
// Mono.Xml.Schema.XsdNMToken
struct XsdNMToken_t547135877;
// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_t1753890866;
// Mono.Xml.Schema.XsdName
struct XsdName_t1409945702;
// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t414304927;
// Mono.Xml.Schema.XsdID
struct XsdID_t1067193160;
// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_t1377311049;
// Mono.Xml.Schema.XsdIDRefs
struct XsdIDRefs_t763119546;
// Mono.Xml.Schema.XsdEntity
struct XsdEntity_t2664305022;
// Mono.Xml.Schema.XsdEntities
struct XsdEntities_t1103053540;
// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t720379093;
// Mono.Xml.Schema.XsdDecimal
struct XsdDecimal_t2932251550;
// Mono.Xml.Schema.XsdInteger
struct XsdInteger_t1330502157;
// Mono.Xml.Schema.XsdLong
struct XsdLong_t4179235589;
// Mono.Xml.Schema.XsdInt
struct XsdInt_t1488443894;
// Mono.Xml.Schema.XsdShort
struct XsdShort_t1778530041;
// Mono.Xml.Schema.XsdByte
struct XsdByte_t1120972221;
// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t3587933853;
// Mono.Xml.Schema.XsdPositiveInteger
struct XsdPositiveInteger_t1896481288;
// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t137890294;
// Mono.Xml.Schema.XsdUnsignedInt
struct XsdUnsignedInt_t2956447959;
// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_t3693774826;
// Mono.Xml.Schema.XsdUnsignedByte
struct XsdUnsignedByte_t3216355454;
// Mono.Xml.Schema.XsdNonPositiveInteger
struct XsdNonPositiveInteger_t409343009;
// Mono.Xml.Schema.XsdNegativeInteger
struct XsdNegativeInteger_t399444136;
// Mono.Xml.Schema.XsdFloat
struct XsdFloat_t386143221;
// Mono.Xml.Schema.XsdDouble
struct XsdDouble_t2510112208;
// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t1094704629;
// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_t4126353587;
// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t2527983239;
// Mono.Xml.Schema.XsdDuration
struct XsdDuration_t1605638443;
// Mono.Xml.Schema.XsdDateTime
struct XsdDateTime_t1344468684;
// Mono.Xml.Schema.XsdDate
struct XsdDate_t919459387;
// Mono.Xml.Schema.XsdTime
struct XsdTime_t4165680512;
// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t3496718151;
// Mono.Xml.Schema.XsdQName
struct XsdQName_t930779123;
// Mono.Xml.Schema.XsdGYearMonth
struct XsdGYearMonth_t1363357165;
// Mono.Xml.Schema.XsdGMonthDay
struct XsdGMonthDay_t3859978294;
// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t3810382607;
// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t4076673358;
// Mono.Xml.Schema.XsdGDay
struct XsdGDay_t1914244270;
// Mono.Xml.Schema.XdtAnyAtomicType
struct XdtAnyAtomicType_t3359210273;
// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t2904699188;
// Mono.Xml.Schema.XdtDayTimeDuration
struct XdtDayTimeDuration_t2797717973;
// Mono.Xml.Schema.XdtYearMonthDuration
struct XdtYearMonthDuration_t1764328599;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728;
// System.Xml.Schema.XmlSchemaAttribute[]
struct XmlSchemaAttributeU5BU5D_t3434391819;
// System.Xml.Schema.XmlSchemaValidator
struct XmlSchemaValidator_t1978690704;
// System.Xml.Schema.XmlValueGetter
struct XmlValueGetter_t1685472371;
// System.Xml.Schema.XmlSchemaInfo
struct XmlSchemaInfo_t2864028808;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t3928241465;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_t1795078578;
// UnityEngine.Font
struct Font_t4239498691;
// System.Action`1<UnityEngine.Font>
struct Action_1_t4041298073;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t1272078033;
// Mono.Xml.Schema.XsdValidationContext
struct XsdValidationContext_t3720679969;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_t2050913741;
// System.Collections.Specialized.StringCollection
struct StringCollection_t352985975;
// System.Xml.XmlResolver
struct XmlResolver_t2024571559;
// Mono.Xml.IHasXmlSchemaInfo
struct IHasXmlSchemaInfo_t3449605529;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t313318308;
// Mono.Xml.Schema.XsdIDManager
struct XsdIDManager_t3860002335;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t1580700381;
// UnityEngine.AudioListener
struct AudioListener_t1996719162;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t573379950;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t2425757932;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t2990399006;

struct UnityWebRequest_t254341728_marshaled_com;



#ifndef U3CMODULEU3E_T3783534221_H
#define U3CMODULEU3E_T3783534221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534221 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534221_H
#ifndef U3CMODULEU3E_T3783534220_H
#define U3CMODULEU3E_T3783534220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534220 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534220_H
#ifndef U3CMODULEU3E_T3783534223_H
#define U3CMODULEU3E_T3783534223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534223 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534223_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3783534222_H
#define U3CMODULEU3E_T3783534222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534222 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534222_H
#ifndef WWWTRANSCODER_T1124214756_H
#define WWWTRANSCODER_T1124214756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWWTranscoder
struct  WWWTranscoder_t1124214756  : public RuntimeObject
{
public:

public:
};

struct WWWTranscoder_t1124214756_StaticFields
{
public:
	// System.Byte[] UnityEngine.WWWTranscoder::ucHexChars
	ByteU5BU5D_t3397334013* ___ucHexChars_0;
	// System.Byte[] UnityEngine.WWWTranscoder::lcHexChars
	ByteU5BU5D_t3397334013* ___lcHexChars_1;
	// System.Byte UnityEngine.WWWTranscoder::urlEscapeChar
	uint8_t ___urlEscapeChar_2;
	// System.Byte UnityEngine.WWWTranscoder::urlSpace
	uint8_t ___urlSpace_3;
	// System.Byte[] UnityEngine.WWWTranscoder::urlForbidden
	ByteU5BU5D_t3397334013* ___urlForbidden_4;
	// System.Byte UnityEngine.WWWTranscoder::qpEscapeChar
	uint8_t ___qpEscapeChar_5;
	// System.Byte UnityEngine.WWWTranscoder::qpSpace
	uint8_t ___qpSpace_6;
	// System.Byte[] UnityEngine.WWWTranscoder::qpForbidden
	ByteU5BU5D_t3397334013* ___qpForbidden_7;

public:
	inline static int32_t get_offset_of_ucHexChars_0() { return static_cast<int32_t>(offsetof(WWWTranscoder_t1124214756_StaticFields, ___ucHexChars_0)); }
	inline ByteU5BU5D_t3397334013* get_ucHexChars_0() const { return ___ucHexChars_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_ucHexChars_0() { return &___ucHexChars_0; }
	inline void set_ucHexChars_0(ByteU5BU5D_t3397334013* value)
	{
		___ucHexChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___ucHexChars_0), value);
	}

	inline static int32_t get_offset_of_lcHexChars_1() { return static_cast<int32_t>(offsetof(WWWTranscoder_t1124214756_StaticFields, ___lcHexChars_1)); }
	inline ByteU5BU5D_t3397334013* get_lcHexChars_1() const { return ___lcHexChars_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_lcHexChars_1() { return &___lcHexChars_1; }
	inline void set_lcHexChars_1(ByteU5BU5D_t3397334013* value)
	{
		___lcHexChars_1 = value;
		Il2CppCodeGenWriteBarrier((&___lcHexChars_1), value);
	}

	inline static int32_t get_offset_of_urlEscapeChar_2() { return static_cast<int32_t>(offsetof(WWWTranscoder_t1124214756_StaticFields, ___urlEscapeChar_2)); }
	inline uint8_t get_urlEscapeChar_2() const { return ___urlEscapeChar_2; }
	inline uint8_t* get_address_of_urlEscapeChar_2() { return &___urlEscapeChar_2; }
	inline void set_urlEscapeChar_2(uint8_t value)
	{
		___urlEscapeChar_2 = value;
	}

	inline static int32_t get_offset_of_urlSpace_3() { return static_cast<int32_t>(offsetof(WWWTranscoder_t1124214756_StaticFields, ___urlSpace_3)); }
	inline uint8_t get_urlSpace_3() const { return ___urlSpace_3; }
	inline uint8_t* get_address_of_urlSpace_3() { return &___urlSpace_3; }
	inline void set_urlSpace_3(uint8_t value)
	{
		___urlSpace_3 = value;
	}

	inline static int32_t get_offset_of_urlForbidden_4() { return static_cast<int32_t>(offsetof(WWWTranscoder_t1124214756_StaticFields, ___urlForbidden_4)); }
	inline ByteU5BU5D_t3397334013* get_urlForbidden_4() const { return ___urlForbidden_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_urlForbidden_4() { return &___urlForbidden_4; }
	inline void set_urlForbidden_4(ByteU5BU5D_t3397334013* value)
	{
		___urlForbidden_4 = value;
		Il2CppCodeGenWriteBarrier((&___urlForbidden_4), value);
	}

	inline static int32_t get_offset_of_qpEscapeChar_5() { return static_cast<int32_t>(offsetof(WWWTranscoder_t1124214756_StaticFields, ___qpEscapeChar_5)); }
	inline uint8_t get_qpEscapeChar_5() const { return ___qpEscapeChar_5; }
	inline uint8_t* get_address_of_qpEscapeChar_5() { return &___qpEscapeChar_5; }
	inline void set_qpEscapeChar_5(uint8_t value)
	{
		___qpEscapeChar_5 = value;
	}

	inline static int32_t get_offset_of_qpSpace_6() { return static_cast<int32_t>(offsetof(WWWTranscoder_t1124214756_StaticFields, ___qpSpace_6)); }
	inline uint8_t get_qpSpace_6() const { return ___qpSpace_6; }
	inline uint8_t* get_address_of_qpSpace_6() { return &___qpSpace_6; }
	inline void set_qpSpace_6(uint8_t value)
	{
		___qpSpace_6 = value;
	}

	inline static int32_t get_offset_of_qpForbidden_7() { return static_cast<int32_t>(offsetof(WWWTranscoder_t1124214756_StaticFields, ___qpForbidden_7)); }
	inline ByteU5BU5D_t3397334013* get_qpForbidden_7() const { return ___qpForbidden_7; }
	inline ByteU5BU5D_t3397334013** get_address_of_qpForbidden_7() { return &___qpForbidden_7; }
	inline void set_qpForbidden_7(ByteU5BU5D_t3397334013* value)
	{
		___qpForbidden_7 = value;
		Il2CppCodeGenWriteBarrier((&___qpForbidden_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWTRANSCODER_T1124214756_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CXMLSCHEMAVALIDATINGREADERU3EC__ANONSTOREY4_T2009608413_H
#define U3CXMLSCHEMAVALIDATINGREADERU3EC__ANONSTOREY4_T2009608413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4
struct  U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t2009608413  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderSettings Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4::settings
	XmlReaderSettings_t1578612233 * ___settings_0;
	// Mono.Xml.Schema.XmlSchemaValidatingReader Mono.Xml.Schema.XmlSchemaValidatingReader/<XmlSchemaValidatingReader>c__AnonStorey4::<>f__this
	XmlSchemaValidatingReader_t3861297856 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t2009608413, ___settings_0)); }
	inline XmlReaderSettings_t1578612233 * get_settings_0() const { return ___settings_0; }
	inline XmlReaderSettings_t1578612233 ** get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(XmlReaderSettings_t1578612233 * value)
	{
		___settings_0 = value;
		Il2CppCodeGenWriteBarrier((&___settings_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t2009608413, ___U3CU3Ef__this_1)); }
	inline XmlSchemaValidatingReader_t3861297856 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline XmlSchemaValidatingReader_t3861297856 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(XmlSchemaValidatingReader_t3861297856 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CXMLSCHEMAVALIDATINGREADERU3EC__ANONSTOREY4_T2009608413_H
#ifndef WWWFORM_T3950226929_H
#define WWWFORM_T3950226929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWWForm
struct  WWWForm_t3950226929  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWFORM_T3950226929_H
#ifndef ATTRIBUTE_T542643598_H
#define ATTRIBUTE_T542643598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t542643598  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T542643598_H
#ifndef WEBREQUESTUTILS_T4100941042_H
#define WEBREQUESTUTILS_T4100941042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.WebRequestUtils
struct  WebRequestUtils_t4100941042  : public RuntimeObject
{
public:

public:
};

struct WebRequestUtils_t4100941042_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex UnityEngineInternal.WebRequestUtils::domainRegex
	Regex_t1803876613 * ___domainRegex_0;

public:
	inline static int32_t get_offset_of_domainRegex_0() { return static_cast<int32_t>(offsetof(WebRequestUtils_t4100941042_StaticFields, ___domainRegex_0)); }
	inline Regex_t1803876613 * get_domainRegex_0() const { return ___domainRegex_0; }
	inline Regex_t1803876613 ** get_address_of_domainRegex_0() { return &___domainRegex_0; }
	inline void set_domainRegex_0(Regex_t1803876613 * value)
	{
		___domainRegex_0 = value;
		Il2CppCodeGenWriteBarrier((&___domainRegex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTUTILS_T4100941042_H
#ifndef XMLREADER_T3675626668_H
#define XMLREADER_T3675626668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t3675626668  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_t1548133672 * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_t1578612233 * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_t3675626668, ___binary_0)); }
	inline XmlReaderBinarySupport_t1548133672 * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_t1548133672 ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_t1548133672 * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_t3675626668, ___settings_1)); }
	inline XmlReaderSettings_t1578612233 * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_t1578612233 ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_t1578612233 * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T3675626668_H
#ifndef XSDIDENTITYSELECTOR_T185499482_H
#define XSDIDENTITYSELECTOR_T185499482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentitySelector
struct  XsdIdentitySelector_t185499482  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityPath[] Mono.Xml.Schema.XsdIdentitySelector::selectorPaths
	XsdIdentityPathU5BU5D_t526781831* ___selectorPaths_0;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdIdentitySelector::fields
	ArrayList_t4252133567 * ___fields_1;
	// Mono.Xml.Schema.XsdIdentityField[] Mono.Xml.Schema.XsdIdentitySelector::cachedFields
	XsdIdentityFieldU5BU5D_t3429900132* ___cachedFields_2;

public:
	inline static int32_t get_offset_of_selectorPaths_0() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t185499482, ___selectorPaths_0)); }
	inline XsdIdentityPathU5BU5D_t526781831* get_selectorPaths_0() const { return ___selectorPaths_0; }
	inline XsdIdentityPathU5BU5D_t526781831** get_address_of_selectorPaths_0() { return &___selectorPaths_0; }
	inline void set_selectorPaths_0(XsdIdentityPathU5BU5D_t526781831* value)
	{
		___selectorPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___selectorPaths_0), value);
	}

	inline static int32_t get_offset_of_fields_1() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t185499482, ___fields_1)); }
	inline ArrayList_t4252133567 * get_fields_1() const { return ___fields_1; }
	inline ArrayList_t4252133567 ** get_address_of_fields_1() { return &___fields_1; }
	inline void set_fields_1(ArrayList_t4252133567 * value)
	{
		___fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___fields_1), value);
	}

	inline static int32_t get_offset_of_cachedFields_2() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t185499482, ___cachedFields_2)); }
	inline XsdIdentityFieldU5BU5D_t3429900132* get_cachedFields_2() const { return ___cachedFields_2; }
	inline XsdIdentityFieldU5BU5D_t3429900132** get_address_of_cachedFields_2() { return &___cachedFields_2; }
	inline void set_cachedFields_2(XsdIdentityFieldU5BU5D_t3429900132* value)
	{
		___cachedFields_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFields_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYSELECTOR_T185499482_H
#ifndef XSDVALIDATIONSTATE_T3545965403_H
#define XSDVALIDATIONSTATE_T3545965403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidationState
struct  XsdValidationState_t3545965403  : public RuntimeObject
{
public:
	// System.Int32 Mono.Xml.Schema.XsdValidationState::occured
	int32_t ___occured_1;
	// Mono.Xml.Schema.XsdParticleStateManager Mono.Xml.Schema.XsdValidationState::manager
	XsdParticleStateManager_t4119977941 * ___manager_2;

public:
	inline static int32_t get_offset_of_occured_1() { return static_cast<int32_t>(offsetof(XsdValidationState_t3545965403, ___occured_1)); }
	inline int32_t get_occured_1() const { return ___occured_1; }
	inline int32_t* get_address_of_occured_1() { return &___occured_1; }
	inline void set_occured_1(int32_t value)
	{
		___occured_1 = value;
	}

	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(XsdValidationState_t3545965403, ___manager_2)); }
	inline XsdParticleStateManager_t4119977941 * get_manager_2() const { return ___manager_2; }
	inline XsdParticleStateManager_t4119977941 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(XsdParticleStateManager_t4119977941 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}
};

struct XsdValidationState_t3545965403_StaticFields
{
public:
	// Mono.Xml.Schema.XsdInvalidValidationState Mono.Xml.Schema.XsdValidationState::invalid
	XsdInvalidValidationState_t1112885736 * ___invalid_0;

public:
	inline static int32_t get_offset_of_invalid_0() { return static_cast<int32_t>(offsetof(XsdValidationState_t3545965403_StaticFields, ___invalid_0)); }
	inline XsdInvalidValidationState_t1112885736 * get_invalid_0() const { return ___invalid_0; }
	inline XsdInvalidValidationState_t1112885736 ** get_address_of_invalid_0() { return &___invalid_0; }
	inline void set_invalid_0(XsdInvalidValidationState_t1112885736 * value)
	{
		___invalid_0 = value;
		Il2CppCodeGenWriteBarrier((&___invalid_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATIONSTATE_T3545965403_H
#ifndef XSDKEYTABLE_T2980902100_H
#define XSDKEYTABLE_T2980902100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyTable
struct  XsdKeyTable_t2980902100  : public RuntimeObject
{
public:
	// System.Boolean Mono.Xml.Schema.XsdKeyTable::alwaysTrue
	bool ___alwaysTrue_0;
	// Mono.Xml.Schema.XsdIdentitySelector Mono.Xml.Schema.XsdKeyTable::selector
	XsdIdentitySelector_t185499482 * ___selector_1;
	// System.Xml.Schema.XmlSchemaIdentityConstraint Mono.Xml.Schema.XsdKeyTable::source
	XmlSchemaIdentityConstraint_t1058613623 * ___source_2;
	// System.Xml.XmlQualifiedName Mono.Xml.Schema.XsdKeyTable::qname
	XmlQualifiedName_t1944712516 * ___qname_3;
	// System.Xml.XmlQualifiedName Mono.Xml.Schema.XsdKeyTable::refKeyName
	XmlQualifiedName_t1944712516 * ___refKeyName_4;
	// Mono.Xml.Schema.XsdKeyEntryCollection Mono.Xml.Schema.XsdKeyTable::Entries
	XsdKeyEntryCollection_t1714053544 * ___Entries_5;
	// Mono.Xml.Schema.XsdKeyEntryCollection Mono.Xml.Schema.XsdKeyTable::FinishedEntries
	XsdKeyEntryCollection_t1714053544 * ___FinishedEntries_6;
	// System.Int32 Mono.Xml.Schema.XsdKeyTable::StartDepth
	int32_t ___StartDepth_7;
	// Mono.Xml.Schema.XsdKeyTable Mono.Xml.Schema.XsdKeyTable::ReferencedKey
	XsdKeyTable_t2980902100 * ___ReferencedKey_8;

public:
	inline static int32_t get_offset_of_alwaysTrue_0() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2980902100, ___alwaysTrue_0)); }
	inline bool get_alwaysTrue_0() const { return ___alwaysTrue_0; }
	inline bool* get_address_of_alwaysTrue_0() { return &___alwaysTrue_0; }
	inline void set_alwaysTrue_0(bool value)
	{
		___alwaysTrue_0 = value;
	}

	inline static int32_t get_offset_of_selector_1() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2980902100, ___selector_1)); }
	inline XsdIdentitySelector_t185499482 * get_selector_1() const { return ___selector_1; }
	inline XsdIdentitySelector_t185499482 ** get_address_of_selector_1() { return &___selector_1; }
	inline void set_selector_1(XsdIdentitySelector_t185499482 * value)
	{
		___selector_1 = value;
		Il2CppCodeGenWriteBarrier((&___selector_1), value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2980902100, ___source_2)); }
	inline XmlSchemaIdentityConstraint_t1058613623 * get_source_2() const { return ___source_2; }
	inline XmlSchemaIdentityConstraint_t1058613623 ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(XmlSchemaIdentityConstraint_t1058613623 * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_qname_3() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2980902100, ___qname_3)); }
	inline XmlQualifiedName_t1944712516 * get_qname_3() const { return ___qname_3; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_qname_3() { return &___qname_3; }
	inline void set_qname_3(XmlQualifiedName_t1944712516 * value)
	{
		___qname_3 = value;
		Il2CppCodeGenWriteBarrier((&___qname_3), value);
	}

	inline static int32_t get_offset_of_refKeyName_4() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2980902100, ___refKeyName_4)); }
	inline XmlQualifiedName_t1944712516 * get_refKeyName_4() const { return ___refKeyName_4; }
	inline XmlQualifiedName_t1944712516 ** get_address_of_refKeyName_4() { return &___refKeyName_4; }
	inline void set_refKeyName_4(XmlQualifiedName_t1944712516 * value)
	{
		___refKeyName_4 = value;
		Il2CppCodeGenWriteBarrier((&___refKeyName_4), value);
	}

	inline static int32_t get_offset_of_Entries_5() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2980902100, ___Entries_5)); }
	inline XsdKeyEntryCollection_t1714053544 * get_Entries_5() const { return ___Entries_5; }
	inline XsdKeyEntryCollection_t1714053544 ** get_address_of_Entries_5() { return &___Entries_5; }
	inline void set_Entries_5(XsdKeyEntryCollection_t1714053544 * value)
	{
		___Entries_5 = value;
		Il2CppCodeGenWriteBarrier((&___Entries_5), value);
	}

	inline static int32_t get_offset_of_FinishedEntries_6() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2980902100, ___FinishedEntries_6)); }
	inline XsdKeyEntryCollection_t1714053544 * get_FinishedEntries_6() const { return ___FinishedEntries_6; }
	inline XsdKeyEntryCollection_t1714053544 ** get_address_of_FinishedEntries_6() { return &___FinishedEntries_6; }
	inline void set_FinishedEntries_6(XsdKeyEntryCollection_t1714053544 * value)
	{
		___FinishedEntries_6 = value;
		Il2CppCodeGenWriteBarrier((&___FinishedEntries_6), value);
	}

	inline static int32_t get_offset_of_StartDepth_7() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2980902100, ___StartDepth_7)); }
	inline int32_t get_StartDepth_7() const { return ___StartDepth_7; }
	inline int32_t* get_address_of_StartDepth_7() { return &___StartDepth_7; }
	inline void set_StartDepth_7(int32_t value)
	{
		___StartDepth_7 = value;
	}

	inline static int32_t get_offset_of_ReferencedKey_8() { return static_cast<int32_t>(offsetof(XsdKeyTable_t2980902100, ___ReferencedKey_8)); }
	inline XsdKeyTable_t2980902100 * get_ReferencedKey_8() const { return ___ReferencedKey_8; }
	inline XsdKeyTable_t2980902100 ** get_address_of_ReferencedKey_8() { return &___ReferencedKey_8; }
	inline void set_ReferencedKey_8(XsdKeyTable_t2980902100 * value)
	{
		___ReferencedKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedKey_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYTABLE_T2980902100_H
#ifndef XSDIDMANAGER_T3860002335_H
#define XSDIDMANAGER_T3860002335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDManager
struct  XsdIDManager_t3860002335  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Schema.XsdIDManager::idList
	Hashtable_t909839986 * ___idList_0;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdIDManager::missingIDReferences
	ArrayList_t4252133567 * ___missingIDReferences_1;
	// System.String Mono.Xml.Schema.XsdIDManager::thisElementId
	String_t* ___thisElementId_2;

public:
	inline static int32_t get_offset_of_idList_0() { return static_cast<int32_t>(offsetof(XsdIDManager_t3860002335, ___idList_0)); }
	inline Hashtable_t909839986 * get_idList_0() const { return ___idList_0; }
	inline Hashtable_t909839986 ** get_address_of_idList_0() { return &___idList_0; }
	inline void set_idList_0(Hashtable_t909839986 * value)
	{
		___idList_0 = value;
		Il2CppCodeGenWriteBarrier((&___idList_0), value);
	}

	inline static int32_t get_offset_of_missingIDReferences_1() { return static_cast<int32_t>(offsetof(XsdIDManager_t3860002335, ___missingIDReferences_1)); }
	inline ArrayList_t4252133567 * get_missingIDReferences_1() const { return ___missingIDReferences_1; }
	inline ArrayList_t4252133567 ** get_address_of_missingIDReferences_1() { return &___missingIDReferences_1; }
	inline void set_missingIDReferences_1(ArrayList_t4252133567 * value)
	{
		___missingIDReferences_1 = value;
		Il2CppCodeGenWriteBarrier((&___missingIDReferences_1), value);
	}

	inline static int32_t get_offset_of_thisElementId_2() { return static_cast<int32_t>(offsetof(XsdIDManager_t3860002335, ___thisElementId_2)); }
	inline String_t* get_thisElementId_2() const { return ___thisElementId_2; }
	inline String_t** get_address_of_thisElementId_2() { return &___thisElementId_2; }
	inline void set_thisElementId_2(String_t* value)
	{
		___thisElementId_2 = value;
		Il2CppCodeGenWriteBarrier((&___thisElementId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDMANAGER_T3860002335_H
#ifndef XSDVALIDATIONCONTEXT_T3720679969_H
#define XSDVALIDATIONCONTEXT_T3720679969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidationContext
struct  XsdValidationContext_t3720679969  : public RuntimeObject
{
public:
	// System.Object Mono.Xml.Schema.XsdValidationContext::xsi_type
	RuntimeObject * ___xsi_type_0;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdValidationContext::State
	XsdValidationState_t3545965403 * ___State_1;
	// System.Collections.Stack Mono.Xml.Schema.XsdValidationContext::element_stack
	Stack_t1043988394 * ___element_stack_2;

public:
	inline static int32_t get_offset_of_xsi_type_0() { return static_cast<int32_t>(offsetof(XsdValidationContext_t3720679969, ___xsi_type_0)); }
	inline RuntimeObject * get_xsi_type_0() const { return ___xsi_type_0; }
	inline RuntimeObject ** get_address_of_xsi_type_0() { return &___xsi_type_0; }
	inline void set_xsi_type_0(RuntimeObject * value)
	{
		___xsi_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___xsi_type_0), value);
	}

	inline static int32_t get_offset_of_State_1() { return static_cast<int32_t>(offsetof(XsdValidationContext_t3720679969, ___State_1)); }
	inline XsdValidationState_t3545965403 * get_State_1() const { return ___State_1; }
	inline XsdValidationState_t3545965403 ** get_address_of_State_1() { return &___State_1; }
	inline void set_State_1(XsdValidationState_t3545965403 * value)
	{
		___State_1 = value;
		Il2CppCodeGenWriteBarrier((&___State_1), value);
	}

	inline static int32_t get_offset_of_element_stack_2() { return static_cast<int32_t>(offsetof(XsdValidationContext_t3720679969, ___element_stack_2)); }
	inline Stack_t1043988394 * get_element_stack_2() const { return ___element_stack_2; }
	inline Stack_t1043988394 ** get_address_of_element_stack_2() { return &___element_stack_2; }
	inline void set_element_stack_2(Stack_t1043988394 * value)
	{
		___element_stack_2 = value;
		Il2CppCodeGenWriteBarrier((&___element_stack_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATIONCONTEXT_T3720679969_H
#ifndef XSDKEYENTRY_T3532015222_H
#define XSDKEYENTRY_T3532015222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntry
struct  XsdKeyEntry_t3532015222  : public RuntimeObject
{
public:
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::StartDepth
	int32_t ___StartDepth_0;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::SelectorLineNumber
	int32_t ___SelectorLineNumber_1;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::SelectorLinePosition
	int32_t ___SelectorLinePosition_2;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::SelectorHasLineInfo
	bool ___SelectorHasLineInfo_3;
	// Mono.Xml.Schema.XsdKeyEntryFieldCollection Mono.Xml.Schema.XsdKeyEntry::KeyFields
	XsdKeyEntryFieldCollection_t2946985218 * ___KeyFields_4;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::KeyRefFound
	bool ___KeyRefFound_5;
	// Mono.Xml.Schema.XsdKeyTable Mono.Xml.Schema.XsdKeyEntry::OwnerSequence
	XsdKeyTable_t2980902100 * ___OwnerSequence_6;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::keyFound
	bool ___keyFound_7;

public:
	inline static int32_t get_offset_of_StartDepth_0() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t3532015222, ___StartDepth_0)); }
	inline int32_t get_StartDepth_0() const { return ___StartDepth_0; }
	inline int32_t* get_address_of_StartDepth_0() { return &___StartDepth_0; }
	inline void set_StartDepth_0(int32_t value)
	{
		___StartDepth_0 = value;
	}

	inline static int32_t get_offset_of_SelectorLineNumber_1() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t3532015222, ___SelectorLineNumber_1)); }
	inline int32_t get_SelectorLineNumber_1() const { return ___SelectorLineNumber_1; }
	inline int32_t* get_address_of_SelectorLineNumber_1() { return &___SelectorLineNumber_1; }
	inline void set_SelectorLineNumber_1(int32_t value)
	{
		___SelectorLineNumber_1 = value;
	}

	inline static int32_t get_offset_of_SelectorLinePosition_2() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t3532015222, ___SelectorLinePosition_2)); }
	inline int32_t get_SelectorLinePosition_2() const { return ___SelectorLinePosition_2; }
	inline int32_t* get_address_of_SelectorLinePosition_2() { return &___SelectorLinePosition_2; }
	inline void set_SelectorLinePosition_2(int32_t value)
	{
		___SelectorLinePosition_2 = value;
	}

	inline static int32_t get_offset_of_SelectorHasLineInfo_3() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t3532015222, ___SelectorHasLineInfo_3)); }
	inline bool get_SelectorHasLineInfo_3() const { return ___SelectorHasLineInfo_3; }
	inline bool* get_address_of_SelectorHasLineInfo_3() { return &___SelectorHasLineInfo_3; }
	inline void set_SelectorHasLineInfo_3(bool value)
	{
		___SelectorHasLineInfo_3 = value;
	}

	inline static int32_t get_offset_of_KeyFields_4() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t3532015222, ___KeyFields_4)); }
	inline XsdKeyEntryFieldCollection_t2946985218 * get_KeyFields_4() const { return ___KeyFields_4; }
	inline XsdKeyEntryFieldCollection_t2946985218 ** get_address_of_KeyFields_4() { return &___KeyFields_4; }
	inline void set_KeyFields_4(XsdKeyEntryFieldCollection_t2946985218 * value)
	{
		___KeyFields_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyFields_4), value);
	}

	inline static int32_t get_offset_of_KeyRefFound_5() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t3532015222, ___KeyRefFound_5)); }
	inline bool get_KeyRefFound_5() const { return ___KeyRefFound_5; }
	inline bool* get_address_of_KeyRefFound_5() { return &___KeyRefFound_5; }
	inline void set_KeyRefFound_5(bool value)
	{
		___KeyRefFound_5 = value;
	}

	inline static int32_t get_offset_of_OwnerSequence_6() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t3532015222, ___OwnerSequence_6)); }
	inline XsdKeyTable_t2980902100 * get_OwnerSequence_6() const { return ___OwnerSequence_6; }
	inline XsdKeyTable_t2980902100 ** get_address_of_OwnerSequence_6() { return &___OwnerSequence_6; }
	inline void set_OwnerSequence_6(XsdKeyTable_t2980902100 * value)
	{
		___OwnerSequence_6 = value;
		Il2CppCodeGenWriteBarrier((&___OwnerSequence_6), value);
	}

	inline static int32_t get_offset_of_keyFound_7() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t3532015222, ___keyFound_7)); }
	inline bool get_keyFound_7() const { return ___keyFound_7; }
	inline bool* get_address_of_keyFound_7() { return &___keyFound_7; }
	inline void set_keyFound_7(bool value)
	{
		___keyFound_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRY_T3532015222_H
#ifndef XSDIDENTITYPATH_T2037874_H
#define XSDIDENTITYPATH_T2037874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityPath
struct  XsdIdentityPath_t2037874  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityStep[] Mono.Xml.Schema.XsdIdentityPath::OrderedSteps
	XsdIdentityStepU5BU5D_t1247465330* ___OrderedSteps_0;
	// System.Boolean Mono.Xml.Schema.XsdIdentityPath::Descendants
	bool ___Descendants_1;

public:
	inline static int32_t get_offset_of_OrderedSteps_0() { return static_cast<int32_t>(offsetof(XsdIdentityPath_t2037874, ___OrderedSteps_0)); }
	inline XsdIdentityStepU5BU5D_t1247465330* get_OrderedSteps_0() const { return ___OrderedSteps_0; }
	inline XsdIdentityStepU5BU5D_t1247465330** get_address_of_OrderedSteps_0() { return &___OrderedSteps_0; }
	inline void set_OrderedSteps_0(XsdIdentityStepU5BU5D_t1247465330* value)
	{
		___OrderedSteps_0 = value;
		Il2CppCodeGenWriteBarrier((&___OrderedSteps_0), value);
	}

	inline static int32_t get_offset_of_Descendants_1() { return static_cast<int32_t>(offsetof(XsdIdentityPath_t2037874, ___Descendants_1)); }
	inline bool get_Descendants_1() const { return ___Descendants_1; }
	inline bool* get_address_of_Descendants_1() { return &___Descendants_1; }
	inline void set_Descendants_1(bool value)
	{
		___Descendants_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYPATH_T2037874_H
#ifndef XSDIDENTITYFIELD_T2563516441_H
#define XSDIDENTITYFIELD_T2563516441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityField
struct  XsdIdentityField_t2563516441  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityPath[] Mono.Xml.Schema.XsdIdentityField::fieldPaths
	XsdIdentityPathU5BU5D_t526781831* ___fieldPaths_0;
	// System.Int32 Mono.Xml.Schema.XsdIdentityField::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_fieldPaths_0() { return static_cast<int32_t>(offsetof(XsdIdentityField_t2563516441, ___fieldPaths_0)); }
	inline XsdIdentityPathU5BU5D_t526781831* get_fieldPaths_0() const { return ___fieldPaths_0; }
	inline XsdIdentityPathU5BU5D_t526781831** get_address_of_fieldPaths_0() { return &___fieldPaths_0; }
	inline void set_fieldPaths_0(XsdIdentityPathU5BU5D_t526781831* value)
	{
		___fieldPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldPaths_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(XsdIdentityField_t2563516441, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYFIELD_T2563516441_H
#ifndef XSDKEYENTRYFIELD_T3632833996_H
#define XSDKEYENTRYFIELD_T3632833996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryField
struct  XsdKeyEntryField_t3632833996  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdKeyEntry Mono.Xml.Schema.XsdKeyEntryField::entry
	XsdKeyEntry_t3532015222 * ___entry_0;
	// Mono.Xml.Schema.XsdIdentityField Mono.Xml.Schema.XsdKeyEntryField::field
	XsdIdentityField_t2563516441 * ___field_1;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::FieldFound
	bool ___FieldFound_2;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldLineNumber
	int32_t ___FieldLineNumber_3;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldLinePosition
	int32_t ___FieldLinePosition_4;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::FieldHasLineInfo
	bool ___FieldHasLineInfo_5;
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdKeyEntryField::FieldType
	XsdAnySimpleType_t1096449895 * ___FieldType_6;
	// System.Object Mono.Xml.Schema.XsdKeyEntryField::Identity
	RuntimeObject * ___Identity_7;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::IsXsiNil
	bool ___IsXsiNil_8;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldFoundDepth
	int32_t ___FieldFoundDepth_9;
	// Mono.Xml.Schema.XsdIdentityPath Mono.Xml.Schema.XsdKeyEntryField::FieldFoundPath
	XsdIdentityPath_t2037874 * ___FieldFoundPath_10;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::Consuming
	bool ___Consuming_11;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::Consumed
	bool ___Consumed_12;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___entry_0)); }
	inline XsdKeyEntry_t3532015222 * get_entry_0() const { return ___entry_0; }
	inline XsdKeyEntry_t3532015222 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(XsdKeyEntry_t3532015222 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_field_1() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___field_1)); }
	inline XsdIdentityField_t2563516441 * get_field_1() const { return ___field_1; }
	inline XsdIdentityField_t2563516441 ** get_address_of_field_1() { return &___field_1; }
	inline void set_field_1(XsdIdentityField_t2563516441 * value)
	{
		___field_1 = value;
		Il2CppCodeGenWriteBarrier((&___field_1), value);
	}

	inline static int32_t get_offset_of_FieldFound_2() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___FieldFound_2)); }
	inline bool get_FieldFound_2() const { return ___FieldFound_2; }
	inline bool* get_address_of_FieldFound_2() { return &___FieldFound_2; }
	inline void set_FieldFound_2(bool value)
	{
		___FieldFound_2 = value;
	}

	inline static int32_t get_offset_of_FieldLineNumber_3() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___FieldLineNumber_3)); }
	inline int32_t get_FieldLineNumber_3() const { return ___FieldLineNumber_3; }
	inline int32_t* get_address_of_FieldLineNumber_3() { return &___FieldLineNumber_3; }
	inline void set_FieldLineNumber_3(int32_t value)
	{
		___FieldLineNumber_3 = value;
	}

	inline static int32_t get_offset_of_FieldLinePosition_4() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___FieldLinePosition_4)); }
	inline int32_t get_FieldLinePosition_4() const { return ___FieldLinePosition_4; }
	inline int32_t* get_address_of_FieldLinePosition_4() { return &___FieldLinePosition_4; }
	inline void set_FieldLinePosition_4(int32_t value)
	{
		___FieldLinePosition_4 = value;
	}

	inline static int32_t get_offset_of_FieldHasLineInfo_5() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___FieldHasLineInfo_5)); }
	inline bool get_FieldHasLineInfo_5() const { return ___FieldHasLineInfo_5; }
	inline bool* get_address_of_FieldHasLineInfo_5() { return &___FieldHasLineInfo_5; }
	inline void set_FieldHasLineInfo_5(bool value)
	{
		___FieldHasLineInfo_5 = value;
	}

	inline static int32_t get_offset_of_FieldType_6() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___FieldType_6)); }
	inline XsdAnySimpleType_t1096449895 * get_FieldType_6() const { return ___FieldType_6; }
	inline XsdAnySimpleType_t1096449895 ** get_address_of_FieldType_6() { return &___FieldType_6; }
	inline void set_FieldType_6(XsdAnySimpleType_t1096449895 * value)
	{
		___FieldType_6 = value;
		Il2CppCodeGenWriteBarrier((&___FieldType_6), value);
	}

	inline static int32_t get_offset_of_Identity_7() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___Identity_7)); }
	inline RuntimeObject * get_Identity_7() const { return ___Identity_7; }
	inline RuntimeObject ** get_address_of_Identity_7() { return &___Identity_7; }
	inline void set_Identity_7(RuntimeObject * value)
	{
		___Identity_7 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_7), value);
	}

	inline static int32_t get_offset_of_IsXsiNil_8() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___IsXsiNil_8)); }
	inline bool get_IsXsiNil_8() const { return ___IsXsiNil_8; }
	inline bool* get_address_of_IsXsiNil_8() { return &___IsXsiNil_8; }
	inline void set_IsXsiNil_8(bool value)
	{
		___IsXsiNil_8 = value;
	}

	inline static int32_t get_offset_of_FieldFoundDepth_9() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___FieldFoundDepth_9)); }
	inline int32_t get_FieldFoundDepth_9() const { return ___FieldFoundDepth_9; }
	inline int32_t* get_address_of_FieldFoundDepth_9() { return &___FieldFoundDepth_9; }
	inline void set_FieldFoundDepth_9(int32_t value)
	{
		___FieldFoundDepth_9 = value;
	}

	inline static int32_t get_offset_of_FieldFoundPath_10() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___FieldFoundPath_10)); }
	inline XsdIdentityPath_t2037874 * get_FieldFoundPath_10() const { return ___FieldFoundPath_10; }
	inline XsdIdentityPath_t2037874 ** get_address_of_FieldFoundPath_10() { return &___FieldFoundPath_10; }
	inline void set_FieldFoundPath_10(XsdIdentityPath_t2037874 * value)
	{
		___FieldFoundPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___FieldFoundPath_10), value);
	}

	inline static int32_t get_offset_of_Consuming_11() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___Consuming_11)); }
	inline bool get_Consuming_11() const { return ___Consuming_11; }
	inline bool* get_address_of_Consuming_11() { return &___Consuming_11; }
	inline void set_Consuming_11(bool value)
	{
		___Consuming_11 = value;
	}

	inline static int32_t get_offset_of_Consumed_12() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t3632833996, ___Consumed_12)); }
	inline bool get_Consumed_12() const { return ___Consumed_12; }
	inline bool* get_address_of_Consumed_12() { return &___Consumed_12; }
	inline void set_Consumed_12(bool value)
	{
		___Consumed_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYFIELD_T3632833996_H
#ifndef XSDIDENTITYSTEP_T452377251_H
#define XSDIDENTITYSTEP_T452377251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityStep
struct  XsdIdentityStep_t452377251  : public RuntimeObject
{
public:
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsCurrent
	bool ___IsCurrent_0;
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsAttribute
	bool ___IsAttribute_1;
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsAnyName
	bool ___IsAnyName_2;
	// System.String Mono.Xml.Schema.XsdIdentityStep::NsName
	String_t* ___NsName_3;
	// System.String Mono.Xml.Schema.XsdIdentityStep::Name
	String_t* ___Name_4;
	// System.String Mono.Xml.Schema.XsdIdentityStep::Namespace
	String_t* ___Namespace_5;

public:
	inline static int32_t get_offset_of_IsCurrent_0() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t452377251, ___IsCurrent_0)); }
	inline bool get_IsCurrent_0() const { return ___IsCurrent_0; }
	inline bool* get_address_of_IsCurrent_0() { return &___IsCurrent_0; }
	inline void set_IsCurrent_0(bool value)
	{
		___IsCurrent_0 = value;
	}

	inline static int32_t get_offset_of_IsAttribute_1() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t452377251, ___IsAttribute_1)); }
	inline bool get_IsAttribute_1() const { return ___IsAttribute_1; }
	inline bool* get_address_of_IsAttribute_1() { return &___IsAttribute_1; }
	inline void set_IsAttribute_1(bool value)
	{
		___IsAttribute_1 = value;
	}

	inline static int32_t get_offset_of_IsAnyName_2() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t452377251, ___IsAnyName_2)); }
	inline bool get_IsAnyName_2() const { return ___IsAnyName_2; }
	inline bool* get_address_of_IsAnyName_2() { return &___IsAnyName_2; }
	inline void set_IsAnyName_2(bool value)
	{
		___IsAnyName_2 = value;
	}

	inline static int32_t get_offset_of_NsName_3() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t452377251, ___NsName_3)); }
	inline String_t* get_NsName_3() const { return ___NsName_3; }
	inline String_t** get_address_of_NsName_3() { return &___NsName_3; }
	inline void set_NsName_3(String_t* value)
	{
		___NsName_3 = value;
		Il2CppCodeGenWriteBarrier((&___NsName_3), value);
	}

	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t452377251, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}

	inline static int32_t get_offset_of_Namespace_5() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t452377251, ___Namespace_5)); }
	inline String_t* get_Namespace_5() const { return ___Namespace_5; }
	inline String_t** get_address_of_Namespace_5() { return &___Namespace_5; }
	inline void set_Namespace_5(String_t* value)
	{
		___Namespace_5 = value;
		Il2CppCodeGenWriteBarrier((&___Namespace_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYSTEP_T452377251_H
#ifndef AUDIOEXTENSIONDEFINITION_T1867593088_H
#define AUDIOEXTENSIONDEFINITION_T1867593088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioExtensionDefinition
struct  AudioExtensionDefinition_t1867593088  : public RuntimeObject
{
public:
	// System.String UnityEngine.AudioExtensionDefinition::assemblyName
	String_t* ___assemblyName_0;
	// System.String UnityEngine.AudioExtensionDefinition::extensionNamespace
	String_t* ___extensionNamespace_1;
	// System.String UnityEngine.AudioExtensionDefinition::extensionTypeName
	String_t* ___extensionTypeName_2;
	// System.Type UnityEngine.AudioExtensionDefinition::extensionType
	Type_t * ___extensionType_3;

public:
	inline static int32_t get_offset_of_assemblyName_0() { return static_cast<int32_t>(offsetof(AudioExtensionDefinition_t1867593088, ___assemblyName_0)); }
	inline String_t* get_assemblyName_0() const { return ___assemblyName_0; }
	inline String_t** get_address_of_assemblyName_0() { return &___assemblyName_0; }
	inline void set_assemblyName_0(String_t* value)
	{
		___assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_0), value);
	}

	inline static int32_t get_offset_of_extensionNamespace_1() { return static_cast<int32_t>(offsetof(AudioExtensionDefinition_t1867593088, ___extensionNamespace_1)); }
	inline String_t* get_extensionNamespace_1() const { return ___extensionNamespace_1; }
	inline String_t** get_address_of_extensionNamespace_1() { return &___extensionNamespace_1; }
	inline void set_extensionNamespace_1(String_t* value)
	{
		___extensionNamespace_1 = value;
		Il2CppCodeGenWriteBarrier((&___extensionNamespace_1), value);
	}

	inline static int32_t get_offset_of_extensionTypeName_2() { return static_cast<int32_t>(offsetof(AudioExtensionDefinition_t1867593088, ___extensionTypeName_2)); }
	inline String_t* get_extensionTypeName_2() const { return ___extensionTypeName_2; }
	inline String_t** get_address_of_extensionTypeName_2() { return &___extensionTypeName_2; }
	inline void set_extensionTypeName_2(String_t* value)
	{
		___extensionTypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensionTypeName_2), value);
	}

	inline static int32_t get_offset_of_extensionType_3() { return static_cast<int32_t>(offsetof(AudioExtensionDefinition_t1867593088, ___extensionType_3)); }
	inline Type_t * get_extensionType_3() const { return ___extensionType_3; }
	inline Type_t ** get_address_of_extensionType_3() { return &___extensionType_3; }
	inline void set_extensionType_3(Type_t * value)
	{
		___extensionType_3 = value;
		Il2CppCodeGenWriteBarrier((&___extensionType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOEXTENSIONDEFINITION_T1867593088_H
#ifndef PHYSICS2D_T2540166467_H
#define PHYSICS2D_T2540166467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Physics2D
struct  Physics2D_t2540166467  : public RuntimeObject
{
public:

public:
};

struct Physics2D_t2540166467_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D> UnityEngine.Physics2D::m_LastDisabledRigidbody2D
	List_1_t4166282325 * ___m_LastDisabledRigidbody2D_0;

public:
	inline static int32_t get_offset_of_m_LastDisabledRigidbody2D_0() { return static_cast<int32_t>(offsetof(Physics2D_t2540166467_StaticFields, ___m_LastDisabledRigidbody2D_0)); }
	inline List_1_t4166282325 * get_m_LastDisabledRigidbody2D_0() const { return ___m_LastDisabledRigidbody2D_0; }
	inline List_1_t4166282325 ** get_address_of_m_LastDisabledRigidbody2D_0() { return &___m_LastDisabledRigidbody2D_0; }
	inline void set_m_LastDisabledRigidbody2D_0(List_1_t4166282325 * value)
	{
		___m_LastDisabledRigidbody2D_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastDisabledRigidbody2D_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PHYSICS2D_T2540166467_H
#ifndef COLLECTIONBASE_T1101587467_H
#define COLLECTIONBASE_T1101587467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_t1101587467  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t4252133567 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_t1101587467, ___list_0)); }
	inline ArrayList_t4252133567 * get_list_0() const { return ___list_0; }
	inline ArrayList_t4252133567 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t4252133567 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_T1101587467_H
#ifndef YIELDINSTRUCTION_T3462875981_H
#define YIELDINSTRUCTION_T3462875981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3462875981  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3462875981_H
#ifndef XSDCHOICEVALIDATIONSTATE_T1506407122_H
#define XSDCHOICEVALIDATIONSTATE_T1506407122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdChoiceValidationState
struct  XsdChoiceValidationState_t1506407122  : public XsdValidationState_t3545965403
{
public:
	// System.Xml.Schema.XmlSchemaChoice Mono.Xml.Schema.XsdChoiceValidationState::choice
	XmlSchemaChoice_t654568461 * ___choice_3;
	// System.Boolean Mono.Xml.Schema.XsdChoiceValidationState::emptiable
	bool ___emptiable_4;
	// System.Boolean Mono.Xml.Schema.XsdChoiceValidationState::emptiableComputed
	bool ___emptiableComputed_5;

public:
	inline static int32_t get_offset_of_choice_3() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t1506407122, ___choice_3)); }
	inline XmlSchemaChoice_t654568461 * get_choice_3() const { return ___choice_3; }
	inline XmlSchemaChoice_t654568461 ** get_address_of_choice_3() { return &___choice_3; }
	inline void set_choice_3(XmlSchemaChoice_t654568461 * value)
	{
		___choice_3 = value;
		Il2CppCodeGenWriteBarrier((&___choice_3), value);
	}

	inline static int32_t get_offset_of_emptiable_4() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t1506407122, ___emptiable_4)); }
	inline bool get_emptiable_4() const { return ___emptiable_4; }
	inline bool* get_address_of_emptiable_4() { return &___emptiable_4; }
	inline void set_emptiable_4(bool value)
	{
		___emptiable_4 = value;
	}

	inline static int32_t get_offset_of_emptiableComputed_5() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t1506407122, ___emptiableComputed_5)); }
	inline bool get_emptiableComputed_5() const { return ___emptiableComputed_5; }
	inline bool* get_address_of_emptiableComputed_5() { return &___emptiableComputed_5; }
	inline void set_emptiableComputed_5(bool value)
	{
		___emptiableComputed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDCHOICEVALIDATIONSTATE_T1506407122_H
#ifndef XSDSEQUENCEVALIDATIONSTATE_T3542030006_H
#define XSDSEQUENCEVALIDATIONSTATE_T3542030006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdSequenceValidationState
struct  XsdSequenceValidationState_t3542030006  : public XsdValidationState_t3545965403
{
public:
	// System.Xml.Schema.XmlSchemaSequence Mono.Xml.Schema.XsdSequenceValidationState::seq
	XmlSchemaSequence_t728414063 * ___seq_3;
	// System.Int32 Mono.Xml.Schema.XsdSequenceValidationState::current
	int32_t ___current_4;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdSequenceValidationState::currentAutomata
	XsdValidationState_t3545965403 * ___currentAutomata_5;
	// System.Boolean Mono.Xml.Schema.XsdSequenceValidationState::emptiable
	bool ___emptiable_6;

public:
	inline static int32_t get_offset_of_seq_3() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_t3542030006, ___seq_3)); }
	inline XmlSchemaSequence_t728414063 * get_seq_3() const { return ___seq_3; }
	inline XmlSchemaSequence_t728414063 ** get_address_of_seq_3() { return &___seq_3; }
	inline void set_seq_3(XmlSchemaSequence_t728414063 * value)
	{
		___seq_3 = value;
		Il2CppCodeGenWriteBarrier((&___seq_3), value);
	}

	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_t3542030006, ___current_4)); }
	inline int32_t get_current_4() const { return ___current_4; }
	inline int32_t* get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(int32_t value)
	{
		___current_4 = value;
	}

	inline static int32_t get_offset_of_currentAutomata_5() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_t3542030006, ___currentAutomata_5)); }
	inline XsdValidationState_t3545965403 * get_currentAutomata_5() const { return ___currentAutomata_5; }
	inline XsdValidationState_t3545965403 ** get_address_of_currentAutomata_5() { return &___currentAutomata_5; }
	inline void set_currentAutomata_5(XsdValidationState_t3545965403 * value)
	{
		___currentAutomata_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentAutomata_5), value);
	}

	inline static int32_t get_offset_of_emptiable_6() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_t3542030006, ___emptiable_6)); }
	inline bool get_emptiable_6() const { return ___emptiable_6; }
	inline bool* get_address_of_emptiable_6() { return &___emptiable_6; }
	inline void set_emptiable_6(bool value)
	{
		___emptiable_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSEQUENCEVALIDATIONSTATE_T3542030006_H
#ifndef XSDANYVALIDATIONSTATE_T204702461_H
#define XSDANYVALIDATIONSTATE_T204702461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnyValidationState
struct  XsdAnyValidationState_t204702461  : public XsdValidationState_t3545965403
{
public:
	// System.Xml.Schema.XmlSchemaAny Mono.Xml.Schema.XsdAnyValidationState::any
	XmlSchemaAny_t3277730824 * ___any_3;

public:
	inline static int32_t get_offset_of_any_3() { return static_cast<int32_t>(offsetof(XsdAnyValidationState_t204702461, ___any_3)); }
	inline XmlSchemaAny_t3277730824 * get_any_3() const { return ___any_3; }
	inline XmlSchemaAny_t3277730824 ** get_address_of_any_3() { return &___any_3; }
	inline void set_any_3(XmlSchemaAny_t3277730824 * value)
	{
		___any_3 = value;
		Il2CppCodeGenWriteBarrier((&___any_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYVALIDATIONSTATE_T204702461_H
#ifndef XSDALLVALIDATIONSTATE_T1028212608_H
#define XSDALLVALIDATIONSTATE_T1028212608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAllValidationState
struct  XsdAllValidationState_t1028212608  : public XsdValidationState_t3545965403
{
public:
	// System.Xml.Schema.XmlSchemaAll Mono.Xml.Schema.XsdAllValidationState::all
	XmlSchemaAll_t1805755215 * ___all_3;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdAllValidationState::consumed
	ArrayList_t4252133567 * ___consumed_4;

public:
	inline static int32_t get_offset_of_all_3() { return static_cast<int32_t>(offsetof(XsdAllValidationState_t1028212608, ___all_3)); }
	inline XmlSchemaAll_t1805755215 * get_all_3() const { return ___all_3; }
	inline XmlSchemaAll_t1805755215 ** get_address_of_all_3() { return &___all_3; }
	inline void set_all_3(XmlSchemaAll_t1805755215 * value)
	{
		___all_3 = value;
		Il2CppCodeGenWriteBarrier((&___all_3), value);
	}

	inline static int32_t get_offset_of_consumed_4() { return static_cast<int32_t>(offsetof(XsdAllValidationState_t1028212608, ___consumed_4)); }
	inline ArrayList_t4252133567 * get_consumed_4() const { return ___consumed_4; }
	inline ArrayList_t4252133567 ** get_address_of_consumed_4() { return &___consumed_4; }
	inline void set_consumed_4(ArrayList_t4252133567 * value)
	{
		___consumed_4 = value;
		Il2CppCodeGenWriteBarrier((&___consumed_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDALLVALIDATIONSTATE_T1028212608_H
#ifndef XSDELEMENTVALIDATIONSTATE_T152111323_H
#define XSDELEMENTVALIDATIONSTATE_T152111323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdElementValidationState
struct  XsdElementValidationState_t152111323  : public XsdValidationState_t3545965403
{
public:
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XsdElementValidationState::element
	XmlSchemaElement_t2433337156 * ___element_3;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(XsdElementValidationState_t152111323, ___element_3)); }
	inline XmlSchemaElement_t2433337156 * get_element_3() const { return ___element_3; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(XmlSchemaElement_t2433337156 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDELEMENTVALIDATIONSTATE_T152111323_H
#ifndef XSDKEYENTRYFIELDCOLLECTION_T2946985218_H
#define XSDKEYENTRYFIELDCOLLECTION_T2946985218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryFieldCollection
struct  XsdKeyEntryFieldCollection_t2946985218  : public CollectionBase_t1101587467
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYFIELDCOLLECTION_T2946985218_H
#ifndef XSDEMPTYVALIDATIONSTATE_T1914323912_H
#define XSDEMPTYVALIDATIONSTATE_T1914323912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEmptyValidationState
struct  XsdEmptyValidationState_t1914323912  : public XsdValidationState_t3545965403
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDEMPTYVALIDATIONSTATE_T1914323912_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef XSDKEYENTRYCOLLECTION_T1714053544_H
#define XSDKEYENTRYCOLLECTION_T1714053544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryCollection
struct  XsdKeyEntryCollection_t1714053544  : public CollectionBase_t1101587467
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYCOLLECTION_T1714053544_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef XMLFILTERREADER_T256953178_H
#define XMLFILTERREADER_T256953178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.XmlFilterReader
struct  XmlFilterReader_t256953178  : public XmlReader_t3675626668
{
public:
	// System.Xml.XmlReader Mono.Xml.XmlFilterReader::reader
	XmlReader_t3675626668 * ___reader_2;
	// System.Xml.XmlReaderSettings Mono.Xml.XmlFilterReader::settings
	XmlReaderSettings_t1578612233 * ___settings_3;
	// System.Xml.IXmlLineInfo Mono.Xml.XmlFilterReader::lineInfo
	RuntimeObject* ___lineInfo_4;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(XmlFilterReader_t256953178, ___reader_2)); }
	inline XmlReader_t3675626668 * get_reader_2() const { return ___reader_2; }
	inline XmlReader_t3675626668 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(XmlReader_t3675626668 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_settings_3() { return static_cast<int32_t>(offsetof(XmlFilterReader_t256953178, ___settings_3)); }
	inline XmlReaderSettings_t1578612233 * get_settings_3() const { return ___settings_3; }
	inline XmlReaderSettings_t1578612233 ** get_address_of_settings_3() { return &___settings_3; }
	inline void set_settings_3(XmlReaderSettings_t1578612233 * value)
	{
		___settings_3 = value;
		Il2CppCodeGenWriteBarrier((&___settings_3), value);
	}

	inline static int32_t get_offset_of_lineInfo_4() { return static_cast<int32_t>(offsetof(XmlFilterReader_t256953178, ___lineInfo_4)); }
	inline RuntimeObject* get_lineInfo_4() const { return ___lineInfo_4; }
	inline RuntimeObject** get_address_of_lineInfo_4() { return &___lineInfo_4; }
	inline void set_lineInfo_4(RuntimeObject* value)
	{
		___lineInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLFILTERREADER_T256953178_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef XSDAPPENDEDVALIDATIONSTATE_T3724408090_H
#define XSDAPPENDEDVALIDATIONSTATE_T3724408090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAppendedValidationState
struct  XsdAppendedValidationState_t3724408090  : public XsdValidationState_t3545965403
{
public:
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdAppendedValidationState::head
	XsdValidationState_t3545965403 * ___head_3;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdAppendedValidationState::rest
	XsdValidationState_t3545965403 * ___rest_4;

public:
	inline static int32_t get_offset_of_head_3() { return static_cast<int32_t>(offsetof(XsdAppendedValidationState_t3724408090, ___head_3)); }
	inline XsdValidationState_t3545965403 * get_head_3() const { return ___head_3; }
	inline XsdValidationState_t3545965403 ** get_address_of_head_3() { return &___head_3; }
	inline void set_head_3(XsdValidationState_t3545965403 * value)
	{
		___head_3 = value;
		Il2CppCodeGenWriteBarrier((&___head_3), value);
	}

	inline static int32_t get_offset_of_rest_4() { return static_cast<int32_t>(offsetof(XsdAppendedValidationState_t3724408090, ___rest_4)); }
	inline XsdValidationState_t3545965403 * get_rest_4() const { return ___rest_4; }
	inline XsdValidationState_t3545965403 ** get_address_of_rest_4() { return &___rest_4; }
	inline void set_rest_4(XsdValidationState_t3545965403 * value)
	{
		___rest_4 = value;
		Il2CppCodeGenWriteBarrier((&___rest_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDAPPENDEDVALIDATIONSTATE_T3724408090_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef XSDINVALIDVALIDATIONSTATE_T1112885736_H
#define XSDINVALIDVALIDATIONSTATE_T1112885736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInvalidValidationState
struct  XsdInvalidValidationState_t1112885736  : public XsdValidationState_t3545965403
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINVALIDVALIDATIONSTATE_T1112885736_H
#ifndef PROPERTYNAME_T217228410_H
#define PROPERTYNAME_T217228410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyName
struct  PropertyName_t217228410 
{
public:
	// System.Int32 UnityEngine.PropertyName::id
	int32_t ___id_0;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PropertyName_t217228410, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYNAME_T217228410_H
#ifndef MONOTODOATTRIBUTE_T3487514022_H
#define MONOTODOATTRIBUTE_T3487514022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t3487514022  : public Attribute_t542643598
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T3487514022_H
#ifndef UILINEINFO_T3621277874_H
#define UILINEINFO_T3621277874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UILineInfo
struct  UILineInfo_t3621277874 
{
public:
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;

public:
	inline static int32_t get_offset_of_startCharIdx_0() { return static_cast<int32_t>(offsetof(UILineInfo_t3621277874, ___startCharIdx_0)); }
	inline int32_t get_startCharIdx_0() const { return ___startCharIdx_0; }
	inline int32_t* get_address_of_startCharIdx_0() { return &___startCharIdx_0; }
	inline void set_startCharIdx_0(int32_t value)
	{
		___startCharIdx_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(UILineInfo_t3621277874, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_topY_2() { return static_cast<int32_t>(offsetof(UILineInfo_t3621277874, ___topY_2)); }
	inline float get_topY_2() const { return ___topY_2; }
	inline float* get_address_of_topY_2() { return &___topY_2; }
	inline void set_topY_2(float value)
	{
		___topY_2 = value;
	}

	inline static int32_t get_offset_of_leading_3() { return static_cast<int32_t>(offsetof(UILineInfo_t3621277874, ___leading_3)); }
	inline float get_leading_3() const { return ___leading_3; }
	inline float* get_address_of_leading_3() { return &___leading_3; }
	inline void set_leading_3(float value)
	{
		___leading_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINEINFO_T3621277874_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef ASYNCOPERATION_T3814632279_H
#define ASYNCOPERATION_T3814632279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AsyncOperation
struct  AsyncOperation_t3814632279  : public YieldInstruction_t3462875981
{
public:
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_t3616431661 * ___m_completeCallback_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AsyncOperation_t3814632279, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_completeCallback_1() { return static_cast<int32_t>(offsetof(AsyncOperation_t3814632279, ___m_completeCallback_1)); }
	inline Action_1_t3616431661 * get_m_completeCallback_1() const { return ___m_completeCallback_1; }
	inline Action_1_t3616431661 ** get_address_of_m_completeCallback_1() { return &___m_completeCallback_1; }
	inline void set_m_completeCallback_1(Action_1_t3616431661 * value)
	{
		___m_completeCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_completeCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279_marshaled_pinvoke : public YieldInstruction_t3462875981_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279_marshaled_com : public YieldInstruction_t3462875981_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
#endif // ASYNCOPERATION_T3814632279_H
#ifndef XSDWHITESPACEFACET_T2758097827_H
#define XSDWHITESPACEFACET_T2758097827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWhitespaceFacet
struct  XsdWhitespaceFacet_t2758097827 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdWhitespaceFacet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdWhitespaceFacet_t2758097827, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWHITESPACEFACET_T2758097827_H
#ifndef AUDIOSPATIALIZEREXTENSIONDEFINITION_T3666104158_H
#define AUDIOSPATIALIZEREXTENSIONDEFINITION_T3666104158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSpatializerExtensionDefinition
struct  AudioSpatializerExtensionDefinition_t3666104158  : public RuntimeObject
{
public:
	// UnityEngine.PropertyName UnityEngine.AudioSpatializerExtensionDefinition::spatializerName
	PropertyName_t217228410  ___spatializerName_0;
	// UnityEngine.AudioExtensionDefinition UnityEngine.AudioSpatializerExtensionDefinition::definition
	AudioExtensionDefinition_t1867593088 * ___definition_1;
	// UnityEngine.AudioExtensionDefinition UnityEngine.AudioSpatializerExtensionDefinition::editorDefinition
	AudioExtensionDefinition_t1867593088 * ___editorDefinition_2;

public:
	inline static int32_t get_offset_of_spatializerName_0() { return static_cast<int32_t>(offsetof(AudioSpatializerExtensionDefinition_t3666104158, ___spatializerName_0)); }
	inline PropertyName_t217228410  get_spatializerName_0() const { return ___spatializerName_0; }
	inline PropertyName_t217228410 * get_address_of_spatializerName_0() { return &___spatializerName_0; }
	inline void set_spatializerName_0(PropertyName_t217228410  value)
	{
		___spatializerName_0 = value;
	}

	inline static int32_t get_offset_of_definition_1() { return static_cast<int32_t>(offsetof(AudioSpatializerExtensionDefinition_t3666104158, ___definition_1)); }
	inline AudioExtensionDefinition_t1867593088 * get_definition_1() const { return ___definition_1; }
	inline AudioExtensionDefinition_t1867593088 ** get_address_of_definition_1() { return &___definition_1; }
	inline void set_definition_1(AudioExtensionDefinition_t1867593088 * value)
	{
		___definition_1 = value;
		Il2CppCodeGenWriteBarrier((&___definition_1), value);
	}

	inline static int32_t get_offset_of_editorDefinition_2() { return static_cast<int32_t>(offsetof(AudioSpatializerExtensionDefinition_t3666104158, ___editorDefinition_2)); }
	inline AudioExtensionDefinition_t1867593088 * get_editorDefinition_2() const { return ___editorDefinition_2; }
	inline AudioExtensionDefinition_t1867593088 ** get_address_of_editorDefinition_2() { return &___editorDefinition_2; }
	inline void set_editorDefinition_2(AudioExtensionDefinition_t1867593088 * value)
	{
		___editorDefinition_2 = value;
		Il2CppCodeGenWriteBarrier((&___editorDefinition_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSPATIALIZEREXTENSIONDEFINITION_T3666104158_H
#ifndef FONTSTYLE_T2764949590_H
#define FONTSTYLE_T2764949590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t2764949590 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyle_t2764949590, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T2764949590_H
#ifndef DOWNLOADHANDLER_T1216180266_H
#define DOWNLOADHANDLER_T1216180266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_t1216180266  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_t1216180266, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1216180266_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1216180266_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // DOWNLOADHANDLER_T1216180266_H
#ifndef XMLSCHEMAVALIDATIONFLAGS_T910489930_H
#define XMLSCHEMAVALIDATIONFLAGS_T910489930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationFlags
struct  XmlSchemaValidationFlags_t910489930 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaValidationFlags_t910489930, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONFLAGS_T910489930_H
#ifndef TEXTGENERATIONERROR_T780770201_H
#define TEXTGENERATIONERROR_T780770201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextGenerationError
struct  TextGenerationError_t780770201 
{
public:
	// System.Int32 UnityEngine.TextGenerationError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextGenerationError_t780770201, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTGENERATIONERROR_T780770201_H
#ifndef PLAYABLEOUTPUTHANDLE_T551742311_H
#define PLAYABLEOUTPUTHANDLE_T551742311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableOutputHandle
struct  PlayableOutputHandle_t551742311 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableOutputHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableOutputHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t551742311, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableOutputHandle_t551742311, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEOUTPUTHANDLE_T551742311_H
#ifndef UIVERTEX_T1204258818_H
#define UIVERTEX_T1204258818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UIVertex
struct  UIVertex_t1204258818 
{
public:
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t2243707580  ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t2243707580  ___normal_1;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t874517518  ___color_2;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv0
	Vector2_t2243707579  ___uv0_3;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv1
	Vector2_t2243707579  ___uv1_4;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv2
	Vector2_t2243707579  ___uv2_5;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv3
	Vector2_t2243707579  ___uv3_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t2243707581  ___tangent_7;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818, ___position_0)); }
	inline Vector3_t2243707580  get_position_0() const { return ___position_0; }
	inline Vector3_t2243707580 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2243707580  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818, ___normal_1)); }
	inline Vector3_t2243707580  get_normal_1() const { return ___normal_1; }
	inline Vector3_t2243707580 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_t2243707580  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818, ___color_2)); }
	inline Color32_t874517518  get_color_2() const { return ___color_2; }
	inline Color32_t874517518 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color32_t874517518  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_uv0_3() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818, ___uv0_3)); }
	inline Vector2_t2243707579  get_uv0_3() const { return ___uv0_3; }
	inline Vector2_t2243707579 * get_address_of_uv0_3() { return &___uv0_3; }
	inline void set_uv0_3(Vector2_t2243707579  value)
	{
		___uv0_3 = value;
	}

	inline static int32_t get_offset_of_uv1_4() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818, ___uv1_4)); }
	inline Vector2_t2243707579  get_uv1_4() const { return ___uv1_4; }
	inline Vector2_t2243707579 * get_address_of_uv1_4() { return &___uv1_4; }
	inline void set_uv1_4(Vector2_t2243707579  value)
	{
		___uv1_4 = value;
	}

	inline static int32_t get_offset_of_uv2_5() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818, ___uv2_5)); }
	inline Vector2_t2243707579  get_uv2_5() const { return ___uv2_5; }
	inline Vector2_t2243707579 * get_address_of_uv2_5() { return &___uv2_5; }
	inline void set_uv2_5(Vector2_t2243707579  value)
	{
		___uv2_5 = value;
	}

	inline static int32_t get_offset_of_uv3_6() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818, ___uv3_6)); }
	inline Vector2_t2243707579  get_uv3_6() const { return ___uv3_6; }
	inline Vector2_t2243707579 * get_address_of_uv3_6() { return &___uv3_6; }
	inline void set_uv3_6(Vector2_t2243707579  value)
	{
		___uv3_6 = value;
	}

	inline static int32_t get_offset_of_tangent_7() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818, ___tangent_7)); }
	inline Vector4_t2243707581  get_tangent_7() const { return ___tangent_7; }
	inline Vector4_t2243707581 * get_address_of_tangent_7() { return &___tangent_7; }
	inline void set_tangent_7(Vector4_t2243707581  value)
	{
		___tangent_7 = value;
	}
};

struct UIVertex_t1204258818_StaticFields
{
public:
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t874517518  ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t2243707581  ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_t1204258818  ___simpleVert_10;

public:
	inline static int32_t get_offset_of_s_DefaultColor_8() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818_StaticFields, ___s_DefaultColor_8)); }
	inline Color32_t874517518  get_s_DefaultColor_8() const { return ___s_DefaultColor_8; }
	inline Color32_t874517518 * get_address_of_s_DefaultColor_8() { return &___s_DefaultColor_8; }
	inline void set_s_DefaultColor_8(Color32_t874517518  value)
	{
		___s_DefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t2243707581  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t2243707581 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t2243707581  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_simpleVert_10() { return static_cast<int32_t>(offsetof(UIVertex_t1204258818_StaticFields, ___simpleVert_10)); }
	inline UIVertex_t1204258818  get_simpleVert_10() const { return ___simpleVert_10; }
	inline UIVertex_t1204258818 * get_address_of_simpleVert_10() { return &___simpleVert_10; }
	inline void set_simpleVert_10(UIVertex_t1204258818  value)
	{
		___simpleVert_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVERTEX_T1204258818_H
#ifndef XSDORDERING_T3741887935_H
#define XSDORDERING_T3741887935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdOrdering
struct  XsdOrdering_t3741887935 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdOrdering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdOrdering_t3741887935, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDORDERING_T3741887935_H
#ifndef UPLOADHANDLER_T3552561393_H
#define UPLOADHANDLER_T3552561393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandler
struct  UploadHandler_t3552561393  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t3552561393, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t3552561393_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t3552561393_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // UPLOADHANDLER_T3552561393_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef TEXTANCHOR_T112990806_H
#define TEXTANCHOR_T112990806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t112990806 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t112990806, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T112990806_H
#ifndef AUDIOAMBISONICEXTENSIONDEFINITION_T52665437_H
#define AUDIOAMBISONICEXTENSIONDEFINITION_T52665437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioAmbisonicExtensionDefinition
struct  AudioAmbisonicExtensionDefinition_t52665437  : public RuntimeObject
{
public:
	// UnityEngine.PropertyName UnityEngine.AudioAmbisonicExtensionDefinition::ambisonicPluginName
	PropertyName_t217228410  ___ambisonicPluginName_0;
	// UnityEngine.AudioExtensionDefinition UnityEngine.AudioAmbisonicExtensionDefinition::definition
	AudioExtensionDefinition_t1867593088 * ___definition_1;

public:
	inline static int32_t get_offset_of_ambisonicPluginName_0() { return static_cast<int32_t>(offsetof(AudioAmbisonicExtensionDefinition_t52665437, ___ambisonicPluginName_0)); }
	inline PropertyName_t217228410  get_ambisonicPluginName_0() const { return ___ambisonicPluginName_0; }
	inline PropertyName_t217228410 * get_address_of_ambisonicPluginName_0() { return &___ambisonicPluginName_0; }
	inline void set_ambisonicPluginName_0(PropertyName_t217228410  value)
	{
		___ambisonicPluginName_0 = value;
	}

	inline static int32_t get_offset_of_definition_1() { return static_cast<int32_t>(offsetof(AudioAmbisonicExtensionDefinition_t52665437, ___definition_1)); }
	inline AudioExtensionDefinition_t1867593088 * get_definition_1() const { return ___definition_1; }
	inline AudioExtensionDefinition_t1867593088 ** get_address_of_definition_1() { return &___definition_1; }
	inline void set_definition_1(AudioExtensionDefinition_t1867593088 * value)
	{
		___definition_1 = value;
		Il2CppCodeGenWriteBarrier((&___definition_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOAMBISONICEXTENSIONDEFINITION_T52665437_H
#ifndef HORIZONTALWRAPMODE_T2027154177_H
#define HORIZONTALWRAPMODE_T2027154177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HorizontalWrapMode
struct  HorizontalWrapMode_t2027154177 
{
public:
	// System.Int32 UnityEngine.HorizontalWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HorizontalWrapMode_t2027154177, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALWRAPMODE_T2027154177_H
#ifndef FACET_T3019654938_H
#define FACET_T3019654938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet/Facet
struct  Facet_t3019654938 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaFacet/Facet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Facet_t3019654938, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACET_T3019654938_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef RAYCASTHIT2D_T4063908774_H
#define RAYCASTHIT2D_T4063908774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t4063908774 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2243707579  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2243707579  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2243707579  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t646061738 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Centroid_0)); }
	inline Vector2_t2243707579  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2243707579 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2243707579  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Point_1)); }
	inline Vector2_t2243707579  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2243707579 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2243707579  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Normal_2)); }
	inline Vector2_t2243707579  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2243707579 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2243707579  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Collider_5)); }
	inline Collider2D_t646061738 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t646061738 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t646061738 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_pinvoke
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_com
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T4063908774_H
#ifndef UNITYWEBREQUEST_T254341728_H
#define UNITYWEBREQUEST_T254341728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequest
struct  UnityWebRequest_t254341728  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_t254341728, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_t254341728, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_t254341728, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t254341728_marshaled_com
{
	intptr_t ___m_Ptr_0;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2;
};
#endif // UNITYWEBREQUEST_T254341728_H
#ifndef UICHARINFO_T3056636800_H
#define UICHARINFO_T3056636800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UICharInfo
struct  UICharInfo_t3056636800 
{
public:
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t2243707579  ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;

public:
	inline static int32_t get_offset_of_cursorPos_0() { return static_cast<int32_t>(offsetof(UICharInfo_t3056636800, ___cursorPos_0)); }
	inline Vector2_t2243707579  get_cursorPos_0() const { return ___cursorPos_0; }
	inline Vector2_t2243707579 * get_address_of_cursorPos_0() { return &___cursorPos_0; }
	inline void set_cursorPos_0(Vector2_t2243707579  value)
	{
		___cursorPos_0 = value;
	}

	inline static int32_t get_offset_of_charWidth_1() { return static_cast<int32_t>(offsetof(UICharInfo_t3056636800, ___charWidth_1)); }
	inline float get_charWidth_1() const { return ___charWidth_1; }
	inline float* get_address_of_charWidth_1() { return &___charWidth_1; }
	inline void set_charWidth_1(float value)
	{
		___charWidth_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICHARINFO_T3056636800_H
#ifndef UNITYWEBREQUESTMETHOD_T2654069489_H
#define UNITYWEBREQUESTMETHOD_T2654069489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod
struct  UnityWebRequestMethod_t2654069489 
{
public:
	// System.Int32 UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UnityWebRequestMethod_t2654069489, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBREQUESTMETHOD_T2654069489_H
#ifndef XMLSCHEMACONTENTPROCESSING_T74226324_H
#define XMLSCHEMACONTENTPROCESSING_T74226324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_t74226324 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_t74226324, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_T74226324_H
#ifndef VERTICALWRAPMODE_T3668245347_H
#define VERTICALWRAPMODE_T3668245347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VerticalWrapMode
struct  VerticalWrapMode_t3668245347 
{
public:
	// System.Int32 UnityEngine.VerticalWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VerticalWrapMode_t3668245347, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALWRAPMODE_T3668245347_H
#ifndef VALIDATIONTYPE_T1401987383_H
#define VALIDATIONTYPE_T1401987383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_t1401987383 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ValidationType_t1401987383, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_T1401987383_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef XMLSCHEMADATATYPE_T1195946242_H
#define XMLSCHEMADATATYPE_T1195946242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t1195946242  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdWhitespaceFacet System.Xml.Schema.XmlSchemaDatatype::WhitespaceValue
	int32_t ___WhitespaceValue_0;
	// System.Text.StringBuilder System.Xml.Schema.XmlSchemaDatatype::sb
	StringBuilder_t1221177846 * ___sb_2;

public:
	inline static int32_t get_offset_of_WhitespaceValue_0() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242, ___WhitespaceValue_0)); }
	inline int32_t get_WhitespaceValue_0() const { return ___WhitespaceValue_0; }
	inline int32_t* get_address_of_WhitespaceValue_0() { return &___WhitespaceValue_0; }
	inline void set_WhitespaceValue_0(int32_t value)
	{
		___WhitespaceValue_0 = value;
	}

	inline static int32_t get_offset_of_sb_2() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242, ___sb_2)); }
	inline StringBuilder_t1221177846 * get_sb_2() const { return ___sb_2; }
	inline StringBuilder_t1221177846 ** get_address_of_sb_2() { return &___sb_2; }
	inline void set_sb_2(StringBuilder_t1221177846 * value)
	{
		___sb_2 = value;
		Il2CppCodeGenWriteBarrier((&___sb_2), value);
	}
};

struct XmlSchemaDatatype_t1195946242_StaticFields
{
public:
	// System.Char[] System.Xml.Schema.XmlSchemaDatatype::wsChars
	CharU5BU5D_t1328083999* ___wsChars_1;
	// Mono.Xml.Schema.XsdAnySimpleType System.Xml.Schema.XmlSchemaDatatype::datatypeAnySimpleType
	XsdAnySimpleType_t1096449895 * ___datatypeAnySimpleType_3;
	// Mono.Xml.Schema.XsdString System.Xml.Schema.XmlSchemaDatatype::datatypeString
	XsdString_t263933896 * ___datatypeString_4;
	// Mono.Xml.Schema.XsdNormalizedString System.Xml.Schema.XmlSchemaDatatype::datatypeNormalizedString
	XsdNormalizedString_t2132216291 * ___datatypeNormalizedString_5;
	// Mono.Xml.Schema.XsdToken System.Xml.Schema.XmlSchemaDatatype::datatypeToken
	XsdToken_t2902215462 * ___datatypeToken_6;
	// Mono.Xml.Schema.XsdLanguage System.Xml.Schema.XmlSchemaDatatype::datatypeLanguage
	XsdLanguage_t3897851897 * ___datatypeLanguage_7;
	// Mono.Xml.Schema.XsdNMToken System.Xml.Schema.XmlSchemaDatatype::datatypeNMToken
	XsdNMToken_t547135877 * ___datatypeNMToken_8;
	// Mono.Xml.Schema.XsdNMTokens System.Xml.Schema.XmlSchemaDatatype::datatypeNMTokens
	XsdNMTokens_t1753890866 * ___datatypeNMTokens_9;
	// Mono.Xml.Schema.XsdName System.Xml.Schema.XmlSchemaDatatype::datatypeName
	XsdName_t1409945702 * ___datatypeName_10;
	// Mono.Xml.Schema.XsdNCName System.Xml.Schema.XmlSchemaDatatype::datatypeNCName
	XsdNCName_t414304927 * ___datatypeNCName_11;
	// Mono.Xml.Schema.XsdID System.Xml.Schema.XmlSchemaDatatype::datatypeID
	XsdID_t1067193160 * ___datatypeID_12;
	// Mono.Xml.Schema.XsdIDRef System.Xml.Schema.XmlSchemaDatatype::datatypeIDRef
	XsdIDRef_t1377311049 * ___datatypeIDRef_13;
	// Mono.Xml.Schema.XsdIDRefs System.Xml.Schema.XmlSchemaDatatype::datatypeIDRefs
	XsdIDRefs_t763119546 * ___datatypeIDRefs_14;
	// Mono.Xml.Schema.XsdEntity System.Xml.Schema.XmlSchemaDatatype::datatypeEntity
	XsdEntity_t2664305022 * ___datatypeEntity_15;
	// Mono.Xml.Schema.XsdEntities System.Xml.Schema.XmlSchemaDatatype::datatypeEntities
	XsdEntities_t1103053540 * ___datatypeEntities_16;
	// Mono.Xml.Schema.XsdNotation System.Xml.Schema.XmlSchemaDatatype::datatypeNotation
	XsdNotation_t720379093 * ___datatypeNotation_17;
	// Mono.Xml.Schema.XsdDecimal System.Xml.Schema.XmlSchemaDatatype::datatypeDecimal
	XsdDecimal_t2932251550 * ___datatypeDecimal_18;
	// Mono.Xml.Schema.XsdInteger System.Xml.Schema.XmlSchemaDatatype::datatypeInteger
	XsdInteger_t1330502157 * ___datatypeInteger_19;
	// Mono.Xml.Schema.XsdLong System.Xml.Schema.XmlSchemaDatatype::datatypeLong
	XsdLong_t4179235589 * ___datatypeLong_20;
	// Mono.Xml.Schema.XsdInt System.Xml.Schema.XmlSchemaDatatype::datatypeInt
	XsdInt_t1488443894 * ___datatypeInt_21;
	// Mono.Xml.Schema.XsdShort System.Xml.Schema.XmlSchemaDatatype::datatypeShort
	XsdShort_t1778530041 * ___datatypeShort_22;
	// Mono.Xml.Schema.XsdByte System.Xml.Schema.XmlSchemaDatatype::datatypeByte
	XsdByte_t1120972221 * ___datatypeByte_23;
	// Mono.Xml.Schema.XsdNonNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonNegativeInteger
	XsdNonNegativeInteger_t3587933853 * ___datatypeNonNegativeInteger_24;
	// Mono.Xml.Schema.XsdPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypePositiveInteger
	XsdPositiveInteger_t1896481288 * ___datatypePositiveInteger_25;
	// Mono.Xml.Schema.XsdUnsignedLong System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedLong
	XsdUnsignedLong_t137890294 * ___datatypeUnsignedLong_26;
	// Mono.Xml.Schema.XsdUnsignedInt System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedInt
	XsdUnsignedInt_t2956447959 * ___datatypeUnsignedInt_27;
	// Mono.Xml.Schema.XsdUnsignedShort System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedShort
	XsdUnsignedShort_t3693774826 * ___datatypeUnsignedShort_28;
	// Mono.Xml.Schema.XsdUnsignedByte System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedByte
	XsdUnsignedByte_t3216355454 * ___datatypeUnsignedByte_29;
	// Mono.Xml.Schema.XsdNonPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonPositiveInteger
	XsdNonPositiveInteger_t409343009 * ___datatypeNonPositiveInteger_30;
	// Mono.Xml.Schema.XsdNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNegativeInteger
	XsdNegativeInteger_t399444136 * ___datatypeNegativeInteger_31;
	// Mono.Xml.Schema.XsdFloat System.Xml.Schema.XmlSchemaDatatype::datatypeFloat
	XsdFloat_t386143221 * ___datatypeFloat_32;
	// Mono.Xml.Schema.XsdDouble System.Xml.Schema.XmlSchemaDatatype::datatypeDouble
	XsdDouble_t2510112208 * ___datatypeDouble_33;
	// Mono.Xml.Schema.XsdBase64Binary System.Xml.Schema.XmlSchemaDatatype::datatypeBase64Binary
	XsdBase64Binary_t1094704629 * ___datatypeBase64Binary_34;
	// Mono.Xml.Schema.XsdBoolean System.Xml.Schema.XmlSchemaDatatype::datatypeBoolean
	XsdBoolean_t4126353587 * ___datatypeBoolean_35;
	// Mono.Xml.Schema.XsdAnyURI System.Xml.Schema.XmlSchemaDatatype::datatypeAnyURI
	XsdAnyURI_t2527983239 * ___datatypeAnyURI_36;
	// Mono.Xml.Schema.XsdDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDuration
	XsdDuration_t1605638443 * ___datatypeDuration_37;
	// Mono.Xml.Schema.XsdDateTime System.Xml.Schema.XmlSchemaDatatype::datatypeDateTime
	XsdDateTime_t1344468684 * ___datatypeDateTime_38;
	// Mono.Xml.Schema.XsdDate System.Xml.Schema.XmlSchemaDatatype::datatypeDate
	XsdDate_t919459387 * ___datatypeDate_39;
	// Mono.Xml.Schema.XsdTime System.Xml.Schema.XmlSchemaDatatype::datatypeTime
	XsdTime_t4165680512 * ___datatypeTime_40;
	// Mono.Xml.Schema.XsdHexBinary System.Xml.Schema.XmlSchemaDatatype::datatypeHexBinary
	XsdHexBinary_t3496718151 * ___datatypeHexBinary_41;
	// Mono.Xml.Schema.XsdQName System.Xml.Schema.XmlSchemaDatatype::datatypeQName
	XsdQName_t930779123 * ___datatypeQName_42;
	// Mono.Xml.Schema.XsdGYearMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGYearMonth
	XsdGYearMonth_t1363357165 * ___datatypeGYearMonth_43;
	// Mono.Xml.Schema.XsdGMonthDay System.Xml.Schema.XmlSchemaDatatype::datatypeGMonthDay
	XsdGMonthDay_t3859978294 * ___datatypeGMonthDay_44;
	// Mono.Xml.Schema.XsdGYear System.Xml.Schema.XmlSchemaDatatype::datatypeGYear
	XsdGYear_t3810382607 * ___datatypeGYear_45;
	// Mono.Xml.Schema.XsdGMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGMonth
	XsdGMonth_t4076673358 * ___datatypeGMonth_46;
	// Mono.Xml.Schema.XsdGDay System.Xml.Schema.XmlSchemaDatatype::datatypeGDay
	XsdGDay_t1914244270 * ___datatypeGDay_47;
	// Mono.Xml.Schema.XdtAnyAtomicType System.Xml.Schema.XmlSchemaDatatype::datatypeAnyAtomicType
	XdtAnyAtomicType_t3359210273 * ___datatypeAnyAtomicType_48;
	// Mono.Xml.Schema.XdtUntypedAtomic System.Xml.Schema.XmlSchemaDatatype::datatypeUntypedAtomic
	XdtUntypedAtomic_t2904699188 * ___datatypeUntypedAtomic_49;
	// Mono.Xml.Schema.XdtDayTimeDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDayTimeDuration
	XdtDayTimeDuration_t2797717973 * ___datatypeDayTimeDuration_50;
	// Mono.Xml.Schema.XdtYearMonthDuration System.Xml.Schema.XmlSchemaDatatype::datatypeYearMonthDuration
	XdtYearMonthDuration_t1764328599 * ___datatypeYearMonthDuration_51;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2A
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2A_52;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2B
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2B_53;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switch$map2C
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2C_54;

public:
	inline static int32_t get_offset_of_wsChars_1() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___wsChars_1)); }
	inline CharU5BU5D_t1328083999* get_wsChars_1() const { return ___wsChars_1; }
	inline CharU5BU5D_t1328083999** get_address_of_wsChars_1() { return &___wsChars_1; }
	inline void set_wsChars_1(CharU5BU5D_t1328083999* value)
	{
		___wsChars_1 = value;
		Il2CppCodeGenWriteBarrier((&___wsChars_1), value);
	}

	inline static int32_t get_offset_of_datatypeAnySimpleType_3() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeAnySimpleType_3)); }
	inline XsdAnySimpleType_t1096449895 * get_datatypeAnySimpleType_3() const { return ___datatypeAnySimpleType_3; }
	inline XsdAnySimpleType_t1096449895 ** get_address_of_datatypeAnySimpleType_3() { return &___datatypeAnySimpleType_3; }
	inline void set_datatypeAnySimpleType_3(XsdAnySimpleType_t1096449895 * value)
	{
		___datatypeAnySimpleType_3 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnySimpleType_3), value);
	}

	inline static int32_t get_offset_of_datatypeString_4() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeString_4)); }
	inline XsdString_t263933896 * get_datatypeString_4() const { return ___datatypeString_4; }
	inline XsdString_t263933896 ** get_address_of_datatypeString_4() { return &___datatypeString_4; }
	inline void set_datatypeString_4(XsdString_t263933896 * value)
	{
		___datatypeString_4 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeString_4), value);
	}

	inline static int32_t get_offset_of_datatypeNormalizedString_5() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNormalizedString_5)); }
	inline XsdNormalizedString_t2132216291 * get_datatypeNormalizedString_5() const { return ___datatypeNormalizedString_5; }
	inline XsdNormalizedString_t2132216291 ** get_address_of_datatypeNormalizedString_5() { return &___datatypeNormalizedString_5; }
	inline void set_datatypeNormalizedString_5(XsdNormalizedString_t2132216291 * value)
	{
		___datatypeNormalizedString_5 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNormalizedString_5), value);
	}

	inline static int32_t get_offset_of_datatypeToken_6() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeToken_6)); }
	inline XsdToken_t2902215462 * get_datatypeToken_6() const { return ___datatypeToken_6; }
	inline XsdToken_t2902215462 ** get_address_of_datatypeToken_6() { return &___datatypeToken_6; }
	inline void set_datatypeToken_6(XsdToken_t2902215462 * value)
	{
		___datatypeToken_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeToken_6), value);
	}

	inline static int32_t get_offset_of_datatypeLanguage_7() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeLanguage_7)); }
	inline XsdLanguage_t3897851897 * get_datatypeLanguage_7() const { return ___datatypeLanguage_7; }
	inline XsdLanguage_t3897851897 ** get_address_of_datatypeLanguage_7() { return &___datatypeLanguage_7; }
	inline void set_datatypeLanguage_7(XsdLanguage_t3897851897 * value)
	{
		___datatypeLanguage_7 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLanguage_7), value);
	}

	inline static int32_t get_offset_of_datatypeNMToken_8() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNMToken_8)); }
	inline XsdNMToken_t547135877 * get_datatypeNMToken_8() const { return ___datatypeNMToken_8; }
	inline XsdNMToken_t547135877 ** get_address_of_datatypeNMToken_8() { return &___datatypeNMToken_8; }
	inline void set_datatypeNMToken_8(XsdNMToken_t547135877 * value)
	{
		___datatypeNMToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMToken_8), value);
	}

	inline static int32_t get_offset_of_datatypeNMTokens_9() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNMTokens_9)); }
	inline XsdNMTokens_t1753890866 * get_datatypeNMTokens_9() const { return ___datatypeNMTokens_9; }
	inline XsdNMTokens_t1753890866 ** get_address_of_datatypeNMTokens_9() { return &___datatypeNMTokens_9; }
	inline void set_datatypeNMTokens_9(XsdNMTokens_t1753890866 * value)
	{
		___datatypeNMTokens_9 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMTokens_9), value);
	}

	inline static int32_t get_offset_of_datatypeName_10() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeName_10)); }
	inline XsdName_t1409945702 * get_datatypeName_10() const { return ___datatypeName_10; }
	inline XsdName_t1409945702 ** get_address_of_datatypeName_10() { return &___datatypeName_10; }
	inline void set_datatypeName_10(XsdName_t1409945702 * value)
	{
		___datatypeName_10 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeName_10), value);
	}

	inline static int32_t get_offset_of_datatypeNCName_11() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNCName_11)); }
	inline XsdNCName_t414304927 * get_datatypeNCName_11() const { return ___datatypeNCName_11; }
	inline XsdNCName_t414304927 ** get_address_of_datatypeNCName_11() { return &___datatypeNCName_11; }
	inline void set_datatypeNCName_11(XsdNCName_t414304927 * value)
	{
		___datatypeNCName_11 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNCName_11), value);
	}

	inline static int32_t get_offset_of_datatypeID_12() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeID_12)); }
	inline XsdID_t1067193160 * get_datatypeID_12() const { return ___datatypeID_12; }
	inline XsdID_t1067193160 ** get_address_of_datatypeID_12() { return &___datatypeID_12; }
	inline void set_datatypeID_12(XsdID_t1067193160 * value)
	{
		___datatypeID_12 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeID_12), value);
	}

	inline static int32_t get_offset_of_datatypeIDRef_13() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeIDRef_13)); }
	inline XsdIDRef_t1377311049 * get_datatypeIDRef_13() const { return ___datatypeIDRef_13; }
	inline XsdIDRef_t1377311049 ** get_address_of_datatypeIDRef_13() { return &___datatypeIDRef_13; }
	inline void set_datatypeIDRef_13(XsdIDRef_t1377311049 * value)
	{
		___datatypeIDRef_13 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRef_13), value);
	}

	inline static int32_t get_offset_of_datatypeIDRefs_14() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeIDRefs_14)); }
	inline XsdIDRefs_t763119546 * get_datatypeIDRefs_14() const { return ___datatypeIDRefs_14; }
	inline XsdIDRefs_t763119546 ** get_address_of_datatypeIDRefs_14() { return &___datatypeIDRefs_14; }
	inline void set_datatypeIDRefs_14(XsdIDRefs_t763119546 * value)
	{
		___datatypeIDRefs_14 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRefs_14), value);
	}

	inline static int32_t get_offset_of_datatypeEntity_15() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeEntity_15)); }
	inline XsdEntity_t2664305022 * get_datatypeEntity_15() const { return ___datatypeEntity_15; }
	inline XsdEntity_t2664305022 ** get_address_of_datatypeEntity_15() { return &___datatypeEntity_15; }
	inline void set_datatypeEntity_15(XsdEntity_t2664305022 * value)
	{
		___datatypeEntity_15 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntity_15), value);
	}

	inline static int32_t get_offset_of_datatypeEntities_16() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeEntities_16)); }
	inline XsdEntities_t1103053540 * get_datatypeEntities_16() const { return ___datatypeEntities_16; }
	inline XsdEntities_t1103053540 ** get_address_of_datatypeEntities_16() { return &___datatypeEntities_16; }
	inline void set_datatypeEntities_16(XsdEntities_t1103053540 * value)
	{
		___datatypeEntities_16 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntities_16), value);
	}

	inline static int32_t get_offset_of_datatypeNotation_17() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNotation_17)); }
	inline XsdNotation_t720379093 * get_datatypeNotation_17() const { return ___datatypeNotation_17; }
	inline XsdNotation_t720379093 ** get_address_of_datatypeNotation_17() { return &___datatypeNotation_17; }
	inline void set_datatypeNotation_17(XsdNotation_t720379093 * value)
	{
		___datatypeNotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNotation_17), value);
	}

	inline static int32_t get_offset_of_datatypeDecimal_18() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDecimal_18)); }
	inline XsdDecimal_t2932251550 * get_datatypeDecimal_18() const { return ___datatypeDecimal_18; }
	inline XsdDecimal_t2932251550 ** get_address_of_datatypeDecimal_18() { return &___datatypeDecimal_18; }
	inline void set_datatypeDecimal_18(XsdDecimal_t2932251550 * value)
	{
		___datatypeDecimal_18 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDecimal_18), value);
	}

	inline static int32_t get_offset_of_datatypeInteger_19() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeInteger_19)); }
	inline XsdInteger_t1330502157 * get_datatypeInteger_19() const { return ___datatypeInteger_19; }
	inline XsdInteger_t1330502157 ** get_address_of_datatypeInteger_19() { return &___datatypeInteger_19; }
	inline void set_datatypeInteger_19(XsdInteger_t1330502157 * value)
	{
		___datatypeInteger_19 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInteger_19), value);
	}

	inline static int32_t get_offset_of_datatypeLong_20() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeLong_20)); }
	inline XsdLong_t4179235589 * get_datatypeLong_20() const { return ___datatypeLong_20; }
	inline XsdLong_t4179235589 ** get_address_of_datatypeLong_20() { return &___datatypeLong_20; }
	inline void set_datatypeLong_20(XsdLong_t4179235589 * value)
	{
		___datatypeLong_20 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLong_20), value);
	}

	inline static int32_t get_offset_of_datatypeInt_21() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeInt_21)); }
	inline XsdInt_t1488443894 * get_datatypeInt_21() const { return ___datatypeInt_21; }
	inline XsdInt_t1488443894 ** get_address_of_datatypeInt_21() { return &___datatypeInt_21; }
	inline void set_datatypeInt_21(XsdInt_t1488443894 * value)
	{
		___datatypeInt_21 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInt_21), value);
	}

	inline static int32_t get_offset_of_datatypeShort_22() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeShort_22)); }
	inline XsdShort_t1778530041 * get_datatypeShort_22() const { return ___datatypeShort_22; }
	inline XsdShort_t1778530041 ** get_address_of_datatypeShort_22() { return &___datatypeShort_22; }
	inline void set_datatypeShort_22(XsdShort_t1778530041 * value)
	{
		___datatypeShort_22 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeShort_22), value);
	}

	inline static int32_t get_offset_of_datatypeByte_23() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeByte_23)); }
	inline XsdByte_t1120972221 * get_datatypeByte_23() const { return ___datatypeByte_23; }
	inline XsdByte_t1120972221 ** get_address_of_datatypeByte_23() { return &___datatypeByte_23; }
	inline void set_datatypeByte_23(XsdByte_t1120972221 * value)
	{
		___datatypeByte_23 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeByte_23), value);
	}

	inline static int32_t get_offset_of_datatypeNonNegativeInteger_24() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNonNegativeInteger_24)); }
	inline XsdNonNegativeInteger_t3587933853 * get_datatypeNonNegativeInteger_24() const { return ___datatypeNonNegativeInteger_24; }
	inline XsdNonNegativeInteger_t3587933853 ** get_address_of_datatypeNonNegativeInteger_24() { return &___datatypeNonNegativeInteger_24; }
	inline void set_datatypeNonNegativeInteger_24(XsdNonNegativeInteger_t3587933853 * value)
	{
		___datatypeNonNegativeInteger_24 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonNegativeInteger_24), value);
	}

	inline static int32_t get_offset_of_datatypePositiveInteger_25() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypePositiveInteger_25)); }
	inline XsdPositiveInteger_t1896481288 * get_datatypePositiveInteger_25() const { return ___datatypePositiveInteger_25; }
	inline XsdPositiveInteger_t1896481288 ** get_address_of_datatypePositiveInteger_25() { return &___datatypePositiveInteger_25; }
	inline void set_datatypePositiveInteger_25(XsdPositiveInteger_t1896481288 * value)
	{
		___datatypePositiveInteger_25 = value;
		Il2CppCodeGenWriteBarrier((&___datatypePositiveInteger_25), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedLong_26() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUnsignedLong_26)); }
	inline XsdUnsignedLong_t137890294 * get_datatypeUnsignedLong_26() const { return ___datatypeUnsignedLong_26; }
	inline XsdUnsignedLong_t137890294 ** get_address_of_datatypeUnsignedLong_26() { return &___datatypeUnsignedLong_26; }
	inline void set_datatypeUnsignedLong_26(XsdUnsignedLong_t137890294 * value)
	{
		___datatypeUnsignedLong_26 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedLong_26), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedInt_27() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUnsignedInt_27)); }
	inline XsdUnsignedInt_t2956447959 * get_datatypeUnsignedInt_27() const { return ___datatypeUnsignedInt_27; }
	inline XsdUnsignedInt_t2956447959 ** get_address_of_datatypeUnsignedInt_27() { return &___datatypeUnsignedInt_27; }
	inline void set_datatypeUnsignedInt_27(XsdUnsignedInt_t2956447959 * value)
	{
		___datatypeUnsignedInt_27 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedInt_27), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedShort_28() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUnsignedShort_28)); }
	inline XsdUnsignedShort_t3693774826 * get_datatypeUnsignedShort_28() const { return ___datatypeUnsignedShort_28; }
	inline XsdUnsignedShort_t3693774826 ** get_address_of_datatypeUnsignedShort_28() { return &___datatypeUnsignedShort_28; }
	inline void set_datatypeUnsignedShort_28(XsdUnsignedShort_t3693774826 * value)
	{
		___datatypeUnsignedShort_28 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedShort_28), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedByte_29() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUnsignedByte_29)); }
	inline XsdUnsignedByte_t3216355454 * get_datatypeUnsignedByte_29() const { return ___datatypeUnsignedByte_29; }
	inline XsdUnsignedByte_t3216355454 ** get_address_of_datatypeUnsignedByte_29() { return &___datatypeUnsignedByte_29; }
	inline void set_datatypeUnsignedByte_29(XsdUnsignedByte_t3216355454 * value)
	{
		___datatypeUnsignedByte_29 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedByte_29), value);
	}

	inline static int32_t get_offset_of_datatypeNonPositiveInteger_30() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNonPositiveInteger_30)); }
	inline XsdNonPositiveInteger_t409343009 * get_datatypeNonPositiveInteger_30() const { return ___datatypeNonPositiveInteger_30; }
	inline XsdNonPositiveInteger_t409343009 ** get_address_of_datatypeNonPositiveInteger_30() { return &___datatypeNonPositiveInteger_30; }
	inline void set_datatypeNonPositiveInteger_30(XsdNonPositiveInteger_t409343009 * value)
	{
		___datatypeNonPositiveInteger_30 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonPositiveInteger_30), value);
	}

	inline static int32_t get_offset_of_datatypeNegativeInteger_31() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeNegativeInteger_31)); }
	inline XsdNegativeInteger_t399444136 * get_datatypeNegativeInteger_31() const { return ___datatypeNegativeInteger_31; }
	inline XsdNegativeInteger_t399444136 ** get_address_of_datatypeNegativeInteger_31() { return &___datatypeNegativeInteger_31; }
	inline void set_datatypeNegativeInteger_31(XsdNegativeInteger_t399444136 * value)
	{
		___datatypeNegativeInteger_31 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNegativeInteger_31), value);
	}

	inline static int32_t get_offset_of_datatypeFloat_32() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeFloat_32)); }
	inline XsdFloat_t386143221 * get_datatypeFloat_32() const { return ___datatypeFloat_32; }
	inline XsdFloat_t386143221 ** get_address_of_datatypeFloat_32() { return &___datatypeFloat_32; }
	inline void set_datatypeFloat_32(XsdFloat_t386143221 * value)
	{
		___datatypeFloat_32 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeFloat_32), value);
	}

	inline static int32_t get_offset_of_datatypeDouble_33() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDouble_33)); }
	inline XsdDouble_t2510112208 * get_datatypeDouble_33() const { return ___datatypeDouble_33; }
	inline XsdDouble_t2510112208 ** get_address_of_datatypeDouble_33() { return &___datatypeDouble_33; }
	inline void set_datatypeDouble_33(XsdDouble_t2510112208 * value)
	{
		___datatypeDouble_33 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDouble_33), value);
	}

	inline static int32_t get_offset_of_datatypeBase64Binary_34() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeBase64Binary_34)); }
	inline XsdBase64Binary_t1094704629 * get_datatypeBase64Binary_34() const { return ___datatypeBase64Binary_34; }
	inline XsdBase64Binary_t1094704629 ** get_address_of_datatypeBase64Binary_34() { return &___datatypeBase64Binary_34; }
	inline void set_datatypeBase64Binary_34(XsdBase64Binary_t1094704629 * value)
	{
		___datatypeBase64Binary_34 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBase64Binary_34), value);
	}

	inline static int32_t get_offset_of_datatypeBoolean_35() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeBoolean_35)); }
	inline XsdBoolean_t4126353587 * get_datatypeBoolean_35() const { return ___datatypeBoolean_35; }
	inline XsdBoolean_t4126353587 ** get_address_of_datatypeBoolean_35() { return &___datatypeBoolean_35; }
	inline void set_datatypeBoolean_35(XsdBoolean_t4126353587 * value)
	{
		___datatypeBoolean_35 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBoolean_35), value);
	}

	inline static int32_t get_offset_of_datatypeAnyURI_36() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeAnyURI_36)); }
	inline XsdAnyURI_t2527983239 * get_datatypeAnyURI_36() const { return ___datatypeAnyURI_36; }
	inline XsdAnyURI_t2527983239 ** get_address_of_datatypeAnyURI_36() { return &___datatypeAnyURI_36; }
	inline void set_datatypeAnyURI_36(XsdAnyURI_t2527983239 * value)
	{
		___datatypeAnyURI_36 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyURI_36), value);
	}

	inline static int32_t get_offset_of_datatypeDuration_37() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDuration_37)); }
	inline XsdDuration_t1605638443 * get_datatypeDuration_37() const { return ___datatypeDuration_37; }
	inline XsdDuration_t1605638443 ** get_address_of_datatypeDuration_37() { return &___datatypeDuration_37; }
	inline void set_datatypeDuration_37(XsdDuration_t1605638443 * value)
	{
		___datatypeDuration_37 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDuration_37), value);
	}

	inline static int32_t get_offset_of_datatypeDateTime_38() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDateTime_38)); }
	inline XsdDateTime_t1344468684 * get_datatypeDateTime_38() const { return ___datatypeDateTime_38; }
	inline XsdDateTime_t1344468684 ** get_address_of_datatypeDateTime_38() { return &___datatypeDateTime_38; }
	inline void set_datatypeDateTime_38(XsdDateTime_t1344468684 * value)
	{
		___datatypeDateTime_38 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDateTime_38), value);
	}

	inline static int32_t get_offset_of_datatypeDate_39() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDate_39)); }
	inline XsdDate_t919459387 * get_datatypeDate_39() const { return ___datatypeDate_39; }
	inline XsdDate_t919459387 ** get_address_of_datatypeDate_39() { return &___datatypeDate_39; }
	inline void set_datatypeDate_39(XsdDate_t919459387 * value)
	{
		___datatypeDate_39 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDate_39), value);
	}

	inline static int32_t get_offset_of_datatypeTime_40() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeTime_40)); }
	inline XsdTime_t4165680512 * get_datatypeTime_40() const { return ___datatypeTime_40; }
	inline XsdTime_t4165680512 ** get_address_of_datatypeTime_40() { return &___datatypeTime_40; }
	inline void set_datatypeTime_40(XsdTime_t4165680512 * value)
	{
		___datatypeTime_40 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeTime_40), value);
	}

	inline static int32_t get_offset_of_datatypeHexBinary_41() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeHexBinary_41)); }
	inline XsdHexBinary_t3496718151 * get_datatypeHexBinary_41() const { return ___datatypeHexBinary_41; }
	inline XsdHexBinary_t3496718151 ** get_address_of_datatypeHexBinary_41() { return &___datatypeHexBinary_41; }
	inline void set_datatypeHexBinary_41(XsdHexBinary_t3496718151 * value)
	{
		___datatypeHexBinary_41 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeHexBinary_41), value);
	}

	inline static int32_t get_offset_of_datatypeQName_42() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeQName_42)); }
	inline XsdQName_t930779123 * get_datatypeQName_42() const { return ___datatypeQName_42; }
	inline XsdQName_t930779123 ** get_address_of_datatypeQName_42() { return &___datatypeQName_42; }
	inline void set_datatypeQName_42(XsdQName_t930779123 * value)
	{
		___datatypeQName_42 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeQName_42), value);
	}

	inline static int32_t get_offset_of_datatypeGYearMonth_43() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGYearMonth_43)); }
	inline XsdGYearMonth_t1363357165 * get_datatypeGYearMonth_43() const { return ___datatypeGYearMonth_43; }
	inline XsdGYearMonth_t1363357165 ** get_address_of_datatypeGYearMonth_43() { return &___datatypeGYearMonth_43; }
	inline void set_datatypeGYearMonth_43(XsdGYearMonth_t1363357165 * value)
	{
		___datatypeGYearMonth_43 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYearMonth_43), value);
	}

	inline static int32_t get_offset_of_datatypeGMonthDay_44() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGMonthDay_44)); }
	inline XsdGMonthDay_t3859978294 * get_datatypeGMonthDay_44() const { return ___datatypeGMonthDay_44; }
	inline XsdGMonthDay_t3859978294 ** get_address_of_datatypeGMonthDay_44() { return &___datatypeGMonthDay_44; }
	inline void set_datatypeGMonthDay_44(XsdGMonthDay_t3859978294 * value)
	{
		___datatypeGMonthDay_44 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonthDay_44), value);
	}

	inline static int32_t get_offset_of_datatypeGYear_45() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGYear_45)); }
	inline XsdGYear_t3810382607 * get_datatypeGYear_45() const { return ___datatypeGYear_45; }
	inline XsdGYear_t3810382607 ** get_address_of_datatypeGYear_45() { return &___datatypeGYear_45; }
	inline void set_datatypeGYear_45(XsdGYear_t3810382607 * value)
	{
		___datatypeGYear_45 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYear_45), value);
	}

	inline static int32_t get_offset_of_datatypeGMonth_46() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGMonth_46)); }
	inline XsdGMonth_t4076673358 * get_datatypeGMonth_46() const { return ___datatypeGMonth_46; }
	inline XsdGMonth_t4076673358 ** get_address_of_datatypeGMonth_46() { return &___datatypeGMonth_46; }
	inline void set_datatypeGMonth_46(XsdGMonth_t4076673358 * value)
	{
		___datatypeGMonth_46 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonth_46), value);
	}

	inline static int32_t get_offset_of_datatypeGDay_47() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeGDay_47)); }
	inline XsdGDay_t1914244270 * get_datatypeGDay_47() const { return ___datatypeGDay_47; }
	inline XsdGDay_t1914244270 ** get_address_of_datatypeGDay_47() { return &___datatypeGDay_47; }
	inline void set_datatypeGDay_47(XsdGDay_t1914244270 * value)
	{
		___datatypeGDay_47 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGDay_47), value);
	}

	inline static int32_t get_offset_of_datatypeAnyAtomicType_48() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeAnyAtomicType_48)); }
	inline XdtAnyAtomicType_t3359210273 * get_datatypeAnyAtomicType_48() const { return ___datatypeAnyAtomicType_48; }
	inline XdtAnyAtomicType_t3359210273 ** get_address_of_datatypeAnyAtomicType_48() { return &___datatypeAnyAtomicType_48; }
	inline void set_datatypeAnyAtomicType_48(XdtAnyAtomicType_t3359210273 * value)
	{
		___datatypeAnyAtomicType_48 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyAtomicType_48), value);
	}

	inline static int32_t get_offset_of_datatypeUntypedAtomic_49() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeUntypedAtomic_49)); }
	inline XdtUntypedAtomic_t2904699188 * get_datatypeUntypedAtomic_49() const { return ___datatypeUntypedAtomic_49; }
	inline XdtUntypedAtomic_t2904699188 ** get_address_of_datatypeUntypedAtomic_49() { return &___datatypeUntypedAtomic_49; }
	inline void set_datatypeUntypedAtomic_49(XdtUntypedAtomic_t2904699188 * value)
	{
		___datatypeUntypedAtomic_49 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUntypedAtomic_49), value);
	}

	inline static int32_t get_offset_of_datatypeDayTimeDuration_50() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeDayTimeDuration_50)); }
	inline XdtDayTimeDuration_t2797717973 * get_datatypeDayTimeDuration_50() const { return ___datatypeDayTimeDuration_50; }
	inline XdtDayTimeDuration_t2797717973 ** get_address_of_datatypeDayTimeDuration_50() { return &___datatypeDayTimeDuration_50; }
	inline void set_datatypeDayTimeDuration_50(XdtDayTimeDuration_t2797717973 * value)
	{
		___datatypeDayTimeDuration_50 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDayTimeDuration_50), value);
	}

	inline static int32_t get_offset_of_datatypeYearMonthDuration_51() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___datatypeYearMonthDuration_51)); }
	inline XdtYearMonthDuration_t1764328599 * get_datatypeYearMonthDuration_51() const { return ___datatypeYearMonthDuration_51; }
	inline XdtYearMonthDuration_t1764328599 ** get_address_of_datatypeYearMonthDuration_51() { return &___datatypeYearMonthDuration_51; }
	inline void set_datatypeYearMonthDuration_51(XdtYearMonthDuration_t1764328599 * value)
	{
		___datatypeYearMonthDuration_51 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeYearMonthDuration_51), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2A_52() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___U3CU3Ef__switchU24map2A_52)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2A_52() const { return ___U3CU3Ef__switchU24map2A_52; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2A_52() { return &___U3CU3Ef__switchU24map2A_52; }
	inline void set_U3CU3Ef__switchU24map2A_52(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2A_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2A_52), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2B_53() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___U3CU3Ef__switchU24map2B_53)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2B_53() const { return ___U3CU3Ef__switchU24map2B_53; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2B_53() { return &___U3CU3Ef__switchU24map2B_53; }
	inline void set_U3CU3Ef__switchU24map2B_53(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2B_53 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2B_53), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2C_54() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t1195946242_StaticFields, ___U3CU3Ef__switchU24map2C_54)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2C_54() const { return ___U3CU3Ef__switchU24map2C_54; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2C_54() { return &___U3CU3Ef__switchU24map2C_54; }
	inline void set_U3CU3Ef__switchU24map2C_54(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2C_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2C_54), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T1195946242_H
#ifndef AUDIOPLAYABLEOUTPUT_T3110750149_H
#define AUDIOPLAYABLEOUTPUT_T3110750149_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Audio.AudioPlayableOutput
struct  AudioPlayableOutput_t3110750149 
{
public:
	// UnityEngine.Playables.PlayableOutputHandle UnityEngine.Audio.AudioPlayableOutput::m_Handle
	PlayableOutputHandle_t551742311  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AudioPlayableOutput_t3110750149, ___m_Handle_0)); }
	inline PlayableOutputHandle_t551742311  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableOutputHandle_t551742311 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableOutputHandle_t551742311  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOPLAYABLEOUTPUT_T3110750149_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef UNITYWEBREQUESTASYNCOPERATION_T2821884557_H
#define UNITYWEBREQUESTASYNCOPERATION_T2821884557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct  UnityWebRequestAsyncOperation_t2821884557  : public AsyncOperation_t3814632279
{
public:
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::m_webRequest
	UnityWebRequest_t254341728 * ___m_webRequest_2;

public:
	inline static int32_t get_offset_of_m_webRequest_2() { return static_cast<int32_t>(offsetof(UnityWebRequestAsyncOperation_t2821884557, ___m_webRequest_2)); }
	inline UnityWebRequest_t254341728 * get_m_webRequest_2() const { return ___m_webRequest_2; }
	inline UnityWebRequest_t254341728 ** get_address_of_m_webRequest_2() { return &___m_webRequest_2; }
	inline void set_m_webRequest_2(UnityWebRequest_t254341728 * value)
	{
		___m_webRequest_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_webRequest_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t2821884557_marshaled_pinvoke : public AsyncOperation_t3814632279_marshaled_pinvoke
{
	UnityWebRequest_t254341728_marshaled_pinvoke ___m_webRequest_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t2821884557_marshaled_com : public AsyncOperation_t3814632279_marshaled_com
{
	UnityWebRequest_t254341728_marshaled_com* ___m_webRequest_2;
};
#endif // UNITYWEBREQUESTASYNCOPERATION_T2821884557_H
#ifndef XMLSCHEMAVALIDATINGREADER_T3861297856_H
#define XMLSCHEMAVALIDATINGREADER_T3861297856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaValidatingReader
struct  XmlSchemaValidatingReader_t3861297856  : public XmlReader_t3675626668
{
public:
	// System.Xml.XmlReader Mono.Xml.Schema.XmlSchemaValidatingReader::reader
	XmlReader_t3675626668 * ___reader_3;
	// System.Xml.Schema.XmlSchemaValidationFlags Mono.Xml.Schema.XmlSchemaValidatingReader::options
	int32_t ___options_4;
	// System.Xml.Schema.XmlSchemaValidator Mono.Xml.Schema.XmlSchemaValidatingReader::v
	XmlSchemaValidator_t1978690704 * ___v_5;
	// System.Xml.Schema.XmlValueGetter Mono.Xml.Schema.XmlSchemaValidatingReader::getter
	XmlValueGetter_t1685472371 * ___getter_6;
	// System.Xml.Schema.XmlSchemaInfo Mono.Xml.Schema.XmlSchemaValidatingReader::xsinfo
	XmlSchemaInfo_t2864028808 * ___xsinfo_7;
	// System.Xml.IXmlLineInfo Mono.Xml.Schema.XmlSchemaValidatingReader::readerLineInfo
	RuntimeObject* ___readerLineInfo_8;
	// System.Xml.IXmlNamespaceResolver Mono.Xml.Schema.XmlSchemaValidatingReader::nsResolver
	RuntimeObject* ___nsResolver_9;
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XmlSchemaValidatingReader::defaultAttributes
	XmlSchemaAttributeU5BU5D_t3434391819* ___defaultAttributes_10;
	// System.Int32 Mono.Xml.Schema.XmlSchemaValidatingReader::currentDefaultAttribute
	int32_t ___currentDefaultAttribute_11;
	// System.Collections.ArrayList Mono.Xml.Schema.XmlSchemaValidatingReader::defaultAttributesCache
	ArrayList_t4252133567 * ___defaultAttributesCache_12;
	// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::defaultAttributeConsumed
	bool ___defaultAttributeConsumed_13;
	// System.Xml.Schema.XmlSchemaType Mono.Xml.Schema.XmlSchemaValidatingReader::currentAttrType
	XmlSchemaType_t1795078578 * ___currentAttrType_14;
	// System.Boolean Mono.Xml.Schema.XmlSchemaValidatingReader::validationDone
	bool ___validationDone_15;
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XmlSchemaValidatingReader::element
	XmlSchemaElement_t2433337156 * ___element_16;

public:
	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___reader_3)); }
	inline XmlReader_t3675626668 * get_reader_3() const { return ___reader_3; }
	inline XmlReader_t3675626668 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(XmlReader_t3675626668 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier((&___reader_3), value);
	}

	inline static int32_t get_offset_of_options_4() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___options_4)); }
	inline int32_t get_options_4() const { return ___options_4; }
	inline int32_t* get_address_of_options_4() { return &___options_4; }
	inline void set_options_4(int32_t value)
	{
		___options_4 = value;
	}

	inline static int32_t get_offset_of_v_5() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___v_5)); }
	inline XmlSchemaValidator_t1978690704 * get_v_5() const { return ___v_5; }
	inline XmlSchemaValidator_t1978690704 ** get_address_of_v_5() { return &___v_5; }
	inline void set_v_5(XmlSchemaValidator_t1978690704 * value)
	{
		___v_5 = value;
		Il2CppCodeGenWriteBarrier((&___v_5), value);
	}

	inline static int32_t get_offset_of_getter_6() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___getter_6)); }
	inline XmlValueGetter_t1685472371 * get_getter_6() const { return ___getter_6; }
	inline XmlValueGetter_t1685472371 ** get_address_of_getter_6() { return &___getter_6; }
	inline void set_getter_6(XmlValueGetter_t1685472371 * value)
	{
		___getter_6 = value;
		Il2CppCodeGenWriteBarrier((&___getter_6), value);
	}

	inline static int32_t get_offset_of_xsinfo_7() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___xsinfo_7)); }
	inline XmlSchemaInfo_t2864028808 * get_xsinfo_7() const { return ___xsinfo_7; }
	inline XmlSchemaInfo_t2864028808 ** get_address_of_xsinfo_7() { return &___xsinfo_7; }
	inline void set_xsinfo_7(XmlSchemaInfo_t2864028808 * value)
	{
		___xsinfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___xsinfo_7), value);
	}

	inline static int32_t get_offset_of_readerLineInfo_8() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___readerLineInfo_8)); }
	inline RuntimeObject* get_readerLineInfo_8() const { return ___readerLineInfo_8; }
	inline RuntimeObject** get_address_of_readerLineInfo_8() { return &___readerLineInfo_8; }
	inline void set_readerLineInfo_8(RuntimeObject* value)
	{
		___readerLineInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___readerLineInfo_8), value);
	}

	inline static int32_t get_offset_of_nsResolver_9() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___nsResolver_9)); }
	inline RuntimeObject* get_nsResolver_9() const { return ___nsResolver_9; }
	inline RuntimeObject** get_address_of_nsResolver_9() { return &___nsResolver_9; }
	inline void set_nsResolver_9(RuntimeObject* value)
	{
		___nsResolver_9 = value;
		Il2CppCodeGenWriteBarrier((&___nsResolver_9), value);
	}

	inline static int32_t get_offset_of_defaultAttributes_10() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___defaultAttributes_10)); }
	inline XmlSchemaAttributeU5BU5D_t3434391819* get_defaultAttributes_10() const { return ___defaultAttributes_10; }
	inline XmlSchemaAttributeU5BU5D_t3434391819** get_address_of_defaultAttributes_10() { return &___defaultAttributes_10; }
	inline void set_defaultAttributes_10(XmlSchemaAttributeU5BU5D_t3434391819* value)
	{
		___defaultAttributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributes_10), value);
	}

	inline static int32_t get_offset_of_currentDefaultAttribute_11() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___currentDefaultAttribute_11)); }
	inline int32_t get_currentDefaultAttribute_11() const { return ___currentDefaultAttribute_11; }
	inline int32_t* get_address_of_currentDefaultAttribute_11() { return &___currentDefaultAttribute_11; }
	inline void set_currentDefaultAttribute_11(int32_t value)
	{
		___currentDefaultAttribute_11 = value;
	}

	inline static int32_t get_offset_of_defaultAttributesCache_12() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___defaultAttributesCache_12)); }
	inline ArrayList_t4252133567 * get_defaultAttributesCache_12() const { return ___defaultAttributesCache_12; }
	inline ArrayList_t4252133567 ** get_address_of_defaultAttributesCache_12() { return &___defaultAttributesCache_12; }
	inline void set_defaultAttributesCache_12(ArrayList_t4252133567 * value)
	{
		___defaultAttributesCache_12 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributesCache_12), value);
	}

	inline static int32_t get_offset_of_defaultAttributeConsumed_13() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___defaultAttributeConsumed_13)); }
	inline bool get_defaultAttributeConsumed_13() const { return ___defaultAttributeConsumed_13; }
	inline bool* get_address_of_defaultAttributeConsumed_13() { return &___defaultAttributeConsumed_13; }
	inline void set_defaultAttributeConsumed_13(bool value)
	{
		___defaultAttributeConsumed_13 = value;
	}

	inline static int32_t get_offset_of_currentAttrType_14() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___currentAttrType_14)); }
	inline XmlSchemaType_t1795078578 * get_currentAttrType_14() const { return ___currentAttrType_14; }
	inline XmlSchemaType_t1795078578 ** get_address_of_currentAttrType_14() { return &___currentAttrType_14; }
	inline void set_currentAttrType_14(XmlSchemaType_t1795078578 * value)
	{
		___currentAttrType_14 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttrType_14), value);
	}

	inline static int32_t get_offset_of_validationDone_15() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___validationDone_15)); }
	inline bool get_validationDone_15() const { return ___validationDone_15; }
	inline bool* get_address_of_validationDone_15() { return &___validationDone_15; }
	inline void set_validationDone_15(bool value)
	{
		___validationDone_15 = value;
	}

	inline static int32_t get_offset_of_element_16() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856, ___element_16)); }
	inline XmlSchemaElement_t2433337156 * get_element_16() const { return ___element_16; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_element_16() { return &___element_16; }
	inline void set_element_16(XmlSchemaElement_t2433337156 * value)
	{
		___element_16 = value;
		Il2CppCodeGenWriteBarrier((&___element_16), value);
	}
};

struct XmlSchemaValidatingReader_t3861297856_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XmlSchemaValidatingReader::emptyAttributeArray
	XmlSchemaAttributeU5BU5D_t3434391819* ___emptyAttributeArray_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XmlSchemaValidatingReader::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_17;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XmlSchemaValidatingReader::<>f__switch$map1
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1_18;

public:
	inline static int32_t get_offset_of_emptyAttributeArray_2() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856_StaticFields, ___emptyAttributeArray_2)); }
	inline XmlSchemaAttributeU5BU5D_t3434391819* get_emptyAttributeArray_2() const { return ___emptyAttributeArray_2; }
	inline XmlSchemaAttributeU5BU5D_t3434391819** get_address_of_emptyAttributeArray_2() { return &___emptyAttributeArray_2; }
	inline void set_emptyAttributeArray_2(XmlSchemaAttributeU5BU5D_t3434391819* value)
	{
		___emptyAttributeArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAttributeArray_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_17() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856_StaticFields, ___U3CU3Ef__switchU24map0_17)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_17() const { return ___U3CU3Ef__switchU24map0_17; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_17() { return &___U3CU3Ef__switchU24map0_17; }
	inline void set_U3CU3Ef__switchU24map0_17(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_18() { return static_cast<int32_t>(offsetof(XmlSchemaValidatingReader_t3861297856_StaticFields, ___U3CU3Ef__switchU24map1_18)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1_18() const { return ___U3CU3Ef__switchU24map1_18; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1_18() { return &___U3CU3Ef__switchU24map1_18; }
	inline void set_U3CU3Ef__switchU24map1_18(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATINGREADER_T3861297856_H
#ifndef DOWNLOADHANDLERBUFFER_T3443159558_H
#define DOWNLOADHANDLERBUFFER_T3443159558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandlerBuffer
struct  DownloadHandlerBuffer_t3443159558  : public DownloadHandler_t1216180266
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t3443159558_marshaled_pinvoke : public DownloadHandler_t1216180266_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t3443159558_marshaled_com : public DownloadHandler_t1216180266_marshaled_com
{
};
#endif // DOWNLOADHANDLERBUFFER_T3443159558_H
#ifndef TEXTGENERATIONSETTINGS_T2543476768_H
#define TEXTGENERATIONSETTINGS_T2543476768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextGenerationSettings
struct  TextGenerationSettings_t2543476768 
{
public:
	// UnityEngine.Font UnityEngine.TextGenerationSettings::font
	Font_t4239498691 * ___font_0;
	// UnityEngine.Color UnityEngine.TextGenerationSettings::color
	Color_t2020392075  ___color_1;
	// System.Int32 UnityEngine.TextGenerationSettings::fontSize
	int32_t ___fontSize_2;
	// System.Single UnityEngine.TextGenerationSettings::lineSpacing
	float ___lineSpacing_3;
	// System.Boolean UnityEngine.TextGenerationSettings::richText
	bool ___richText_4;
	// System.Single UnityEngine.TextGenerationSettings::scaleFactor
	float ___scaleFactor_5;
	// UnityEngine.FontStyle UnityEngine.TextGenerationSettings::fontStyle
	int32_t ___fontStyle_6;
	// UnityEngine.TextAnchor UnityEngine.TextGenerationSettings::textAnchor
	int32_t ___textAnchor_7;
	// System.Boolean UnityEngine.TextGenerationSettings::alignByGeometry
	bool ___alignByGeometry_8;
	// System.Boolean UnityEngine.TextGenerationSettings::resizeTextForBestFit
	bool ___resizeTextForBestFit_9;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMinSize
	int32_t ___resizeTextMinSize_10;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMaxSize
	int32_t ___resizeTextMaxSize_11;
	// System.Boolean UnityEngine.TextGenerationSettings::updateBounds
	bool ___updateBounds_12;
	// UnityEngine.VerticalWrapMode UnityEngine.TextGenerationSettings::verticalOverflow
	int32_t ___verticalOverflow_13;
	// UnityEngine.HorizontalWrapMode UnityEngine.TextGenerationSettings::horizontalOverflow
	int32_t ___horizontalOverflow_14;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::generationExtents
	Vector2_t2243707579  ___generationExtents_15;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::pivot
	Vector2_t2243707579  ___pivot_16;
	// System.Boolean UnityEngine.TextGenerationSettings::generateOutOfBounds
	bool ___generateOutOfBounds_17;

public:
	inline static int32_t get_offset_of_font_0() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___font_0)); }
	inline Font_t4239498691 * get_font_0() const { return ___font_0; }
	inline Font_t4239498691 ** get_address_of_font_0() { return &___font_0; }
	inline void set_font_0(Font_t4239498691 * value)
	{
		___font_0 = value;
		Il2CppCodeGenWriteBarrier((&___font_0), value);
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___color_1)); }
	inline Color_t2020392075  get_color_1() const { return ___color_1; }
	inline Color_t2020392075 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2020392075  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_fontSize_2() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___fontSize_2)); }
	inline int32_t get_fontSize_2() const { return ___fontSize_2; }
	inline int32_t* get_address_of_fontSize_2() { return &___fontSize_2; }
	inline void set_fontSize_2(int32_t value)
	{
		___fontSize_2 = value;
	}

	inline static int32_t get_offset_of_lineSpacing_3() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___lineSpacing_3)); }
	inline float get_lineSpacing_3() const { return ___lineSpacing_3; }
	inline float* get_address_of_lineSpacing_3() { return &___lineSpacing_3; }
	inline void set_lineSpacing_3(float value)
	{
		___lineSpacing_3 = value;
	}

	inline static int32_t get_offset_of_richText_4() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___richText_4)); }
	inline bool get_richText_4() const { return ___richText_4; }
	inline bool* get_address_of_richText_4() { return &___richText_4; }
	inline void set_richText_4(bool value)
	{
		___richText_4 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_5() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___scaleFactor_5)); }
	inline float get_scaleFactor_5() const { return ___scaleFactor_5; }
	inline float* get_address_of_scaleFactor_5() { return &___scaleFactor_5; }
	inline void set_scaleFactor_5(float value)
	{
		___scaleFactor_5 = value;
	}

	inline static int32_t get_offset_of_fontStyle_6() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___fontStyle_6)); }
	inline int32_t get_fontStyle_6() const { return ___fontStyle_6; }
	inline int32_t* get_address_of_fontStyle_6() { return &___fontStyle_6; }
	inline void set_fontStyle_6(int32_t value)
	{
		___fontStyle_6 = value;
	}

	inline static int32_t get_offset_of_textAnchor_7() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___textAnchor_7)); }
	inline int32_t get_textAnchor_7() const { return ___textAnchor_7; }
	inline int32_t* get_address_of_textAnchor_7() { return &___textAnchor_7; }
	inline void set_textAnchor_7(int32_t value)
	{
		___textAnchor_7 = value;
	}

	inline static int32_t get_offset_of_alignByGeometry_8() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___alignByGeometry_8)); }
	inline bool get_alignByGeometry_8() const { return ___alignByGeometry_8; }
	inline bool* get_address_of_alignByGeometry_8() { return &___alignByGeometry_8; }
	inline void set_alignByGeometry_8(bool value)
	{
		___alignByGeometry_8 = value;
	}

	inline static int32_t get_offset_of_resizeTextForBestFit_9() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___resizeTextForBestFit_9)); }
	inline bool get_resizeTextForBestFit_9() const { return ___resizeTextForBestFit_9; }
	inline bool* get_address_of_resizeTextForBestFit_9() { return &___resizeTextForBestFit_9; }
	inline void set_resizeTextForBestFit_9(bool value)
	{
		___resizeTextForBestFit_9 = value;
	}

	inline static int32_t get_offset_of_resizeTextMinSize_10() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___resizeTextMinSize_10)); }
	inline int32_t get_resizeTextMinSize_10() const { return ___resizeTextMinSize_10; }
	inline int32_t* get_address_of_resizeTextMinSize_10() { return &___resizeTextMinSize_10; }
	inline void set_resizeTextMinSize_10(int32_t value)
	{
		___resizeTextMinSize_10 = value;
	}

	inline static int32_t get_offset_of_resizeTextMaxSize_11() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___resizeTextMaxSize_11)); }
	inline int32_t get_resizeTextMaxSize_11() const { return ___resizeTextMaxSize_11; }
	inline int32_t* get_address_of_resizeTextMaxSize_11() { return &___resizeTextMaxSize_11; }
	inline void set_resizeTextMaxSize_11(int32_t value)
	{
		___resizeTextMaxSize_11 = value;
	}

	inline static int32_t get_offset_of_updateBounds_12() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___updateBounds_12)); }
	inline bool get_updateBounds_12() const { return ___updateBounds_12; }
	inline bool* get_address_of_updateBounds_12() { return &___updateBounds_12; }
	inline void set_updateBounds_12(bool value)
	{
		___updateBounds_12 = value;
	}

	inline static int32_t get_offset_of_verticalOverflow_13() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___verticalOverflow_13)); }
	inline int32_t get_verticalOverflow_13() const { return ___verticalOverflow_13; }
	inline int32_t* get_address_of_verticalOverflow_13() { return &___verticalOverflow_13; }
	inline void set_verticalOverflow_13(int32_t value)
	{
		___verticalOverflow_13 = value;
	}

	inline static int32_t get_offset_of_horizontalOverflow_14() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___horizontalOverflow_14)); }
	inline int32_t get_horizontalOverflow_14() const { return ___horizontalOverflow_14; }
	inline int32_t* get_address_of_horizontalOverflow_14() { return &___horizontalOverflow_14; }
	inline void set_horizontalOverflow_14(int32_t value)
	{
		___horizontalOverflow_14 = value;
	}

	inline static int32_t get_offset_of_generationExtents_15() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___generationExtents_15)); }
	inline Vector2_t2243707579  get_generationExtents_15() const { return ___generationExtents_15; }
	inline Vector2_t2243707579 * get_address_of_generationExtents_15() { return &___generationExtents_15; }
	inline void set_generationExtents_15(Vector2_t2243707579  value)
	{
		___generationExtents_15 = value;
	}

	inline static int32_t get_offset_of_pivot_16() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___pivot_16)); }
	inline Vector2_t2243707579  get_pivot_16() const { return ___pivot_16; }
	inline Vector2_t2243707579 * get_address_of_pivot_16() { return &___pivot_16; }
	inline void set_pivot_16(Vector2_t2243707579  value)
	{
		___pivot_16 = value;
	}

	inline static int32_t get_offset_of_generateOutOfBounds_17() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t2543476768, ___generateOutOfBounds_17)); }
	inline bool get_generateOutOfBounds_17() const { return ___generateOutOfBounds_17; }
	inline bool* get_address_of_generateOutOfBounds_17() { return &___generateOutOfBounds_17; }
	inline void set_generateOutOfBounds_17(bool value)
	{
		___generateOutOfBounds_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_t2543476768_marshaled_pinvoke
{
	Font_t4239498691 * ___font_0;
	Color_t2020392075  ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t2243707579  ___generationExtents_15;
	Vector2_t2243707579  ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};
// Native definition for COM marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_t2543476768_marshaled_com
{
	Font_t4239498691 * ___font_0;
	Color_t2020392075  ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t2243707579  ___generationExtents_15;
	Vector2_t2243707579  ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};
#endif // TEXTGENERATIONSETTINGS_T2543476768_H
#ifndef FONT_T4239498691_H
#define FONT_T4239498691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Font
struct  Font_t4239498691  : public Object_t1021602117
{
public:
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t1272078033 * ___m_FontTextureRebuildCallback_3;

public:
	inline static int32_t get_offset_of_m_FontTextureRebuildCallback_3() { return static_cast<int32_t>(offsetof(Font_t4239498691, ___m_FontTextureRebuildCallback_3)); }
	inline FontTextureRebuildCallback_t1272078033 * get_m_FontTextureRebuildCallback_3() const { return ___m_FontTextureRebuildCallback_3; }
	inline FontTextureRebuildCallback_t1272078033 ** get_address_of_m_FontTextureRebuildCallback_3() { return &___m_FontTextureRebuildCallback_3; }
	inline void set_m_FontTextureRebuildCallback_3(FontTextureRebuildCallback_t1272078033 * value)
	{
		___m_FontTextureRebuildCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontTextureRebuildCallback_3), value);
	}
};

struct Font_t4239498691_StaticFields
{
public:
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t4041298073 * ___textureRebuilt_2;

public:
	inline static int32_t get_offset_of_textureRebuilt_2() { return static_cast<int32_t>(offsetof(Font_t4239498691_StaticFields, ___textureRebuilt_2)); }
	inline Action_1_t4041298073 * get_textureRebuilt_2() const { return ___textureRebuilt_2; }
	inline Action_1_t4041298073 ** get_address_of_textureRebuilt_2() { return &___textureRebuilt_2; }
	inline void set_textureRebuilt_2(Action_1_t4041298073 * value)
	{
		___textureRebuilt_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureRebuilt_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONT_T4239498691_H
#ifndef XSDPARTICLESTATEMANAGER_T4119977941_H
#define XSDPARTICLESTATEMANAGER_T4119977941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdParticleStateManager
struct  XsdParticleStateManager_t4119977941  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Schema.XsdParticleStateManager::table
	Hashtable_t909839986 * ___table_0;
	// System.Xml.Schema.XmlSchemaContentProcessing Mono.Xml.Schema.XsdParticleStateManager::processContents
	int32_t ___processContents_1;
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XsdParticleStateManager::CurrentElement
	XmlSchemaElement_t2433337156 * ___CurrentElement_2;
	// System.Collections.Stack Mono.Xml.Schema.XsdParticleStateManager::ContextStack
	Stack_t1043988394 * ___ContextStack_3;
	// Mono.Xml.Schema.XsdValidationContext Mono.Xml.Schema.XsdParticleStateManager::Context
	XsdValidationContext_t3720679969 * ___Context_4;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t4119977941, ___table_0)); }
	inline Hashtable_t909839986 * get_table_0() const { return ___table_0; }
	inline Hashtable_t909839986 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Hashtable_t909839986 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}

	inline static int32_t get_offset_of_processContents_1() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t4119977941, ___processContents_1)); }
	inline int32_t get_processContents_1() const { return ___processContents_1; }
	inline int32_t* get_address_of_processContents_1() { return &___processContents_1; }
	inline void set_processContents_1(int32_t value)
	{
		___processContents_1 = value;
	}

	inline static int32_t get_offset_of_CurrentElement_2() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t4119977941, ___CurrentElement_2)); }
	inline XmlSchemaElement_t2433337156 * get_CurrentElement_2() const { return ___CurrentElement_2; }
	inline XmlSchemaElement_t2433337156 ** get_address_of_CurrentElement_2() { return &___CurrentElement_2; }
	inline void set_CurrentElement_2(XmlSchemaElement_t2433337156 * value)
	{
		___CurrentElement_2 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentElement_2), value);
	}

	inline static int32_t get_offset_of_ContextStack_3() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t4119977941, ___ContextStack_3)); }
	inline Stack_t1043988394 * get_ContextStack_3() const { return ___ContextStack_3; }
	inline Stack_t1043988394 ** get_address_of_ContextStack_3() { return &___ContextStack_3; }
	inline void set_ContextStack_3(Stack_t1043988394 * value)
	{
		___ContextStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContextStack_3), value);
	}

	inline static int32_t get_offset_of_Context_4() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t4119977941, ___Context_4)); }
	inline XsdValidationContext_t3720679969 * get_Context_4() const { return ___Context_4; }
	inline XsdValidationContext_t3720679969 ** get_address_of_Context_4() { return &___Context_4; }
	inline void set_Context_4(XsdValidationContext_t3720679969 * value)
	{
		___Context_4 = value;
		Il2CppCodeGenWriteBarrier((&___Context_4), value);
	}
};

struct XsdParticleStateManager_t4119977941_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdParticleStateManager::<>f__switch$map2
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map2_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_5() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t4119977941_StaticFields, ___U3CU3Ef__switchU24map2_5)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map2_5() const { return ___U3CU3Ef__switchU24map2_5; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map2_5() { return &___U3CU3Ef__switchU24map2_5; }
	inline void set_U3CU3Ef__switchU24map2_5(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDPARTICLESTATEMANAGER_T4119977941_H
#ifndef UPLOADHANDLERRAW_T3420491431_H
#define UPLOADHANDLERRAW_T3420491431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.UploadHandlerRaw
struct  UploadHandlerRaw_t3420491431  : public UploadHandler_t3552561393
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t3420491431_marshaled_pinvoke : public UploadHandler_t3552561393_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandlerRaw
struct UploadHandlerRaw_t3420491431_marshaled_com : public UploadHandler_t3552561393_marshaled_com
{
};
#endif // UPLOADHANDLERRAW_T3420491431_H
#ifndef XSDWILDCARD_T625524157_H
#define XSDWILDCARD_T625524157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWildcard
struct  XsdWildcard_t625524157  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaObject Mono.Xml.Schema.XsdWildcard::xsobj
	XmlSchemaObject_t2050913741 * ___xsobj_0;
	// System.Xml.Schema.XmlSchemaContentProcessing Mono.Xml.Schema.XsdWildcard::ResolvedProcessing
	int32_t ___ResolvedProcessing_1;
	// System.String Mono.Xml.Schema.XsdWildcard::TargetNamespace
	String_t* ___TargetNamespace_2;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::SkipCompile
	bool ___SkipCompile_3;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueAny
	bool ___HasValueAny_4;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueLocal
	bool ___HasValueLocal_5;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueOther
	bool ___HasValueOther_6;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueTargetNamespace
	bool ___HasValueTargetNamespace_7;
	// System.Collections.Specialized.StringCollection Mono.Xml.Schema.XsdWildcard::ResolvedNamespaces
	StringCollection_t352985975 * ___ResolvedNamespaces_8;

public:
	inline static int32_t get_offset_of_xsobj_0() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157, ___xsobj_0)); }
	inline XmlSchemaObject_t2050913741 * get_xsobj_0() const { return ___xsobj_0; }
	inline XmlSchemaObject_t2050913741 ** get_address_of_xsobj_0() { return &___xsobj_0; }
	inline void set_xsobj_0(XmlSchemaObject_t2050913741 * value)
	{
		___xsobj_0 = value;
		Il2CppCodeGenWriteBarrier((&___xsobj_0), value);
	}

	inline static int32_t get_offset_of_ResolvedProcessing_1() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157, ___ResolvedProcessing_1)); }
	inline int32_t get_ResolvedProcessing_1() const { return ___ResolvedProcessing_1; }
	inline int32_t* get_address_of_ResolvedProcessing_1() { return &___ResolvedProcessing_1; }
	inline void set_ResolvedProcessing_1(int32_t value)
	{
		___ResolvedProcessing_1 = value;
	}

	inline static int32_t get_offset_of_TargetNamespace_2() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157, ___TargetNamespace_2)); }
	inline String_t* get_TargetNamespace_2() const { return ___TargetNamespace_2; }
	inline String_t** get_address_of_TargetNamespace_2() { return &___TargetNamespace_2; }
	inline void set_TargetNamespace_2(String_t* value)
	{
		___TargetNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetNamespace_2), value);
	}

	inline static int32_t get_offset_of_SkipCompile_3() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157, ___SkipCompile_3)); }
	inline bool get_SkipCompile_3() const { return ___SkipCompile_3; }
	inline bool* get_address_of_SkipCompile_3() { return &___SkipCompile_3; }
	inline void set_SkipCompile_3(bool value)
	{
		___SkipCompile_3 = value;
	}

	inline static int32_t get_offset_of_HasValueAny_4() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157, ___HasValueAny_4)); }
	inline bool get_HasValueAny_4() const { return ___HasValueAny_4; }
	inline bool* get_address_of_HasValueAny_4() { return &___HasValueAny_4; }
	inline void set_HasValueAny_4(bool value)
	{
		___HasValueAny_4 = value;
	}

	inline static int32_t get_offset_of_HasValueLocal_5() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157, ___HasValueLocal_5)); }
	inline bool get_HasValueLocal_5() const { return ___HasValueLocal_5; }
	inline bool* get_address_of_HasValueLocal_5() { return &___HasValueLocal_5; }
	inline void set_HasValueLocal_5(bool value)
	{
		___HasValueLocal_5 = value;
	}

	inline static int32_t get_offset_of_HasValueOther_6() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157, ___HasValueOther_6)); }
	inline bool get_HasValueOther_6() const { return ___HasValueOther_6; }
	inline bool* get_address_of_HasValueOther_6() { return &___HasValueOther_6; }
	inline void set_HasValueOther_6(bool value)
	{
		___HasValueOther_6 = value;
	}

	inline static int32_t get_offset_of_HasValueTargetNamespace_7() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157, ___HasValueTargetNamespace_7)); }
	inline bool get_HasValueTargetNamespace_7() const { return ___HasValueTargetNamespace_7; }
	inline bool* get_address_of_HasValueTargetNamespace_7() { return &___HasValueTargetNamespace_7; }
	inline void set_HasValueTargetNamespace_7(bool value)
	{
		___HasValueTargetNamespace_7 = value;
	}

	inline static int32_t get_offset_of_ResolvedNamespaces_8() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157, ___ResolvedNamespaces_8)); }
	inline StringCollection_t352985975 * get_ResolvedNamespaces_8() const { return ___ResolvedNamespaces_8; }
	inline StringCollection_t352985975 ** get_address_of_ResolvedNamespaces_8() { return &___ResolvedNamespaces_8; }
	inline void set_ResolvedNamespaces_8(StringCollection_t352985975 * value)
	{
		___ResolvedNamespaces_8 = value;
		Il2CppCodeGenWriteBarrier((&___ResolvedNamespaces_8), value);
	}
};

struct XsdWildcard_t625524157_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdWildcard::<>f__switch$map6
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map6_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_9() { return static_cast<int32_t>(offsetof(XsdWildcard_t625524157_StaticFields, ___U3CU3Ef__switchU24map6_9)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map6_9() const { return ___U3CU3Ef__switchU24map6_9; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map6_9() { return &___U3CU3Ef__switchU24map6_9; }
	inline void set_U3CU3Ef__switchU24map6_9(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWILDCARD_T625524157_H
#ifndef XSDVALIDATINGREADER_T1704923617_H
#define XSDVALIDATINGREADER_T1704923617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidatingReader
struct  XsdValidatingReader_t1704923617  : public XmlReader_t3675626668
{
public:
	// System.Xml.XmlReader Mono.Xml.Schema.XsdValidatingReader::reader
	XmlReader_t3675626668 * ___reader_3;
	// System.Xml.XmlResolver Mono.Xml.Schema.XsdValidatingReader::resolver
	XmlResolver_t2024571559 * ___resolver_4;
	// Mono.Xml.IHasXmlSchemaInfo Mono.Xml.Schema.XsdValidatingReader::sourceReaderSchemaInfo
	RuntimeObject* ___sourceReaderSchemaInfo_5;
	// System.Xml.IXmlLineInfo Mono.Xml.Schema.XsdValidatingReader::readerLineInfo
	RuntimeObject* ___readerLineInfo_6;
	// System.Xml.ValidationType Mono.Xml.Schema.XsdValidatingReader::validationType
	int32_t ___validationType_7;
	// System.Xml.Schema.XmlSchemaSet Mono.Xml.Schema.XsdValidatingReader::schemas
	XmlSchemaSet_t313318308 * ___schemas_8;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::namespaces
	bool ___namespaces_9;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::validationStarted
	bool ___validationStarted_10;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::checkIdentity
	bool ___checkIdentity_11;
	// Mono.Xml.Schema.XsdIDManager Mono.Xml.Schema.XsdValidatingReader::idManager
	XsdIDManager_t3860002335 * ___idManager_12;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::checkKeyConstraints
	bool ___checkKeyConstraints_13;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::keyTables
	ArrayList_t4252133567 * ___keyTables_14;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::currentKeyFieldConsumers
	ArrayList_t4252133567 * ___currentKeyFieldConsumers_15;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::tmpKeyrefPool
	ArrayList_t4252133567 * ___tmpKeyrefPool_16;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::elementQNameStack
	ArrayList_t4252133567 * ___elementQNameStack_17;
	// Mono.Xml.Schema.XsdParticleStateManager Mono.Xml.Schema.XsdValidatingReader::state
	XsdParticleStateManager_t4119977941 * ___state_18;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::skipValidationDepth
	int32_t ___skipValidationDepth_19;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::xsiNilDepth
	int32_t ___xsiNilDepth_20;
	// System.Text.StringBuilder Mono.Xml.Schema.XsdValidatingReader::storedCharacters
	StringBuilder_t1221177846 * ___storedCharacters_21;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::shouldValidateCharacters
	bool ___shouldValidateCharacters_22;
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XsdValidatingReader::defaultAttributes
	XmlSchemaAttributeU5BU5D_t3434391819* ___defaultAttributes_23;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::currentDefaultAttribute
	int32_t ___currentDefaultAttribute_24;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::defaultAttributesCache
	ArrayList_t4252133567 * ___defaultAttributesCache_25;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::defaultAttributeConsumed
	bool ___defaultAttributeConsumed_26;
	// System.Object Mono.Xml.Schema.XsdValidatingReader::currentAttrType
	RuntimeObject * ___currentAttrType_27;
	// System.Xml.Schema.ValidationEventHandler Mono.Xml.Schema.XsdValidatingReader::ValidationEventHandler
	ValidationEventHandler_t1580700381 * ___ValidationEventHandler_28;

public:
	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___reader_3)); }
	inline XmlReader_t3675626668 * get_reader_3() const { return ___reader_3; }
	inline XmlReader_t3675626668 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(XmlReader_t3675626668 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier((&___reader_3), value);
	}

	inline static int32_t get_offset_of_resolver_4() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___resolver_4)); }
	inline XmlResolver_t2024571559 * get_resolver_4() const { return ___resolver_4; }
	inline XmlResolver_t2024571559 ** get_address_of_resolver_4() { return &___resolver_4; }
	inline void set_resolver_4(XmlResolver_t2024571559 * value)
	{
		___resolver_4 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_4), value);
	}

	inline static int32_t get_offset_of_sourceReaderSchemaInfo_5() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___sourceReaderSchemaInfo_5)); }
	inline RuntimeObject* get_sourceReaderSchemaInfo_5() const { return ___sourceReaderSchemaInfo_5; }
	inline RuntimeObject** get_address_of_sourceReaderSchemaInfo_5() { return &___sourceReaderSchemaInfo_5; }
	inline void set_sourceReaderSchemaInfo_5(RuntimeObject* value)
	{
		___sourceReaderSchemaInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceReaderSchemaInfo_5), value);
	}

	inline static int32_t get_offset_of_readerLineInfo_6() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___readerLineInfo_6)); }
	inline RuntimeObject* get_readerLineInfo_6() const { return ___readerLineInfo_6; }
	inline RuntimeObject** get_address_of_readerLineInfo_6() { return &___readerLineInfo_6; }
	inline void set_readerLineInfo_6(RuntimeObject* value)
	{
		___readerLineInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___readerLineInfo_6), value);
	}

	inline static int32_t get_offset_of_validationType_7() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___validationType_7)); }
	inline int32_t get_validationType_7() const { return ___validationType_7; }
	inline int32_t* get_address_of_validationType_7() { return &___validationType_7; }
	inline void set_validationType_7(int32_t value)
	{
		___validationType_7 = value;
	}

	inline static int32_t get_offset_of_schemas_8() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___schemas_8)); }
	inline XmlSchemaSet_t313318308 * get_schemas_8() const { return ___schemas_8; }
	inline XmlSchemaSet_t313318308 ** get_address_of_schemas_8() { return &___schemas_8; }
	inline void set_schemas_8(XmlSchemaSet_t313318308 * value)
	{
		___schemas_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_8), value);
	}

	inline static int32_t get_offset_of_namespaces_9() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___namespaces_9)); }
	inline bool get_namespaces_9() const { return ___namespaces_9; }
	inline bool* get_address_of_namespaces_9() { return &___namespaces_9; }
	inline void set_namespaces_9(bool value)
	{
		___namespaces_9 = value;
	}

	inline static int32_t get_offset_of_validationStarted_10() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___validationStarted_10)); }
	inline bool get_validationStarted_10() const { return ___validationStarted_10; }
	inline bool* get_address_of_validationStarted_10() { return &___validationStarted_10; }
	inline void set_validationStarted_10(bool value)
	{
		___validationStarted_10 = value;
	}

	inline static int32_t get_offset_of_checkIdentity_11() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___checkIdentity_11)); }
	inline bool get_checkIdentity_11() const { return ___checkIdentity_11; }
	inline bool* get_address_of_checkIdentity_11() { return &___checkIdentity_11; }
	inline void set_checkIdentity_11(bool value)
	{
		___checkIdentity_11 = value;
	}

	inline static int32_t get_offset_of_idManager_12() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___idManager_12)); }
	inline XsdIDManager_t3860002335 * get_idManager_12() const { return ___idManager_12; }
	inline XsdIDManager_t3860002335 ** get_address_of_idManager_12() { return &___idManager_12; }
	inline void set_idManager_12(XsdIDManager_t3860002335 * value)
	{
		___idManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___idManager_12), value);
	}

	inline static int32_t get_offset_of_checkKeyConstraints_13() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___checkKeyConstraints_13)); }
	inline bool get_checkKeyConstraints_13() const { return ___checkKeyConstraints_13; }
	inline bool* get_address_of_checkKeyConstraints_13() { return &___checkKeyConstraints_13; }
	inline void set_checkKeyConstraints_13(bool value)
	{
		___checkKeyConstraints_13 = value;
	}

	inline static int32_t get_offset_of_keyTables_14() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___keyTables_14)); }
	inline ArrayList_t4252133567 * get_keyTables_14() const { return ___keyTables_14; }
	inline ArrayList_t4252133567 ** get_address_of_keyTables_14() { return &___keyTables_14; }
	inline void set_keyTables_14(ArrayList_t4252133567 * value)
	{
		___keyTables_14 = value;
		Il2CppCodeGenWriteBarrier((&___keyTables_14), value);
	}

	inline static int32_t get_offset_of_currentKeyFieldConsumers_15() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___currentKeyFieldConsumers_15)); }
	inline ArrayList_t4252133567 * get_currentKeyFieldConsumers_15() const { return ___currentKeyFieldConsumers_15; }
	inline ArrayList_t4252133567 ** get_address_of_currentKeyFieldConsumers_15() { return &___currentKeyFieldConsumers_15; }
	inline void set_currentKeyFieldConsumers_15(ArrayList_t4252133567 * value)
	{
		___currentKeyFieldConsumers_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentKeyFieldConsumers_15), value);
	}

	inline static int32_t get_offset_of_tmpKeyrefPool_16() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___tmpKeyrefPool_16)); }
	inline ArrayList_t4252133567 * get_tmpKeyrefPool_16() const { return ___tmpKeyrefPool_16; }
	inline ArrayList_t4252133567 ** get_address_of_tmpKeyrefPool_16() { return &___tmpKeyrefPool_16; }
	inline void set_tmpKeyrefPool_16(ArrayList_t4252133567 * value)
	{
		___tmpKeyrefPool_16 = value;
		Il2CppCodeGenWriteBarrier((&___tmpKeyrefPool_16), value);
	}

	inline static int32_t get_offset_of_elementQNameStack_17() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___elementQNameStack_17)); }
	inline ArrayList_t4252133567 * get_elementQNameStack_17() const { return ___elementQNameStack_17; }
	inline ArrayList_t4252133567 ** get_address_of_elementQNameStack_17() { return &___elementQNameStack_17; }
	inline void set_elementQNameStack_17(ArrayList_t4252133567 * value)
	{
		___elementQNameStack_17 = value;
		Il2CppCodeGenWriteBarrier((&___elementQNameStack_17), value);
	}

	inline static int32_t get_offset_of_state_18() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___state_18)); }
	inline XsdParticleStateManager_t4119977941 * get_state_18() const { return ___state_18; }
	inline XsdParticleStateManager_t4119977941 ** get_address_of_state_18() { return &___state_18; }
	inline void set_state_18(XsdParticleStateManager_t4119977941 * value)
	{
		___state_18 = value;
		Il2CppCodeGenWriteBarrier((&___state_18), value);
	}

	inline static int32_t get_offset_of_skipValidationDepth_19() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___skipValidationDepth_19)); }
	inline int32_t get_skipValidationDepth_19() const { return ___skipValidationDepth_19; }
	inline int32_t* get_address_of_skipValidationDepth_19() { return &___skipValidationDepth_19; }
	inline void set_skipValidationDepth_19(int32_t value)
	{
		___skipValidationDepth_19 = value;
	}

	inline static int32_t get_offset_of_xsiNilDepth_20() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___xsiNilDepth_20)); }
	inline int32_t get_xsiNilDepth_20() const { return ___xsiNilDepth_20; }
	inline int32_t* get_address_of_xsiNilDepth_20() { return &___xsiNilDepth_20; }
	inline void set_xsiNilDepth_20(int32_t value)
	{
		___xsiNilDepth_20 = value;
	}

	inline static int32_t get_offset_of_storedCharacters_21() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___storedCharacters_21)); }
	inline StringBuilder_t1221177846 * get_storedCharacters_21() const { return ___storedCharacters_21; }
	inline StringBuilder_t1221177846 ** get_address_of_storedCharacters_21() { return &___storedCharacters_21; }
	inline void set_storedCharacters_21(StringBuilder_t1221177846 * value)
	{
		___storedCharacters_21 = value;
		Il2CppCodeGenWriteBarrier((&___storedCharacters_21), value);
	}

	inline static int32_t get_offset_of_shouldValidateCharacters_22() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___shouldValidateCharacters_22)); }
	inline bool get_shouldValidateCharacters_22() const { return ___shouldValidateCharacters_22; }
	inline bool* get_address_of_shouldValidateCharacters_22() { return &___shouldValidateCharacters_22; }
	inline void set_shouldValidateCharacters_22(bool value)
	{
		___shouldValidateCharacters_22 = value;
	}

	inline static int32_t get_offset_of_defaultAttributes_23() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___defaultAttributes_23)); }
	inline XmlSchemaAttributeU5BU5D_t3434391819* get_defaultAttributes_23() const { return ___defaultAttributes_23; }
	inline XmlSchemaAttributeU5BU5D_t3434391819** get_address_of_defaultAttributes_23() { return &___defaultAttributes_23; }
	inline void set_defaultAttributes_23(XmlSchemaAttributeU5BU5D_t3434391819* value)
	{
		___defaultAttributes_23 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributes_23), value);
	}

	inline static int32_t get_offset_of_currentDefaultAttribute_24() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___currentDefaultAttribute_24)); }
	inline int32_t get_currentDefaultAttribute_24() const { return ___currentDefaultAttribute_24; }
	inline int32_t* get_address_of_currentDefaultAttribute_24() { return &___currentDefaultAttribute_24; }
	inline void set_currentDefaultAttribute_24(int32_t value)
	{
		___currentDefaultAttribute_24 = value;
	}

	inline static int32_t get_offset_of_defaultAttributesCache_25() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___defaultAttributesCache_25)); }
	inline ArrayList_t4252133567 * get_defaultAttributesCache_25() const { return ___defaultAttributesCache_25; }
	inline ArrayList_t4252133567 ** get_address_of_defaultAttributesCache_25() { return &___defaultAttributesCache_25; }
	inline void set_defaultAttributesCache_25(ArrayList_t4252133567 * value)
	{
		___defaultAttributesCache_25 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributesCache_25), value);
	}

	inline static int32_t get_offset_of_defaultAttributeConsumed_26() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___defaultAttributeConsumed_26)); }
	inline bool get_defaultAttributeConsumed_26() const { return ___defaultAttributeConsumed_26; }
	inline bool* get_address_of_defaultAttributeConsumed_26() { return &___defaultAttributeConsumed_26; }
	inline void set_defaultAttributeConsumed_26(bool value)
	{
		___defaultAttributeConsumed_26 = value;
	}

	inline static int32_t get_offset_of_currentAttrType_27() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___currentAttrType_27)); }
	inline RuntimeObject * get_currentAttrType_27() const { return ___currentAttrType_27; }
	inline RuntimeObject ** get_address_of_currentAttrType_27() { return &___currentAttrType_27; }
	inline void set_currentAttrType_27(RuntimeObject * value)
	{
		___currentAttrType_27 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttrType_27), value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_28() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617, ___ValidationEventHandler_28)); }
	inline ValidationEventHandler_t1580700381 * get_ValidationEventHandler_28() const { return ___ValidationEventHandler_28; }
	inline ValidationEventHandler_t1580700381 ** get_address_of_ValidationEventHandler_28() { return &___ValidationEventHandler_28; }
	inline void set_ValidationEventHandler_28(ValidationEventHandler_t1580700381 * value)
	{
		___ValidationEventHandler_28 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_28), value);
	}
};

struct XsdValidatingReader_t1704923617_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XsdValidatingReader::emptyAttributeArray
	XmlSchemaAttributeU5BU5D_t3434391819* ___emptyAttributeArray_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switch$map3
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map3_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switch$map4
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map4_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switch$map5
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map5_31;

public:
	inline static int32_t get_offset_of_emptyAttributeArray_2() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617_StaticFields, ___emptyAttributeArray_2)); }
	inline XmlSchemaAttributeU5BU5D_t3434391819* get_emptyAttributeArray_2() const { return ___emptyAttributeArray_2; }
	inline XmlSchemaAttributeU5BU5D_t3434391819** get_address_of_emptyAttributeArray_2() { return &___emptyAttributeArray_2; }
	inline void set_emptyAttributeArray_2(XmlSchemaAttributeU5BU5D_t3434391819* value)
	{
		___emptyAttributeArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAttributeArray_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_29() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617_StaticFields, ___U3CU3Ef__switchU24map3_29)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map3_29() const { return ___U3CU3Ef__switchU24map3_29; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map3_29() { return &___U3CU3Ef__switchU24map3_29; }
	inline void set_U3CU3Ef__switchU24map3_29(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map3_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_30() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617_StaticFields, ___U3CU3Ef__switchU24map4_30)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map4_30() const { return ___U3CU3Ef__switchU24map4_30; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map4_30() { return &___U3CU3Ef__switchU24map4_30; }
	inline void set_U3CU3Ef__switchU24map4_30(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map4_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_31() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t1704923617_StaticFields, ___U3CU3Ef__switchU24map5_31)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map5_31() const { return ___U3CU3Ef__switchU24map5_31; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map5_31() { return &___U3CU3Ef__switchU24map5_31; }
	inline void set_U3CU3Ef__switchU24map5_31(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map5_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATINGREADER_T1704923617_H
#ifndef XSDANYSIMPLETYPE_T1096449895_H
#define XSDANYSIMPLETYPE_T1096449895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnySimpleType
struct  XsdAnySimpleType_t1096449895  : public XmlSchemaDatatype_t1195946242
{
public:

public:
};

struct XsdAnySimpleType_t1096449895_StaticFields
{
public:
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdAnySimpleType::instance
	XsdAnySimpleType_t1096449895 * ___instance_55;
	// System.Char[] Mono.Xml.Schema.XsdAnySimpleType::whitespaceArray
	CharU5BU5D_t1328083999* ___whitespaceArray_56;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::booleanAllowedFacets
	int32_t ___booleanAllowedFacets_57;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::decimalAllowedFacets
	int32_t ___decimalAllowedFacets_58;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::durationAllowedFacets
	int32_t ___durationAllowedFacets_59;
	// System.Xml.Schema.XmlSchemaFacet/Facet Mono.Xml.Schema.XsdAnySimpleType::stringAllowedFacets
	int32_t ___stringAllowedFacets_60;

public:
	inline static int32_t get_offset_of_instance_55() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___instance_55)); }
	inline XsdAnySimpleType_t1096449895 * get_instance_55() const { return ___instance_55; }
	inline XsdAnySimpleType_t1096449895 ** get_address_of_instance_55() { return &___instance_55; }
	inline void set_instance_55(XsdAnySimpleType_t1096449895 * value)
	{
		___instance_55 = value;
		Il2CppCodeGenWriteBarrier((&___instance_55), value);
	}

	inline static int32_t get_offset_of_whitespaceArray_56() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___whitespaceArray_56)); }
	inline CharU5BU5D_t1328083999* get_whitespaceArray_56() const { return ___whitespaceArray_56; }
	inline CharU5BU5D_t1328083999** get_address_of_whitespaceArray_56() { return &___whitespaceArray_56; }
	inline void set_whitespaceArray_56(CharU5BU5D_t1328083999* value)
	{
		___whitespaceArray_56 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceArray_56), value);
	}

	inline static int32_t get_offset_of_booleanAllowedFacets_57() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___booleanAllowedFacets_57)); }
	inline int32_t get_booleanAllowedFacets_57() const { return ___booleanAllowedFacets_57; }
	inline int32_t* get_address_of_booleanAllowedFacets_57() { return &___booleanAllowedFacets_57; }
	inline void set_booleanAllowedFacets_57(int32_t value)
	{
		___booleanAllowedFacets_57 = value;
	}

	inline static int32_t get_offset_of_decimalAllowedFacets_58() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___decimalAllowedFacets_58)); }
	inline int32_t get_decimalAllowedFacets_58() const { return ___decimalAllowedFacets_58; }
	inline int32_t* get_address_of_decimalAllowedFacets_58() { return &___decimalAllowedFacets_58; }
	inline void set_decimalAllowedFacets_58(int32_t value)
	{
		___decimalAllowedFacets_58 = value;
	}

	inline static int32_t get_offset_of_durationAllowedFacets_59() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___durationAllowedFacets_59)); }
	inline int32_t get_durationAllowedFacets_59() const { return ___durationAllowedFacets_59; }
	inline int32_t* get_address_of_durationAllowedFacets_59() { return &___durationAllowedFacets_59; }
	inline void set_durationAllowedFacets_59(int32_t value)
	{
		___durationAllowedFacets_59 = value;
	}

	inline static int32_t get_offset_of_stringAllowedFacets_60() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t1096449895_StaticFields, ___stringAllowedFacets_60)); }
	inline int32_t get_stringAllowedFacets_60() const { return ___stringAllowedFacets_60; }
	inline int32_t* get_address_of_stringAllowedFacets_60() { return &___stringAllowedFacets_60; }
	inline void set_stringAllowedFacets_60(int32_t value)
	{
		___stringAllowedFacets_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYSIMPLETYPE_T1096449895_H
#ifndef AUDIOLISTENEREXTENSION_T3456787073_H
#define AUDIOLISTENEREXTENSION_T3456787073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioListenerExtension
struct  AudioListenerExtension_t3456787073  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.AudioListener UnityEngine.AudioListenerExtension::m_audioListener
	AudioListener_t1996719162 * ___m_audioListener_2;

public:
	inline static int32_t get_offset_of_m_audioListener_2() { return static_cast<int32_t>(offsetof(AudioListenerExtension_t3456787073, ___m_audioListener_2)); }
	inline AudioListener_t1996719162 * get_m_audioListener_2() const { return ___m_audioListener_2; }
	inline AudioListener_t1996719162 ** get_address_of_m_audioListener_2() { return &___m_audioListener_2; }
	inline void set_m_audioListener_2(AudioListener_t1996719162 * value)
	{
		___m_audioListener_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioListener_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOLISTENEREXTENSION_T3456787073_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef RIGIDBODY2D_T502193897_H
#define RIGIDBODY2D_T502193897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t502193897  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T502193897_H
#ifndef TEXTMESH_T1641806576_H
#define TEXTMESH_T1641806576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextMesh
struct  TextMesh_t1641806576  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTMESH_T1641806576_H
#ifndef FONTTEXTUREREBUILDCALLBACK_T1272078033_H
#define FONTTEXTUREREBUILDCALLBACK_T1272078033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Font/FontTextureRebuildCallback
struct  FontTextureRebuildCallback_t1272078033  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTTEXTUREREBUILDCALLBACK_T1272078033_H
#ifndef AUDIOSOURCEEXTENSION_T1813599864_H
#define AUDIOSOURCEEXTENSION_T1813599864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSourceExtension
struct  AudioSourceExtension_t1813599864  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.AudioSource UnityEngine.AudioSourceExtension::m_audioSource
	AudioSource_t1135106623 * ___m_audioSource_2;
	// System.Int32 UnityEngine.AudioSourceExtension::m_ExtensionManagerUpdateIndex
	int32_t ___m_ExtensionManagerUpdateIndex_3;

public:
	inline static int32_t get_offset_of_m_audioSource_2() { return static_cast<int32_t>(offsetof(AudioSourceExtension_t1813599864, ___m_audioSource_2)); }
	inline AudioSource_t1135106623 * get_m_audioSource_2() const { return ___m_audioSource_2; }
	inline AudioSource_t1135106623 ** get_address_of_m_audioSource_2() { return &___m_audioSource_2; }
	inline void set_m_audioSource_2(AudioSource_t1135106623 * value)
	{
		___m_audioSource_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_audioSource_2), value);
	}

	inline static int32_t get_offset_of_m_ExtensionManagerUpdateIndex_3() { return static_cast<int32_t>(offsetof(AudioSourceExtension_t1813599864, ___m_ExtensionManagerUpdateIndex_3)); }
	inline int32_t get_m_ExtensionManagerUpdateIndex_3() const { return ___m_ExtensionManagerUpdateIndex_3; }
	inline int32_t* get_address_of_m_ExtensionManagerUpdateIndex_3() { return &___m_ExtensionManagerUpdateIndex_3; }
	inline void set_m_ExtensionManagerUpdateIndex_3(int32_t value)
	{
		___m_ExtensionManagerUpdateIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCEEXTENSION_T1813599864_H
#ifndef TEXTGENERATOR_T647235000_H
#define TEXTGENERATOR_T647235000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextGenerator
struct  TextGenerator_t647235000  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TextGenerator::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.String UnityEngine.TextGenerator::m_LastString
	String_t* ___m_LastString_1;
	// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::m_LastSettings
	TextGenerationSettings_t2543476768  ___m_LastSettings_2;
	// System.Boolean UnityEngine.TextGenerator::m_HasGenerated
	bool ___m_HasGenerated_3;
	// UnityEngine.TextGenerationError UnityEngine.TextGenerator::m_LastValid
	int32_t ___m_LastValid_4;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::m_Verts
	List_1_t573379950 * ___m_Verts_5;
	// System.Collections.Generic.List`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::m_Characters
	List_1_t2425757932 * ___m_Characters_6;
	// System.Collections.Generic.List`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::m_Lines
	List_1_t2990399006 * ___m_Lines_7;
	// System.Boolean UnityEngine.TextGenerator::m_CachedVerts
	bool ___m_CachedVerts_8;
	// System.Boolean UnityEngine.TextGenerator::m_CachedCharacters
	bool ___m_CachedCharacters_9;
	// System.Boolean UnityEngine.TextGenerator::m_CachedLines
	bool ___m_CachedLines_10;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_LastString_1() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_LastString_1)); }
	inline String_t* get_m_LastString_1() const { return ___m_LastString_1; }
	inline String_t** get_address_of_m_LastString_1() { return &___m_LastString_1; }
	inline void set_m_LastString_1(String_t* value)
	{
		___m_LastString_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastString_1), value);
	}

	inline static int32_t get_offset_of_m_LastSettings_2() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_LastSettings_2)); }
	inline TextGenerationSettings_t2543476768  get_m_LastSettings_2() const { return ___m_LastSettings_2; }
	inline TextGenerationSettings_t2543476768 * get_address_of_m_LastSettings_2() { return &___m_LastSettings_2; }
	inline void set_m_LastSettings_2(TextGenerationSettings_t2543476768  value)
	{
		___m_LastSettings_2 = value;
	}

	inline static int32_t get_offset_of_m_HasGenerated_3() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_HasGenerated_3)); }
	inline bool get_m_HasGenerated_3() const { return ___m_HasGenerated_3; }
	inline bool* get_address_of_m_HasGenerated_3() { return &___m_HasGenerated_3; }
	inline void set_m_HasGenerated_3(bool value)
	{
		___m_HasGenerated_3 = value;
	}

	inline static int32_t get_offset_of_m_LastValid_4() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_LastValid_4)); }
	inline int32_t get_m_LastValid_4() const { return ___m_LastValid_4; }
	inline int32_t* get_address_of_m_LastValid_4() { return &___m_LastValid_4; }
	inline void set_m_LastValid_4(int32_t value)
	{
		___m_LastValid_4 = value;
	}

	inline static int32_t get_offset_of_m_Verts_5() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_Verts_5)); }
	inline List_1_t573379950 * get_m_Verts_5() const { return ___m_Verts_5; }
	inline List_1_t573379950 ** get_address_of_m_Verts_5() { return &___m_Verts_5; }
	inline void set_m_Verts_5(List_1_t573379950 * value)
	{
		___m_Verts_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Verts_5), value);
	}

	inline static int32_t get_offset_of_m_Characters_6() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_Characters_6)); }
	inline List_1_t2425757932 * get_m_Characters_6() const { return ___m_Characters_6; }
	inline List_1_t2425757932 ** get_address_of_m_Characters_6() { return &___m_Characters_6; }
	inline void set_m_Characters_6(List_1_t2425757932 * value)
	{
		___m_Characters_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Characters_6), value);
	}

	inline static int32_t get_offset_of_m_Lines_7() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_Lines_7)); }
	inline List_1_t2990399006 * get_m_Lines_7() const { return ___m_Lines_7; }
	inline List_1_t2990399006 ** get_address_of_m_Lines_7() { return &___m_Lines_7; }
	inline void set_m_Lines_7(List_1_t2990399006 * value)
	{
		___m_Lines_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Lines_7), value);
	}

	inline static int32_t get_offset_of_m_CachedVerts_8() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_CachedVerts_8)); }
	inline bool get_m_CachedVerts_8() const { return ___m_CachedVerts_8; }
	inline bool* get_address_of_m_CachedVerts_8() { return &___m_CachedVerts_8; }
	inline void set_m_CachedVerts_8(bool value)
	{
		___m_CachedVerts_8 = value;
	}

	inline static int32_t get_offset_of_m_CachedCharacters_9() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_CachedCharacters_9)); }
	inline bool get_m_CachedCharacters_9() const { return ___m_CachedCharacters_9; }
	inline bool* get_address_of_m_CachedCharacters_9() { return &___m_CachedCharacters_9; }
	inline void set_m_CachedCharacters_9(bool value)
	{
		___m_CachedCharacters_9 = value;
	}

	inline static int32_t get_offset_of_m_CachedLines_10() { return static_cast<int32_t>(offsetof(TextGenerator_t647235000, ___m_CachedLines_10)); }
	inline bool get_m_CachedLines_10() const { return ___m_CachedLines_10; }
	inline bool* get_address_of_m_CachedLines_10() { return &___m_CachedLines_10; }
	inline void set_m_CachedLines_10(bool value)
	{
		___m_CachedLines_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerator
struct TextGenerator_t647235000_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	char* ___m_LastString_1;
	TextGenerationSettings_t2543476768_marshaled_pinvoke ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t573379950 * ___m_Verts_5;
	List_1_t2425757932 * ___m_Characters_6;
	List_1_t2990399006 * ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};
// Native definition for COM marshalling of UnityEngine.TextGenerator
struct TextGenerator_t647235000_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppChar* ___m_LastString_1;
	TextGenerationSettings_t2543476768_marshaled_com ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t573379950 * ___m_Verts_5;
	List_1_t2425757932 * ___m_Characters_6;
	List_1_t2990399006 * ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};
#endif // TEXTGENERATOR_T647235000_H
#ifndef XSDSTRING_T263933896_H
#define XSDSTRING_T263933896_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdString
struct  XsdString_t263933896  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSTRING_T263933896_H
#ifndef XDTANYATOMICTYPE_T3359210273_H
#define XDTANYATOMICTYPE_T3359210273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtAnyAtomicType
struct  XdtAnyAtomicType_t3359210273  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTANYATOMICTYPE_T3359210273_H
#ifndef COLLIDER2D_T646061738_H
#define COLLIDER2D_T646061738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t646061738  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T646061738_H
#ifndef XSDDECIMAL_T2932251550_H
#define XSDDECIMAL_T2932251550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDecimal
struct  XsdDecimal_t2932251550  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDECIMAL_T2932251550_H
#ifndef XSDFLOAT_T386143221_H
#define XSDFLOAT_T386143221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdFloat
struct  XsdFloat_t386143221  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDFLOAT_T386143221_H
#ifndef XSDNOTATION_T720379093_H
#define XSDNOTATION_T720379093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNotation
struct  XsdNotation_t720379093  : public XsdAnySimpleType_t1096449895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNOTATION_T720379093_H
#ifndef XDTUNTYPEDATOMIC_T2904699188_H
#define XDTUNTYPEDATOMIC_T2904699188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtUntypedAtomic
struct  XdtUntypedAtomic_t2904699188  : public XdtAnyAtomicType_t3359210273
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTUNTYPEDATOMIC_T2904699188_H
#ifndef XSDINTEGER_T1330502157_H
#define XSDINTEGER_T1330502157_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInteger
struct  XsdInteger_t1330502157  : public XsdDecimal_t2932251550
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINTEGER_T1330502157_H
#ifndef XSDNORMALIZEDSTRING_T2132216291_H
#define XSDNORMALIZEDSTRING_T2132216291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNormalizedString
struct  XsdNormalizedString_t2132216291  : public XsdString_t263933896
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNORMALIZEDSTRING_T2132216291_H
#ifndef XSDNONNEGATIVEINTEGER_T3587933853_H
#define XSDNONNEGATIVEINTEGER_T3587933853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonNegativeInteger
struct  XsdNonNegativeInteger_t3587933853  : public XsdInteger_t1330502157
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONNEGATIVEINTEGER_T3587933853_H
#ifndef XSDNONPOSITIVEINTEGER_T409343009_H
#define XSDNONPOSITIVEINTEGER_T409343009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonPositiveInteger
struct  XsdNonPositiveInteger_t409343009  : public XsdInteger_t1330502157
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONPOSITIVEINTEGER_T409343009_H
#ifndef XSDLONG_T4179235589_H
#define XSDLONG_T4179235589_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdLong
struct  XsdLong_t4179235589  : public XsdInteger_t1330502157
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDLONG_T4179235589_H
#ifndef XSDTOKEN_T2902215462_H
#define XSDTOKEN_T2902215462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdToken
struct  XsdToken_t2902215462  : public XsdNormalizedString_t2132216291
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTOKEN_T2902215462_H
#ifndef XSDPOSITIVEINTEGER_T1896481288_H
#define XSDPOSITIVEINTEGER_T1896481288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdPositiveInteger
struct  XsdPositiveInteger_t1896481288  : public XsdNonNegativeInteger_t3587933853
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDPOSITIVEINTEGER_T1896481288_H
#ifndef XSDNEGATIVEINTEGER_T399444136_H
#define XSDNEGATIVEINTEGER_T399444136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNegativeInteger
struct  XsdNegativeInteger_t399444136  : public XsdNonPositiveInteger_t409343009
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNEGATIVEINTEGER_T399444136_H
#ifndef XSDINT_T1488443894_H
#define XSDINT_T1488443894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInt
struct  XsdInt_t1488443894  : public XsdLong_t4179235589
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINT_T1488443894_H
#ifndef XSDUNSIGNEDLONG_T137890294_H
#define XSDUNSIGNEDLONG_T137890294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedLong
struct  XsdUnsignedLong_t137890294  : public XsdNonNegativeInteger_t3587933853
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDLONG_T137890294_H
#ifndef XSDNMTOKEN_T547135877_H
#define XSDNMTOKEN_T547135877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNMToken
struct  XsdNMToken_t547135877  : public XsdToken_t2902215462
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNMTOKEN_T547135877_H
#ifndef XSDNAME_T1409945702_H
#define XSDNAME_T1409945702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdName
struct  XsdName_t1409945702  : public XsdToken_t2902215462
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNAME_T1409945702_H
#ifndef XSDLANGUAGE_T3897851897_H
#define XSDLANGUAGE_T3897851897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdLanguage
struct  XsdLanguage_t3897851897  : public XsdToken_t2902215462
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDLANGUAGE_T3897851897_H
#ifndef XSDENTITIES_T1103053540_H
#define XSDENTITIES_T1103053540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEntities
struct  XsdEntities_t1103053540  : public XsdName_t1409945702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENTITIES_T1103053540_H
#ifndef XSDIDREF_T1377311049_H
#define XSDIDREF_T1377311049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDRef
struct  XsdIDRef_t1377311049  : public XsdName_t1409945702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDREF_T1377311049_H
#ifndef XSDID_T1067193160_H
#define XSDID_T1067193160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdID
struct  XsdID_t1067193160  : public XsdName_t1409945702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDID_T1067193160_H
#ifndef XSDIDREFS_T763119546_H
#define XSDIDREFS_T763119546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDRefs
struct  XsdIDRefs_t763119546  : public XsdName_t1409945702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDREFS_T763119546_H
#ifndef XSDENTITY_T2664305022_H
#define XSDENTITY_T2664305022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEntity
struct  XsdEntity_t2664305022  : public XsdName_t1409945702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENTITY_T2664305022_H
#ifndef XSDSHORT_T1778530041_H
#define XSDSHORT_T1778530041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdShort
struct  XsdShort_t1778530041  : public XsdInt_t1488443894
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSHORT_T1778530041_H
#ifndef XSDNCNAME_T414304927_H
#define XSDNCNAME_T414304927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNCName
struct  XsdNCName_t414304927  : public XsdName_t1409945702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNCNAME_T414304927_H
#ifndef XSDUNSIGNEDINT_T2956447959_H
#define XSDUNSIGNEDINT_T2956447959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedInt
struct  XsdUnsignedInt_t2956447959  : public XsdUnsignedLong_t137890294
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDINT_T2956447959_H
#ifndef XSDNMTOKENS_T1753890866_H
#define XSDNMTOKENS_T1753890866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNMTokens
struct  XsdNMTokens_t1753890866  : public XsdNMToken_t547135877
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNMTOKENS_T1753890866_H
#ifndef XSDBYTE_T1120972221_H
#define XSDBYTE_T1120972221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdByte
struct  XsdByte_t1120972221  : public XsdShort_t1778530041
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBYTE_T1120972221_H
#ifndef XSDUNSIGNEDSHORT_T3693774826_H
#define XSDUNSIGNEDSHORT_T3693774826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedShort
struct  XsdUnsignedShort_t3693774826  : public XsdUnsignedInt_t2956447959
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDSHORT_T3693774826_H
#ifndef XSDUNSIGNEDBYTE_T3216355454_H
#define XSDUNSIGNEDBYTE_T3216355454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedByte
struct  XsdUnsignedByte_t3216355454  : public XsdUnsignedShort_t3693774826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDBYTE_T3216355454_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (AudioPlayableOutput_t3110750149)+ sizeof (RuntimeObject), sizeof(AudioPlayableOutput_t3110750149 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1600[1] = 
{
	AudioPlayableOutput_t3110750149::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (AudioExtensionDefinition_t1867593088), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[4] = 
{
	AudioExtensionDefinition_t1867593088::get_offset_of_assemblyName_0(),
	AudioExtensionDefinition_t1867593088::get_offset_of_extensionNamespace_1(),
	AudioExtensionDefinition_t1867593088::get_offset_of_extensionTypeName_2(),
	AudioExtensionDefinition_t1867593088::get_offset_of_extensionType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (AudioSpatializerExtensionDefinition_t3666104158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1602[3] = 
{
	AudioSpatializerExtensionDefinition_t3666104158::get_offset_of_spatializerName_0(),
	AudioSpatializerExtensionDefinition_t3666104158::get_offset_of_definition_1(),
	AudioSpatializerExtensionDefinition_t3666104158::get_offset_of_editorDefinition_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (AudioAmbisonicExtensionDefinition_t52665437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1603[2] = 
{
	AudioAmbisonicExtensionDefinition_t52665437::get_offset_of_ambisonicPluginName_0(),
	AudioAmbisonicExtensionDefinition_t52665437::get_offset_of_definition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (AudioListenerExtension_t3456787073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1604[1] = 
{
	AudioListenerExtension_t3456787073::get_offset_of_m_audioListener_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (AudioSourceExtension_t1813599864), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[2] = 
{
	AudioSourceExtension_t1813599864::get_offset_of_m_audioSource_2(),
	AudioSourceExtension_t1813599864::get_offset_of_m_ExtensionManagerUpdateIndex_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (Physics2D_t2540166467), -1, sizeof(Physics2D_t2540166467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1607[1] = 
{
	Physics2D_t2540166467_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (Rigidbody2D_t502193897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (Collider2D_t646061738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (RaycastHit2D_t4063908774)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1610[6] = 
{
	RaycastHit2D_t4063908774::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (TextAnchor_t112990806)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1612[10] = 
{
	TextAnchor_t112990806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (HorizontalWrapMode_t2027154177)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1613[3] = 
{
	HorizontalWrapMode_t2027154177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (VerticalWrapMode_t3668245347)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1614[3] = 
{
	VerticalWrapMode_t3668245347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (TextMesh_t1641806576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (Font_t4239498691), -1, sizeof(Font_t4239498691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1616[2] = 
{
	Font_t4239498691_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t4239498691::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (FontTextureRebuildCallback_t1272078033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (UICharInfo_t3056636800)+ sizeof (RuntimeObject), sizeof(UICharInfo_t3056636800 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1618[2] = 
{
	UICharInfo_t3056636800::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UICharInfo_t3056636800::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (UILineInfo_t3621277874)+ sizeof (RuntimeObject), sizeof(UILineInfo_t3621277874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1619[4] = 
{
	UILineInfo_t3621277874::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UILineInfo_t3621277874::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UILineInfo_t3621277874::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UILineInfo_t3621277874::get_offset_of_leading_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (UIVertex_t1204258818)+ sizeof (RuntimeObject), sizeof(UIVertex_t1204258818 ), sizeof(UIVertex_t1204258818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1620[11] = 
{
	UIVertex_t1204258818::get_offset_of_position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIVertex_t1204258818::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIVertex_t1204258818::get_offset_of_color_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIVertex_t1204258818::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIVertex_t1204258818::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIVertex_t1204258818::get_offset_of_uv2_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIVertex_t1204258818::get_offset_of_uv3_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIVertex_t1204258818::get_offset_of_tangent_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultColor_8(),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultTangent_9(),
	UIVertex_t1204258818_StaticFields::get_offset_of_simpleVert_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (TextGenerator_t647235000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1621[11] = 
{
	TextGenerator_t647235000::get_offset_of_m_Ptr_0(),
	TextGenerator_t647235000::get_offset_of_m_LastString_1(),
	TextGenerator_t647235000::get_offset_of_m_LastSettings_2(),
	TextGenerator_t647235000::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t647235000::get_offset_of_m_LastValid_4(),
	TextGenerator_t647235000::get_offset_of_m_Verts_5(),
	TextGenerator_t647235000::get_offset_of_m_Characters_6(),
	TextGenerator_t647235000::get_offset_of_m_Lines_7(),
	TextGenerator_t647235000::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t647235000::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t647235000::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (FontStyle_t2764949590)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1622[5] = 
{
	FontStyle_t2764949590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (TextGenerationError_t780770201)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1623[5] = 
{
	TextGenerationError_t780770201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (TextGenerationSettings_t2543476768)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1624[18] = 
{
	TextGenerationSettings_t2543476768::get_offset_of_font_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_color_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (UploadHandler_t3552561393), sizeof(UploadHandler_t3552561393_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1626[1] = 
{
	UploadHandler_t3552561393::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (UploadHandlerRaw_t3420491431), sizeof(UploadHandlerRaw_t3420491431_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (DownloadHandler_t1216180266), sizeof(DownloadHandler_t1216180266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1628[1] = 
{
	DownloadHandler_t1216180266::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (DownloadHandlerBuffer_t3443159558), sizeof(DownloadHandlerBuffer_t3443159558_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (UnityWebRequestAsyncOperation_t2821884557), sizeof(UnityWebRequestAsyncOperation_t2821884557_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1630[1] = 
{
	UnityWebRequestAsyncOperation_t2821884557::get_offset_of_m_webRequest_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (UnityWebRequest_t254341728), sizeof(UnityWebRequest_t254341728_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1631[3] = 
{
	UnityWebRequest_t254341728::get_offset_of_m_Ptr_0(),
	UnityWebRequest_t254341728::get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_1(),
	UnityWebRequest_t254341728::get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (UnityWebRequestMethod_t2654069489)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1632[6] = 
{
	UnityWebRequestMethod_t2654069489::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (WWWForm_t3950226929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (WWWTranscoder_t1124214756), -1, sizeof(WWWTranscoder_t1124214756_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1634[8] = 
{
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_ucHexChars_0(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_lcHexChars_1(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_urlEscapeChar_2(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_urlSpace_3(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_urlForbidden_4(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_qpEscapeChar_5(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_qpSpace_6(),
	WWWTranscoder_t1124214756_StaticFields::get_offset_of_qpForbidden_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (WebRequestUtils_t4100941042), -1, sizeof(WebRequestUtils_t4100941042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1635[1] = 
{
	WebRequestUtils_t4100941042_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (MonoTODOAttribute_t3487514022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (XmlSchemaValidatingReader_t3861297856), -1, sizeof(XmlSchemaValidatingReader_t3861297856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1638[17] = 
{
	XmlSchemaValidatingReader_t3861297856_StaticFields::get_offset_of_emptyAttributeArray_2(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_reader_3(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_options_4(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_v_5(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_getter_6(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_xsinfo_7(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_readerLineInfo_8(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_nsResolver_9(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_defaultAttributes_10(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_currentDefaultAttribute_11(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_defaultAttributesCache_12(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_defaultAttributeConsumed_13(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_currentAttrType_14(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_validationDone_15(),
	XmlSchemaValidatingReader_t3861297856::get_offset_of_element_16(),
	XmlSchemaValidatingReader_t3861297856_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_17(),
	XmlSchemaValidatingReader_t3861297856_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t2009608413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1639[2] = 
{
	U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t2009608413::get_offset_of_settings_0(),
	U3CXmlSchemaValidatingReaderU3Ec__AnonStorey4_t2009608413::get_offset_of_U3CU3Ef__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (XsdIdentitySelector_t185499482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1640[3] = 
{
	XsdIdentitySelector_t185499482::get_offset_of_selectorPaths_0(),
	XsdIdentitySelector_t185499482::get_offset_of_fields_1(),
	XsdIdentitySelector_t185499482::get_offset_of_cachedFields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (XsdIdentityField_t2563516441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1641[2] = 
{
	XsdIdentityField_t2563516441::get_offset_of_fieldPaths_0(),
	XsdIdentityField_t2563516441::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (XsdIdentityPath_t2037874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1642[2] = 
{
	XsdIdentityPath_t2037874::get_offset_of_OrderedSteps_0(),
	XsdIdentityPath_t2037874::get_offset_of_Descendants_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (XsdIdentityStep_t452377251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1643[6] = 
{
	XsdIdentityStep_t452377251::get_offset_of_IsCurrent_0(),
	XsdIdentityStep_t452377251::get_offset_of_IsAttribute_1(),
	XsdIdentityStep_t452377251::get_offset_of_IsAnyName_2(),
	XsdIdentityStep_t452377251::get_offset_of_NsName_3(),
	XsdIdentityStep_t452377251::get_offset_of_Name_4(),
	XsdIdentityStep_t452377251::get_offset_of_Namespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (XsdKeyEntryField_t3632833996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1644[13] = 
{
	XsdKeyEntryField_t3632833996::get_offset_of_entry_0(),
	XsdKeyEntryField_t3632833996::get_offset_of_field_1(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFound_2(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldLineNumber_3(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldLinePosition_4(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldHasLineInfo_5(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldType_6(),
	XsdKeyEntryField_t3632833996::get_offset_of_Identity_7(),
	XsdKeyEntryField_t3632833996::get_offset_of_IsXsiNil_8(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFoundDepth_9(),
	XsdKeyEntryField_t3632833996::get_offset_of_FieldFoundPath_10(),
	XsdKeyEntryField_t3632833996::get_offset_of_Consuming_11(),
	XsdKeyEntryField_t3632833996::get_offset_of_Consumed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (XsdKeyEntryFieldCollection_t2946985218), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (XsdKeyEntry_t3532015222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[8] = 
{
	XsdKeyEntry_t3532015222::get_offset_of_StartDepth_0(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorLineNumber_1(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorLinePosition_2(),
	XsdKeyEntry_t3532015222::get_offset_of_SelectorHasLineInfo_3(),
	XsdKeyEntry_t3532015222::get_offset_of_KeyFields_4(),
	XsdKeyEntry_t3532015222::get_offset_of_KeyRefFound_5(),
	XsdKeyEntry_t3532015222::get_offset_of_OwnerSequence_6(),
	XsdKeyEntry_t3532015222::get_offset_of_keyFound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (XsdKeyEntryCollection_t1714053544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (XsdKeyTable_t2980902100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1648[9] = 
{
	XsdKeyTable_t2980902100::get_offset_of_alwaysTrue_0(),
	XsdKeyTable_t2980902100::get_offset_of_selector_1(),
	XsdKeyTable_t2980902100::get_offset_of_source_2(),
	XsdKeyTable_t2980902100::get_offset_of_qname_3(),
	XsdKeyTable_t2980902100::get_offset_of_refKeyName_4(),
	XsdKeyTable_t2980902100::get_offset_of_Entries_5(),
	XsdKeyTable_t2980902100::get_offset_of_FinishedEntries_6(),
	XsdKeyTable_t2980902100::get_offset_of_StartDepth_7(),
	XsdKeyTable_t2980902100::get_offset_of_ReferencedKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (XsdParticleStateManager_t4119977941), -1, sizeof(XsdParticleStateManager_t4119977941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1649[6] = 
{
	XsdParticleStateManager_t4119977941::get_offset_of_table_0(),
	XsdParticleStateManager_t4119977941::get_offset_of_processContents_1(),
	XsdParticleStateManager_t4119977941::get_offset_of_CurrentElement_2(),
	XsdParticleStateManager_t4119977941::get_offset_of_ContextStack_3(),
	XsdParticleStateManager_t4119977941::get_offset_of_Context_4(),
	XsdParticleStateManager_t4119977941_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (XsdValidationState_t3545965403), -1, sizeof(XsdValidationState_t3545965403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1650[3] = 
{
	XsdValidationState_t3545965403_StaticFields::get_offset_of_invalid_0(),
	XsdValidationState_t3545965403::get_offset_of_occured_1(),
	XsdValidationState_t3545965403::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (XsdElementValidationState_t152111323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1651[1] = 
{
	XsdElementValidationState_t152111323::get_offset_of_element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (XsdSequenceValidationState_t3542030006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[4] = 
{
	XsdSequenceValidationState_t3542030006::get_offset_of_seq_3(),
	XsdSequenceValidationState_t3542030006::get_offset_of_current_4(),
	XsdSequenceValidationState_t3542030006::get_offset_of_currentAutomata_5(),
	XsdSequenceValidationState_t3542030006::get_offset_of_emptiable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (XsdChoiceValidationState_t1506407122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[3] = 
{
	XsdChoiceValidationState_t1506407122::get_offset_of_choice_3(),
	XsdChoiceValidationState_t1506407122::get_offset_of_emptiable_4(),
	XsdChoiceValidationState_t1506407122::get_offset_of_emptiableComputed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (XsdAllValidationState_t1028212608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[2] = 
{
	XsdAllValidationState_t1028212608::get_offset_of_all_3(),
	XsdAllValidationState_t1028212608::get_offset_of_consumed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (XsdAnyValidationState_t204702461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[1] = 
{
	XsdAnyValidationState_t204702461::get_offset_of_any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (XsdAppendedValidationState_t3724408090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[2] = 
{
	XsdAppendedValidationState_t3724408090::get_offset_of_head_3(),
	XsdAppendedValidationState_t3724408090::get_offset_of_rest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (XsdEmptyValidationState_t1914323912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (XsdInvalidValidationState_t1112885736), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (XsdValidatingReader_t1704923617), -1, sizeof(XsdValidatingReader_t1704923617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1659[30] = 
{
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_emptyAttributeArray_2(),
	XsdValidatingReader_t1704923617::get_offset_of_reader_3(),
	XsdValidatingReader_t1704923617::get_offset_of_resolver_4(),
	XsdValidatingReader_t1704923617::get_offset_of_sourceReaderSchemaInfo_5(),
	XsdValidatingReader_t1704923617::get_offset_of_readerLineInfo_6(),
	XsdValidatingReader_t1704923617::get_offset_of_validationType_7(),
	XsdValidatingReader_t1704923617::get_offset_of_schemas_8(),
	XsdValidatingReader_t1704923617::get_offset_of_namespaces_9(),
	XsdValidatingReader_t1704923617::get_offset_of_validationStarted_10(),
	XsdValidatingReader_t1704923617::get_offset_of_checkIdentity_11(),
	XsdValidatingReader_t1704923617::get_offset_of_idManager_12(),
	XsdValidatingReader_t1704923617::get_offset_of_checkKeyConstraints_13(),
	XsdValidatingReader_t1704923617::get_offset_of_keyTables_14(),
	XsdValidatingReader_t1704923617::get_offset_of_currentKeyFieldConsumers_15(),
	XsdValidatingReader_t1704923617::get_offset_of_tmpKeyrefPool_16(),
	XsdValidatingReader_t1704923617::get_offset_of_elementQNameStack_17(),
	XsdValidatingReader_t1704923617::get_offset_of_state_18(),
	XsdValidatingReader_t1704923617::get_offset_of_skipValidationDepth_19(),
	XsdValidatingReader_t1704923617::get_offset_of_xsiNilDepth_20(),
	XsdValidatingReader_t1704923617::get_offset_of_storedCharacters_21(),
	XsdValidatingReader_t1704923617::get_offset_of_shouldValidateCharacters_22(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributes_23(),
	XsdValidatingReader_t1704923617::get_offset_of_currentDefaultAttribute_24(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributesCache_25(),
	XsdValidatingReader_t1704923617::get_offset_of_defaultAttributeConsumed_26(),
	XsdValidatingReader_t1704923617::get_offset_of_currentAttrType_27(),
	XsdValidatingReader_t1704923617::get_offset_of_ValidationEventHandler_28(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_29(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_30(),
	XsdValidatingReader_t1704923617_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (XsdValidationContext_t3720679969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1660[3] = 
{
	XsdValidationContext_t3720679969::get_offset_of_xsi_type_0(),
	XsdValidationContext_t3720679969::get_offset_of_State_1(),
	XsdValidationContext_t3720679969::get_offset_of_element_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (XsdIDManager_t3860002335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1661[3] = 
{
	XsdIDManager_t3860002335::get_offset_of_idList_0(),
	XsdIDManager_t3860002335::get_offset_of_missingIDReferences_1(),
	XsdIDManager_t3860002335::get_offset_of_thisElementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (XsdWildcard_t625524157), -1, sizeof(XsdWildcard_t625524157_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1662[10] = 
{
	XsdWildcard_t625524157::get_offset_of_xsobj_0(),
	XsdWildcard_t625524157::get_offset_of_ResolvedProcessing_1(),
	XsdWildcard_t625524157::get_offset_of_TargetNamespace_2(),
	XsdWildcard_t625524157::get_offset_of_SkipCompile_3(),
	XsdWildcard_t625524157::get_offset_of_HasValueAny_4(),
	XsdWildcard_t625524157::get_offset_of_HasValueLocal_5(),
	XsdWildcard_t625524157::get_offset_of_HasValueOther_6(),
	XsdWildcard_t625524157::get_offset_of_HasValueTargetNamespace_7(),
	XsdWildcard_t625524157::get_offset_of_ResolvedNamespaces_8(),
	XsdWildcard_t625524157_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (XmlFilterReader_t256953178), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[3] = 
{
	XmlFilterReader_t256953178::get_offset_of_reader_2(),
	XmlFilterReader_t256953178::get_offset_of_settings_3(),
	XmlFilterReader_t256953178::get_offset_of_lineInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (XsdWhitespaceFacet_t2758097827)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1666[4] = 
{
	XsdWhitespaceFacet_t2758097827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (XsdOrdering_t3741887935)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1667[5] = 
{
	XsdOrdering_t3741887935::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (XsdAnySimpleType_t1096449895), -1, sizeof(XsdAnySimpleType_t1096449895_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1668[6] = 
{
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t1096449895_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (XdtAnyAtomicType_t3359210273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (XdtUntypedAtomic_t2904699188), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (XsdString_t263933896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (XsdNormalizedString_t2132216291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (XsdToken_t2902215462), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (XsdLanguage_t3897851897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (XsdNMToken_t547135877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (XsdNMTokens_t1753890866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (XsdName_t1409945702), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (XsdNCName_t414304927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (XsdID_t1067193160), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (XsdIDRef_t1377311049), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (XsdIDRefs_t763119546), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (XsdEntity_t2664305022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (XsdEntities_t1103053540), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (XsdNotation_t720379093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (XsdDecimal_t2932251550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (XsdInteger_t1330502157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (XsdLong_t4179235589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (XsdInt_t1488443894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (XsdShort_t1778530041), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (XsdByte_t1120972221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (XsdNonNegativeInteger_t3587933853), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (XsdUnsignedLong_t137890294), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (XsdUnsignedInt_t2956447959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (XsdUnsignedShort_t3693774826), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (XsdUnsignedByte_t3216355454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (XsdPositiveInteger_t1896481288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (XsdNonPositiveInteger_t409343009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (XsdNegativeInteger_t399444136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (XsdFloat_t386143221), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
