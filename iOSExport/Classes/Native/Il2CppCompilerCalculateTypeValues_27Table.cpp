﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Material
struct Material_t193706927;
// TMPro.TMP_InputField
struct TMP_InputField_t1778301588;
// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/MaskingMaterial>
struct List_1_t4254159116;
// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_MaterialManager/FallbackMaterial>
struct Dictionary_2_t4275507642;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int64>
struct Dictionary_2_t4211870968;
// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/FallbackMaterial>
struct List_1_t2655110372;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Char>
struct Dictionary_2_t2462306973;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<TMPro.TMP_Text>
struct List_1_t1289121909;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1079703083;
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_t355641911;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData>
struct List_1_t3921943330;
// TMPro.TMP_Dropdown
struct TMP_Dropdown_t1768193147;
// TMPro.TMP_Dropdown/DropdownItem
struct DropdownItem_t1251916390;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2295673753;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t339478082;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Collections.Generic.List`1<TMPro.TMP_Dropdown/OptionData>
struct List_1_t3898801349;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Void
struct Void_t1841601450;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t2530419979;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t2641813093;
// TMPro.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t2368686856;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Byte
struct Byte_t3683104436;
// System.Double
struct Double_t4078015681;
// System.UInt16
struct UInt16_t986882611;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t2828559218;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_t1159837347;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// TMPro.TMP_Text
struct TMP_Text_t1920000777;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t602810366;
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t968836101;
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t2563924433;
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t1617569211;
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_t2788613100;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t2398608976;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t627890505;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t1658499504;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t1615060493;
// TMPro.TMP_TextElement
struct TMP_TextElement_t2285620223;
// TMPro.ColorTween/ColorTweenCallback
struct ColorTweenCallback_t1307749522;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t2849466151;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_t2347923044;
// System.Collections.Generic.List`1<TMPro.TMP_Style>
struct List_1_t3733825879;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style>
struct Dictionary_2_t3372530382;
// System.Collections.Generic.List`1<TMPro.TMP_FontAsset>
struct List_1_t1899541111;
// TMPro.TMP_StyleSheet
struct TMP_StyleSheet_t335925700;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// TMPro.TMP_Settings/LineBreakingTable
struct LineBreakingTable_t2043799287;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// TMPro.FaceInfo
struct FaceInfo_t3239700425;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Collections.Generic.List`1<TMPro.TMP_Glyph>
struct List_1_t278915034;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Glyph>
struct Dictionary_2_t4212586833;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.KerningPair>
struct Dictionary_2_t585579557;
// TMPro.KerningTable
struct KerningTable_t2970824110;
// TMPro.KerningPair
struct KerningPair_t1577753922;
// TMPro.TMP_FontWeights[]
struct TMP_FontWeightsU5BU5D_t3725552155;
// System.Func`2<TMPro.TMP_Glyph,System.Int32>
struct Func_2_t1421371187;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Collections.Generic.List`1<TMPro.TMP_Sprite>
struct List_1_t3768471933;
// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset>
struct List_1_t2010934225;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// TMPro.TextMeshPro
struct TextMeshPro_t2521834357;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t2833400353;
// UnityEngine.Gyroscope
struct Gyroscope_t1705362817;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable>
struct List_1_t859513320;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t3244928895;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2665681875;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t601950206;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t3248359358;
// TMPro.TMP_ScrollbarEventHandler
struct TMP_ScrollbarEventHandler_t1222271750;
// TMPro.TMP_InputField/SubmitEvent
struct SubmitEvent_t3359162065;
// TMPro.TMP_InputField/SelectionEvent
struct SelectionEvent_t3883897865;
// TMPro.TMP_InputField/TextSelectionEvent
struct TextSelectionEvent_t3887183206;
// TMPro.TMP_InputField/OnChangeEvent
struct OnChangeEvent_t2139251414;
// TMPro.TMP_InputField/OnValidateInput
struct OnValidateInput_t3285190392;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// TMPro.TMP_InputValidator
struct TMP_InputValidator_t3726817866;
// UnityEngine.Event
struct Event_t3028476042;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// TMPro.TMP_Dropdown/OptionDataList
struct OptionDataList_t457963479;
// TMPro.TMP_Dropdown/DropdownEvent
struct DropdownEvent_t1859881917;
// System.Collections.Generic.List`1<TMPro.TMP_Dropdown/DropdownItem>
struct List_1_t621037522;
// TMPro.TweenRunner`1<TMPro.FloatTween>
struct TweenRunner_1_t1736487048;
// TMPro.TMP_Dropdown/OptionData
struct OptionData_t234712921;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3123989686;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.LayoutElement
struct LayoutElement_t2808691390;
// TMPro.XML_TagAttribute[]
struct XML_TagAttributeU5BU5D_t573465953;
// TMPro.TMP_Glyph
struct TMP_Glyph_t909793902;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t934157183;

struct Vector3_t2243707580 ;
struct Vector4_t2243707581 ;
struct Vector2_t2243707579 ;
struct Color32_t874517518 ;



#ifndef U3CMODULEU3E_T3783534239_H
#define U3CMODULEU3E_T3783534239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534239 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534239_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TMP_TEXTELEMENT_T2285620223_H
#define TMP_TEXTELEMENT_T2285620223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_t2285620223  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_t2285620223, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_T2285620223_H
#ifndef U3CADDMASKINGMATERIALU3EC__ANONSTOREY1_T1699651275_H
#define U3CADDMASKINGMATERIALU3EC__ANONSTOREY1_T1699651275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<AddMaskingMaterial>c__AnonStorey1
struct  U3CAddMaskingMaterialU3Ec__AnonStorey1_t1699651275  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<AddMaskingMaterial>c__AnonStorey1::stencilMaterial
	Material_t193706927 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CAddMaskingMaterialU3Ec__AnonStorey1_t1699651275, ___stencilMaterial_0)); }
	inline Material_t193706927 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t193706927 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t193706927 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CADDMASKINGMATERIALU3EC__ANONSTOREY1_T1699651275_H
#ifndef U3CRELEASEBASEMATERIALU3EC__ANONSTOREY3_T3729327270_H
#define U3CRELEASEBASEMATERIALU3EC__ANONSTOREY3_T3729327270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<ReleaseBaseMaterial>c__AnonStorey3
struct  U3CReleaseBaseMaterialU3Ec__AnonStorey3_t3729327270  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<ReleaseBaseMaterial>c__AnonStorey3::baseMaterial
	Material_t193706927 * ___baseMaterial_0;

public:
	inline static int32_t get_offset_of_baseMaterial_0() { return static_cast<int32_t>(offsetof(U3CReleaseBaseMaterialU3Ec__AnonStorey3_t3729327270, ___baseMaterial_0)); }
	inline Material_t193706927 * get_baseMaterial_0() const { return ___baseMaterial_0; }
	inline Material_t193706927 ** get_address_of_baseMaterial_0() { return &___baseMaterial_0; }
	inline void set_baseMaterial_0(Material_t193706927 * value)
	{
		___baseMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRELEASEBASEMATERIALU3EC__ANONSTOREY3_T3729327270_H
#ifndef U3CREMOVESTENCILMATERIALU3EC__ANONSTOREY2_T4145099293_H
#define U3CREMOVESTENCILMATERIALU3EC__ANONSTOREY2_T4145099293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<RemoveStencilMaterial>c__AnonStorey2
struct  U3CRemoveStencilMaterialU3Ec__AnonStorey2_t4145099293  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<RemoveStencilMaterial>c__AnonStorey2::stencilMaterial
	Material_t193706927 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CRemoveStencilMaterialU3Ec__AnonStorey2_t4145099293, ___stencilMaterial_0)); }
	inline Material_t193706927 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t193706927 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t193706927 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVESTENCILMATERIALU3EC__ANONSTOREY2_T4145099293_H
#ifndef MASKINGMATERIAL_T590070688_H
#define MASKINGMATERIAL_T590070688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/MaskingMaterial
struct  MaskingMaterial_t590070688  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/MaskingMaterial::baseMaterial
	Material_t193706927 * ___baseMaterial_0;
	// UnityEngine.Material TMPro.TMP_MaterialManager/MaskingMaterial::stencilMaterial
	Material_t193706927 * ___stencilMaterial_1;
	// System.Int32 TMPro.TMP_MaterialManager/MaskingMaterial::count
	int32_t ___count_2;
	// System.Int32 TMPro.TMP_MaterialManager/MaskingMaterial::stencilID
	int32_t ___stencilID_3;

public:
	inline static int32_t get_offset_of_baseMaterial_0() { return static_cast<int32_t>(offsetof(MaskingMaterial_t590070688, ___baseMaterial_0)); }
	inline Material_t193706927 * get_baseMaterial_0() const { return ___baseMaterial_0; }
	inline Material_t193706927 ** get_address_of_baseMaterial_0() { return &___baseMaterial_0; }
	inline void set_baseMaterial_0(Material_t193706927 * value)
	{
		___baseMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_0), value);
	}

	inline static int32_t get_offset_of_stencilMaterial_1() { return static_cast<int32_t>(offsetof(MaskingMaterial_t590070688, ___stencilMaterial_1)); }
	inline Material_t193706927 * get_stencilMaterial_1() const { return ___stencilMaterial_1; }
	inline Material_t193706927 ** get_address_of_stencilMaterial_1() { return &___stencilMaterial_1; }
	inline void set_stencilMaterial_1(Material_t193706927 * value)
	{
		___stencilMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(MaskingMaterial_t590070688, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_stencilID_3() { return static_cast<int32_t>(offsetof(MaskingMaterial_t590070688, ___stencilID_3)); }
	inline int32_t get_stencilID_3() const { return ___stencilID_3; }
	inline int32_t* get_address_of_stencilID_3() { return &___stencilID_3; }
	inline void set_stencilID_3(int32_t value)
	{
		___stencilID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGMATERIAL_T590070688_H
#ifndef SETPROPERTYUTILITY_T4168857211_H
#define SETPROPERTYUTILITY_T4168857211_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SetPropertyUtility
struct  SetPropertyUtility_t4168857211  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETPROPERTYUTILITY_T4168857211_H
#ifndef U3CCARETBLINKU3EC__ITERATOR0_T2727982073_H
#define U3CCARETBLINKU3EC__ITERATOR0_T2727982073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/<CaretBlink>c__Iterator0
struct  U3CCaretBlinkU3Ec__Iterator0_t2727982073  : public RuntimeObject
{
public:
	// System.Single TMPro.TMP_InputField/<CaretBlink>c__Iterator0::<blinkPeriod>__1
	float ___U3CblinkPeriodU3E__1_0;
	// System.Boolean TMPro.TMP_InputField/<CaretBlink>c__Iterator0::<blinkState>__1
	bool ___U3CblinkStateU3E__1_1;
	// TMPro.TMP_InputField TMPro.TMP_InputField/<CaretBlink>c__Iterator0::$this
	TMP_InputField_t1778301588 * ___U24this_2;
	// System.Object TMPro.TMP_InputField/<CaretBlink>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean TMPro.TMP_InputField/<CaretBlink>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 TMPro.TMP_InputField/<CaretBlink>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CblinkPeriodU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t2727982073, ___U3CblinkPeriodU3E__1_0)); }
	inline float get_U3CblinkPeriodU3E__1_0() const { return ___U3CblinkPeriodU3E__1_0; }
	inline float* get_address_of_U3CblinkPeriodU3E__1_0() { return &___U3CblinkPeriodU3E__1_0; }
	inline void set_U3CblinkPeriodU3E__1_0(float value)
	{
		___U3CblinkPeriodU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CblinkStateU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t2727982073, ___U3CblinkStateU3E__1_1)); }
	inline bool get_U3CblinkStateU3E__1_1() const { return ___U3CblinkStateU3E__1_1; }
	inline bool* get_address_of_U3CblinkStateU3E__1_1() { return &___U3CblinkStateU3E__1_1; }
	inline void set_U3CblinkStateU3E__1_1(bool value)
	{
		___U3CblinkStateU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t2727982073, ___U24this_2)); }
	inline TMP_InputField_t1778301588 * get_U24this_2() const { return ___U24this_2; }
	inline TMP_InputField_t1778301588 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(TMP_InputField_t1778301588 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t2727982073, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t2727982073, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CCaretBlinkU3Ec__Iterator0_t2727982073, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCARETBLINKU3EC__ITERATOR0_T2727982073_H
#ifndef FALLBACKMATERIAL_T3285989240_H
#define FALLBACKMATERIAL_T3285989240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/FallbackMaterial
struct  FallbackMaterial_t3285989240  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_MaterialManager/FallbackMaterial::baseID
	int32_t ___baseID_0;
	// UnityEngine.Material TMPro.TMP_MaterialManager/FallbackMaterial::baseMaterial
	Material_t193706927 * ___baseMaterial_1;
	// System.Int64 TMPro.TMP_MaterialManager/FallbackMaterial::fallbackID
	int64_t ___fallbackID_2;
	// UnityEngine.Material TMPro.TMP_MaterialManager/FallbackMaterial::fallbackMaterial
	Material_t193706927 * ___fallbackMaterial_3;
	// System.Int32 TMPro.TMP_MaterialManager/FallbackMaterial::count
	int32_t ___count_4;

public:
	inline static int32_t get_offset_of_baseID_0() { return static_cast<int32_t>(offsetof(FallbackMaterial_t3285989240, ___baseID_0)); }
	inline int32_t get_baseID_0() const { return ___baseID_0; }
	inline int32_t* get_address_of_baseID_0() { return &___baseID_0; }
	inline void set_baseID_0(int32_t value)
	{
		___baseID_0 = value;
	}

	inline static int32_t get_offset_of_baseMaterial_1() { return static_cast<int32_t>(offsetof(FallbackMaterial_t3285989240, ___baseMaterial_1)); }
	inline Material_t193706927 * get_baseMaterial_1() const { return ___baseMaterial_1; }
	inline Material_t193706927 ** get_address_of_baseMaterial_1() { return &___baseMaterial_1; }
	inline void set_baseMaterial_1(Material_t193706927 * value)
	{
		___baseMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___baseMaterial_1), value);
	}

	inline static int32_t get_offset_of_fallbackID_2() { return static_cast<int32_t>(offsetof(FallbackMaterial_t3285989240, ___fallbackID_2)); }
	inline int64_t get_fallbackID_2() const { return ___fallbackID_2; }
	inline int64_t* get_address_of_fallbackID_2() { return &___fallbackID_2; }
	inline void set_fallbackID_2(int64_t value)
	{
		___fallbackID_2 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_3() { return static_cast<int32_t>(offsetof(FallbackMaterial_t3285989240, ___fallbackMaterial_3)); }
	inline Material_t193706927 * get_fallbackMaterial_3() const { return ___fallbackMaterial_3; }
	inline Material_t193706927 ** get_address_of_fallbackMaterial_3() { return &___fallbackMaterial_3; }
	inline void set_fallbackMaterial_3(Material_t193706927 * value)
	{
		___fallbackMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_3), value);
	}

	inline static int32_t get_offset_of_count_4() { return static_cast<int32_t>(offsetof(FallbackMaterial_t3285989240, ___count_4)); }
	inline int32_t get_count_4() const { return ___count_4; }
	inline int32_t* get_address_of_count_4() { return &___count_4; }
	inline void set_count_4(int32_t value)
	{
		___count_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FALLBACKMATERIAL_T3285989240_H
#ifndef TMP_MATERIALMANAGER_T3562209168_H
#define TMP_MATERIALMANAGER_T3562209168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager
struct  TMP_MaterialManager_t3562209168  : public RuntimeObject
{
public:

public:
};

struct TMP_MaterialManager_t3562209168_StaticFields
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/MaskingMaterial> TMPro.TMP_MaterialManager::m_materialList
	List_1_t4254159116 * ___m_materialList_0;
	// System.Collections.Generic.Dictionary`2<System.Int64,TMPro.TMP_MaterialManager/FallbackMaterial> TMPro.TMP_MaterialManager::m_fallbackMaterials
	Dictionary_2_t4275507642 * ___m_fallbackMaterials_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int64> TMPro.TMP_MaterialManager::m_fallbackMaterialLookup
	Dictionary_2_t4211870968 * ___m_fallbackMaterialLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_MaterialManager/FallbackMaterial> TMPro.TMP_MaterialManager::m_fallbackCleanupList
	List_1_t2655110372 * ___m_fallbackCleanupList_3;
	// System.Boolean TMPro.TMP_MaterialManager::isFallbackListDirty
	bool ___isFallbackListDirty_4;

public:
	inline static int32_t get_offset_of_m_materialList_0() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t3562209168_StaticFields, ___m_materialList_0)); }
	inline List_1_t4254159116 * get_m_materialList_0() const { return ___m_materialList_0; }
	inline List_1_t4254159116 ** get_address_of_m_materialList_0() { return &___m_materialList_0; }
	inline void set_m_materialList_0(List_1_t4254159116 * value)
	{
		___m_materialList_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialList_0), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterials_1() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t3562209168_StaticFields, ___m_fallbackMaterials_1)); }
	inline Dictionary_2_t4275507642 * get_m_fallbackMaterials_1() const { return ___m_fallbackMaterials_1; }
	inline Dictionary_2_t4275507642 ** get_address_of_m_fallbackMaterials_1() { return &___m_fallbackMaterials_1; }
	inline void set_m_fallbackMaterials_1(Dictionary_2_t4275507642 * value)
	{
		___m_fallbackMaterials_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterials_1), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterialLookup_2() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t3562209168_StaticFields, ___m_fallbackMaterialLookup_2)); }
	inline Dictionary_2_t4211870968 * get_m_fallbackMaterialLookup_2() const { return ___m_fallbackMaterialLookup_2; }
	inline Dictionary_2_t4211870968 ** get_address_of_m_fallbackMaterialLookup_2() { return &___m_fallbackMaterialLookup_2; }
	inline void set_m_fallbackMaterialLookup_2(Dictionary_2_t4211870968 * value)
	{
		___m_fallbackMaterialLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterialLookup_2), value);
	}

	inline static int32_t get_offset_of_m_fallbackCleanupList_3() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t3562209168_StaticFields, ___m_fallbackCleanupList_3)); }
	inline List_1_t2655110372 * get_m_fallbackCleanupList_3() const { return ___m_fallbackCleanupList_3; }
	inline List_1_t2655110372 ** get_address_of_m_fallbackCleanupList_3() { return &___m_fallbackCleanupList_3; }
	inline void set_m_fallbackCleanupList_3(List_1_t2655110372 * value)
	{
		___m_fallbackCleanupList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackCleanupList_3), value);
	}

	inline static int32_t get_offset_of_isFallbackListDirty_4() { return static_cast<int32_t>(offsetof(TMP_MaterialManager_t3562209168_StaticFields, ___isFallbackListDirty_4)); }
	inline bool get_isFallbackListDirty_4() const { return ___isFallbackListDirty_4; }
	inline bool* get_address_of_isFallbackListDirty_4() { return &___isFallbackListDirty_4; }
	inline void set_isFallbackListDirty_4(bool value)
	{
		___isFallbackListDirty_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATERIALMANAGER_T3562209168_H
#ifndef LINEBREAKINGTABLE_T2043799287_H
#define LINEBREAKINGTABLE_T2043799287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Settings/LineBreakingTable
struct  LineBreakingTable_t2043799287  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Char> TMPro.TMP_Settings/LineBreakingTable::leadingCharacters
	Dictionary_2_t2462306973 * ___leadingCharacters_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Char> TMPro.TMP_Settings/LineBreakingTable::followingCharacters
	Dictionary_2_t2462306973 * ___followingCharacters_1;

public:
	inline static int32_t get_offset_of_leadingCharacters_0() { return static_cast<int32_t>(offsetof(LineBreakingTable_t2043799287, ___leadingCharacters_0)); }
	inline Dictionary_2_t2462306973 * get_leadingCharacters_0() const { return ___leadingCharacters_0; }
	inline Dictionary_2_t2462306973 ** get_address_of_leadingCharacters_0() { return &___leadingCharacters_0; }
	inline void set_leadingCharacters_0(Dictionary_2_t2462306973 * value)
	{
		___leadingCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___leadingCharacters_0), value);
	}

	inline static int32_t get_offset_of_followingCharacters_1() { return static_cast<int32_t>(offsetof(LineBreakingTable_t2043799287, ___followingCharacters_1)); }
	inline Dictionary_2_t2462306973 * get_followingCharacters_1() const { return ___followingCharacters_1; }
	inline Dictionary_2_t2462306973 ** get_address_of_followingCharacters_1() { return &___followingCharacters_1; }
	inline void set_followingCharacters_1(Dictionary_2_t2462306973 * value)
	{
		___followingCharacters_1 = value;
		Il2CppCodeGenWriteBarrier((&___followingCharacters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEBREAKINGTABLE_T2043799287_H
#ifndef BEZIER_T2878762569_H
#define BEZIER_T2878762569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bezier
struct  Bezier_t2878762569  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIER_T2878762569_H
#ifndef BEZIERPATH_T978602816_H
#define BEZIERPATH_T978602816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BezierPath
struct  BezierPath_t978602816  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector2> BezierPath::points
	List_1_t1612828711 * ___points_0;
	// System.Boolean BezierPath::isClosed
	bool ___isClosed_1;
	// System.Boolean BezierPath::autoSetControlPoints
	bool ___autoSetControlPoints_2;

public:
	inline static int32_t get_offset_of_points_0() { return static_cast<int32_t>(offsetof(BezierPath_t978602816, ___points_0)); }
	inline List_1_t1612828711 * get_points_0() const { return ___points_0; }
	inline List_1_t1612828711 ** get_address_of_points_0() { return &___points_0; }
	inline void set_points_0(List_1_t1612828711 * value)
	{
		___points_0 = value;
		Il2CppCodeGenWriteBarrier((&___points_0), value);
	}

	inline static int32_t get_offset_of_isClosed_1() { return static_cast<int32_t>(offsetof(BezierPath_t978602816, ___isClosed_1)); }
	inline bool get_isClosed_1() const { return ___isClosed_1; }
	inline bool* get_address_of_isClosed_1() { return &___isClosed_1; }
	inline void set_isClosed_1(bool value)
	{
		___isClosed_1 = value;
	}

	inline static int32_t get_offset_of_autoSetControlPoints_2() { return static_cast<int32_t>(offsetof(BezierPath_t978602816, ___autoSetControlPoints_2)); }
	inline bool get_autoSetControlPoints_2() const { return ___autoSetControlPoints_2; }
	inline bool* get_address_of_autoSetControlPoints_2() { return &___autoSetControlPoints_2; }
	inline void set_autoSetControlPoints_2(bool value)
	{
		___autoSetControlPoints_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEZIERPATH_T978602816_H
#ifndef TMP_UPDATEMANAGER_T505251708_H
#define TMP_UPDATEMANAGER_T505251708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateManager
struct  TMP_UpdateManager_t505251708  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_LayoutRebuildQueue
	List_1_t1289121909 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_LayoutQueueLookup
	Dictionary_2_t1079703083 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_GraphicRebuildQueue
	List_1_t1289121909 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_GraphicQueueLookup
	Dictionary_2_t1079703083 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t505251708, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t1289121909 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t1289121909 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t1289121909 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t505251708, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1079703083 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1079703083 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1079703083 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t505251708, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t1289121909 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t1289121909 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t1289121909 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t505251708, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1079703083 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1079703083 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1079703083 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateManager_t505251708_StaticFields
{
public:
	// TMPro.TMP_UpdateManager TMPro.TMP_UpdateManager::s_Instance
	TMP_UpdateManager_t505251708 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t505251708_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateManager_t505251708 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateManager_t505251708 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateManager_t505251708 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEMANAGER_T505251708_H
#ifndef TMP_UPDATEREGISTRY_T2664963242_H
#define TMP_UPDATEREGISTRY_T2664963242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateRegistry
struct  TMP_UpdateRegistry_t2664963242  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_LayoutRebuildQueue
	List_1_t355641911 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_LayoutQueueLookup
	Dictionary_2_t1079703083 * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_GraphicRebuildQueue
	List_1_t355641911 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_GraphicQueueLookup
	Dictionary_2_t1079703083 * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t2664963242, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t355641911 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t355641911 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t355641911 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t2664963242, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_t1079703083 * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_t1079703083 ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_t1079703083 * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t2664963242, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t355641911 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t355641911 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t355641911 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t2664963242, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_t1079703083 * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_t1079703083 ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_t1079703083 * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateRegistry_t2664963242_StaticFields
{
public:
	// TMPro.TMP_UpdateRegistry TMPro.TMP_UpdateRegistry::s_Instance
	TMP_UpdateRegistry_t2664963242 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_t2664963242_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateRegistry_t2664963242 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateRegistry_t2664963242 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateRegistry_t2664963242 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEREGISTRY_T2664963242_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef TEXTUREPACKER_T950091243_H
#define TEXTUREPACKER_T950091243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker
struct  TexturePacker_t950091243  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPACKER_T950091243_H
#ifndef TMP_TEXTUTILITIES_T2068549775_H
#define TMP_TEXTUTILITIES_T2068549775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities
struct  TMP_TextUtilities_t2068549775  : public RuntimeObject
{
public:

public:
};

struct TMP_TextUtilities_t2068549775_StaticFields
{
public:
	// UnityEngine.Vector3[] TMPro.TMP_TextUtilities::m_rectWorldCorners
	Vector3U5BU5D_t1172311765* ___m_rectWorldCorners_0;

public:
	inline static int32_t get_offset_of_m_rectWorldCorners_0() { return static_cast<int32_t>(offsetof(TMP_TextUtilities_t2068549775_StaticFields, ___m_rectWorldCorners_0)); }
	inline Vector3U5BU5D_t1172311765* get_m_rectWorldCorners_0() const { return ___m_rectWorldCorners_0; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_rectWorldCorners_0() { return &___m_rectWorldCorners_0; }
	inline void set_m_rectWorldCorners_0(Vector3U5BU5D_t1172311765* value)
	{
		___m_rectWorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectWorldCorners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTUTILITIES_T2068549775_H
#ifndef TMP_STYLE_T69737451_H
#define TMP_STYLE_T69737451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Style
struct  TMP_Style_t69737451  : public RuntimeObject
{
public:
	// System.String TMPro.TMP_Style::m_Name
	String_t* ___m_Name_0;
	// System.Int32 TMPro.TMP_Style::m_HashCode
	int32_t ___m_HashCode_1;
	// System.String TMPro.TMP_Style::m_OpeningDefinition
	String_t* ___m_OpeningDefinition_2;
	// System.String TMPro.TMP_Style::m_ClosingDefinition
	String_t* ___m_ClosingDefinition_3;
	// System.Int32[] TMPro.TMP_Style::m_OpeningTagArray
	Int32U5BU5D_t3030399641* ___m_OpeningTagArray_4;
	// System.Int32[] TMPro.TMP_Style::m_ClosingTagArray
	Int32U5BU5D_t3030399641* ___m_ClosingTagArray_5;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(TMP_Style_t69737451, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_0), value);
	}

	inline static int32_t get_offset_of_m_HashCode_1() { return static_cast<int32_t>(offsetof(TMP_Style_t69737451, ___m_HashCode_1)); }
	inline int32_t get_m_HashCode_1() const { return ___m_HashCode_1; }
	inline int32_t* get_address_of_m_HashCode_1() { return &___m_HashCode_1; }
	inline void set_m_HashCode_1(int32_t value)
	{
		___m_HashCode_1 = value;
	}

	inline static int32_t get_offset_of_m_OpeningDefinition_2() { return static_cast<int32_t>(offsetof(TMP_Style_t69737451, ___m_OpeningDefinition_2)); }
	inline String_t* get_m_OpeningDefinition_2() const { return ___m_OpeningDefinition_2; }
	inline String_t** get_address_of_m_OpeningDefinition_2() { return &___m_OpeningDefinition_2; }
	inline void set_m_OpeningDefinition_2(String_t* value)
	{
		___m_OpeningDefinition_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningDefinition_2), value);
	}

	inline static int32_t get_offset_of_m_ClosingDefinition_3() { return static_cast<int32_t>(offsetof(TMP_Style_t69737451, ___m_ClosingDefinition_3)); }
	inline String_t* get_m_ClosingDefinition_3() const { return ___m_ClosingDefinition_3; }
	inline String_t** get_address_of_m_ClosingDefinition_3() { return &___m_ClosingDefinition_3; }
	inline void set_m_ClosingDefinition_3(String_t* value)
	{
		___m_ClosingDefinition_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingDefinition_3), value);
	}

	inline static int32_t get_offset_of_m_OpeningTagArray_4() { return static_cast<int32_t>(offsetof(TMP_Style_t69737451, ___m_OpeningTagArray_4)); }
	inline Int32U5BU5D_t3030399641* get_m_OpeningTagArray_4() const { return ___m_OpeningTagArray_4; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_OpeningTagArray_4() { return &___m_OpeningTagArray_4; }
	inline void set_m_OpeningTagArray_4(Int32U5BU5D_t3030399641* value)
	{
		___m_OpeningTagArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningTagArray_4), value);
	}

	inline static int32_t get_offset_of_m_ClosingTagArray_5() { return static_cast<int32_t>(offsetof(TMP_Style_t69737451, ___m_ClosingTagArray_5)); }
	inline Int32U5BU5D_t3030399641* get_m_ClosingTagArray_5() const { return ___m_ClosingTagArray_5; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_ClosingTagArray_5() { return &___m_ClosingTagArray_5; }
	inline void set_m_ClosingTagArray_5(Int32U5BU5D_t3030399641* value)
	{
		___m_ClosingTagArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingTagArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLE_T69737451_H
#ifndef SPRITEDATAOBJECT_T1790914055_H
#define SPRITEDATAOBJECT_T1790914055_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject
struct  SpriteDataObject_t1790914055  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData> TMPro.SpriteAssetUtilities.TexturePacker/SpriteDataObject::frames
	List_1_t3921943330 * ___frames_0;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(SpriteDataObject_t1790914055, ___frames_0)); }
	inline List_1_t3921943330 * get_frames_0() const { return ___frames_0; }
	inline List_1_t3921943330 ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_t3921943330 * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEDATAOBJECT_T1790914055_H
#ifndef U3CDELAYEDDESTROYDROPDOWNLISTU3EC__ITERATOR0_T3901319626_H
#define U3CDELAYEDDESTROYDROPDOWNLISTU3EC__ITERATOR0_T3901319626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>c__Iterator0
struct  U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626  : public RuntimeObject
{
public:
	// System.Single TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>c__Iterator0::delay
	float ___delay_0;
	// TMPro.TMP_Dropdown TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>c__Iterator0::$this
	TMP_Dropdown_t1768193147 * ___U24this_1;
	// System.Object TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.TMP_Dropdown/<DelayedDestroyDropdownList>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_delay_0() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626, ___delay_0)); }
	inline float get_delay_0() const { return ___delay_0; }
	inline float* get_address_of_delay_0() { return &___delay_0; }
	inline void set_delay_0(float value)
	{
		___delay_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626, ___U24this_1)); }
	inline TMP_Dropdown_t1768193147 * get_U24this_1() const { return ___U24this_1; }
	inline TMP_Dropdown_t1768193147 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TMP_Dropdown_t1768193147 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDDESTROYDROPDOWNLISTU3EC__ITERATOR0_T3901319626_H
#ifndef U3CSHOWU3EC__ANONSTOREY1_T540509321_H
#define U3CSHOWU3EC__ANONSTOREY1_T540509321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/<Show>c__AnonStorey1
struct  U3CShowU3Ec__AnonStorey1_t540509321  : public RuntimeObject
{
public:
	// TMPro.TMP_Dropdown/DropdownItem TMPro.TMP_Dropdown/<Show>c__AnonStorey1::item
	DropdownItem_t1251916390 * ___item_0;
	// TMPro.TMP_Dropdown TMPro.TMP_Dropdown/<Show>c__AnonStorey1::$this
	TMP_Dropdown_t1768193147 * ___U24this_1;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CShowU3Ec__AnonStorey1_t540509321, ___item_0)); }
	inline DropdownItem_t1251916390 * get_item_0() const { return ___item_0; }
	inline DropdownItem_t1251916390 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(DropdownItem_t1251916390 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CShowU3Ec__AnonStorey1_t540509321, ___U24this_1)); }
	inline TMP_Dropdown_t1768193147 * get_U24this_1() const { return ___U24this_1; }
	inline TMP_Dropdown_t1768193147 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TMP_Dropdown_t1768193147 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWU3EC__ANONSTOREY1_T540509321_H
#ifndef UNITYEVENTBASE_T828812576_H
#define UNITYEVENTBASE_T828812576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t828812576  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2295673753 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t339478082 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_Calls_0)); }
	inline InvokableCallList_t2295673753 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2295673753 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2295673753 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t339478082 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t339478082 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t339478082 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t828812576, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T828812576_H
#ifndef OPTIONDATA_T234712921_H
#define OPTIONDATA_T234712921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/OptionData
struct  OptionData_t234712921  : public RuntimeObject
{
public:
	// System.String TMPro.TMP_Dropdown/OptionData::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Sprite TMPro.TMP_Dropdown/OptionData::m_Image
	Sprite_t309593783 * ___m_Image_1;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(OptionData_t234712921, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_0), value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(OptionData_t234712921, ___m_Image_1)); }
	inline Sprite_t309593783 * get_m_Image_1() const { return ___m_Image_1; }
	inline Sprite_t309593783 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Sprite_t309593783 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONDATA_T234712921_H
#ifndef U3CGETBASEMATERIALU3EC__ANONSTOREY0_T1824273780_H
#define U3CGETBASEMATERIALU3EC__ANONSTOREY0_T1824273780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<GetBaseMaterial>c__AnonStorey0
struct  U3CGetBaseMaterialU3Ec__AnonStorey0_t1824273780  : public RuntimeObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<GetBaseMaterial>c__AnonStorey0::stencilMaterial
	Material_t193706927 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CGetBaseMaterialU3Ec__AnonStorey0_t1824273780, ___stencilMaterial_0)); }
	inline Material_t193706927 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t193706927 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t193706927 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___stencilMaterial_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETBASEMATERIALU3EC__ANONSTOREY0_T1824273780_H
#ifndef OPTIONDATALIST_T457963479_H
#define OPTIONDATALIST_T457963479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/OptionDataList
struct  OptionDataList_t457963479  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Dropdown/OptionData> TMPro.TMP_Dropdown/OptionDataList::m_Options
	List_1_t3898801349 * ___m_Options_0;

public:
	inline static int32_t get_offset_of_m_Options_0() { return static_cast<int32_t>(offsetof(OptionDataList_t457963479, ___m_Options_0)); }
	inline List_1_t3898801349 * get_m_Options_0() const { return ___m_Options_0; }
	inline List_1_t3898801349 ** get_address_of_m_Options_0() { return &___m_Options_0; }
	inline void set_m_Options_0(List_1_t3898801349 * value)
	{
		___m_Options_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONDATALIST_T457963479_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef UNITYEVENT_1_T2114859947_H
#define UNITYEVENT_1_T2114859947_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t2114859947  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2114859947, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2114859947_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef UNITYEVENT_1_T2110227463_H
#define UNITYEVENT_1_T2110227463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t2110227463  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2110227463, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2110227463_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef SPRITEFRAME_T3204261111_H
#define SPRITEFRAME_T3204261111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame
struct  SpriteFrame_t3204261111 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::x
	float ___x_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::y
	float ___y_1;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::w
	float ___w_2;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame::h
	float ___h_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SpriteFrame_t3204261111, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SpriteFrame_t3204261111, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(SpriteFrame_t3204261111, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(SpriteFrame_t3204261111, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEFRAME_T3204261111_H
#ifndef MATERIALREFERENCE_T2854353496_H
#define MATERIALREFERENCE_T2854353496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_t2854353496 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t2530419979 * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_t193706927 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_t193706927 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___fontAsset_1)); }
	inline TMP_FontAsset_t2530419979 * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t2530419979 * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_t2641813093 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_t2641813093 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___material_3)); }
	inline Material_t193706927 * get_material_3() const { return ___material_3; }
	inline Material_t193706927 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t193706927 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___fallbackMaterial_6)); }
	inline Material_t193706927 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_t193706927 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_t193706927 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_t2854353496, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_t2854353496_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t2530419979 * ___fontAsset_1;
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_2;
	Material_t193706927 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t193706927 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_t2854353496_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t2530419979 * ___fontAsset_1;
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_2;
	Material_t193706927 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_t193706927 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_T2854353496_H
#ifndef FLOATTWEEN_T1652887471_H
#define FLOATTWEEN_T1652887471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FloatTween
struct  FloatTween_t1652887471 
{
public:
	// TMPro.FloatTween/FloatTweenCallback TMPro.FloatTween::m_Target
	FloatTweenCallback_t2368686856 * ___m_Target_0;
	// System.Single TMPro.FloatTween::m_StartValue
	float ___m_StartValue_1;
	// System.Single TMPro.FloatTween::m_TargetValue
	float ___m_TargetValue_2;
	// System.Single TMPro.FloatTween::m_Duration
	float ___m_Duration_3;
	// System.Boolean TMPro.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FloatTween_t1652887471, ___m_Target_0)); }
	inline FloatTweenCallback_t2368686856 * get_m_Target_0() const { return ___m_Target_0; }
	inline FloatTweenCallback_t2368686856 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(FloatTweenCallback_t2368686856 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartValue_1() { return static_cast<int32_t>(offsetof(FloatTween_t1652887471, ___m_StartValue_1)); }
	inline float get_m_StartValue_1() const { return ___m_StartValue_1; }
	inline float* get_address_of_m_StartValue_1() { return &___m_StartValue_1; }
	inline void set_m_StartValue_1(float value)
	{
		___m_StartValue_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetValue_2() { return static_cast<int32_t>(offsetof(FloatTween_t1652887471, ___m_TargetValue_2)); }
	inline float get_m_TargetValue_2() const { return ___m_TargetValue_2; }
	inline float* get_address_of_m_TargetValue_2() { return &___m_TargetValue_2; }
	inline void set_m_TargetValue_2(float value)
	{
		___m_TargetValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Duration_3() { return static_cast<int32_t>(offsetof(FloatTween_t1652887471, ___m_Duration_3)); }
	inline float get_m_Duration_3() const { return ___m_Duration_3; }
	inline float* get_address_of_m_Duration_3() { return &___m_Duration_3; }
	inline void set_m_Duration_3(float value)
	{
		___m_Duration_3 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(FloatTween_t1652887471, ___m_IgnoreTimeScale_4)); }
	inline bool get_m_IgnoreTimeScale_4() const { return ___m_IgnoreTimeScale_4; }
	inline bool* get_address_of_m_IgnoreTimeScale_4() { return &___m_IgnoreTimeScale_4; }
	inline void set_m_IgnoreTimeScale_4(bool value)
	{
		___m_IgnoreTimeScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FloatTween
struct FloatTween_t1652887471_marshaled_pinvoke
{
	FloatTweenCallback_t2368686856 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
// Native definition for COM marshalling of TMPro.FloatTween
struct FloatTween_t1652887471_marshaled_com
{
	FloatTweenCallback_t2368686856 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
#endif // FLOATTWEEN_T1652887471_H
#ifndef U24ARRAYTYPEU3D40_T2731437126_H
#define U24ARRAYTYPEU3D40_T2731437126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D40_t2731437126 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D40_t2731437126__padding[40];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D40_T2731437126_H
#ifndef TMP_BASICXMLTAGSTACK_T937156555_H
#define TMP_BASICXMLTAGSTACK_T937156555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_BasicXmlTagStack
struct  TMP_BasicXmlTagStack_t937156555 
{
public:
	// System.Byte TMPro.TMP_BasicXmlTagStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_BasicXmlTagStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_BasicXmlTagStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_BasicXmlTagStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_BasicXmlTagStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_BasicXmlTagStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_BasicXmlTagStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_BasicXmlTagStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_BasicXmlTagStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_BasicXmlTagStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_BasicXmlTagStack_t937156555, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_BASICXMLTAGSTACK_T937156555_H
#ifndef U24ARRAYTYPEU3D12_T1568637718_H
#define U24ARRAYTYPEU3D12_T1568637718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t1568637718 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t1568637718__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T1568637718_H
#ifndef UNITYEVENT_1_T2058742090_H
#define UNITYEVENT_1_T2058742090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.Color>
struct  UnityEvent_1_t2058742090  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2058742090, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2058742090_H
#ifndef SPRITESIZE_T2240777863_H
#define SPRITESIZE_T2240777863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize
struct  SpriteSize_t2240777863 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::w
	float ___w_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize::h
	float ___h_1;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(SpriteSize_t2240777863, ___w_0)); }
	inline float get_w_0() const { return ___w_0; }
	inline float* get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(float value)
	{
		___w_0 = value;
	}

	inline static int32_t get_offset_of_h_1() { return static_cast<int32_t>(offsetof(SpriteSize_t2240777863, ___h_1)); }
	inline float get_h_1() const { return ___h_1; }
	inline float* get_address_of_h_1() { return &___h_1; }
	inline void set_h_1(float value)
	{
		___h_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESIZE_T2240777863_H
#ifndef RESOURCES_T4030816863_H
#define RESOURCES_T4030816863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DefaultControls/Resources
struct  Resources_t4030816863 
{
public:
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::standard
	Sprite_t309593783 * ___standard_0;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::background
	Sprite_t309593783 * ___background_1;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::inputField
	Sprite_t309593783 * ___inputField_2;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::knob
	Sprite_t309593783 * ___knob_3;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::checkmark
	Sprite_t309593783 * ___checkmark_4;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::dropdown
	Sprite_t309593783 * ___dropdown_5;
	// UnityEngine.Sprite TMPro.TMP_DefaultControls/Resources::mask
	Sprite_t309593783 * ___mask_6;

public:
	inline static int32_t get_offset_of_standard_0() { return static_cast<int32_t>(offsetof(Resources_t4030816863, ___standard_0)); }
	inline Sprite_t309593783 * get_standard_0() const { return ___standard_0; }
	inline Sprite_t309593783 ** get_address_of_standard_0() { return &___standard_0; }
	inline void set_standard_0(Sprite_t309593783 * value)
	{
		___standard_0 = value;
		Il2CppCodeGenWriteBarrier((&___standard_0), value);
	}

	inline static int32_t get_offset_of_background_1() { return static_cast<int32_t>(offsetof(Resources_t4030816863, ___background_1)); }
	inline Sprite_t309593783 * get_background_1() const { return ___background_1; }
	inline Sprite_t309593783 ** get_address_of_background_1() { return &___background_1; }
	inline void set_background_1(Sprite_t309593783 * value)
	{
		___background_1 = value;
		Il2CppCodeGenWriteBarrier((&___background_1), value);
	}

	inline static int32_t get_offset_of_inputField_2() { return static_cast<int32_t>(offsetof(Resources_t4030816863, ___inputField_2)); }
	inline Sprite_t309593783 * get_inputField_2() const { return ___inputField_2; }
	inline Sprite_t309593783 ** get_address_of_inputField_2() { return &___inputField_2; }
	inline void set_inputField_2(Sprite_t309593783 * value)
	{
		___inputField_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputField_2), value);
	}

	inline static int32_t get_offset_of_knob_3() { return static_cast<int32_t>(offsetof(Resources_t4030816863, ___knob_3)); }
	inline Sprite_t309593783 * get_knob_3() const { return ___knob_3; }
	inline Sprite_t309593783 ** get_address_of_knob_3() { return &___knob_3; }
	inline void set_knob_3(Sprite_t309593783 * value)
	{
		___knob_3 = value;
		Il2CppCodeGenWriteBarrier((&___knob_3), value);
	}

	inline static int32_t get_offset_of_checkmark_4() { return static_cast<int32_t>(offsetof(Resources_t4030816863, ___checkmark_4)); }
	inline Sprite_t309593783 * get_checkmark_4() const { return ___checkmark_4; }
	inline Sprite_t309593783 ** get_address_of_checkmark_4() { return &___checkmark_4; }
	inline void set_checkmark_4(Sprite_t309593783 * value)
	{
		___checkmark_4 = value;
		Il2CppCodeGenWriteBarrier((&___checkmark_4), value);
	}

	inline static int32_t get_offset_of_dropdown_5() { return static_cast<int32_t>(offsetof(Resources_t4030816863, ___dropdown_5)); }
	inline Sprite_t309593783 * get_dropdown_5() const { return ___dropdown_5; }
	inline Sprite_t309593783 ** get_address_of_dropdown_5() { return &___dropdown_5; }
	inline void set_dropdown_5(Sprite_t309593783 * value)
	{
		___dropdown_5 = value;
		Il2CppCodeGenWriteBarrier((&___dropdown_5), value);
	}

	inline static int32_t get_offset_of_mask_6() { return static_cast<int32_t>(offsetof(Resources_t4030816863, ___mask_6)); }
	inline Sprite_t309593783 * get_mask_6() const { return ___mask_6; }
	inline Sprite_t309593783 ** get_address_of_mask_6() { return &___mask_6; }
	inline void set_mask_6(Sprite_t309593783 * value)
	{
		___mask_6 = value;
		Il2CppCodeGenWriteBarrier((&___mask_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_DefaultControls/Resources
struct Resources_t4030816863_marshaled_pinvoke
{
	Sprite_t309593783 * ___standard_0;
	Sprite_t309593783 * ___background_1;
	Sprite_t309593783 * ___inputField_2;
	Sprite_t309593783 * ___knob_3;
	Sprite_t309593783 * ___checkmark_4;
	Sprite_t309593783 * ___dropdown_5;
	Sprite_t309593783 * ___mask_6;
};
// Native definition for COM marshalling of TMPro.TMP_DefaultControls/Resources
struct Resources_t4030816863_marshaled_com
{
	Sprite_t309593783 * ___standard_0;
	Sprite_t309593783 * ___background_1;
	Sprite_t309593783 * ___inputField_2;
	Sprite_t309593783 * ___knob_3;
	Sprite_t309593783 * ___checkmark_4;
	Sprite_t309593783 * ___dropdown_5;
	Sprite_t309593783 * ___mask_6;
};
#endif // RESOURCES_T4030816863_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef CHAR_T3454481338_H
#define CHAR_T3454481338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3454481338 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3454481338, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3454481338_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3454481338_H
#ifndef TMP_XMLTAGSTACK_1_T1818389866_H
#define TMP_XMLTAGSTACK_1_T1818389866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_XmlTagStack_1_t1818389866 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TMP_ColorGradientU5BU5D_t2828559218* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	TMP_ColorGradient_t1159837347 * ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1818389866, ___itemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t2828559218* get_itemStack_0() const { return ___itemStack_0; }
	inline TMP_ColorGradientU5BU5D_t2828559218** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TMP_ColorGradientU5BU5D_t2828559218* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1818389866, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1818389866, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1818389866, ___m_defaultItem_3)); }
	inline TMP_ColorGradient_t1159837347 * get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline TMP_ColorGradient_t1159837347 ** get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(TMP_ColorGradient_t1159837347 * value)
	{
		___m_defaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1818389866_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef TMP_XMLTAGSTACK_1_T2730429967_H
#define TMP_XMLTAGSTACK_1_T2730429967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Int32>
struct  TMP_XmlTagStack_1_t2730429967 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Int32U5BU5D_t3030399641* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2730429967, ___itemStack_0)); }
	inline Int32U5BU5D_t3030399641* get_itemStack_0() const { return ___itemStack_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Int32U5BU5D_t3030399641* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2730429967, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2730429967, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2730429967, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2730429967_H
#ifndef TMP_XMLTAGSTACK_1_T2735062451_H
#define TMP_XMLTAGSTACK_1_T2735062451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<System.Single>
struct  TMP_XmlTagStack_1_t2735062451 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	SingleU5BU5D_t577127397* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	float ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2735062451, ___itemStack_0)); }
	inline SingleU5BU5D_t577127397* get_itemStack_0() const { return ___itemStack_0; }
	inline SingleU5BU5D_t577127397** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(SingleU5BU5D_t577127397* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2735062451, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2735062451, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2735062451, ___m_defaultItem_3)); }
	inline float get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline float* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(float value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2735062451_H
#ifndef VECTOR4_T2243707581_H
#define VECTOR4_T2243707581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2243707581 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2243707581, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2243707581_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2243707581  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2243707581  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2243707581  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2243707581  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2243707581  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2243707581 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2243707581  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___oneVector_6)); }
	inline Vector4_t2243707581  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2243707581 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2243707581  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2243707581  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2243707581 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2243707581  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2243707581_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2243707581  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2243707581 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2243707581  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2243707581_H
#ifndef MATRIX4X4_T2933234003_H
#define MATRIX4X4_T2933234003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t2933234003 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t2933234003_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t2933234003  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t2933234003  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t2933234003  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t2933234003 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t2933234003  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t2933234003_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t2933234003  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t2933234003 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t2933234003  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T2933234003_H
#ifndef FONTCREATIONSETTING_T1093397046_H
#define FONTCREATIONSETTING_T1093397046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontCreationSetting
struct  FontCreationSetting_t1093397046 
{
public:
	// System.String TMPro.FontCreationSetting::fontSourcePath
	String_t* ___fontSourcePath_0;
	// System.Int32 TMPro.FontCreationSetting::fontSizingMode
	int32_t ___fontSizingMode_1;
	// System.Int32 TMPro.FontCreationSetting::fontSize
	int32_t ___fontSize_2;
	// System.Int32 TMPro.FontCreationSetting::fontPadding
	int32_t ___fontPadding_3;
	// System.Int32 TMPro.FontCreationSetting::fontPackingMode
	int32_t ___fontPackingMode_4;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasWidth
	int32_t ___fontAtlasWidth_5;
	// System.Int32 TMPro.FontCreationSetting::fontAtlasHeight
	int32_t ___fontAtlasHeight_6;
	// System.Int32 TMPro.FontCreationSetting::fontCharacterSet
	int32_t ___fontCharacterSet_7;
	// System.Int32 TMPro.FontCreationSetting::fontStyle
	int32_t ___fontStyle_8;
	// System.Single TMPro.FontCreationSetting::fontStlyeModifier
	float ___fontStlyeModifier_9;
	// System.Int32 TMPro.FontCreationSetting::fontRenderMode
	int32_t ___fontRenderMode_10;
	// System.Boolean TMPro.FontCreationSetting::fontKerning
	bool ___fontKerning_11;

public:
	inline static int32_t get_offset_of_fontSourcePath_0() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontSourcePath_0)); }
	inline String_t* get_fontSourcePath_0() const { return ___fontSourcePath_0; }
	inline String_t** get_address_of_fontSourcePath_0() { return &___fontSourcePath_0; }
	inline void set_fontSourcePath_0(String_t* value)
	{
		___fontSourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___fontSourcePath_0), value);
	}

	inline static int32_t get_offset_of_fontSizingMode_1() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontSizingMode_1)); }
	inline int32_t get_fontSizingMode_1() const { return ___fontSizingMode_1; }
	inline int32_t* get_address_of_fontSizingMode_1() { return &___fontSizingMode_1; }
	inline void set_fontSizingMode_1(int32_t value)
	{
		___fontSizingMode_1 = value;
	}

	inline static int32_t get_offset_of_fontSize_2() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontSize_2)); }
	inline int32_t get_fontSize_2() const { return ___fontSize_2; }
	inline int32_t* get_address_of_fontSize_2() { return &___fontSize_2; }
	inline void set_fontSize_2(int32_t value)
	{
		___fontSize_2 = value;
	}

	inline static int32_t get_offset_of_fontPadding_3() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontPadding_3)); }
	inline int32_t get_fontPadding_3() const { return ___fontPadding_3; }
	inline int32_t* get_address_of_fontPadding_3() { return &___fontPadding_3; }
	inline void set_fontPadding_3(int32_t value)
	{
		___fontPadding_3 = value;
	}

	inline static int32_t get_offset_of_fontPackingMode_4() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontPackingMode_4)); }
	inline int32_t get_fontPackingMode_4() const { return ___fontPackingMode_4; }
	inline int32_t* get_address_of_fontPackingMode_4() { return &___fontPackingMode_4; }
	inline void set_fontPackingMode_4(int32_t value)
	{
		___fontPackingMode_4 = value;
	}

	inline static int32_t get_offset_of_fontAtlasWidth_5() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontAtlasWidth_5)); }
	inline int32_t get_fontAtlasWidth_5() const { return ___fontAtlasWidth_5; }
	inline int32_t* get_address_of_fontAtlasWidth_5() { return &___fontAtlasWidth_5; }
	inline void set_fontAtlasWidth_5(int32_t value)
	{
		___fontAtlasWidth_5 = value;
	}

	inline static int32_t get_offset_of_fontAtlasHeight_6() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontAtlasHeight_6)); }
	inline int32_t get_fontAtlasHeight_6() const { return ___fontAtlasHeight_6; }
	inline int32_t* get_address_of_fontAtlasHeight_6() { return &___fontAtlasHeight_6; }
	inline void set_fontAtlasHeight_6(int32_t value)
	{
		___fontAtlasHeight_6 = value;
	}

	inline static int32_t get_offset_of_fontCharacterSet_7() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontCharacterSet_7)); }
	inline int32_t get_fontCharacterSet_7() const { return ___fontCharacterSet_7; }
	inline int32_t* get_address_of_fontCharacterSet_7() { return &___fontCharacterSet_7; }
	inline void set_fontCharacterSet_7(int32_t value)
	{
		___fontCharacterSet_7 = value;
	}

	inline static int32_t get_offset_of_fontStyle_8() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontStyle_8)); }
	inline int32_t get_fontStyle_8() const { return ___fontStyle_8; }
	inline int32_t* get_address_of_fontStyle_8() { return &___fontStyle_8; }
	inline void set_fontStyle_8(int32_t value)
	{
		___fontStyle_8 = value;
	}

	inline static int32_t get_offset_of_fontStlyeModifier_9() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontStlyeModifier_9)); }
	inline float get_fontStlyeModifier_9() const { return ___fontStlyeModifier_9; }
	inline float* get_address_of_fontStlyeModifier_9() { return &___fontStlyeModifier_9; }
	inline void set_fontStlyeModifier_9(float value)
	{
		___fontStlyeModifier_9 = value;
	}

	inline static int32_t get_offset_of_fontRenderMode_10() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontRenderMode_10)); }
	inline int32_t get_fontRenderMode_10() const { return ___fontRenderMode_10; }
	inline int32_t* get_address_of_fontRenderMode_10() { return &___fontRenderMode_10; }
	inline void set_fontRenderMode_10(int32_t value)
	{
		___fontRenderMode_10 = value;
	}

	inline static int32_t get_offset_of_fontKerning_11() { return static_cast<int32_t>(offsetof(FontCreationSetting_t1093397046, ___fontKerning_11)); }
	inline bool get_fontKerning_11() const { return ___fontKerning_11; }
	inline bool* get_address_of_fontKerning_11() { return &___fontKerning_11; }
	inline void set_fontKerning_11(bool value)
	{
		___fontKerning_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t1093397046_marshaled_pinvoke
{
	char* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
// Native definition for COM marshalling of TMPro.FontCreationSetting
struct FontCreationSetting_t1093397046_marshaled_com
{
	Il2CppChar* ___fontSourcePath_0;
	int32_t ___fontSizingMode_1;
	int32_t ___fontSize_2;
	int32_t ___fontPadding_3;
	int32_t ___fontPackingMode_4;
	int32_t ___fontAtlasWidth_5;
	int32_t ___fontAtlasHeight_6;
	int32_t ___fontCharacterSet_7;
	int32_t ___fontStyle_8;
	float ___fontStlyeModifier_9;
	int32_t ___fontRenderMode_10;
	int32_t ___fontKerning_11;
};
#endif // FONTCREATIONSETTING_T1093397046_H
#ifndef UNITYEVENT_1_T2067570248_H
#define UNITYEVENT_1_T2067570248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2067570248  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2067570248, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2067570248_H
#ifndef SPRITESTATE_T1353336012_H
#define SPRITESTATE_T1353336012_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.SpriteState
struct  SpriteState_t1353336012 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t309593783 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t309593783 * ___m_DisabledSprite_2;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_HighlightedSprite_0)); }
	inline Sprite_t309593783 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t309593783 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t309593783 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_HighlightedSprite_0), value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_PressedSprite_1)); }
	inline Sprite_t309593783 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t309593783 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t309593783 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PressedSprite_1), value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t1353336012, ___m_DisabledSprite_2)); }
	inline Sprite_t309593783 * get_m_DisabledSprite_2() const { return ___m_DisabledSprite_2; }
	inline Sprite_t309593783 ** get_address_of_m_DisabledSprite_2() { return &___m_DisabledSprite_2; }
	inline void set_m_DisabledSprite_2(Sprite_t309593783 * value)
	{
		___m_DisabledSprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DisabledSprite_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_pinvoke
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t1353336012_marshaled_com
{
	Sprite_t309593783 * ___m_HighlightedSprite_0;
	Sprite_t309593783 * ___m_PressedSprite_1;
	Sprite_t309593783 * ___m_DisabledSprite_2;
};
#endif // SPRITESTATE_T1353336012_H
#ifndef UNITYEVENT_3_T2425689862_H
#define UNITYEVENT_3_T2425689862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`3<System.String,System.Int32,System.Int32>
struct  UnityEvent_3_t2425689862  : public UnityEventBase_t828812576
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`3::m_InvokeArray
	ObjectU5BU5D_t3614634134* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_3_t2425689862, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t3614634134* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t3614634134** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t3614634134* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_3_T2425689862_H
#ifndef TMP_FONTWEIGHTS_T1564168302_H
#define TMP_FONTWEIGHTS_T1564168302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontWeights
struct  TMP_FontWeights_t1564168302 
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_FontWeights::regularTypeface
	TMP_FontAsset_t2530419979 * ___regularTypeface_0;
	// TMPro.TMP_FontAsset TMPro.TMP_FontWeights::italicTypeface
	TMP_FontAsset_t2530419979 * ___italicTypeface_1;

public:
	inline static int32_t get_offset_of_regularTypeface_0() { return static_cast<int32_t>(offsetof(TMP_FontWeights_t1564168302, ___regularTypeface_0)); }
	inline TMP_FontAsset_t2530419979 * get_regularTypeface_0() const { return ___regularTypeface_0; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_regularTypeface_0() { return &___regularTypeface_0; }
	inline void set_regularTypeface_0(TMP_FontAsset_t2530419979 * value)
	{
		___regularTypeface_0 = value;
		Il2CppCodeGenWriteBarrier((&___regularTypeface_0), value);
	}

	inline static int32_t get_offset_of_italicTypeface_1() { return static_cast<int32_t>(offsetof(TMP_FontWeights_t1564168302, ___italicTypeface_1)); }
	inline TMP_FontAsset_t2530419979 * get_italicTypeface_1() const { return ___italicTypeface_1; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_italicTypeface_1() { return &___italicTypeface_1; }
	inline void set_italicTypeface_1(TMP_FontAsset_t2530419979 * value)
	{
		___italicTypeface_1 = value;
		Il2CppCodeGenWriteBarrier((&___italicTypeface_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_FontWeights
struct TMP_FontWeights_t1564168302_marshaled_pinvoke
{
	TMP_FontAsset_t2530419979 * ___regularTypeface_0;
	TMP_FontAsset_t2530419979 * ___italicTypeface_1;
};
// Native definition for COM marshalling of TMPro.TMP_FontWeights
struct TMP_FontWeights_t1564168302_marshaled_com
{
	TMP_FontAsset_t2530419979 * ___regularTypeface_0;
	TMP_FontAsset_t2530419979 * ___italicTypeface_1;
};
#endif // TMP_FONTWEIGHTS_T1564168302_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef TMP_XMLTAGSTACK_1_T1533070037_H
#define TMP_XMLTAGSTACK_1_T1533070037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32>
struct  TMP_XmlTagStack_1_t1533070037 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	Color32U5BU5D_t30278651* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	Color32_t874517518  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1533070037, ___itemStack_0)); }
	inline Color32U5BU5D_t30278651* get_itemStack_0() const { return ___itemStack_0; }
	inline Color32U5BU5D_t30278651** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(Color32U5BU5D_t30278651* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1533070037, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1533070037, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t1533070037, ___m_defaultItem_3)); }
	inline Color32_t874517518  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline Color32_t874517518 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(Color32_t874517518  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T1533070037_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305145_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305145  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t1568637718  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>/$ArrayType=40 <PrivateImplementationDetails>::$field-9E6378168821DBABB7EE3D0154346480FAC8AEF1
	U24ArrayTypeU3D40_t2731437126  ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t1568637718  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t1568637718 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t1568637718  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields, ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline U24ArrayTypeU3D40_t2731437126  get_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline U24ArrayTypeU3D40_t2731437126 * get_address_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(U24ArrayTypeU3D40_t2731437126  value)
	{
		___U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305145_H
#ifndef LINESEGMENT_T2997084511_H
#define LINESEGMENT_T2997084511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities/LineSegment
struct  LineSegment_t2997084511 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point1
	Vector3_t2243707580  ___Point1_0;
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities/LineSegment::Point2
	Vector3_t2243707580  ___Point2_1;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(LineSegment_t2997084511, ___Point1_0)); }
	inline Vector3_t2243707580  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_t2243707580 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_t2243707580  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(LineSegment_t2997084511, ___Point2_1)); }
	inline Vector3_t2243707580  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_t2243707580 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_t2243707580  value)
	{
		___Point2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESEGMENT_T2997084511_H
#ifndef CARETPOSITION_T420625986_H
#define CARETPOSITION_T420625986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretPosition
struct  CaretPosition_t420625986 
{
public:
	// System.Int32 TMPro.CaretPosition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CaretPosition_t420625986, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETPOSITION_T420625986_H
#ifndef TMP_TEXTINFO_T2849466151_H
#define TMP_TEXTINFO_T2849466151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextInfo
struct  TMP_TextInfo_t2849466151  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.TMP_TextInfo::textComponent
	TMP_Text_t1920000777 * ___textComponent_2;
	// System.Int32 TMPro.TMP_TextInfo::characterCount
	int32_t ___characterCount_3;
	// System.Int32 TMPro.TMP_TextInfo::spriteCount
	int32_t ___spriteCount_4;
	// System.Int32 TMPro.TMP_TextInfo::spaceCount
	int32_t ___spaceCount_5;
	// System.Int32 TMPro.TMP_TextInfo::wordCount
	int32_t ___wordCount_6;
	// System.Int32 TMPro.TMP_TextInfo::linkCount
	int32_t ___linkCount_7;
	// System.Int32 TMPro.TMP_TextInfo::lineCount
	int32_t ___lineCount_8;
	// System.Int32 TMPro.TMP_TextInfo::pageCount
	int32_t ___pageCount_9;
	// System.Int32 TMPro.TMP_TextInfo::materialCount
	int32_t ___materialCount_10;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_TextInfo::characterInfo
	TMP_CharacterInfoU5BU5D_t602810366* ___characterInfo_11;
	// TMPro.TMP_WordInfo[] TMPro.TMP_TextInfo::wordInfo
	TMP_WordInfoU5BU5D_t968836101* ___wordInfo_12;
	// TMPro.TMP_LinkInfo[] TMPro.TMP_TextInfo::linkInfo
	TMP_LinkInfoU5BU5D_t2563924433* ___linkInfo_13;
	// TMPro.TMP_LineInfo[] TMPro.TMP_TextInfo::lineInfo
	TMP_LineInfoU5BU5D_t1617569211* ___lineInfo_14;
	// TMPro.TMP_PageInfo[] TMPro.TMP_TextInfo::pageInfo
	TMP_PageInfoU5BU5D_t2788613100* ___pageInfo_15;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::meshInfo
	TMP_MeshInfoU5BU5D_t2398608976* ___meshInfo_16;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::m_CachedMeshInfo
	TMP_MeshInfoU5BU5D_t2398608976* ___m_CachedMeshInfo_17;

public:
	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___textComponent_2)); }
	inline TMP_Text_t1920000777 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t1920000777 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t1920000777 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_2), value);
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}

	inline static int32_t get_offset_of_spriteCount_4() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___spriteCount_4)); }
	inline int32_t get_spriteCount_4() const { return ___spriteCount_4; }
	inline int32_t* get_address_of_spriteCount_4() { return &___spriteCount_4; }
	inline void set_spriteCount_4(int32_t value)
	{
		___spriteCount_4 = value;
	}

	inline static int32_t get_offset_of_spaceCount_5() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___spaceCount_5)); }
	inline int32_t get_spaceCount_5() const { return ___spaceCount_5; }
	inline int32_t* get_address_of_spaceCount_5() { return &___spaceCount_5; }
	inline void set_spaceCount_5(int32_t value)
	{
		___spaceCount_5 = value;
	}

	inline static int32_t get_offset_of_wordCount_6() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___wordCount_6)); }
	inline int32_t get_wordCount_6() const { return ___wordCount_6; }
	inline int32_t* get_address_of_wordCount_6() { return &___wordCount_6; }
	inline void set_wordCount_6(int32_t value)
	{
		___wordCount_6 = value;
	}

	inline static int32_t get_offset_of_linkCount_7() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___linkCount_7)); }
	inline int32_t get_linkCount_7() const { return ___linkCount_7; }
	inline int32_t* get_address_of_linkCount_7() { return &___linkCount_7; }
	inline void set_linkCount_7(int32_t value)
	{
		___linkCount_7 = value;
	}

	inline static int32_t get_offset_of_lineCount_8() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___lineCount_8)); }
	inline int32_t get_lineCount_8() const { return ___lineCount_8; }
	inline int32_t* get_address_of_lineCount_8() { return &___lineCount_8; }
	inline void set_lineCount_8(int32_t value)
	{
		___lineCount_8 = value;
	}

	inline static int32_t get_offset_of_pageCount_9() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___pageCount_9)); }
	inline int32_t get_pageCount_9() const { return ___pageCount_9; }
	inline int32_t* get_address_of_pageCount_9() { return &___pageCount_9; }
	inline void set_pageCount_9(int32_t value)
	{
		___pageCount_9 = value;
	}

	inline static int32_t get_offset_of_materialCount_10() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___materialCount_10)); }
	inline int32_t get_materialCount_10() const { return ___materialCount_10; }
	inline int32_t* get_address_of_materialCount_10() { return &___materialCount_10; }
	inline void set_materialCount_10(int32_t value)
	{
		___materialCount_10 = value;
	}

	inline static int32_t get_offset_of_characterInfo_11() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___characterInfo_11)); }
	inline TMP_CharacterInfoU5BU5D_t602810366* get_characterInfo_11() const { return ___characterInfo_11; }
	inline TMP_CharacterInfoU5BU5D_t602810366** get_address_of_characterInfo_11() { return &___characterInfo_11; }
	inline void set_characterInfo_11(TMP_CharacterInfoU5BU5D_t602810366* value)
	{
		___characterInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterInfo_11), value);
	}

	inline static int32_t get_offset_of_wordInfo_12() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___wordInfo_12)); }
	inline TMP_WordInfoU5BU5D_t968836101* get_wordInfo_12() const { return ___wordInfo_12; }
	inline TMP_WordInfoU5BU5D_t968836101** get_address_of_wordInfo_12() { return &___wordInfo_12; }
	inline void set_wordInfo_12(TMP_WordInfoU5BU5D_t968836101* value)
	{
		___wordInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___wordInfo_12), value);
	}

	inline static int32_t get_offset_of_linkInfo_13() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___linkInfo_13)); }
	inline TMP_LinkInfoU5BU5D_t2563924433* get_linkInfo_13() const { return ___linkInfo_13; }
	inline TMP_LinkInfoU5BU5D_t2563924433** get_address_of_linkInfo_13() { return &___linkInfo_13; }
	inline void set_linkInfo_13(TMP_LinkInfoU5BU5D_t2563924433* value)
	{
		___linkInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___linkInfo_13), value);
	}

	inline static int32_t get_offset_of_lineInfo_14() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___lineInfo_14)); }
	inline TMP_LineInfoU5BU5D_t1617569211* get_lineInfo_14() const { return ___lineInfo_14; }
	inline TMP_LineInfoU5BU5D_t1617569211** get_address_of_lineInfo_14() { return &___lineInfo_14; }
	inline void set_lineInfo_14(TMP_LineInfoU5BU5D_t1617569211* value)
	{
		___lineInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_14), value);
	}

	inline static int32_t get_offset_of_pageInfo_15() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___pageInfo_15)); }
	inline TMP_PageInfoU5BU5D_t2788613100* get_pageInfo_15() const { return ___pageInfo_15; }
	inline TMP_PageInfoU5BU5D_t2788613100** get_address_of_pageInfo_15() { return &___pageInfo_15; }
	inline void set_pageInfo_15(TMP_PageInfoU5BU5D_t2788613100* value)
	{
		___pageInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_15), value);
	}

	inline static int32_t get_offset_of_meshInfo_16() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___meshInfo_16)); }
	inline TMP_MeshInfoU5BU5D_t2398608976* get_meshInfo_16() const { return ___meshInfo_16; }
	inline TMP_MeshInfoU5BU5D_t2398608976** get_address_of_meshInfo_16() { return &___meshInfo_16; }
	inline void set_meshInfo_16(TMP_MeshInfoU5BU5D_t2398608976* value)
	{
		___meshInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshInfo_16), value);
	}

	inline static int32_t get_offset_of_m_CachedMeshInfo_17() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151, ___m_CachedMeshInfo_17)); }
	inline TMP_MeshInfoU5BU5D_t2398608976* get_m_CachedMeshInfo_17() const { return ___m_CachedMeshInfo_17; }
	inline TMP_MeshInfoU5BU5D_t2398608976** get_address_of_m_CachedMeshInfo_17() { return &___m_CachedMeshInfo_17; }
	inline void set_m_CachedMeshInfo_17(TMP_MeshInfoU5BU5D_t2398608976* value)
	{
		___m_CachedMeshInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedMeshInfo_17), value);
	}
};

struct TMP_TextInfo_t2849466151_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorPositive
	Vector2_t2243707579  ___k_InfinityVectorPositive_0;
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorNegative
	Vector2_t2243707579  ___k_InfinityVectorNegative_1;

public:
	inline static int32_t get_offset_of_k_InfinityVectorPositive_0() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151_StaticFields, ___k_InfinityVectorPositive_0)); }
	inline Vector2_t2243707579  get_k_InfinityVectorPositive_0() const { return ___k_InfinityVectorPositive_0; }
	inline Vector2_t2243707579 * get_address_of_k_InfinityVectorPositive_0() { return &___k_InfinityVectorPositive_0; }
	inline void set_k_InfinityVectorPositive_0(Vector2_t2243707579  value)
	{
		___k_InfinityVectorPositive_0 = value;
	}

	inline static int32_t get_offset_of_k_InfinityVectorNegative_1() { return static_cast<int32_t>(offsetof(TMP_TextInfo_t2849466151_StaticFields, ___k_InfinityVectorNegative_1)); }
	inline Vector2_t2243707579  get_k_InfinityVectorNegative_1() const { return ___k_InfinityVectorNegative_1; }
	inline Vector2_t2243707579 * get_address_of_k_InfinityVectorNegative_1() { return &___k_InfinityVectorNegative_1; }
	inline void set_k_InfinityVectorNegative_1(Vector2_t2243707579  value)
	{
		___k_InfinityVectorNegative_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFO_T2849466151_H
#ifndef ANCHORPOSITIONS_T2740933726_H
#define ANCHORPOSITIONS_T2740933726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Compatibility/AnchorPositions
struct  AnchorPositions_t2740933726 
{
public:
	// System.Int32 TMPro.TMP_Compatibility/AnchorPositions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnchorPositions_t2740933726, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORPOSITIONS_T2740933726_H
#ifndef MODE_T1081683921_H
#define MODE_T1081683921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation/Mode
struct  Mode_t1081683921 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t1081683921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T1081683921_H
#ifndef SELECTIONSTATE_T3187567897_H
#define SELECTIONSTATE_T3187567897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/SelectionState
struct  SelectionState_t3187567897 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/SelectionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SelectionState_t3187567897, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONSTATE_T3187567897_H
#ifndef EXTENTS_T3018556803_H
#define EXTENTS_T3018556803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_t3018556803 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_t2243707579  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_t2243707579  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_t3018556803, ___min_0)); }
	inline Vector2_t2243707579  get_min_0() const { return ___min_0; }
	inline Vector2_t2243707579 * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_t2243707579  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_t3018556803, ___max_1)); }
	inline Vector2_t2243707579  get_max_1() const { return ___max_1; }
	inline Vector2_t2243707579 * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_t2243707579  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_T3018556803_H
#ifndef TRANSITION_T605142169_H
#define TRANSITION_T605142169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable/Transition
struct  Transition_t605142169 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Transition_t605142169, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSITION_T605142169_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T875112366_H
#define TOUCHSCREENKEYBOARDTYPE_T875112366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t875112366 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t875112366, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T875112366_H
#ifndef COLORBLOCK_T2652774230_H
#define COLORBLOCK_T2652774230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t2652774230 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t2020392075  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t2020392075  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t2020392075  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t2020392075  ___m_DisabledColor_3;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_4;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_5;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_NormalColor_0)); }
	inline Color_t2020392075  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t2020392075 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t2020392075  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_HighlightedColor_1)); }
	inline Color_t2020392075  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t2020392075 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t2020392075  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_PressedColor_2)); }
	inline Color_t2020392075  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t2020392075 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t2020392075  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_DisabledColor_3)); }
	inline Color_t2020392075  get_m_DisabledColor_3() const { return ___m_DisabledColor_3; }
	inline Color_t2020392075 * get_address_of_m_DisabledColor_3() { return &___m_DisabledColor_3; }
	inline void set_m_DisabledColor_3(Color_t2020392075  value)
	{
		___m_DisabledColor_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_4() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_ColorMultiplier_4)); }
	inline float get_m_ColorMultiplier_4() const { return ___m_ColorMultiplier_4; }
	inline float* get_address_of_m_ColorMultiplier_4() { return &___m_ColorMultiplier_4; }
	inline void set_m_ColorMultiplier_4(float value)
	{
		___m_ColorMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_5() { return static_cast<int32_t>(offsetof(ColorBlock_t2652774230, ___m_FadeDuration_5)); }
	inline float get_m_FadeDuration_5() const { return ___m_FadeDuration_5; }
	inline float* get_address_of_m_FadeDuration_5() { return &___m_FadeDuration_5; }
	inline void set_m_FadeDuration_5(float value)
	{
		___m_FadeDuration_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORBLOCK_T2652774230_H
#ifndef TMP_XMLTAGSTACK_1_T3512906015_H
#define TMP_XMLTAGSTACK_1_T3512906015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference>
struct  TMP_XmlTagStack_1_t3512906015 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	MaterialReferenceU5BU5D_t627890505* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	MaterialReference_t2854353496  ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3512906015, ___itemStack_0)); }
	inline MaterialReferenceU5BU5D_t627890505* get_itemStack_0() const { return ___itemStack_0; }
	inline MaterialReferenceU5BU5D_t627890505** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(MaterialReferenceU5BU5D_t627890505* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3512906015, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3512906015, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t3512906015, ___m_defaultItem_3)); }
	inline MaterialReference_t2854353496  get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline MaterialReference_t2854353496 * get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(MaterialReference_t2854353496  value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T3512906015_H
#ifndef VERTEXGRADIENT_T1602386880_H
#define VERTEXGRADIENT_T1602386880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_t1602386880 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t2020392075  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t2020392075  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t2020392075  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t2020392075  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_t1602386880, ___topLeft_0)); }
	inline Color_t2020392075  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t2020392075 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t2020392075  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_t1602386880, ___topRight_1)); }
	inline Color_t2020392075  get_topRight_1() const { return ___topRight_1; }
	inline Color_t2020392075 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t2020392075  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_t1602386880, ___bottomLeft_2)); }
	inline Color_t2020392075  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t2020392075 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t2020392075  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_t1602386880, ___bottomRight_3)); }
	inline Color_t2020392075  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t2020392075 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t2020392075  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_T1602386880_H
#ifndef TMP_VERTEX_T1422870470_H
#define TMP_VERTEX_T1422870470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t1422870470 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_t2243707580  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_t2243707579  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_t2243707579  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_t2243707579  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t874517518  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___position_0)); }
	inline Vector3_t2243707580  get_position_0() const { return ___position_0; }
	inline Vector3_t2243707580 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2243707580  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___uv_1)); }
	inline Vector2_t2243707579  get_uv_1() const { return ___uv_1; }
	inline Vector2_t2243707579 * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_t2243707579  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___uv2_2)); }
	inline Vector2_t2243707579  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_t2243707579 * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_t2243707579  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___uv4_3)); }
	inline Vector2_t2243707579  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_t2243707579 * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_t2243707579  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t1422870470, ___color_4)); }
	inline Color32_t874517518  get_color_4() const { return ___color_4; }
	inline Color32_t874517518 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t874517518  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T1422870470_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef TEXTINPUTSOURCES_T434791461_H
#define TEXTINPUTSOURCES_T434791461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text/TextInputSources
struct  TextInputSources_t434791461 
{
public:
	// System.Int32 TMPro.TMP_Text/TextInputSources::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextInputSources_t434791461, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T434791461_H
#ifndef CONTENTTYPE_T4294436424_H
#define CONTENTTYPE_T4294436424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/ContentType
struct  ContentType_t4294436424 
{
public:
	// System.Int32 TMPro.TMP_InputField/ContentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ContentType_t4294436424, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTTYPE_T4294436424_H
#ifndef FONTASSETTYPES_T2622056438_H
#define FONTASSETTYPES_T2622056438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAsset/FontAssetTypes
struct  FontAssetTypes_t2622056438 
{
public:
	// System.Int32 TMPro.TMP_FontAsset/FontAssetTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontAssetTypes_t2622056438, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTASSETTYPES_T2622056438_H
#ifndef CHARACTERVALIDATION_T487603955_H
#define CHARACTERVALIDATION_T487603955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/CharacterValidation
struct  CharacterValidation_t487603955 
{
public:
	// System.Int32 TMPro.TMP_InputField/CharacterValidation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CharacterValidation_t487603955, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERVALIDATION_T487603955_H
#ifndef INPUTTYPE_T379073331_H
#define INPUTTYPE_T379073331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/InputType
struct  InputType_t379073331 
{
public:
	// System.Int32 TMPro.TMP_InputField/InputType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputType_t379073331, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTTYPE_T379073331_H
#ifndef DROPDOWNEVENT_T1859881917_H
#define DROPDOWNEVENT_T1859881917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/DropdownEvent
struct  DropdownEvent_t1859881917  : public UnityEvent_1_t2110227463
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNEVENT_T1859881917_H
#ifndef TMP_DEFAULTCONTROLS_T1315555119_H
#define TMP_DEFAULTCONTROLS_T1315555119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_DefaultControls
struct  TMP_DefaultControls_t1315555119  : public RuntimeObject
{
public:

public:
};

struct TMP_DefaultControls_t1315555119_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_DefaultControls::s_ThickElementSize
	Vector2_t2243707579  ___s_ThickElementSize_3;
	// UnityEngine.Vector2 TMPro.TMP_DefaultControls::s_ThinElementSize
	Vector2_t2243707579  ___s_ThinElementSize_4;
	// UnityEngine.Color TMPro.TMP_DefaultControls::s_DefaultSelectableColor
	Color_t2020392075  ___s_DefaultSelectableColor_5;
	// UnityEngine.Color TMPro.TMP_DefaultControls::s_TextColor
	Color_t2020392075  ___s_TextColor_6;

public:
	inline static int32_t get_offset_of_s_ThickElementSize_3() { return static_cast<int32_t>(offsetof(TMP_DefaultControls_t1315555119_StaticFields, ___s_ThickElementSize_3)); }
	inline Vector2_t2243707579  get_s_ThickElementSize_3() const { return ___s_ThickElementSize_3; }
	inline Vector2_t2243707579 * get_address_of_s_ThickElementSize_3() { return &___s_ThickElementSize_3; }
	inline void set_s_ThickElementSize_3(Vector2_t2243707579  value)
	{
		___s_ThickElementSize_3 = value;
	}

	inline static int32_t get_offset_of_s_ThinElementSize_4() { return static_cast<int32_t>(offsetof(TMP_DefaultControls_t1315555119_StaticFields, ___s_ThinElementSize_4)); }
	inline Vector2_t2243707579  get_s_ThinElementSize_4() const { return ___s_ThinElementSize_4; }
	inline Vector2_t2243707579 * get_address_of_s_ThinElementSize_4() { return &___s_ThinElementSize_4; }
	inline void set_s_ThinElementSize_4(Vector2_t2243707579  value)
	{
		___s_ThinElementSize_4 = value;
	}

	inline static int32_t get_offset_of_s_DefaultSelectableColor_5() { return static_cast<int32_t>(offsetof(TMP_DefaultControls_t1315555119_StaticFields, ___s_DefaultSelectableColor_5)); }
	inline Color_t2020392075  get_s_DefaultSelectableColor_5() const { return ___s_DefaultSelectableColor_5; }
	inline Color_t2020392075 * get_address_of_s_DefaultSelectableColor_5() { return &___s_DefaultSelectableColor_5; }
	inline void set_s_DefaultSelectableColor_5(Color_t2020392075  value)
	{
		___s_DefaultSelectableColor_5 = value;
	}

	inline static int32_t get_offset_of_s_TextColor_6() { return static_cast<int32_t>(offsetof(TMP_DefaultControls_t1315555119_StaticFields, ___s_TextColor_6)); }
	inline Color_t2020392075  get_s_TextColor_6() const { return ___s_TextColor_6; }
	inline Color_t2020392075 * get_address_of_s_TextColor_6() { return &___s_TextColor_6; }
	inline void set_s_TextColor_6(Color_t2020392075  value)
	{
		___s_TextColor_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DEFAULTCONTROLS_T1315555119_H
#ifndef SPRITEASSETIMPORTFORMATS_T73460506_H
#define SPRITEASSETIMPORTFORMATS_T73460506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.SpriteAssetImportFormats
struct  SpriteAssetImportFormats_t73460506 
{
public:
	// System.Int32 TMPro.SpriteAssetUtilities.SpriteAssetImportFormats::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpriteAssetImportFormats_t73460506, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEASSETIMPORTFORMATS_T73460506_H
#ifndef SPRITEDATA_T257854902_H
#define SPRITEDATA_T257854902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct  SpriteData_t257854902 
{
public:
	// System.String TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::filename
	String_t* ___filename_0;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::frame
	SpriteFrame_t3204261111  ___frame_1;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::rotated
	bool ___rotated_2;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::trimmed
	bool ___trimmed_3;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::spriteSourceSize
	SpriteFrame_t3204261111  ___spriteSourceSize_4;
	// TMPro.SpriteAssetUtilities.TexturePacker/SpriteSize TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::sourceSize
	SpriteSize_t2240777863  ___sourceSize_5;
	// UnityEngine.Vector2 TMPro.SpriteAssetUtilities.TexturePacker/SpriteData::pivot
	Vector2_t2243707579  ___pivot_6;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(SpriteData_t257854902, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(SpriteData_t257854902, ___frame_1)); }
	inline SpriteFrame_t3204261111  get_frame_1() const { return ___frame_1; }
	inline SpriteFrame_t3204261111 * get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(SpriteFrame_t3204261111  value)
	{
		___frame_1 = value;
	}

	inline static int32_t get_offset_of_rotated_2() { return static_cast<int32_t>(offsetof(SpriteData_t257854902, ___rotated_2)); }
	inline bool get_rotated_2() const { return ___rotated_2; }
	inline bool* get_address_of_rotated_2() { return &___rotated_2; }
	inline void set_rotated_2(bool value)
	{
		___rotated_2 = value;
	}

	inline static int32_t get_offset_of_trimmed_3() { return static_cast<int32_t>(offsetof(SpriteData_t257854902, ___trimmed_3)); }
	inline bool get_trimmed_3() const { return ___trimmed_3; }
	inline bool* get_address_of_trimmed_3() { return &___trimmed_3; }
	inline void set_trimmed_3(bool value)
	{
		___trimmed_3 = value;
	}

	inline static int32_t get_offset_of_spriteSourceSize_4() { return static_cast<int32_t>(offsetof(SpriteData_t257854902, ___spriteSourceSize_4)); }
	inline SpriteFrame_t3204261111  get_spriteSourceSize_4() const { return ___spriteSourceSize_4; }
	inline SpriteFrame_t3204261111 * get_address_of_spriteSourceSize_4() { return &___spriteSourceSize_4; }
	inline void set_spriteSourceSize_4(SpriteFrame_t3204261111  value)
	{
		___spriteSourceSize_4 = value;
	}

	inline static int32_t get_offset_of_sourceSize_5() { return static_cast<int32_t>(offsetof(SpriteData_t257854902, ___sourceSize_5)); }
	inline SpriteSize_t2240777863  get_sourceSize_5() const { return ___sourceSize_5; }
	inline SpriteSize_t2240777863 * get_address_of_sourceSize_5() { return &___sourceSize_5; }
	inline void set_sourceSize_5(SpriteSize_t2240777863  value)
	{
		___sourceSize_5 = value;
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(SpriteData_t257854902, ___pivot_6)); }
	inline Vector2_t2243707579  get_pivot_6() const { return ___pivot_6; }
	inline Vector2_t2243707579 * get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Vector2_t2243707579  value)
	{
		___pivot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t257854902_marshaled_pinvoke
{
	char* ___filename_0;
	SpriteFrame_t3204261111  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3204261111  ___spriteSourceSize_4;
	SpriteSize_t2240777863  ___sourceSize_5;
	Vector2_t2243707579  ___pivot_6;
};
// Native definition for COM marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t257854902_marshaled_com
{
	Il2CppChar* ___filename_0;
	SpriteFrame_t3204261111  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_t3204261111  ___spriteSourceSize_4;
	SpriteSize_t2240777863  ___sourceSize_5;
	Vector2_t2243707579  ___pivot_6;
};
#endif // SPRITEDATA_T257854902_H
#ifndef LINETYPE_T2475898675_H
#define LINETYPE_T2475898675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/LineType
struct  LineType_t2475898675 
{
public:
	// System.Int32 TMPro.TMP_InputField/LineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineType_t2475898675, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINETYPE_T2475898675_H
#ifndef TEXTSELECTIONEVENT_T3887183206_H
#define TEXTSELECTIONEVENT_T3887183206_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/TextSelectionEvent
struct  TextSelectionEvent_t3887183206  : public UnityEvent_3_t2425689862
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTSELECTIONEVENT_T3887183206_H
#ifndef TMP_MESHINFO_T1297308317_H
#define TMP_MESHINFO_T1297308317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MeshInfo
struct  TMP_MeshInfo_t1297308317 
{
public:
	// UnityEngine.Mesh TMPro.TMP_MeshInfo::mesh
	Mesh_t1356156583 * ___mesh_3;
	// System.Int32 TMPro.TMP_MeshInfo::vertexCount
	int32_t ___vertexCount_4;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::vertices
	Vector3U5BU5D_t1172311765* ___vertices_5;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::normals
	Vector3U5BU5D_t1172311765* ___normals_6;
	// UnityEngine.Vector4[] TMPro.TMP_MeshInfo::tangents
	Vector4U5BU5D_t1658499504* ___tangents_7;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs0
	Vector2U5BU5D_t686124026* ___uvs0_8;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs2
	Vector2U5BU5D_t686124026* ___uvs2_9;
	// UnityEngine.Color32[] TMPro.TMP_MeshInfo::colors32
	Color32U5BU5D_t30278651* ___colors32_10;
	// System.Int32[] TMPro.TMP_MeshInfo::triangles
	Int32U5BU5D_t3030399641* ___triangles_11;

public:
	inline static int32_t get_offset_of_mesh_3() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317, ___mesh_3)); }
	inline Mesh_t1356156583 * get_mesh_3() const { return ___mesh_3; }
	inline Mesh_t1356156583 ** get_address_of_mesh_3() { return &___mesh_3; }
	inline void set_mesh_3(Mesh_t1356156583 * value)
	{
		___mesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_3), value);
	}

	inline static int32_t get_offset_of_vertexCount_4() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317, ___vertexCount_4)); }
	inline int32_t get_vertexCount_4() const { return ___vertexCount_4; }
	inline int32_t* get_address_of_vertexCount_4() { return &___vertexCount_4; }
	inline void set_vertexCount_4(int32_t value)
	{
		___vertexCount_4 = value;
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317, ___vertices_5)); }
	inline Vector3U5BU5D_t1172311765* get_vertices_5() const { return ___vertices_5; }
	inline Vector3U5BU5D_t1172311765** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(Vector3U5BU5D_t1172311765* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_normals_6() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317, ___normals_6)); }
	inline Vector3U5BU5D_t1172311765* get_normals_6() const { return ___normals_6; }
	inline Vector3U5BU5D_t1172311765** get_address_of_normals_6() { return &___normals_6; }
	inline void set_normals_6(Vector3U5BU5D_t1172311765* value)
	{
		___normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___normals_6), value);
	}

	inline static int32_t get_offset_of_tangents_7() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317, ___tangents_7)); }
	inline Vector4U5BU5D_t1658499504* get_tangents_7() const { return ___tangents_7; }
	inline Vector4U5BU5D_t1658499504** get_address_of_tangents_7() { return &___tangents_7; }
	inline void set_tangents_7(Vector4U5BU5D_t1658499504* value)
	{
		___tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_7), value);
	}

	inline static int32_t get_offset_of_uvs0_8() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317, ___uvs0_8)); }
	inline Vector2U5BU5D_t686124026* get_uvs0_8() const { return ___uvs0_8; }
	inline Vector2U5BU5D_t686124026** get_address_of_uvs0_8() { return &___uvs0_8; }
	inline void set_uvs0_8(Vector2U5BU5D_t686124026* value)
	{
		___uvs0_8 = value;
		Il2CppCodeGenWriteBarrier((&___uvs0_8), value);
	}

	inline static int32_t get_offset_of_uvs2_9() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317, ___uvs2_9)); }
	inline Vector2U5BU5D_t686124026* get_uvs2_9() const { return ___uvs2_9; }
	inline Vector2U5BU5D_t686124026** get_address_of_uvs2_9() { return &___uvs2_9; }
	inline void set_uvs2_9(Vector2U5BU5D_t686124026* value)
	{
		___uvs2_9 = value;
		Il2CppCodeGenWriteBarrier((&___uvs2_9), value);
	}

	inline static int32_t get_offset_of_colors32_10() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317, ___colors32_10)); }
	inline Color32U5BU5D_t30278651* get_colors32_10() const { return ___colors32_10; }
	inline Color32U5BU5D_t30278651** get_address_of_colors32_10() { return &___colors32_10; }
	inline void set_colors32_10(Color32U5BU5D_t30278651* value)
	{
		___colors32_10 = value;
		Il2CppCodeGenWriteBarrier((&___colors32_10), value);
	}

	inline static int32_t get_offset_of_triangles_11() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317, ___triangles_11)); }
	inline Int32U5BU5D_t3030399641* get_triangles_11() const { return ___triangles_11; }
	inline Int32U5BU5D_t3030399641** get_address_of_triangles_11() { return &___triangles_11; }
	inline void set_triangles_11(Int32U5BU5D_t3030399641* value)
	{
		___triangles_11 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_11), value);
	}
};

struct TMP_MeshInfo_t1297308317_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_MeshInfo::s_DefaultColor
	Color32_t874517518  ___s_DefaultColor_0;
	// UnityEngine.Vector3 TMPro.TMP_MeshInfo::s_DefaultNormal
	Vector3_t2243707580  ___s_DefaultNormal_1;
	// UnityEngine.Vector4 TMPro.TMP_MeshInfo::s_DefaultTangent
	Vector4_t2243707581  ___s_DefaultTangent_2;

public:
	inline static int32_t get_offset_of_s_DefaultColor_0() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317_StaticFields, ___s_DefaultColor_0)); }
	inline Color32_t874517518  get_s_DefaultColor_0() const { return ___s_DefaultColor_0; }
	inline Color32_t874517518 * get_address_of_s_DefaultColor_0() { return &___s_DefaultColor_0; }
	inline void set_s_DefaultColor_0(Color32_t874517518  value)
	{
		___s_DefaultColor_0 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_1() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317_StaticFields, ___s_DefaultNormal_1)); }
	inline Vector3_t2243707580  get_s_DefaultNormal_1() const { return ___s_DefaultNormal_1; }
	inline Vector3_t2243707580 * get_address_of_s_DefaultNormal_1() { return &___s_DefaultNormal_1; }
	inline void set_s_DefaultNormal_1(Vector3_t2243707580  value)
	{
		___s_DefaultNormal_1 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_2() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t1297308317_StaticFields, ___s_DefaultTangent_2)); }
	inline Vector4_t2243707581  get_s_DefaultTangent_2() const { return ___s_DefaultTangent_2; }
	inline Vector4_t2243707581 * get_address_of_s_DefaultTangent_2() { return &___s_DefaultTangent_2; }
	inline void set_s_DefaultTangent_2(Vector4_t2243707581  value)
	{
		___s_DefaultTangent_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t1297308317_marshaled_pinvoke
{
	Mesh_t1356156583 * ___mesh_3;
	int32_t ___vertexCount_4;
	Vector3_t2243707580 * ___vertices_5;
	Vector3_t2243707580 * ___normals_6;
	Vector4_t2243707581 * ___tangents_7;
	Vector2_t2243707579 * ___uvs0_8;
	Vector2_t2243707579 * ___uvs2_9;
	Color32_t874517518 * ___colors32_10;
	int32_t* ___triangles_11;
};
// Native definition for COM marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t1297308317_marshaled_com
{
	Mesh_t1356156583 * ___mesh_3;
	int32_t ___vertexCount_4;
	Vector3_t2243707580 * ___vertices_5;
	Vector3_t2243707580 * ___normals_6;
	Vector4_t2243707581 * ___tangents_7;
	Vector2_t2243707579 * ___uvs0_8;
	Vector2_t2243707579 * ___uvs2_9;
	Color32_t874517518 * ___colors32_10;
	int32_t* ___triangles_11;
};
#endif // TMP_MESHINFO_T1297308317_H
#ifndef U3CMOUSEDRAGOUTSIDERECTU3EC__ITERATOR1_T2119315303_H
#define U3CMOUSEDRAGOUTSIDERECTU3EC__ITERATOR1_T2119315303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1
struct  U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303  : public RuntimeObject
{
public:
	// UnityEngine.EventSystems.PointerEventData TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::eventData
	PointerEventData_t1599784723 * ___eventData_0;
	// UnityEngine.Vector2 TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::<localMousePos>__1
	Vector2_t2243707579  ___U3ClocalMousePosU3E__1_1;
	// UnityEngine.Rect TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::<rect>__1
	Rect_t3681755626  ___U3CrectU3E__1_2;
	// System.Single TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::<delay>__1
	float ___U3CdelayU3E__1_3;
	// TMPro.TMP_InputField TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::$this
	TMP_InputField_t1778301588 * ___U24this_4;
	// System.Object TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 TMPro.TMP_InputField/<MouseDragOutsideRect>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303, ___eventData_0)); }
	inline PointerEventData_t1599784723 * get_eventData_0() const { return ___eventData_0; }
	inline PointerEventData_t1599784723 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(PointerEventData_t1599784723 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}

	inline static int32_t get_offset_of_U3ClocalMousePosU3E__1_1() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303, ___U3ClocalMousePosU3E__1_1)); }
	inline Vector2_t2243707579  get_U3ClocalMousePosU3E__1_1() const { return ___U3ClocalMousePosU3E__1_1; }
	inline Vector2_t2243707579 * get_address_of_U3ClocalMousePosU3E__1_1() { return &___U3ClocalMousePosU3E__1_1; }
	inline void set_U3ClocalMousePosU3E__1_1(Vector2_t2243707579  value)
	{
		___U3ClocalMousePosU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CrectU3E__1_2() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303, ___U3CrectU3E__1_2)); }
	inline Rect_t3681755626  get_U3CrectU3E__1_2() const { return ___U3CrectU3E__1_2; }
	inline Rect_t3681755626 * get_address_of_U3CrectU3E__1_2() { return &___U3CrectU3E__1_2; }
	inline void set_U3CrectU3E__1_2(Rect_t3681755626  value)
	{
		___U3CrectU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CdelayU3E__1_3() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303, ___U3CdelayU3E__1_3)); }
	inline float get_U3CdelayU3E__1_3() const { return ___U3CdelayU3E__1_3; }
	inline float* get_address_of_U3CdelayU3E__1_3() { return &___U3CdelayU3E__1_3; }
	inline void set_U3CdelayU3E__1_3(float value)
	{
		___U3CdelayU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303, ___U24this_4)); }
	inline TMP_InputField_t1778301588 * get_U24this_4() const { return ___U24this_4; }
	inline TMP_InputField_t1778301588 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TMP_InputField_t1778301588 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOUSEDRAGOUTSIDERECTU3EC__ITERATOR1_T2119315303_H
#ifndef EDITSTATE_T4052837484_H
#define EDITSTATE_T4052837484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/EditState
struct  EditState_t4052837484 
{
public:
	// System.Int32 TMPro.TMP_InputField/EditState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EditState_t4052837484, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITSTATE_T4052837484_H
#ifndef SUBMITEVENT_T3359162065_H
#define SUBMITEVENT_T3359162065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/SubmitEvent
struct  SubmitEvent_t3359162065  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBMITEVENT_T3359162065_H
#ifndef TMP_SPRITE_T104383505_H
#define TMP_SPRITE_T104383505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Sprite
struct  TMP_Sprite_t104383505  : public TMP_TextElement_t2285620223
{
public:
	// System.String TMPro.TMP_Sprite::name
	String_t* ___name_9;
	// System.Int32 TMPro.TMP_Sprite::hashCode
	int32_t ___hashCode_10;
	// System.Int32 TMPro.TMP_Sprite::unicode
	int32_t ___unicode_11;
	// UnityEngine.Vector2 TMPro.TMP_Sprite::pivot
	Vector2_t2243707579  ___pivot_12;
	// UnityEngine.Sprite TMPro.TMP_Sprite::sprite
	Sprite_t309593783 * ___sprite_13;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(TMP_Sprite_t104383505, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_hashCode_10() { return static_cast<int32_t>(offsetof(TMP_Sprite_t104383505, ___hashCode_10)); }
	inline int32_t get_hashCode_10() const { return ___hashCode_10; }
	inline int32_t* get_address_of_hashCode_10() { return &___hashCode_10; }
	inline void set_hashCode_10(int32_t value)
	{
		___hashCode_10 = value;
	}

	inline static int32_t get_offset_of_unicode_11() { return static_cast<int32_t>(offsetof(TMP_Sprite_t104383505, ___unicode_11)); }
	inline int32_t get_unicode_11() const { return ___unicode_11; }
	inline int32_t* get_address_of_unicode_11() { return &___unicode_11; }
	inline void set_unicode_11(int32_t value)
	{
		___unicode_11 = value;
	}

	inline static int32_t get_offset_of_pivot_12() { return static_cast<int32_t>(offsetof(TMP_Sprite_t104383505, ___pivot_12)); }
	inline Vector2_t2243707579  get_pivot_12() const { return ___pivot_12; }
	inline Vector2_t2243707579 * get_address_of_pivot_12() { return &___pivot_12; }
	inline void set_pivot_12(Vector2_t2243707579  value)
	{
		___pivot_12 = value;
	}

	inline static int32_t get_offset_of_sprite_13() { return static_cast<int32_t>(offsetof(TMP_Sprite_t104383505, ___sprite_13)); }
	inline Sprite_t309593783 * get_sprite_13() const { return ___sprite_13; }
	inline Sprite_t309593783 ** get_address_of_sprite_13() { return &___sprite_13; }
	inline void set_sprite_13(Sprite_t309593783 * value)
	{
		___sprite_13 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITE_T104383505_H
#ifndef SELECTIONEVENT_T3883897865_H
#define SELECTIONEVENT_T3883897865_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/SelectionEvent
struct  SelectionEvent_t3883897865  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTIONEVENT_T3883897865_H
#ifndef ONCHANGEEVENT_T2139251414_H
#define ONCHANGEEVENT_T2139251414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/OnChangeEvent
struct  OnChangeEvent_t2139251414  : public UnityEvent_1_t2067570248
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCHANGEEVENT_T2139251414_H
#ifndef FLOATTWEENCALLBACK_T2368686856_H
#define FLOATTWEENCALLBACK_T2368686856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FloatTween/FloatTweenCallback
struct  FloatTweenCallback_t2368686856  : public UnityEvent_1_t2114859947
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATTWEENCALLBACK_T2368686856_H
#ifndef TEXTUREMAPPINGOPTIONS_T761764377_H
#define TEXTUREMAPPINGOPTIONS_T761764377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_t761764377 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureMappingOptions_t761764377, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_T761764377_H
#ifndef FONTSTYLES_T3171728781_H
#define FONTSTYLES_T3171728781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t3171728781 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyles_t3171728781, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T3171728781_H
#ifndef TEXTOVERFLOWMODES_T2644609723_H
#define TEXTOVERFLOWMODES_T2644609723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_t2644609723 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextOverflowModes_t2644609723, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_T2644609723_H
#ifndef MASKINGOFFSETMODE_T92608302_H
#define MASKINGOFFSETMODE_T92608302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingOffsetMode
struct  MaskingOffsetMode_t92608302 
{
public:
	// System.Int32 TMPro.MaskingOffsetMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingOffsetMode_t92608302, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGOFFSETMODE_T92608302_H
#ifndef TAGTYPE_T1698342214_H
#define TAGTYPE_T1698342214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagType
struct  TagType_t1698342214 
{
public:
	// System.Int32 TMPro.TagType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagType_t1698342214, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGTYPE_T1698342214_H
#ifndef VERTEXSORTINGORDER_T471281372_H
#define VERTEXSORTINGORDER_T471281372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t471281372 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t471281372, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T471281372_H
#ifndef FONTWEIGHTS_T1074713040_H
#define FONTWEIGHTS_T1074713040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeights
struct  FontWeights_t1074713040 
{
public:
	// System.Int32 TMPro.FontWeights::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontWeights_t1074713040, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHTS_T1074713040_H
#ifndef TAGUNITS_T1942447065_H
#define TAGUNITS_T1942447065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnits
struct  TagUnits_t1942447065 
{
public:
	// System.Int32 TMPro.TagUnits::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TagUnits_t1942447065, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITS_T1942447065_H
#ifndef MASKINGTYPES_T259687445_H
#define MASKINGTYPES_T259687445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t259687445 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MaskingTypes_t259687445, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T259687445_H
#ifndef COLORTWEENCALLBACK_T1307749522_H
#define COLORTWEENCALLBACK_T1307749522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorTween/ColorTweenCallback
struct  ColorTweenCallback_t1307749522  : public UnityEvent_1_t2058742090
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENCALLBACK_T1307749522_H
#ifndef COLORTWEENMODE_T903336102_H
#define COLORTWEENMODE_T903336102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorTween/ColorTweenMode
struct  ColorTweenMode_t903336102 
{
public:
	// System.Int32 TMPro.ColorTween/ColorTweenMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorTweenMode_t903336102, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTWEENMODE_T903336102_H
#ifndef TEXTALIGNMENTOPTIONS_T1466788324_H
#define TEXTALIGNMENTOPTIONS_T1466788324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t1466788324 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t1466788324, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T1466788324_H
#ifndef _HORIZONTALALIGNMENTOPTIONS_T1321544838_H
#define _HORIZONTALALIGNMENTOPTIONS_T1321544838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._HorizontalAlignmentOptions
struct  _HorizontalAlignmentOptions_t1321544838 
{
public:
	// System.Int32 TMPro._HorizontalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_HorizontalAlignmentOptions_t1321544838, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _HORIZONTALALIGNMENTOPTIONS_T1321544838_H
#ifndef _VERTICALALIGNMENTOPTIONS_T3567775144_H
#define _VERTICALALIGNMENTOPTIONS_T3567775144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._VerticalAlignmentOptions
struct  _VerticalAlignmentOptions_t3567775144 
{
public:
	// System.Int32 TMPro._VerticalAlignmentOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_VerticalAlignmentOptions_t3567775144, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _VERTICALALIGNMENTOPTIONS_T3567775144_H
#ifndef TEXTRENDERFLAGS_T7026704_H
#define TEXTRENDERFLAGS_T7026704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t7026704 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextRenderFlags_t7026704, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T7026704_H
#ifndef TMP_TEXTELEMENTTYPE_T1959651783_H
#define TMP_TEXTELEMENTTYPE_T1959651783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_t1959651783 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TMP_TextElementType_t1959651783, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_T1959651783_H
#ifndef NAVIGATION_T1571958496_H
#define NAVIGATION_T1571958496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Navigation
struct  Navigation_t1571958496 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t1490392188 * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnUp_1)); }
	inline Selectable_t1490392188 * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_t1490392188 * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnUp_1), value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnDown_2)); }
	inline Selectable_t1490392188 * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_t1490392188 * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnDown_2), value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnLeft_3)); }
	inline Selectable_t1490392188 * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_t1490392188 * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnLeft_3), value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t1571958496, ___m_SelectOnRight_4)); }
	inline Selectable_t1490392188 * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_t1490392188 ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_t1490392188 * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_SelectOnRight_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1571958496_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_t1490392188 * ___m_SelectOnUp_1;
	Selectable_t1490392188 * ___m_SelectOnDown_2;
	Selectable_t1490392188 * ___m_SelectOnLeft_3;
	Selectable_t1490392188 * ___m_SelectOnRight_4;
};
#endif // NAVIGATION_T1571958496_H
#ifndef TMP_XMLTAGSTACK_1_T2125340843_H
#define TMP_XMLTAGSTACK_1_T2125340843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_XmlTagStack_1_t2125340843 
{
public:
	// T[] TMPro.TMP_XmlTagStack`1::itemStack
	TextAlignmentOptionsU5BU5D_t1615060493* ___itemStack_0;
	// System.Int32 TMPro.TMP_XmlTagStack`1::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_XmlTagStack`1::m_capacity
	int32_t ___m_capacity_2;
	// T TMPro.TMP_XmlTagStack`1::m_defaultItem
	int32_t ___m_defaultItem_3;

public:
	inline static int32_t get_offset_of_itemStack_0() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2125340843, ___itemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t1615060493* get_itemStack_0() const { return ___itemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t1615060493** get_address_of_itemStack_0() { return &___itemStack_0; }
	inline void set_itemStack_0(TextAlignmentOptionsU5BU5D_t1615060493* value)
	{
		___itemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___itemStack_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2125340843, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_m_capacity_2() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2125340843, ___m_capacity_2)); }
	inline int32_t get_m_capacity_2() const { return ___m_capacity_2; }
	inline int32_t* get_address_of_m_capacity_2() { return &___m_capacity_2; }
	inline void set_m_capacity_2(int32_t value)
	{
		___m_capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_defaultItem_3() { return static_cast<int32_t>(offsetof(TMP_XmlTagStack_1_t2125340843, ___m_defaultItem_3)); }
	inline int32_t get_m_defaultItem_3() const { return ___m_defaultItem_3; }
	inline int32_t* get_address_of_m_defaultItem_3() { return &___m_defaultItem_3; }
	inline void set_m_defaultItem_3(int32_t value)
	{
		___m_defaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_XMLTAGSTACK_1_T2125340843_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef TMP_CHARACTERINFO_T1421302791_H
#define TMP_CHARACTERINFO_T1421302791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t1421302791 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_2;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_t2285620223 * ___textElement_3;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t2530419979 * ___fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_6;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_t193706927 * ___material_7;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_8;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_9;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_10;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_13;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t1422870470  ___vertex_TL_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t1422870470  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t1422870470  ___vertex_TR_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t1422870470  ___vertex_BR_17;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_t2243707580  ___topLeft_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_t2243707580  ___bottomLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_t2243707580  ___topRight_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_t2243707580  ___bottomRight_21;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_22;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_23;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_24;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_25;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_26;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_27;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_28;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t874517518  ___color_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t874517518  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t874517518  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t874517518  ___highlightColor_32;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_33;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_34;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_elementType_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___elementType_2)); }
	inline int32_t get_elementType_2() const { return ___elementType_2; }
	inline int32_t* get_address_of_elementType_2() { return &___elementType_2; }
	inline void set_elementType_2(int32_t value)
	{
		___elementType_2 = value;
	}

	inline static int32_t get_offset_of_textElement_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___textElement_3)); }
	inline TMP_TextElement_t2285620223 * get_textElement_3() const { return ___textElement_3; }
	inline TMP_TextElement_t2285620223 ** get_address_of_textElement_3() { return &___textElement_3; }
	inline void set_textElement_3(TMP_TextElement_t2285620223 * value)
	{
		___textElement_3 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_3), value);
	}

	inline static int32_t get_offset_of_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___fontAsset_4)); }
	inline TMP_FontAsset_t2530419979 * get_fontAsset_4() const { return ___fontAsset_4; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_fontAsset_4() { return &___fontAsset_4; }
	inline void set_fontAsset_4(TMP_FontAsset_t2530419979 * value)
	{
		___fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_4), value);
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_t2641813093 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_t2641813093 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteIndex_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___spriteIndex_6)); }
	inline int32_t get_spriteIndex_6() const { return ___spriteIndex_6; }
	inline int32_t* get_address_of_spriteIndex_6() { return &___spriteIndex_6; }
	inline void set_spriteIndex_6(int32_t value)
	{
		___spriteIndex_6 = value;
	}

	inline static int32_t get_offset_of_material_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___material_7)); }
	inline Material_t193706927 * get_material_7() const { return ___material_7; }
	inline Material_t193706927 ** get_address_of_material_7() { return &___material_7; }
	inline void set_material_7(Material_t193706927 * value)
	{
		___material_7 = value;
		Il2CppCodeGenWriteBarrier((&___material_7), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___materialReferenceIndex_8)); }
	inline int32_t get_materialReferenceIndex_8() const { return ___materialReferenceIndex_8; }
	inline int32_t* get_address_of_materialReferenceIndex_8() { return &___materialReferenceIndex_8; }
	inline void set_materialReferenceIndex_8(int32_t value)
	{
		___materialReferenceIndex_8 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___isUsingAlternateTypeface_9)); }
	inline bool get_isUsingAlternateTypeface_9() const { return ___isUsingAlternateTypeface_9; }
	inline bool* get_address_of_isUsingAlternateTypeface_9() { return &___isUsingAlternateTypeface_9; }
	inline void set_isUsingAlternateTypeface_9(bool value)
	{
		___isUsingAlternateTypeface_9 = value;
	}

	inline static int32_t get_offset_of_pointSize_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___pointSize_10)); }
	inline float get_pointSize_10() const { return ___pointSize_10; }
	inline float* get_address_of_pointSize_10() { return &___pointSize_10; }
	inline void set_pointSize_10(float value)
	{
		___pointSize_10 = value;
	}

	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_pageNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___pageNumber_12)); }
	inline int32_t get_pageNumber_12() const { return ___pageNumber_12; }
	inline int32_t* get_address_of_pageNumber_12() { return &___pageNumber_12; }
	inline void set_pageNumber_12(int32_t value)
	{
		___pageNumber_12 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertexIndex_13)); }
	inline int32_t get_vertexIndex_13() const { return ___vertexIndex_13; }
	inline int32_t* get_address_of_vertexIndex_13() { return &___vertexIndex_13; }
	inline void set_vertexIndex_13(int32_t value)
	{
		___vertexIndex_13 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertex_TL_14)); }
	inline TMP_Vertex_t1422870470  get_vertex_TL_14() const { return ___vertex_TL_14; }
	inline TMP_Vertex_t1422870470 * get_address_of_vertex_TL_14() { return &___vertex_TL_14; }
	inline void set_vertex_TL_14(TMP_Vertex_t1422870470  value)
	{
		___vertex_TL_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertex_BL_15)); }
	inline TMP_Vertex_t1422870470  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t1422870470 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t1422870470  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertex_TR_16)); }
	inline TMP_Vertex_t1422870470  get_vertex_TR_16() const { return ___vertex_TR_16; }
	inline TMP_Vertex_t1422870470 * get_address_of_vertex_TR_16() { return &___vertex_TR_16; }
	inline void set_vertex_TR_16(TMP_Vertex_t1422870470  value)
	{
		___vertex_TR_16 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___vertex_BR_17)); }
	inline TMP_Vertex_t1422870470  get_vertex_BR_17() const { return ___vertex_BR_17; }
	inline TMP_Vertex_t1422870470 * get_address_of_vertex_BR_17() { return &___vertex_BR_17; }
	inline void set_vertex_BR_17(TMP_Vertex_t1422870470  value)
	{
		___vertex_BR_17 = value;
	}

	inline static int32_t get_offset_of_topLeft_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___topLeft_18)); }
	inline Vector3_t2243707580  get_topLeft_18() const { return ___topLeft_18; }
	inline Vector3_t2243707580 * get_address_of_topLeft_18() { return &___topLeft_18; }
	inline void set_topLeft_18(Vector3_t2243707580  value)
	{
		___topLeft_18 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___bottomLeft_19)); }
	inline Vector3_t2243707580  get_bottomLeft_19() const { return ___bottomLeft_19; }
	inline Vector3_t2243707580 * get_address_of_bottomLeft_19() { return &___bottomLeft_19; }
	inline void set_bottomLeft_19(Vector3_t2243707580  value)
	{
		___bottomLeft_19 = value;
	}

	inline static int32_t get_offset_of_topRight_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___topRight_20)); }
	inline Vector3_t2243707580  get_topRight_20() const { return ___topRight_20; }
	inline Vector3_t2243707580 * get_address_of_topRight_20() { return &___topRight_20; }
	inline void set_topRight_20(Vector3_t2243707580  value)
	{
		___topRight_20 = value;
	}

	inline static int32_t get_offset_of_bottomRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___bottomRight_21)); }
	inline Vector3_t2243707580  get_bottomRight_21() const { return ___bottomRight_21; }
	inline Vector3_t2243707580 * get_address_of_bottomRight_21() { return &___bottomRight_21; }
	inline void set_bottomRight_21(Vector3_t2243707580  value)
	{
		___bottomRight_21 = value;
	}

	inline static int32_t get_offset_of_origin_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___origin_22)); }
	inline float get_origin_22() const { return ___origin_22; }
	inline float* get_address_of_origin_22() { return &___origin_22; }
	inline void set_origin_22(float value)
	{
		___origin_22 = value;
	}

	inline static int32_t get_offset_of_ascender_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___ascender_23)); }
	inline float get_ascender_23() const { return ___ascender_23; }
	inline float* get_address_of_ascender_23() { return &___ascender_23; }
	inline void set_ascender_23(float value)
	{
		___ascender_23 = value;
	}

	inline static int32_t get_offset_of_baseLine_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___baseLine_24)); }
	inline float get_baseLine_24() const { return ___baseLine_24; }
	inline float* get_address_of_baseLine_24() { return &___baseLine_24; }
	inline void set_baseLine_24(float value)
	{
		___baseLine_24 = value;
	}

	inline static int32_t get_offset_of_descender_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___descender_25)); }
	inline float get_descender_25() const { return ___descender_25; }
	inline float* get_address_of_descender_25() { return &___descender_25; }
	inline void set_descender_25(float value)
	{
		___descender_25 = value;
	}

	inline static int32_t get_offset_of_xAdvance_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___xAdvance_26)); }
	inline float get_xAdvance_26() const { return ___xAdvance_26; }
	inline float* get_address_of_xAdvance_26() { return &___xAdvance_26; }
	inline void set_xAdvance_26(float value)
	{
		___xAdvance_26 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___aspectRatio_27)); }
	inline float get_aspectRatio_27() const { return ___aspectRatio_27; }
	inline float* get_address_of_aspectRatio_27() { return &___aspectRatio_27; }
	inline void set_aspectRatio_27(float value)
	{
		___aspectRatio_27 = value;
	}

	inline static int32_t get_offset_of_scale_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___scale_28)); }
	inline float get_scale_28() const { return ___scale_28; }
	inline float* get_address_of_scale_28() { return &___scale_28; }
	inline void set_scale_28(float value)
	{
		___scale_28 = value;
	}

	inline static int32_t get_offset_of_color_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___color_29)); }
	inline Color32_t874517518  get_color_29() const { return ___color_29; }
	inline Color32_t874517518 * get_address_of_color_29() { return &___color_29; }
	inline void set_color_29(Color32_t874517518  value)
	{
		___color_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___underlineColor_30)); }
	inline Color32_t874517518  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t874517518 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t874517518  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___strikethroughColor_31)); }
	inline Color32_t874517518  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t874517518 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t874517518  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___highlightColor_32)); }
	inline Color32_t874517518  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t874517518 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t874517518  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_style_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___style_33)); }
	inline int32_t get_style_33() const { return ___style_33; }
	inline int32_t* get_address_of_style_33() { return &___style_33; }
	inline void set_style_33(int32_t value)
	{
		___style_33 = value;
	}

	inline static int32_t get_offset_of_isVisible_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t1421302791, ___isVisible_34)); }
	inline bool get_isVisible_34() const { return ___isVisible_34; }
	inline bool* get_address_of_isVisible_34() { return &___isVisible_34; }
	inline void set_isVisible_34(bool value)
	{
		___isVisible_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t1421302791_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t2285620223 * ___textElement_3;
	TMP_FontAsset_t2530419979 * ___fontAsset_4;
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t193706927 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t1422870470  ___vertex_TL_14;
	TMP_Vertex_t1422870470  ___vertex_BL_15;
	TMP_Vertex_t1422870470  ___vertex_TR_16;
	TMP_Vertex_t1422870470  ___vertex_BR_17;
	Vector3_t2243707580  ___topLeft_18;
	Vector3_t2243707580  ___bottomLeft_19;
	Vector3_t2243707580  ___topRight_20;
	Vector3_t2243707580  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t874517518  ___color_29;
	Color32_t874517518  ___underlineColor_30;
	Color32_t874517518  ___strikethroughColor_31;
	Color32_t874517518  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t1421302791_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___elementType_2;
	TMP_TextElement_t2285620223 * ___textElement_3;
	TMP_FontAsset_t2530419979 * ___fontAsset_4;
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_5;
	int32_t ___spriteIndex_6;
	Material_t193706927 * ___material_7;
	int32_t ___materialReferenceIndex_8;
	int32_t ___isUsingAlternateTypeface_9;
	float ___pointSize_10;
	int32_t ___lineNumber_11;
	int32_t ___pageNumber_12;
	int32_t ___vertexIndex_13;
	TMP_Vertex_t1422870470  ___vertex_TL_14;
	TMP_Vertex_t1422870470  ___vertex_BL_15;
	TMP_Vertex_t1422870470  ___vertex_TR_16;
	TMP_Vertex_t1422870470  ___vertex_BR_17;
	Vector3_t2243707580  ___topLeft_18;
	Vector3_t2243707580  ___bottomLeft_19;
	Vector3_t2243707580  ___topRight_20;
	Vector3_t2243707580  ___bottomRight_21;
	float ___origin_22;
	float ___ascender_23;
	float ___baseLine_24;
	float ___descender_25;
	float ___xAdvance_26;
	float ___aspectRatio_27;
	float ___scale_28;
	Color32_t874517518  ___color_29;
	Color32_t874517518  ___underlineColor_30;
	Color32_t874517518  ___strikethroughColor_31;
	Color32_t874517518  ___highlightColor_32;
	int32_t ___style_33;
	int32_t ___isVisible_34;
};
#endif // TMP_CHARACTERINFO_T1421302791_H
#ifndef CARETINFO_T598977269_H
#define CARETINFO_T598977269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretInfo
struct  CaretInfo_t598977269 
{
public:
	// System.Int32 TMPro.CaretInfo::index
	int32_t ___index_0;
	// TMPro.CaretPosition TMPro.CaretInfo::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CaretInfo_t598977269, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(CaretInfo_t598977269, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETINFO_T598977269_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef TMP_LINEINFO_T2320418126_H
#define TMP_LINEINFO_T2320418126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_t2320418126 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_2;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_3;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_7;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_8;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_9;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_10;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_11;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_12;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_13;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_14;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_15;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_16;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_17;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_t3018556803  ___lineExtents_18;

public:
	inline static int32_t get_offset_of_characterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___characterCount_0)); }
	inline int32_t get_characterCount_0() const { return ___characterCount_0; }
	inline int32_t* get_address_of_characterCount_0() { return &___characterCount_0; }
	inline void set_characterCount_0(int32_t value)
	{
		___characterCount_0 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___visibleCharacterCount_1)); }
	inline int32_t get_visibleCharacterCount_1() const { return ___visibleCharacterCount_1; }
	inline int32_t* get_address_of_visibleCharacterCount_1() { return &___visibleCharacterCount_1; }
	inline void set_visibleCharacterCount_1(int32_t value)
	{
		___visibleCharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_spaceCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___spaceCount_2)); }
	inline int32_t get_spaceCount_2() const { return ___spaceCount_2; }
	inline int32_t* get_address_of_spaceCount_2() { return &___spaceCount_2; }
	inline void set_spaceCount_2(int32_t value)
	{
		___spaceCount_2 = value;
	}

	inline static int32_t get_offset_of_wordCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___wordCount_3)); }
	inline int32_t get_wordCount_3() const { return ___wordCount_3; }
	inline int32_t* get_address_of_wordCount_3() { return &___wordCount_3; }
	inline void set_wordCount_3(int32_t value)
	{
		___wordCount_3 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___firstCharacterIndex_4)); }
	inline int32_t get_firstCharacterIndex_4() const { return ___firstCharacterIndex_4; }
	inline int32_t* get_address_of_firstCharacterIndex_4() { return &___firstCharacterIndex_4; }
	inline void set_firstCharacterIndex_4(int32_t value)
	{
		___firstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___firstVisibleCharacterIndex_5)); }
	inline int32_t get_firstVisibleCharacterIndex_5() const { return ___firstVisibleCharacterIndex_5; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_5() { return &___firstVisibleCharacterIndex_5; }
	inline void set_firstVisibleCharacterIndex_5(int32_t value)
	{
		___firstVisibleCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___lastCharacterIndex_6)); }
	inline int32_t get_lastCharacterIndex_6() const { return ___lastCharacterIndex_6; }
	inline int32_t* get_address_of_lastCharacterIndex_6() { return &___lastCharacterIndex_6; }
	inline void set_lastCharacterIndex_6(int32_t value)
	{
		___lastCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___lastVisibleCharacterIndex_7)); }
	inline int32_t get_lastVisibleCharacterIndex_7() const { return ___lastVisibleCharacterIndex_7; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_7() { return &___lastVisibleCharacterIndex_7; }
	inline void set_lastVisibleCharacterIndex_7(int32_t value)
	{
		___lastVisibleCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_lineHeight_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___lineHeight_9)); }
	inline float get_lineHeight_9() const { return ___lineHeight_9; }
	inline float* get_address_of_lineHeight_9() { return &___lineHeight_9; }
	inline void set_lineHeight_9(float value)
	{
		___lineHeight_9 = value;
	}

	inline static int32_t get_offset_of_ascender_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___ascender_10)); }
	inline float get_ascender_10() const { return ___ascender_10; }
	inline float* get_address_of_ascender_10() { return &___ascender_10; }
	inline void set_ascender_10(float value)
	{
		___ascender_10 = value;
	}

	inline static int32_t get_offset_of_baseline_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___baseline_11)); }
	inline float get_baseline_11() const { return ___baseline_11; }
	inline float* get_address_of_baseline_11() { return &___baseline_11; }
	inline void set_baseline_11(float value)
	{
		___baseline_11 = value;
	}

	inline static int32_t get_offset_of_descender_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___descender_12)); }
	inline float get_descender_12() const { return ___descender_12; }
	inline float* get_address_of_descender_12() { return &___descender_12; }
	inline void set_descender_12(float value)
	{
		___descender_12 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___maxAdvance_13)); }
	inline float get_maxAdvance_13() const { return ___maxAdvance_13; }
	inline float* get_address_of_maxAdvance_13() { return &___maxAdvance_13; }
	inline void set_maxAdvance_13(float value)
	{
		___maxAdvance_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_marginLeft_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___marginLeft_15)); }
	inline float get_marginLeft_15() const { return ___marginLeft_15; }
	inline float* get_address_of_marginLeft_15() { return &___marginLeft_15; }
	inline void set_marginLeft_15(float value)
	{
		___marginLeft_15 = value;
	}

	inline static int32_t get_offset_of_marginRight_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___marginRight_16)); }
	inline float get_marginRight_16() const { return ___marginRight_16; }
	inline float* get_address_of_marginRight_16() { return &___marginRight_16; }
	inline void set_marginRight_16(float value)
	{
		___marginRight_16 = value;
	}

	inline static int32_t get_offset_of_alignment_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___alignment_17)); }
	inline int32_t get_alignment_17() const { return ___alignment_17; }
	inline int32_t* get_address_of_alignment_17() { return &___alignment_17; }
	inline void set_alignment_17(int32_t value)
	{
		___alignment_17 = value;
	}

	inline static int32_t get_offset_of_lineExtents_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_t2320418126, ___lineExtents_18)); }
	inline Extents_t3018556803  get_lineExtents_18() const { return ___lineExtents_18; }
	inline Extents_t3018556803 * get_address_of_lineExtents_18() { return &___lineExtents_18; }
	inline void set_lineExtents_18(Extents_t3018556803  value)
	{
		___lineExtents_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_T2320418126_H
#ifndef COLORTWEEN_T1637921736_H
#define COLORTWEEN_T1637921736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorTween
struct  ColorTween_t1637921736 
{
public:
	// TMPro.ColorTween/ColorTweenCallback TMPro.ColorTween::m_Target
	ColorTweenCallback_t1307749522 * ___m_Target_0;
	// UnityEngine.Color TMPro.ColorTween::m_StartColor
	Color_t2020392075  ___m_StartColor_1;
	// UnityEngine.Color TMPro.ColorTween::m_TargetColor
	Color_t2020392075  ___m_TargetColor_2;
	// TMPro.ColorTween/ColorTweenMode TMPro.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single TMPro.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean TMPro.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t1637921736, ___m_Target_0)); }
	inline ColorTweenCallback_t1307749522 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_t1307749522 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_t1307749522 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t1637921736, ___m_StartColor_1)); }
	inline Color_t2020392075  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t2020392075 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t2020392075  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t1637921736, ___m_TargetColor_2)); }
	inline Color_t2020392075  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t2020392075 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t2020392075  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t1637921736, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t1637921736, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t1637921736, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.ColorTween
struct ColorTween_t1637921736_marshaled_pinvoke
{
	ColorTweenCallback_t1307749522 * ___m_Target_0;
	Color_t2020392075  ___m_StartColor_1;
	Color_t2020392075  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of TMPro.ColorTween
struct ColorTween_t1637921736_marshaled_com
{
	ColorTweenCallback_t1307749522 * ___m_Target_0;
	Color_t2020392075  ___m_StartColor_1;
	Color_t2020392075  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
#endif // COLORTWEEN_T1637921736_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef TMP_ASSET_T1084708044_H
#define TMP_ASSET_T1084708044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Asset
struct  TMP_Asset_t1084708044  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 TMPro.TMP_Asset::hashCode
	int32_t ___hashCode_2;
	// UnityEngine.Material TMPro.TMP_Asset::material
	Material_t193706927 * ___material_3;
	// System.Int32 TMPro.TMP_Asset::materialHashCode
	int32_t ___materialHashCode_4;

public:
	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TMP_Asset_t1084708044, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(TMP_Asset_t1084708044, ___material_3)); }
	inline Material_t193706927 * get_material_3() const { return ___material_3; }
	inline Material_t193706927 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t193706927 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_materialHashCode_4() { return static_cast<int32_t>(offsetof(TMP_Asset_t1084708044, ___materialHashCode_4)); }
	inline int32_t get_materialHashCode_4() const { return ___materialHashCode_4; }
	inline int32_t* get_address_of_materialHashCode_4() { return &___materialHashCode_4; }
	inline void set_materialHashCode_4(int32_t value)
	{
		___materialHashCode_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_ASSET_T1084708044_H
#ifndef WORDWRAPSTATE_T433984875_H
#define WORDWRAPSTATE_T433984875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t433984875 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t2849466151 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t2320418126  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t874517518  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t874517518  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t874517518  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t874517518  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t937156555  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t1533070037  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t1533070037  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t1533070037  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t1533070037  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_XmlTagStack_1_t1818389866  ___colorGradientStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t2735062451  ___sizeStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t2735062451  ___indentStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2730429967  ___fontWeightStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2730429967  ___styleStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t2735062451  ___baselineStack_43;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2730429967  ___actionStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t3512906015  ___materialReferenceStack_45;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t2125340843  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t2530419979 * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t2641813093 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t193706927 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3018556803  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___textInfo_27)); }
	inline TMP_TextInfo_t2849466151 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t2849466151 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lineInfo_28)); }
	inline TMP_LineInfo_t2320418126  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t2320418126 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t2320418126  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___vertexColor_29)); }
	inline Color32_t874517518  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t874517518 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t874517518  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___underlineColor_30)); }
	inline Color32_t874517518  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t874517518 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t874517518  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___strikethroughColor_31)); }
	inline Color32_t874517518  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t874517518 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t874517518  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___highlightColor_32)); }
	inline Color32_t874517518  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t874517518 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t874517518  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t937156555  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t937156555 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t937156555  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t1533070037  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t1533070037  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t1533070037  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t1533070037  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t1533070037  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t1533070037  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t1533070037  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t1533070037  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___colorGradientStack_38)); }
	inline TMP_XmlTagStack_1_t1818389866  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_XmlTagStack_1_t1818389866 * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_XmlTagStack_1_t1818389866  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___sizeStack_39)); }
	inline TMP_XmlTagStack_1_t2735062451  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_XmlTagStack_1_t2735062451  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___indentStack_40)); }
	inline TMP_XmlTagStack_1_t2735062451  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_XmlTagStack_1_t2735062451  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___fontWeightStack_41)); }
	inline TMP_XmlTagStack_1_t2730429967  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_XmlTagStack_1_t2730429967  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___styleStack_42)); }
	inline TMP_XmlTagStack_1_t2730429967  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_XmlTagStack_1_t2730429967  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___baselineStack_43)); }
	inline TMP_XmlTagStack_1_t2735062451  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_XmlTagStack_1_t2735062451  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___actionStack_44)); }
	inline TMP_XmlTagStack_1_t2730429967  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_XmlTagStack_1_t2730429967  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___materialReferenceStack_45)); }
	inline TMP_XmlTagStack_1_t3512906015  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_XmlTagStack_1_t3512906015 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_XmlTagStack_1_t3512906015  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___lineJustificationStack_46)); }
	inline TMP_XmlTagStack_1_t2125340843  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_XmlTagStack_1_t2125340843 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_XmlTagStack_1_t2125340843  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t2530419979 * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t2530419979 * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_t2641813093 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_t2641813093 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentMaterial_50)); }
	inline Material_t193706927 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_t193706927 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_t193706927 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___meshExtents_52)); }
	inline Extents_t3018556803  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_t3018556803 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_t3018556803  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t433984875, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t433984875_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t2849466151 * ___textInfo_27;
	TMP_LineInfo_t2320418126  ___lineInfo_28;
	Color32_t874517518  ___vertexColor_29;
	Color32_t874517518  ___underlineColor_30;
	Color32_t874517518  ___strikethroughColor_31;
	Color32_t874517518  ___highlightColor_32;
	TMP_BasicXmlTagStack_t937156555  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t1533070037  ___colorStack_34;
	TMP_XmlTagStack_1_t1533070037  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t1533070037  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t1533070037  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t1818389866  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t2735062451  ___sizeStack_39;
	TMP_XmlTagStack_1_t2735062451  ___indentStack_40;
	TMP_XmlTagStack_1_t2730429967  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2730429967  ___styleStack_42;
	TMP_XmlTagStack_1_t2735062451  ___baselineStack_43;
	TMP_XmlTagStack_1_t2730429967  ___actionStack_44;
	TMP_XmlTagStack_1_t3512906015  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t2125340843  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t2530419979 * ___currentFontAsset_48;
	TMP_SpriteAsset_t2641813093 * ___currentSpriteAsset_49;
	Material_t193706927 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3018556803  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t433984875_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t2849466151 * ___textInfo_27;
	TMP_LineInfo_t2320418126  ___lineInfo_28;
	Color32_t874517518  ___vertexColor_29;
	Color32_t874517518  ___underlineColor_30;
	Color32_t874517518  ___strikethroughColor_31;
	Color32_t874517518  ___highlightColor_32;
	TMP_BasicXmlTagStack_t937156555  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t1533070037  ___colorStack_34;
	TMP_XmlTagStack_1_t1533070037  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t1533070037  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t1533070037  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t1818389866  ___colorGradientStack_38;
	TMP_XmlTagStack_1_t2735062451  ___sizeStack_39;
	TMP_XmlTagStack_1_t2735062451  ___indentStack_40;
	TMP_XmlTagStack_1_t2730429967  ___fontWeightStack_41;
	TMP_XmlTagStack_1_t2730429967  ___styleStack_42;
	TMP_XmlTagStack_1_t2735062451  ___baselineStack_43;
	TMP_XmlTagStack_1_t2730429967  ___actionStack_44;
	TMP_XmlTagStack_1_t3512906015  ___materialReferenceStack_45;
	TMP_XmlTagStack_1_t2125340843  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t2530419979 * ___currentFontAsset_48;
	TMP_SpriteAsset_t2641813093 * ___currentSpriteAsset_49;
	Material_t193706927 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_t3018556803  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T433984875_H
#ifndef U3CDOSPRITEANIMATIONINTERNALU3EC__ITERATOR0_T12671535_H
#define U3CDOSPRITEANIMATIONINTERNALU3EC__ITERATOR0_T12671535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0
struct  U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::start
	int32_t ___start_0;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<currentFrame>__0
	int32_t ___U3CcurrentFrameU3E__0_1;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::end
	int32_t ___end_2;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::spriteAsset
	TMP_SpriteAsset_t2641813093 * ___spriteAsset_3;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::currentCharacter
	int32_t ___currentCharacter_4;
	// TMPro.TMP_CharacterInfo TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<charInfo>__0
	TMP_CharacterInfo_t1421302791  ___U3CcharInfoU3E__0_5;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<materialIndex>__0
	int32_t ___U3CmaterialIndexU3E__0_6;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<vertexIndex>__0
	int32_t ___U3CvertexIndexU3E__0_7;
	// TMPro.TMP_MeshInfo TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<meshInfo>__0
	TMP_MeshInfo_t1297308317  ___U3CmeshInfoU3E__0_8;
	// System.Single TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_9;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::framerate
	int32_t ___framerate_10;
	// System.Single TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::<targetTime>__0
	float ___U3CtargetTimeU3E__0_11;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::$this
	TMP_SpriteAnimator_t2347923044 * ___U24this_12;
	// System.Object TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::$current
	RuntimeObject * ___U24current_13;
	// System.Boolean TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::$disposing
	bool ___U24disposing_14;
	// System.Int32 TMPro.TMP_SpriteAnimator/<DoSpriteAnimationInternal>c__Iterator0::$PC
	int32_t ___U24PC_15;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentFrameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U3CcurrentFrameU3E__0_1)); }
	inline int32_t get_U3CcurrentFrameU3E__0_1() const { return ___U3CcurrentFrameU3E__0_1; }
	inline int32_t* get_address_of_U3CcurrentFrameU3E__0_1() { return &___U3CcurrentFrameU3E__0_1; }
	inline void set_U3CcurrentFrameU3E__0_1(int32_t value)
	{
		___U3CcurrentFrameU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_end_2() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___end_2)); }
	inline int32_t get_end_2() const { return ___end_2; }
	inline int32_t* get_address_of_end_2() { return &___end_2; }
	inline void set_end_2(int32_t value)
	{
		___end_2 = value;
	}

	inline static int32_t get_offset_of_spriteAsset_3() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___spriteAsset_3)); }
	inline TMP_SpriteAsset_t2641813093 * get_spriteAsset_3() const { return ___spriteAsset_3; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_spriteAsset_3() { return &___spriteAsset_3; }
	inline void set_spriteAsset_3(TMP_SpriteAsset_t2641813093 * value)
	{
		___spriteAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_3), value);
	}

	inline static int32_t get_offset_of_currentCharacter_4() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___currentCharacter_4)); }
	inline int32_t get_currentCharacter_4() const { return ___currentCharacter_4; }
	inline int32_t* get_address_of_currentCharacter_4() { return &___currentCharacter_4; }
	inline void set_currentCharacter_4(int32_t value)
	{
		___currentCharacter_4 = value;
	}

	inline static int32_t get_offset_of_U3CcharInfoU3E__0_5() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U3CcharInfoU3E__0_5)); }
	inline TMP_CharacterInfo_t1421302791  get_U3CcharInfoU3E__0_5() const { return ___U3CcharInfoU3E__0_5; }
	inline TMP_CharacterInfo_t1421302791 * get_address_of_U3CcharInfoU3E__0_5() { return &___U3CcharInfoU3E__0_5; }
	inline void set_U3CcharInfoU3E__0_5(TMP_CharacterInfo_t1421302791  value)
	{
		___U3CcharInfoU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CmaterialIndexU3E__0_6() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U3CmaterialIndexU3E__0_6)); }
	inline int32_t get_U3CmaterialIndexU3E__0_6() const { return ___U3CmaterialIndexU3E__0_6; }
	inline int32_t* get_address_of_U3CmaterialIndexU3E__0_6() { return &___U3CmaterialIndexU3E__0_6; }
	inline void set_U3CmaterialIndexU3E__0_6(int32_t value)
	{
		___U3CmaterialIndexU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CvertexIndexU3E__0_7() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U3CvertexIndexU3E__0_7)); }
	inline int32_t get_U3CvertexIndexU3E__0_7() const { return ___U3CvertexIndexU3E__0_7; }
	inline int32_t* get_address_of_U3CvertexIndexU3E__0_7() { return &___U3CvertexIndexU3E__0_7; }
	inline void set_U3CvertexIndexU3E__0_7(int32_t value)
	{
		___U3CvertexIndexU3E__0_7 = value;
	}

	inline static int32_t get_offset_of_U3CmeshInfoU3E__0_8() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U3CmeshInfoU3E__0_8)); }
	inline TMP_MeshInfo_t1297308317  get_U3CmeshInfoU3E__0_8() const { return ___U3CmeshInfoU3E__0_8; }
	inline TMP_MeshInfo_t1297308317 * get_address_of_U3CmeshInfoU3E__0_8() { return &___U3CmeshInfoU3E__0_8; }
	inline void set_U3CmeshInfoU3E__0_8(TMP_MeshInfo_t1297308317  value)
	{
		___U3CmeshInfoU3E__0_8 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_9() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U3CelapsedTimeU3E__0_9)); }
	inline float get_U3CelapsedTimeU3E__0_9() const { return ___U3CelapsedTimeU3E__0_9; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_9() { return &___U3CelapsedTimeU3E__0_9; }
	inline void set_U3CelapsedTimeU3E__0_9(float value)
	{
		___U3CelapsedTimeU3E__0_9 = value;
	}

	inline static int32_t get_offset_of_framerate_10() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___framerate_10)); }
	inline int32_t get_framerate_10() const { return ___framerate_10; }
	inline int32_t* get_address_of_framerate_10() { return &___framerate_10; }
	inline void set_framerate_10(int32_t value)
	{
		___framerate_10 = value;
	}

	inline static int32_t get_offset_of_U3CtargetTimeU3E__0_11() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U3CtargetTimeU3E__0_11)); }
	inline float get_U3CtargetTimeU3E__0_11() const { return ___U3CtargetTimeU3E__0_11; }
	inline float* get_address_of_U3CtargetTimeU3E__0_11() { return &___U3CtargetTimeU3E__0_11; }
	inline void set_U3CtargetTimeU3E__0_11(float value)
	{
		___U3CtargetTimeU3E__0_11 = value;
	}

	inline static int32_t get_offset_of_U24this_12() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U24this_12)); }
	inline TMP_SpriteAnimator_t2347923044 * get_U24this_12() const { return ___U24this_12; }
	inline TMP_SpriteAnimator_t2347923044 ** get_address_of_U24this_12() { return &___U24this_12; }
	inline void set_U24this_12(TMP_SpriteAnimator_t2347923044 * value)
	{
		___U24this_12 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_12), value);
	}

	inline static int32_t get_offset_of_U24current_13() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U24current_13)); }
	inline RuntimeObject * get_U24current_13() const { return ___U24current_13; }
	inline RuntimeObject ** get_address_of_U24current_13() { return &___U24current_13; }
	inline void set_U24current_13(RuntimeObject * value)
	{
		___U24current_13 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_13), value);
	}

	inline static int32_t get_offset_of_U24disposing_14() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U24disposing_14)); }
	inline bool get_U24disposing_14() const { return ___U24disposing_14; }
	inline bool* get_address_of_U24disposing_14() { return &___U24disposing_14; }
	inline void set_U24disposing_14(bool value)
	{
		___U24disposing_14 = value;
	}

	inline static int32_t get_offset_of_U24PC_15() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535, ___U24PC_15)); }
	inline int32_t get_U24PC_15() const { return ___U24PC_15; }
	inline int32_t* get_address_of_U24PC_15() { return &___U24PC_15; }
	inline void set_U24PC_15(int32_t value)
	{
		___U24PC_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSPRITEANIMATIONINTERNALU3EC__ITERATOR0_T12671535_H
#ifndef TMP_STYLESHEET_T335925700_H
#define TMP_STYLESHEET_T335925700_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_StyleSheet
struct  TMP_StyleSheet_t335925700  : public ScriptableObject_t1975622470
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleList
	List_1_t3733825879 * ___m_StyleList_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleDictionary
	Dictionary_2_t3372530382 * ___m_StyleDictionary_4;

public:
	inline static int32_t get_offset_of_m_StyleList_3() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t335925700, ___m_StyleList_3)); }
	inline List_1_t3733825879 * get_m_StyleList_3() const { return ___m_StyleList_3; }
	inline List_1_t3733825879 ** get_address_of_m_StyleList_3() { return &___m_StyleList_3; }
	inline void set_m_StyleList_3(List_1_t3733825879 * value)
	{
		___m_StyleList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleList_3), value);
	}

	inline static int32_t get_offset_of_m_StyleDictionary_4() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t335925700, ___m_StyleDictionary_4)); }
	inline Dictionary_2_t3372530382 * get_m_StyleDictionary_4() const { return ___m_StyleDictionary_4; }
	inline Dictionary_2_t3372530382 ** get_address_of_m_StyleDictionary_4() { return &___m_StyleDictionary_4; }
	inline void set_m_StyleDictionary_4(Dictionary_2_t3372530382 * value)
	{
		___m_StyleDictionary_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleDictionary_4), value);
	}
};

struct TMP_StyleSheet_t335925700_StaticFields
{
public:
	// TMPro.TMP_StyleSheet TMPro.TMP_StyleSheet::s_Instance
	TMP_StyleSheet_t335925700 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_t335925700_StaticFields, ___s_Instance_2)); }
	inline TMP_StyleSheet_t335925700 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline TMP_StyleSheet_t335925700 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(TMP_StyleSheet_t335925700 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLESHEET_T335925700_H
#ifndef TMP_SETTINGS_T1236229477_H
#define TMP_SETTINGS_T1236229477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Settings
struct  TMP_Settings_t1236229477  : public ScriptableObject_t1975622470
{
public:
	// System.String TMPro.TMP_Settings::m_version
	String_t* ___m_version_3;
	// System.Boolean TMPro.TMP_Settings::m_enableWordWrapping
	bool ___m_enableWordWrapping_4;
	// System.Boolean TMPro.TMP_Settings::m_enableKerning
	bool ___m_enableKerning_5;
	// System.Boolean TMPro.TMP_Settings::m_enableExtraPadding
	bool ___m_enableExtraPadding_6;
	// System.Boolean TMPro.TMP_Settings::m_enableTintAllSprites
	bool ___m_enableTintAllSprites_7;
	// System.Boolean TMPro.TMP_Settings::m_enableParseEscapeCharacters
	bool ___m_enableParseEscapeCharacters_8;
	// System.Int32 TMPro.TMP_Settings::m_missingGlyphCharacter
	int32_t ___m_missingGlyphCharacter_9;
	// System.Boolean TMPro.TMP_Settings::m_warningsDisabled
	bool ___m_warningsDisabled_10;
	// TMPro.TMP_FontAsset TMPro.TMP_Settings::m_defaultFontAsset
	TMP_FontAsset_t2530419979 * ___m_defaultFontAsset_11;
	// System.String TMPro.TMP_Settings::m_defaultFontAssetPath
	String_t* ___m_defaultFontAssetPath_12;
	// System.Single TMPro.TMP_Settings::m_defaultFontSize
	float ___m_defaultFontSize_13;
	// System.Single TMPro.TMP_Settings::m_defaultAutoSizeMinRatio
	float ___m_defaultAutoSizeMinRatio_14;
	// System.Single TMPro.TMP_Settings::m_defaultAutoSizeMaxRatio
	float ___m_defaultAutoSizeMaxRatio_15;
	// UnityEngine.Vector2 TMPro.TMP_Settings::m_defaultTextMeshProTextContainerSize
	Vector2_t2243707579  ___m_defaultTextMeshProTextContainerSize_16;
	// UnityEngine.Vector2 TMPro.TMP_Settings::m_defaultTextMeshProUITextContainerSize
	Vector2_t2243707579  ___m_defaultTextMeshProUITextContainerSize_17;
	// System.Boolean TMPro.TMP_Settings::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_18;
	// System.Collections.Generic.List`1<TMPro.TMP_FontAsset> TMPro.TMP_Settings::m_fallbackFontAssets
	List_1_t1899541111 * ___m_fallbackFontAssets_19;
	// System.Boolean TMPro.TMP_Settings::m_matchMaterialPreset
	bool ___m_matchMaterialPreset_20;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Settings::m_defaultSpriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_defaultSpriteAsset_21;
	// System.String TMPro.TMP_Settings::m_defaultSpriteAssetPath
	String_t* ___m_defaultSpriteAssetPath_22;
	// System.String TMPro.TMP_Settings::m_defaultColorGradientPresetsPath
	String_t* ___m_defaultColorGradientPresetsPath_23;
	// System.Boolean TMPro.TMP_Settings::m_enableEmojiSupport
	bool ___m_enableEmojiSupport_24;
	// TMPro.TMP_StyleSheet TMPro.TMP_Settings::m_defaultStyleSheet
	TMP_StyleSheet_t335925700 * ___m_defaultStyleSheet_25;
	// UnityEngine.TextAsset TMPro.TMP_Settings::m_leadingCharacters
	TextAsset_t3973159845 * ___m_leadingCharacters_26;
	// UnityEngine.TextAsset TMPro.TMP_Settings::m_followingCharacters
	TextAsset_t3973159845 * ___m_followingCharacters_27;
	// TMPro.TMP_Settings/LineBreakingTable TMPro.TMP_Settings::m_linebreakingRules
	LineBreakingTable_t2043799287 * ___m_linebreakingRules_28;

public:
	inline static int32_t get_offset_of_m_version_3() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_version_3)); }
	inline String_t* get_m_version_3() const { return ___m_version_3; }
	inline String_t** get_address_of_m_version_3() { return &___m_version_3; }
	inline void set_m_version_3(String_t* value)
	{
		___m_version_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_version_3), value);
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_4() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_enableWordWrapping_4)); }
	inline bool get_m_enableWordWrapping_4() const { return ___m_enableWordWrapping_4; }
	inline bool* get_address_of_m_enableWordWrapping_4() { return &___m_enableWordWrapping_4; }
	inline void set_m_enableWordWrapping_4(bool value)
	{
		___m_enableWordWrapping_4 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_5() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_enableKerning_5)); }
	inline bool get_m_enableKerning_5() const { return ___m_enableKerning_5; }
	inline bool* get_address_of_m_enableKerning_5() { return &___m_enableKerning_5; }
	inline void set_m_enableKerning_5(bool value)
	{
		___m_enableKerning_5 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_6() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_enableExtraPadding_6)); }
	inline bool get_m_enableExtraPadding_6() const { return ___m_enableExtraPadding_6; }
	inline bool* get_address_of_m_enableExtraPadding_6() { return &___m_enableExtraPadding_6; }
	inline void set_m_enableExtraPadding_6(bool value)
	{
		___m_enableExtraPadding_6 = value;
	}

	inline static int32_t get_offset_of_m_enableTintAllSprites_7() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_enableTintAllSprites_7)); }
	inline bool get_m_enableTintAllSprites_7() const { return ___m_enableTintAllSprites_7; }
	inline bool* get_address_of_m_enableTintAllSprites_7() { return &___m_enableTintAllSprites_7; }
	inline void set_m_enableTintAllSprites_7(bool value)
	{
		___m_enableTintAllSprites_7 = value;
	}

	inline static int32_t get_offset_of_m_enableParseEscapeCharacters_8() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_enableParseEscapeCharacters_8)); }
	inline bool get_m_enableParseEscapeCharacters_8() const { return ___m_enableParseEscapeCharacters_8; }
	inline bool* get_address_of_m_enableParseEscapeCharacters_8() { return &___m_enableParseEscapeCharacters_8; }
	inline void set_m_enableParseEscapeCharacters_8(bool value)
	{
		___m_enableParseEscapeCharacters_8 = value;
	}

	inline static int32_t get_offset_of_m_missingGlyphCharacter_9() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_missingGlyphCharacter_9)); }
	inline int32_t get_m_missingGlyphCharacter_9() const { return ___m_missingGlyphCharacter_9; }
	inline int32_t* get_address_of_m_missingGlyphCharacter_9() { return &___m_missingGlyphCharacter_9; }
	inline void set_m_missingGlyphCharacter_9(int32_t value)
	{
		___m_missingGlyphCharacter_9 = value;
	}

	inline static int32_t get_offset_of_m_warningsDisabled_10() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_warningsDisabled_10)); }
	inline bool get_m_warningsDisabled_10() const { return ___m_warningsDisabled_10; }
	inline bool* get_address_of_m_warningsDisabled_10() { return &___m_warningsDisabled_10; }
	inline void set_m_warningsDisabled_10(bool value)
	{
		___m_warningsDisabled_10 = value;
	}

	inline static int32_t get_offset_of_m_defaultFontAsset_11() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultFontAsset_11)); }
	inline TMP_FontAsset_t2530419979 * get_m_defaultFontAsset_11() const { return ___m_defaultFontAsset_11; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_m_defaultFontAsset_11() { return &___m_defaultFontAsset_11; }
	inline void set_m_defaultFontAsset_11(TMP_FontAsset_t2530419979 * value)
	{
		___m_defaultFontAsset_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultFontAsset_11), value);
	}

	inline static int32_t get_offset_of_m_defaultFontAssetPath_12() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultFontAssetPath_12)); }
	inline String_t* get_m_defaultFontAssetPath_12() const { return ___m_defaultFontAssetPath_12; }
	inline String_t** get_address_of_m_defaultFontAssetPath_12() { return &___m_defaultFontAssetPath_12; }
	inline void set_m_defaultFontAssetPath_12(String_t* value)
	{
		___m_defaultFontAssetPath_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultFontAssetPath_12), value);
	}

	inline static int32_t get_offset_of_m_defaultFontSize_13() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultFontSize_13)); }
	inline float get_m_defaultFontSize_13() const { return ___m_defaultFontSize_13; }
	inline float* get_address_of_m_defaultFontSize_13() { return &___m_defaultFontSize_13; }
	inline void set_m_defaultFontSize_13(float value)
	{
		___m_defaultFontSize_13 = value;
	}

	inline static int32_t get_offset_of_m_defaultAutoSizeMinRatio_14() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultAutoSizeMinRatio_14)); }
	inline float get_m_defaultAutoSizeMinRatio_14() const { return ___m_defaultAutoSizeMinRatio_14; }
	inline float* get_address_of_m_defaultAutoSizeMinRatio_14() { return &___m_defaultAutoSizeMinRatio_14; }
	inline void set_m_defaultAutoSizeMinRatio_14(float value)
	{
		___m_defaultAutoSizeMinRatio_14 = value;
	}

	inline static int32_t get_offset_of_m_defaultAutoSizeMaxRatio_15() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultAutoSizeMaxRatio_15)); }
	inline float get_m_defaultAutoSizeMaxRatio_15() const { return ___m_defaultAutoSizeMaxRatio_15; }
	inline float* get_address_of_m_defaultAutoSizeMaxRatio_15() { return &___m_defaultAutoSizeMaxRatio_15; }
	inline void set_m_defaultAutoSizeMaxRatio_15(float value)
	{
		___m_defaultAutoSizeMaxRatio_15 = value;
	}

	inline static int32_t get_offset_of_m_defaultTextMeshProTextContainerSize_16() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultTextMeshProTextContainerSize_16)); }
	inline Vector2_t2243707579  get_m_defaultTextMeshProTextContainerSize_16() const { return ___m_defaultTextMeshProTextContainerSize_16; }
	inline Vector2_t2243707579 * get_address_of_m_defaultTextMeshProTextContainerSize_16() { return &___m_defaultTextMeshProTextContainerSize_16; }
	inline void set_m_defaultTextMeshProTextContainerSize_16(Vector2_t2243707579  value)
	{
		___m_defaultTextMeshProTextContainerSize_16 = value;
	}

	inline static int32_t get_offset_of_m_defaultTextMeshProUITextContainerSize_17() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultTextMeshProUITextContainerSize_17)); }
	inline Vector2_t2243707579  get_m_defaultTextMeshProUITextContainerSize_17() const { return ___m_defaultTextMeshProUITextContainerSize_17; }
	inline Vector2_t2243707579 * get_address_of_m_defaultTextMeshProUITextContainerSize_17() { return &___m_defaultTextMeshProUITextContainerSize_17; }
	inline void set_m_defaultTextMeshProUITextContainerSize_17(Vector2_t2243707579  value)
	{
		___m_defaultTextMeshProUITextContainerSize_17 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_18() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_autoSizeTextContainer_18)); }
	inline bool get_m_autoSizeTextContainer_18() const { return ___m_autoSizeTextContainer_18; }
	inline bool* get_address_of_m_autoSizeTextContainer_18() { return &___m_autoSizeTextContainer_18; }
	inline void set_m_autoSizeTextContainer_18(bool value)
	{
		___m_autoSizeTextContainer_18 = value;
	}

	inline static int32_t get_offset_of_m_fallbackFontAssets_19() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_fallbackFontAssets_19)); }
	inline List_1_t1899541111 * get_m_fallbackFontAssets_19() const { return ___m_fallbackFontAssets_19; }
	inline List_1_t1899541111 ** get_address_of_m_fallbackFontAssets_19() { return &___m_fallbackFontAssets_19; }
	inline void set_m_fallbackFontAssets_19(List_1_t1899541111 * value)
	{
		___m_fallbackFontAssets_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackFontAssets_19), value);
	}

	inline static int32_t get_offset_of_m_matchMaterialPreset_20() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_matchMaterialPreset_20)); }
	inline bool get_m_matchMaterialPreset_20() const { return ___m_matchMaterialPreset_20; }
	inline bool* get_address_of_m_matchMaterialPreset_20() { return &___m_matchMaterialPreset_20; }
	inline void set_m_matchMaterialPreset_20(bool value)
	{
		___m_matchMaterialPreset_20 = value;
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_21() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultSpriteAsset_21)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_defaultSpriteAsset_21() const { return ___m_defaultSpriteAsset_21; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_defaultSpriteAsset_21() { return &___m_defaultSpriteAsset_21; }
	inline void set_m_defaultSpriteAsset_21(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_defaultSpriteAsset_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_21), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAssetPath_22() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultSpriteAssetPath_22)); }
	inline String_t* get_m_defaultSpriteAssetPath_22() const { return ___m_defaultSpriteAssetPath_22; }
	inline String_t** get_address_of_m_defaultSpriteAssetPath_22() { return &___m_defaultSpriteAssetPath_22; }
	inline void set_m_defaultSpriteAssetPath_22(String_t* value)
	{
		___m_defaultSpriteAssetPath_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAssetPath_22), value);
	}

	inline static int32_t get_offset_of_m_defaultColorGradientPresetsPath_23() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultColorGradientPresetsPath_23)); }
	inline String_t* get_m_defaultColorGradientPresetsPath_23() const { return ___m_defaultColorGradientPresetsPath_23; }
	inline String_t** get_address_of_m_defaultColorGradientPresetsPath_23() { return &___m_defaultColorGradientPresetsPath_23; }
	inline void set_m_defaultColorGradientPresetsPath_23(String_t* value)
	{
		___m_defaultColorGradientPresetsPath_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultColorGradientPresetsPath_23), value);
	}

	inline static int32_t get_offset_of_m_enableEmojiSupport_24() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_enableEmojiSupport_24)); }
	inline bool get_m_enableEmojiSupport_24() const { return ___m_enableEmojiSupport_24; }
	inline bool* get_address_of_m_enableEmojiSupport_24() { return &___m_enableEmojiSupport_24; }
	inline void set_m_enableEmojiSupport_24(bool value)
	{
		___m_enableEmojiSupport_24 = value;
	}

	inline static int32_t get_offset_of_m_defaultStyleSheet_25() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_defaultStyleSheet_25)); }
	inline TMP_StyleSheet_t335925700 * get_m_defaultStyleSheet_25() const { return ___m_defaultStyleSheet_25; }
	inline TMP_StyleSheet_t335925700 ** get_address_of_m_defaultStyleSheet_25() { return &___m_defaultStyleSheet_25; }
	inline void set_m_defaultStyleSheet_25(TMP_StyleSheet_t335925700 * value)
	{
		___m_defaultStyleSheet_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultStyleSheet_25), value);
	}

	inline static int32_t get_offset_of_m_leadingCharacters_26() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_leadingCharacters_26)); }
	inline TextAsset_t3973159845 * get_m_leadingCharacters_26() const { return ___m_leadingCharacters_26; }
	inline TextAsset_t3973159845 ** get_address_of_m_leadingCharacters_26() { return &___m_leadingCharacters_26; }
	inline void set_m_leadingCharacters_26(TextAsset_t3973159845 * value)
	{
		___m_leadingCharacters_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_leadingCharacters_26), value);
	}

	inline static int32_t get_offset_of_m_followingCharacters_27() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_followingCharacters_27)); }
	inline TextAsset_t3973159845 * get_m_followingCharacters_27() const { return ___m_followingCharacters_27; }
	inline TextAsset_t3973159845 ** get_address_of_m_followingCharacters_27() { return &___m_followingCharacters_27; }
	inline void set_m_followingCharacters_27(TextAsset_t3973159845 * value)
	{
		___m_followingCharacters_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_followingCharacters_27), value);
	}

	inline static int32_t get_offset_of_m_linebreakingRules_28() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477, ___m_linebreakingRules_28)); }
	inline LineBreakingTable_t2043799287 * get_m_linebreakingRules_28() const { return ___m_linebreakingRules_28; }
	inline LineBreakingTable_t2043799287 ** get_address_of_m_linebreakingRules_28() { return &___m_linebreakingRules_28; }
	inline void set_m_linebreakingRules_28(LineBreakingTable_t2043799287 * value)
	{
		___m_linebreakingRules_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_linebreakingRules_28), value);
	}
};

struct TMP_Settings_t1236229477_StaticFields
{
public:
	// TMPro.TMP_Settings TMPro.TMP_Settings::s_Instance
	TMP_Settings_t1236229477 * ___s_Instance_2;

public:
	inline static int32_t get_offset_of_s_Instance_2() { return static_cast<int32_t>(offsetof(TMP_Settings_t1236229477_StaticFields, ___s_Instance_2)); }
	inline TMP_Settings_t1236229477 * get_s_Instance_2() const { return ___s_Instance_2; }
	inline TMP_Settings_t1236229477 ** get_address_of_s_Instance_2() { return &___s_Instance_2; }
	inline void set_s_Instance_2(TMP_Settings_t1236229477 * value)
	{
		___s_Instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SETTINGS_T1236229477_H
#ifndef TMP_INPUTVALIDATOR_T3726817866_H
#define TMP_INPUTVALIDATOR_T3726817866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputValidator
struct  TMP_InputValidator_t3726817866  : public ScriptableObject_t1975622470
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTVALIDATOR_T3726817866_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ONVALIDATEINPUT_T3285190392_H
#define ONVALIDATEINPUT_T3285190392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/OnValidateInput
struct  OnValidateInput_t3285190392  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONVALIDATEINPUT_T3285190392_H
#ifndef TMP_FONTASSET_T2530419979_H
#define TMP_FONTASSET_T2530419979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAsset
struct  TMP_FontAsset_t2530419979  : public TMP_Asset_t1084708044
{
public:
	// TMPro.TMP_FontAsset/FontAssetTypes TMPro.TMP_FontAsset::fontAssetType
	int32_t ___fontAssetType_6;
	// TMPro.FaceInfo TMPro.TMP_FontAsset::m_fontInfo
	FaceInfo_t3239700425 * ___m_fontInfo_7;
	// UnityEngine.Texture2D TMPro.TMP_FontAsset::atlas
	Texture2D_t3542995729 * ___atlas_8;
	// System.Collections.Generic.List`1<TMPro.TMP_Glyph> TMPro.TMP_FontAsset::m_glyphInfoList
	List_1_t278915034 * ___m_glyphInfoList_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Glyph> TMPro.TMP_FontAsset::m_characterDictionary
	Dictionary_2_t4212586833 * ___m_characterDictionary_10;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.KerningPair> TMPro.TMP_FontAsset::m_kerningDictionary
	Dictionary_2_t585579557 * ___m_kerningDictionary_11;
	// TMPro.KerningTable TMPro.TMP_FontAsset::m_kerningInfo
	KerningTable_t2970824110 * ___m_kerningInfo_12;
	// TMPro.KerningPair TMPro.TMP_FontAsset::m_kerningPair
	KerningPair_t1577753922 * ___m_kerningPair_13;
	// System.Collections.Generic.List`1<TMPro.TMP_FontAsset> TMPro.TMP_FontAsset::fallbackFontAssets
	List_1_t1899541111 * ___fallbackFontAssets_14;
	// TMPro.FontCreationSetting TMPro.TMP_FontAsset::fontCreationSettings
	FontCreationSetting_t1093397046  ___fontCreationSettings_15;
	// TMPro.TMP_FontWeights[] TMPro.TMP_FontAsset::fontWeights
	TMP_FontWeightsU5BU5D_t3725552155* ___fontWeights_16;
	// System.Int32[] TMPro.TMP_FontAsset::m_characterSet
	Int32U5BU5D_t3030399641* ___m_characterSet_17;
	// System.Single TMPro.TMP_FontAsset::normalStyle
	float ___normalStyle_18;
	// System.Single TMPro.TMP_FontAsset::normalSpacingOffset
	float ___normalSpacingOffset_19;
	// System.Single TMPro.TMP_FontAsset::boldStyle
	float ___boldStyle_20;
	// System.Single TMPro.TMP_FontAsset::boldSpacing
	float ___boldSpacing_21;
	// System.Byte TMPro.TMP_FontAsset::italicStyle
	uint8_t ___italicStyle_22;
	// System.Byte TMPro.TMP_FontAsset::tabSize
	uint8_t ___tabSize_23;
	// System.Byte TMPro.TMP_FontAsset::m_oldTabSize
	uint8_t ___m_oldTabSize_24;

public:
	inline static int32_t get_offset_of_fontAssetType_6() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___fontAssetType_6)); }
	inline int32_t get_fontAssetType_6() const { return ___fontAssetType_6; }
	inline int32_t* get_address_of_fontAssetType_6() { return &___fontAssetType_6; }
	inline void set_fontAssetType_6(int32_t value)
	{
		___fontAssetType_6 = value;
	}

	inline static int32_t get_offset_of_m_fontInfo_7() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___m_fontInfo_7)); }
	inline FaceInfo_t3239700425 * get_m_fontInfo_7() const { return ___m_fontInfo_7; }
	inline FaceInfo_t3239700425 ** get_address_of_m_fontInfo_7() { return &___m_fontInfo_7; }
	inline void set_m_fontInfo_7(FaceInfo_t3239700425 * value)
	{
		___m_fontInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontInfo_7), value);
	}

	inline static int32_t get_offset_of_atlas_8() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___atlas_8)); }
	inline Texture2D_t3542995729 * get_atlas_8() const { return ___atlas_8; }
	inline Texture2D_t3542995729 ** get_address_of_atlas_8() { return &___atlas_8; }
	inline void set_atlas_8(Texture2D_t3542995729 * value)
	{
		___atlas_8 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_8), value);
	}

	inline static int32_t get_offset_of_m_glyphInfoList_9() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___m_glyphInfoList_9)); }
	inline List_1_t278915034 * get_m_glyphInfoList_9() const { return ___m_glyphInfoList_9; }
	inline List_1_t278915034 ** get_address_of_m_glyphInfoList_9() { return &___m_glyphInfoList_9; }
	inline void set_m_glyphInfoList_9(List_1_t278915034 * value)
	{
		___m_glyphInfoList_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_glyphInfoList_9), value);
	}

	inline static int32_t get_offset_of_m_characterDictionary_10() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___m_characterDictionary_10)); }
	inline Dictionary_2_t4212586833 * get_m_characterDictionary_10() const { return ___m_characterDictionary_10; }
	inline Dictionary_2_t4212586833 ** get_address_of_m_characterDictionary_10() { return &___m_characterDictionary_10; }
	inline void set_m_characterDictionary_10(Dictionary_2_t4212586833 * value)
	{
		___m_characterDictionary_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_characterDictionary_10), value);
	}

	inline static int32_t get_offset_of_m_kerningDictionary_11() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___m_kerningDictionary_11)); }
	inline Dictionary_2_t585579557 * get_m_kerningDictionary_11() const { return ___m_kerningDictionary_11; }
	inline Dictionary_2_t585579557 ** get_address_of_m_kerningDictionary_11() { return &___m_kerningDictionary_11; }
	inline void set_m_kerningDictionary_11(Dictionary_2_t585579557 * value)
	{
		___m_kerningDictionary_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_kerningDictionary_11), value);
	}

	inline static int32_t get_offset_of_m_kerningInfo_12() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___m_kerningInfo_12)); }
	inline KerningTable_t2970824110 * get_m_kerningInfo_12() const { return ___m_kerningInfo_12; }
	inline KerningTable_t2970824110 ** get_address_of_m_kerningInfo_12() { return &___m_kerningInfo_12; }
	inline void set_m_kerningInfo_12(KerningTable_t2970824110 * value)
	{
		___m_kerningInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_kerningInfo_12), value);
	}

	inline static int32_t get_offset_of_m_kerningPair_13() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___m_kerningPair_13)); }
	inline KerningPair_t1577753922 * get_m_kerningPair_13() const { return ___m_kerningPair_13; }
	inline KerningPair_t1577753922 ** get_address_of_m_kerningPair_13() { return &___m_kerningPair_13; }
	inline void set_m_kerningPair_13(KerningPair_t1577753922 * value)
	{
		___m_kerningPair_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_kerningPair_13), value);
	}

	inline static int32_t get_offset_of_fallbackFontAssets_14() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___fallbackFontAssets_14)); }
	inline List_1_t1899541111 * get_fallbackFontAssets_14() const { return ___fallbackFontAssets_14; }
	inline List_1_t1899541111 ** get_address_of_fallbackFontAssets_14() { return &___fallbackFontAssets_14; }
	inline void set_fallbackFontAssets_14(List_1_t1899541111 * value)
	{
		___fallbackFontAssets_14 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackFontAssets_14), value);
	}

	inline static int32_t get_offset_of_fontCreationSettings_15() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___fontCreationSettings_15)); }
	inline FontCreationSetting_t1093397046  get_fontCreationSettings_15() const { return ___fontCreationSettings_15; }
	inline FontCreationSetting_t1093397046 * get_address_of_fontCreationSettings_15() { return &___fontCreationSettings_15; }
	inline void set_fontCreationSettings_15(FontCreationSetting_t1093397046  value)
	{
		___fontCreationSettings_15 = value;
	}

	inline static int32_t get_offset_of_fontWeights_16() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___fontWeights_16)); }
	inline TMP_FontWeightsU5BU5D_t3725552155* get_fontWeights_16() const { return ___fontWeights_16; }
	inline TMP_FontWeightsU5BU5D_t3725552155** get_address_of_fontWeights_16() { return &___fontWeights_16; }
	inline void set_fontWeights_16(TMP_FontWeightsU5BU5D_t3725552155* value)
	{
		___fontWeights_16 = value;
		Il2CppCodeGenWriteBarrier((&___fontWeights_16), value);
	}

	inline static int32_t get_offset_of_m_characterSet_17() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___m_characterSet_17)); }
	inline Int32U5BU5D_t3030399641* get_m_characterSet_17() const { return ___m_characterSet_17; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_characterSet_17() { return &___m_characterSet_17; }
	inline void set_m_characterSet_17(Int32U5BU5D_t3030399641* value)
	{
		___m_characterSet_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_characterSet_17), value);
	}

	inline static int32_t get_offset_of_normalStyle_18() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___normalStyle_18)); }
	inline float get_normalStyle_18() const { return ___normalStyle_18; }
	inline float* get_address_of_normalStyle_18() { return &___normalStyle_18; }
	inline void set_normalStyle_18(float value)
	{
		___normalStyle_18 = value;
	}

	inline static int32_t get_offset_of_normalSpacingOffset_19() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___normalSpacingOffset_19)); }
	inline float get_normalSpacingOffset_19() const { return ___normalSpacingOffset_19; }
	inline float* get_address_of_normalSpacingOffset_19() { return &___normalSpacingOffset_19; }
	inline void set_normalSpacingOffset_19(float value)
	{
		___normalSpacingOffset_19 = value;
	}

	inline static int32_t get_offset_of_boldStyle_20() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___boldStyle_20)); }
	inline float get_boldStyle_20() const { return ___boldStyle_20; }
	inline float* get_address_of_boldStyle_20() { return &___boldStyle_20; }
	inline void set_boldStyle_20(float value)
	{
		___boldStyle_20 = value;
	}

	inline static int32_t get_offset_of_boldSpacing_21() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___boldSpacing_21)); }
	inline float get_boldSpacing_21() const { return ___boldSpacing_21; }
	inline float* get_address_of_boldSpacing_21() { return &___boldSpacing_21; }
	inline void set_boldSpacing_21(float value)
	{
		___boldSpacing_21 = value;
	}

	inline static int32_t get_offset_of_italicStyle_22() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___italicStyle_22)); }
	inline uint8_t get_italicStyle_22() const { return ___italicStyle_22; }
	inline uint8_t* get_address_of_italicStyle_22() { return &___italicStyle_22; }
	inline void set_italicStyle_22(uint8_t value)
	{
		___italicStyle_22 = value;
	}

	inline static int32_t get_offset_of_tabSize_23() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___tabSize_23)); }
	inline uint8_t get_tabSize_23() const { return ___tabSize_23; }
	inline uint8_t* get_address_of_tabSize_23() { return &___tabSize_23; }
	inline void set_tabSize_23(uint8_t value)
	{
		___tabSize_23 = value;
	}

	inline static int32_t get_offset_of_m_oldTabSize_24() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979, ___m_oldTabSize_24)); }
	inline uint8_t get_m_oldTabSize_24() const { return ___m_oldTabSize_24; }
	inline uint8_t* get_address_of_m_oldTabSize_24() { return &___m_oldTabSize_24; }
	inline void set_m_oldTabSize_24(uint8_t value)
	{
		___m_oldTabSize_24 = value;
	}
};

struct TMP_FontAsset_t2530419979_StaticFields
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_FontAsset::s_defaultFontAsset
	TMP_FontAsset_t2530419979 * ___s_defaultFontAsset_5;
	// System.Func`2<TMPro.TMP_Glyph,System.Int32> TMPro.TMP_FontAsset::<>f__am$cache0
	Func_2_t1421371187 * ___U3CU3Ef__amU24cache0_25;
	// System.Func`2<TMPro.TMP_Glyph,System.Int32> TMPro.TMP_FontAsset::<>f__am$cache1
	Func_2_t1421371187 * ___U3CU3Ef__amU24cache1_26;

public:
	inline static int32_t get_offset_of_s_defaultFontAsset_5() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979_StaticFields, ___s_defaultFontAsset_5)); }
	inline TMP_FontAsset_t2530419979 * get_s_defaultFontAsset_5() const { return ___s_defaultFontAsset_5; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_s_defaultFontAsset_5() { return &___s_defaultFontAsset_5; }
	inline void set_s_defaultFontAsset_5(TMP_FontAsset_t2530419979 * value)
	{
		___s_defaultFontAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultFontAsset_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_25() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979_StaticFields, ___U3CU3Ef__amU24cache0_25)); }
	inline Func_2_t1421371187 * get_U3CU3Ef__amU24cache0_25() const { return ___U3CU3Ef__amU24cache0_25; }
	inline Func_2_t1421371187 ** get_address_of_U3CU3Ef__amU24cache0_25() { return &___U3CU3Ef__amU24cache0_25; }
	inline void set_U3CU3Ef__amU24cache0_25(Func_2_t1421371187 * value)
	{
		___U3CU3Ef__amU24cache0_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_26() { return static_cast<int32_t>(offsetof(TMP_FontAsset_t2530419979_StaticFields, ___U3CU3Ef__amU24cache1_26)); }
	inline Func_2_t1421371187 * get_U3CU3Ef__amU24cache1_26() const { return ___U3CU3Ef__amU24cache1_26; }
	inline Func_2_t1421371187 ** get_address_of_U3CU3Ef__amU24cache1_26() { return &___U3CU3Ef__amU24cache1_26; }
	inline void set_U3CU3Ef__amU24cache1_26(Func_2_t1421371187 * value)
	{
		___U3CU3Ef__amU24cache1_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTASSET_T2530419979_H
#ifndef TMP_SPRITEASSET_T2641813093_H
#define TMP_SPRITEASSET_T2641813093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAsset
struct  TMP_SpriteAsset_t2641813093  : public TMP_Asset_t1084708044
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_UnicodeLookup
	Dictionary_2_t1079703083 * ___m_UnicodeLookup_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_NameLookup
	Dictionary_2_t1079703083 * ___m_NameLookup_6;
	// UnityEngine.Texture TMPro.TMP_SpriteAsset::spriteSheet
	Texture_t2243626319 * ___spriteSheet_8;
	// System.Collections.Generic.List`1<TMPro.TMP_Sprite> TMPro.TMP_SpriteAsset::spriteInfoList
	List_1_t3768471933 * ___spriteInfoList_9;
	// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset> TMPro.TMP_SpriteAsset::fallbackSpriteAssets
	List_1_t2010934225 * ___fallbackSpriteAssets_10;

public:
	inline static int32_t get_offset_of_m_UnicodeLookup_5() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t2641813093, ___m_UnicodeLookup_5)); }
	inline Dictionary_2_t1079703083 * get_m_UnicodeLookup_5() const { return ___m_UnicodeLookup_5; }
	inline Dictionary_2_t1079703083 ** get_address_of_m_UnicodeLookup_5() { return &___m_UnicodeLookup_5; }
	inline void set_m_UnicodeLookup_5(Dictionary_2_t1079703083 * value)
	{
		___m_UnicodeLookup_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnicodeLookup_5), value);
	}

	inline static int32_t get_offset_of_m_NameLookup_6() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t2641813093, ___m_NameLookup_6)); }
	inline Dictionary_2_t1079703083 * get_m_NameLookup_6() const { return ___m_NameLookup_6; }
	inline Dictionary_2_t1079703083 ** get_address_of_m_NameLookup_6() { return &___m_NameLookup_6; }
	inline void set_m_NameLookup_6(Dictionary_2_t1079703083 * value)
	{
		___m_NameLookup_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_NameLookup_6), value);
	}

	inline static int32_t get_offset_of_spriteSheet_8() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t2641813093, ___spriteSheet_8)); }
	inline Texture_t2243626319 * get_spriteSheet_8() const { return ___spriteSheet_8; }
	inline Texture_t2243626319 ** get_address_of_spriteSheet_8() { return &___spriteSheet_8; }
	inline void set_spriteSheet_8(Texture_t2243626319 * value)
	{
		___spriteSheet_8 = value;
		Il2CppCodeGenWriteBarrier((&___spriteSheet_8), value);
	}

	inline static int32_t get_offset_of_spriteInfoList_9() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t2641813093, ___spriteInfoList_9)); }
	inline List_1_t3768471933 * get_spriteInfoList_9() const { return ___spriteInfoList_9; }
	inline List_1_t3768471933 ** get_address_of_spriteInfoList_9() { return &___spriteInfoList_9; }
	inline void set_spriteInfoList_9(List_1_t3768471933 * value)
	{
		___spriteInfoList_9 = value;
		Il2CppCodeGenWriteBarrier((&___spriteInfoList_9), value);
	}

	inline static int32_t get_offset_of_fallbackSpriteAssets_10() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t2641813093, ___fallbackSpriteAssets_10)); }
	inline List_1_t2010934225 * get_fallbackSpriteAssets_10() const { return ___fallbackSpriteAssets_10; }
	inline List_1_t2010934225 ** get_address_of_fallbackSpriteAssets_10() { return &___fallbackSpriteAssets_10; }
	inline void set_fallbackSpriteAssets_10(List_1_t2010934225 * value)
	{
		___fallbackSpriteAssets_10 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackSpriteAssets_10), value);
	}
};

struct TMP_SpriteAsset_t2641813093_StaticFields
{
public:
	// TMPro.TMP_SpriteAsset TMPro.TMP_SpriteAsset::m_defaultSpriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_defaultSpriteAsset_7;

public:
	inline static int32_t get_offset_of_m_defaultSpriteAsset_7() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t2641813093_StaticFields, ___m_defaultSpriteAsset_7)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_defaultSpriteAsset_7() const { return ___m_defaultSpriteAsset_7; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_defaultSpriteAsset_7() { return &___m_defaultSpriteAsset_7; }
	inline void set_m_defaultSpriteAsset_7(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_defaultSpriteAsset_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEASSET_T2641813093_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef DROPDOWNITEM_T1251916390_H
#define DROPDOWNITEM_T1251916390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown/DropdownItem
struct  DropdownItem_t1251916390  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_Text TMPro.TMP_Dropdown/DropdownItem::m_Text
	TMP_Text_t1920000777 * ___m_Text_2;
	// UnityEngine.UI.Image TMPro.TMP_Dropdown/DropdownItem::m_Image
	Image_t2042527209 * ___m_Image_3;
	// UnityEngine.RectTransform TMPro.TMP_Dropdown/DropdownItem::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_4;
	// UnityEngine.UI.Toggle TMPro.TMP_Dropdown/DropdownItem::m_Toggle
	Toggle_t3976754468 * ___m_Toggle_5;

public:
	inline static int32_t get_offset_of_m_Text_2() { return static_cast<int32_t>(offsetof(DropdownItem_t1251916390, ___m_Text_2)); }
	inline TMP_Text_t1920000777 * get_m_Text_2() const { return ___m_Text_2; }
	inline TMP_Text_t1920000777 ** get_address_of_m_Text_2() { return &___m_Text_2; }
	inline void set_m_Text_2(TMP_Text_t1920000777 * value)
	{
		___m_Text_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_2), value);
	}

	inline static int32_t get_offset_of_m_Image_3() { return static_cast<int32_t>(offsetof(DropdownItem_t1251916390, ___m_Image_3)); }
	inline Image_t2042527209 * get_m_Image_3() const { return ___m_Image_3; }
	inline Image_t2042527209 ** get_address_of_m_Image_3() { return &___m_Image_3; }
	inline void set_m_Image_3(Image_t2042527209 * value)
	{
		___m_Image_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_3), value);
	}

	inline static int32_t get_offset_of_m_RectTransform_4() { return static_cast<int32_t>(offsetof(DropdownItem_t1251916390, ___m_RectTransform_4)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_4() const { return ___m_RectTransform_4; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_4() { return &___m_RectTransform_4; }
	inline void set_m_RectTransform_4(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_4), value);
	}

	inline static int32_t get_offset_of_m_Toggle_5() { return static_cast<int32_t>(offsetof(DropdownItem_t1251916390, ___m_Toggle_5)); }
	inline Toggle_t3976754468 * get_m_Toggle_5() const { return ___m_Toggle_5; }
	inline Toggle_t3976754468 ** get_address_of_m_Toggle_5() { return &___m_Toggle_5; }
	inline void set_m_Toggle_5(Toggle_t3976754468 * value)
	{
		___m_Toggle_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Toggle_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPDOWNITEM_T1251916390_H
#ifndef TMP_SUBMESH_T3507543655_H
#define TMP_SUBMESH_T3507543655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMesh
struct  TMP_SubMesh_t3507543655  : public MonoBehaviour_t1158329972
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMesh::m_fontAsset
	TMP_FontAsset_t2530419979 * ___m_fontAsset_2;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMesh::m_spriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_spriteAsset_3;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_material
	Material_t193706927 * ___m_material_4;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_sharedMaterial
	Material_t193706927 * ___m_sharedMaterial_5;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackMaterial
	Material_t193706927 * ___m_fallbackMaterial_6;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackSourceMaterial
	Material_t193706927 * ___m_fallbackSourceMaterial_7;
	// System.Boolean TMPro.TMP_SubMesh::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_8;
	// System.Single TMPro.TMP_SubMesh::m_padding
	float ___m_padding_9;
	// UnityEngine.Renderer TMPro.TMP_SubMesh::m_renderer
	Renderer_t257310565 * ___m_renderer_10;
	// UnityEngine.MeshFilter TMPro.TMP_SubMesh::m_meshFilter
	MeshFilter_t3026937449 * ___m_meshFilter_11;
	// UnityEngine.Mesh TMPro.TMP_SubMesh::m_mesh
	Mesh_t1356156583 * ___m_mesh_12;
	// UnityEngine.BoxCollider TMPro.TMP_SubMesh::m_boxCollider
	BoxCollider_t22920061 * ___m_boxCollider_13;
	// TMPro.TextMeshPro TMPro.TMP_SubMesh::m_TextComponent
	TextMeshPro_t2521834357 * ___m_TextComponent_14;
	// System.Boolean TMPro.TMP_SubMesh::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_15;

public:
	inline static int32_t get_offset_of_m_fontAsset_2() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_fontAsset_2)); }
	inline TMP_FontAsset_t2530419979 * get_m_fontAsset_2() const { return ___m_fontAsset_2; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_m_fontAsset_2() { return &___m_fontAsset_2; }
	inline void set_m_fontAsset_2(TMP_FontAsset_t2530419979 * value)
	{
		___m_fontAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_2), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_3() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_spriteAsset_3)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_spriteAsset_3() const { return ___m_spriteAsset_3; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_spriteAsset_3() { return &___m_spriteAsset_3; }
	inline void set_m_spriteAsset_3(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_spriteAsset_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_3), value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_material_4)); }
	inline Material_t193706927 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t193706927 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t193706927 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_4), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_5() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_sharedMaterial_5)); }
	inline Material_t193706927 * get_m_sharedMaterial_5() const { return ___m_sharedMaterial_5; }
	inline Material_t193706927 ** get_address_of_m_sharedMaterial_5() { return &___m_sharedMaterial_5; }
	inline void set_m_sharedMaterial_5(Material_t193706927 * value)
	{
		___m_sharedMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_5), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_fallbackMaterial_6)); }
	inline Material_t193706927 * get_m_fallbackMaterial_6() const { return ___m_fallbackMaterial_6; }
	inline Material_t193706927 ** get_address_of_m_fallbackMaterial_6() { return &___m_fallbackMaterial_6; }
	inline void set_m_fallbackMaterial_6(Material_t193706927 * value)
	{
		___m_fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_7() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_fallbackSourceMaterial_7)); }
	inline Material_t193706927 * get_m_fallbackSourceMaterial_7() const { return ___m_fallbackSourceMaterial_7; }
	inline Material_t193706927 ** get_address_of_m_fallbackSourceMaterial_7() { return &___m_fallbackSourceMaterial_7; }
	inline void set_m_fallbackSourceMaterial_7(Material_t193706927 * value)
	{
		___m_fallbackSourceMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_7), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_8() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_isDefaultMaterial_8)); }
	inline bool get_m_isDefaultMaterial_8() const { return ___m_isDefaultMaterial_8; }
	inline bool* get_address_of_m_isDefaultMaterial_8() { return &___m_isDefaultMaterial_8; }
	inline void set_m_isDefaultMaterial_8(bool value)
	{
		___m_isDefaultMaterial_8 = value;
	}

	inline static int32_t get_offset_of_m_padding_9() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_padding_9)); }
	inline float get_m_padding_9() const { return ___m_padding_9; }
	inline float* get_address_of_m_padding_9() { return &___m_padding_9; }
	inline void set_m_padding_9(float value)
	{
		___m_padding_9 = value;
	}

	inline static int32_t get_offset_of_m_renderer_10() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_renderer_10)); }
	inline Renderer_t257310565 * get_m_renderer_10() const { return ___m_renderer_10; }
	inline Renderer_t257310565 ** get_address_of_m_renderer_10() { return &___m_renderer_10; }
	inline void set_m_renderer_10(Renderer_t257310565 * value)
	{
		___m_renderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_10), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_11() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_meshFilter_11)); }
	inline MeshFilter_t3026937449 * get_m_meshFilter_11() const { return ___m_meshFilter_11; }
	inline MeshFilter_t3026937449 ** get_address_of_m_meshFilter_11() { return &___m_meshFilter_11; }
	inline void set_m_meshFilter_11(MeshFilter_t3026937449 * value)
	{
		___m_meshFilter_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_11), value);
	}

	inline static int32_t get_offset_of_m_mesh_12() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_mesh_12)); }
	inline Mesh_t1356156583 * get_m_mesh_12() const { return ___m_mesh_12; }
	inline Mesh_t1356156583 ** get_address_of_m_mesh_12() { return &___m_mesh_12; }
	inline void set_m_mesh_12(Mesh_t1356156583 * value)
	{
		___m_mesh_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_12), value);
	}

	inline static int32_t get_offset_of_m_boxCollider_13() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_boxCollider_13)); }
	inline BoxCollider_t22920061 * get_m_boxCollider_13() const { return ___m_boxCollider_13; }
	inline BoxCollider_t22920061 ** get_address_of_m_boxCollider_13() { return &___m_boxCollider_13; }
	inline void set_m_boxCollider_13(BoxCollider_t22920061 * value)
	{
		___m_boxCollider_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_boxCollider_13), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_14() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_TextComponent_14)); }
	inline TextMeshPro_t2521834357 * get_m_TextComponent_14() const { return ___m_TextComponent_14; }
	inline TextMeshPro_t2521834357 ** get_address_of_m_TextComponent_14() { return &___m_TextComponent_14; }
	inline void set_m_TextComponent_14(TextMeshPro_t2521834357 * value)
	{
		___m_TextComponent_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_14), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_15() { return static_cast<int32_t>(offsetof(TMP_SubMesh_t3507543655, ___m_isRegisteredForEvents_15)); }
	inline bool get_m_isRegisteredForEvents_15() const { return ___m_isRegisteredForEvents_15; }
	inline bool* get_address_of_m_isRegisteredForEvents_15() { return &___m_isRegisteredForEvents_15; }
	inline void set_m_isRegisteredForEvents_15(bool value)
	{
		___m_isRegisteredForEvents_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESH_T3507543655_H
#ifndef TMP_SPRITEANIMATOR_T2347923044_H
#define TMP_SPRITEANIMATOR_T2347923044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAnimator
struct  TMP_SpriteAnimator_t2347923044  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> TMPro.TMP_SpriteAnimator::m_animations
	Dictionary_2_t2833400353 * ___m_animations_2;
	// TMPro.TMP_Text TMPro.TMP_SpriteAnimator::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_3;

public:
	inline static int32_t get_offset_of_m_animations_2() { return static_cast<int32_t>(offsetof(TMP_SpriteAnimator_t2347923044, ___m_animations_2)); }
	inline Dictionary_2_t2833400353 * get_m_animations_2() const { return ___m_animations_2; }
	inline Dictionary_2_t2833400353 ** get_address_of_m_animations_2() { return &___m_animations_2; }
	inline void set_m_animations_2(Dictionary_2_t2833400353 * value)
	{
		___m_animations_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_animations_2), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_3() { return static_cast<int32_t>(offsetof(TMP_SpriteAnimator_t2347923044, ___m_TextComponent_3)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_3() const { return ___m_TextComponent_3; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_3() { return &___m_TextComponent_3; }
	inline void set_m_TextComponent_3(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEANIMATOR_T2347923044_H
#ifndef GYROCONTROL_T1958351024_H
#define GYROCONTROL_T1958351024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GyroControl
struct  GyroControl_t1958351024  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GyroControl::gyroEnabled
	bool ___gyroEnabled_2;
	// UnityEngine.Gyroscope GyroControl::gyro
	Gyroscope_t1705362817 * ___gyro_3;
	// UnityEngine.GameObject GyroControl::cameraContainer
	GameObject_t1756533147 * ___cameraContainer_4;
	// UnityEngine.Quaternion GyroControl::rot
	Quaternion_t4030073918  ___rot_5;

public:
	inline static int32_t get_offset_of_gyroEnabled_2() { return static_cast<int32_t>(offsetof(GyroControl_t1958351024, ___gyroEnabled_2)); }
	inline bool get_gyroEnabled_2() const { return ___gyroEnabled_2; }
	inline bool* get_address_of_gyroEnabled_2() { return &___gyroEnabled_2; }
	inline void set_gyroEnabled_2(bool value)
	{
		___gyroEnabled_2 = value;
	}

	inline static int32_t get_offset_of_gyro_3() { return static_cast<int32_t>(offsetof(GyroControl_t1958351024, ___gyro_3)); }
	inline Gyroscope_t1705362817 * get_gyro_3() const { return ___gyro_3; }
	inline Gyroscope_t1705362817 ** get_address_of_gyro_3() { return &___gyro_3; }
	inline void set_gyro_3(Gyroscope_t1705362817 * value)
	{
		___gyro_3 = value;
		Il2CppCodeGenWriteBarrier((&___gyro_3), value);
	}

	inline static int32_t get_offset_of_cameraContainer_4() { return static_cast<int32_t>(offsetof(GyroControl_t1958351024, ___cameraContainer_4)); }
	inline GameObject_t1756533147 * get_cameraContainer_4() const { return ___cameraContainer_4; }
	inline GameObject_t1756533147 ** get_address_of_cameraContainer_4() { return &___cameraContainer_4; }
	inline void set_cameraContainer_4(GameObject_t1756533147 * value)
	{
		___cameraContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&___cameraContainer_4), value);
	}

	inline static int32_t get_offset_of_rot_5() { return static_cast<int32_t>(offsetof(GyroControl_t1958351024, ___rot_5)); }
	inline Quaternion_t4030073918  get_rot_5() const { return ___rot_5; }
	inline Quaternion_t4030073918 * get_address_of_rot_5() { return &___rot_5; }
	inline void set_rot_5(Quaternion_t4030073918  value)
	{
		___rot_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROCONTROL_T1958351024_H
#ifndef TMP_SCROLLBAREVENTHANDLER_T1222271750_H
#define TMP_SCROLLBAREVENTHANDLER_T1222271750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_ScrollbarEventHandler
struct  TMP_ScrollbarEventHandler_t1222271750  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean TMPro.TMP_ScrollbarEventHandler::isSelected
	bool ___isSelected_2;

public:
	inline static int32_t get_offset_of_isSelected_2() { return static_cast<int32_t>(offsetof(TMP_ScrollbarEventHandler_t1222271750, ___isSelected_2)); }
	inline bool get_isSelected_2() const { return ___isSelected_2; }
	inline bool* get_address_of_isSelected_2() { return &___isSelected_2; }
	inline void set_isSelected_2(bool value)
	{
		___isSelected_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SCROLLBAREVENTHANDLER_T1222271750_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef MOVEMENT_T2096174109_H
#define MOVEMENT_T2096174109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Movement
struct  Movement_t2096174109  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Movement::mouseSensitivity
	float ___mouseSensitivity_2;
	// System.Single Movement::xAxisClamp
	float ___xAxisClamp_3;

public:
	inline static int32_t get_offset_of_mouseSensitivity_2() { return static_cast<int32_t>(offsetof(Movement_t2096174109, ___mouseSensitivity_2)); }
	inline float get_mouseSensitivity_2() const { return ___mouseSensitivity_2; }
	inline float* get_address_of_mouseSensitivity_2() { return &___mouseSensitivity_2; }
	inline void set_mouseSensitivity_2(float value)
	{
		___mouseSensitivity_2 = value;
	}

	inline static int32_t get_offset_of_xAxisClamp_3() { return static_cast<int32_t>(offsetof(Movement_t2096174109, ___xAxisClamp_3)); }
	inline float get_xAxisClamp_3() const { return ___xAxisClamp_3; }
	inline float* get_address_of_xAxisClamp_3() { return &___xAxisClamp_3; }
	inline void set_xAxisClamp_3(float value)
	{
		___xAxisClamp_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMENT_T2096174109_H
#ifndef SELECTABLE_T1490392188_H
#define SELECTABLE_T1490392188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Selectable
struct  Selectable_t1490392188  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1571958496  ___m_Navigation_3;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_4;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t2652774230  ___m_Colors_5;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t1353336012  ___m_SpriteState_6;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t3244928895 * ___m_AnimationTriggers_7;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_8;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_t2426225576 * ___m_TargetGraphic_9;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_10;
	// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::m_CurrentSelectionState
	int32_t ___m_CurrentSelectionState_11;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_12;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_13;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_14;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2665681875 * ___m_CanvasGroupCache_15;

public:
	inline static int32_t get_offset_of_m_Navigation_3() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Navigation_3)); }
	inline Navigation_t1571958496  get_m_Navigation_3() const { return ___m_Navigation_3; }
	inline Navigation_t1571958496 * get_address_of_m_Navigation_3() { return &___m_Navigation_3; }
	inline void set_m_Navigation_3(Navigation_t1571958496  value)
	{
		___m_Navigation_3 = value;
	}

	inline static int32_t get_offset_of_m_Transition_4() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Transition_4)); }
	inline int32_t get_m_Transition_4() const { return ___m_Transition_4; }
	inline int32_t* get_address_of_m_Transition_4() { return &___m_Transition_4; }
	inline void set_m_Transition_4(int32_t value)
	{
		___m_Transition_4 = value;
	}

	inline static int32_t get_offset_of_m_Colors_5() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Colors_5)); }
	inline ColorBlock_t2652774230  get_m_Colors_5() const { return ___m_Colors_5; }
	inline ColorBlock_t2652774230 * get_address_of_m_Colors_5() { return &___m_Colors_5; }
	inline void set_m_Colors_5(ColorBlock_t2652774230  value)
	{
		___m_Colors_5 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_6() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_SpriteState_6)); }
	inline SpriteState_t1353336012  get_m_SpriteState_6() const { return ___m_SpriteState_6; }
	inline SpriteState_t1353336012 * get_address_of_m_SpriteState_6() { return &___m_SpriteState_6; }
	inline void set_m_SpriteState_6(SpriteState_t1353336012  value)
	{
		___m_SpriteState_6 = value;
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_7() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_AnimationTriggers_7)); }
	inline AnimationTriggers_t3244928895 * get_m_AnimationTriggers_7() const { return ___m_AnimationTriggers_7; }
	inline AnimationTriggers_t3244928895 ** get_address_of_m_AnimationTriggers_7() { return &___m_AnimationTriggers_7; }
	inline void set_m_AnimationTriggers_7(AnimationTriggers_t3244928895 * value)
	{
		___m_AnimationTriggers_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationTriggers_7), value);
	}

	inline static int32_t get_offset_of_m_Interactable_8() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_Interactable_8)); }
	inline bool get_m_Interactable_8() const { return ___m_Interactable_8; }
	inline bool* get_address_of_m_Interactable_8() { return &___m_Interactable_8; }
	inline void set_m_Interactable_8(bool value)
	{
		___m_Interactable_8 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_9() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_TargetGraphic_9)); }
	inline Graphic_t2426225576 * get_m_TargetGraphic_9() const { return ___m_TargetGraphic_9; }
	inline Graphic_t2426225576 ** get_address_of_m_TargetGraphic_9() { return &___m_TargetGraphic_9; }
	inline void set_m_TargetGraphic_9(Graphic_t2426225576 * value)
	{
		___m_TargetGraphic_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetGraphic_9), value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_10() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_GroupsAllowInteraction_10)); }
	inline bool get_m_GroupsAllowInteraction_10() const { return ___m_GroupsAllowInteraction_10; }
	inline bool* get_address_of_m_GroupsAllowInteraction_10() { return &___m_GroupsAllowInteraction_10; }
	inline void set_m_GroupsAllowInteraction_10(bool value)
	{
		___m_GroupsAllowInteraction_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelectionState_11() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CurrentSelectionState_11)); }
	inline int32_t get_m_CurrentSelectionState_11() const { return ___m_CurrentSelectionState_11; }
	inline int32_t* get_address_of_m_CurrentSelectionState_11() { return &___m_CurrentSelectionState_11; }
	inline void set_m_CurrentSelectionState_11(int32_t value)
	{
		___m_CurrentSelectionState_11 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerInsideU3Ek__BackingField_12)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_12() const { return ___U3CisPointerInsideU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_12() { return &___U3CisPointerInsideU3Ek__BackingField_12; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_12(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3CisPointerDownU3Ek__BackingField_13)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_13() const { return ___U3CisPointerDownU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_13() { return &___U3CisPointerDownU3Ek__BackingField_13; }
	inline void set_U3CisPointerDownU3Ek__BackingField_13(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___U3ChasSelectionU3Ek__BackingField_14)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_14() const { return ___U3ChasSelectionU3Ek__BackingField_14; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_14() { return &___U3ChasSelectionU3Ek__BackingField_14; }
	inline void set_U3ChasSelectionU3Ek__BackingField_14(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_15() { return static_cast<int32_t>(offsetof(Selectable_t1490392188, ___m_CanvasGroupCache_15)); }
	inline List_1_t2665681875 * get_m_CanvasGroupCache_15() const { return ___m_CanvasGroupCache_15; }
	inline List_1_t2665681875 ** get_address_of_m_CanvasGroupCache_15() { return &___m_CanvasGroupCache_15; }
	inline void set_m_CanvasGroupCache_15(List_1_t2665681875 * value)
	{
		___m_CanvasGroupCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasGroupCache_15), value);
	}
};

struct Selectable_t1490392188_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::s_List
	List_1_t859513320 * ___s_List_2;

public:
	inline static int32_t get_offset_of_s_List_2() { return static_cast<int32_t>(offsetof(Selectable_t1490392188_StaticFields, ___s_List_2)); }
	inline List_1_t859513320 * get_s_List_2() const { return ___s_List_2; }
	inline List_1_t859513320 ** get_address_of_s_List_2() { return &___s_List_2; }
	inline void set_s_List_2(List_1_t859513320 * value)
	{
		___s_List_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_List_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTABLE_T1490392188_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef TMP_INPUTFIELD_T1778301588_H
#define TMP_INPUTFIELD_T1778301588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField
struct  TMP_InputField_t1778301588  : public Selectable_t1490392188
{
public:
	// UnityEngine.TouchScreenKeyboard TMPro.TMP_InputField::m_Keyboard
	TouchScreenKeyboard_t601950206 * ___m_Keyboard_16;
	// UnityEngine.RectTransform TMPro.TMP_InputField::m_TextViewport
	RectTransform_t3349966182 * ___m_TextViewport_18;
	// TMPro.TMP_Text TMPro.TMP_InputField::m_TextComponent
	TMP_Text_t1920000777 * ___m_TextComponent_19;
	// UnityEngine.RectTransform TMPro.TMP_InputField::m_TextComponentRectTransform
	RectTransform_t3349966182 * ___m_TextComponentRectTransform_20;
	// UnityEngine.UI.Graphic TMPro.TMP_InputField::m_Placeholder
	Graphic_t2426225576 * ___m_Placeholder_21;
	// UnityEngine.UI.Scrollbar TMPro.TMP_InputField::m_VerticalScrollbar
	Scrollbar_t3248359358 * ___m_VerticalScrollbar_22;
	// TMPro.TMP_ScrollbarEventHandler TMPro.TMP_InputField::m_VerticalScrollbarEventHandler
	TMP_ScrollbarEventHandler_t1222271750 * ___m_VerticalScrollbarEventHandler_23;
	// System.Single TMPro.TMP_InputField::m_ScrollPosition
	float ___m_ScrollPosition_24;
	// System.Single TMPro.TMP_InputField::m_ScrollSensitivity
	float ___m_ScrollSensitivity_25;
	// TMPro.TMP_InputField/ContentType TMPro.TMP_InputField::m_ContentType
	int32_t ___m_ContentType_26;
	// TMPro.TMP_InputField/InputType TMPro.TMP_InputField::m_InputType
	int32_t ___m_InputType_27;
	// System.Char TMPro.TMP_InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_28;
	// UnityEngine.TouchScreenKeyboardType TMPro.TMP_InputField::m_KeyboardType
	int32_t ___m_KeyboardType_29;
	// TMPro.TMP_InputField/LineType TMPro.TMP_InputField::m_LineType
	int32_t ___m_LineType_30;
	// System.Boolean TMPro.TMP_InputField::m_HideMobileInput
	bool ___m_HideMobileInput_31;
	// TMPro.TMP_InputField/CharacterValidation TMPro.TMP_InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_32;
	// System.String TMPro.TMP_InputField::m_RegexValue
	String_t* ___m_RegexValue_33;
	// System.Single TMPro.TMP_InputField::m_GlobalPointSize
	float ___m_GlobalPointSize_34;
	// System.Int32 TMPro.TMP_InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_35;
	// TMPro.TMP_InputField/SubmitEvent TMPro.TMP_InputField::m_OnEndEdit
	SubmitEvent_t3359162065 * ___m_OnEndEdit_36;
	// TMPro.TMP_InputField/SubmitEvent TMPro.TMP_InputField::m_OnSubmit
	SubmitEvent_t3359162065 * ___m_OnSubmit_37;
	// TMPro.TMP_InputField/SelectionEvent TMPro.TMP_InputField::m_OnSelect
	SelectionEvent_t3883897865 * ___m_OnSelect_38;
	// TMPro.TMP_InputField/SelectionEvent TMPro.TMP_InputField::m_OnDeselect
	SelectionEvent_t3883897865 * ___m_OnDeselect_39;
	// TMPro.TMP_InputField/TextSelectionEvent TMPro.TMP_InputField::m_OnTextSelection
	TextSelectionEvent_t3887183206 * ___m_OnTextSelection_40;
	// TMPro.TMP_InputField/TextSelectionEvent TMPro.TMP_InputField::m_OnEndTextSelection
	TextSelectionEvent_t3887183206 * ___m_OnEndTextSelection_41;
	// TMPro.TMP_InputField/OnChangeEvent TMPro.TMP_InputField::m_OnValueChanged
	OnChangeEvent_t2139251414 * ___m_OnValueChanged_42;
	// TMPro.TMP_InputField/OnValidateInput TMPro.TMP_InputField::m_OnValidateInput
	OnValidateInput_t3285190392 * ___m_OnValidateInput_43;
	// UnityEngine.Color TMPro.TMP_InputField::m_CaretColor
	Color_t2020392075  ___m_CaretColor_44;
	// System.Boolean TMPro.TMP_InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_45;
	// UnityEngine.Color TMPro.TMP_InputField::m_SelectionColor
	Color_t2020392075  ___m_SelectionColor_46;
	// System.String TMPro.TMP_InputField::m_Text
	String_t* ___m_Text_47;
	// System.Single TMPro.TMP_InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_48;
	// System.Int32 TMPro.TMP_InputField::m_CaretWidth
	int32_t ___m_CaretWidth_49;
	// System.Boolean TMPro.TMP_InputField::m_ReadOnly
	bool ___m_ReadOnly_50;
	// System.Boolean TMPro.TMP_InputField::m_RichText
	bool ___m_RichText_51;
	// System.Int32 TMPro.TMP_InputField::m_StringPosition
	int32_t ___m_StringPosition_52;
	// System.Int32 TMPro.TMP_InputField::m_StringSelectPosition
	int32_t ___m_StringSelectPosition_53;
	// System.Int32 TMPro.TMP_InputField::m_CaretPosition
	int32_t ___m_CaretPosition_54;
	// System.Int32 TMPro.TMP_InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_55;
	// UnityEngine.RectTransform TMPro.TMP_InputField::caretRectTrans
	RectTransform_t3349966182 * ___caretRectTrans_56;
	// UnityEngine.UIVertex[] TMPro.TMP_InputField::m_CursorVerts
	UIVertexU5BU5D_t3048644023* ___m_CursorVerts_57;
	// UnityEngine.CanvasRenderer TMPro.TMP_InputField::m_CachedInputRenderer
	CanvasRenderer_t261436805 * ___m_CachedInputRenderer_58;
	// UnityEngine.Vector2 TMPro.TMP_InputField::m_DefaultTransformPosition
	Vector2_t2243707579  ___m_DefaultTransformPosition_59;
	// UnityEngine.Vector2 TMPro.TMP_InputField::m_LastPosition
	Vector2_t2243707579  ___m_LastPosition_60;
	// UnityEngine.Mesh TMPro.TMP_InputField::m_Mesh
	Mesh_t1356156583 * ___m_Mesh_61;
	// System.Boolean TMPro.TMP_InputField::m_AllowInput
	bool ___m_AllowInput_62;
	// System.Boolean TMPro.TMP_InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_63;
	// System.Boolean TMPro.TMP_InputField::m_UpdateDrag
	bool ___m_UpdateDrag_64;
	// System.Boolean TMPro.TMP_InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_65;
	// System.Boolean TMPro.TMP_InputField::m_CaretVisible
	bool ___m_CaretVisible_68;
	// UnityEngine.Coroutine TMPro.TMP_InputField::m_BlinkCoroutine
	Coroutine_t2299508840 * ___m_BlinkCoroutine_69;
	// System.Single TMPro.TMP_InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_70;
	// UnityEngine.Coroutine TMPro.TMP_InputField::m_DragCoroutine
	Coroutine_t2299508840 * ___m_DragCoroutine_71;
	// System.String TMPro.TMP_InputField::m_OriginalText
	String_t* ___m_OriginalText_72;
	// System.Boolean TMPro.TMP_InputField::m_WasCanceled
	bool ___m_WasCanceled_73;
	// System.Boolean TMPro.TMP_InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_74;
	// System.Boolean TMPro.TMP_InputField::m_IsScrollbarUpdateRequired
	bool ___m_IsScrollbarUpdateRequired_75;
	// System.Boolean TMPro.TMP_InputField::m_IsUpdatingScrollbarValues
	bool ___m_IsUpdatingScrollbarValues_76;
	// System.Boolean TMPro.TMP_InputField::m_isLastKeyBackspace
	bool ___m_isLastKeyBackspace_77;
	// System.Single TMPro.TMP_InputField::m_ClickStartTime
	float ___m_ClickStartTime_78;
	// System.Single TMPro.TMP_InputField::m_DoubleClickDelay
	float ___m_DoubleClickDelay_79;
	// TMPro.TMP_FontAsset TMPro.TMP_InputField::m_GlobalFontAsset
	TMP_FontAsset_t2530419979 * ___m_GlobalFontAsset_81;
	// System.Boolean TMPro.TMP_InputField::m_OnFocusSelectAll
	bool ___m_OnFocusSelectAll_82;
	// System.Boolean TMPro.TMP_InputField::m_isSelectAll
	bool ___m_isSelectAll_83;
	// System.Boolean TMPro.TMP_InputField::m_ResetOnDeActivation
	bool ___m_ResetOnDeActivation_84;
	// System.Boolean TMPro.TMP_InputField::m_RestoreOriginalTextOnEscape
	bool ___m_RestoreOriginalTextOnEscape_85;
	// System.Boolean TMPro.TMP_InputField::m_isRichTextEditingAllowed
	bool ___m_isRichTextEditingAllowed_86;
	// TMPro.TMP_InputValidator TMPro.TMP_InputField::m_InputValidator
	TMP_InputValidator_t3726817866 * ___m_InputValidator_87;
	// System.Boolean TMPro.TMP_InputField::m_isSelected
	bool ___m_isSelected_88;
	// System.Boolean TMPro.TMP_InputField::isStringPositionDirty
	bool ___isStringPositionDirty_89;
	// System.Boolean TMPro.TMP_InputField::m_forceRectTransformAdjustment
	bool ___m_forceRectTransformAdjustment_90;
	// UnityEngine.Event TMPro.TMP_InputField::m_ProcessingEvent
	Event_t3028476042 * ___m_ProcessingEvent_91;

public:
	inline static int32_t get_offset_of_m_Keyboard_16() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_Keyboard_16)); }
	inline TouchScreenKeyboard_t601950206 * get_m_Keyboard_16() const { return ___m_Keyboard_16; }
	inline TouchScreenKeyboard_t601950206 ** get_address_of_m_Keyboard_16() { return &___m_Keyboard_16; }
	inline void set_m_Keyboard_16(TouchScreenKeyboard_t601950206 * value)
	{
		___m_Keyboard_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Keyboard_16), value);
	}

	inline static int32_t get_offset_of_m_TextViewport_18() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_TextViewport_18)); }
	inline RectTransform_t3349966182 * get_m_TextViewport_18() const { return ___m_TextViewport_18; }
	inline RectTransform_t3349966182 ** get_address_of_m_TextViewport_18() { return &___m_TextViewport_18; }
	inline void set_m_TextViewport_18(RectTransform_t3349966182 * value)
	{
		___m_TextViewport_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextViewport_18), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_19() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_TextComponent_19)); }
	inline TMP_Text_t1920000777 * get_m_TextComponent_19() const { return ___m_TextComponent_19; }
	inline TMP_Text_t1920000777 ** get_address_of_m_TextComponent_19() { return &___m_TextComponent_19; }
	inline void set_m_TextComponent_19(TMP_Text_t1920000777 * value)
	{
		___m_TextComponent_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_19), value);
	}

	inline static int32_t get_offset_of_m_TextComponentRectTransform_20() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_TextComponentRectTransform_20)); }
	inline RectTransform_t3349966182 * get_m_TextComponentRectTransform_20() const { return ___m_TextComponentRectTransform_20; }
	inline RectTransform_t3349966182 ** get_address_of_m_TextComponentRectTransform_20() { return &___m_TextComponentRectTransform_20; }
	inline void set_m_TextComponentRectTransform_20(RectTransform_t3349966182 * value)
	{
		___m_TextComponentRectTransform_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponentRectTransform_20), value);
	}

	inline static int32_t get_offset_of_m_Placeholder_21() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_Placeholder_21)); }
	inline Graphic_t2426225576 * get_m_Placeholder_21() const { return ___m_Placeholder_21; }
	inline Graphic_t2426225576 ** get_address_of_m_Placeholder_21() { return &___m_Placeholder_21; }
	inline void set_m_Placeholder_21(Graphic_t2426225576 * value)
	{
		___m_Placeholder_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Placeholder_21), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_22() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_VerticalScrollbar_22)); }
	inline Scrollbar_t3248359358 * get_m_VerticalScrollbar_22() const { return ___m_VerticalScrollbar_22; }
	inline Scrollbar_t3248359358 ** get_address_of_m_VerticalScrollbar_22() { return &___m_VerticalScrollbar_22; }
	inline void set_m_VerticalScrollbar_22(Scrollbar_t3248359358 * value)
	{
		___m_VerticalScrollbar_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbar_22), value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarEventHandler_23() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_VerticalScrollbarEventHandler_23)); }
	inline TMP_ScrollbarEventHandler_t1222271750 * get_m_VerticalScrollbarEventHandler_23() const { return ___m_VerticalScrollbarEventHandler_23; }
	inline TMP_ScrollbarEventHandler_t1222271750 ** get_address_of_m_VerticalScrollbarEventHandler_23() { return &___m_VerticalScrollbarEventHandler_23; }
	inline void set_m_VerticalScrollbarEventHandler_23(TMP_ScrollbarEventHandler_t1222271750 * value)
	{
		___m_VerticalScrollbarEventHandler_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_VerticalScrollbarEventHandler_23), value);
	}

	inline static int32_t get_offset_of_m_ScrollPosition_24() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_ScrollPosition_24)); }
	inline float get_m_ScrollPosition_24() const { return ___m_ScrollPosition_24; }
	inline float* get_address_of_m_ScrollPosition_24() { return &___m_ScrollPosition_24; }
	inline void set_m_ScrollPosition_24(float value)
	{
		___m_ScrollPosition_24 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_25() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_ScrollSensitivity_25)); }
	inline float get_m_ScrollSensitivity_25() const { return ___m_ScrollSensitivity_25; }
	inline float* get_address_of_m_ScrollSensitivity_25() { return &___m_ScrollSensitivity_25; }
	inline void set_m_ScrollSensitivity_25(float value)
	{
		___m_ScrollSensitivity_25 = value;
	}

	inline static int32_t get_offset_of_m_ContentType_26() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_ContentType_26)); }
	inline int32_t get_m_ContentType_26() const { return ___m_ContentType_26; }
	inline int32_t* get_address_of_m_ContentType_26() { return &___m_ContentType_26; }
	inline void set_m_ContentType_26(int32_t value)
	{
		___m_ContentType_26 = value;
	}

	inline static int32_t get_offset_of_m_InputType_27() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_InputType_27)); }
	inline int32_t get_m_InputType_27() const { return ___m_InputType_27; }
	inline int32_t* get_address_of_m_InputType_27() { return &___m_InputType_27; }
	inline void set_m_InputType_27(int32_t value)
	{
		___m_InputType_27 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_28() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_AsteriskChar_28)); }
	inline Il2CppChar get_m_AsteriskChar_28() const { return ___m_AsteriskChar_28; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_28() { return &___m_AsteriskChar_28; }
	inline void set_m_AsteriskChar_28(Il2CppChar value)
	{
		___m_AsteriskChar_28 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_29() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_KeyboardType_29)); }
	inline int32_t get_m_KeyboardType_29() const { return ___m_KeyboardType_29; }
	inline int32_t* get_address_of_m_KeyboardType_29() { return &___m_KeyboardType_29; }
	inline void set_m_KeyboardType_29(int32_t value)
	{
		___m_KeyboardType_29 = value;
	}

	inline static int32_t get_offset_of_m_LineType_30() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_LineType_30)); }
	inline int32_t get_m_LineType_30() const { return ___m_LineType_30; }
	inline int32_t* get_address_of_m_LineType_30() { return &___m_LineType_30; }
	inline void set_m_LineType_30(int32_t value)
	{
		___m_LineType_30 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_31() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_HideMobileInput_31)); }
	inline bool get_m_HideMobileInput_31() const { return ___m_HideMobileInput_31; }
	inline bool* get_address_of_m_HideMobileInput_31() { return &___m_HideMobileInput_31; }
	inline void set_m_HideMobileInput_31(bool value)
	{
		___m_HideMobileInput_31 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_32() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CharacterValidation_32)); }
	inline int32_t get_m_CharacterValidation_32() const { return ___m_CharacterValidation_32; }
	inline int32_t* get_address_of_m_CharacterValidation_32() { return &___m_CharacterValidation_32; }
	inline void set_m_CharacterValidation_32(int32_t value)
	{
		___m_CharacterValidation_32 = value;
	}

	inline static int32_t get_offset_of_m_RegexValue_33() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_RegexValue_33)); }
	inline String_t* get_m_RegexValue_33() const { return ___m_RegexValue_33; }
	inline String_t** get_address_of_m_RegexValue_33() { return &___m_RegexValue_33; }
	inline void set_m_RegexValue_33(String_t* value)
	{
		___m_RegexValue_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_RegexValue_33), value);
	}

	inline static int32_t get_offset_of_m_GlobalPointSize_34() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_GlobalPointSize_34)); }
	inline float get_m_GlobalPointSize_34() const { return ___m_GlobalPointSize_34; }
	inline float* get_address_of_m_GlobalPointSize_34() { return &___m_GlobalPointSize_34; }
	inline void set_m_GlobalPointSize_34(float value)
	{
		___m_GlobalPointSize_34 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_35() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CharacterLimit_35)); }
	inline int32_t get_m_CharacterLimit_35() const { return ___m_CharacterLimit_35; }
	inline int32_t* get_address_of_m_CharacterLimit_35() { return &___m_CharacterLimit_35; }
	inline void set_m_CharacterLimit_35(int32_t value)
	{
		___m_CharacterLimit_35 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_36() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OnEndEdit_36)); }
	inline SubmitEvent_t3359162065 * get_m_OnEndEdit_36() const { return ___m_OnEndEdit_36; }
	inline SubmitEvent_t3359162065 ** get_address_of_m_OnEndEdit_36() { return &___m_OnEndEdit_36; }
	inline void set_m_OnEndEdit_36(SubmitEvent_t3359162065 * value)
	{
		___m_OnEndEdit_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndEdit_36), value);
	}

	inline static int32_t get_offset_of_m_OnSubmit_37() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OnSubmit_37)); }
	inline SubmitEvent_t3359162065 * get_m_OnSubmit_37() const { return ___m_OnSubmit_37; }
	inline SubmitEvent_t3359162065 ** get_address_of_m_OnSubmit_37() { return &___m_OnSubmit_37; }
	inline void set_m_OnSubmit_37(SubmitEvent_t3359162065 * value)
	{
		___m_OnSubmit_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSubmit_37), value);
	}

	inline static int32_t get_offset_of_m_OnSelect_38() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OnSelect_38)); }
	inline SelectionEvent_t3883897865 * get_m_OnSelect_38() const { return ___m_OnSelect_38; }
	inline SelectionEvent_t3883897865 ** get_address_of_m_OnSelect_38() { return &___m_OnSelect_38; }
	inline void set_m_OnSelect_38(SelectionEvent_t3883897865 * value)
	{
		___m_OnSelect_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnSelect_38), value);
	}

	inline static int32_t get_offset_of_m_OnDeselect_39() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OnDeselect_39)); }
	inline SelectionEvent_t3883897865 * get_m_OnDeselect_39() const { return ___m_OnDeselect_39; }
	inline SelectionEvent_t3883897865 ** get_address_of_m_OnDeselect_39() { return &___m_OnDeselect_39; }
	inline void set_m_OnDeselect_39(SelectionEvent_t3883897865 * value)
	{
		___m_OnDeselect_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDeselect_39), value);
	}

	inline static int32_t get_offset_of_m_OnTextSelection_40() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OnTextSelection_40)); }
	inline TextSelectionEvent_t3887183206 * get_m_OnTextSelection_40() const { return ___m_OnTextSelection_40; }
	inline TextSelectionEvent_t3887183206 ** get_address_of_m_OnTextSelection_40() { return &___m_OnTextSelection_40; }
	inline void set_m_OnTextSelection_40(TextSelectionEvent_t3887183206 * value)
	{
		___m_OnTextSelection_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnTextSelection_40), value);
	}

	inline static int32_t get_offset_of_m_OnEndTextSelection_41() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OnEndTextSelection_41)); }
	inline TextSelectionEvent_t3887183206 * get_m_OnEndTextSelection_41() const { return ___m_OnEndTextSelection_41; }
	inline TextSelectionEvent_t3887183206 ** get_address_of_m_OnEndTextSelection_41() { return &___m_OnEndTextSelection_41; }
	inline void set_m_OnEndTextSelection_41(TextSelectionEvent_t3887183206 * value)
	{
		___m_OnEndTextSelection_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnEndTextSelection_41), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_42() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OnValueChanged_42)); }
	inline OnChangeEvent_t2139251414 * get_m_OnValueChanged_42() const { return ___m_OnValueChanged_42; }
	inline OnChangeEvent_t2139251414 ** get_address_of_m_OnValueChanged_42() { return &___m_OnValueChanged_42; }
	inline void set_m_OnValueChanged_42(OnChangeEvent_t2139251414 * value)
	{
		___m_OnValueChanged_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_42), value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_43() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OnValidateInput_43)); }
	inline OnValidateInput_t3285190392 * get_m_OnValidateInput_43() const { return ___m_OnValidateInput_43; }
	inline OnValidateInput_t3285190392 ** get_address_of_m_OnValidateInput_43() { return &___m_OnValidateInput_43; }
	inline void set_m_OnValidateInput_43(OnValidateInput_t3285190392 * value)
	{
		___m_OnValidateInput_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValidateInput_43), value);
	}

	inline static int32_t get_offset_of_m_CaretColor_44() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CaretColor_44)); }
	inline Color_t2020392075  get_m_CaretColor_44() const { return ___m_CaretColor_44; }
	inline Color_t2020392075 * get_address_of_m_CaretColor_44() { return &___m_CaretColor_44; }
	inline void set_m_CaretColor_44(Color_t2020392075  value)
	{
		___m_CaretColor_44 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_45() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CustomCaretColor_45)); }
	inline bool get_m_CustomCaretColor_45() const { return ___m_CustomCaretColor_45; }
	inline bool* get_address_of_m_CustomCaretColor_45() { return &___m_CustomCaretColor_45; }
	inline void set_m_CustomCaretColor_45(bool value)
	{
		___m_CustomCaretColor_45 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_46() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_SelectionColor_46)); }
	inline Color_t2020392075  get_m_SelectionColor_46() const { return ___m_SelectionColor_46; }
	inline Color_t2020392075 * get_address_of_m_SelectionColor_46() { return &___m_SelectionColor_46; }
	inline void set_m_SelectionColor_46(Color_t2020392075  value)
	{
		___m_SelectionColor_46 = value;
	}

	inline static int32_t get_offset_of_m_Text_47() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_Text_47)); }
	inline String_t* get_m_Text_47() const { return ___m_Text_47; }
	inline String_t** get_address_of_m_Text_47() { return &___m_Text_47; }
	inline void set_m_Text_47(String_t* value)
	{
		___m_Text_47 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_47), value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_48() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CaretBlinkRate_48)); }
	inline float get_m_CaretBlinkRate_48() const { return ___m_CaretBlinkRate_48; }
	inline float* get_address_of_m_CaretBlinkRate_48() { return &___m_CaretBlinkRate_48; }
	inline void set_m_CaretBlinkRate_48(float value)
	{
		___m_CaretBlinkRate_48 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_49() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CaretWidth_49)); }
	inline int32_t get_m_CaretWidth_49() const { return ___m_CaretWidth_49; }
	inline int32_t* get_address_of_m_CaretWidth_49() { return &___m_CaretWidth_49; }
	inline void set_m_CaretWidth_49(int32_t value)
	{
		___m_CaretWidth_49 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_50() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_ReadOnly_50)); }
	inline bool get_m_ReadOnly_50() const { return ___m_ReadOnly_50; }
	inline bool* get_address_of_m_ReadOnly_50() { return &___m_ReadOnly_50; }
	inline void set_m_ReadOnly_50(bool value)
	{
		___m_ReadOnly_50 = value;
	}

	inline static int32_t get_offset_of_m_RichText_51() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_RichText_51)); }
	inline bool get_m_RichText_51() const { return ___m_RichText_51; }
	inline bool* get_address_of_m_RichText_51() { return &___m_RichText_51; }
	inline void set_m_RichText_51(bool value)
	{
		___m_RichText_51 = value;
	}

	inline static int32_t get_offset_of_m_StringPosition_52() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_StringPosition_52)); }
	inline int32_t get_m_StringPosition_52() const { return ___m_StringPosition_52; }
	inline int32_t* get_address_of_m_StringPosition_52() { return &___m_StringPosition_52; }
	inline void set_m_StringPosition_52(int32_t value)
	{
		___m_StringPosition_52 = value;
	}

	inline static int32_t get_offset_of_m_StringSelectPosition_53() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_StringSelectPosition_53)); }
	inline int32_t get_m_StringSelectPosition_53() const { return ___m_StringSelectPosition_53; }
	inline int32_t* get_address_of_m_StringSelectPosition_53() { return &___m_StringSelectPosition_53; }
	inline void set_m_StringSelectPosition_53(int32_t value)
	{
		___m_StringSelectPosition_53 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_54() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CaretPosition_54)); }
	inline int32_t get_m_CaretPosition_54() const { return ___m_CaretPosition_54; }
	inline int32_t* get_address_of_m_CaretPosition_54() { return &___m_CaretPosition_54; }
	inline void set_m_CaretPosition_54(int32_t value)
	{
		___m_CaretPosition_54 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_55() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CaretSelectPosition_55)); }
	inline int32_t get_m_CaretSelectPosition_55() const { return ___m_CaretSelectPosition_55; }
	inline int32_t* get_address_of_m_CaretSelectPosition_55() { return &___m_CaretSelectPosition_55; }
	inline void set_m_CaretSelectPosition_55(int32_t value)
	{
		___m_CaretSelectPosition_55 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_56() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___caretRectTrans_56)); }
	inline RectTransform_t3349966182 * get_caretRectTrans_56() const { return ___caretRectTrans_56; }
	inline RectTransform_t3349966182 ** get_address_of_caretRectTrans_56() { return &___caretRectTrans_56; }
	inline void set_caretRectTrans_56(RectTransform_t3349966182 * value)
	{
		___caretRectTrans_56 = value;
		Il2CppCodeGenWriteBarrier((&___caretRectTrans_56), value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_57() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CursorVerts_57)); }
	inline UIVertexU5BU5D_t3048644023* get_m_CursorVerts_57() const { return ___m_CursorVerts_57; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_CursorVerts_57() { return &___m_CursorVerts_57; }
	inline void set_m_CursorVerts_57(UIVertexU5BU5D_t3048644023* value)
	{
		___m_CursorVerts_57 = value;
		Il2CppCodeGenWriteBarrier((&___m_CursorVerts_57), value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_58() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CachedInputRenderer_58)); }
	inline CanvasRenderer_t261436805 * get_m_CachedInputRenderer_58() const { return ___m_CachedInputRenderer_58; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CachedInputRenderer_58() { return &___m_CachedInputRenderer_58; }
	inline void set_m_CachedInputRenderer_58(CanvasRenderer_t261436805 * value)
	{
		___m_CachedInputRenderer_58 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedInputRenderer_58), value);
	}

	inline static int32_t get_offset_of_m_DefaultTransformPosition_59() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_DefaultTransformPosition_59)); }
	inline Vector2_t2243707579  get_m_DefaultTransformPosition_59() const { return ___m_DefaultTransformPosition_59; }
	inline Vector2_t2243707579 * get_address_of_m_DefaultTransformPosition_59() { return &___m_DefaultTransformPosition_59; }
	inline void set_m_DefaultTransformPosition_59(Vector2_t2243707579  value)
	{
		___m_DefaultTransformPosition_59 = value;
	}

	inline static int32_t get_offset_of_m_LastPosition_60() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_LastPosition_60)); }
	inline Vector2_t2243707579  get_m_LastPosition_60() const { return ___m_LastPosition_60; }
	inline Vector2_t2243707579 * get_address_of_m_LastPosition_60() { return &___m_LastPosition_60; }
	inline void set_m_LastPosition_60(Vector2_t2243707579  value)
	{
		___m_LastPosition_60 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_61() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_Mesh_61)); }
	inline Mesh_t1356156583 * get_m_Mesh_61() const { return ___m_Mesh_61; }
	inline Mesh_t1356156583 ** get_address_of_m_Mesh_61() { return &___m_Mesh_61; }
	inline void set_m_Mesh_61(Mesh_t1356156583 * value)
	{
		___m_Mesh_61 = value;
		Il2CppCodeGenWriteBarrier((&___m_Mesh_61), value);
	}

	inline static int32_t get_offset_of_m_AllowInput_62() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_AllowInput_62)); }
	inline bool get_m_AllowInput_62() const { return ___m_AllowInput_62; }
	inline bool* get_address_of_m_AllowInput_62() { return &___m_AllowInput_62; }
	inline void set_m_AllowInput_62(bool value)
	{
		___m_AllowInput_62 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_63() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_ShouldActivateNextUpdate_63)); }
	inline bool get_m_ShouldActivateNextUpdate_63() const { return ___m_ShouldActivateNextUpdate_63; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_63() { return &___m_ShouldActivateNextUpdate_63; }
	inline void set_m_ShouldActivateNextUpdate_63(bool value)
	{
		___m_ShouldActivateNextUpdate_63 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_64() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_UpdateDrag_64)); }
	inline bool get_m_UpdateDrag_64() const { return ___m_UpdateDrag_64; }
	inline bool* get_address_of_m_UpdateDrag_64() { return &___m_UpdateDrag_64; }
	inline void set_m_UpdateDrag_64(bool value)
	{
		___m_UpdateDrag_64 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_65() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_DragPositionOutOfBounds_65)); }
	inline bool get_m_DragPositionOutOfBounds_65() const { return ___m_DragPositionOutOfBounds_65; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_65() { return &___m_DragPositionOutOfBounds_65; }
	inline void set_m_DragPositionOutOfBounds_65(bool value)
	{
		___m_DragPositionOutOfBounds_65 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_68() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_CaretVisible_68)); }
	inline bool get_m_CaretVisible_68() const { return ___m_CaretVisible_68; }
	inline bool* get_address_of_m_CaretVisible_68() { return &___m_CaretVisible_68; }
	inline void set_m_CaretVisible_68(bool value)
	{
		___m_CaretVisible_68 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_69() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_BlinkCoroutine_69)); }
	inline Coroutine_t2299508840 * get_m_BlinkCoroutine_69() const { return ___m_BlinkCoroutine_69; }
	inline Coroutine_t2299508840 ** get_address_of_m_BlinkCoroutine_69() { return &___m_BlinkCoroutine_69; }
	inline void set_m_BlinkCoroutine_69(Coroutine_t2299508840 * value)
	{
		___m_BlinkCoroutine_69 = value;
		Il2CppCodeGenWriteBarrier((&___m_BlinkCoroutine_69), value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_70() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_BlinkStartTime_70)); }
	inline float get_m_BlinkStartTime_70() const { return ___m_BlinkStartTime_70; }
	inline float* get_address_of_m_BlinkStartTime_70() { return &___m_BlinkStartTime_70; }
	inline void set_m_BlinkStartTime_70(float value)
	{
		___m_BlinkStartTime_70 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_71() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_DragCoroutine_71)); }
	inline Coroutine_t2299508840 * get_m_DragCoroutine_71() const { return ___m_DragCoroutine_71; }
	inline Coroutine_t2299508840 ** get_address_of_m_DragCoroutine_71() { return &___m_DragCoroutine_71; }
	inline void set_m_DragCoroutine_71(Coroutine_t2299508840 * value)
	{
		___m_DragCoroutine_71 = value;
		Il2CppCodeGenWriteBarrier((&___m_DragCoroutine_71), value);
	}

	inline static int32_t get_offset_of_m_OriginalText_72() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OriginalText_72)); }
	inline String_t* get_m_OriginalText_72() const { return ___m_OriginalText_72; }
	inline String_t** get_address_of_m_OriginalText_72() { return &___m_OriginalText_72; }
	inline void set_m_OriginalText_72(String_t* value)
	{
		___m_OriginalText_72 = value;
		Il2CppCodeGenWriteBarrier((&___m_OriginalText_72), value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_73() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_WasCanceled_73)); }
	inline bool get_m_WasCanceled_73() const { return ___m_WasCanceled_73; }
	inline bool* get_address_of_m_WasCanceled_73() { return &___m_WasCanceled_73; }
	inline void set_m_WasCanceled_73(bool value)
	{
		___m_WasCanceled_73 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_74() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_HasDoneFocusTransition_74)); }
	inline bool get_m_HasDoneFocusTransition_74() const { return ___m_HasDoneFocusTransition_74; }
	inline bool* get_address_of_m_HasDoneFocusTransition_74() { return &___m_HasDoneFocusTransition_74; }
	inline void set_m_HasDoneFocusTransition_74(bool value)
	{
		___m_HasDoneFocusTransition_74 = value;
	}

	inline static int32_t get_offset_of_m_IsScrollbarUpdateRequired_75() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_IsScrollbarUpdateRequired_75)); }
	inline bool get_m_IsScrollbarUpdateRequired_75() const { return ___m_IsScrollbarUpdateRequired_75; }
	inline bool* get_address_of_m_IsScrollbarUpdateRequired_75() { return &___m_IsScrollbarUpdateRequired_75; }
	inline void set_m_IsScrollbarUpdateRequired_75(bool value)
	{
		___m_IsScrollbarUpdateRequired_75 = value;
	}

	inline static int32_t get_offset_of_m_IsUpdatingScrollbarValues_76() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_IsUpdatingScrollbarValues_76)); }
	inline bool get_m_IsUpdatingScrollbarValues_76() const { return ___m_IsUpdatingScrollbarValues_76; }
	inline bool* get_address_of_m_IsUpdatingScrollbarValues_76() { return &___m_IsUpdatingScrollbarValues_76; }
	inline void set_m_IsUpdatingScrollbarValues_76(bool value)
	{
		___m_IsUpdatingScrollbarValues_76 = value;
	}

	inline static int32_t get_offset_of_m_isLastKeyBackspace_77() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_isLastKeyBackspace_77)); }
	inline bool get_m_isLastKeyBackspace_77() const { return ___m_isLastKeyBackspace_77; }
	inline bool* get_address_of_m_isLastKeyBackspace_77() { return &___m_isLastKeyBackspace_77; }
	inline void set_m_isLastKeyBackspace_77(bool value)
	{
		___m_isLastKeyBackspace_77 = value;
	}

	inline static int32_t get_offset_of_m_ClickStartTime_78() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_ClickStartTime_78)); }
	inline float get_m_ClickStartTime_78() const { return ___m_ClickStartTime_78; }
	inline float* get_address_of_m_ClickStartTime_78() { return &___m_ClickStartTime_78; }
	inline void set_m_ClickStartTime_78(float value)
	{
		___m_ClickStartTime_78 = value;
	}

	inline static int32_t get_offset_of_m_DoubleClickDelay_79() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_DoubleClickDelay_79)); }
	inline float get_m_DoubleClickDelay_79() const { return ___m_DoubleClickDelay_79; }
	inline float* get_address_of_m_DoubleClickDelay_79() { return &___m_DoubleClickDelay_79; }
	inline void set_m_DoubleClickDelay_79(float value)
	{
		___m_DoubleClickDelay_79 = value;
	}

	inline static int32_t get_offset_of_m_GlobalFontAsset_81() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_GlobalFontAsset_81)); }
	inline TMP_FontAsset_t2530419979 * get_m_GlobalFontAsset_81() const { return ___m_GlobalFontAsset_81; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_m_GlobalFontAsset_81() { return &___m_GlobalFontAsset_81; }
	inline void set_m_GlobalFontAsset_81(TMP_FontAsset_t2530419979 * value)
	{
		___m_GlobalFontAsset_81 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlobalFontAsset_81), value);
	}

	inline static int32_t get_offset_of_m_OnFocusSelectAll_82() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_OnFocusSelectAll_82)); }
	inline bool get_m_OnFocusSelectAll_82() const { return ___m_OnFocusSelectAll_82; }
	inline bool* get_address_of_m_OnFocusSelectAll_82() { return &___m_OnFocusSelectAll_82; }
	inline void set_m_OnFocusSelectAll_82(bool value)
	{
		___m_OnFocusSelectAll_82 = value;
	}

	inline static int32_t get_offset_of_m_isSelectAll_83() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_isSelectAll_83)); }
	inline bool get_m_isSelectAll_83() const { return ___m_isSelectAll_83; }
	inline bool* get_address_of_m_isSelectAll_83() { return &___m_isSelectAll_83; }
	inline void set_m_isSelectAll_83(bool value)
	{
		___m_isSelectAll_83 = value;
	}

	inline static int32_t get_offset_of_m_ResetOnDeActivation_84() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_ResetOnDeActivation_84)); }
	inline bool get_m_ResetOnDeActivation_84() const { return ___m_ResetOnDeActivation_84; }
	inline bool* get_address_of_m_ResetOnDeActivation_84() { return &___m_ResetOnDeActivation_84; }
	inline void set_m_ResetOnDeActivation_84(bool value)
	{
		___m_ResetOnDeActivation_84 = value;
	}

	inline static int32_t get_offset_of_m_RestoreOriginalTextOnEscape_85() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_RestoreOriginalTextOnEscape_85)); }
	inline bool get_m_RestoreOriginalTextOnEscape_85() const { return ___m_RestoreOriginalTextOnEscape_85; }
	inline bool* get_address_of_m_RestoreOriginalTextOnEscape_85() { return &___m_RestoreOriginalTextOnEscape_85; }
	inline void set_m_RestoreOriginalTextOnEscape_85(bool value)
	{
		___m_RestoreOriginalTextOnEscape_85 = value;
	}

	inline static int32_t get_offset_of_m_isRichTextEditingAllowed_86() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_isRichTextEditingAllowed_86)); }
	inline bool get_m_isRichTextEditingAllowed_86() const { return ___m_isRichTextEditingAllowed_86; }
	inline bool* get_address_of_m_isRichTextEditingAllowed_86() { return &___m_isRichTextEditingAllowed_86; }
	inline void set_m_isRichTextEditingAllowed_86(bool value)
	{
		___m_isRichTextEditingAllowed_86 = value;
	}

	inline static int32_t get_offset_of_m_InputValidator_87() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_InputValidator_87)); }
	inline TMP_InputValidator_t3726817866 * get_m_InputValidator_87() const { return ___m_InputValidator_87; }
	inline TMP_InputValidator_t3726817866 ** get_address_of_m_InputValidator_87() { return &___m_InputValidator_87; }
	inline void set_m_InputValidator_87(TMP_InputValidator_t3726817866 * value)
	{
		___m_InputValidator_87 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputValidator_87), value);
	}

	inline static int32_t get_offset_of_m_isSelected_88() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_isSelected_88)); }
	inline bool get_m_isSelected_88() const { return ___m_isSelected_88; }
	inline bool* get_address_of_m_isSelected_88() { return &___m_isSelected_88; }
	inline void set_m_isSelected_88(bool value)
	{
		___m_isSelected_88 = value;
	}

	inline static int32_t get_offset_of_isStringPositionDirty_89() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___isStringPositionDirty_89)); }
	inline bool get_isStringPositionDirty_89() const { return ___isStringPositionDirty_89; }
	inline bool* get_address_of_isStringPositionDirty_89() { return &___isStringPositionDirty_89; }
	inline void set_isStringPositionDirty_89(bool value)
	{
		___isStringPositionDirty_89 = value;
	}

	inline static int32_t get_offset_of_m_forceRectTransformAdjustment_90() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_forceRectTransformAdjustment_90)); }
	inline bool get_m_forceRectTransformAdjustment_90() const { return ___m_forceRectTransformAdjustment_90; }
	inline bool* get_address_of_m_forceRectTransformAdjustment_90() { return &___m_forceRectTransformAdjustment_90; }
	inline void set_m_forceRectTransformAdjustment_90(bool value)
	{
		___m_forceRectTransformAdjustment_90 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_91() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588, ___m_ProcessingEvent_91)); }
	inline Event_t3028476042 * get_m_ProcessingEvent_91() const { return ___m_ProcessingEvent_91; }
	inline Event_t3028476042 ** get_address_of_m_ProcessingEvent_91() { return &___m_ProcessingEvent_91; }
	inline void set_m_ProcessingEvent_91(Event_t3028476042 * value)
	{
		___m_ProcessingEvent_91 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProcessingEvent_91), value);
	}
};

struct TMP_InputField_t1778301588_StaticFields
{
public:
	// System.Char[] TMPro.TMP_InputField::kSeparators
	CharU5BU5D_t1328083999* ___kSeparators_17;

public:
	inline static int32_t get_offset_of_kSeparators_17() { return static_cast<int32_t>(offsetof(TMP_InputField_t1778301588_StaticFields, ___kSeparators_17)); }
	inline CharU5BU5D_t1328083999* get_kSeparators_17() const { return ___kSeparators_17; }
	inline CharU5BU5D_t1328083999** get_address_of_kSeparators_17() { return &___kSeparators_17; }
	inline void set_kSeparators_17(CharU5BU5D_t1328083999* value)
	{
		___kSeparators_17 = value;
		Il2CppCodeGenWriteBarrier((&___kSeparators_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_INPUTFIELD_T1778301588_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef TMP_DROPDOWN_T1768193147_H
#define TMP_DROPDOWN_T1768193147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Dropdown
struct  TMP_Dropdown_t1768193147  : public Selectable_t1490392188
{
public:
	// UnityEngine.RectTransform TMPro.TMP_Dropdown::m_Template
	RectTransform_t3349966182 * ___m_Template_16;
	// TMPro.TMP_Text TMPro.TMP_Dropdown::m_CaptionText
	TMP_Text_t1920000777 * ___m_CaptionText_17;
	// UnityEngine.UI.Image TMPro.TMP_Dropdown::m_CaptionImage
	Image_t2042527209 * ___m_CaptionImage_18;
	// TMPro.TMP_Text TMPro.TMP_Dropdown::m_ItemText
	TMP_Text_t1920000777 * ___m_ItemText_19;
	// UnityEngine.UI.Image TMPro.TMP_Dropdown::m_ItemImage
	Image_t2042527209 * ___m_ItemImage_20;
	// System.Int32 TMPro.TMP_Dropdown::m_Value
	int32_t ___m_Value_21;
	// TMPro.TMP_Dropdown/OptionDataList TMPro.TMP_Dropdown::m_Options
	OptionDataList_t457963479 * ___m_Options_22;
	// TMPro.TMP_Dropdown/DropdownEvent TMPro.TMP_Dropdown::m_OnValueChanged
	DropdownEvent_t1859881917 * ___m_OnValueChanged_23;
	// UnityEngine.GameObject TMPro.TMP_Dropdown::m_Dropdown
	GameObject_t1756533147 * ___m_Dropdown_24;
	// UnityEngine.GameObject TMPro.TMP_Dropdown::m_Blocker
	GameObject_t1756533147 * ___m_Blocker_25;
	// System.Collections.Generic.List`1<TMPro.TMP_Dropdown/DropdownItem> TMPro.TMP_Dropdown::m_Items
	List_1_t621037522 * ___m_Items_26;
	// TMPro.TweenRunner`1<TMPro.FloatTween> TMPro.TMP_Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t1736487048 * ___m_AlphaTweenRunner_27;
	// System.Boolean TMPro.TMP_Dropdown::validTemplate
	bool ___validTemplate_28;

public:
	inline static int32_t get_offset_of_m_Template_16() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_Template_16)); }
	inline RectTransform_t3349966182 * get_m_Template_16() const { return ___m_Template_16; }
	inline RectTransform_t3349966182 ** get_address_of_m_Template_16() { return &___m_Template_16; }
	inline void set_m_Template_16(RectTransform_t3349966182 * value)
	{
		___m_Template_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_Template_16), value);
	}

	inline static int32_t get_offset_of_m_CaptionText_17() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_CaptionText_17)); }
	inline TMP_Text_t1920000777 * get_m_CaptionText_17() const { return ___m_CaptionText_17; }
	inline TMP_Text_t1920000777 ** get_address_of_m_CaptionText_17() { return &___m_CaptionText_17; }
	inline void set_m_CaptionText_17(TMP_Text_t1920000777 * value)
	{
		___m_CaptionText_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionText_17), value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_18() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_CaptionImage_18)); }
	inline Image_t2042527209 * get_m_CaptionImage_18() const { return ___m_CaptionImage_18; }
	inline Image_t2042527209 ** get_address_of_m_CaptionImage_18() { return &___m_CaptionImage_18; }
	inline void set_m_CaptionImage_18(Image_t2042527209 * value)
	{
		___m_CaptionImage_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_CaptionImage_18), value);
	}

	inline static int32_t get_offset_of_m_ItemText_19() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_ItemText_19)); }
	inline TMP_Text_t1920000777 * get_m_ItemText_19() const { return ___m_ItemText_19; }
	inline TMP_Text_t1920000777 ** get_address_of_m_ItemText_19() { return &___m_ItemText_19; }
	inline void set_m_ItemText_19(TMP_Text_t1920000777 * value)
	{
		___m_ItemText_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemText_19), value);
	}

	inline static int32_t get_offset_of_m_ItemImage_20() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_ItemImage_20)); }
	inline Image_t2042527209 * get_m_ItemImage_20() const { return ___m_ItemImage_20; }
	inline Image_t2042527209 ** get_address_of_m_ItemImage_20() { return &___m_ItemImage_20; }
	inline void set_m_ItemImage_20(Image_t2042527209 * value)
	{
		___m_ItemImage_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemImage_20), value);
	}

	inline static int32_t get_offset_of_m_Value_21() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_Value_21)); }
	inline int32_t get_m_Value_21() const { return ___m_Value_21; }
	inline int32_t* get_address_of_m_Value_21() { return &___m_Value_21; }
	inline void set_m_Value_21(int32_t value)
	{
		___m_Value_21 = value;
	}

	inline static int32_t get_offset_of_m_Options_22() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_Options_22)); }
	inline OptionDataList_t457963479 * get_m_Options_22() const { return ___m_Options_22; }
	inline OptionDataList_t457963479 ** get_address_of_m_Options_22() { return &___m_Options_22; }
	inline void set_m_Options_22(OptionDataList_t457963479 * value)
	{
		___m_Options_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_Options_22), value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_23() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_OnValueChanged_23)); }
	inline DropdownEvent_t1859881917 * get_m_OnValueChanged_23() const { return ___m_OnValueChanged_23; }
	inline DropdownEvent_t1859881917 ** get_address_of_m_OnValueChanged_23() { return &___m_OnValueChanged_23; }
	inline void set_m_OnValueChanged_23(DropdownEvent_t1859881917 * value)
	{
		___m_OnValueChanged_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnValueChanged_23), value);
	}

	inline static int32_t get_offset_of_m_Dropdown_24() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_Dropdown_24)); }
	inline GameObject_t1756533147 * get_m_Dropdown_24() const { return ___m_Dropdown_24; }
	inline GameObject_t1756533147 ** get_address_of_m_Dropdown_24() { return &___m_Dropdown_24; }
	inline void set_m_Dropdown_24(GameObject_t1756533147 * value)
	{
		___m_Dropdown_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dropdown_24), value);
	}

	inline static int32_t get_offset_of_m_Blocker_25() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_Blocker_25)); }
	inline GameObject_t1756533147 * get_m_Blocker_25() const { return ___m_Blocker_25; }
	inline GameObject_t1756533147 ** get_address_of_m_Blocker_25() { return &___m_Blocker_25; }
	inline void set_m_Blocker_25(GameObject_t1756533147 * value)
	{
		___m_Blocker_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Blocker_25), value);
	}

	inline static int32_t get_offset_of_m_Items_26() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_Items_26)); }
	inline List_1_t621037522 * get_m_Items_26() const { return ___m_Items_26; }
	inline List_1_t621037522 ** get_address_of_m_Items_26() { return &___m_Items_26; }
	inline void set_m_Items_26(List_1_t621037522 * value)
	{
		___m_Items_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Items_26), value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_27() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___m_AlphaTweenRunner_27)); }
	inline TweenRunner_1_t1736487048 * get_m_AlphaTweenRunner_27() const { return ___m_AlphaTweenRunner_27; }
	inline TweenRunner_1_t1736487048 ** get_address_of_m_AlphaTweenRunner_27() { return &___m_AlphaTweenRunner_27; }
	inline void set_m_AlphaTweenRunner_27(TweenRunner_1_t1736487048 * value)
	{
		___m_AlphaTweenRunner_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaTweenRunner_27), value);
	}

	inline static int32_t get_offset_of_validTemplate_28() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147, ___validTemplate_28)); }
	inline bool get_validTemplate_28() const { return ___validTemplate_28; }
	inline bool* get_address_of_validTemplate_28() { return &___validTemplate_28; }
	inline void set_validTemplate_28(bool value)
	{
		___validTemplate_28 = value;
	}
};

struct TMP_Dropdown_t1768193147_StaticFields
{
public:
	// TMPro.TMP_Dropdown/OptionData TMPro.TMP_Dropdown::s_NoOptionData
	OptionData_t234712921 * ___s_NoOptionData_29;

public:
	inline static int32_t get_offset_of_s_NoOptionData_29() { return static_cast<int32_t>(offsetof(TMP_Dropdown_t1768193147_StaticFields, ___s_NoOptionData_29)); }
	inline OptionData_t234712921 * get_s_NoOptionData_29() const { return ___s_NoOptionData_29; }
	inline OptionData_t234712921 ** get_address_of_s_NoOptionData_29() { return &___s_NoOptionData_29; }
	inline void set_s_NoOptionData_29(OptionData_t234712921 * value)
	{
		___s_NoOptionData_29 = value;
		Il2CppCodeGenWriteBarrier((&___s_NoOptionData_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_DROPDOWN_T1768193147_H
#ifndef TMP_TEXT_T1920000777_H
#define TMP_TEXT_T1920000777_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t1920000777  : public MaskableGraphic_t540192618
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_28;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_29;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t2530419979 * ___m_fontAsset_30;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t2530419979 * ___m_currentFontAsset_31;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_32;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_t193706927 * ___m_sharedMaterial_33;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_t193706927 * ___m_currentMaterial_34;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t627890505* ___m_materialReferences_35;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_t1079703083 * ___m_materialReferenceIndexLookup_36;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_XmlTagStack_1_t3512906015  ___m_materialReferenceStack_37;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_38;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_t3123989686* ___m_fontSharedMaterials_39;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_t193706927 * ___m_fontMaterial_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_t3123989686* ___m_fontMaterials_41;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_42;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t874517518  ___m_fontColor32_43;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t2020392075  ___m_fontColor_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t874517518  ___m_underlineColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t874517518  ___m_strikethroughColor_47;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t874517518  ___m_highlightColor_48;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_49;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_t1602386880  ___m_fontColorGradient_50;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_t1159837347 * ___m_fontColorGradientPreset_51;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_spriteAsset_52;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_53;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_54;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t874517518  ___m_spriteColor_55;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_56;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t874517518  ___m_faceColor_57;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t874517518  ___m_outlineColor_58;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_59;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_60;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_61;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_62;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_XmlTagStack_1_t2735062451  ___m_sizeStack_63;
	// System.Int32 TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_64;
	// System.Int32 TMPro.TMP_Text::m_fontWeightInternal
	int32_t ___m_fontWeightInternal_65;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_fontWeightStack
	TMP_XmlTagStack_1_t2730429967  ___m_fontWeightStack_66;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_67;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_68;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_69;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_70;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_71;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_72;
	// TMPro.FontStyles TMPro.TMP_Text::m_style
	int32_t ___m_style_73;
	// TMPro.TMP_BasicXmlTagStack TMPro.TMP_Text::m_fontStyleStack
	TMP_BasicXmlTagStack_t937156555  ___m_fontStyleStack_74;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_75;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_76;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_77;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_XmlTagStack_1_t2125340843  ___m_lineJustificationStack_78;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_t1172311765* ___m_textContainerLocalCorners_79;
	// System.Boolean TMPro.TMP_Text::m_isAlignmentEnumConverted
	bool ___m_isAlignmentEnumConverted_80;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_81;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_82;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_83;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_84;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_85;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_86;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_87;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_88;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_89;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_90;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_91;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_92;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_93;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_94;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_95;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_96;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_97;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_98;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t1920000777 * ___m_linkedTextComponent_99;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_100;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_101;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_102;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_103;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_104;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_105;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_106;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_107;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_108;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_109;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_110;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_111;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_112;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_113;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_114;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_115;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_116;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_117;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_118;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_119;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_120;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_121;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_122;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_123;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_t2243707581  ___m_margin_124;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_125;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_126;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_127;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_128;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_129;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_t2849466151 * ___m_textInfo_130;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_131;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_132;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_t3275118058 * ___m_transform_133;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t3349966182 * ___m_rectTransform_134;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_135;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_136;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t1356156583 * ___m_mesh_137;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_138;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_t2347923044 * ___m_spriteAnimator_139;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_140;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_141;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_142;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_143;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_144;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_145;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_t2808691390 * ___m_LayoutElement_146;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_147;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_148;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_149;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_150;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_151;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_152;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_153;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_154;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_155;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_156;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_157;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_158;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_159;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_160;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_161;
	// TMPro.TMP_Text/TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_162;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_163;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_164;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_165;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t1328083999* ___m_htmlTag_166;
	// TMPro.XML_TagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	XML_TagAttributeU5BU5D_t573465953* ___m_xmlAttribute_167;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_t577127397* ___m_attributeParameterValues_168;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_169;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_170;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_XmlTagStack_1_t2735062451  ___m_indentStack_171;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_172;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_173;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t2933234003  ___m_FXMatrix_174;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_175;
	// System.Int32[] TMPro.TMP_Text::m_char_buffer
	Int32U5BU5D_t3030399641* ___m_char_buffer_176;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t602810366* ___m_internalCharacterInfo_177;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t1328083999* ___m_input_CharArray_178;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_179;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_180;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t433984875  ___m_SavedWordWrapState_181;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t433984875  ___m_SavedLineState_182;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_183;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_184;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_185;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_186;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_187;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_188;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_189;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_190;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_191;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_192;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_193;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_194;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_195;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_196;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_197;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_t3018556803  ___m_meshExtents_198;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t874517518  ___m_htmlColor_199;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_XmlTagStack_1_t1533070037  ___m_colorStack_200;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_XmlTagStack_1_t1533070037  ___m_underlineColorStack_201;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_XmlTagStack_1_t1533070037  ___m_strikethroughColorStack_202;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_XmlTagStack_1_t1533070037  ___m_highlightColorStack_203;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_t1159837347 * ___m_colorGradientPreset_204;
	// TMPro.TMP_XmlTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_XmlTagStack_1_t1818389866  ___m_colorGradientStack_205;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_206;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_207;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_XmlTagStack_1_t2730429967  ___m_styleStack_208;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_XmlTagStack_1_t2730429967  ___m_actionStack_209;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_210;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_211;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_XmlTagStack_1_t2735062451  ___m_baselineOffsetStack_212;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_213;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_214;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_t2285620223 * ___m_cached_TextElement_215;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Underline_GlyphInfo
	TMP_Glyph_t909793902 * ___m_cached_Underline_GlyphInfo_216;
	// TMPro.TMP_Glyph TMPro.TMP_Text::m_cached_Ellipsis_GlyphInfo
	TMP_Glyph_t909793902 * ___m_cached_Ellipsis_GlyphInfo_217;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_defaultSpriteAsset_218;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_currentSpriteAsset_219;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_220;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_221;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_222;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_223;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_t577127397* ___k_Power_224;

public:
	inline static int32_t get_offset_of_m_text_28() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_text_28)); }
	inline String_t* get_m_text_28() const { return ___m_text_28; }
	inline String_t** get_address_of_m_text_28() { return &___m_text_28; }
	inline void set_m_text_28(String_t* value)
	{
		___m_text_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_28), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_29() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isRightToLeft_29)); }
	inline bool get_m_isRightToLeft_29() const { return ___m_isRightToLeft_29; }
	inline bool* get_address_of_m_isRightToLeft_29() { return &___m_isRightToLeft_29; }
	inline void set_m_isRightToLeft_29(bool value)
	{
		___m_isRightToLeft_29 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_30() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontAsset_30)); }
	inline TMP_FontAsset_t2530419979 * get_m_fontAsset_30() const { return ___m_fontAsset_30; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_m_fontAsset_30() { return &___m_fontAsset_30; }
	inline void set_m_fontAsset_30(TMP_FontAsset_t2530419979 * value)
	{
		___m_fontAsset_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_30), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_31() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentFontAsset_31)); }
	inline TMP_FontAsset_t2530419979 * get_m_currentFontAsset_31() const { return ___m_currentFontAsset_31; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_m_currentFontAsset_31() { return &___m_currentFontAsset_31; }
	inline void set_m_currentFontAsset_31(TMP_FontAsset_t2530419979 * value)
	{
		___m_currentFontAsset_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_31), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_32() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isSDFShader_32)); }
	inline bool get_m_isSDFShader_32() const { return ___m_isSDFShader_32; }
	inline bool* get_address_of_m_isSDFShader_32() { return &___m_isSDFShader_32; }
	inline void set_m_isSDFShader_32(bool value)
	{
		___m_isSDFShader_32 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_33() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_sharedMaterial_33)); }
	inline Material_t193706927 * get_m_sharedMaterial_33() const { return ___m_sharedMaterial_33; }
	inline Material_t193706927 ** get_address_of_m_sharedMaterial_33() { return &___m_sharedMaterial_33; }
	inline void set_m_sharedMaterial_33(Material_t193706927 * value)
	{
		___m_sharedMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_34() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentMaterial_34)); }
	inline Material_t193706927 * get_m_currentMaterial_34() const { return ___m_currentMaterial_34; }
	inline Material_t193706927 ** get_address_of_m_currentMaterial_34() { return &___m_currentMaterial_34; }
	inline void set_m_currentMaterial_34(Material_t193706927 * value)
	{
		___m_currentMaterial_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_34), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_35() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_materialReferences_35)); }
	inline MaterialReferenceU5BU5D_t627890505* get_m_materialReferences_35() const { return ___m_materialReferences_35; }
	inline MaterialReferenceU5BU5D_t627890505** get_address_of_m_materialReferences_35() { return &___m_materialReferences_35; }
	inline void set_m_materialReferences_35(MaterialReferenceU5BU5D_t627890505* value)
	{
		___m_materialReferences_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_35), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_36() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_materialReferenceIndexLookup_36)); }
	inline Dictionary_2_t1079703083 * get_m_materialReferenceIndexLookup_36() const { return ___m_materialReferenceIndexLookup_36; }
	inline Dictionary_2_t1079703083 ** get_address_of_m_materialReferenceIndexLookup_36() { return &___m_materialReferenceIndexLookup_36; }
	inline void set_m_materialReferenceIndexLookup_36(Dictionary_2_t1079703083 * value)
	{
		___m_materialReferenceIndexLookup_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_37() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_materialReferenceStack_37)); }
	inline TMP_XmlTagStack_1_t3512906015  get_m_materialReferenceStack_37() const { return ___m_materialReferenceStack_37; }
	inline TMP_XmlTagStack_1_t3512906015 * get_address_of_m_materialReferenceStack_37() { return &___m_materialReferenceStack_37; }
	inline void set_m_materialReferenceStack_37(TMP_XmlTagStack_1_t3512906015  value)
	{
		___m_materialReferenceStack_37 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_38() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentMaterialIndex_38)); }
	inline int32_t get_m_currentMaterialIndex_38() const { return ___m_currentMaterialIndex_38; }
	inline int32_t* get_address_of_m_currentMaterialIndex_38() { return &___m_currentMaterialIndex_38; }
	inline void set_m_currentMaterialIndex_38(int32_t value)
	{
		___m_currentMaterialIndex_38 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_39() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSharedMaterials_39)); }
	inline MaterialU5BU5D_t3123989686* get_m_fontSharedMaterials_39() const { return ___m_fontSharedMaterials_39; }
	inline MaterialU5BU5D_t3123989686** get_address_of_m_fontSharedMaterials_39() { return &___m_fontSharedMaterials_39; }
	inline void set_m_fontSharedMaterials_39(MaterialU5BU5D_t3123989686* value)
	{
		___m_fontSharedMaterials_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_39), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_40() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontMaterial_40)); }
	inline Material_t193706927 * get_m_fontMaterial_40() const { return ___m_fontMaterial_40; }
	inline Material_t193706927 ** get_address_of_m_fontMaterial_40() { return &___m_fontMaterial_40; }
	inline void set_m_fontMaterial_40(Material_t193706927 * value)
	{
		___m_fontMaterial_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_40), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontMaterials_41)); }
	inline MaterialU5BU5D_t3123989686* get_m_fontMaterials_41() const { return ___m_fontMaterials_41; }
	inline MaterialU5BU5D_t3123989686** get_address_of_m_fontMaterials_41() { return &___m_fontMaterials_41; }
	inline void set_m_fontMaterials_41(MaterialU5BU5D_t3123989686* value)
	{
		___m_fontMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_42() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isMaterialDirty_42)); }
	inline bool get_m_isMaterialDirty_42() const { return ___m_isMaterialDirty_42; }
	inline bool* get_address_of_m_isMaterialDirty_42() { return &___m_isMaterialDirty_42; }
	inline void set_m_isMaterialDirty_42(bool value)
	{
		___m_isMaterialDirty_42 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_43() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontColor32_43)); }
	inline Color32_t874517518  get_m_fontColor32_43() const { return ___m_fontColor32_43; }
	inline Color32_t874517518 * get_address_of_m_fontColor32_43() { return &___m_fontColor32_43; }
	inline void set_m_fontColor32_43(Color32_t874517518  value)
	{
		___m_fontColor32_43 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_44() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontColor_44)); }
	inline Color_t2020392075  get_m_fontColor_44() const { return ___m_fontColor_44; }
	inline Color_t2020392075 * get_address_of_m_fontColor_44() { return &___m_fontColor_44; }
	inline void set_m_fontColor_44(Color_t2020392075  value)
	{
		___m_fontColor_44 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_underlineColor_46)); }
	inline Color32_t874517518  get_m_underlineColor_46() const { return ___m_underlineColor_46; }
	inline Color32_t874517518 * get_address_of_m_underlineColor_46() { return &___m_underlineColor_46; }
	inline void set_m_underlineColor_46(Color32_t874517518  value)
	{
		___m_underlineColor_46 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_47() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_strikethroughColor_47)); }
	inline Color32_t874517518  get_m_strikethroughColor_47() const { return ___m_strikethroughColor_47; }
	inline Color32_t874517518 * get_address_of_m_strikethroughColor_47() { return &___m_strikethroughColor_47; }
	inline void set_m_strikethroughColor_47(Color32_t874517518  value)
	{
		___m_strikethroughColor_47 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_highlightColor_48)); }
	inline Color32_t874517518  get_m_highlightColor_48() const { return ___m_highlightColor_48; }
	inline Color32_t874517518 * get_address_of_m_highlightColor_48() { return &___m_highlightColor_48; }
	inline void set_m_highlightColor_48(Color32_t874517518  value)
	{
		___m_highlightColor_48 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_49() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableVertexGradient_49)); }
	inline bool get_m_enableVertexGradient_49() const { return ___m_enableVertexGradient_49; }
	inline bool* get_address_of_m_enableVertexGradient_49() { return &___m_enableVertexGradient_49; }
	inline void set_m_enableVertexGradient_49(bool value)
	{
		___m_enableVertexGradient_49 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_50() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontColorGradient_50)); }
	inline VertexGradient_t1602386880  get_m_fontColorGradient_50() const { return ___m_fontColorGradient_50; }
	inline VertexGradient_t1602386880 * get_address_of_m_fontColorGradient_50() { return &___m_fontColorGradient_50; }
	inline void set_m_fontColorGradient_50(VertexGradient_t1602386880  value)
	{
		___m_fontColorGradient_50 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_51() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontColorGradientPreset_51)); }
	inline TMP_ColorGradient_t1159837347 * get_m_fontColorGradientPreset_51() const { return ___m_fontColorGradientPreset_51; }
	inline TMP_ColorGradient_t1159837347 ** get_address_of_m_fontColorGradientPreset_51() { return &___m_fontColorGradientPreset_51; }
	inline void set_m_fontColorGradientPreset_51(TMP_ColorGradient_t1159837347 * value)
	{
		___m_fontColorGradientPreset_51 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_51), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_52() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteAsset_52)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_spriteAsset_52() const { return ___m_spriteAsset_52; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_spriteAsset_52() { return &___m_spriteAsset_52; }
	inline void set_m_spriteAsset_52(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_spriteAsset_52 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_52), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_53() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_tintAllSprites_53)); }
	inline bool get_m_tintAllSprites_53() const { return ___m_tintAllSprites_53; }
	inline bool* get_address_of_m_tintAllSprites_53() { return &___m_tintAllSprites_53; }
	inline void set_m_tintAllSprites_53(bool value)
	{
		___m_tintAllSprites_53 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_54() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_tintSprite_54)); }
	inline bool get_m_tintSprite_54() const { return ___m_tintSprite_54; }
	inline bool* get_address_of_m_tintSprite_54() { return &___m_tintSprite_54; }
	inline void set_m_tintSprite_54(bool value)
	{
		___m_tintSprite_54 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_55() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteColor_55)); }
	inline Color32_t874517518  get_m_spriteColor_55() const { return ___m_spriteColor_55; }
	inline Color32_t874517518 * get_address_of_m_spriteColor_55() { return &___m_spriteColor_55; }
	inline void set_m_spriteColor_55(Color32_t874517518  value)
	{
		___m_spriteColor_55 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_56() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_overrideHtmlColors_56)); }
	inline bool get_m_overrideHtmlColors_56() const { return ___m_overrideHtmlColors_56; }
	inline bool* get_address_of_m_overrideHtmlColors_56() { return &___m_overrideHtmlColors_56; }
	inline void set_m_overrideHtmlColors_56(bool value)
	{
		___m_overrideHtmlColors_56 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_57() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_faceColor_57)); }
	inline Color32_t874517518  get_m_faceColor_57() const { return ___m_faceColor_57; }
	inline Color32_t874517518 * get_address_of_m_faceColor_57() { return &___m_faceColor_57; }
	inline void set_m_faceColor_57(Color32_t874517518  value)
	{
		___m_faceColor_57 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_58() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_outlineColor_58)); }
	inline Color32_t874517518  get_m_outlineColor_58() const { return ___m_outlineColor_58; }
	inline Color32_t874517518 * get_address_of_m_outlineColor_58() { return &___m_outlineColor_58; }
	inline void set_m_outlineColor_58(Color32_t874517518  value)
	{
		___m_outlineColor_58 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_59() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_outlineWidth_59)); }
	inline float get_m_outlineWidth_59() const { return ___m_outlineWidth_59; }
	inline float* get_address_of_m_outlineWidth_59() { return &___m_outlineWidth_59; }
	inline void set_m_outlineWidth_59(float value)
	{
		___m_outlineWidth_59 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_60() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSize_60)); }
	inline float get_m_fontSize_60() const { return ___m_fontSize_60; }
	inline float* get_address_of_m_fontSize_60() { return &___m_fontSize_60; }
	inline void set_m_fontSize_60(float value)
	{
		___m_fontSize_60 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_61() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentFontSize_61)); }
	inline float get_m_currentFontSize_61() const { return ___m_currentFontSize_61; }
	inline float* get_address_of_m_currentFontSize_61() { return &___m_currentFontSize_61; }
	inline void set_m_currentFontSize_61(float value)
	{
		___m_currentFontSize_61 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_62() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSizeBase_62)); }
	inline float get_m_fontSizeBase_62() const { return ___m_fontSizeBase_62; }
	inline float* get_address_of_m_fontSizeBase_62() { return &___m_fontSizeBase_62; }
	inline void set_m_fontSizeBase_62(float value)
	{
		___m_fontSizeBase_62 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_63() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_sizeStack_63)); }
	inline TMP_XmlTagStack_1_t2735062451  get_m_sizeStack_63() const { return ___m_sizeStack_63; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_m_sizeStack_63() { return &___m_sizeStack_63; }
	inline void set_m_sizeStack_63(TMP_XmlTagStack_1_t2735062451  value)
	{
		___m_sizeStack_63 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_64() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontWeight_64)); }
	inline int32_t get_m_fontWeight_64() const { return ___m_fontWeight_64; }
	inline int32_t* get_address_of_m_fontWeight_64() { return &___m_fontWeight_64; }
	inline void set_m_fontWeight_64(int32_t value)
	{
		___m_fontWeight_64 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightInternal_65() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontWeightInternal_65)); }
	inline int32_t get_m_fontWeightInternal_65() const { return ___m_fontWeightInternal_65; }
	inline int32_t* get_address_of_m_fontWeightInternal_65() { return &___m_fontWeightInternal_65; }
	inline void set_m_fontWeightInternal_65(int32_t value)
	{
		___m_fontWeightInternal_65 = value;
	}

	inline static int32_t get_offset_of_m_fontWeightStack_66() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontWeightStack_66)); }
	inline TMP_XmlTagStack_1_t2730429967  get_m_fontWeightStack_66() const { return ___m_fontWeightStack_66; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_m_fontWeightStack_66() { return &___m_fontWeightStack_66; }
	inline void set_m_fontWeightStack_66(TMP_XmlTagStack_1_t2730429967  value)
	{
		___m_fontWeightStack_66 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_67() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableAutoSizing_67)); }
	inline bool get_m_enableAutoSizing_67() const { return ___m_enableAutoSizing_67; }
	inline bool* get_address_of_m_enableAutoSizing_67() { return &___m_enableAutoSizing_67; }
	inline void set_m_enableAutoSizing_67(bool value)
	{
		___m_enableAutoSizing_67 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_68() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxFontSize_68)); }
	inline float get_m_maxFontSize_68() const { return ___m_maxFontSize_68; }
	inline float* get_address_of_m_maxFontSize_68() { return &___m_maxFontSize_68; }
	inline void set_m_maxFontSize_68(float value)
	{
		___m_maxFontSize_68 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_69() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_minFontSize_69)); }
	inline float get_m_minFontSize_69() const { return ___m_minFontSize_69; }
	inline float* get_address_of_m_minFontSize_69() { return &___m_minFontSize_69; }
	inline void set_m_minFontSize_69(float value)
	{
		___m_minFontSize_69 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_70() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSizeMin_70)); }
	inline float get_m_fontSizeMin_70() const { return ___m_fontSizeMin_70; }
	inline float* get_address_of_m_fontSizeMin_70() { return &___m_fontSizeMin_70; }
	inline void set_m_fontSizeMin_70(float value)
	{
		___m_fontSizeMin_70 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_71() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontSizeMax_71)); }
	inline float get_m_fontSizeMax_71() const { return ___m_fontSizeMax_71; }
	inline float* get_address_of_m_fontSizeMax_71() { return &___m_fontSizeMax_71; }
	inline void set_m_fontSizeMax_71(float value)
	{
		___m_fontSizeMax_71 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_72() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontStyle_72)); }
	inline int32_t get_m_fontStyle_72() const { return ___m_fontStyle_72; }
	inline int32_t* get_address_of_m_fontStyle_72() { return &___m_fontStyle_72; }
	inline void set_m_fontStyle_72(int32_t value)
	{
		___m_fontStyle_72 = value;
	}

	inline static int32_t get_offset_of_m_style_73() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_style_73)); }
	inline int32_t get_m_style_73() const { return ___m_style_73; }
	inline int32_t* get_address_of_m_style_73() { return &___m_style_73; }
	inline void set_m_style_73(int32_t value)
	{
		___m_style_73 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_74() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontStyleStack_74)); }
	inline TMP_BasicXmlTagStack_t937156555  get_m_fontStyleStack_74() const { return ___m_fontStyleStack_74; }
	inline TMP_BasicXmlTagStack_t937156555 * get_address_of_m_fontStyleStack_74() { return &___m_fontStyleStack_74; }
	inline void set_m_fontStyleStack_74(TMP_BasicXmlTagStack_t937156555  value)
	{
		___m_fontStyleStack_74 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_75() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isUsingBold_75)); }
	inline bool get_m_isUsingBold_75() const { return ___m_isUsingBold_75; }
	inline bool* get_address_of_m_isUsingBold_75() { return &___m_isUsingBold_75; }
	inline void set_m_isUsingBold_75(bool value)
	{
		___m_isUsingBold_75 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_76() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_textAlignment_76)); }
	inline int32_t get_m_textAlignment_76() const { return ___m_textAlignment_76; }
	inline int32_t* get_address_of_m_textAlignment_76() { return &___m_textAlignment_76; }
	inline void set_m_textAlignment_76(int32_t value)
	{
		___m_textAlignment_76 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_77() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineJustification_77)); }
	inline int32_t get_m_lineJustification_77() const { return ___m_lineJustification_77; }
	inline int32_t* get_address_of_m_lineJustification_77() { return &___m_lineJustification_77; }
	inline void set_m_lineJustification_77(int32_t value)
	{
		___m_lineJustification_77 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_78() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineJustificationStack_78)); }
	inline TMP_XmlTagStack_1_t2125340843  get_m_lineJustificationStack_78() const { return ___m_lineJustificationStack_78; }
	inline TMP_XmlTagStack_1_t2125340843 * get_address_of_m_lineJustificationStack_78() { return &___m_lineJustificationStack_78; }
	inline void set_m_lineJustificationStack_78(TMP_XmlTagStack_1_t2125340843  value)
	{
		___m_lineJustificationStack_78 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_79() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_textContainerLocalCorners_79)); }
	inline Vector3U5BU5D_t1172311765* get_m_textContainerLocalCorners_79() const { return ___m_textContainerLocalCorners_79; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_textContainerLocalCorners_79() { return &___m_textContainerLocalCorners_79; }
	inline void set_m_textContainerLocalCorners_79(Vector3U5BU5D_t1172311765* value)
	{
		___m_textContainerLocalCorners_79 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_79), value);
	}

	inline static int32_t get_offset_of_m_isAlignmentEnumConverted_80() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isAlignmentEnumConverted_80)); }
	inline bool get_m_isAlignmentEnumConverted_80() const { return ___m_isAlignmentEnumConverted_80; }
	inline bool* get_address_of_m_isAlignmentEnumConverted_80() { return &___m_isAlignmentEnumConverted_80; }
	inline void set_m_isAlignmentEnumConverted_80(bool value)
	{
		___m_isAlignmentEnumConverted_80 = value;
	}

	inline static int32_t get_offset_of_m_characterSpacing_81() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_characterSpacing_81)); }
	inline float get_m_characterSpacing_81() const { return ___m_characterSpacing_81; }
	inline float* get_address_of_m_characterSpacing_81() { return &___m_characterSpacing_81; }
	inline void set_m_characterSpacing_81(float value)
	{
		___m_characterSpacing_81 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_82() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_cSpacing_82)); }
	inline float get_m_cSpacing_82() const { return ___m_cSpacing_82; }
	inline float* get_address_of_m_cSpacing_82() { return &___m_cSpacing_82; }
	inline void set_m_cSpacing_82(float value)
	{
		___m_cSpacing_82 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_83() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_monoSpacing_83)); }
	inline float get_m_monoSpacing_83() const { return ___m_monoSpacing_83; }
	inline float* get_address_of_m_monoSpacing_83() { return &___m_monoSpacing_83; }
	inline void set_m_monoSpacing_83(float value)
	{
		___m_monoSpacing_83 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_wordSpacing_84)); }
	inline float get_m_wordSpacing_84() const { return ___m_wordSpacing_84; }
	inline float* get_address_of_m_wordSpacing_84() { return &___m_wordSpacing_84; }
	inline void set_m_wordSpacing_84(float value)
	{
		___m_wordSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineSpacing_85)); }
	inline float get_m_lineSpacing_85() const { return ___m_lineSpacing_85; }
	inline float* get_address_of_m_lineSpacing_85() { return &___m_lineSpacing_85; }
	inline void set_m_lineSpacing_85(float value)
	{
		___m_lineSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_86() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineSpacingDelta_86)); }
	inline float get_m_lineSpacingDelta_86() const { return ___m_lineSpacingDelta_86; }
	inline float* get_address_of_m_lineSpacingDelta_86() { return &___m_lineSpacingDelta_86; }
	inline void set_m_lineSpacingDelta_86(float value)
	{
		___m_lineSpacingDelta_86 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_87() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineHeight_87)); }
	inline float get_m_lineHeight_87() const { return ___m_lineHeight_87; }
	inline float* get_address_of_m_lineHeight_87() { return &___m_lineHeight_87; }
	inline void set_m_lineHeight_87(float value)
	{
		___m_lineHeight_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_88() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineSpacingMax_88)); }
	inline float get_m_lineSpacingMax_88() const { return ___m_lineSpacingMax_88; }
	inline float* get_address_of_m_lineSpacingMax_88() { return &___m_lineSpacingMax_88; }
	inline void set_m_lineSpacingMax_88(float value)
	{
		___m_lineSpacingMax_88 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_89() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_paragraphSpacing_89)); }
	inline float get_m_paragraphSpacing_89() const { return ___m_paragraphSpacing_89; }
	inline float* get_address_of_m_paragraphSpacing_89() { return &___m_paragraphSpacing_89; }
	inline void set_m_paragraphSpacing_89(float value)
	{
		___m_paragraphSpacing_89 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_90() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_charWidthMaxAdj_90)); }
	inline float get_m_charWidthMaxAdj_90() const { return ___m_charWidthMaxAdj_90; }
	inline float* get_address_of_m_charWidthMaxAdj_90() { return &___m_charWidthMaxAdj_90; }
	inline void set_m_charWidthMaxAdj_90(float value)
	{
		___m_charWidthMaxAdj_90 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_91() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_charWidthAdjDelta_91)); }
	inline float get_m_charWidthAdjDelta_91() const { return ___m_charWidthAdjDelta_91; }
	inline float* get_address_of_m_charWidthAdjDelta_91() { return &___m_charWidthAdjDelta_91; }
	inline void set_m_charWidthAdjDelta_91(float value)
	{
		___m_charWidthAdjDelta_91 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_92() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableWordWrapping_92)); }
	inline bool get_m_enableWordWrapping_92() const { return ___m_enableWordWrapping_92; }
	inline bool* get_address_of_m_enableWordWrapping_92() { return &___m_enableWordWrapping_92; }
	inline void set_m_enableWordWrapping_92(bool value)
	{
		___m_enableWordWrapping_92 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_93() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isCharacterWrappingEnabled_93)); }
	inline bool get_m_isCharacterWrappingEnabled_93() const { return ___m_isCharacterWrappingEnabled_93; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_93() { return &___m_isCharacterWrappingEnabled_93; }
	inline void set_m_isCharacterWrappingEnabled_93(bool value)
	{
		___m_isCharacterWrappingEnabled_93 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_94() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isNonBreakingSpace_94)); }
	inline bool get_m_isNonBreakingSpace_94() const { return ___m_isNonBreakingSpace_94; }
	inline bool* get_address_of_m_isNonBreakingSpace_94() { return &___m_isNonBreakingSpace_94; }
	inline void set_m_isNonBreakingSpace_94(bool value)
	{
		___m_isNonBreakingSpace_94 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_95() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isIgnoringAlignment_95)); }
	inline bool get_m_isIgnoringAlignment_95() const { return ___m_isIgnoringAlignment_95; }
	inline bool* get_address_of_m_isIgnoringAlignment_95() { return &___m_isIgnoringAlignment_95; }
	inline void set_m_isIgnoringAlignment_95(bool value)
	{
		___m_isIgnoringAlignment_95 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_96() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_wordWrappingRatios_96)); }
	inline float get_m_wordWrappingRatios_96() const { return ___m_wordWrappingRatios_96; }
	inline float* get_address_of_m_wordWrappingRatios_96() { return &___m_wordWrappingRatios_96; }
	inline void set_m_wordWrappingRatios_96(float value)
	{
		___m_wordWrappingRatios_96 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_97() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_overflowMode_97)); }
	inline int32_t get_m_overflowMode_97() const { return ___m_overflowMode_97; }
	inline int32_t* get_address_of_m_overflowMode_97() { return &___m_overflowMode_97; }
	inline void set_m_overflowMode_97(int32_t value)
	{
		___m_overflowMode_97 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_98() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_firstOverflowCharacterIndex_98)); }
	inline int32_t get_m_firstOverflowCharacterIndex_98() const { return ___m_firstOverflowCharacterIndex_98; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_98() { return &___m_firstOverflowCharacterIndex_98; }
	inline void set_m_firstOverflowCharacterIndex_98(int32_t value)
	{
		___m_firstOverflowCharacterIndex_98 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_99() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_linkedTextComponent_99)); }
	inline TMP_Text_t1920000777 * get_m_linkedTextComponent_99() const { return ___m_linkedTextComponent_99; }
	inline TMP_Text_t1920000777 ** get_address_of_m_linkedTextComponent_99() { return &___m_linkedTextComponent_99; }
	inline void set_m_linkedTextComponent_99(TMP_Text_t1920000777 * value)
	{
		___m_linkedTextComponent_99 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_99), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_100() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isLinkedTextComponent_100)); }
	inline bool get_m_isLinkedTextComponent_100() const { return ___m_isLinkedTextComponent_100; }
	inline bool* get_address_of_m_isLinkedTextComponent_100() { return &___m_isLinkedTextComponent_100; }
	inline void set_m_isLinkedTextComponent_100(bool value)
	{
		___m_isLinkedTextComponent_100 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_101() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isTextTruncated_101)); }
	inline bool get_m_isTextTruncated_101() const { return ___m_isTextTruncated_101; }
	inline bool* get_address_of_m_isTextTruncated_101() { return &___m_isTextTruncated_101; }
	inline void set_m_isTextTruncated_101(bool value)
	{
		___m_isTextTruncated_101 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_102() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableKerning_102)); }
	inline bool get_m_enableKerning_102() const { return ___m_enableKerning_102; }
	inline bool* get_address_of_m_enableKerning_102() { return &___m_enableKerning_102; }
	inline void set_m_enableKerning_102(bool value)
	{
		___m_enableKerning_102 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_103() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_enableExtraPadding_103)); }
	inline bool get_m_enableExtraPadding_103() const { return ___m_enableExtraPadding_103; }
	inline bool* get_address_of_m_enableExtraPadding_103() { return &___m_enableExtraPadding_103; }
	inline void set_m_enableExtraPadding_103(bool value)
	{
		___m_enableExtraPadding_103 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_104() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___checkPaddingRequired_104)); }
	inline bool get_checkPaddingRequired_104() const { return ___checkPaddingRequired_104; }
	inline bool* get_address_of_checkPaddingRequired_104() { return &___checkPaddingRequired_104; }
	inline void set_checkPaddingRequired_104(bool value)
	{
		___checkPaddingRequired_104 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_105() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isRichText_105)); }
	inline bool get_m_isRichText_105() const { return ___m_isRichText_105; }
	inline bool* get_address_of_m_isRichText_105() { return &___m_isRichText_105; }
	inline void set_m_isRichText_105(bool value)
	{
		___m_isRichText_105 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_106() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_parseCtrlCharacters_106)); }
	inline bool get_m_parseCtrlCharacters_106() const { return ___m_parseCtrlCharacters_106; }
	inline bool* get_address_of_m_parseCtrlCharacters_106() { return &___m_parseCtrlCharacters_106; }
	inline void set_m_parseCtrlCharacters_106(bool value)
	{
		___m_parseCtrlCharacters_106 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_107() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isOverlay_107)); }
	inline bool get_m_isOverlay_107() const { return ___m_isOverlay_107; }
	inline bool* get_address_of_m_isOverlay_107() { return &___m_isOverlay_107; }
	inline void set_m_isOverlay_107(bool value)
	{
		___m_isOverlay_107 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_108() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isOrthographic_108)); }
	inline bool get_m_isOrthographic_108() const { return ___m_isOrthographic_108; }
	inline bool* get_address_of_m_isOrthographic_108() { return &___m_isOrthographic_108; }
	inline void set_m_isOrthographic_108(bool value)
	{
		___m_isOrthographic_108 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_109() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isCullingEnabled_109)); }
	inline bool get_m_isCullingEnabled_109() const { return ___m_isCullingEnabled_109; }
	inline bool* get_address_of_m_isCullingEnabled_109() { return &___m_isCullingEnabled_109; }
	inline void set_m_isCullingEnabled_109(bool value)
	{
		___m_isCullingEnabled_109 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_110() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_ignoreRectMaskCulling_110)); }
	inline bool get_m_ignoreRectMaskCulling_110() const { return ___m_ignoreRectMaskCulling_110; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_110() { return &___m_ignoreRectMaskCulling_110; }
	inline void set_m_ignoreRectMaskCulling_110(bool value)
	{
		___m_ignoreRectMaskCulling_110 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_111() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_ignoreCulling_111)); }
	inline bool get_m_ignoreCulling_111() const { return ___m_ignoreCulling_111; }
	inline bool* get_address_of_m_ignoreCulling_111() { return &___m_ignoreCulling_111; }
	inline void set_m_ignoreCulling_111(bool value)
	{
		___m_ignoreCulling_111 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_112() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_horizontalMapping_112)); }
	inline int32_t get_m_horizontalMapping_112() const { return ___m_horizontalMapping_112; }
	inline int32_t* get_address_of_m_horizontalMapping_112() { return &___m_horizontalMapping_112; }
	inline void set_m_horizontalMapping_112(int32_t value)
	{
		___m_horizontalMapping_112 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_113() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_verticalMapping_113)); }
	inline int32_t get_m_verticalMapping_113() const { return ___m_verticalMapping_113; }
	inline int32_t* get_address_of_m_verticalMapping_113() { return &___m_verticalMapping_113; }
	inline void set_m_verticalMapping_113(int32_t value)
	{
		___m_verticalMapping_113 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_114() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_uvLineOffset_114)); }
	inline float get_m_uvLineOffset_114() const { return ___m_uvLineOffset_114; }
	inline float* get_address_of_m_uvLineOffset_114() { return &___m_uvLineOffset_114; }
	inline void set_m_uvLineOffset_114(float value)
	{
		___m_uvLineOffset_114 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_115() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_renderMode_115)); }
	inline int32_t get_m_renderMode_115() const { return ___m_renderMode_115; }
	inline int32_t* get_address_of_m_renderMode_115() { return &___m_renderMode_115; }
	inline void set_m_renderMode_115(int32_t value)
	{
		___m_renderMode_115 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_116() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_geometrySortingOrder_116)); }
	inline int32_t get_m_geometrySortingOrder_116() const { return ___m_geometrySortingOrder_116; }
	inline int32_t* get_address_of_m_geometrySortingOrder_116() { return &___m_geometrySortingOrder_116; }
	inline void set_m_geometrySortingOrder_116(int32_t value)
	{
		___m_geometrySortingOrder_116 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_117() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_firstVisibleCharacter_117)); }
	inline int32_t get_m_firstVisibleCharacter_117() const { return ___m_firstVisibleCharacter_117; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_117() { return &___m_firstVisibleCharacter_117; }
	inline void set_m_firstVisibleCharacter_117(int32_t value)
	{
		___m_firstVisibleCharacter_117 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_118() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxVisibleCharacters_118)); }
	inline int32_t get_m_maxVisibleCharacters_118() const { return ___m_maxVisibleCharacters_118; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_118() { return &___m_maxVisibleCharacters_118; }
	inline void set_m_maxVisibleCharacters_118(int32_t value)
	{
		___m_maxVisibleCharacters_118 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_119() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxVisibleWords_119)); }
	inline int32_t get_m_maxVisibleWords_119() const { return ___m_maxVisibleWords_119; }
	inline int32_t* get_address_of_m_maxVisibleWords_119() { return &___m_maxVisibleWords_119; }
	inline void set_m_maxVisibleWords_119(int32_t value)
	{
		___m_maxVisibleWords_119 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_120() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxVisibleLines_120)); }
	inline int32_t get_m_maxVisibleLines_120() const { return ___m_maxVisibleLines_120; }
	inline int32_t* get_address_of_m_maxVisibleLines_120() { return &___m_maxVisibleLines_120; }
	inline void set_m_maxVisibleLines_120(int32_t value)
	{
		___m_maxVisibleLines_120 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_121() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_useMaxVisibleDescender_121)); }
	inline bool get_m_useMaxVisibleDescender_121() const { return ___m_useMaxVisibleDescender_121; }
	inline bool* get_address_of_m_useMaxVisibleDescender_121() { return &___m_useMaxVisibleDescender_121; }
	inline void set_m_useMaxVisibleDescender_121(bool value)
	{
		___m_useMaxVisibleDescender_121 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_122() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_pageToDisplay_122)); }
	inline int32_t get_m_pageToDisplay_122() const { return ___m_pageToDisplay_122; }
	inline int32_t* get_address_of_m_pageToDisplay_122() { return &___m_pageToDisplay_122; }
	inline void set_m_pageToDisplay_122(int32_t value)
	{
		___m_pageToDisplay_122 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_123() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isNewPage_123)); }
	inline bool get_m_isNewPage_123() const { return ___m_isNewPage_123; }
	inline bool* get_address_of_m_isNewPage_123() { return &___m_isNewPage_123; }
	inline void set_m_isNewPage_123(bool value)
	{
		___m_isNewPage_123 = value;
	}

	inline static int32_t get_offset_of_m_margin_124() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_margin_124)); }
	inline Vector4_t2243707581  get_m_margin_124() const { return ___m_margin_124; }
	inline Vector4_t2243707581 * get_address_of_m_margin_124() { return &___m_margin_124; }
	inline void set_m_margin_124(Vector4_t2243707581  value)
	{
		___m_margin_124 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_125() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_marginLeft_125)); }
	inline float get_m_marginLeft_125() const { return ___m_marginLeft_125; }
	inline float* get_address_of_m_marginLeft_125() { return &___m_marginLeft_125; }
	inline void set_m_marginLeft_125(float value)
	{
		___m_marginLeft_125 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_126() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_marginRight_126)); }
	inline float get_m_marginRight_126() const { return ___m_marginRight_126; }
	inline float* get_address_of_m_marginRight_126() { return &___m_marginRight_126; }
	inline void set_m_marginRight_126(float value)
	{
		___m_marginRight_126 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_127() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_marginWidth_127)); }
	inline float get_m_marginWidth_127() const { return ___m_marginWidth_127; }
	inline float* get_address_of_m_marginWidth_127() { return &___m_marginWidth_127; }
	inline void set_m_marginWidth_127(float value)
	{
		___m_marginWidth_127 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_128() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_marginHeight_128)); }
	inline float get_m_marginHeight_128() const { return ___m_marginHeight_128; }
	inline float* get_address_of_m_marginHeight_128() { return &___m_marginHeight_128; }
	inline void set_m_marginHeight_128(float value)
	{
		___m_marginHeight_128 = value;
	}

	inline static int32_t get_offset_of_m_width_129() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_width_129)); }
	inline float get_m_width_129() const { return ___m_width_129; }
	inline float* get_address_of_m_width_129() { return &___m_width_129; }
	inline void set_m_width_129(float value)
	{
		___m_width_129 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_130() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_textInfo_130)); }
	inline TMP_TextInfo_t2849466151 * get_m_textInfo_130() const { return ___m_textInfo_130; }
	inline TMP_TextInfo_t2849466151 ** get_address_of_m_textInfo_130() { return &___m_textInfo_130; }
	inline void set_m_textInfo_130(TMP_TextInfo_t2849466151 * value)
	{
		___m_textInfo_130 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_130), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_131() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_havePropertiesChanged_131)); }
	inline bool get_m_havePropertiesChanged_131() const { return ___m_havePropertiesChanged_131; }
	inline bool* get_address_of_m_havePropertiesChanged_131() { return &___m_havePropertiesChanged_131; }
	inline void set_m_havePropertiesChanged_131(bool value)
	{
		___m_havePropertiesChanged_131 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_132() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isUsingLegacyAnimationComponent_132)); }
	inline bool get_m_isUsingLegacyAnimationComponent_132() const { return ___m_isUsingLegacyAnimationComponent_132; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_132() { return &___m_isUsingLegacyAnimationComponent_132; }
	inline void set_m_isUsingLegacyAnimationComponent_132(bool value)
	{
		___m_isUsingLegacyAnimationComponent_132 = value;
	}

	inline static int32_t get_offset_of_m_transform_133() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_transform_133)); }
	inline Transform_t3275118058 * get_m_transform_133() const { return ___m_transform_133; }
	inline Transform_t3275118058 ** get_address_of_m_transform_133() { return &___m_transform_133; }
	inline void set_m_transform_133(Transform_t3275118058 * value)
	{
		___m_transform_133 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_133), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_134() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_rectTransform_134)); }
	inline RectTransform_t3349966182 * get_m_rectTransform_134() const { return ___m_rectTransform_134; }
	inline RectTransform_t3349966182 ** get_address_of_m_rectTransform_134() { return &___m_rectTransform_134; }
	inline void set_m_rectTransform_134(RectTransform_t3349966182 * value)
	{
		___m_rectTransform_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_134), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___U3CautoSizeTextContainerU3Ek__BackingField_135)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_135() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_135() { return &___U3CautoSizeTextContainerU3Ek__BackingField_135; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_135(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_135 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_136() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_autoSizeTextContainer_136)); }
	inline bool get_m_autoSizeTextContainer_136() const { return ___m_autoSizeTextContainer_136; }
	inline bool* get_address_of_m_autoSizeTextContainer_136() { return &___m_autoSizeTextContainer_136; }
	inline void set_m_autoSizeTextContainer_136(bool value)
	{
		___m_autoSizeTextContainer_136 = value;
	}

	inline static int32_t get_offset_of_m_mesh_137() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_mesh_137)); }
	inline Mesh_t1356156583 * get_m_mesh_137() const { return ___m_mesh_137; }
	inline Mesh_t1356156583 ** get_address_of_m_mesh_137() { return &___m_mesh_137; }
	inline void set_m_mesh_137(Mesh_t1356156583 * value)
	{
		___m_mesh_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_137), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_138() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isVolumetricText_138)); }
	inline bool get_m_isVolumetricText_138() const { return ___m_isVolumetricText_138; }
	inline bool* get_address_of_m_isVolumetricText_138() { return &___m_isVolumetricText_138; }
	inline void set_m_isVolumetricText_138(bool value)
	{
		___m_isVolumetricText_138 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_139() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteAnimator_139)); }
	inline TMP_SpriteAnimator_t2347923044 * get_m_spriteAnimator_139() const { return ___m_spriteAnimator_139; }
	inline TMP_SpriteAnimator_t2347923044 ** get_address_of_m_spriteAnimator_139() { return &___m_spriteAnimator_139; }
	inline void set_m_spriteAnimator_139(TMP_SpriteAnimator_t2347923044 * value)
	{
		___m_spriteAnimator_139 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_139), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_140() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_flexibleHeight_140)); }
	inline float get_m_flexibleHeight_140() const { return ___m_flexibleHeight_140; }
	inline float* get_address_of_m_flexibleHeight_140() { return &___m_flexibleHeight_140; }
	inline void set_m_flexibleHeight_140(float value)
	{
		___m_flexibleHeight_140 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_141() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_flexibleWidth_141)); }
	inline float get_m_flexibleWidth_141() const { return ___m_flexibleWidth_141; }
	inline float* get_address_of_m_flexibleWidth_141() { return &___m_flexibleWidth_141; }
	inline void set_m_flexibleWidth_141(float value)
	{
		___m_flexibleWidth_141 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_142() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_minWidth_142)); }
	inline float get_m_minWidth_142() const { return ___m_minWidth_142; }
	inline float* get_address_of_m_minWidth_142() { return &___m_minWidth_142; }
	inline void set_m_minWidth_142(float value)
	{
		___m_minWidth_142 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_143() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_minHeight_143)); }
	inline float get_m_minHeight_143() const { return ___m_minHeight_143; }
	inline float* get_address_of_m_minHeight_143() { return &___m_minHeight_143; }
	inline void set_m_minHeight_143(float value)
	{
		___m_minHeight_143 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_144() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxWidth_144)); }
	inline float get_m_maxWidth_144() const { return ___m_maxWidth_144; }
	inline float* get_address_of_m_maxWidth_144() { return &___m_maxWidth_144; }
	inline void set_m_maxWidth_144(float value)
	{
		___m_maxWidth_144 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_145() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxHeight_145)); }
	inline float get_m_maxHeight_145() const { return ___m_maxHeight_145; }
	inline float* get_address_of_m_maxHeight_145() { return &___m_maxHeight_145; }
	inline void set_m_maxHeight_145(float value)
	{
		___m_maxHeight_145 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_146() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_LayoutElement_146)); }
	inline LayoutElement_t2808691390 * get_m_LayoutElement_146() const { return ___m_LayoutElement_146; }
	inline LayoutElement_t2808691390 ** get_address_of_m_LayoutElement_146() { return &___m_LayoutElement_146; }
	inline void set_m_LayoutElement_146(LayoutElement_t2808691390 * value)
	{
		___m_LayoutElement_146 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_146), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_147() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_preferredWidth_147)); }
	inline float get_m_preferredWidth_147() const { return ___m_preferredWidth_147; }
	inline float* get_address_of_m_preferredWidth_147() { return &___m_preferredWidth_147; }
	inline void set_m_preferredWidth_147(float value)
	{
		___m_preferredWidth_147 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_148() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_renderedWidth_148)); }
	inline float get_m_renderedWidth_148() const { return ___m_renderedWidth_148; }
	inline float* get_address_of_m_renderedWidth_148() { return &___m_renderedWidth_148; }
	inline void set_m_renderedWidth_148(float value)
	{
		___m_renderedWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_149() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isPreferredWidthDirty_149)); }
	inline bool get_m_isPreferredWidthDirty_149() const { return ___m_isPreferredWidthDirty_149; }
	inline bool* get_address_of_m_isPreferredWidthDirty_149() { return &___m_isPreferredWidthDirty_149; }
	inline void set_m_isPreferredWidthDirty_149(bool value)
	{
		___m_isPreferredWidthDirty_149 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_150() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_preferredHeight_150)); }
	inline float get_m_preferredHeight_150() const { return ___m_preferredHeight_150; }
	inline float* get_address_of_m_preferredHeight_150() { return &___m_preferredHeight_150; }
	inline void set_m_preferredHeight_150(float value)
	{
		___m_preferredHeight_150 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_151() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_renderedHeight_151)); }
	inline float get_m_renderedHeight_151() const { return ___m_renderedHeight_151; }
	inline float* get_address_of_m_renderedHeight_151() { return &___m_renderedHeight_151; }
	inline void set_m_renderedHeight_151(float value)
	{
		___m_renderedHeight_151 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_152() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isPreferredHeightDirty_152)); }
	inline bool get_m_isPreferredHeightDirty_152() const { return ___m_isPreferredHeightDirty_152; }
	inline bool* get_address_of_m_isPreferredHeightDirty_152() { return &___m_isPreferredHeightDirty_152; }
	inline void set_m_isPreferredHeightDirty_152(bool value)
	{
		___m_isPreferredHeightDirty_152 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_153() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isCalculatingPreferredValues_153)); }
	inline bool get_m_isCalculatingPreferredValues_153() const { return ___m_isCalculatingPreferredValues_153; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_153() { return &___m_isCalculatingPreferredValues_153; }
	inline void set_m_isCalculatingPreferredValues_153(bool value)
	{
		___m_isCalculatingPreferredValues_153 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_154() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_recursiveCount_154)); }
	inline int32_t get_m_recursiveCount_154() const { return ___m_recursiveCount_154; }
	inline int32_t* get_address_of_m_recursiveCount_154() { return &___m_recursiveCount_154; }
	inline void set_m_recursiveCount_154(int32_t value)
	{
		___m_recursiveCount_154 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_155() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_layoutPriority_155)); }
	inline int32_t get_m_layoutPriority_155() const { return ___m_layoutPriority_155; }
	inline int32_t* get_address_of_m_layoutPriority_155() { return &___m_layoutPriority_155; }
	inline void set_m_layoutPriority_155(int32_t value)
	{
		___m_layoutPriority_155 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_156() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isCalculateSizeRequired_156)); }
	inline bool get_m_isCalculateSizeRequired_156() const { return ___m_isCalculateSizeRequired_156; }
	inline bool* get_address_of_m_isCalculateSizeRequired_156() { return &___m_isCalculateSizeRequired_156; }
	inline void set_m_isCalculateSizeRequired_156(bool value)
	{
		___m_isCalculateSizeRequired_156 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_157() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isLayoutDirty_157)); }
	inline bool get_m_isLayoutDirty_157() const { return ___m_isLayoutDirty_157; }
	inline bool* get_address_of_m_isLayoutDirty_157() { return &___m_isLayoutDirty_157; }
	inline void set_m_isLayoutDirty_157(bool value)
	{
		___m_isLayoutDirty_157 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_158() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_verticesAlreadyDirty_158)); }
	inline bool get_m_verticesAlreadyDirty_158() const { return ___m_verticesAlreadyDirty_158; }
	inline bool* get_address_of_m_verticesAlreadyDirty_158() { return &___m_verticesAlreadyDirty_158; }
	inline void set_m_verticesAlreadyDirty_158(bool value)
	{
		___m_verticesAlreadyDirty_158 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_159() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_layoutAlreadyDirty_159)); }
	inline bool get_m_layoutAlreadyDirty_159() const { return ___m_layoutAlreadyDirty_159; }
	inline bool* get_address_of_m_layoutAlreadyDirty_159() { return &___m_layoutAlreadyDirty_159; }
	inline void set_m_layoutAlreadyDirty_159(bool value)
	{
		___m_layoutAlreadyDirty_159 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_160() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isAwake_160)); }
	inline bool get_m_isAwake_160() const { return ___m_isAwake_160; }
	inline bool* get_address_of_m_isAwake_160() { return &___m_isAwake_160; }
	inline void set_m_isAwake_160(bool value)
	{
		___m_isAwake_160 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_161() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isInputParsingRequired_161)); }
	inline bool get_m_isInputParsingRequired_161() const { return ___m_isInputParsingRequired_161; }
	inline bool* get_address_of_m_isInputParsingRequired_161() { return &___m_isInputParsingRequired_161; }
	inline void set_m_isInputParsingRequired_161(bool value)
	{
		___m_isInputParsingRequired_161 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_162() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_inputSource_162)); }
	inline int32_t get_m_inputSource_162() const { return ___m_inputSource_162; }
	inline int32_t* get_address_of_m_inputSource_162() { return &___m_inputSource_162; }
	inline void set_m_inputSource_162(int32_t value)
	{
		___m_inputSource_162 = value;
	}

	inline static int32_t get_offset_of_old_text_163() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___old_text_163)); }
	inline String_t* get_old_text_163() const { return ___old_text_163; }
	inline String_t** get_address_of_old_text_163() { return &___old_text_163; }
	inline void set_old_text_163(String_t* value)
	{
		___old_text_163 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_163), value);
	}

	inline static int32_t get_offset_of_m_fontScale_164() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontScale_164)); }
	inline float get_m_fontScale_164() const { return ___m_fontScale_164; }
	inline float* get_address_of_m_fontScale_164() { return &___m_fontScale_164; }
	inline void set_m_fontScale_164(float value)
	{
		___m_fontScale_164 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_165() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_fontScaleMultiplier_165)); }
	inline float get_m_fontScaleMultiplier_165() const { return ___m_fontScaleMultiplier_165; }
	inline float* get_address_of_m_fontScaleMultiplier_165() { return &___m_fontScaleMultiplier_165; }
	inline void set_m_fontScaleMultiplier_165(float value)
	{
		___m_fontScaleMultiplier_165 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_166() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_htmlTag_166)); }
	inline CharU5BU5D_t1328083999* get_m_htmlTag_166() const { return ___m_htmlTag_166; }
	inline CharU5BU5D_t1328083999** get_address_of_m_htmlTag_166() { return &___m_htmlTag_166; }
	inline void set_m_htmlTag_166(CharU5BU5D_t1328083999* value)
	{
		___m_htmlTag_166 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_166), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_167() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_xmlAttribute_167)); }
	inline XML_TagAttributeU5BU5D_t573465953* get_m_xmlAttribute_167() const { return ___m_xmlAttribute_167; }
	inline XML_TagAttributeU5BU5D_t573465953** get_address_of_m_xmlAttribute_167() { return &___m_xmlAttribute_167; }
	inline void set_m_xmlAttribute_167(XML_TagAttributeU5BU5D_t573465953* value)
	{
		___m_xmlAttribute_167 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_167), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_168() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_attributeParameterValues_168)); }
	inline SingleU5BU5D_t577127397* get_m_attributeParameterValues_168() const { return ___m_attributeParameterValues_168; }
	inline SingleU5BU5D_t577127397** get_address_of_m_attributeParameterValues_168() { return &___m_attributeParameterValues_168; }
	inline void set_m_attributeParameterValues_168(SingleU5BU5D_t577127397* value)
	{
		___m_attributeParameterValues_168 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_168), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_169() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___tag_LineIndent_169)); }
	inline float get_tag_LineIndent_169() const { return ___tag_LineIndent_169; }
	inline float* get_address_of_tag_LineIndent_169() { return &___tag_LineIndent_169; }
	inline void set_tag_LineIndent_169(float value)
	{
		___tag_LineIndent_169 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_170() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___tag_Indent_170)); }
	inline float get_tag_Indent_170() const { return ___tag_Indent_170; }
	inline float* get_address_of_tag_Indent_170() { return &___tag_Indent_170; }
	inline void set_tag_Indent_170(float value)
	{
		___tag_Indent_170 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_171() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_indentStack_171)); }
	inline TMP_XmlTagStack_1_t2735062451  get_m_indentStack_171() const { return ___m_indentStack_171; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_m_indentStack_171() { return &___m_indentStack_171; }
	inline void set_m_indentStack_171(TMP_XmlTagStack_1_t2735062451  value)
	{
		___m_indentStack_171 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_172() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___tag_NoParsing_172)); }
	inline bool get_tag_NoParsing_172() const { return ___tag_NoParsing_172; }
	inline bool* get_address_of_tag_NoParsing_172() { return &___tag_NoParsing_172; }
	inline void set_tag_NoParsing_172(bool value)
	{
		___tag_NoParsing_172 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_173() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isParsingText_173)); }
	inline bool get_m_isParsingText_173() const { return ___m_isParsingText_173; }
	inline bool* get_address_of_m_isParsingText_173() { return &___m_isParsingText_173; }
	inline void set_m_isParsingText_173(bool value)
	{
		___m_isParsingText_173 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_174() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_FXMatrix_174)); }
	inline Matrix4x4_t2933234003  get_m_FXMatrix_174() const { return ___m_FXMatrix_174; }
	inline Matrix4x4_t2933234003 * get_address_of_m_FXMatrix_174() { return &___m_FXMatrix_174; }
	inline void set_m_FXMatrix_174(Matrix4x4_t2933234003  value)
	{
		___m_FXMatrix_174 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_175() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_isFXMatrixSet_175)); }
	inline bool get_m_isFXMatrixSet_175() const { return ___m_isFXMatrixSet_175; }
	inline bool* get_address_of_m_isFXMatrixSet_175() { return &___m_isFXMatrixSet_175; }
	inline void set_m_isFXMatrixSet_175(bool value)
	{
		___m_isFXMatrixSet_175 = value;
	}

	inline static int32_t get_offset_of_m_char_buffer_176() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_char_buffer_176)); }
	inline Int32U5BU5D_t3030399641* get_m_char_buffer_176() const { return ___m_char_buffer_176; }
	inline Int32U5BU5D_t3030399641** get_address_of_m_char_buffer_176() { return &___m_char_buffer_176; }
	inline void set_m_char_buffer_176(Int32U5BU5D_t3030399641* value)
	{
		___m_char_buffer_176 = value;
		Il2CppCodeGenWriteBarrier((&___m_char_buffer_176), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_177() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_internalCharacterInfo_177)); }
	inline TMP_CharacterInfoU5BU5D_t602810366* get_m_internalCharacterInfo_177() const { return ___m_internalCharacterInfo_177; }
	inline TMP_CharacterInfoU5BU5D_t602810366** get_address_of_m_internalCharacterInfo_177() { return &___m_internalCharacterInfo_177; }
	inline void set_m_internalCharacterInfo_177(TMP_CharacterInfoU5BU5D_t602810366* value)
	{
		___m_internalCharacterInfo_177 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_177), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_178() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_input_CharArray_178)); }
	inline CharU5BU5D_t1328083999* get_m_input_CharArray_178() const { return ___m_input_CharArray_178; }
	inline CharU5BU5D_t1328083999** get_address_of_m_input_CharArray_178() { return &___m_input_CharArray_178; }
	inline void set_m_input_CharArray_178(CharU5BU5D_t1328083999* value)
	{
		___m_input_CharArray_178 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_178), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_179() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_charArray_Length_179)); }
	inline int32_t get_m_charArray_Length_179() const { return ___m_charArray_Length_179; }
	inline int32_t* get_address_of_m_charArray_Length_179() { return &___m_charArray_Length_179; }
	inline void set_m_charArray_Length_179(int32_t value)
	{
		___m_charArray_Length_179 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_180() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_totalCharacterCount_180)); }
	inline int32_t get_m_totalCharacterCount_180() const { return ___m_totalCharacterCount_180; }
	inline int32_t* get_address_of_m_totalCharacterCount_180() { return &___m_totalCharacterCount_180; }
	inline void set_m_totalCharacterCount_180(int32_t value)
	{
		___m_totalCharacterCount_180 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_181() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_SavedWordWrapState_181)); }
	inline WordWrapState_t433984875  get_m_SavedWordWrapState_181() const { return ___m_SavedWordWrapState_181; }
	inline WordWrapState_t433984875 * get_address_of_m_SavedWordWrapState_181() { return &___m_SavedWordWrapState_181; }
	inline void set_m_SavedWordWrapState_181(WordWrapState_t433984875  value)
	{
		___m_SavedWordWrapState_181 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_182() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_SavedLineState_182)); }
	inline WordWrapState_t433984875  get_m_SavedLineState_182() const { return ___m_SavedLineState_182; }
	inline WordWrapState_t433984875 * get_address_of_m_SavedLineState_182() { return &___m_SavedLineState_182; }
	inline void set_m_SavedLineState_182(WordWrapState_t433984875  value)
	{
		___m_SavedLineState_182 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_183() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_characterCount_183)); }
	inline int32_t get_m_characterCount_183() const { return ___m_characterCount_183; }
	inline int32_t* get_address_of_m_characterCount_183() { return &___m_characterCount_183; }
	inline void set_m_characterCount_183(int32_t value)
	{
		___m_characterCount_183 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_184() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_firstCharacterOfLine_184)); }
	inline int32_t get_m_firstCharacterOfLine_184() const { return ___m_firstCharacterOfLine_184; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_184() { return &___m_firstCharacterOfLine_184; }
	inline void set_m_firstCharacterOfLine_184(int32_t value)
	{
		___m_firstCharacterOfLine_184 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_185() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_firstVisibleCharacterOfLine_185)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_185() const { return ___m_firstVisibleCharacterOfLine_185; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_185() { return &___m_firstVisibleCharacterOfLine_185; }
	inline void set_m_firstVisibleCharacterOfLine_185(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_185 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_186() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lastCharacterOfLine_186)); }
	inline int32_t get_m_lastCharacterOfLine_186() const { return ___m_lastCharacterOfLine_186; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_186() { return &___m_lastCharacterOfLine_186; }
	inline void set_m_lastCharacterOfLine_186(int32_t value)
	{
		___m_lastCharacterOfLine_186 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_187() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lastVisibleCharacterOfLine_187)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_187() const { return ___m_lastVisibleCharacterOfLine_187; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_187() { return &___m_lastVisibleCharacterOfLine_187; }
	inline void set_m_lastVisibleCharacterOfLine_187(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_187 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_188() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineNumber_188)); }
	inline int32_t get_m_lineNumber_188() const { return ___m_lineNumber_188; }
	inline int32_t* get_address_of_m_lineNumber_188() { return &___m_lineNumber_188; }
	inline void set_m_lineNumber_188(int32_t value)
	{
		___m_lineNumber_188 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_189() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineVisibleCharacterCount_189)); }
	inline int32_t get_m_lineVisibleCharacterCount_189() const { return ___m_lineVisibleCharacterCount_189; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_189() { return &___m_lineVisibleCharacterCount_189; }
	inline void set_m_lineVisibleCharacterCount_189(int32_t value)
	{
		___m_lineVisibleCharacterCount_189 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_190() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_pageNumber_190)); }
	inline int32_t get_m_pageNumber_190() const { return ___m_pageNumber_190; }
	inline int32_t* get_address_of_m_pageNumber_190() { return &___m_pageNumber_190; }
	inline void set_m_pageNumber_190(int32_t value)
	{
		___m_pageNumber_190 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_191() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxAscender_191)); }
	inline float get_m_maxAscender_191() const { return ___m_maxAscender_191; }
	inline float* get_address_of_m_maxAscender_191() { return &___m_maxAscender_191; }
	inline void set_m_maxAscender_191(float value)
	{
		___m_maxAscender_191 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_192() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxCapHeight_192)); }
	inline float get_m_maxCapHeight_192() const { return ___m_maxCapHeight_192; }
	inline float* get_address_of_m_maxCapHeight_192() { return &___m_maxCapHeight_192; }
	inline void set_m_maxCapHeight_192(float value)
	{
		___m_maxCapHeight_192 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_193() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxDescender_193)); }
	inline float get_m_maxDescender_193() const { return ___m_maxDescender_193; }
	inline float* get_address_of_m_maxDescender_193() { return &___m_maxDescender_193; }
	inline void set_m_maxDescender_193(float value)
	{
		___m_maxDescender_193 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_194() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxLineAscender_194)); }
	inline float get_m_maxLineAscender_194() const { return ___m_maxLineAscender_194; }
	inline float* get_address_of_m_maxLineAscender_194() { return &___m_maxLineAscender_194; }
	inline void set_m_maxLineAscender_194(float value)
	{
		___m_maxLineAscender_194 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_195() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_maxLineDescender_195)); }
	inline float get_m_maxLineDescender_195() const { return ___m_maxLineDescender_195; }
	inline float* get_address_of_m_maxLineDescender_195() { return &___m_maxLineDescender_195; }
	inline void set_m_maxLineDescender_195(float value)
	{
		___m_maxLineDescender_195 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_startOfLineAscender_196)); }
	inline float get_m_startOfLineAscender_196() const { return ___m_startOfLineAscender_196; }
	inline float* get_address_of_m_startOfLineAscender_196() { return &___m_startOfLineAscender_196; }
	inline void set_m_startOfLineAscender_196(float value)
	{
		___m_startOfLineAscender_196 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_197() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_lineOffset_197)); }
	inline float get_m_lineOffset_197() const { return ___m_lineOffset_197; }
	inline float* get_address_of_m_lineOffset_197() { return &___m_lineOffset_197; }
	inline void set_m_lineOffset_197(float value)
	{
		___m_lineOffset_197 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_198() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_meshExtents_198)); }
	inline Extents_t3018556803  get_m_meshExtents_198() const { return ___m_meshExtents_198; }
	inline Extents_t3018556803 * get_address_of_m_meshExtents_198() { return &___m_meshExtents_198; }
	inline void set_m_meshExtents_198(Extents_t3018556803  value)
	{
		___m_meshExtents_198 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_199() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_htmlColor_199)); }
	inline Color32_t874517518  get_m_htmlColor_199() const { return ___m_htmlColor_199; }
	inline Color32_t874517518 * get_address_of_m_htmlColor_199() { return &___m_htmlColor_199; }
	inline void set_m_htmlColor_199(Color32_t874517518  value)
	{
		___m_htmlColor_199 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_200() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_colorStack_200)); }
	inline TMP_XmlTagStack_1_t1533070037  get_m_colorStack_200() const { return ___m_colorStack_200; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_m_colorStack_200() { return &___m_colorStack_200; }
	inline void set_m_colorStack_200(TMP_XmlTagStack_1_t1533070037  value)
	{
		___m_colorStack_200 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_201() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_underlineColorStack_201)); }
	inline TMP_XmlTagStack_1_t1533070037  get_m_underlineColorStack_201() const { return ___m_underlineColorStack_201; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_m_underlineColorStack_201() { return &___m_underlineColorStack_201; }
	inline void set_m_underlineColorStack_201(TMP_XmlTagStack_1_t1533070037  value)
	{
		___m_underlineColorStack_201 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_202() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_strikethroughColorStack_202)); }
	inline TMP_XmlTagStack_1_t1533070037  get_m_strikethroughColorStack_202() const { return ___m_strikethroughColorStack_202; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_m_strikethroughColorStack_202() { return &___m_strikethroughColorStack_202; }
	inline void set_m_strikethroughColorStack_202(TMP_XmlTagStack_1_t1533070037  value)
	{
		___m_strikethroughColorStack_202 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_203() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_highlightColorStack_203)); }
	inline TMP_XmlTagStack_1_t1533070037  get_m_highlightColorStack_203() const { return ___m_highlightColorStack_203; }
	inline TMP_XmlTagStack_1_t1533070037 * get_address_of_m_highlightColorStack_203() { return &___m_highlightColorStack_203; }
	inline void set_m_highlightColorStack_203(TMP_XmlTagStack_1_t1533070037  value)
	{
		___m_highlightColorStack_203 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_204() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_colorGradientPreset_204)); }
	inline TMP_ColorGradient_t1159837347 * get_m_colorGradientPreset_204() const { return ___m_colorGradientPreset_204; }
	inline TMP_ColorGradient_t1159837347 ** get_address_of_m_colorGradientPreset_204() { return &___m_colorGradientPreset_204; }
	inline void set_m_colorGradientPreset_204(TMP_ColorGradient_t1159837347 * value)
	{
		___m_colorGradientPreset_204 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_204), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_colorGradientStack_205)); }
	inline TMP_XmlTagStack_1_t1818389866  get_m_colorGradientStack_205() const { return ___m_colorGradientStack_205; }
	inline TMP_XmlTagStack_1_t1818389866 * get_address_of_m_colorGradientStack_205() { return &___m_colorGradientStack_205; }
	inline void set_m_colorGradientStack_205(TMP_XmlTagStack_1_t1818389866  value)
	{
		___m_colorGradientStack_205 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_206() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_tabSpacing_206)); }
	inline float get_m_tabSpacing_206() const { return ___m_tabSpacing_206; }
	inline float* get_address_of_m_tabSpacing_206() { return &___m_tabSpacing_206; }
	inline void set_m_tabSpacing_206(float value)
	{
		___m_tabSpacing_206 = value;
	}

	inline static int32_t get_offset_of_m_spacing_207() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spacing_207)); }
	inline float get_m_spacing_207() const { return ___m_spacing_207; }
	inline float* get_address_of_m_spacing_207() { return &___m_spacing_207; }
	inline void set_m_spacing_207(float value)
	{
		___m_spacing_207 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_styleStack_208)); }
	inline TMP_XmlTagStack_1_t2730429967  get_m_styleStack_208() const { return ___m_styleStack_208; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_m_styleStack_208() { return &___m_styleStack_208; }
	inline void set_m_styleStack_208(TMP_XmlTagStack_1_t2730429967  value)
	{
		___m_styleStack_208 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_209() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_actionStack_209)); }
	inline TMP_XmlTagStack_1_t2730429967  get_m_actionStack_209() const { return ___m_actionStack_209; }
	inline TMP_XmlTagStack_1_t2730429967 * get_address_of_m_actionStack_209() { return &___m_actionStack_209; }
	inline void set_m_actionStack_209(TMP_XmlTagStack_1_t2730429967  value)
	{
		___m_actionStack_209 = value;
	}

	inline static int32_t get_offset_of_m_padding_210() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_padding_210)); }
	inline float get_m_padding_210() const { return ___m_padding_210; }
	inline float* get_address_of_m_padding_210() { return &___m_padding_210; }
	inline void set_m_padding_210(float value)
	{
		___m_padding_210 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_211() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_baselineOffset_211)); }
	inline float get_m_baselineOffset_211() const { return ___m_baselineOffset_211; }
	inline float* get_address_of_m_baselineOffset_211() { return &___m_baselineOffset_211; }
	inline void set_m_baselineOffset_211(float value)
	{
		___m_baselineOffset_211 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_212() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_baselineOffsetStack_212)); }
	inline TMP_XmlTagStack_1_t2735062451  get_m_baselineOffsetStack_212() const { return ___m_baselineOffsetStack_212; }
	inline TMP_XmlTagStack_1_t2735062451 * get_address_of_m_baselineOffsetStack_212() { return &___m_baselineOffsetStack_212; }
	inline void set_m_baselineOffsetStack_212(TMP_XmlTagStack_1_t2735062451  value)
	{
		___m_baselineOffsetStack_212 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_213() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_xAdvance_213)); }
	inline float get_m_xAdvance_213() const { return ___m_xAdvance_213; }
	inline float* get_address_of_m_xAdvance_213() { return &___m_xAdvance_213; }
	inline void set_m_xAdvance_213(float value)
	{
		___m_xAdvance_213 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_214() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_textElementType_214)); }
	inline int32_t get_m_textElementType_214() const { return ___m_textElementType_214; }
	inline int32_t* get_address_of_m_textElementType_214() { return &___m_textElementType_214; }
	inline void set_m_textElementType_214(int32_t value)
	{
		___m_textElementType_214 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_215() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_cached_TextElement_215)); }
	inline TMP_TextElement_t2285620223 * get_m_cached_TextElement_215() const { return ___m_cached_TextElement_215; }
	inline TMP_TextElement_t2285620223 ** get_address_of_m_cached_TextElement_215() { return &___m_cached_TextElement_215; }
	inline void set_m_cached_TextElement_215(TMP_TextElement_t2285620223 * value)
	{
		___m_cached_TextElement_215 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_215), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_GlyphInfo_216() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_cached_Underline_GlyphInfo_216)); }
	inline TMP_Glyph_t909793902 * get_m_cached_Underline_GlyphInfo_216() const { return ___m_cached_Underline_GlyphInfo_216; }
	inline TMP_Glyph_t909793902 ** get_address_of_m_cached_Underline_GlyphInfo_216() { return &___m_cached_Underline_GlyphInfo_216; }
	inline void set_m_cached_Underline_GlyphInfo_216(TMP_Glyph_t909793902 * value)
	{
		___m_cached_Underline_GlyphInfo_216 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_GlyphInfo_216), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_GlyphInfo_217() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_cached_Ellipsis_GlyphInfo_217)); }
	inline TMP_Glyph_t909793902 * get_m_cached_Ellipsis_GlyphInfo_217() const { return ___m_cached_Ellipsis_GlyphInfo_217; }
	inline TMP_Glyph_t909793902 ** get_address_of_m_cached_Ellipsis_GlyphInfo_217() { return &___m_cached_Ellipsis_GlyphInfo_217; }
	inline void set_m_cached_Ellipsis_GlyphInfo_217(TMP_Glyph_t909793902 * value)
	{
		___m_cached_Ellipsis_GlyphInfo_217 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_GlyphInfo_217), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_218() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_defaultSpriteAsset_218)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_defaultSpriteAsset_218() const { return ___m_defaultSpriteAsset_218; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_defaultSpriteAsset_218() { return &___m_defaultSpriteAsset_218; }
	inline void set_m_defaultSpriteAsset_218(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_defaultSpriteAsset_218 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_218), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_219() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_currentSpriteAsset_219)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_currentSpriteAsset_219() const { return ___m_currentSpriteAsset_219; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_currentSpriteAsset_219() { return &___m_currentSpriteAsset_219; }
	inline void set_m_currentSpriteAsset_219(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_currentSpriteAsset_219 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_219), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_220() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteCount_220)); }
	inline int32_t get_m_spriteCount_220() const { return ___m_spriteCount_220; }
	inline int32_t* get_address_of_m_spriteCount_220() { return &___m_spriteCount_220; }
	inline void set_m_spriteCount_220(int32_t value)
	{
		___m_spriteCount_220 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_221() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteIndex_221)); }
	inline int32_t get_m_spriteIndex_221() const { return ___m_spriteIndex_221; }
	inline int32_t* get_address_of_m_spriteIndex_221() { return &___m_spriteIndex_221; }
	inline void set_m_spriteIndex_221(int32_t value)
	{
		___m_spriteIndex_221 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_222() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_spriteAnimationID_222)); }
	inline int32_t get_m_spriteAnimationID_222() const { return ___m_spriteAnimationID_222; }
	inline int32_t* get_address_of_m_spriteAnimationID_222() { return &___m_spriteAnimationID_222; }
	inline void set_m_spriteAnimationID_222(int32_t value)
	{
		___m_spriteAnimationID_222 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_223() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___m_ignoreActiveState_223)); }
	inline bool get_m_ignoreActiveState_223() const { return ___m_ignoreActiveState_223; }
	inline bool* get_address_of_m_ignoreActiveState_223() { return &___m_ignoreActiveState_223; }
	inline void set_m_ignoreActiveState_223(bool value)
	{
		___m_ignoreActiveState_223 = value;
	}

	inline static int32_t get_offset_of_k_Power_224() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777, ___k_Power_224)); }
	inline SingleU5BU5D_t577127397* get_k_Power_224() const { return ___k_Power_224; }
	inline SingleU5BU5D_t577127397** get_address_of_k_Power_224() { return &___k_Power_224; }
	inline void set_k_Power_224(SingleU5BU5D_t577127397* value)
	{
		___k_Power_224 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_224), value);
	}
};

struct TMP_Text_t1920000777_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t874517518  ___s_colorWhite_45;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_t2243707579  ___k_LargePositiveVector2_225;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_t2243707579  ___k_LargeNegativeVector2_226;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_227;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_228;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_229;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_230;

public:
	inline static int32_t get_offset_of_s_colorWhite_45() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___s_colorWhite_45)); }
	inline Color32_t874517518  get_s_colorWhite_45() const { return ___s_colorWhite_45; }
	inline Color32_t874517518 * get_address_of_s_colorWhite_45() { return &___s_colorWhite_45; }
	inline void set_s_colorWhite_45(Color32_t874517518  value)
	{
		___s_colorWhite_45 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_225() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargePositiveVector2_225)); }
	inline Vector2_t2243707579  get_k_LargePositiveVector2_225() const { return ___k_LargePositiveVector2_225; }
	inline Vector2_t2243707579 * get_address_of_k_LargePositiveVector2_225() { return &___k_LargePositiveVector2_225; }
	inline void set_k_LargePositiveVector2_225(Vector2_t2243707579  value)
	{
		___k_LargePositiveVector2_225 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_226() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargeNegativeVector2_226)); }
	inline Vector2_t2243707579  get_k_LargeNegativeVector2_226() const { return ___k_LargeNegativeVector2_226; }
	inline Vector2_t2243707579 * get_address_of_k_LargeNegativeVector2_226() { return &___k_LargeNegativeVector2_226; }
	inline void set_k_LargeNegativeVector2_226(Vector2_t2243707579  value)
	{
		___k_LargeNegativeVector2_226 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_227() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargePositiveFloat_227)); }
	inline float get_k_LargePositiveFloat_227() const { return ___k_LargePositiveFloat_227; }
	inline float* get_address_of_k_LargePositiveFloat_227() { return &___k_LargePositiveFloat_227; }
	inline void set_k_LargePositiveFloat_227(float value)
	{
		___k_LargePositiveFloat_227 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_228() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargeNegativeFloat_228)); }
	inline float get_k_LargeNegativeFloat_228() const { return ___k_LargeNegativeFloat_228; }
	inline float* get_address_of_k_LargeNegativeFloat_228() { return &___k_LargeNegativeFloat_228; }
	inline void set_k_LargeNegativeFloat_228(float value)
	{
		___k_LargeNegativeFloat_228 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_229() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargePositiveInt_229)); }
	inline int32_t get_k_LargePositiveInt_229() const { return ___k_LargePositiveInt_229; }
	inline int32_t* get_address_of_k_LargePositiveInt_229() { return &___k_LargePositiveInt_229; }
	inline void set_k_LargePositiveInt_229(int32_t value)
	{
		___k_LargePositiveInt_229 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_230() { return static_cast<int32_t>(offsetof(TMP_Text_t1920000777_StaticFields, ___k_LargeNegativeInt_230)); }
	inline int32_t get_k_LargeNegativeInt_230() const { return ___k_LargeNegativeInt_230; }
	inline int32_t* get_address_of_k_LargeNegativeInt_230() { return &___k_LargeNegativeInt_230; }
	inline void set_k_LargeNegativeInt_230(int32_t value)
	{
		___k_LargeNegativeInt_230 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T1920000777_H
#ifndef TMP_SUBMESHUI_T1983202343_H
#define TMP_SUBMESHUI_T1983202343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMeshUI
struct  TMP_SubMeshUI_t1983202343  : public MaskableGraphic_t540192618
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMeshUI::m_fontAsset
	TMP_FontAsset_t2530419979 * ___m_fontAsset_28;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMeshUI::m_spriteAsset
	TMP_SpriteAsset_t2641813093 * ___m_spriteAsset_29;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_material
	Material_t193706927 * ___m_material_30;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_sharedMaterial
	Material_t193706927 * ___m_sharedMaterial_31;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackMaterial
	Material_t193706927 * ___m_fallbackMaterial_32;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackSourceMaterial
	Material_t193706927 * ___m_fallbackSourceMaterial_33;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_34;
	// System.Single TMPro.TMP_SubMeshUI::m_padding
	float ___m_padding_35;
	// UnityEngine.CanvasRenderer TMPro.TMP_SubMeshUI::m_canvasRenderer
	CanvasRenderer_t261436805 * ___m_canvasRenderer_36;
	// UnityEngine.Mesh TMPro.TMP_SubMeshUI::m_mesh
	Mesh_t1356156583 * ___m_mesh_37;
	// TMPro.TextMeshProUGUI TMPro.TMP_SubMeshUI::m_TextComponent
	TextMeshProUGUI_t934157183 * ___m_TextComponent_38;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_39;
	// System.Boolean TMPro.TMP_SubMeshUI::m_materialDirty
	bool ___m_materialDirty_40;
	// System.Int32 TMPro.TMP_SubMeshUI::m_materialReferenceIndex
	int32_t ___m_materialReferenceIndex_41;

public:
	inline static int32_t get_offset_of_m_fontAsset_28() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_fontAsset_28)); }
	inline TMP_FontAsset_t2530419979 * get_m_fontAsset_28() const { return ___m_fontAsset_28; }
	inline TMP_FontAsset_t2530419979 ** get_address_of_m_fontAsset_28() { return &___m_fontAsset_28; }
	inline void set_m_fontAsset_28(TMP_FontAsset_t2530419979 * value)
	{
		___m_fontAsset_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_28), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_29() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_spriteAsset_29)); }
	inline TMP_SpriteAsset_t2641813093 * get_m_spriteAsset_29() const { return ___m_spriteAsset_29; }
	inline TMP_SpriteAsset_t2641813093 ** get_address_of_m_spriteAsset_29() { return &___m_spriteAsset_29; }
	inline void set_m_spriteAsset_29(TMP_SpriteAsset_t2641813093 * value)
	{
		___m_spriteAsset_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_29), value);
	}

	inline static int32_t get_offset_of_m_material_30() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_material_30)); }
	inline Material_t193706927 * get_m_material_30() const { return ___m_material_30; }
	inline Material_t193706927 ** get_address_of_m_material_30() { return &___m_material_30; }
	inline void set_m_material_30(Material_t193706927 * value)
	{
		___m_material_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_30), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_31() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_sharedMaterial_31)); }
	inline Material_t193706927 * get_m_sharedMaterial_31() const { return ___m_sharedMaterial_31; }
	inline Material_t193706927 ** get_address_of_m_sharedMaterial_31() { return &___m_sharedMaterial_31; }
	inline void set_m_sharedMaterial_31(Material_t193706927 * value)
	{
		___m_sharedMaterial_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_31), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_32() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_fallbackMaterial_32)); }
	inline Material_t193706927 * get_m_fallbackMaterial_32() const { return ___m_fallbackMaterial_32; }
	inline Material_t193706927 ** get_address_of_m_fallbackMaterial_32() { return &___m_fallbackMaterial_32; }
	inline void set_m_fallbackMaterial_32(Material_t193706927 * value)
	{
		___m_fallbackMaterial_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_32), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_33() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_fallbackSourceMaterial_33)); }
	inline Material_t193706927 * get_m_fallbackSourceMaterial_33() const { return ___m_fallbackSourceMaterial_33; }
	inline Material_t193706927 ** get_address_of_m_fallbackSourceMaterial_33() { return &___m_fallbackSourceMaterial_33; }
	inline void set_m_fallbackSourceMaterial_33(Material_t193706927 * value)
	{
		___m_fallbackSourceMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_34() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_isDefaultMaterial_34)); }
	inline bool get_m_isDefaultMaterial_34() const { return ___m_isDefaultMaterial_34; }
	inline bool* get_address_of_m_isDefaultMaterial_34() { return &___m_isDefaultMaterial_34; }
	inline void set_m_isDefaultMaterial_34(bool value)
	{
		___m_isDefaultMaterial_34 = value;
	}

	inline static int32_t get_offset_of_m_padding_35() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_padding_35)); }
	inline float get_m_padding_35() const { return ___m_padding_35; }
	inline float* get_address_of_m_padding_35() { return &___m_padding_35; }
	inline void set_m_padding_35(float value)
	{
		___m_padding_35 = value;
	}

	inline static int32_t get_offset_of_m_canvasRenderer_36() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_canvasRenderer_36)); }
	inline CanvasRenderer_t261436805 * get_m_canvasRenderer_36() const { return ___m_canvasRenderer_36; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_canvasRenderer_36() { return &___m_canvasRenderer_36; }
	inline void set_m_canvasRenderer_36(CanvasRenderer_t261436805 * value)
	{
		___m_canvasRenderer_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_36), value);
	}

	inline static int32_t get_offset_of_m_mesh_37() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_mesh_37)); }
	inline Mesh_t1356156583 * get_m_mesh_37() const { return ___m_mesh_37; }
	inline Mesh_t1356156583 ** get_address_of_m_mesh_37() { return &___m_mesh_37; }
	inline void set_m_mesh_37(Mesh_t1356156583 * value)
	{
		___m_mesh_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_37), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_38() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_TextComponent_38)); }
	inline TextMeshProUGUI_t934157183 * get_m_TextComponent_38() const { return ___m_TextComponent_38; }
	inline TextMeshProUGUI_t934157183 ** get_address_of_m_TextComponent_38() { return &___m_TextComponent_38; }
	inline void set_m_TextComponent_38(TextMeshProUGUI_t934157183 * value)
	{
		___m_TextComponent_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_38), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_39() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_isRegisteredForEvents_39)); }
	inline bool get_m_isRegisteredForEvents_39() const { return ___m_isRegisteredForEvents_39; }
	inline bool* get_address_of_m_isRegisteredForEvents_39() { return &___m_isRegisteredForEvents_39; }
	inline void set_m_isRegisteredForEvents_39(bool value)
	{
		___m_isRegisteredForEvents_39 = value;
	}

	inline static int32_t get_offset_of_m_materialDirty_40() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_materialDirty_40)); }
	inline bool get_m_materialDirty_40() const { return ___m_materialDirty_40; }
	inline bool* get_address_of_m_materialDirty_40() { return &___m_materialDirty_40; }
	inline void set_m_materialDirty_40(bool value)
	{
		___m_materialDirty_40 = value;
	}

	inline static int32_t get_offset_of_m_materialReferenceIndex_41() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_t1983202343, ___m_materialReferenceIndex_41)); }
	inline int32_t get_m_materialReferenceIndex_41() const { return ___m_materialReferenceIndex_41; }
	inline int32_t* get_address_of_m_materialReferenceIndex_41() { return &___m_materialReferenceIndex_41; }
	inline void set_m_materialReferenceIndex_41(int32_t value)
	{
		___m_materialReferenceIndex_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESHUI_T1983202343_H
#ifndef TMP_SELECTIONCARET_T3802037885_H
#define TMP_SELECTIONCARET_T3802037885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SelectionCaret
struct  TMP_SelectionCaret_t3802037885  : public MaskableGraphic_t540192618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SELECTIONCARET_T3802037885_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { sizeof (AnchorPositions_t2740933726)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2700[12] = 
{
	AnchorPositions_t2740933726::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { sizeof (ColorTween_t1637921736)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[6] = 
{
	ColorTween_t1637921736::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t1637921736::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t1637921736::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t1637921736::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t1637921736::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorTween_t1637921736::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { sizeof (ColorTweenMode_t903336102)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2703[4] = 
{
	ColorTweenMode_t903336102::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { sizeof (ColorTweenCallback_t1307749522), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { sizeof (FloatTween_t1652887471)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[5] = 
{
	FloatTween_t1652887471::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t1652887471::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t1652887471::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t1652887471::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatTween_t1652887471::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { sizeof (FloatTweenCallback_t2368686856), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { sizeof (TMP_DefaultControls_t1315555119), -1, sizeof(TMP_DefaultControls_t1315555119_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2709[7] = 
{
	0,
	0,
	0,
	TMP_DefaultControls_t1315555119_StaticFields::get_offset_of_s_ThickElementSize_3(),
	TMP_DefaultControls_t1315555119_StaticFields::get_offset_of_s_ThinElementSize_4(),
	TMP_DefaultControls_t1315555119_StaticFields::get_offset_of_s_DefaultSelectableColor_5(),
	TMP_DefaultControls_t1315555119_StaticFields::get_offset_of_s_TextColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { sizeof (Resources_t4030816863)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[7] = 
{
	Resources_t4030816863::get_offset_of_standard_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t4030816863::get_offset_of_background_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t4030816863::get_offset_of_inputField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t4030816863::get_offset_of_knob_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t4030816863::get_offset_of_checkmark_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t4030816863::get_offset_of_dropdown_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Resources_t4030816863::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { sizeof (TMP_Dropdown_t1768193147), -1, sizeof(TMP_Dropdown_t1768193147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2711[14] = 
{
	TMP_Dropdown_t1768193147::get_offset_of_m_Template_16(),
	TMP_Dropdown_t1768193147::get_offset_of_m_CaptionText_17(),
	TMP_Dropdown_t1768193147::get_offset_of_m_CaptionImage_18(),
	TMP_Dropdown_t1768193147::get_offset_of_m_ItemText_19(),
	TMP_Dropdown_t1768193147::get_offset_of_m_ItemImage_20(),
	TMP_Dropdown_t1768193147::get_offset_of_m_Value_21(),
	TMP_Dropdown_t1768193147::get_offset_of_m_Options_22(),
	TMP_Dropdown_t1768193147::get_offset_of_m_OnValueChanged_23(),
	TMP_Dropdown_t1768193147::get_offset_of_m_Dropdown_24(),
	TMP_Dropdown_t1768193147::get_offset_of_m_Blocker_25(),
	TMP_Dropdown_t1768193147::get_offset_of_m_Items_26(),
	TMP_Dropdown_t1768193147::get_offset_of_m_AlphaTweenRunner_27(),
	TMP_Dropdown_t1768193147::get_offset_of_validTemplate_28(),
	TMP_Dropdown_t1768193147_StaticFields::get_offset_of_s_NoOptionData_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (DropdownItem_t1251916390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[4] = 
{
	DropdownItem_t1251916390::get_offset_of_m_Text_2(),
	DropdownItem_t1251916390::get_offset_of_m_Image_3(),
	DropdownItem_t1251916390::get_offset_of_m_RectTransform_4(),
	DropdownItem_t1251916390::get_offset_of_m_Toggle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (OptionData_t234712921), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2713[2] = 
{
	OptionData_t234712921::get_offset_of_m_Text_0(),
	OptionData_t234712921::get_offset_of_m_Image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (OptionDataList_t457963479), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[1] = 
{
	OptionDataList_t457963479::get_offset_of_m_Options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (DropdownEvent_t1859881917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { sizeof (U3CShowU3Ec__AnonStorey1_t540509321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[2] = 
{
	U3CShowU3Ec__AnonStorey1_t540509321::get_offset_of_item_0(),
	U3CShowU3Ec__AnonStorey1_t540509321::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { sizeof (U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[5] = 
{
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626::get_offset_of_delay_0(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626::get_offset_of_U24this_1(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626::get_offset_of_U24current_2(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626::get_offset_of_U24disposing_3(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3901319626::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { sizeof (TMP_FontWeights_t1564168302)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[2] = 
{
	TMP_FontWeights_t1564168302::get_offset_of_regularTypeface_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_FontWeights_t1564168302::get_offset_of_italicTypeface_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { sizeof (TMP_FontAsset_t2530419979), -1, sizeof(TMP_FontAsset_t2530419979_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2719[22] = 
{
	TMP_FontAsset_t2530419979_StaticFields::get_offset_of_s_defaultFontAsset_5(),
	TMP_FontAsset_t2530419979::get_offset_of_fontAssetType_6(),
	TMP_FontAsset_t2530419979::get_offset_of_m_fontInfo_7(),
	TMP_FontAsset_t2530419979::get_offset_of_atlas_8(),
	TMP_FontAsset_t2530419979::get_offset_of_m_glyphInfoList_9(),
	TMP_FontAsset_t2530419979::get_offset_of_m_characterDictionary_10(),
	TMP_FontAsset_t2530419979::get_offset_of_m_kerningDictionary_11(),
	TMP_FontAsset_t2530419979::get_offset_of_m_kerningInfo_12(),
	TMP_FontAsset_t2530419979::get_offset_of_m_kerningPair_13(),
	TMP_FontAsset_t2530419979::get_offset_of_fallbackFontAssets_14(),
	TMP_FontAsset_t2530419979::get_offset_of_fontCreationSettings_15(),
	TMP_FontAsset_t2530419979::get_offset_of_fontWeights_16(),
	TMP_FontAsset_t2530419979::get_offset_of_m_characterSet_17(),
	TMP_FontAsset_t2530419979::get_offset_of_normalStyle_18(),
	TMP_FontAsset_t2530419979::get_offset_of_normalSpacingOffset_19(),
	TMP_FontAsset_t2530419979::get_offset_of_boldStyle_20(),
	TMP_FontAsset_t2530419979::get_offset_of_boldSpacing_21(),
	TMP_FontAsset_t2530419979::get_offset_of_italicStyle_22(),
	TMP_FontAsset_t2530419979::get_offset_of_tabSize_23(),
	TMP_FontAsset_t2530419979::get_offset_of_m_oldTabSize_24(),
	TMP_FontAsset_t2530419979_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_25(),
	TMP_FontAsset_t2530419979_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { sizeof (FontAssetTypes_t2622056438)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2720[4] = 
{
	FontAssetTypes_t2622056438::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { sizeof (TMP_InputField_t1778301588), -1, sizeof(TMP_InputField_t1778301588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2721[76] = 
{
	TMP_InputField_t1778301588::get_offset_of_m_Keyboard_16(),
	TMP_InputField_t1778301588_StaticFields::get_offset_of_kSeparators_17(),
	TMP_InputField_t1778301588::get_offset_of_m_TextViewport_18(),
	TMP_InputField_t1778301588::get_offset_of_m_TextComponent_19(),
	TMP_InputField_t1778301588::get_offset_of_m_TextComponentRectTransform_20(),
	TMP_InputField_t1778301588::get_offset_of_m_Placeholder_21(),
	TMP_InputField_t1778301588::get_offset_of_m_VerticalScrollbar_22(),
	TMP_InputField_t1778301588::get_offset_of_m_VerticalScrollbarEventHandler_23(),
	TMP_InputField_t1778301588::get_offset_of_m_ScrollPosition_24(),
	TMP_InputField_t1778301588::get_offset_of_m_ScrollSensitivity_25(),
	TMP_InputField_t1778301588::get_offset_of_m_ContentType_26(),
	TMP_InputField_t1778301588::get_offset_of_m_InputType_27(),
	TMP_InputField_t1778301588::get_offset_of_m_AsteriskChar_28(),
	TMP_InputField_t1778301588::get_offset_of_m_KeyboardType_29(),
	TMP_InputField_t1778301588::get_offset_of_m_LineType_30(),
	TMP_InputField_t1778301588::get_offset_of_m_HideMobileInput_31(),
	TMP_InputField_t1778301588::get_offset_of_m_CharacterValidation_32(),
	TMP_InputField_t1778301588::get_offset_of_m_RegexValue_33(),
	TMP_InputField_t1778301588::get_offset_of_m_GlobalPointSize_34(),
	TMP_InputField_t1778301588::get_offset_of_m_CharacterLimit_35(),
	TMP_InputField_t1778301588::get_offset_of_m_OnEndEdit_36(),
	TMP_InputField_t1778301588::get_offset_of_m_OnSubmit_37(),
	TMP_InputField_t1778301588::get_offset_of_m_OnSelect_38(),
	TMP_InputField_t1778301588::get_offset_of_m_OnDeselect_39(),
	TMP_InputField_t1778301588::get_offset_of_m_OnTextSelection_40(),
	TMP_InputField_t1778301588::get_offset_of_m_OnEndTextSelection_41(),
	TMP_InputField_t1778301588::get_offset_of_m_OnValueChanged_42(),
	TMP_InputField_t1778301588::get_offset_of_m_OnValidateInput_43(),
	TMP_InputField_t1778301588::get_offset_of_m_CaretColor_44(),
	TMP_InputField_t1778301588::get_offset_of_m_CustomCaretColor_45(),
	TMP_InputField_t1778301588::get_offset_of_m_SelectionColor_46(),
	TMP_InputField_t1778301588::get_offset_of_m_Text_47(),
	TMP_InputField_t1778301588::get_offset_of_m_CaretBlinkRate_48(),
	TMP_InputField_t1778301588::get_offset_of_m_CaretWidth_49(),
	TMP_InputField_t1778301588::get_offset_of_m_ReadOnly_50(),
	TMP_InputField_t1778301588::get_offset_of_m_RichText_51(),
	TMP_InputField_t1778301588::get_offset_of_m_StringPosition_52(),
	TMP_InputField_t1778301588::get_offset_of_m_StringSelectPosition_53(),
	TMP_InputField_t1778301588::get_offset_of_m_CaretPosition_54(),
	TMP_InputField_t1778301588::get_offset_of_m_CaretSelectPosition_55(),
	TMP_InputField_t1778301588::get_offset_of_caretRectTrans_56(),
	TMP_InputField_t1778301588::get_offset_of_m_CursorVerts_57(),
	TMP_InputField_t1778301588::get_offset_of_m_CachedInputRenderer_58(),
	TMP_InputField_t1778301588::get_offset_of_m_DefaultTransformPosition_59(),
	TMP_InputField_t1778301588::get_offset_of_m_LastPosition_60(),
	TMP_InputField_t1778301588::get_offset_of_m_Mesh_61(),
	TMP_InputField_t1778301588::get_offset_of_m_AllowInput_62(),
	TMP_InputField_t1778301588::get_offset_of_m_ShouldActivateNextUpdate_63(),
	TMP_InputField_t1778301588::get_offset_of_m_UpdateDrag_64(),
	TMP_InputField_t1778301588::get_offset_of_m_DragPositionOutOfBounds_65(),
	0,
	0,
	TMP_InputField_t1778301588::get_offset_of_m_CaretVisible_68(),
	TMP_InputField_t1778301588::get_offset_of_m_BlinkCoroutine_69(),
	TMP_InputField_t1778301588::get_offset_of_m_BlinkStartTime_70(),
	TMP_InputField_t1778301588::get_offset_of_m_DragCoroutine_71(),
	TMP_InputField_t1778301588::get_offset_of_m_OriginalText_72(),
	TMP_InputField_t1778301588::get_offset_of_m_WasCanceled_73(),
	TMP_InputField_t1778301588::get_offset_of_m_HasDoneFocusTransition_74(),
	TMP_InputField_t1778301588::get_offset_of_m_IsScrollbarUpdateRequired_75(),
	TMP_InputField_t1778301588::get_offset_of_m_IsUpdatingScrollbarValues_76(),
	TMP_InputField_t1778301588::get_offset_of_m_isLastKeyBackspace_77(),
	TMP_InputField_t1778301588::get_offset_of_m_ClickStartTime_78(),
	TMP_InputField_t1778301588::get_offset_of_m_DoubleClickDelay_79(),
	0,
	TMP_InputField_t1778301588::get_offset_of_m_GlobalFontAsset_81(),
	TMP_InputField_t1778301588::get_offset_of_m_OnFocusSelectAll_82(),
	TMP_InputField_t1778301588::get_offset_of_m_isSelectAll_83(),
	TMP_InputField_t1778301588::get_offset_of_m_ResetOnDeActivation_84(),
	TMP_InputField_t1778301588::get_offset_of_m_RestoreOriginalTextOnEscape_85(),
	TMP_InputField_t1778301588::get_offset_of_m_isRichTextEditingAllowed_86(),
	TMP_InputField_t1778301588::get_offset_of_m_InputValidator_87(),
	TMP_InputField_t1778301588::get_offset_of_m_isSelected_88(),
	TMP_InputField_t1778301588::get_offset_of_isStringPositionDirty_89(),
	TMP_InputField_t1778301588::get_offset_of_m_forceRectTransformAdjustment_90(),
	TMP_InputField_t1778301588::get_offset_of_m_ProcessingEvent_91(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { sizeof (ContentType_t4294436424)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2722[11] = 
{
	ContentType_t4294436424::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { sizeof (InputType_t379073331)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2723[4] = 
{
	InputType_t379073331::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { sizeof (CharacterValidation_t487603955)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2724[10] = 
{
	CharacterValidation_t487603955::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { sizeof (LineType_t2475898675)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2725[4] = 
{
	LineType_t2475898675::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { sizeof (OnValidateInput_t3285190392), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { sizeof (SubmitEvent_t3359162065), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { sizeof (OnChangeEvent_t2139251414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (SelectionEvent_t3883897865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (TextSelectionEvent_t3887183206), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (EditState_t4052837484)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2731[3] = 
{
	EditState_t4052837484::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { sizeof (U3CCaretBlinkU3Ec__Iterator0_t2727982073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[6] = 
{
	U3CCaretBlinkU3Ec__Iterator0_t2727982073::get_offset_of_U3CblinkPeriodU3E__1_0(),
	U3CCaretBlinkU3Ec__Iterator0_t2727982073::get_offset_of_U3CblinkStateU3E__1_1(),
	U3CCaretBlinkU3Ec__Iterator0_t2727982073::get_offset_of_U24this_2(),
	U3CCaretBlinkU3Ec__Iterator0_t2727982073::get_offset_of_U24current_3(),
	U3CCaretBlinkU3Ec__Iterator0_t2727982073::get_offset_of_U24disposing_4(),
	U3CCaretBlinkU3Ec__Iterator0_t2727982073::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { sizeof (U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[8] = 
{
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303::get_offset_of_eventData_0(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303::get_offset_of_U3ClocalMousePosU3E__1_1(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303::get_offset_of_U3CrectU3E__1_2(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303::get_offset_of_U3CdelayU3E__1_3(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303::get_offset_of_U24this_4(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303::get_offset_of_U24current_5(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303::get_offset_of_U24disposing_6(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t2119315303::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { sizeof (SetPropertyUtility_t4168857211), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { sizeof (TMP_InputValidator_t3726817866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { sizeof (TMP_LineInfo_t2320418126)+ sizeof (RuntimeObject), sizeof(TMP_LineInfo_t2320418126 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2736[19] = 
{
	TMP_LineInfo_t2320418126::get_offset_of_characterCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_visibleCharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_spaceCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_wordCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_firstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_firstVisibleCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_lastCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_lastVisibleCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_length_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_lineHeight_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_ascender_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_baseline_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_descender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_maxAdvance_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_width_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_marginLeft_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_marginRight_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_alignment_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LineInfo_t2320418126::get_offset_of_lineExtents_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { sizeof (TMP_MaterialManager_t3562209168), -1, sizeof(TMP_MaterialManager_t3562209168_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2738[5] = 
{
	TMP_MaterialManager_t3562209168_StaticFields::get_offset_of_m_materialList_0(),
	TMP_MaterialManager_t3562209168_StaticFields::get_offset_of_m_fallbackMaterials_1(),
	TMP_MaterialManager_t3562209168_StaticFields::get_offset_of_m_fallbackMaterialLookup_2(),
	TMP_MaterialManager_t3562209168_StaticFields::get_offset_of_m_fallbackCleanupList_3(),
	TMP_MaterialManager_t3562209168_StaticFields::get_offset_of_isFallbackListDirty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { sizeof (FallbackMaterial_t3285989240), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[5] = 
{
	FallbackMaterial_t3285989240::get_offset_of_baseID_0(),
	FallbackMaterial_t3285989240::get_offset_of_baseMaterial_1(),
	FallbackMaterial_t3285989240::get_offset_of_fallbackID_2(),
	FallbackMaterial_t3285989240::get_offset_of_fallbackMaterial_3(),
	FallbackMaterial_t3285989240::get_offset_of_count_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { sizeof (MaskingMaterial_t590070688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[4] = 
{
	MaskingMaterial_t590070688::get_offset_of_baseMaterial_0(),
	MaskingMaterial_t590070688::get_offset_of_stencilMaterial_1(),
	MaskingMaterial_t590070688::get_offset_of_count_2(),
	MaskingMaterial_t590070688::get_offset_of_stencilID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { sizeof (U3CGetBaseMaterialU3Ec__AnonStorey0_t1824273780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2741[1] = 
{
	U3CGetBaseMaterialU3Ec__AnonStorey0_t1824273780::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { sizeof (U3CAddMaskingMaterialU3Ec__AnonStorey1_t1699651275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[1] = 
{
	U3CAddMaskingMaterialU3Ec__AnonStorey1_t1699651275::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { sizeof (U3CRemoveStencilMaterialU3Ec__AnonStorey2_t4145099293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[1] = 
{
	U3CRemoveStencilMaterialU3Ec__AnonStorey2_t4145099293::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { sizeof (U3CReleaseBaseMaterialU3Ec__AnonStorey3_t3729327270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[1] = 
{
	U3CReleaseBaseMaterialU3Ec__AnonStorey3_t3729327270::get_offset_of_baseMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { sizeof (VertexSortingOrder_t471281372)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2745[3] = 
{
	VertexSortingOrder_t471281372::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { sizeof (TMP_MeshInfo_t1297308317)+ sizeof (RuntimeObject), -1, sizeof(TMP_MeshInfo_t1297308317_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2746[12] = 
{
	TMP_MeshInfo_t1297308317_StaticFields::get_offset_of_s_DefaultColor_0(),
	TMP_MeshInfo_t1297308317_StaticFields::get_offset_of_s_DefaultNormal_1(),
	TMP_MeshInfo_t1297308317_StaticFields::get_offset_of_s_DefaultTangent_2(),
	TMP_MeshInfo_t1297308317::get_offset_of_mesh_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t1297308317::get_offset_of_vertexCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t1297308317::get_offset_of_vertices_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t1297308317::get_offset_of_normals_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t1297308317::get_offset_of_tangents_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t1297308317::get_offset_of_uvs0_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t1297308317::get_offset_of_uvs2_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t1297308317::get_offset_of_colors32_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_MeshInfo_t1297308317::get_offset_of_triangles_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { sizeof (TMP_ScrollbarEventHandler_t1222271750), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[1] = 
{
	TMP_ScrollbarEventHandler_t1222271750::get_offset_of_isSelected_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { sizeof (TMP_SelectionCaret_t3802037885), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { sizeof (TMP_Settings_t1236229477), -1, sizeof(TMP_Settings_t1236229477_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2750[27] = 
{
	TMP_Settings_t1236229477_StaticFields::get_offset_of_s_Instance_2(),
	TMP_Settings_t1236229477::get_offset_of_m_version_3(),
	TMP_Settings_t1236229477::get_offset_of_m_enableWordWrapping_4(),
	TMP_Settings_t1236229477::get_offset_of_m_enableKerning_5(),
	TMP_Settings_t1236229477::get_offset_of_m_enableExtraPadding_6(),
	TMP_Settings_t1236229477::get_offset_of_m_enableTintAllSprites_7(),
	TMP_Settings_t1236229477::get_offset_of_m_enableParseEscapeCharacters_8(),
	TMP_Settings_t1236229477::get_offset_of_m_missingGlyphCharacter_9(),
	TMP_Settings_t1236229477::get_offset_of_m_warningsDisabled_10(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultFontAsset_11(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultFontAssetPath_12(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultFontSize_13(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultAutoSizeMinRatio_14(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultAutoSizeMaxRatio_15(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultTextMeshProTextContainerSize_16(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultTextMeshProUITextContainerSize_17(),
	TMP_Settings_t1236229477::get_offset_of_m_autoSizeTextContainer_18(),
	TMP_Settings_t1236229477::get_offset_of_m_fallbackFontAssets_19(),
	TMP_Settings_t1236229477::get_offset_of_m_matchMaterialPreset_20(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultSpriteAsset_21(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultSpriteAssetPath_22(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultColorGradientPresetsPath_23(),
	TMP_Settings_t1236229477::get_offset_of_m_enableEmojiSupport_24(),
	TMP_Settings_t1236229477::get_offset_of_m_defaultStyleSheet_25(),
	TMP_Settings_t1236229477::get_offset_of_m_leadingCharacters_26(),
	TMP_Settings_t1236229477::get_offset_of_m_followingCharacters_27(),
	TMP_Settings_t1236229477::get_offset_of_m_linebreakingRules_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { sizeof (LineBreakingTable_t2043799287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[2] = 
{
	LineBreakingTable_t2043799287::get_offset_of_leadingCharacters_0(),
	LineBreakingTable_t2043799287::get_offset_of_followingCharacters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { sizeof (TMP_Sprite_t104383505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[5] = 
{
	TMP_Sprite_t104383505::get_offset_of_name_9(),
	TMP_Sprite_t104383505::get_offset_of_hashCode_10(),
	TMP_Sprite_t104383505::get_offset_of_unicode_11(),
	TMP_Sprite_t104383505::get_offset_of_pivot_12(),
	TMP_Sprite_t104383505::get_offset_of_sprite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { sizeof (TMP_SpriteAnimator_t2347923044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[2] = 
{
	TMP_SpriteAnimator_t2347923044::get_offset_of_m_animations_2(),
	TMP_SpriteAnimator_t2347923044::get_offset_of_m_TextComponent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { sizeof (U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[16] = 
{
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_start_0(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U3CcurrentFrameU3E__0_1(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_end_2(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_spriteAsset_3(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_currentCharacter_4(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U3CcharInfoU3E__0_5(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U3CmaterialIndexU3E__0_6(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U3CvertexIndexU3E__0_7(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U3CmeshInfoU3E__0_8(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U3CelapsedTimeU3E__0_9(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_framerate_10(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U3CtargetTimeU3E__0_11(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U24this_12(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U24current_13(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U24disposing_14(),
	U3CDoSpriteAnimationInternalU3Ec__Iterator0_t12671535::get_offset_of_U24PC_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { sizeof (TMP_SpriteAsset_t2641813093), -1, sizeof(TMP_SpriteAsset_t2641813093_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2755[6] = 
{
	TMP_SpriteAsset_t2641813093::get_offset_of_m_UnicodeLookup_5(),
	TMP_SpriteAsset_t2641813093::get_offset_of_m_NameLookup_6(),
	TMP_SpriteAsset_t2641813093_StaticFields::get_offset_of_m_defaultSpriteAsset_7(),
	TMP_SpriteAsset_t2641813093::get_offset_of_spriteSheet_8(),
	TMP_SpriteAsset_t2641813093::get_offset_of_spriteInfoList_9(),
	TMP_SpriteAsset_t2641813093::get_offset_of_fallbackSpriteAssets_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { sizeof (SpriteAssetImportFormats_t73460506)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2756[3] = 
{
	SpriteAssetImportFormats_t73460506::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { sizeof (TexturePacker_t950091243), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { sizeof (SpriteFrame_t3204261111)+ sizeof (RuntimeObject), sizeof(SpriteFrame_t3204261111 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2758[4] = 
{
	SpriteFrame_t3204261111::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3204261111::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3204261111::get_offset_of_w_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_t3204261111::get_offset_of_h_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { sizeof (SpriteSize_t2240777863)+ sizeof (RuntimeObject), sizeof(SpriteSize_t2240777863 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2759[2] = 
{
	SpriteSize_t2240777863::get_offset_of_w_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteSize_t2240777863::get_offset_of_h_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { sizeof (SpriteData_t257854902)+ sizeof (RuntimeObject), sizeof(SpriteData_t257854902_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2760[7] = 
{
	SpriteData_t257854902::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t257854902::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t257854902::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t257854902::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t257854902::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t257854902::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t257854902::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { sizeof (SpriteDataObject_t1790914055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[1] = 
{
	SpriteDataObject_t1790914055::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { sizeof (TMP_Style_t69737451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[6] = 
{
	TMP_Style_t69737451::get_offset_of_m_Name_0(),
	TMP_Style_t69737451::get_offset_of_m_HashCode_1(),
	TMP_Style_t69737451::get_offset_of_m_OpeningDefinition_2(),
	TMP_Style_t69737451::get_offset_of_m_ClosingDefinition_3(),
	TMP_Style_t69737451::get_offset_of_m_OpeningTagArray_4(),
	TMP_Style_t69737451::get_offset_of_m_ClosingTagArray_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { sizeof (TMP_StyleSheet_t335925700), -1, sizeof(TMP_StyleSheet_t335925700_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2763[3] = 
{
	TMP_StyleSheet_t335925700_StaticFields::get_offset_of_s_Instance_2(),
	TMP_StyleSheet_t335925700::get_offset_of_m_StyleList_3(),
	TMP_StyleSheet_t335925700::get_offset_of_m_StyleDictionary_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { sizeof (TMP_SubMesh_t3507543655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[14] = 
{
	TMP_SubMesh_t3507543655::get_offset_of_m_fontAsset_2(),
	TMP_SubMesh_t3507543655::get_offset_of_m_spriteAsset_3(),
	TMP_SubMesh_t3507543655::get_offset_of_m_material_4(),
	TMP_SubMesh_t3507543655::get_offset_of_m_sharedMaterial_5(),
	TMP_SubMesh_t3507543655::get_offset_of_m_fallbackMaterial_6(),
	TMP_SubMesh_t3507543655::get_offset_of_m_fallbackSourceMaterial_7(),
	TMP_SubMesh_t3507543655::get_offset_of_m_isDefaultMaterial_8(),
	TMP_SubMesh_t3507543655::get_offset_of_m_padding_9(),
	TMP_SubMesh_t3507543655::get_offset_of_m_renderer_10(),
	TMP_SubMesh_t3507543655::get_offset_of_m_meshFilter_11(),
	TMP_SubMesh_t3507543655::get_offset_of_m_mesh_12(),
	TMP_SubMesh_t3507543655::get_offset_of_m_boxCollider_13(),
	TMP_SubMesh_t3507543655::get_offset_of_m_TextComponent_14(),
	TMP_SubMesh_t3507543655::get_offset_of_m_isRegisteredForEvents_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { sizeof (TMP_SubMeshUI_t1983202343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[14] = 
{
	TMP_SubMeshUI_t1983202343::get_offset_of_m_fontAsset_28(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_spriteAsset_29(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_material_30(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_sharedMaterial_31(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_fallbackMaterial_32(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_fallbackSourceMaterial_33(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_isDefaultMaterial_34(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_padding_35(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_canvasRenderer_36(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_mesh_37(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_TextComponent_38(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_isRegisteredForEvents_39(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_materialDirty_40(),
	TMP_SubMeshUI_t1983202343::get_offset_of_m_materialReferenceIndex_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { sizeof (TextAlignmentOptions_t1466788324)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2767[37] = 
{
	TextAlignmentOptions_t1466788324::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { sizeof (_HorizontalAlignmentOptions_t1321544838)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2768[7] = 
{
	_HorizontalAlignmentOptions_t1321544838::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { sizeof (_VerticalAlignmentOptions_t3567775144)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2769[7] = 
{
	_VerticalAlignmentOptions_t3567775144::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { sizeof (TextRenderFlags_t7026704)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2770[3] = 
{
	TextRenderFlags_t7026704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { sizeof (TMP_TextElementType_t1959651783)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2771[3] = 
{
	TMP_TextElementType_t1959651783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { sizeof (MaskingTypes_t259687445)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2772[4] = 
{
	MaskingTypes_t259687445::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { sizeof (TextOverflowModes_t2644609723)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2773[8] = 
{
	TextOverflowModes_t2644609723::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { sizeof (MaskingOffsetMode_t92608302)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2774[3] = 
{
	MaskingOffsetMode_t92608302::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { sizeof (TextureMappingOptions_t761764377)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2775[5] = 
{
	TextureMappingOptions_t761764377::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { sizeof (FontStyles_t3171728781)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2776[12] = 
{
	FontStyles_t3171728781::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { sizeof (FontWeights_t1074713040)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2777[10] = 
{
	FontWeights_t1074713040::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { sizeof (TagUnits_t1942447065)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2778[4] = 
{
	TagUnits_t1942447065::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { sizeof (TagType_t1698342214)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2779[5] = 
{
	TagType_t1698342214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { sizeof (TMP_Text_t1920000777), -1, sizeof(TMP_Text_t1920000777_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2780[203] = 
{
	TMP_Text_t1920000777::get_offset_of_m_text_28(),
	TMP_Text_t1920000777::get_offset_of_m_isRightToLeft_29(),
	TMP_Text_t1920000777::get_offset_of_m_fontAsset_30(),
	TMP_Text_t1920000777::get_offset_of_m_currentFontAsset_31(),
	TMP_Text_t1920000777::get_offset_of_m_isSDFShader_32(),
	TMP_Text_t1920000777::get_offset_of_m_sharedMaterial_33(),
	TMP_Text_t1920000777::get_offset_of_m_currentMaterial_34(),
	TMP_Text_t1920000777::get_offset_of_m_materialReferences_35(),
	TMP_Text_t1920000777::get_offset_of_m_materialReferenceIndexLookup_36(),
	TMP_Text_t1920000777::get_offset_of_m_materialReferenceStack_37(),
	TMP_Text_t1920000777::get_offset_of_m_currentMaterialIndex_38(),
	TMP_Text_t1920000777::get_offset_of_m_fontSharedMaterials_39(),
	TMP_Text_t1920000777::get_offset_of_m_fontMaterial_40(),
	TMP_Text_t1920000777::get_offset_of_m_fontMaterials_41(),
	TMP_Text_t1920000777::get_offset_of_m_isMaterialDirty_42(),
	TMP_Text_t1920000777::get_offset_of_m_fontColor32_43(),
	TMP_Text_t1920000777::get_offset_of_m_fontColor_44(),
	TMP_Text_t1920000777_StaticFields::get_offset_of_s_colorWhite_45(),
	TMP_Text_t1920000777::get_offset_of_m_underlineColor_46(),
	TMP_Text_t1920000777::get_offset_of_m_strikethroughColor_47(),
	TMP_Text_t1920000777::get_offset_of_m_highlightColor_48(),
	TMP_Text_t1920000777::get_offset_of_m_enableVertexGradient_49(),
	TMP_Text_t1920000777::get_offset_of_m_fontColorGradient_50(),
	TMP_Text_t1920000777::get_offset_of_m_fontColorGradientPreset_51(),
	TMP_Text_t1920000777::get_offset_of_m_spriteAsset_52(),
	TMP_Text_t1920000777::get_offset_of_m_tintAllSprites_53(),
	TMP_Text_t1920000777::get_offset_of_m_tintSprite_54(),
	TMP_Text_t1920000777::get_offset_of_m_spriteColor_55(),
	TMP_Text_t1920000777::get_offset_of_m_overrideHtmlColors_56(),
	TMP_Text_t1920000777::get_offset_of_m_faceColor_57(),
	TMP_Text_t1920000777::get_offset_of_m_outlineColor_58(),
	TMP_Text_t1920000777::get_offset_of_m_outlineWidth_59(),
	TMP_Text_t1920000777::get_offset_of_m_fontSize_60(),
	TMP_Text_t1920000777::get_offset_of_m_currentFontSize_61(),
	TMP_Text_t1920000777::get_offset_of_m_fontSizeBase_62(),
	TMP_Text_t1920000777::get_offset_of_m_sizeStack_63(),
	TMP_Text_t1920000777::get_offset_of_m_fontWeight_64(),
	TMP_Text_t1920000777::get_offset_of_m_fontWeightInternal_65(),
	TMP_Text_t1920000777::get_offset_of_m_fontWeightStack_66(),
	TMP_Text_t1920000777::get_offset_of_m_enableAutoSizing_67(),
	TMP_Text_t1920000777::get_offset_of_m_maxFontSize_68(),
	TMP_Text_t1920000777::get_offset_of_m_minFontSize_69(),
	TMP_Text_t1920000777::get_offset_of_m_fontSizeMin_70(),
	TMP_Text_t1920000777::get_offset_of_m_fontSizeMax_71(),
	TMP_Text_t1920000777::get_offset_of_m_fontStyle_72(),
	TMP_Text_t1920000777::get_offset_of_m_style_73(),
	TMP_Text_t1920000777::get_offset_of_m_fontStyleStack_74(),
	TMP_Text_t1920000777::get_offset_of_m_isUsingBold_75(),
	TMP_Text_t1920000777::get_offset_of_m_textAlignment_76(),
	TMP_Text_t1920000777::get_offset_of_m_lineJustification_77(),
	TMP_Text_t1920000777::get_offset_of_m_lineJustificationStack_78(),
	TMP_Text_t1920000777::get_offset_of_m_textContainerLocalCorners_79(),
	TMP_Text_t1920000777::get_offset_of_m_isAlignmentEnumConverted_80(),
	TMP_Text_t1920000777::get_offset_of_m_characterSpacing_81(),
	TMP_Text_t1920000777::get_offset_of_m_cSpacing_82(),
	TMP_Text_t1920000777::get_offset_of_m_monoSpacing_83(),
	TMP_Text_t1920000777::get_offset_of_m_wordSpacing_84(),
	TMP_Text_t1920000777::get_offset_of_m_lineSpacing_85(),
	TMP_Text_t1920000777::get_offset_of_m_lineSpacingDelta_86(),
	TMP_Text_t1920000777::get_offset_of_m_lineHeight_87(),
	TMP_Text_t1920000777::get_offset_of_m_lineSpacingMax_88(),
	TMP_Text_t1920000777::get_offset_of_m_paragraphSpacing_89(),
	TMP_Text_t1920000777::get_offset_of_m_charWidthMaxAdj_90(),
	TMP_Text_t1920000777::get_offset_of_m_charWidthAdjDelta_91(),
	TMP_Text_t1920000777::get_offset_of_m_enableWordWrapping_92(),
	TMP_Text_t1920000777::get_offset_of_m_isCharacterWrappingEnabled_93(),
	TMP_Text_t1920000777::get_offset_of_m_isNonBreakingSpace_94(),
	TMP_Text_t1920000777::get_offset_of_m_isIgnoringAlignment_95(),
	TMP_Text_t1920000777::get_offset_of_m_wordWrappingRatios_96(),
	TMP_Text_t1920000777::get_offset_of_m_overflowMode_97(),
	TMP_Text_t1920000777::get_offset_of_m_firstOverflowCharacterIndex_98(),
	TMP_Text_t1920000777::get_offset_of_m_linkedTextComponent_99(),
	TMP_Text_t1920000777::get_offset_of_m_isLinkedTextComponent_100(),
	TMP_Text_t1920000777::get_offset_of_m_isTextTruncated_101(),
	TMP_Text_t1920000777::get_offset_of_m_enableKerning_102(),
	TMP_Text_t1920000777::get_offset_of_m_enableExtraPadding_103(),
	TMP_Text_t1920000777::get_offset_of_checkPaddingRequired_104(),
	TMP_Text_t1920000777::get_offset_of_m_isRichText_105(),
	TMP_Text_t1920000777::get_offset_of_m_parseCtrlCharacters_106(),
	TMP_Text_t1920000777::get_offset_of_m_isOverlay_107(),
	TMP_Text_t1920000777::get_offset_of_m_isOrthographic_108(),
	TMP_Text_t1920000777::get_offset_of_m_isCullingEnabled_109(),
	TMP_Text_t1920000777::get_offset_of_m_ignoreRectMaskCulling_110(),
	TMP_Text_t1920000777::get_offset_of_m_ignoreCulling_111(),
	TMP_Text_t1920000777::get_offset_of_m_horizontalMapping_112(),
	TMP_Text_t1920000777::get_offset_of_m_verticalMapping_113(),
	TMP_Text_t1920000777::get_offset_of_m_uvLineOffset_114(),
	TMP_Text_t1920000777::get_offset_of_m_renderMode_115(),
	TMP_Text_t1920000777::get_offset_of_m_geometrySortingOrder_116(),
	TMP_Text_t1920000777::get_offset_of_m_firstVisibleCharacter_117(),
	TMP_Text_t1920000777::get_offset_of_m_maxVisibleCharacters_118(),
	TMP_Text_t1920000777::get_offset_of_m_maxVisibleWords_119(),
	TMP_Text_t1920000777::get_offset_of_m_maxVisibleLines_120(),
	TMP_Text_t1920000777::get_offset_of_m_useMaxVisibleDescender_121(),
	TMP_Text_t1920000777::get_offset_of_m_pageToDisplay_122(),
	TMP_Text_t1920000777::get_offset_of_m_isNewPage_123(),
	TMP_Text_t1920000777::get_offset_of_m_margin_124(),
	TMP_Text_t1920000777::get_offset_of_m_marginLeft_125(),
	TMP_Text_t1920000777::get_offset_of_m_marginRight_126(),
	TMP_Text_t1920000777::get_offset_of_m_marginWidth_127(),
	TMP_Text_t1920000777::get_offset_of_m_marginHeight_128(),
	TMP_Text_t1920000777::get_offset_of_m_width_129(),
	TMP_Text_t1920000777::get_offset_of_m_textInfo_130(),
	TMP_Text_t1920000777::get_offset_of_m_havePropertiesChanged_131(),
	TMP_Text_t1920000777::get_offset_of_m_isUsingLegacyAnimationComponent_132(),
	TMP_Text_t1920000777::get_offset_of_m_transform_133(),
	TMP_Text_t1920000777::get_offset_of_m_rectTransform_134(),
	TMP_Text_t1920000777::get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135(),
	TMP_Text_t1920000777::get_offset_of_m_autoSizeTextContainer_136(),
	TMP_Text_t1920000777::get_offset_of_m_mesh_137(),
	TMP_Text_t1920000777::get_offset_of_m_isVolumetricText_138(),
	TMP_Text_t1920000777::get_offset_of_m_spriteAnimator_139(),
	TMP_Text_t1920000777::get_offset_of_m_flexibleHeight_140(),
	TMP_Text_t1920000777::get_offset_of_m_flexibleWidth_141(),
	TMP_Text_t1920000777::get_offset_of_m_minWidth_142(),
	TMP_Text_t1920000777::get_offset_of_m_minHeight_143(),
	TMP_Text_t1920000777::get_offset_of_m_maxWidth_144(),
	TMP_Text_t1920000777::get_offset_of_m_maxHeight_145(),
	TMP_Text_t1920000777::get_offset_of_m_LayoutElement_146(),
	TMP_Text_t1920000777::get_offset_of_m_preferredWidth_147(),
	TMP_Text_t1920000777::get_offset_of_m_renderedWidth_148(),
	TMP_Text_t1920000777::get_offset_of_m_isPreferredWidthDirty_149(),
	TMP_Text_t1920000777::get_offset_of_m_preferredHeight_150(),
	TMP_Text_t1920000777::get_offset_of_m_renderedHeight_151(),
	TMP_Text_t1920000777::get_offset_of_m_isPreferredHeightDirty_152(),
	TMP_Text_t1920000777::get_offset_of_m_isCalculatingPreferredValues_153(),
	TMP_Text_t1920000777::get_offset_of_m_recursiveCount_154(),
	TMP_Text_t1920000777::get_offset_of_m_layoutPriority_155(),
	TMP_Text_t1920000777::get_offset_of_m_isCalculateSizeRequired_156(),
	TMP_Text_t1920000777::get_offset_of_m_isLayoutDirty_157(),
	TMP_Text_t1920000777::get_offset_of_m_verticesAlreadyDirty_158(),
	TMP_Text_t1920000777::get_offset_of_m_layoutAlreadyDirty_159(),
	TMP_Text_t1920000777::get_offset_of_m_isAwake_160(),
	TMP_Text_t1920000777::get_offset_of_m_isInputParsingRequired_161(),
	TMP_Text_t1920000777::get_offset_of_m_inputSource_162(),
	TMP_Text_t1920000777::get_offset_of_old_text_163(),
	TMP_Text_t1920000777::get_offset_of_m_fontScale_164(),
	TMP_Text_t1920000777::get_offset_of_m_fontScaleMultiplier_165(),
	TMP_Text_t1920000777::get_offset_of_m_htmlTag_166(),
	TMP_Text_t1920000777::get_offset_of_m_xmlAttribute_167(),
	TMP_Text_t1920000777::get_offset_of_m_attributeParameterValues_168(),
	TMP_Text_t1920000777::get_offset_of_tag_LineIndent_169(),
	TMP_Text_t1920000777::get_offset_of_tag_Indent_170(),
	TMP_Text_t1920000777::get_offset_of_m_indentStack_171(),
	TMP_Text_t1920000777::get_offset_of_tag_NoParsing_172(),
	TMP_Text_t1920000777::get_offset_of_m_isParsingText_173(),
	TMP_Text_t1920000777::get_offset_of_m_FXMatrix_174(),
	TMP_Text_t1920000777::get_offset_of_m_isFXMatrixSet_175(),
	TMP_Text_t1920000777::get_offset_of_m_char_buffer_176(),
	TMP_Text_t1920000777::get_offset_of_m_internalCharacterInfo_177(),
	TMP_Text_t1920000777::get_offset_of_m_input_CharArray_178(),
	TMP_Text_t1920000777::get_offset_of_m_charArray_Length_179(),
	TMP_Text_t1920000777::get_offset_of_m_totalCharacterCount_180(),
	TMP_Text_t1920000777::get_offset_of_m_SavedWordWrapState_181(),
	TMP_Text_t1920000777::get_offset_of_m_SavedLineState_182(),
	TMP_Text_t1920000777::get_offset_of_m_characterCount_183(),
	TMP_Text_t1920000777::get_offset_of_m_firstCharacterOfLine_184(),
	TMP_Text_t1920000777::get_offset_of_m_firstVisibleCharacterOfLine_185(),
	TMP_Text_t1920000777::get_offset_of_m_lastCharacterOfLine_186(),
	TMP_Text_t1920000777::get_offset_of_m_lastVisibleCharacterOfLine_187(),
	TMP_Text_t1920000777::get_offset_of_m_lineNumber_188(),
	TMP_Text_t1920000777::get_offset_of_m_lineVisibleCharacterCount_189(),
	TMP_Text_t1920000777::get_offset_of_m_pageNumber_190(),
	TMP_Text_t1920000777::get_offset_of_m_maxAscender_191(),
	TMP_Text_t1920000777::get_offset_of_m_maxCapHeight_192(),
	TMP_Text_t1920000777::get_offset_of_m_maxDescender_193(),
	TMP_Text_t1920000777::get_offset_of_m_maxLineAscender_194(),
	TMP_Text_t1920000777::get_offset_of_m_maxLineDescender_195(),
	TMP_Text_t1920000777::get_offset_of_m_startOfLineAscender_196(),
	TMP_Text_t1920000777::get_offset_of_m_lineOffset_197(),
	TMP_Text_t1920000777::get_offset_of_m_meshExtents_198(),
	TMP_Text_t1920000777::get_offset_of_m_htmlColor_199(),
	TMP_Text_t1920000777::get_offset_of_m_colorStack_200(),
	TMP_Text_t1920000777::get_offset_of_m_underlineColorStack_201(),
	TMP_Text_t1920000777::get_offset_of_m_strikethroughColorStack_202(),
	TMP_Text_t1920000777::get_offset_of_m_highlightColorStack_203(),
	TMP_Text_t1920000777::get_offset_of_m_colorGradientPreset_204(),
	TMP_Text_t1920000777::get_offset_of_m_colorGradientStack_205(),
	TMP_Text_t1920000777::get_offset_of_m_tabSpacing_206(),
	TMP_Text_t1920000777::get_offset_of_m_spacing_207(),
	TMP_Text_t1920000777::get_offset_of_m_styleStack_208(),
	TMP_Text_t1920000777::get_offset_of_m_actionStack_209(),
	TMP_Text_t1920000777::get_offset_of_m_padding_210(),
	TMP_Text_t1920000777::get_offset_of_m_baselineOffset_211(),
	TMP_Text_t1920000777::get_offset_of_m_baselineOffsetStack_212(),
	TMP_Text_t1920000777::get_offset_of_m_xAdvance_213(),
	TMP_Text_t1920000777::get_offset_of_m_textElementType_214(),
	TMP_Text_t1920000777::get_offset_of_m_cached_TextElement_215(),
	TMP_Text_t1920000777::get_offset_of_m_cached_Underline_GlyphInfo_216(),
	TMP_Text_t1920000777::get_offset_of_m_cached_Ellipsis_GlyphInfo_217(),
	TMP_Text_t1920000777::get_offset_of_m_defaultSpriteAsset_218(),
	TMP_Text_t1920000777::get_offset_of_m_currentSpriteAsset_219(),
	TMP_Text_t1920000777::get_offset_of_m_spriteCount_220(),
	TMP_Text_t1920000777::get_offset_of_m_spriteIndex_221(),
	TMP_Text_t1920000777::get_offset_of_m_spriteAnimationID_222(),
	TMP_Text_t1920000777::get_offset_of_m_ignoreActiveState_223(),
	TMP_Text_t1920000777::get_offset_of_k_Power_224(),
	TMP_Text_t1920000777_StaticFields::get_offset_of_k_LargePositiveVector2_225(),
	TMP_Text_t1920000777_StaticFields::get_offset_of_k_LargeNegativeVector2_226(),
	TMP_Text_t1920000777_StaticFields::get_offset_of_k_LargePositiveFloat_227(),
	TMP_Text_t1920000777_StaticFields::get_offset_of_k_LargeNegativeFloat_228(),
	TMP_Text_t1920000777_StaticFields::get_offset_of_k_LargePositiveInt_229(),
	TMP_Text_t1920000777_StaticFields::get_offset_of_k_LargeNegativeInt_230(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { sizeof (TextInputSources_t434791461)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2781[5] = 
{
	TextInputSources_t434791461::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { sizeof (TMP_TextElement_t2285620223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[9] = 
{
	TMP_TextElement_t2285620223::get_offset_of_id_0(),
	TMP_TextElement_t2285620223::get_offset_of_x_1(),
	TMP_TextElement_t2285620223::get_offset_of_y_2(),
	TMP_TextElement_t2285620223::get_offset_of_width_3(),
	TMP_TextElement_t2285620223::get_offset_of_height_4(),
	TMP_TextElement_t2285620223::get_offset_of_xOffset_5(),
	TMP_TextElement_t2285620223::get_offset_of_yOffset_6(),
	TMP_TextElement_t2285620223::get_offset_of_xAdvance_7(),
	TMP_TextElement_t2285620223::get_offset_of_scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { sizeof (TMP_TextInfo_t2849466151), -1, sizeof(TMP_TextInfo_t2849466151_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2783[18] = 
{
	TMP_TextInfo_t2849466151_StaticFields::get_offset_of_k_InfinityVectorPositive_0(),
	TMP_TextInfo_t2849466151_StaticFields::get_offset_of_k_InfinityVectorNegative_1(),
	TMP_TextInfo_t2849466151::get_offset_of_textComponent_2(),
	TMP_TextInfo_t2849466151::get_offset_of_characterCount_3(),
	TMP_TextInfo_t2849466151::get_offset_of_spriteCount_4(),
	TMP_TextInfo_t2849466151::get_offset_of_spaceCount_5(),
	TMP_TextInfo_t2849466151::get_offset_of_wordCount_6(),
	TMP_TextInfo_t2849466151::get_offset_of_linkCount_7(),
	TMP_TextInfo_t2849466151::get_offset_of_lineCount_8(),
	TMP_TextInfo_t2849466151::get_offset_of_pageCount_9(),
	TMP_TextInfo_t2849466151::get_offset_of_materialCount_10(),
	TMP_TextInfo_t2849466151::get_offset_of_characterInfo_11(),
	TMP_TextInfo_t2849466151::get_offset_of_wordInfo_12(),
	TMP_TextInfo_t2849466151::get_offset_of_linkInfo_13(),
	TMP_TextInfo_t2849466151::get_offset_of_lineInfo_14(),
	TMP_TextInfo_t2849466151::get_offset_of_pageInfo_15(),
	TMP_TextInfo_t2849466151::get_offset_of_meshInfo_16(),
	TMP_TextInfo_t2849466151::get_offset_of_m_CachedMeshInfo_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { sizeof (CaretPosition_t420625986)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2784[4] = 
{
	CaretPosition_t420625986::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { sizeof (CaretInfo_t598977269)+ sizeof (RuntimeObject), sizeof(CaretInfo_t598977269 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2785[2] = 
{
	CaretInfo_t598977269::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CaretInfo_t598977269::get_offset_of_position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { sizeof (TMP_TextUtilities_t2068549775), -1, sizeof(TMP_TextUtilities_t2068549775_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2786[3] = 
{
	TMP_TextUtilities_t2068549775_StaticFields::get_offset_of_m_rectWorldCorners_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { sizeof (LineSegment_t2997084511)+ sizeof (RuntimeObject), sizeof(LineSegment_t2997084511 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2787[2] = 
{
	LineSegment_t2997084511::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LineSegment_t2997084511::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { sizeof (TMP_UpdateManager_t505251708), -1, sizeof(TMP_UpdateManager_t505251708_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2788[5] = 
{
	TMP_UpdateManager_t505251708_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateManager_t505251708::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateManager_t505251708::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateManager_t505251708::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateManager_t505251708::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { sizeof (TMP_UpdateRegistry_t2664963242), -1, sizeof(TMP_UpdateRegistry_t2664963242_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2789[5] = 
{
	TMP_UpdateRegistry_t2664963242_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateRegistry_t2664963242::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateRegistry_t2664963242::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateRegistry_t2664963242::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateRegistry_t2664963242::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { sizeof (TMP_BasicXmlTagStack_t937156555)+ sizeof (RuntimeObject), sizeof(TMP_BasicXmlTagStack_t937156555 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2790[10] = 
{
	TMP_BasicXmlTagStack_t937156555::get_offset_of_bold_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t937156555::get_offset_of_italic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t937156555::get_offset_of_underline_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t937156555::get_offset_of_strikethrough_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t937156555::get_offset_of_highlight_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t937156555::get_offset_of_superscript_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t937156555::get_offset_of_subscript_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t937156555::get_offset_of_uppercase_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t937156555::get_offset_of_lowercase_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_BasicXmlTagStack_t937156555::get_offset_of_smallcaps_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305145), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2792[2] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t1486305145_StaticFields::get_offset_of_U24fieldU2D9E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { sizeof (U24ArrayTypeU3D12_t1568637718)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t1568637718 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { sizeof (U24ArrayTypeU3D40_t2731437126)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D40_t2731437126 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { sizeof (U3CModuleU3E_t3783534239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { sizeof (GyroControl_t1958351024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[4] = 
{
	GyroControl_t1958351024::get_offset_of_gyroEnabled_2(),
	GyroControl_t1958351024::get_offset_of_gyro_3(),
	GyroControl_t1958351024::get_offset_of_cameraContainer_4(),
	GyroControl_t1958351024::get_offset_of_rot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { sizeof (Movement_t2096174109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[2] = 
{
	Movement_t2096174109::get_offset_of_mouseSensitivity_2(),
	Movement_t2096174109::get_offset_of_xAxisClamp_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { sizeof (Bezier_t2878762569), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { sizeof (BezierPath_t978602816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[3] = 
{
	BezierPath_t978602816::get_offset_of_points_0(),
	BezierPath_t978602816::get_offset_of_isClosed_1(),
	BezierPath_t978602816::get_offset_of_autoSetControlPoints_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
