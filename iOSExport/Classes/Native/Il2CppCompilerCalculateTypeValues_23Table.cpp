﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Action`1<System.Action>
struct Action_1_t3028271134;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t2207932334;
// System.String
struct String_t;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t2624936259;
// System.Type
struct Type_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1736152084;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t152480188;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Amazon.Runtime.Internal.DownloadHandlerBufferWrapper
struct DownloadHandlerBufferWrapper_t1800693841;
// Amazon.Runtime.Internal.UploadHandlerRawWrapper
struct UploadHandlerRawWrapper_t3528989734;
// ThirdParty.Ionic.Zlib.CrcCalculatorStream
struct CrcCalculatorStream_t2228597532;
// Amazon.Runtime.Internal.Transform.IWebResponseData
struct IWebResponseData_t3815728244;
// Amazon.Runtime.Internal.Util.CachingWrapperStream
struct CachingWrapperStream_t380166576;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.IFormatProvider
struct IFormatProvider_t2849799027;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t3116948387;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Func`2<Amazon.Runtime.Metric,System.String>
struct Func_2_t1028326184;
// System.Diagnostics.Stopwatch
struct Stopwatch_t1380178105;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing>
struct Dictionary_2_t3597272751;
// System.Collections.Generic.List`1<Amazon.Runtime.Internal.Util.MetricError>
struct List_1_t333565938;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>>
struct Dictionary_2_t513681620;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>>
struct Dictionary_2_t2292610545;
// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64>
struct Dictionary_2_t3659156526;
// System.Text.RegularExpressions.Regex
struct Regex_t1803876613;
// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger>
struct IDictionary_2_t2198939132;
// System.Collections.Generic.List`1<Amazon.Runtime.Internal.Util.InternalLogger>
struct List_1_t2341494475;
// Amazon.Runtime.Internal.Util.ILogger
struct ILogger_t676853571;
// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t3046128587;
// System.Threading.Thread
struct Thread_t241561612;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;
// Amazon.Runtime.Internal.UnityRequest
struct UnityRequest_t4218620704;
// Amazon.Runtime.Internal.UnityWwwRequest
struct UnityWwwRequest_t118609659;
// System.Uri
struct Uri_t19570940;
// System.IDisposable
struct IDisposable_t2427283555;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// System.Exception
struct Exception_t1927440687;
// Amazon.Runtime.Internal.StreamReadTracker
struct StreamReadTracker_t1958363340;
// Amazon.Runtime.IPipelineHandler
struct IPipelineHandler_t2320756001;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1656058977;
// System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest>
struct Queue_1_t1679560232;
// System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult>
struct Queue_1_t4099012848;
// Amazon.Runtime.AmazonWebServiceResponse
struct AmazonWebServiceResponse_t529043356;
// Amazon.Runtime.AmazonWebServiceRequest
struct AmazonWebServiceRequest_t3384026212;
// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions>
struct Action_4_t897279368;
// Amazon.Runtime.AsyncOptions
struct AsyncOptions_t558351272;
// Amazon.Runtime.Internal.Auth.AWS4Signer
struct AWS4Signer_t1011449585;
// Amazon.Runtime.Internal.UnityMainThreadDispatcher
struct UnityMainThreadDispatcher_t4098072663;
// Amazon.Runtime.IUnityHttpRequest
struct IUnityHttpRequest_t1859903397;
// UnityEngine.WWW
struct WWW_t2919945039;
// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t142100588;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// Amazon.Runtime.Internal.UnityWebRequestWrapper
struct UnityWebRequestWrapper_t1542496577;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>
struct Func_2_t4206299577;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.Boolean>
struct Func_2_t1707686766;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.IO.Stream
struct Stream_t3255436806;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Collections.Specialized.StringDictionary
struct StringDictionary_t1070889667;
// System.Action`2<Amazon.Runtime.IExecutionContext,System.Exception>
struct Action_2_t3657272536;
// Amazon.Runtime.AWSCredentials
struct AWSCredentials_t3583921007;
// Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext>
struct ThreadPoolThrottler_1_t736579114;
// Amazon.Runtime.RetryPolicy
struct RetryPolicy_t1476739446;
// System.Action`1<Amazon.Runtime.IExecutionContext>
struct Action_1_t2278930134;
// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.IExceptionHandler>
struct IDictionary_2_t1347456016;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// Amazon.Runtime.Internal.Util.IHashingWrapper
struct IHashingWrapper_t2610776002;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t3052225568;
// Amazon.Util.Internal.ITypeInfo
struct ITypeInfo_t3592676621;
// Amazon.Runtime.Internal.Util.RequestMetrics
struct RequestMetrics_t218029284;
// Amazon.Runtime.Internal.CapacityManager
struct CapacityManager_t2181902141;
// Amazon.Runtime.Internal.RetryCapacity
struct RetryCapacity_t2794668894;
// System.Collections.Generic.ICollection`1<System.Net.HttpStatusCode>
struct ICollection_1_t2850484946;
// System.Collections.Generic.ICollection`1<System.Net.WebExceptionStatus>
struct ICollection_1_t2121448836;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t1578612233;
// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType>
struct HashSet_1_t3367932747;
// System.IO.StreamReader
struct StreamReader_t2360341767;
// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3799711356;
// System.String[]
struct StringU5BU5D_t1642385972;
// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct AWS4SigningResult_t430803065;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// ThirdParty.Json.LitJson.JsonReader
struct JsonReader_t354941621;
// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack
struct JsonPathStack_t805090354;
// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3783534236_H
#define U3CMODULEU3E_T3783534236_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534236 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534236_H
#ifndef EXTENSIONS_T2019041272_H
#define EXTENSIONS_T2019041272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.Extensions
struct  Extensions_t2019041272  : public RuntimeObject
{
public:

public:
};

struct Extensions_t2019041272_StaticFields
{
public:
	// System.Int64 Amazon.Runtime.Internal.Util.Extensions::ticksPerSecond
	int64_t ___ticksPerSecond_0;
	// System.Double Amazon.Runtime.Internal.Util.Extensions::tickFrequency
	double ___tickFrequency_1;

public:
	inline static int32_t get_offset_of_ticksPerSecond_0() { return static_cast<int32_t>(offsetof(Extensions_t2019041272_StaticFields, ___ticksPerSecond_0)); }
	inline int64_t get_ticksPerSecond_0() const { return ___ticksPerSecond_0; }
	inline int64_t* get_address_of_ticksPerSecond_0() { return &___ticksPerSecond_0; }
	inline void set_ticksPerSecond_0(int64_t value)
	{
		___ticksPerSecond_0 = value;
	}

	inline static int32_t get_offset_of_tickFrequency_1() { return static_cast<int32_t>(offsetof(Extensions_t2019041272_StaticFields, ___tickFrequency_1)); }
	inline double get_tickFrequency_1() const { return ___tickFrequency_1; }
	inline double* get_address_of_tickFrequency_1() { return &___tickFrequency_1; }
	inline void set_tickFrequency_1(double value)
	{
		___tickFrequency_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T2019041272_H
#ifndef U3CU3EC_T464705533_H
#define U3CU3EC_T464705533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c
struct  U3CU3Ec_t464705533  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t464705533_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::<>9
	U3CU3Ec_t464705533 * ___U3CU3E9_0;
	// System.Action`1<System.Action> Amazon.Runtime.Internal.Util.BackgroundInvoker/<>c::<>9__0_0
	Action_1_t3028271134 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t464705533_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t464705533 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t464705533 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t464705533 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t464705533_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Action_1_t3028271134 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Action_1_t3028271134 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Action_1_t3028271134 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T464705533_H
#ifndef HASHING_T180418764_H
#define HASHING_T180418764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.Hashing
struct  Hashing_t180418764  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHING_T180418764_H
#ifndef U3CU3EC_T3244221195_H
#define U3CU3EC_T3244221195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.Hashing/<>c
struct  U3CU3Ec_t3244221195  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3244221195_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.Hashing/<>c Amazon.Runtime.Internal.Util.Hashing/<>c::<>9
	U3CU3Ec_t3244221195 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.Int32> Amazon.Runtime.Internal.Util.Hashing/<>c::<>9__0_0
	Func_2_t2207932334 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3244221195_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3244221195 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3244221195 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3244221195 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3244221195_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_t2207932334 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_t2207932334 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_t2207932334 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3244221195_H
#ifndef HASHINGWRAPPER_T516426937_H
#define HASHINGWRAPPER_T516426937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.HashingWrapper
struct  HashingWrapper_t516426937  : public RuntimeObject
{
public:
	// System.Security.Cryptography.HashAlgorithm Amazon.Runtime.Internal.Util.HashingWrapper::_algorithm
	HashAlgorithm_t2624936259 * ____algorithm_1;

public:
	inline static int32_t get_offset_of__algorithm_1() { return static_cast<int32_t>(offsetof(HashingWrapper_t516426937, ____algorithm_1)); }
	inline HashAlgorithm_t2624936259 * get__algorithm_1() const { return ____algorithm_1; }
	inline HashAlgorithm_t2624936259 ** get_address_of__algorithm_1() { return &____algorithm_1; }
	inline void set__algorithm_1(HashAlgorithm_t2624936259 * value)
	{
		____algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&____algorithm_1), value);
	}
};

struct HashingWrapper_t516426937_StaticFields
{
public:
	// System.String Amazon.Runtime.Internal.Util.HashingWrapper::MD5ManagedName
	String_t* ___MD5ManagedName_0;

public:
	inline static int32_t get_offset_of_MD5ManagedName_0() { return static_cast<int32_t>(offsetof(HashingWrapper_t516426937_StaticFields, ___MD5ManagedName_0)); }
	inline String_t* get_MD5ManagedName_0() const { return ___MD5ManagedName_0; }
	inline String_t** get_address_of_MD5ManagedName_0() { return &___MD5ManagedName_0; }
	inline void set_MD5ManagedName_0(String_t* value)
	{
		___MD5ManagedName_0 = value;
		Il2CppCodeGenWriteBarrier((&___MD5ManagedName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHINGWRAPPER_T516426937_H
#ifndef STRINGUNMARSHALLER_T3953260147_H
#define STRINGUNMARSHALLER_T3953260147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.StringUnmarshaller
struct  StringUnmarshaller_t3953260147  : public RuntimeObject
{
public:

public:
};

struct StringUnmarshaller_t3953260147_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.StringUnmarshaller Amazon.Runtime.Internal.Transform.StringUnmarshaller::_instance
	StringUnmarshaller_t3953260147 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(StringUnmarshaller_t3953260147_StaticFields, ____instance_0)); }
	inline StringUnmarshaller_t3953260147 * get__instance_0() const { return ____instance_0; }
	inline StringUnmarshaller_t3953260147 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(StringUnmarshaller_t3953260147 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUNMARSHALLER_T3953260147_H
#ifndef UPLOADHANDLERRAWWRAPPER_T3528989734_H
#define UPLOADHANDLERRAWWRAPPER_T3528989734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UploadHandlerRawWrapper
struct  UploadHandlerRawWrapper_t3528989734  : public RuntimeObject
{
public:
	// System.Object Amazon.Runtime.Internal.UploadHandlerRawWrapper::<Instance>k__BackingField
	RuntimeObject * ___U3CInstanceU3Ek__BackingField_1;
	// System.Boolean Amazon.Runtime.Internal.UploadHandlerRawWrapper::disposedValue
	bool ___disposedValue_2;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UploadHandlerRawWrapper_t3528989734, ___U3CInstanceU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CInstanceU3Ek__BackingField_1() const { return ___U3CInstanceU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CInstanceU3Ek__BackingField_1() { return &___U3CInstanceU3Ek__BackingField_1; }
	inline void set_U3CInstanceU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CInstanceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_disposedValue_2() { return static_cast<int32_t>(offsetof(UploadHandlerRawWrapper_t3528989734, ___disposedValue_2)); }
	inline bool get_disposedValue_2() const { return ___disposedValue_2; }
	inline bool* get_address_of_disposedValue_2() { return &___disposedValue_2; }
	inline void set_disposedValue_2(bool value)
	{
		___disposedValue_2 = value;
	}
};

struct UploadHandlerRawWrapper_t3528989734_StaticFields
{
public:
	// System.Type Amazon.Runtime.Internal.UploadHandlerRawWrapper::uploadHandlerRawType
	Type_t * ___uploadHandlerRawType_0;

public:
	inline static int32_t get_offset_of_uploadHandlerRawType_0() { return static_cast<int32_t>(offsetof(UploadHandlerRawWrapper_t3528989734_StaticFields, ___uploadHandlerRawType_0)); }
	inline Type_t * get_uploadHandlerRawType_0() const { return ___uploadHandlerRawType_0; }
	inline Type_t ** get_address_of_uploadHandlerRawType_0() { return &___uploadHandlerRawType_0; }
	inline void set_uploadHandlerRawType_0(Type_t * value)
	{
		___uploadHandlerRawType_0 = value;
		Il2CppCodeGenWriteBarrier((&___uploadHandlerRawType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPLOADHANDLERRAWWRAPPER_T3528989734_H
#ifndef DOWNLOADHANDLERBUFFERWRAPPER_T1800693841_H
#define DOWNLOADHANDLERBUFFERWRAPPER_T1800693841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.DownloadHandlerBufferWrapper
struct  DownloadHandlerBufferWrapper_t1800693841  : public RuntimeObject
{
public:
	// System.Object Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::<Instance>k__BackingField
	RuntimeObject * ___U3CInstanceU3Ek__BackingField_5;
	// System.Boolean Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::disposedValue
	bool ___disposedValue_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841, ___U3CInstanceU3Ek__BackingField_5)); }
	inline RuntimeObject * get_U3CInstanceU3Ek__BackingField_5() const { return ___U3CInstanceU3Ek__BackingField_5; }
	inline RuntimeObject ** get_address_of_U3CInstanceU3Ek__BackingField_5() { return &___U3CInstanceU3Ek__BackingField_5; }
	inline void set_U3CInstanceU3Ek__BackingField_5(RuntimeObject * value)
	{
		___U3CInstanceU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_disposedValue_6() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841, ___disposedValue_6)); }
	inline bool get_disposedValue_6() const { return ___disposedValue_6; }
	inline bool* get_address_of_disposedValue_6() { return &___disposedValue_6; }
	inline void set_disposedValue_6(bool value)
	{
		___disposedValue_6 = value;
	}
};

struct DownloadHandlerBufferWrapper_t1800693841_StaticFields
{
public:
	// System.Type Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::downloadHandlerBufferType
	Type_t * ___downloadHandlerBufferType_0;
	// System.Reflection.PropertyInfo[] Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::downloadHandlerBufferProperties
	PropertyInfoU5BU5D_t1736152084* ___downloadHandlerBufferProperties_1;
	// System.Reflection.MethodInfo[] Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::downloadHandlerBufferMethods
	MethodInfoU5BU5D_t152480188* ___downloadHandlerBufferMethods_2;
	// System.Reflection.PropertyInfo Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::dataProperty
	PropertyInfo_t * ___dataProperty_3;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.DownloadHandlerBufferWrapper::dataGetMethod
	MethodInfo_t * ___dataGetMethod_4;

public:
	inline static int32_t get_offset_of_downloadHandlerBufferType_0() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___downloadHandlerBufferType_0)); }
	inline Type_t * get_downloadHandlerBufferType_0() const { return ___downloadHandlerBufferType_0; }
	inline Type_t ** get_address_of_downloadHandlerBufferType_0() { return &___downloadHandlerBufferType_0; }
	inline void set_downloadHandlerBufferType_0(Type_t * value)
	{
		___downloadHandlerBufferType_0 = value;
		Il2CppCodeGenWriteBarrier((&___downloadHandlerBufferType_0), value);
	}

	inline static int32_t get_offset_of_downloadHandlerBufferProperties_1() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___downloadHandlerBufferProperties_1)); }
	inline PropertyInfoU5BU5D_t1736152084* get_downloadHandlerBufferProperties_1() const { return ___downloadHandlerBufferProperties_1; }
	inline PropertyInfoU5BU5D_t1736152084** get_address_of_downloadHandlerBufferProperties_1() { return &___downloadHandlerBufferProperties_1; }
	inline void set_downloadHandlerBufferProperties_1(PropertyInfoU5BU5D_t1736152084* value)
	{
		___downloadHandlerBufferProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___downloadHandlerBufferProperties_1), value);
	}

	inline static int32_t get_offset_of_downloadHandlerBufferMethods_2() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___downloadHandlerBufferMethods_2)); }
	inline MethodInfoU5BU5D_t152480188* get_downloadHandlerBufferMethods_2() const { return ___downloadHandlerBufferMethods_2; }
	inline MethodInfoU5BU5D_t152480188** get_address_of_downloadHandlerBufferMethods_2() { return &___downloadHandlerBufferMethods_2; }
	inline void set_downloadHandlerBufferMethods_2(MethodInfoU5BU5D_t152480188* value)
	{
		___downloadHandlerBufferMethods_2 = value;
		Il2CppCodeGenWriteBarrier((&___downloadHandlerBufferMethods_2), value);
	}

	inline static int32_t get_offset_of_dataProperty_3() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___dataProperty_3)); }
	inline PropertyInfo_t * get_dataProperty_3() const { return ___dataProperty_3; }
	inline PropertyInfo_t ** get_address_of_dataProperty_3() { return &___dataProperty_3; }
	inline void set_dataProperty_3(PropertyInfo_t * value)
	{
		___dataProperty_3 = value;
		Il2CppCodeGenWriteBarrier((&___dataProperty_3), value);
	}

	inline static int32_t get_offset_of_dataGetMethod_4() { return static_cast<int32_t>(offsetof(DownloadHandlerBufferWrapper_t1800693841_StaticFields, ___dataGetMethod_4)); }
	inline MethodInfo_t * get_dataGetMethod_4() const { return ___dataGetMethod_4; }
	inline MethodInfo_t ** get_address_of_dataGetMethod_4() { return &___dataGetMethod_4; }
	inline void set_dataGetMethod_4(MethodInfo_t * value)
	{
		___dataGetMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&___dataGetMethod_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADHANDLERBUFFERWRAPPER_T1800693841_H
#ifndef UNITYWEBREQUESTWRAPPER_T1542496577_H
#define UNITYWEBREQUESTWRAPPER_T1542496577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityWebRequestWrapper
struct  UnityWebRequestWrapper_t1542496577  : public RuntimeObject
{
public:
	// System.Object Amazon.Runtime.Internal.UnityWebRequestWrapper::unityWebRequestInstance
	RuntimeObject * ___unityWebRequestInstance_15;
	// Amazon.Runtime.Internal.DownloadHandlerBufferWrapper Amazon.Runtime.Internal.UnityWebRequestWrapper::downloadHandler
	DownloadHandlerBufferWrapper_t1800693841 * ___downloadHandler_16;
	// Amazon.Runtime.Internal.UploadHandlerRawWrapper Amazon.Runtime.Internal.UnityWebRequestWrapper::uploadHandler
	UploadHandlerRawWrapper_t3528989734 * ___uploadHandler_17;
	// System.Boolean Amazon.Runtime.Internal.UnityWebRequestWrapper::disposedValue
	bool ___disposedValue_18;

public:
	inline static int32_t get_offset_of_unityWebRequestInstance_15() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577, ___unityWebRequestInstance_15)); }
	inline RuntimeObject * get_unityWebRequestInstance_15() const { return ___unityWebRequestInstance_15; }
	inline RuntimeObject ** get_address_of_unityWebRequestInstance_15() { return &___unityWebRequestInstance_15; }
	inline void set_unityWebRequestInstance_15(RuntimeObject * value)
	{
		___unityWebRequestInstance_15 = value;
		Il2CppCodeGenWriteBarrier((&___unityWebRequestInstance_15), value);
	}

	inline static int32_t get_offset_of_downloadHandler_16() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577, ___downloadHandler_16)); }
	inline DownloadHandlerBufferWrapper_t1800693841 * get_downloadHandler_16() const { return ___downloadHandler_16; }
	inline DownloadHandlerBufferWrapper_t1800693841 ** get_address_of_downloadHandler_16() { return &___downloadHandler_16; }
	inline void set_downloadHandler_16(DownloadHandlerBufferWrapper_t1800693841 * value)
	{
		___downloadHandler_16 = value;
		Il2CppCodeGenWriteBarrier((&___downloadHandler_16), value);
	}

	inline static int32_t get_offset_of_uploadHandler_17() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577, ___uploadHandler_17)); }
	inline UploadHandlerRawWrapper_t3528989734 * get_uploadHandler_17() const { return ___uploadHandler_17; }
	inline UploadHandlerRawWrapper_t3528989734 ** get_address_of_uploadHandler_17() { return &___uploadHandler_17; }
	inline void set_uploadHandler_17(UploadHandlerRawWrapper_t3528989734 * value)
	{
		___uploadHandler_17 = value;
		Il2CppCodeGenWriteBarrier((&___uploadHandler_17), value);
	}

	inline static int32_t get_offset_of_disposedValue_18() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577, ___disposedValue_18)); }
	inline bool get_disposedValue_18() const { return ___disposedValue_18; }
	inline bool* get_address_of_disposedValue_18() { return &___disposedValue_18; }
	inline void set_disposedValue_18(bool value)
	{
		___disposedValue_18 = value;
	}
};

struct UnityWebRequestWrapper_t1542496577_StaticFields
{
public:
	// System.Type Amazon.Runtime.Internal.UnityWebRequestWrapper::unityWebRequestType
	Type_t * ___unityWebRequestType_0;
	// System.Reflection.PropertyInfo[] Amazon.Runtime.Internal.UnityWebRequestWrapper::unityWebRequestProperties
	PropertyInfoU5BU5D_t1736152084* ___unityWebRequestProperties_1;
	// System.Reflection.MethodInfo[] Amazon.Runtime.Internal.UnityWebRequestWrapper::unityWebRequestMethods
	MethodInfoU5BU5D_t152480188* ___unityWebRequestMethods_2;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::setRequestHeaderMethod
	MethodInfo_t * ___setRequestHeaderMethod_3;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::sendMethod
	MethodInfo_t * ___sendMethod_4;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::getResponseHeadersMethod
	MethodInfo_t * ___getResponseHeadersMethod_5;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::isDoneGetMethod
	MethodInfo_t * ___isDoneGetMethod_6;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::downloadProgressGetMethod
	MethodInfo_t * ___downloadProgressGetMethod_7;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::uploadProgressGetMethod
	MethodInfo_t * ___uploadProgressGetMethod_8;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::isErrorGetMethod
	MethodInfo_t * ___isErrorGetMethod_9;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::downloadedBytesGetMethod
	MethodInfo_t * ___downloadedBytesGetMethod_10;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::responseCodeGetMethod
	MethodInfo_t * ___responseCodeGetMethod_11;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::downloadHandlerSetMethod
	MethodInfo_t * ___downloadHandlerSetMethod_12;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::uploadHandlerSetMethod
	MethodInfo_t * ___uploadHandlerSetMethod_13;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.UnityWebRequestWrapper::errorGetMethod
	MethodInfo_t * ___errorGetMethod_14;

public:
	inline static int32_t get_offset_of_unityWebRequestType_0() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___unityWebRequestType_0)); }
	inline Type_t * get_unityWebRequestType_0() const { return ___unityWebRequestType_0; }
	inline Type_t ** get_address_of_unityWebRequestType_0() { return &___unityWebRequestType_0; }
	inline void set_unityWebRequestType_0(Type_t * value)
	{
		___unityWebRequestType_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityWebRequestType_0), value);
	}

	inline static int32_t get_offset_of_unityWebRequestProperties_1() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___unityWebRequestProperties_1)); }
	inline PropertyInfoU5BU5D_t1736152084* get_unityWebRequestProperties_1() const { return ___unityWebRequestProperties_1; }
	inline PropertyInfoU5BU5D_t1736152084** get_address_of_unityWebRequestProperties_1() { return &___unityWebRequestProperties_1; }
	inline void set_unityWebRequestProperties_1(PropertyInfoU5BU5D_t1736152084* value)
	{
		___unityWebRequestProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___unityWebRequestProperties_1), value);
	}

	inline static int32_t get_offset_of_unityWebRequestMethods_2() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___unityWebRequestMethods_2)); }
	inline MethodInfoU5BU5D_t152480188* get_unityWebRequestMethods_2() const { return ___unityWebRequestMethods_2; }
	inline MethodInfoU5BU5D_t152480188** get_address_of_unityWebRequestMethods_2() { return &___unityWebRequestMethods_2; }
	inline void set_unityWebRequestMethods_2(MethodInfoU5BU5D_t152480188* value)
	{
		___unityWebRequestMethods_2 = value;
		Il2CppCodeGenWriteBarrier((&___unityWebRequestMethods_2), value);
	}

	inline static int32_t get_offset_of_setRequestHeaderMethod_3() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___setRequestHeaderMethod_3)); }
	inline MethodInfo_t * get_setRequestHeaderMethod_3() const { return ___setRequestHeaderMethod_3; }
	inline MethodInfo_t ** get_address_of_setRequestHeaderMethod_3() { return &___setRequestHeaderMethod_3; }
	inline void set_setRequestHeaderMethod_3(MethodInfo_t * value)
	{
		___setRequestHeaderMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&___setRequestHeaderMethod_3), value);
	}

	inline static int32_t get_offset_of_sendMethod_4() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___sendMethod_4)); }
	inline MethodInfo_t * get_sendMethod_4() const { return ___sendMethod_4; }
	inline MethodInfo_t ** get_address_of_sendMethod_4() { return &___sendMethod_4; }
	inline void set_sendMethod_4(MethodInfo_t * value)
	{
		___sendMethod_4 = value;
		Il2CppCodeGenWriteBarrier((&___sendMethod_4), value);
	}

	inline static int32_t get_offset_of_getResponseHeadersMethod_5() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___getResponseHeadersMethod_5)); }
	inline MethodInfo_t * get_getResponseHeadersMethod_5() const { return ___getResponseHeadersMethod_5; }
	inline MethodInfo_t ** get_address_of_getResponseHeadersMethod_5() { return &___getResponseHeadersMethod_5; }
	inline void set_getResponseHeadersMethod_5(MethodInfo_t * value)
	{
		___getResponseHeadersMethod_5 = value;
		Il2CppCodeGenWriteBarrier((&___getResponseHeadersMethod_5), value);
	}

	inline static int32_t get_offset_of_isDoneGetMethod_6() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___isDoneGetMethod_6)); }
	inline MethodInfo_t * get_isDoneGetMethod_6() const { return ___isDoneGetMethod_6; }
	inline MethodInfo_t ** get_address_of_isDoneGetMethod_6() { return &___isDoneGetMethod_6; }
	inline void set_isDoneGetMethod_6(MethodInfo_t * value)
	{
		___isDoneGetMethod_6 = value;
		Il2CppCodeGenWriteBarrier((&___isDoneGetMethod_6), value);
	}

	inline static int32_t get_offset_of_downloadProgressGetMethod_7() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___downloadProgressGetMethod_7)); }
	inline MethodInfo_t * get_downloadProgressGetMethod_7() const { return ___downloadProgressGetMethod_7; }
	inline MethodInfo_t ** get_address_of_downloadProgressGetMethod_7() { return &___downloadProgressGetMethod_7; }
	inline void set_downloadProgressGetMethod_7(MethodInfo_t * value)
	{
		___downloadProgressGetMethod_7 = value;
		Il2CppCodeGenWriteBarrier((&___downloadProgressGetMethod_7), value);
	}

	inline static int32_t get_offset_of_uploadProgressGetMethod_8() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___uploadProgressGetMethod_8)); }
	inline MethodInfo_t * get_uploadProgressGetMethod_8() const { return ___uploadProgressGetMethod_8; }
	inline MethodInfo_t ** get_address_of_uploadProgressGetMethod_8() { return &___uploadProgressGetMethod_8; }
	inline void set_uploadProgressGetMethod_8(MethodInfo_t * value)
	{
		___uploadProgressGetMethod_8 = value;
		Il2CppCodeGenWriteBarrier((&___uploadProgressGetMethod_8), value);
	}

	inline static int32_t get_offset_of_isErrorGetMethod_9() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___isErrorGetMethod_9)); }
	inline MethodInfo_t * get_isErrorGetMethod_9() const { return ___isErrorGetMethod_9; }
	inline MethodInfo_t ** get_address_of_isErrorGetMethod_9() { return &___isErrorGetMethod_9; }
	inline void set_isErrorGetMethod_9(MethodInfo_t * value)
	{
		___isErrorGetMethod_9 = value;
		Il2CppCodeGenWriteBarrier((&___isErrorGetMethod_9), value);
	}

	inline static int32_t get_offset_of_downloadedBytesGetMethod_10() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___downloadedBytesGetMethod_10)); }
	inline MethodInfo_t * get_downloadedBytesGetMethod_10() const { return ___downloadedBytesGetMethod_10; }
	inline MethodInfo_t ** get_address_of_downloadedBytesGetMethod_10() { return &___downloadedBytesGetMethod_10; }
	inline void set_downloadedBytesGetMethod_10(MethodInfo_t * value)
	{
		___downloadedBytesGetMethod_10 = value;
		Il2CppCodeGenWriteBarrier((&___downloadedBytesGetMethod_10), value);
	}

	inline static int32_t get_offset_of_responseCodeGetMethod_11() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___responseCodeGetMethod_11)); }
	inline MethodInfo_t * get_responseCodeGetMethod_11() const { return ___responseCodeGetMethod_11; }
	inline MethodInfo_t ** get_address_of_responseCodeGetMethod_11() { return &___responseCodeGetMethod_11; }
	inline void set_responseCodeGetMethod_11(MethodInfo_t * value)
	{
		___responseCodeGetMethod_11 = value;
		Il2CppCodeGenWriteBarrier((&___responseCodeGetMethod_11), value);
	}

	inline static int32_t get_offset_of_downloadHandlerSetMethod_12() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___downloadHandlerSetMethod_12)); }
	inline MethodInfo_t * get_downloadHandlerSetMethod_12() const { return ___downloadHandlerSetMethod_12; }
	inline MethodInfo_t ** get_address_of_downloadHandlerSetMethod_12() { return &___downloadHandlerSetMethod_12; }
	inline void set_downloadHandlerSetMethod_12(MethodInfo_t * value)
	{
		___downloadHandlerSetMethod_12 = value;
		Il2CppCodeGenWriteBarrier((&___downloadHandlerSetMethod_12), value);
	}

	inline static int32_t get_offset_of_uploadHandlerSetMethod_13() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___uploadHandlerSetMethod_13)); }
	inline MethodInfo_t * get_uploadHandlerSetMethod_13() const { return ___uploadHandlerSetMethod_13; }
	inline MethodInfo_t ** get_address_of_uploadHandlerSetMethod_13() { return &___uploadHandlerSetMethod_13; }
	inline void set_uploadHandlerSetMethod_13(MethodInfo_t * value)
	{
		___uploadHandlerSetMethod_13 = value;
		Il2CppCodeGenWriteBarrier((&___uploadHandlerSetMethod_13), value);
	}

	inline static int32_t get_offset_of_errorGetMethod_14() { return static_cast<int32_t>(offsetof(UnityWebRequestWrapper_t1542496577_StaticFields, ___errorGetMethod_14)); }
	inline MethodInfo_t * get_errorGetMethod_14() const { return ___errorGetMethod_14; }
	inline MethodInfo_t ** get_address_of_errorGetMethod_14() { return &___errorGetMethod_14; }
	inline void set_errorGetMethod_14(MethodInfo_t * value)
	{
		___errorGetMethod_14 = value;
		Il2CppCodeGenWriteBarrier((&___errorGetMethod_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBREQUESTWRAPPER_T1542496577_H
#ifndef DATETIMEUNMARSHALLER_T1723598451_H
#define DATETIMEUNMARSHALLER_T1723598451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller
struct  DateTimeUnmarshaller_t1723598451  : public RuntimeObject
{
public:

public:
};

struct DateTimeUnmarshaller_t1723598451_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller Amazon.Runtime.Internal.Transform.DateTimeUnmarshaller::_instance
	DateTimeUnmarshaller_t1723598451 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DateTimeUnmarshaller_t1723598451_StaticFields, ____instance_0)); }
	inline DateTimeUnmarshaller_t1723598451 * get__instance_0() const { return ____instance_0; }
	inline DateTimeUnmarshaller_t1723598451 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(DateTimeUnmarshaller_t1723598451 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEUNMARSHALLER_T1723598451_H
#ifndef UNMARSHALLEREXTENSIONS_T2325150308_H
#define UNMARSHALLEREXTENSIONS_T2325150308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.UnmarshallerExtensions
struct  UnmarshallerExtensions_t2325150308  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMARSHALLEREXTENSIONS_T2325150308_H
#ifndef UNMARSHALLERCONTEXT_T1608322995_H
#define UNMARSHALLERCONTEXT_T1608322995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.UnmarshallerContext
struct  UnmarshallerContext_t1608322995  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::disposed
	bool ___disposed_0;
	// System.Boolean Amazon.Runtime.Internal.Transform.UnmarshallerContext::<MaintainResponseBody>k__BackingField
	bool ___U3CMaintainResponseBodyU3Ek__BackingField_1;
	// ThirdParty.Ionic.Zlib.CrcCalculatorStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::<CrcStream>k__BackingField
	CrcCalculatorStream_t2228597532 * ___U3CCrcStreamU3Ek__BackingField_2;
	// System.Int32 Amazon.Runtime.Internal.Transform.UnmarshallerContext::<Crc32Result>k__BackingField
	int32_t ___U3CCrc32ResultU3Ek__BackingField_3;
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.Transform.UnmarshallerContext::<WebResponseData>k__BackingField
	RuntimeObject* ___U3CWebResponseDataU3Ek__BackingField_4;
	// Amazon.Runtime.Internal.Util.CachingWrapperStream Amazon.Runtime.Internal.Transform.UnmarshallerContext::<WrappingStream>k__BackingField
	CachingWrapperStream_t380166576 * ___U3CWrappingStreamU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_disposed_0() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___disposed_0)); }
	inline bool get_disposed_0() const { return ___disposed_0; }
	inline bool* get_address_of_disposed_0() { return &___disposed_0; }
	inline void set_disposed_0(bool value)
	{
		___disposed_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaintainResponseBodyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CMaintainResponseBodyU3Ek__BackingField_1)); }
	inline bool get_U3CMaintainResponseBodyU3Ek__BackingField_1() const { return ___U3CMaintainResponseBodyU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CMaintainResponseBodyU3Ek__BackingField_1() { return &___U3CMaintainResponseBodyU3Ek__BackingField_1; }
	inline void set_U3CMaintainResponseBodyU3Ek__BackingField_1(bool value)
	{
		___U3CMaintainResponseBodyU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCrcStreamU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CCrcStreamU3Ek__BackingField_2)); }
	inline CrcCalculatorStream_t2228597532 * get_U3CCrcStreamU3Ek__BackingField_2() const { return ___U3CCrcStreamU3Ek__BackingField_2; }
	inline CrcCalculatorStream_t2228597532 ** get_address_of_U3CCrcStreamU3Ek__BackingField_2() { return &___U3CCrcStreamU3Ek__BackingField_2; }
	inline void set_U3CCrcStreamU3Ek__BackingField_2(CrcCalculatorStream_t2228597532 * value)
	{
		___U3CCrcStreamU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCrcStreamU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCrc32ResultU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CCrc32ResultU3Ek__BackingField_3)); }
	inline int32_t get_U3CCrc32ResultU3Ek__BackingField_3() const { return ___U3CCrc32ResultU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCrc32ResultU3Ek__BackingField_3() { return &___U3CCrc32ResultU3Ek__BackingField_3; }
	inline void set_U3CCrc32ResultU3Ek__BackingField_3(int32_t value)
	{
		___U3CCrc32ResultU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CWebResponseDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CWebResponseDataU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CWebResponseDataU3Ek__BackingField_4() const { return ___U3CWebResponseDataU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CWebResponseDataU3Ek__BackingField_4() { return &___U3CWebResponseDataU3Ek__BackingField_4; }
	inline void set_U3CWebResponseDataU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CWebResponseDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWebResponseDataU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CWrappingStreamU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnmarshallerContext_t1608322995, ___U3CWrappingStreamU3Ek__BackingField_5)); }
	inline CachingWrapperStream_t380166576 * get_U3CWrappingStreamU3Ek__BackingField_5() const { return ___U3CWrappingStreamU3Ek__BackingField_5; }
	inline CachingWrapperStream_t380166576 ** get_address_of_U3CWrappingStreamU3Ek__BackingField_5() { return &___U3CWrappingStreamU3Ek__BackingField_5; }
	inline void set_U3CWrappingStreamU3Ek__BackingField_5(CachingWrapperStream_t380166576 * value)
	{
		___U3CWrappingStreamU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWrappingStreamU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMARSHALLERCONTEXT_T1608322995_H
#ifndef LOGMESSAGE_T4004523899_H
#define LOGMESSAGE_T4004523899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.LogMessage
struct  LogMessage_t4004523899  : public RuntimeObject
{
public:
	// System.Object[] Amazon.Runtime.Internal.Util.LogMessage::<Args>k__BackingField
	ObjectU5BU5D_t3614634134* ___U3CArgsU3Ek__BackingField_0;
	// System.IFormatProvider Amazon.Runtime.Internal.Util.LogMessage::<Provider>k__BackingField
	RuntimeObject* ___U3CProviderU3Ek__BackingField_1;
	// System.String Amazon.Runtime.Internal.Util.LogMessage::<Format>k__BackingField
	String_t* ___U3CFormatU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CArgsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LogMessage_t4004523899, ___U3CArgsU3Ek__BackingField_0)); }
	inline ObjectU5BU5D_t3614634134* get_U3CArgsU3Ek__BackingField_0() const { return ___U3CArgsU3Ek__BackingField_0; }
	inline ObjectU5BU5D_t3614634134** get_address_of_U3CArgsU3Ek__BackingField_0() { return &___U3CArgsU3Ek__BackingField_0; }
	inline void set_U3CArgsU3Ek__BackingField_0(ObjectU5BU5D_t3614634134* value)
	{
		___U3CArgsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProviderU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LogMessage_t4004523899, ___U3CProviderU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CProviderU3Ek__BackingField_1() const { return ___U3CProviderU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CProviderU3Ek__BackingField_1() { return &___U3CProviderU3Ek__BackingField_1; }
	inline void set_U3CProviderU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CProviderU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CFormatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LogMessage_t4004523899, ___U3CFormatU3Ek__BackingField_2)); }
	inline String_t* get_U3CFormatU3Ek__BackingField_2() const { return ___U3CFormatU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CFormatU3Ek__BackingField_2() { return &___U3CFormatU3Ek__BackingField_2; }
	inline void set_U3CFormatU3Ek__BackingField_2(String_t* value)
	{
		___U3CFormatU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormatU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGMESSAGE_T4004523899_H
#ifndef JSONPATHSTACK_T805090354_H
#define JSONPATHSTACK_T805090354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack
struct  JsonPathStack_t805090354  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<System.String> Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::stack
	Stack_1_t3116948387 * ___stack_0;
	// System.Int32 Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::currentDepth
	int32_t ___currentDepth_1;
	// System.Text.StringBuilder Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::stackStringBuilder
	StringBuilder_t1221177846 * ___stackStringBuilder_2;
	// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack::stackString
	String_t* ___stackString_3;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(JsonPathStack_t805090354, ___stack_0)); }
	inline Stack_1_t3116948387 * get_stack_0() const { return ___stack_0; }
	inline Stack_1_t3116948387 ** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(Stack_1_t3116948387 * value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}

	inline static int32_t get_offset_of_currentDepth_1() { return static_cast<int32_t>(offsetof(JsonPathStack_t805090354, ___currentDepth_1)); }
	inline int32_t get_currentDepth_1() const { return ___currentDepth_1; }
	inline int32_t* get_address_of_currentDepth_1() { return &___currentDepth_1; }
	inline void set_currentDepth_1(int32_t value)
	{
		___currentDepth_1 = value;
	}

	inline static int32_t get_offset_of_stackStringBuilder_2() { return static_cast<int32_t>(offsetof(JsonPathStack_t805090354, ___stackStringBuilder_2)); }
	inline StringBuilder_t1221177846 * get_stackStringBuilder_2() const { return ___stackStringBuilder_2; }
	inline StringBuilder_t1221177846 ** get_address_of_stackStringBuilder_2() { return &___stackStringBuilder_2; }
	inline void set_stackStringBuilder_2(StringBuilder_t1221177846 * value)
	{
		___stackStringBuilder_2 = value;
		Il2CppCodeGenWriteBarrier((&___stackStringBuilder_2), value);
	}

	inline static int32_t get_offset_of_stackString_3() { return static_cast<int32_t>(offsetof(JsonPathStack_t805090354, ___stackString_3)); }
	inline String_t* get_stackString_3() const { return ___stackString_3; }
	inline String_t** get_address_of_stackString_3() { return &___stackString_3; }
	inline void set_stackString_3(String_t* value)
	{
		___stackString_3 = value;
		Il2CppCodeGenWriteBarrier((&___stackString_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPATHSTACK_T805090354_H
#ifndef RESPONSEUNMARSHALLER_T3934041557_H
#define RESPONSEUNMARSHALLER_T3934041557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.ResponseUnmarshaller
struct  ResponseUnmarshaller_t3934041557  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSEUNMARSHALLER_T3934041557_H
#ifndef TIMING_T847194262_H
#define TIMING_T847194262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.Timing
struct  Timing_t847194262  : public RuntimeObject
{
public:
	// System.Int64 Amazon.Runtime.Internal.Util.Timing::startTime
	int64_t ___startTime_0;
	// System.Int64 Amazon.Runtime.Internal.Util.Timing::endTime
	int64_t ___endTime_1;
	// System.Boolean Amazon.Runtime.Internal.Util.Timing::<IsFinished>k__BackingField
	bool ___U3CIsFinishedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_startTime_0() { return static_cast<int32_t>(offsetof(Timing_t847194262, ___startTime_0)); }
	inline int64_t get_startTime_0() const { return ___startTime_0; }
	inline int64_t* get_address_of_startTime_0() { return &___startTime_0; }
	inline void set_startTime_0(int64_t value)
	{
		___startTime_0 = value;
	}

	inline static int32_t get_offset_of_endTime_1() { return static_cast<int32_t>(offsetof(Timing_t847194262, ___endTime_1)); }
	inline int64_t get_endTime_1() const { return ___endTime_1; }
	inline int64_t* get_address_of_endTime_1() { return &___endTime_1; }
	inline void set_endTime_1(int64_t value)
	{
		___endTime_1 = value;
	}

	inline static int32_t get_offset_of_U3CIsFinishedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Timing_t847194262, ___U3CIsFinishedU3Ek__BackingField_2)); }
	inline bool get_U3CIsFinishedU3Ek__BackingField_2() const { return ___U3CIsFinishedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsFinishedU3Ek__BackingField_2() { return &___U3CIsFinishedU3Ek__BackingField_2; }
	inline void set_U3CIsFinishedU3Ek__BackingField_2(bool value)
	{
		___U3CIsFinishedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMING_T847194262_H
#ifndef U3CU3EC_T963174459_H
#define U3CU3EC_T963174459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.RequestMetrics/<>c
struct  U3CU3Ec_t963174459  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t963174459_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.RequestMetrics/<>c Amazon.Runtime.Internal.Util.RequestMetrics/<>c::<>9
	U3CU3Ec_t963174459 * ___U3CU3E9_0;
	// System.Func`2<Amazon.Runtime.Metric,System.String> Amazon.Runtime.Internal.Util.RequestMetrics/<>c::<>9__33_0
	Func_2_t1028326184 * ___U3CU3E9__33_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t963174459_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t963174459 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t963174459 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t963174459 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t963174459_StaticFields, ___U3CU3E9__33_0_1)); }
	inline Func_2_t1028326184 * get_U3CU3E9__33_0_1() const { return ___U3CU3E9__33_0_1; }
	inline Func_2_t1028326184 ** get_address_of_U3CU3E9__33_0_1() { return &___U3CU3E9__33_0_1; }
	inline void set_U3CU3E9__33_0_1(Func_2_t1028326184 * value)
	{
		___U3CU3E9__33_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__33_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T963174459_H
#ifndef REQUESTMETRICS_T218029284_H
#define REQUESTMETRICS_T218029284_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.RequestMetrics
struct  RequestMetrics_t218029284  : public RuntimeObject
{
public:
	// System.Object Amazon.Runtime.Internal.Util.RequestMetrics::metricsLock
	RuntimeObject * ___metricsLock_0;
	// System.Diagnostics.Stopwatch Amazon.Runtime.Internal.Util.RequestMetrics::stopWatch
	Stopwatch_t1380178105 * ___stopWatch_1;
	// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,Amazon.Runtime.Internal.Util.Timing> Amazon.Runtime.Internal.Util.RequestMetrics::inFlightTimings
	Dictionary_2_t3597272751 * ___inFlightTimings_2;
	// System.Collections.Generic.List`1<Amazon.Runtime.Internal.Util.MetricError> Amazon.Runtime.Internal.Util.RequestMetrics::errors
	List_1_t333565938 * ___errors_3;
	// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<System.Object>> Amazon.Runtime.Internal.Util.RequestMetrics::<Properties>k__BackingField
	Dictionary_2_t513681620 * ___U3CPropertiesU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Collections.Generic.List`1<Amazon.Runtime.IMetricsTiming>> Amazon.Runtime.Internal.Util.RequestMetrics::<Timings>k__BackingField
	Dictionary_2_t2292610545 * ___U3CTimingsU3Ek__BackingField_5;
	// System.Collections.Generic.Dictionary`2<Amazon.Runtime.Metric,System.Int64> Amazon.Runtime.Internal.Util.RequestMetrics::<Counters>k__BackingField
	Dictionary_2_t3659156526 * ___U3CCountersU3Ek__BackingField_6;
	// System.Boolean Amazon.Runtime.Internal.Util.RequestMetrics::<IsEnabled>k__BackingField
	bool ___U3CIsEnabledU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_metricsLock_0() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___metricsLock_0)); }
	inline RuntimeObject * get_metricsLock_0() const { return ___metricsLock_0; }
	inline RuntimeObject ** get_address_of_metricsLock_0() { return &___metricsLock_0; }
	inline void set_metricsLock_0(RuntimeObject * value)
	{
		___metricsLock_0 = value;
		Il2CppCodeGenWriteBarrier((&___metricsLock_0), value);
	}

	inline static int32_t get_offset_of_stopWatch_1() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___stopWatch_1)); }
	inline Stopwatch_t1380178105 * get_stopWatch_1() const { return ___stopWatch_1; }
	inline Stopwatch_t1380178105 ** get_address_of_stopWatch_1() { return &___stopWatch_1; }
	inline void set_stopWatch_1(Stopwatch_t1380178105 * value)
	{
		___stopWatch_1 = value;
		Il2CppCodeGenWriteBarrier((&___stopWatch_1), value);
	}

	inline static int32_t get_offset_of_inFlightTimings_2() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___inFlightTimings_2)); }
	inline Dictionary_2_t3597272751 * get_inFlightTimings_2() const { return ___inFlightTimings_2; }
	inline Dictionary_2_t3597272751 ** get_address_of_inFlightTimings_2() { return &___inFlightTimings_2; }
	inline void set_inFlightTimings_2(Dictionary_2_t3597272751 * value)
	{
		___inFlightTimings_2 = value;
		Il2CppCodeGenWriteBarrier((&___inFlightTimings_2), value);
	}

	inline static int32_t get_offset_of_errors_3() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___errors_3)); }
	inline List_1_t333565938 * get_errors_3() const { return ___errors_3; }
	inline List_1_t333565938 ** get_address_of_errors_3() { return &___errors_3; }
	inline void set_errors_3(List_1_t333565938 * value)
	{
		___errors_3 = value;
		Il2CppCodeGenWriteBarrier((&___errors_3), value);
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___U3CPropertiesU3Ek__BackingField_4)); }
	inline Dictionary_2_t513681620 * get_U3CPropertiesU3Ek__BackingField_4() const { return ___U3CPropertiesU3Ek__BackingField_4; }
	inline Dictionary_2_t513681620 ** get_address_of_U3CPropertiesU3Ek__BackingField_4() { return &___U3CPropertiesU3Ek__BackingField_4; }
	inline void set_U3CPropertiesU3Ek__BackingField_4(Dictionary_2_t513681620 * value)
	{
		___U3CPropertiesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CTimingsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___U3CTimingsU3Ek__BackingField_5)); }
	inline Dictionary_2_t2292610545 * get_U3CTimingsU3Ek__BackingField_5() const { return ___U3CTimingsU3Ek__BackingField_5; }
	inline Dictionary_2_t2292610545 ** get_address_of_U3CTimingsU3Ek__BackingField_5() { return &___U3CTimingsU3Ek__BackingField_5; }
	inline void set_U3CTimingsU3Ek__BackingField_5(Dictionary_2_t2292610545 * value)
	{
		___U3CTimingsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTimingsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CCountersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___U3CCountersU3Ek__BackingField_6)); }
	inline Dictionary_2_t3659156526 * get_U3CCountersU3Ek__BackingField_6() const { return ___U3CCountersU3Ek__BackingField_6; }
	inline Dictionary_2_t3659156526 ** get_address_of_U3CCountersU3Ek__BackingField_6() { return &___U3CCountersU3Ek__BackingField_6; }
	inline void set_U3CCountersU3Ek__BackingField_6(Dictionary_2_t3659156526 * value)
	{
		___U3CCountersU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCountersU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIsEnabledU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RequestMetrics_t218029284, ___U3CIsEnabledU3Ek__BackingField_7)); }
	inline bool get_U3CIsEnabledU3Ek__BackingField_7() const { return ___U3CIsEnabledU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsEnabledU3Ek__BackingField_7() { return &___U3CIsEnabledU3Ek__BackingField_7; }
	inline void set_U3CIsEnabledU3Ek__BackingField_7(bool value)
	{
		___U3CIsEnabledU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTMETRICS_T218029284_H
#ifndef LONGUNMARSHALLER_T2567652076_H
#define LONGUNMARSHALLER_T2567652076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.LongUnmarshaller
struct  LongUnmarshaller_t2567652076  : public RuntimeObject
{
public:

public:
};

struct LongUnmarshaller_t2567652076_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.LongUnmarshaller Amazon.Runtime.Internal.Transform.LongUnmarshaller::_instance
	LongUnmarshaller_t2567652076 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(LongUnmarshaller_t2567652076_StaticFields, ____instance_0)); }
	inline LongUnmarshaller_t2567652076 * get__instance_0() const { return ____instance_0; }
	inline LongUnmarshaller_t2567652076 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(LongUnmarshaller_t2567652076 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGUNMARSHALLER_T2567652076_H
#ifndef S3URI_T3301881392_H
#define S3URI_T3301881392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.S3Uri
struct  S3Uri_t3301881392  : public RuntimeObject
{
public:

public:
};

struct S3Uri_t3301881392_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex Amazon.Runtime.Internal.Util.S3Uri::S3EndpointRegex
	Regex_t1803876613 * ___S3EndpointRegex_0;

public:
	inline static int32_t get_offset_of_S3EndpointRegex_0() { return static_cast<int32_t>(offsetof(S3Uri_t3301881392_StaticFields, ___S3EndpointRegex_0)); }
	inline Regex_t1803876613 * get_S3EndpointRegex_0() const { return ___S3EndpointRegex_0; }
	inline Regex_t1803876613 ** get_address_of_S3EndpointRegex_0() { return &___S3EndpointRegex_0; }
	inline void set_S3EndpointRegex_0(Regex_t1803876613 * value)
	{
		___S3EndpointRegex_0 = value;
		Il2CppCodeGenWriteBarrier((&___S3EndpointRegex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S3URI_T3301881392_H
#ifndef BOOLUNMARSHALLER_T2985984772_H
#define BOOLUNMARSHALLER_T2985984772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.BoolUnmarshaller
struct  BoolUnmarshaller_t2985984772  : public RuntimeObject
{
public:

public:
};

struct BoolUnmarshaller_t2985984772_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.BoolUnmarshaller Amazon.Runtime.Internal.Transform.BoolUnmarshaller::_instance
	BoolUnmarshaller_t2985984772 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(BoolUnmarshaller_t2985984772_StaticFields, ____instance_0)); }
	inline BoolUnmarshaller_t2985984772 * get__instance_0() const { return ____instance_0; }
	inline BoolUnmarshaller_t2985984772 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(BoolUnmarshaller_t2985984772 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLUNMARSHALLER_T2985984772_H
#ifndef INTERNALLOGGER_T2972373343_H
#define INTERNALLOGGER_T2972373343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.InternalLogger
struct  InternalLogger_t2972373343  : public RuntimeObject
{
public:
	// System.Type Amazon.Runtime.Internal.Util.InternalLogger::<DeclaringType>k__BackingField
	Type_t * ___U3CDeclaringTypeU3Ek__BackingField_0;
	// System.Boolean Amazon.Runtime.Internal.Util.InternalLogger::<IsEnabled>k__BackingField
	bool ___U3CIsEnabledU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDeclaringTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InternalLogger_t2972373343, ___U3CDeclaringTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CDeclaringTypeU3Ek__BackingField_0() const { return ___U3CDeclaringTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CDeclaringTypeU3Ek__BackingField_0() { return &___U3CDeclaringTypeU3Ek__BackingField_0; }
	inline void set_U3CDeclaringTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CDeclaringTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclaringTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIsEnabledU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InternalLogger_t2972373343, ___U3CIsEnabledU3Ek__BackingField_1)); }
	inline bool get_U3CIsEnabledU3Ek__BackingField_1() const { return ___U3CIsEnabledU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsEnabledU3Ek__BackingField_1() { return &___U3CIsEnabledU3Ek__BackingField_1; }
	inline void set_U3CIsEnabledU3Ek__BackingField_1(bool value)
	{
		___U3CIsEnabledU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALLOGGER_T2972373343_H
#ifndef LOGGER_T2262497814_H
#define LOGGER_T2262497814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.Logger
struct  Logger_t2262497814  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Amazon.Runtime.Internal.Util.InternalLogger> Amazon.Runtime.Internal.Util.Logger::loggers
	List_1_t2341494475 * ___loggers_1;

public:
	inline static int32_t get_offset_of_loggers_1() { return static_cast<int32_t>(offsetof(Logger_t2262497814, ___loggers_1)); }
	inline List_1_t2341494475 * get_loggers_1() const { return ___loggers_1; }
	inline List_1_t2341494475 ** get_address_of_loggers_1() { return &___loggers_1; }
	inline void set_loggers_1(List_1_t2341494475 * value)
	{
		___loggers_1 = value;
		Il2CppCodeGenWriteBarrier((&___loggers_1), value);
	}
};

struct Logger_t2262497814_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.Internal.Util.Logger> Amazon.Runtime.Internal.Util.Logger::cachedLoggers
	RuntimeObject* ___cachedLoggers_0;
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.Util.Logger::emptyLogger
	Logger_t2262497814 * ___emptyLogger_2;

public:
	inline static int32_t get_offset_of_cachedLoggers_0() { return static_cast<int32_t>(offsetof(Logger_t2262497814_StaticFields, ___cachedLoggers_0)); }
	inline RuntimeObject* get_cachedLoggers_0() const { return ___cachedLoggers_0; }
	inline RuntimeObject** get_address_of_cachedLoggers_0() { return &___cachedLoggers_0; }
	inline void set_cachedLoggers_0(RuntimeObject* value)
	{
		___cachedLoggers_0 = value;
		Il2CppCodeGenWriteBarrier((&___cachedLoggers_0), value);
	}

	inline static int32_t get_offset_of_emptyLogger_2() { return static_cast<int32_t>(offsetof(Logger_t2262497814_StaticFields, ___emptyLogger_2)); }
	inline Logger_t2262497814 * get_emptyLogger_2() const { return ___emptyLogger_2; }
	inline Logger_t2262497814 ** get_address_of_emptyLogger_2() { return &___emptyLogger_2; }
	inline void set_emptyLogger_2(Logger_t2262497814 * value)
	{
		___emptyLogger_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyLogger_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T2262497814_H
#ifndef INTUNMARSHALLER_T2174001701_H
#define INTUNMARSHALLER_T2174001701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.IntUnmarshaller
struct  IntUnmarshaller_t2174001701  : public RuntimeObject
{
public:

public:
};

struct IntUnmarshaller_t2174001701_StaticFields
{
public:
	// Amazon.Runtime.Internal.Transform.IntUnmarshaller Amazon.Runtime.Internal.Transform.IntUnmarshaller::_instance
	IntUnmarshaller_t2174001701 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(IntUnmarshaller_t2174001701_StaticFields, ____instance_0)); }
	inline IntUnmarshaller_t2174001701 * get__instance_0() const { return ____instance_0; }
	inline IntUnmarshaller_t2174001701 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(IntUnmarshaller_t2174001701 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTUNMARSHALLER_T2174001701_H
#ifndef EXCEPTIONHANDLER_1_T2197621571_H
#define EXCEPTIONHANDLER_1_T2197621571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ExceptionHandler`1<Amazon.Runtime.Internal.HttpErrorResponseException>
struct  ExceptionHandler_1_t2197621571  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.ExceptionHandler`1::_logger
	RuntimeObject* ____logger_0;

public:
	inline static int32_t get_offset_of__logger_0() { return static_cast<int32_t>(offsetof(ExceptionHandler_1_t2197621571, ____logger_0)); }
	inline RuntimeObject* get__logger_0() const { return ____logger_0; }
	inline RuntimeObject** get_address_of__logger_0() { return &____logger_0; }
	inline void set__logger_0(RuntimeObject* value)
	{
		____logger_0 = value;
		Il2CppCodeGenWriteBarrier((&____logger_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTIONHANDLER_1_T2197621571_H
#ifndef BACKGROUNDDISPATCHER_1_T3230906411_H
#define BACKGROUNDDISPATCHER_1_T3230906411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.BackgroundDispatcher`1<System.Action>
struct  BackgroundDispatcher_1_t3230906411  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::isDisposed
	bool ___isDisposed_0;
	// System.Action`1<T> Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::action
	Action_1_t3028271134 * ___action_1;
	// System.Collections.Generic.Queue`1<T> Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::queue
	Queue_1_t3046128587 * ___queue_2;
	// System.Threading.Thread Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::backgroundThread
	Thread_t241561612 * ___backgroundThread_3;
	// System.Threading.AutoResetEvent Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::resetEvent
	AutoResetEvent_t15112628 * ___resetEvent_4;
	// System.Boolean Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::shouldStop
	bool ___shouldStop_5;
	// System.Boolean Amazon.Runtime.Internal.Util.BackgroundDispatcher`1::<IsRunning>k__BackingField
	bool ___U3CIsRunningU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_isDisposed_0() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___isDisposed_0)); }
	inline bool get_isDisposed_0() const { return ___isDisposed_0; }
	inline bool* get_address_of_isDisposed_0() { return &___isDisposed_0; }
	inline void set_isDisposed_0(bool value)
	{
		___isDisposed_0 = value;
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___action_1)); }
	inline Action_1_t3028271134 * get_action_1() const { return ___action_1; }
	inline Action_1_t3028271134 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_1_t3028271134 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}

	inline static int32_t get_offset_of_queue_2() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___queue_2)); }
	inline Queue_1_t3046128587 * get_queue_2() const { return ___queue_2; }
	inline Queue_1_t3046128587 ** get_address_of_queue_2() { return &___queue_2; }
	inline void set_queue_2(Queue_1_t3046128587 * value)
	{
		___queue_2 = value;
		Il2CppCodeGenWriteBarrier((&___queue_2), value);
	}

	inline static int32_t get_offset_of_backgroundThread_3() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___backgroundThread_3)); }
	inline Thread_t241561612 * get_backgroundThread_3() const { return ___backgroundThread_3; }
	inline Thread_t241561612 ** get_address_of_backgroundThread_3() { return &___backgroundThread_3; }
	inline void set_backgroundThread_3(Thread_t241561612 * value)
	{
		___backgroundThread_3 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundThread_3), value);
	}

	inline static int32_t get_offset_of_resetEvent_4() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___resetEvent_4)); }
	inline AutoResetEvent_t15112628 * get_resetEvent_4() const { return ___resetEvent_4; }
	inline AutoResetEvent_t15112628 ** get_address_of_resetEvent_4() { return &___resetEvent_4; }
	inline void set_resetEvent_4(AutoResetEvent_t15112628 * value)
	{
		___resetEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___resetEvent_4), value);
	}

	inline static int32_t get_offset_of_shouldStop_5() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___shouldStop_5)); }
	inline bool get_shouldStop_5() const { return ___shouldStop_5; }
	inline bool* get_address_of_shouldStop_5() { return &___shouldStop_5; }
	inline void set_shouldStop_5(bool value)
	{
		___shouldStop_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsRunningU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BackgroundDispatcher_1_t3230906411, ___U3CIsRunningU3Ek__BackingField_6)); }
	inline bool get_U3CIsRunningU3Ek__BackingField_6() const { return ___U3CIsRunningU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsRunningU3Ek__BackingField_6() { return &___U3CIsRunningU3Ek__BackingField_6; }
	inline void set_U3CIsRunningU3Ek__BackingField_6(bool value)
	{
		___U3CIsRunningU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDDISPATCHER_1_T3230906411_H
#ifndef UNITYWEBREQUESTFACTORY_T752669660_H
#define UNITYWEBREQUESTFACTORY_T752669660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityWebRequestFactory
struct  UnityWebRequestFactory_t752669660  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.UnityRequest Amazon.Runtime.Internal.UnityWebRequestFactory::_unityRequest
	UnityRequest_t4218620704 * ____unityRequest_0;

public:
	inline static int32_t get_offset_of__unityRequest_0() { return static_cast<int32_t>(offsetof(UnityWebRequestFactory_t752669660, ____unityRequest_0)); }
	inline UnityRequest_t4218620704 * get__unityRequest_0() const { return ____unityRequest_0; }
	inline UnityRequest_t4218620704 ** get_address_of__unityRequest_0() { return &____unityRequest_0; }
	inline void set__unityRequest_0(UnityRequest_t4218620704 * value)
	{
		____unityRequest_0 = value;
		Il2CppCodeGenWriteBarrier((&____unityRequest_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBREQUESTFACTORY_T752669660_H
#ifndef UNITYWWWREQUESTFACTORY_T3206525961_H
#define UNITYWWWREQUESTFACTORY_T3206525961_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityWwwRequestFactory
struct  UnityWwwRequestFactory_t3206525961  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.UnityWwwRequest Amazon.Runtime.Internal.UnityWwwRequestFactory::_unityWwwRequest
	UnityWwwRequest_t118609659 * ____unityWwwRequest_0;

public:
	inline static int32_t get_offset_of__unityWwwRequest_0() { return static_cast<int32_t>(offsetof(UnityWwwRequestFactory_t3206525961, ____unityWwwRequest_0)); }
	inline UnityWwwRequest_t118609659 * get__unityWwwRequest_0() const { return ____unityWwwRequest_0; }
	inline UnityWwwRequest_t118609659 ** get_address_of__unityWwwRequest_0() { return &____unityWwwRequest_0; }
	inline void set__unityWwwRequest_0(UnityWwwRequest_t118609659 * value)
	{
		____unityWwwRequest_0 = value;
		Il2CppCodeGenWriteBarrier((&____unityWwwRequest_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWWWREQUESTFACTORY_T3206525961_H
#ifndef UNITYREQUEST_T4218620704_H
#define UNITYREQUEST_T4218620704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityRequest
struct  UnityRequest_t4218620704  : public RuntimeObject
{
public:
	// System.Uri Amazon.Runtime.Internal.UnityRequest::<RequestUri>k__BackingField
	Uri_t19570940 * ___U3CRequestUriU3Ek__BackingField_0;
	// System.IDisposable Amazon.Runtime.Internal.UnityRequest::<WwwRequest>k__BackingField
	RuntimeObject* ___U3CWwwRequestU3Ek__BackingField_1;
	// System.Byte[] Amazon.Runtime.Internal.UnityRequest::<RequestContent>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CRequestContentU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.UnityRequest::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_3;
	// System.AsyncCallback Amazon.Runtime.Internal.UnityRequest::<Callback>k__BackingField
	AsyncCallback_t163412349 * ___U3CCallbackU3Ek__BackingField_4;
	// System.IAsyncResult Amazon.Runtime.Internal.UnityRequest::<AsyncResult>k__BackingField
	RuntimeObject* ___U3CAsyncResultU3Ek__BackingField_5;
	// System.Threading.ManualResetEvent Amazon.Runtime.Internal.UnityRequest::<WaitHandle>k__BackingField
	ManualResetEvent_t926074657 * ___U3CWaitHandleU3Ek__BackingField_6;
	// System.Boolean Amazon.Runtime.Internal.UnityRequest::<IsSync>k__BackingField
	bool ___U3CIsSyncU3Ek__BackingField_7;
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.UnityRequest::<Response>k__BackingField
	RuntimeObject* ___U3CResponseU3Ek__BackingField_8;
	// System.Exception Amazon.Runtime.Internal.UnityRequest::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_9;
	// System.String Amazon.Runtime.Internal.UnityRequest::<Method>k__BackingField
	String_t* ___U3CMethodU3Ek__BackingField_10;
	// System.Boolean Amazon.Runtime.Internal.UnityRequest::_disposed
	bool ____disposed_11;
	// Amazon.Runtime.Internal.StreamReadTracker Amazon.Runtime.Internal.UnityRequest::<Tracker>k__BackingField
	StreamReadTracker_t1958363340 * ___U3CTrackerU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CRequestUriU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CRequestUriU3Ek__BackingField_0)); }
	inline Uri_t19570940 * get_U3CRequestUriU3Ek__BackingField_0() const { return ___U3CRequestUriU3Ek__BackingField_0; }
	inline Uri_t19570940 ** get_address_of_U3CRequestUriU3Ek__BackingField_0() { return &___U3CRequestUriU3Ek__BackingField_0; }
	inline void set_U3CRequestUriU3Ek__BackingField_0(Uri_t19570940 * value)
	{
		___U3CRequestUriU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestUriU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CWwwRequestU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CWwwRequestU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CWwwRequestU3Ek__BackingField_1() const { return ___U3CWwwRequestU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CWwwRequestU3Ek__BackingField_1() { return &___U3CWwwRequestU3Ek__BackingField_1; }
	inline void set_U3CWwwRequestU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CWwwRequestU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWwwRequestU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRequestContentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CRequestContentU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t3397334013* get_U3CRequestContentU3Ek__BackingField_2() const { return ___U3CRequestContentU3Ek__BackingField_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CRequestContentU3Ek__BackingField_2() { return &___U3CRequestContentU3Ek__BackingField_2; }
	inline void set_U3CRequestContentU3Ek__BackingField_2(ByteU5BU5D_t3397334013* value)
	{
		___U3CRequestContentU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestContentU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CHeadersU3Ek__BackingField_3)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_3() const { return ___U3CHeadersU3Ek__BackingField_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_3() { return &___U3CHeadersU3Ek__BackingField_3; }
	inline void set_U3CHeadersU3Ek__BackingField_3(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CCallbackU3Ek__BackingField_4)); }
	inline AsyncCallback_t163412349 * get_U3CCallbackU3Ek__BackingField_4() const { return ___U3CCallbackU3Ek__BackingField_4; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CCallbackU3Ek__BackingField_4() { return &___U3CCallbackU3Ek__BackingField_4; }
	inline void set_U3CCallbackU3Ek__BackingField_4(AsyncCallback_t163412349 * value)
	{
		___U3CCallbackU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CAsyncResultU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CAsyncResultU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CAsyncResultU3Ek__BackingField_5() const { return ___U3CAsyncResultU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CAsyncResultU3Ek__BackingField_5() { return &___U3CAsyncResultU3Ek__BackingField_5; }
	inline void set_U3CAsyncResultU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CAsyncResultU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAsyncResultU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CWaitHandleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CWaitHandleU3Ek__BackingField_6)); }
	inline ManualResetEvent_t926074657 * get_U3CWaitHandleU3Ek__BackingField_6() const { return ___U3CWaitHandleU3Ek__BackingField_6; }
	inline ManualResetEvent_t926074657 ** get_address_of_U3CWaitHandleU3Ek__BackingField_6() { return &___U3CWaitHandleU3Ek__BackingField_6; }
	inline void set_U3CWaitHandleU3Ek__BackingField_6(ManualResetEvent_t926074657 * value)
	{
		___U3CWaitHandleU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWaitHandleU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIsSyncU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CIsSyncU3Ek__BackingField_7)); }
	inline bool get_U3CIsSyncU3Ek__BackingField_7() const { return ___U3CIsSyncU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsSyncU3Ek__BackingField_7() { return &___U3CIsSyncU3Ek__BackingField_7; }
	inline void set_U3CIsSyncU3Ek__BackingField_7(bool value)
	{
		___U3CIsSyncU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CResponseU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CResponseU3Ek__BackingField_8() const { return ___U3CResponseU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CResponseU3Ek__BackingField_8() { return &___U3CResponseU3Ek__BackingField_8; }
	inline void set_U3CResponseU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CExceptionU3Ek__BackingField_9)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_9() const { return ___U3CExceptionU3Ek__BackingField_9; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_9() { return &___U3CExceptionU3Ek__BackingField_9; }
	inline void set_U3CExceptionU3Ek__BackingField_9(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CMethodU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CMethodU3Ek__BackingField_10)); }
	inline String_t* get_U3CMethodU3Ek__BackingField_10() const { return ___U3CMethodU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CMethodU3Ek__BackingField_10() { return &___U3CMethodU3Ek__BackingField_10; }
	inline void set_U3CMethodU3Ek__BackingField_10(String_t* value)
	{
		___U3CMethodU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMethodU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of__disposed_11() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ____disposed_11)); }
	inline bool get__disposed_11() const { return ____disposed_11; }
	inline bool* get_address_of__disposed_11() { return &____disposed_11; }
	inline void set__disposed_11(bool value)
	{
		____disposed_11 = value;
	}

	inline static int32_t get_offset_of_U3CTrackerU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(UnityRequest_t4218620704, ___U3CTrackerU3Ek__BackingField_12)); }
	inline StreamReadTracker_t1958363340 * get_U3CTrackerU3Ek__BackingField_12() const { return ___U3CTrackerU3Ek__BackingField_12; }
	inline StreamReadTracker_t1958363340 ** get_address_of_U3CTrackerU3Ek__BackingField_12() { return &___U3CTrackerU3Ek__BackingField_12; }
	inline void set_U3CTrackerU3Ek__BackingField_12(StreamReadTracker_t1958363340 * value)
	{
		___U3CTrackerU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrackerU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYREQUEST_T4218620704_H
#ifndef RUNTIMEPIPELINE_T3355992556_H
#define RUNTIMEPIPELINE_T3355992556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RuntimePipeline
struct  RuntimePipeline_t3355992556  : public RuntimeObject
{
public:
	// System.Boolean Amazon.Runtime.Internal.RuntimePipeline::_disposed
	bool ____disposed_0;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.RuntimePipeline::_logger
	RuntimeObject* ____logger_1;
	// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.RuntimePipeline::_handler
	RuntimeObject* ____handler_2;

public:
	inline static int32_t get_offset_of__disposed_0() { return static_cast<int32_t>(offsetof(RuntimePipeline_t3355992556, ____disposed_0)); }
	inline bool get__disposed_0() const { return ____disposed_0; }
	inline bool* get_address_of__disposed_0() { return &____disposed_0; }
	inline void set__disposed_0(bool value)
	{
		____disposed_0 = value;
	}

	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(RuntimePipeline_t3355992556, ____logger_1)); }
	inline RuntimeObject* get__logger_1() const { return ____logger_1; }
	inline RuntimeObject** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(RuntimeObject* value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier((&____logger_1), value);
	}

	inline static int32_t get_offset_of__handler_2() { return static_cast<int32_t>(offsetof(RuntimePipeline_t3355992556, ____handler_2)); }
	inline RuntimeObject* get__handler_2() const { return ____handler_2; }
	inline RuntimeObject** get_address_of__handler_2() { return &____handler_2; }
	inline void set__handler_2(RuntimeObject* value)
	{
		____handler_2 = value;
		Il2CppCodeGenWriteBarrier((&____handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPIPELINE_T3355992556_H
#ifndef PIPELINEHANDLER_T1486422324_H
#define PIPELINEHANDLER_T1486422324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.PipelineHandler
struct  PipelineHandler_t1486422324  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.PipelineHandler::<Logger>k__BackingField
	RuntimeObject* ___U3CLoggerU3Ek__BackingField_0;
	// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::<InnerHandler>k__BackingField
	RuntimeObject* ___U3CInnerHandlerU3Ek__BackingField_1;
	// Amazon.Runtime.IPipelineHandler Amazon.Runtime.Internal.PipelineHandler::<OuterHandler>k__BackingField
	RuntimeObject* ___U3COuterHandlerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CLoggerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PipelineHandler_t1486422324, ___U3CLoggerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CLoggerU3Ek__BackingField_0() const { return ___U3CLoggerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CLoggerU3Ek__BackingField_0() { return &___U3CLoggerU3Ek__BackingField_0; }
	inline void set_U3CLoggerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CLoggerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CInnerHandlerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PipelineHandler_t1486422324, ___U3CInnerHandlerU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CInnerHandlerU3Ek__BackingField_1() const { return ___U3CInnerHandlerU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CInnerHandlerU3Ek__BackingField_1() { return &___U3CInnerHandlerU3Ek__BackingField_1; }
	inline void set_U3CInnerHandlerU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CInnerHandlerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInnerHandlerU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COuterHandlerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PipelineHandler_t1486422324, ___U3COuterHandlerU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3COuterHandlerU3Ek__BackingField_2() const { return ___U3COuterHandlerU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3COuterHandlerU3Ek__BackingField_2() { return &___U3COuterHandlerU3Ek__BackingField_2; }
	inline void set_U3COuterHandlerU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3COuterHandlerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COuterHandlerU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIPELINEHANDLER_T1486422324_H
#ifndef MARSHALBYREFOBJECT_T1285298191_H
#define MARSHALBYREFOBJECT_T1285298191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t1285298191  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t1656058977 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t1285298191, ____identity_0)); }
	inline ServerIdentity_t1656058977 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t1656058977 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t1656058977 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T1285298191_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public RuntimeObject
{
public:

public:
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_0)); }
	inline Stream_t3255436806 * get_Null_0() const { return ___Null_0; }
	inline Stream_t3255436806 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t3255436806 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef UNITYREQUESTQUEUE_T3684366031_H
#define UNITYREQUESTQUEUE_T3684366031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityRequestQueue
struct  UnityRequestQueue_t3684366031  : public RuntimeObject
{
public:
	// System.Collections.Generic.Queue`1<Amazon.Runtime.IUnityHttpRequest> Amazon.Runtime.Internal.UnityRequestQueue::_requests
	Queue_1_t1679560232 * ____requests_4;
	// System.Collections.Generic.Queue`1<Amazon.Runtime.Internal.RuntimeAsyncResult> Amazon.Runtime.Internal.UnityRequestQueue::_callbacks
	Queue_1_t4099012848 * ____callbacks_5;
	// System.Collections.Generic.Queue`1<System.Action> Amazon.Runtime.Internal.UnityRequestQueue::_mainThreadCallbacks
	Queue_1_t3046128587 * ____mainThreadCallbacks_6;

public:
	inline static int32_t get_offset_of__requests_4() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031, ____requests_4)); }
	inline Queue_1_t1679560232 * get__requests_4() const { return ____requests_4; }
	inline Queue_1_t1679560232 ** get_address_of__requests_4() { return &____requests_4; }
	inline void set__requests_4(Queue_1_t1679560232 * value)
	{
		____requests_4 = value;
		Il2CppCodeGenWriteBarrier((&____requests_4), value);
	}

	inline static int32_t get_offset_of__callbacks_5() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031, ____callbacks_5)); }
	inline Queue_1_t4099012848 * get__callbacks_5() const { return ____callbacks_5; }
	inline Queue_1_t4099012848 ** get_address_of__callbacks_5() { return &____callbacks_5; }
	inline void set__callbacks_5(Queue_1_t4099012848 * value)
	{
		____callbacks_5 = value;
		Il2CppCodeGenWriteBarrier((&____callbacks_5), value);
	}

	inline static int32_t get_offset_of__mainThreadCallbacks_6() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031, ____mainThreadCallbacks_6)); }
	inline Queue_1_t3046128587 * get__mainThreadCallbacks_6() const { return ____mainThreadCallbacks_6; }
	inline Queue_1_t3046128587 ** get_address_of__mainThreadCallbacks_6() { return &____mainThreadCallbacks_6; }
	inline void set__mainThreadCallbacks_6(Queue_1_t3046128587 * value)
	{
		____mainThreadCallbacks_6 = value;
		Il2CppCodeGenWriteBarrier((&____mainThreadCallbacks_6), value);
	}
};

struct UnityRequestQueue_t3684366031_StaticFields
{
public:
	// Amazon.Runtime.Internal.UnityRequestQueue Amazon.Runtime.Internal.UnityRequestQueue::_instance
	UnityRequestQueue_t3684366031 * ____instance_0;
	// System.Object Amazon.Runtime.Internal.UnityRequestQueue::_requestsLock
	RuntimeObject * ____requestsLock_1;
	// System.Object Amazon.Runtime.Internal.UnityRequestQueue::_callbacksLock
	RuntimeObject * ____callbacksLock_2;
	// System.Object Amazon.Runtime.Internal.UnityRequestQueue::_mainThreadCallbackLock
	RuntimeObject * ____mainThreadCallbackLock_3;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031_StaticFields, ____instance_0)); }
	inline UnityRequestQueue_t3684366031 * get__instance_0() const { return ____instance_0; }
	inline UnityRequestQueue_t3684366031 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(UnityRequestQueue_t3684366031 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}

	inline static int32_t get_offset_of__requestsLock_1() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031_StaticFields, ____requestsLock_1)); }
	inline RuntimeObject * get__requestsLock_1() const { return ____requestsLock_1; }
	inline RuntimeObject ** get_address_of__requestsLock_1() { return &____requestsLock_1; }
	inline void set__requestsLock_1(RuntimeObject * value)
	{
		____requestsLock_1 = value;
		Il2CppCodeGenWriteBarrier((&____requestsLock_1), value);
	}

	inline static int32_t get_offset_of__callbacksLock_2() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031_StaticFields, ____callbacksLock_2)); }
	inline RuntimeObject * get__callbacksLock_2() const { return ____callbacksLock_2; }
	inline RuntimeObject ** get_address_of__callbacksLock_2() { return &____callbacksLock_2; }
	inline void set__callbacksLock_2(RuntimeObject * value)
	{
		____callbacksLock_2 = value;
		Il2CppCodeGenWriteBarrier((&____callbacksLock_2), value);
	}

	inline static int32_t get_offset_of__mainThreadCallbackLock_3() { return static_cast<int32_t>(offsetof(UnityRequestQueue_t3684366031_StaticFields, ____mainThreadCallbackLock_3)); }
	inline RuntimeObject * get__mainThreadCallbackLock_3() const { return ____mainThreadCallbackLock_3; }
	inline RuntimeObject ** get_address_of__mainThreadCallbackLock_3() { return &____mainThreadCallbackLock_3; }
	inline void set__mainThreadCallbackLock_3(RuntimeObject * value)
	{
		____mainThreadCallbackLock_3 = value;
		Il2CppCodeGenWriteBarrier((&____mainThreadCallbackLock_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYREQUESTQUEUE_T3684366031_H
#ifndef RUNTIMEASYNCRESULT_T4279356013_H
#define RUNTIMEASYNCRESULT_T4279356013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RuntimeAsyncResult
struct  RuntimeAsyncResult_t4279356013  : public RuntimeObject
{
public:
	// System.Object Amazon.Runtime.Internal.RuntimeAsyncResult::_lockObj
	RuntimeObject * ____lockObj_0;
	// System.Threading.ManualResetEvent Amazon.Runtime.Internal.RuntimeAsyncResult::_waitHandle
	ManualResetEvent_t926074657 * ____waitHandle_1;
	// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::_disposed
	bool ____disposed_2;
	// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::_callbackInvoked
	bool ____callbackInvoked_3;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.RuntimeAsyncResult::_logger
	RuntimeObject* ____logger_4;
	// System.AsyncCallback Amazon.Runtime.Internal.RuntimeAsyncResult::<AsyncCallback>k__BackingField
	AsyncCallback_t163412349 * ___U3CAsyncCallbackU3Ek__BackingField_5;
	// System.Object Amazon.Runtime.Internal.RuntimeAsyncResult::<AsyncState>k__BackingField
	RuntimeObject * ___U3CAsyncStateU3Ek__BackingField_6;
	// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::<CompletedSynchronously>k__BackingField
	bool ___U3CCompletedSynchronouslyU3Ek__BackingField_7;
	// System.Boolean Amazon.Runtime.Internal.RuntimeAsyncResult::<IsCompleted>k__BackingField
	bool ___U3CIsCompletedU3Ek__BackingField_8;
	// System.Exception Amazon.Runtime.Internal.RuntimeAsyncResult::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_9;
	// Amazon.Runtime.AmazonWebServiceResponse Amazon.Runtime.Internal.RuntimeAsyncResult::<Response>k__BackingField
	AmazonWebServiceResponse_t529043356 * ___U3CResponseU3Ek__BackingField_10;
	// Amazon.Runtime.AmazonWebServiceRequest Amazon.Runtime.Internal.RuntimeAsyncResult::<Request>k__BackingField
	AmazonWebServiceRequest_t3384026212 * ___U3CRequestU3Ek__BackingField_11;
	// System.Action`4<Amazon.Runtime.AmazonWebServiceRequest,Amazon.Runtime.AmazonWebServiceResponse,System.Exception,Amazon.Runtime.AsyncOptions> Amazon.Runtime.Internal.RuntimeAsyncResult::<Action>k__BackingField
	Action_4_t897279368 * ___U3CActionU3Ek__BackingField_12;
	// Amazon.Runtime.AsyncOptions Amazon.Runtime.Internal.RuntimeAsyncResult::<AsyncOptions>k__BackingField
	AsyncOptions_t558351272 * ___U3CAsyncOptionsU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of__lockObj_0() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____lockObj_0)); }
	inline RuntimeObject * get__lockObj_0() const { return ____lockObj_0; }
	inline RuntimeObject ** get_address_of__lockObj_0() { return &____lockObj_0; }
	inline void set__lockObj_0(RuntimeObject * value)
	{
		____lockObj_0 = value;
		Il2CppCodeGenWriteBarrier((&____lockObj_0), value);
	}

	inline static int32_t get_offset_of__waitHandle_1() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____waitHandle_1)); }
	inline ManualResetEvent_t926074657 * get__waitHandle_1() const { return ____waitHandle_1; }
	inline ManualResetEvent_t926074657 ** get_address_of__waitHandle_1() { return &____waitHandle_1; }
	inline void set__waitHandle_1(ManualResetEvent_t926074657 * value)
	{
		____waitHandle_1 = value;
		Il2CppCodeGenWriteBarrier((&____waitHandle_1), value);
	}

	inline static int32_t get_offset_of__disposed_2() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____disposed_2)); }
	inline bool get__disposed_2() const { return ____disposed_2; }
	inline bool* get_address_of__disposed_2() { return &____disposed_2; }
	inline void set__disposed_2(bool value)
	{
		____disposed_2 = value;
	}

	inline static int32_t get_offset_of__callbackInvoked_3() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____callbackInvoked_3)); }
	inline bool get__callbackInvoked_3() const { return ____callbackInvoked_3; }
	inline bool* get_address_of__callbackInvoked_3() { return &____callbackInvoked_3; }
	inline void set__callbackInvoked_3(bool value)
	{
		____callbackInvoked_3 = value;
	}

	inline static int32_t get_offset_of__logger_4() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ____logger_4)); }
	inline RuntimeObject* get__logger_4() const { return ____logger_4; }
	inline RuntimeObject** get_address_of__logger_4() { return &____logger_4; }
	inline void set__logger_4(RuntimeObject* value)
	{
		____logger_4 = value;
		Il2CppCodeGenWriteBarrier((&____logger_4), value);
	}

	inline static int32_t get_offset_of_U3CAsyncCallbackU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CAsyncCallbackU3Ek__BackingField_5)); }
	inline AsyncCallback_t163412349 * get_U3CAsyncCallbackU3Ek__BackingField_5() const { return ___U3CAsyncCallbackU3Ek__BackingField_5; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CAsyncCallbackU3Ek__BackingField_5() { return &___U3CAsyncCallbackU3Ek__BackingField_5; }
	inline void set_U3CAsyncCallbackU3Ek__BackingField_5(AsyncCallback_t163412349 * value)
	{
		___U3CAsyncCallbackU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAsyncCallbackU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CAsyncStateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CAsyncStateU3Ek__BackingField_6)); }
	inline RuntimeObject * get_U3CAsyncStateU3Ek__BackingField_6() const { return ___U3CAsyncStateU3Ek__BackingField_6; }
	inline RuntimeObject ** get_address_of_U3CAsyncStateU3Ek__BackingField_6() { return &___U3CAsyncStateU3Ek__BackingField_6; }
	inline void set_U3CAsyncStateU3Ek__BackingField_6(RuntimeObject * value)
	{
		___U3CAsyncStateU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAsyncStateU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CCompletedSynchronouslyU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CCompletedSynchronouslyU3Ek__BackingField_7)); }
	inline bool get_U3CCompletedSynchronouslyU3Ek__BackingField_7() const { return ___U3CCompletedSynchronouslyU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CCompletedSynchronouslyU3Ek__BackingField_7() { return &___U3CCompletedSynchronouslyU3Ek__BackingField_7; }
	inline void set_U3CCompletedSynchronouslyU3Ek__BackingField_7(bool value)
	{
		___U3CCompletedSynchronouslyU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIsCompletedU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CIsCompletedU3Ek__BackingField_8)); }
	inline bool get_U3CIsCompletedU3Ek__BackingField_8() const { return ___U3CIsCompletedU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIsCompletedU3Ek__BackingField_8() { return &___U3CIsCompletedU3Ek__BackingField_8; }
	inline void set_U3CIsCompletedU3Ek__BackingField_8(bool value)
	{
		___U3CIsCompletedU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CExceptionU3Ek__BackingField_9)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_9() const { return ___U3CExceptionU3Ek__BackingField_9; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_9() { return &___U3CExceptionU3Ek__BackingField_9; }
	inline void set_U3CExceptionU3Ek__BackingField_9(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CResponseU3Ek__BackingField_10)); }
	inline AmazonWebServiceResponse_t529043356 * get_U3CResponseU3Ek__BackingField_10() const { return ___U3CResponseU3Ek__BackingField_10; }
	inline AmazonWebServiceResponse_t529043356 ** get_address_of_U3CResponseU3Ek__BackingField_10() { return &___U3CResponseU3Ek__BackingField_10; }
	inline void set_U3CResponseU3Ek__BackingField_10(AmazonWebServiceResponse_t529043356 * value)
	{
		___U3CResponseU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CRequestU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CRequestU3Ek__BackingField_11)); }
	inline AmazonWebServiceRequest_t3384026212 * get_U3CRequestU3Ek__BackingField_11() const { return ___U3CRequestU3Ek__BackingField_11; }
	inline AmazonWebServiceRequest_t3384026212 ** get_address_of_U3CRequestU3Ek__BackingField_11() { return &___U3CRequestU3Ek__BackingField_11; }
	inline void set_U3CRequestU3Ek__BackingField_11(AmazonWebServiceRequest_t3384026212 * value)
	{
		___U3CRequestU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CActionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CActionU3Ek__BackingField_12)); }
	inline Action_4_t897279368 * get_U3CActionU3Ek__BackingField_12() const { return ___U3CActionU3Ek__BackingField_12; }
	inline Action_4_t897279368 ** get_address_of_U3CActionU3Ek__BackingField_12() { return &___U3CActionU3Ek__BackingField_12; }
	inline void set_U3CActionU3Ek__BackingField_12(Action_4_t897279368 * value)
	{
		___U3CActionU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CActionU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CAsyncOptionsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RuntimeAsyncResult_t4279356013, ___U3CAsyncOptionsU3Ek__BackingField_13)); }
	inline AsyncOptions_t558351272 * get_U3CAsyncOptionsU3Ek__BackingField_13() const { return ___U3CAsyncOptionsU3Ek__BackingField_13; }
	inline AsyncOptions_t558351272 ** get_address_of_U3CAsyncOptionsU3Ek__BackingField_13() { return &___U3CAsyncOptionsU3Ek__BackingField_13; }
	inline void set_U3CAsyncOptionsU3Ek__BackingField_13(AsyncOptions_t558351272 * value)
	{
		___U3CAsyncOptionsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAsyncOptionsU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEASYNCRESULT_T4279356013_H
#ifndef ABSTRACTAWSSIGNER_T2114314031_H
#define ABSTRACTAWSSIGNER_T2114314031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.AbstractAWSSigner
struct  AbstractAWSSigner_t2114314031  : public RuntimeObject
{
public:
	// Amazon.Runtime.Internal.Auth.AWS4Signer Amazon.Runtime.Internal.Auth.AbstractAWSSigner::_aws4Signer
	AWS4Signer_t1011449585 * ____aws4Signer_0;

public:
	inline static int32_t get_offset_of__aws4Signer_0() { return static_cast<int32_t>(offsetof(AbstractAWSSigner_t2114314031, ____aws4Signer_0)); }
	inline AWS4Signer_t1011449585 * get__aws4Signer_0() const { return ____aws4Signer_0; }
	inline AWS4Signer_t1011449585 ** get_address_of__aws4Signer_0() { return &____aws4Signer_0; }
	inline void set__aws4Signer_0(AWS4Signer_t1011449585 * value)
	{
		____aws4Signer_0 = value;
		Il2CppCodeGenWriteBarrier((&____aws4Signer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTAWSSIGNER_T2114314031_H
#ifndef U3CINVOKEREQUESTU3ED__7_T3085820275_H
#define U3CINVOKEREQUESTU3ED__7_T3085820275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7
struct  U3CInvokeRequestU3Ed__7_t3085820275  : public RuntimeObject
{
public:
	// System.Int32 Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Amazon.Runtime.Internal.UnityMainThreadDispatcher Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<>4__this
	UnityMainThreadDispatcher_t4098072663 * ___U3CU3E4__this_2;
	// Amazon.Runtime.IUnityHttpRequest Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::request
	RuntimeObject* ___request_3;
	// UnityEngine.WWW Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<wwwRequest>5__1
	WWW_t2919945039 * ___U3CwwwRequestU3E5__1_4;
	// System.Boolean Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<uploadCompleted>5__2
	bool ___U3CuploadCompletedU3E5__2_5;
	// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0 Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<>8__3
	U3CU3Ec__DisplayClass7_0_t142100588 * ___U3CU3E8__3_6;
	// Amazon.Runtime.Internal.UnityRequest Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<unityRequest>5__4
	UnityRequest_t4218620704 * ___U3CunityRequestU3E5__4_7;
	// UnityEngine.AsyncOperation Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<operation>5__5
	AsyncOperation_t3814632279 * ___U3CoperationU3E5__5_8;
	// System.Boolean Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<uploadCompleted>5__6
	bool ___U3CuploadCompletedU3E5__6_9;
	// Amazon.Runtime.Internal.UnityWebRequestWrapper Amazon.Runtime.Internal.UnityMainThreadDispatcher/<InvokeRequest>d__7::<unityWebRequest>5__7
	UnityWebRequestWrapper_t1542496577 * ___U3CunityWebRequestU3E5__7_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CU3E4__this_2)); }
	inline UnityMainThreadDispatcher_t4098072663 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline UnityMainThreadDispatcher_t4098072663 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(UnityMainThreadDispatcher_t4098072663 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_request_3() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___request_3)); }
	inline RuntimeObject* get_request_3() const { return ___request_3; }
	inline RuntimeObject** get_address_of_request_3() { return &___request_3; }
	inline void set_request_3(RuntimeObject* value)
	{
		___request_3 = value;
		Il2CppCodeGenWriteBarrier((&___request_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwRequestU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CwwwRequestU3E5__1_4)); }
	inline WWW_t2919945039 * get_U3CwwwRequestU3E5__1_4() const { return ___U3CwwwRequestU3E5__1_4; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwRequestU3E5__1_4() { return &___U3CwwwRequestU3E5__1_4; }
	inline void set_U3CwwwRequestU3E5__1_4(WWW_t2919945039 * value)
	{
		___U3CwwwRequestU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwRequestU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_U3CuploadCompletedU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CuploadCompletedU3E5__2_5)); }
	inline bool get_U3CuploadCompletedU3E5__2_5() const { return ___U3CuploadCompletedU3E5__2_5; }
	inline bool* get_address_of_U3CuploadCompletedU3E5__2_5() { return &___U3CuploadCompletedU3E5__2_5; }
	inline void set_U3CuploadCompletedU3E5__2_5(bool value)
	{
		___U3CuploadCompletedU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3E8__3_6() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CU3E8__3_6)); }
	inline U3CU3Ec__DisplayClass7_0_t142100588 * get_U3CU3E8__3_6() const { return ___U3CU3E8__3_6; }
	inline U3CU3Ec__DisplayClass7_0_t142100588 ** get_address_of_U3CU3E8__3_6() { return &___U3CU3E8__3_6; }
	inline void set_U3CU3E8__3_6(U3CU3Ec__DisplayClass7_0_t142100588 * value)
	{
		___U3CU3E8__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__3_6), value);
	}

	inline static int32_t get_offset_of_U3CunityRequestU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CunityRequestU3E5__4_7)); }
	inline UnityRequest_t4218620704 * get_U3CunityRequestU3E5__4_7() const { return ___U3CunityRequestU3E5__4_7; }
	inline UnityRequest_t4218620704 ** get_address_of_U3CunityRequestU3E5__4_7() { return &___U3CunityRequestU3E5__4_7; }
	inline void set_U3CunityRequestU3E5__4_7(UnityRequest_t4218620704 * value)
	{
		___U3CunityRequestU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunityRequestU3E5__4_7), value);
	}

	inline static int32_t get_offset_of_U3CoperationU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CoperationU3E5__5_8)); }
	inline AsyncOperation_t3814632279 * get_U3CoperationU3E5__5_8() const { return ___U3CoperationU3E5__5_8; }
	inline AsyncOperation_t3814632279 ** get_address_of_U3CoperationU3E5__5_8() { return &___U3CoperationU3E5__5_8; }
	inline void set_U3CoperationU3E5__5_8(AsyncOperation_t3814632279 * value)
	{
		___U3CoperationU3E5__5_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoperationU3E5__5_8), value);
	}

	inline static int32_t get_offset_of_U3CuploadCompletedU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CuploadCompletedU3E5__6_9)); }
	inline bool get_U3CuploadCompletedU3E5__6_9() const { return ___U3CuploadCompletedU3E5__6_9; }
	inline bool* get_address_of_U3CuploadCompletedU3E5__6_9() { return &___U3CuploadCompletedU3E5__6_9; }
	inline void set_U3CuploadCompletedU3E5__6_9(bool value)
	{
		___U3CuploadCompletedU3E5__6_9 = value;
	}

	inline static int32_t get_offset_of_U3CunityWebRequestU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CInvokeRequestU3Ed__7_t3085820275, ___U3CunityWebRequestU3E5__7_10)); }
	inline UnityWebRequestWrapper_t1542496577 * get_U3CunityWebRequestU3E5__7_10() const { return ___U3CunityWebRequestU3E5__7_10; }
	inline UnityWebRequestWrapper_t1542496577 ** get_address_of_U3CunityWebRequestU3E5__7_10() { return &___U3CunityWebRequestU3E5__7_10; }
	inline void set_U3CunityWebRequestU3E5__7_10(UnityWebRequestWrapper_t1542496577 * value)
	{
		___U3CunityWebRequestU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CunityWebRequestU3E5__7_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINVOKEREQUESTU3ED__7_T3085820275_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T142100588_H
#define U3CU3EC__DISPLAYCLASS7_0_T142100588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t142100588  : public RuntimeObject
{
public:
	// Amazon.Runtime.IUnityHttpRequest Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0::request
	RuntimeObject* ___request_0;
	// Amazon.Runtime.Internal.UnityMainThreadDispatcher Amazon.Runtime.Internal.UnityMainThreadDispatcher/<>c__DisplayClass7_0::<>4__this
	UnityMainThreadDispatcher_t4098072663 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t142100588, ___request_0)); }
	inline RuntimeObject* get_request_0() const { return ___request_0; }
	inline RuntimeObject** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(RuntimeObject* value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t142100588, ___U3CU3E4__this_1)); }
	inline UnityMainThreadDispatcher_t4098072663 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline UnityMainThreadDispatcher_t4098072663 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(UnityMainThreadDispatcher_t4098072663 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T142100588_H
#ifndef SIMPLEASYNCRESULT_T4203640901_H
#define SIMPLEASYNCRESULT_T4203640901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.SimpleAsyncResult
struct  SimpleAsyncResult_t4203640901  : public RuntimeObject
{
public:
	// System.Object Amazon.Runtime.Internal.SimpleAsyncResult::<AsyncState>k__BackingField
	RuntimeObject * ___U3CAsyncStateU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CAsyncStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SimpleAsyncResult_t4203640901, ___U3CAsyncStateU3Ek__BackingField_0)); }
	inline RuntimeObject * get_U3CAsyncStateU3Ek__BackingField_0() const { return ___U3CAsyncStateU3Ek__BackingField_0; }
	inline RuntimeObject ** get_address_of_U3CAsyncStateU3Ek__BackingField_0() { return &___U3CAsyncStateU3Ek__BackingField_0; }
	inline void set_U3CAsyncStateU3Ek__BackingField_0(RuntimeObject * value)
	{
		___U3CAsyncStateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAsyncStateU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEASYNCRESULT_T4203640901_H
#ifndef UNITYWWWREQUEST_T118609659_H
#define UNITYWWWREQUEST_T118609659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityWwwRequest
struct  UnityWwwRequest_t118609659  : public RuntimeObject
{
public:
	// System.Uri Amazon.Runtime.Internal.UnityWwwRequest::<RequestUri>k__BackingField
	Uri_t19570940 * ___U3CRequestUriU3Ek__BackingField_0;
	// System.IDisposable Amazon.Runtime.Internal.UnityWwwRequest::<WwwRequest>k__BackingField
	RuntimeObject* ___U3CWwwRequestU3Ek__BackingField_1;
	// System.Byte[] Amazon.Runtime.Internal.UnityWwwRequest::<RequestContent>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CRequestContentU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.UnityWwwRequest::<Headers>k__BackingField
	Dictionary_2_t3943999495 * ___U3CHeadersU3Ek__BackingField_3;
	// System.AsyncCallback Amazon.Runtime.Internal.UnityWwwRequest::<Callback>k__BackingField
	AsyncCallback_t163412349 * ___U3CCallbackU3Ek__BackingField_4;
	// System.IAsyncResult Amazon.Runtime.Internal.UnityWwwRequest::<AsyncResult>k__BackingField
	RuntimeObject* ___U3CAsyncResultU3Ek__BackingField_5;
	// System.Threading.ManualResetEvent Amazon.Runtime.Internal.UnityWwwRequest::<WaitHandle>k__BackingField
	ManualResetEvent_t926074657 * ___U3CWaitHandleU3Ek__BackingField_6;
	// System.Boolean Amazon.Runtime.Internal.UnityWwwRequest::<IsSync>k__BackingField
	bool ___U3CIsSyncU3Ek__BackingField_7;
	// Amazon.Runtime.Internal.Transform.IWebResponseData Amazon.Runtime.Internal.UnityWwwRequest::<Response>k__BackingField
	RuntimeObject* ___U3CResponseU3Ek__BackingField_8;
	// System.Exception Amazon.Runtime.Internal.UnityWwwRequest::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_9;
	// System.String Amazon.Runtime.Internal.UnityWwwRequest::<Method>k__BackingField
	String_t* ___U3CMethodU3Ek__BackingField_10;
	// Amazon.Runtime.Internal.StreamReadTracker Amazon.Runtime.Internal.UnityWwwRequest::<Tracker>k__BackingField
	StreamReadTracker_t1958363340 * ___U3CTrackerU3Ek__BackingField_11;
	// System.Boolean Amazon.Runtime.Internal.UnityWwwRequest::_disposed
	bool ____disposed_12;

public:
	inline static int32_t get_offset_of_U3CRequestUriU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CRequestUriU3Ek__BackingField_0)); }
	inline Uri_t19570940 * get_U3CRequestUriU3Ek__BackingField_0() const { return ___U3CRequestUriU3Ek__BackingField_0; }
	inline Uri_t19570940 ** get_address_of_U3CRequestUriU3Ek__BackingField_0() { return &___U3CRequestUriU3Ek__BackingField_0; }
	inline void set_U3CRequestUriU3Ek__BackingField_0(Uri_t19570940 * value)
	{
		___U3CRequestUriU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestUriU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CWwwRequestU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CWwwRequestU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CWwwRequestU3Ek__BackingField_1() const { return ___U3CWwwRequestU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CWwwRequestU3Ek__BackingField_1() { return &___U3CWwwRequestU3Ek__BackingField_1; }
	inline void set_U3CWwwRequestU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CWwwRequestU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWwwRequestU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRequestContentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CRequestContentU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t3397334013* get_U3CRequestContentU3Ek__BackingField_2() const { return ___U3CRequestContentU3Ek__BackingField_2; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CRequestContentU3Ek__BackingField_2() { return &___U3CRequestContentU3Ek__BackingField_2; }
	inline void set_U3CRequestContentU3Ek__BackingField_2(ByteU5BU5D_t3397334013* value)
	{
		___U3CRequestContentU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestContentU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CHeadersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CHeadersU3Ek__BackingField_3)); }
	inline Dictionary_2_t3943999495 * get_U3CHeadersU3Ek__BackingField_3() const { return ___U3CHeadersU3Ek__BackingField_3; }
	inline Dictionary_2_t3943999495 ** get_address_of_U3CHeadersU3Ek__BackingField_3() { return &___U3CHeadersU3Ek__BackingField_3; }
	inline void set_U3CHeadersU3Ek__BackingField_3(Dictionary_2_t3943999495 * value)
	{
		___U3CHeadersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeadersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CCallbackU3Ek__BackingField_4)); }
	inline AsyncCallback_t163412349 * get_U3CCallbackU3Ek__BackingField_4() const { return ___U3CCallbackU3Ek__BackingField_4; }
	inline AsyncCallback_t163412349 ** get_address_of_U3CCallbackU3Ek__BackingField_4() { return &___U3CCallbackU3Ek__BackingField_4; }
	inline void set_U3CCallbackU3Ek__BackingField_4(AsyncCallback_t163412349 * value)
	{
		___U3CCallbackU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CAsyncResultU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CAsyncResultU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CAsyncResultU3Ek__BackingField_5() const { return ___U3CAsyncResultU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CAsyncResultU3Ek__BackingField_5() { return &___U3CAsyncResultU3Ek__BackingField_5; }
	inline void set_U3CAsyncResultU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CAsyncResultU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAsyncResultU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CWaitHandleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CWaitHandleU3Ek__BackingField_6)); }
	inline ManualResetEvent_t926074657 * get_U3CWaitHandleU3Ek__BackingField_6() const { return ___U3CWaitHandleU3Ek__BackingField_6; }
	inline ManualResetEvent_t926074657 ** get_address_of_U3CWaitHandleU3Ek__BackingField_6() { return &___U3CWaitHandleU3Ek__BackingField_6; }
	inline void set_U3CWaitHandleU3Ek__BackingField_6(ManualResetEvent_t926074657 * value)
	{
		___U3CWaitHandleU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWaitHandleU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CIsSyncU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CIsSyncU3Ek__BackingField_7)); }
	inline bool get_U3CIsSyncU3Ek__BackingField_7() const { return ___U3CIsSyncU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CIsSyncU3Ek__BackingField_7() { return &___U3CIsSyncU3Ek__BackingField_7; }
	inline void set_U3CIsSyncU3Ek__BackingField_7(bool value)
	{
		___U3CIsSyncU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CResponseU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CResponseU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CResponseU3Ek__BackingField_8() const { return ___U3CResponseU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CResponseU3Ek__BackingField_8() { return &___U3CResponseU3Ek__BackingField_8; }
	inline void set_U3CResponseU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CResponseU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CExceptionU3Ek__BackingField_9)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_9() const { return ___U3CExceptionU3Ek__BackingField_9; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_9() { return &___U3CExceptionU3Ek__BackingField_9; }
	inline void set_U3CExceptionU3Ek__BackingField_9(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CMethodU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CMethodU3Ek__BackingField_10)); }
	inline String_t* get_U3CMethodU3Ek__BackingField_10() const { return ___U3CMethodU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CMethodU3Ek__BackingField_10() { return &___U3CMethodU3Ek__BackingField_10; }
	inline void set_U3CMethodU3Ek__BackingField_10(String_t* value)
	{
		___U3CMethodU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMethodU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTrackerU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ___U3CTrackerU3Ek__BackingField_11)); }
	inline StreamReadTracker_t1958363340 * get_U3CTrackerU3Ek__BackingField_11() const { return ___U3CTrackerU3Ek__BackingField_11; }
	inline StreamReadTracker_t1958363340 ** get_address_of_U3CTrackerU3Ek__BackingField_11() { return &___U3CTrackerU3Ek__BackingField_11; }
	inline void set_U3CTrackerU3Ek__BackingField_11(StreamReadTracker_t1958363340 * value)
	{
		___U3CTrackerU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTrackerU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of__disposed_12() { return static_cast<int32_t>(offsetof(UnityWwwRequest_t118609659, ____disposed_12)); }
	inline bool get__disposed_12() const { return ____disposed_12; }
	inline bool* get_address_of__disposed_12() { return &____disposed_12; }
	inline void set__disposed_12(bool value)
	{
		____disposed_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWWWREQUEST_T118609659_H
#ifndef U3CU3EC_T2266868118_H
#define U3CU3EC_T2266868118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.AWS4Signer/<>c
struct  U3CU3Ec_t2266868118  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2266868118_StaticFields
{
public:
	// Amazon.Runtime.Internal.Auth.AWS4Signer/<>c Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<>9
	U3CU3Ec_t2266868118 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String> Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<>9__46_0
	Func_2_t4206299577 * ___U3CU3E9__46_0_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String> Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<>9__47_0
	Func_2_t4206299577 * ___U3CU3E9__47_0_2;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.Boolean> Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<>9__48_0
	Func_2_t1707686766 * ___U3CU3E9__48_0_3;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String> Amazon.Runtime.Internal.Auth.AWS4Signer/<>c::<>9__52_0
	Func_2_t4206299577 * ___U3CU3E9__52_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2266868118_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2266868118 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2266868118 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2266868118 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__46_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2266868118_StaticFields, ___U3CU3E9__46_0_1)); }
	inline Func_2_t4206299577 * get_U3CU3E9__46_0_1() const { return ___U3CU3E9__46_0_1; }
	inline Func_2_t4206299577 ** get_address_of_U3CU3E9__46_0_1() { return &___U3CU3E9__46_0_1; }
	inline void set_U3CU3E9__46_0_1(Func_2_t4206299577 * value)
	{
		___U3CU3E9__46_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__46_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__47_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2266868118_StaticFields, ___U3CU3E9__47_0_2)); }
	inline Func_2_t4206299577 * get_U3CU3E9__47_0_2() const { return ___U3CU3E9__47_0_2; }
	inline Func_2_t4206299577 ** get_address_of_U3CU3E9__47_0_2() { return &___U3CU3E9__47_0_2; }
	inline void set_U3CU3E9__47_0_2(Func_2_t4206299577 * value)
	{
		___U3CU3E9__47_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__47_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__48_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2266868118_StaticFields, ___U3CU3E9__48_0_3)); }
	inline Func_2_t1707686766 * get_U3CU3E9__48_0_3() const { return ___U3CU3E9__48_0_3; }
	inline Func_2_t1707686766 ** get_address_of_U3CU3E9__48_0_3() { return &___U3CU3E9__48_0_3; }
	inline void set_U3CU3E9__48_0_3(Func_2_t1707686766 * value)
	{
		___U3CU3E9__48_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__48_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__52_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2266868118_StaticFields, ___U3CU3E9__52_0_4)); }
	inline Func_2_t4206299577 * get_U3CU3E9__52_0_4() const { return ___U3CU3E9__52_0_4; }
	inline Func_2_t4206299577 ** get_address_of_U3CU3E9__52_0_4() { return &___U3CU3E9__52_0_4; }
	inline void set_U3CU3E9__52_0_4(Func_2_t4206299577 * value)
	{
		___U3CU3E9__52_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__52_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2266868118_H
#ifndef AWS4SIGNER_T1011449585_H
#define AWS4SIGNER_T1011449585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.AWS4Signer
struct  AWS4Signer_t1011449585  : public AbstractAWSSigner_t2114314031
{
public:
	// System.Boolean Amazon.Runtime.Internal.Auth.AWS4Signer::<SignPayload>k__BackingField
	bool ___U3CSignPayloadU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CSignPayloadU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AWS4Signer_t1011449585, ___U3CSignPayloadU3Ek__BackingField_4)); }
	inline bool get_U3CSignPayloadU3Ek__BackingField_4() const { return ___U3CSignPayloadU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CSignPayloadU3Ek__BackingField_4() { return &___U3CSignPayloadU3Ek__BackingField_4; }
	inline void set_U3CSignPayloadU3Ek__BackingField_4(bool value)
	{
		___U3CSignPayloadU3Ek__BackingField_4 = value;
	}
};

struct AWS4Signer_t1011449585_StaticFields
{
public:
	// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4Signer::TerminatorBytes
	ByteU5BU5D_t3397334013* ___TerminatorBytes_1;
	// System.Text.RegularExpressions.Regex Amazon.Runtime.Internal.Auth.AWS4Signer::CompressWhitespaceRegex
	Regex_t1803876613 * ___CompressWhitespaceRegex_2;
	// System.Collections.Generic.IEnumerable`1<System.String> Amazon.Runtime.Internal.Auth.AWS4Signer::_headersToIgnoreWhenSigning
	RuntimeObject* ____headersToIgnoreWhenSigning_3;

public:
	inline static int32_t get_offset_of_TerminatorBytes_1() { return static_cast<int32_t>(offsetof(AWS4Signer_t1011449585_StaticFields, ___TerminatorBytes_1)); }
	inline ByteU5BU5D_t3397334013* get_TerminatorBytes_1() const { return ___TerminatorBytes_1; }
	inline ByteU5BU5D_t3397334013** get_address_of_TerminatorBytes_1() { return &___TerminatorBytes_1; }
	inline void set_TerminatorBytes_1(ByteU5BU5D_t3397334013* value)
	{
		___TerminatorBytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___TerminatorBytes_1), value);
	}

	inline static int32_t get_offset_of_CompressWhitespaceRegex_2() { return static_cast<int32_t>(offsetof(AWS4Signer_t1011449585_StaticFields, ___CompressWhitespaceRegex_2)); }
	inline Regex_t1803876613 * get_CompressWhitespaceRegex_2() const { return ___CompressWhitespaceRegex_2; }
	inline Regex_t1803876613 ** get_address_of_CompressWhitespaceRegex_2() { return &___CompressWhitespaceRegex_2; }
	inline void set_CompressWhitespaceRegex_2(Regex_t1803876613 * value)
	{
		___CompressWhitespaceRegex_2 = value;
		Il2CppCodeGenWriteBarrier((&___CompressWhitespaceRegex_2), value);
	}

	inline static int32_t get_offset_of__headersToIgnoreWhenSigning_3() { return static_cast<int32_t>(offsetof(AWS4Signer_t1011449585_StaticFields, ____headersToIgnoreWhenSigning_3)); }
	inline RuntimeObject* get__headersToIgnoreWhenSigning_3() const { return ____headersToIgnoreWhenSigning_3; }
	inline RuntimeObject** get_address_of__headersToIgnoreWhenSigning_3() { return &____headersToIgnoreWhenSigning_3; }
	inline void set__headersToIgnoreWhenSigning_3(RuntimeObject* value)
	{
		____headersToIgnoreWhenSigning_3 = value;
		Il2CppCodeGenWriteBarrier((&____headersToIgnoreWhenSigning_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AWS4SIGNER_T1011449585_H
#ifndef UNITYDEBUGLOGGER_T3216514868_H
#define UNITYDEBUGLOGGER_T3216514868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.UnityDebugLogger
struct  UnityDebugLogger_t3216514868  : public InternalLogger_t2972373343
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYDEBUGLOGGER_T3216514868_H
#ifndef HASHINGWRAPPERMD5_T3486550061_H
#define HASHINGWRAPPERMD5_T3486550061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.HashingWrapperMD5
struct  HashingWrapperMD5_t3486550061  : public HashingWrapper_t516426937
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHINGWRAPPERMD5_T3486550061_H
#ifndef WRAPPERSTREAM_T816765491_H
#define WRAPPERSTREAM_T816765491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.WrapperStream
struct  WrapperStream_t816765491  : public Stream_t3255436806
{
public:
	// System.IO.Stream Amazon.Runtime.Internal.Util.WrapperStream::<BaseStream>k__BackingField
	Stream_t3255436806 * ___U3CBaseStreamU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBaseStreamU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WrapperStream_t816765491, ___U3CBaseStreamU3Ek__BackingField_1)); }
	inline Stream_t3255436806 * get_U3CBaseStreamU3Ek__BackingField_1() const { return ___U3CBaseStreamU3Ek__BackingField_1; }
	inline Stream_t3255436806 ** get_address_of_U3CBaseStreamU3Ek__BackingField_1() { return &___U3CBaseStreamU3Ek__BackingField_1; }
	inline void set_U3CBaseStreamU3Ek__BackingField_1(Stream_t3255436806 * value)
	{
		___U3CBaseStreamU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBaseStreamU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPERSTREAM_T816765491_H
#ifndef NULLABLE_1_T2088641033_H
#define NULLABLE_1_T2088641033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t2088641033 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2088641033_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef XMLRESPONSEUNMARSHALLER_T760346204_H
#define XMLRESPONSEUNMARSHALLER_T760346204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.XmlResponseUnmarshaller
struct  XmlResponseUnmarshaller_t760346204  : public ResponseUnmarshaller_t3934041557
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRESPONSEUNMARSHALLER_T760346204_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef __STATICARRAYINITTYPESIZEU3D112_T2631791551_H
#define __STATICARRAYINITTYPESIZEU3D112_T2631791551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112
struct  __StaticArrayInitTypeSizeU3D112_t2631791551 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D112_t2631791551__padding[112];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D112_T2631791551_H
#ifndef TRACELISTENER_T3414949279_H
#define TRACELISTENER_T3414949279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Diagnostics.TraceListener
struct  TraceListener_t3414949279  : public MarshalByRefObject_t1285298191
{
public:
	// System.Int32 System.Diagnostics.TraceListener::indentSize
	int32_t ___indentSize_1;
	// System.Collections.Specialized.StringDictionary System.Diagnostics.TraceListener::attributes
	StringDictionary_t1070889667 * ___attributes_2;
	// System.String System.Diagnostics.TraceListener::name
	String_t* ___name_3;
	// System.Boolean System.Diagnostics.TraceListener::needIndent
	bool ___needIndent_4;

public:
	inline static int32_t get_offset_of_indentSize_1() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___indentSize_1)); }
	inline int32_t get_indentSize_1() const { return ___indentSize_1; }
	inline int32_t* get_address_of_indentSize_1() { return &___indentSize_1; }
	inline void set_indentSize_1(int32_t value)
	{
		___indentSize_1 = value;
	}

	inline static int32_t get_offset_of_attributes_2() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___attributes_2)); }
	inline StringDictionary_t1070889667 * get_attributes_2() const { return ___attributes_2; }
	inline StringDictionary_t1070889667 ** get_address_of_attributes_2() { return &___attributes_2; }
	inline void set_attributes_2(StringDictionary_t1070889667 * value)
	{
		___attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_needIndent_4() { return static_cast<int32_t>(offsetof(TraceListener_t3414949279, ___needIndent_4)); }
	inline bool get_needIndent_4() const { return ___needIndent_4; }
	inline bool* get_address_of_needIndent_4() { return &___needIndent_4; }
	inline void set_needIndent_4(bool value)
	{
		___needIndent_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACELISTENER_T3414949279_H
#ifndef ERRORCALLBACKHANDLER_T394665619_H
#define ERRORCALLBACKHANDLER_T394665619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ErrorCallbackHandler
struct  ErrorCallbackHandler_t394665619  : public PipelineHandler_t1486422324
{
public:
	// System.Action`2<Amazon.Runtime.IExecutionContext,System.Exception> Amazon.Runtime.Internal.ErrorCallbackHandler::<OnError>k__BackingField
	Action_2_t3657272536 * ___U3COnErrorU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3COnErrorU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ErrorCallbackHandler_t394665619, ___U3COnErrorU3Ek__BackingField_3)); }
	inline Action_2_t3657272536 * get_U3COnErrorU3Ek__BackingField_3() const { return ___U3COnErrorU3Ek__BackingField_3; }
	inline Action_2_t3657272536 ** get_address_of_U3COnErrorU3Ek__BackingField_3() { return &___U3COnErrorU3Ek__BackingField_3; }
	inline void set_U3COnErrorU3Ek__BackingField_3(Action_2_t3657272536 * value)
	{
		___U3COnErrorU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnErrorU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCALLBACKHANDLER_T394665619_H
#ifndef MARSHALLER_T3371889241_H
#define MARSHALLER_T3371889241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Marshaller
struct  Marshaller_t3371889241  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALLER_T3371889241_H
#ifndef ENDPOINTRESOLVER_T2369326703_H
#define ENDPOINTRESOLVER_T2369326703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.EndpointResolver
struct  EndpointResolver_t2369326703  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTRESOLVER_T2369326703_H
#ifndef BACKGROUNDINVOKER_T1722929158_H
#define BACKGROUNDINVOKER_T1722929158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.BackgroundInvoker
struct  BackgroundInvoker_t1722929158  : public BackgroundDispatcher_1_t3230906411
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDINVOKER_T1722929158_H
#ifndef CREDENTIALSRETRIEVER_T208022274_H
#define CREDENTIALSRETRIEVER_T208022274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.CredentialsRetriever
struct  CredentialsRetriever_t208022274  : public PipelineHandler_t1486422324
{
public:
	// Amazon.Runtime.AWSCredentials Amazon.Runtime.Internal.CredentialsRetriever::<Credentials>k__BackingField
	AWSCredentials_t3583921007 * ___U3CCredentialsU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CredentialsRetriever_t208022274, ___U3CCredentialsU3Ek__BackingField_3)); }
	inline AWSCredentials_t3583921007 * get_U3CCredentialsU3Ek__BackingField_3() const { return ___U3CCredentialsU3Ek__BackingField_3; }
	inline AWSCredentials_t3583921007 ** get_address_of_U3CCredentialsU3Ek__BackingField_3() { return &___U3CCredentialsU3Ek__BackingField_3; }
	inline void set_U3CCredentialsU3Ek__BackingField_3(AWSCredentials_t3583921007 * value)
	{
		___U3CCredentialsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCredentialsU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREDENTIALSRETRIEVER_T208022274_H
#ifndef METRICSHANDLER_T1107617423_H
#define METRICSHANDLER_T1107617423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.MetricsHandler
struct  MetricsHandler_t1107617423  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METRICSHANDLER_T1107617423_H
#ifndef SIGNER_T1810841758_H
#define SIGNER_T1810841758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Signer
struct  Signer_t1810841758  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNER_T1810841758_H
#ifndef UNMARSHALLER_T1994457618_H
#define UNMARSHALLER_T1994457618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Unmarshaller
struct  Unmarshaller_t1994457618  : public PipelineHandler_t1486422324
{
public:
	// System.Boolean Amazon.Runtime.Internal.Unmarshaller::_supportsResponseLogging
	bool ____supportsResponseLogging_3;

public:
	inline static int32_t get_offset_of__supportsResponseLogging_3() { return static_cast<int32_t>(offsetof(Unmarshaller_t1994457618, ____supportsResponseLogging_3)); }
	inline bool get__supportsResponseLogging_3() const { return ____supportsResponseLogging_3; }
	inline bool* get_address_of__supportsResponseLogging_3() { return &____supportsResponseLogging_3; }
	inline void set__supportsResponseLogging_3(bool value)
	{
		____supportsResponseLogging_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNMARSHALLER_T1994457618_H
#ifndef REDIRECTHANDLER_T32990664_H
#define REDIRECTHANDLER_T32990664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RedirectHandler
struct  RedirectHandler_t32990664  : public PipelineHandler_t1486422324
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REDIRECTHANDLER_T32990664_H
#ifndef THREADPOOLEXECUTIONHANDLER_T2707466110_H
#define THREADPOOLEXECUTIONHANDLER_T2707466110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ThreadPoolExecutionHandler
struct  ThreadPoolExecutionHandler_t2707466110  : public PipelineHandler_t1486422324
{
public:

public:
};

struct ThreadPoolExecutionHandler_t2707466110_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.ThreadPoolThrottler`1<Amazon.Runtime.IAsyncExecutionContext> Amazon.Runtime.Internal.ThreadPoolExecutionHandler::_throttler
	ThreadPoolThrottler_1_t736579114 * ____throttler_3;
	// System.Object Amazon.Runtime.Internal.ThreadPoolExecutionHandler::_lock
	RuntimeObject * ____lock_4;

public:
	inline static int32_t get_offset_of__throttler_3() { return static_cast<int32_t>(offsetof(ThreadPoolExecutionHandler_t2707466110_StaticFields, ____throttler_3)); }
	inline ThreadPoolThrottler_1_t736579114 * get__throttler_3() const { return ____throttler_3; }
	inline ThreadPoolThrottler_1_t736579114 ** get_address_of__throttler_3() { return &____throttler_3; }
	inline void set__throttler_3(ThreadPoolThrottler_1_t736579114 * value)
	{
		____throttler_3 = value;
		Il2CppCodeGenWriteBarrier((&____throttler_3), value);
	}

	inline static int32_t get_offset_of__lock_4() { return static_cast<int32_t>(offsetof(ThreadPoolExecutionHandler_t2707466110_StaticFields, ____lock_4)); }
	inline RuntimeObject * get__lock_4() const { return ____lock_4; }
	inline RuntimeObject ** get_address_of__lock_4() { return &____lock_4; }
	inline void set__lock_4(RuntimeObject * value)
	{
		____lock_4 = value;
		Il2CppCodeGenWriteBarrier((&____lock_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADPOOLEXECUTIONHANDLER_T2707466110_H
#ifndef RETRYHANDLER_T3257318146_H
#define RETRYHANDLER_T3257318146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.RetryHandler
struct  RetryHandler_t3257318146  : public PipelineHandler_t1486422324
{
public:
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.RetryHandler::_logger
	RuntimeObject* ____logger_3;
	// Amazon.Runtime.RetryPolicy Amazon.Runtime.Internal.RetryHandler::<RetryPolicy>k__BackingField
	RetryPolicy_t1476739446 * ___U3CRetryPolicyU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__logger_3() { return static_cast<int32_t>(offsetof(RetryHandler_t3257318146, ____logger_3)); }
	inline RuntimeObject* get__logger_3() const { return ____logger_3; }
	inline RuntimeObject** get_address_of__logger_3() { return &____logger_3; }
	inline void set__logger_3(RuntimeObject* value)
	{
		____logger_3 = value;
		Il2CppCodeGenWriteBarrier((&____logger_3), value);
	}

	inline static int32_t get_offset_of_U3CRetryPolicyU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RetryHandler_t3257318146, ___U3CRetryPolicyU3Ek__BackingField_4)); }
	inline RetryPolicy_t1476739446 * get_U3CRetryPolicyU3Ek__BackingField_4() const { return ___U3CRetryPolicyU3Ek__BackingField_4; }
	inline RetryPolicy_t1476739446 ** get_address_of_U3CRetryPolicyU3Ek__BackingField_4() { return &___U3CRetryPolicyU3Ek__BackingField_4; }
	inline void set_U3CRetryPolicyU3Ek__BackingField_4(RetryPolicy_t1476739446 * value)
	{
		___U3CRetryPolicyU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRetryPolicyU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYHANDLER_T3257318146_H
#ifndef CALLBACKHANDLER_T174745619_H
#define CALLBACKHANDLER_T174745619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.CallbackHandler
struct  CallbackHandler_t174745619  : public PipelineHandler_t1486422324
{
public:
	// System.Action`1<Amazon.Runtime.IExecutionContext> Amazon.Runtime.Internal.CallbackHandler::<OnPreInvoke>k__BackingField
	Action_1_t2278930134 * ___U3COnPreInvokeU3Ek__BackingField_3;
	// System.Action`1<Amazon.Runtime.IExecutionContext> Amazon.Runtime.Internal.CallbackHandler::<OnPostInvoke>k__BackingField
	Action_1_t2278930134 * ___U3COnPostInvokeU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3COnPreInvokeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CallbackHandler_t174745619, ___U3COnPreInvokeU3Ek__BackingField_3)); }
	inline Action_1_t2278930134 * get_U3COnPreInvokeU3Ek__BackingField_3() const { return ___U3COnPreInvokeU3Ek__BackingField_3; }
	inline Action_1_t2278930134 ** get_address_of_U3COnPreInvokeU3Ek__BackingField_3() { return &___U3COnPreInvokeU3Ek__BackingField_3; }
	inline void set_U3COnPreInvokeU3Ek__BackingField_3(Action_1_t2278930134 * value)
	{
		___U3COnPreInvokeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnPreInvokeU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3COnPostInvokeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CallbackHandler_t174745619, ___U3COnPostInvokeU3Ek__BackingField_4)); }
	inline Action_1_t2278930134 * get_U3COnPostInvokeU3Ek__BackingField_4() const { return ___U3COnPostInvokeU3Ek__BackingField_4; }
	inline Action_1_t2278930134 ** get_address_of_U3COnPostInvokeU3Ek__BackingField_4() { return &___U3COnPostInvokeU3Ek__BackingField_4; }
	inline void set_U3COnPostInvokeU3Ek__BackingField_4(Action_1_t2278930134 * value)
	{
		___U3COnPostInvokeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnPostInvokeU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKHANDLER_T174745619_H
#ifndef HTTPERRORRESPONSEEXCEPTIONHANDLER_T3899688054_H
#define HTTPERRORRESPONSEEXCEPTIONHANDLER_T3899688054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.HttpErrorResponseExceptionHandler
struct  HttpErrorResponseExceptionHandler_t3899688054  : public ExceptionHandler_1_t2197621571
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPERRORRESPONSEEXCEPTIONHANDLER_T3899688054_H
#ifndef ERRORHANDLER_T1573534074_H
#define ERRORHANDLER_T1573534074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.ErrorHandler
struct  ErrorHandler_t1573534074  : public PipelineHandler_t1486422324
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,Amazon.Runtime.IExceptionHandler> Amazon.Runtime.Internal.ErrorHandler::_exceptionHandlers
	RuntimeObject* ____exceptionHandlers_3;

public:
	inline static int32_t get_offset_of__exceptionHandlers_3() { return static_cast<int32_t>(offsetof(ErrorHandler_t1573534074, ____exceptionHandlers_3)); }
	inline RuntimeObject* get__exceptionHandlers_3() const { return ____exceptionHandlers_3; }
	inline RuntimeObject** get_address_of__exceptionHandlers_3() { return &____exceptionHandlers_3; }
	inline void set__exceptionHandlers_3(RuntimeObject* value)
	{
		____exceptionHandlers_3 = value;
		Il2CppCodeGenWriteBarrier((&____exceptionHandlers_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORHANDLER_T1573534074_H
#ifndef INTERNALCONSOLELOGGER_T4018195642_H
#define INTERNALCONSOLELOGGER_T4018195642_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.InternalConsoleLogger
struct  InternalConsoleLogger_t4018195642  : public InternalLogger_t2972373343
{
public:

public:
};

struct InternalConsoleLogger_t4018195642_StaticFields
{
public:
	// System.Int64 Amazon.Runtime.Internal.Util.InternalConsoleLogger::_sequanceId
	int64_t ____sequanceId_2;

public:
	inline static int32_t get_offset_of__sequanceId_2() { return static_cast<int32_t>(offsetof(InternalConsoleLogger_t4018195642_StaticFields, ____sequanceId_2)); }
	inline int64_t get__sequanceId_2() const { return ____sequanceId_2; }
	inline int64_t* get_address_of__sequanceId_2() { return &____sequanceId_2; }
	inline void set__sequanceId_2(int64_t value)
	{
		____sequanceId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONSOLELOGGER_T4018195642_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef RETRYPOLICY_T1476739446_H
#define RETRYPOLICY_T1476739446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.RetryPolicy
struct  RetryPolicy_t1476739446  : public RuntimeObject
{
public:
	// System.Int32 Amazon.Runtime.RetryPolicy::<MaxRetries>k__BackingField
	int32_t ___U3CMaxRetriesU3Ek__BackingField_0;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.RetryPolicy::<Logger>k__BackingField
	RuntimeObject* ___U3CLoggerU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMaxRetriesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446, ___U3CMaxRetriesU3Ek__BackingField_0)); }
	inline int32_t get_U3CMaxRetriesU3Ek__BackingField_0() const { return ___U3CMaxRetriesU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CMaxRetriesU3Ek__BackingField_0() { return &___U3CMaxRetriesU3Ek__BackingField_0; }
	inline void set_U3CMaxRetriesU3Ek__BackingField_0(int32_t value)
	{
		___U3CMaxRetriesU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLoggerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446, ___U3CLoggerU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CLoggerU3Ek__BackingField_1() const { return ___U3CLoggerU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CLoggerU3Ek__BackingField_1() { return &___U3CLoggerU3Ek__BackingField_1; }
	inline void set_U3CLoggerU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CLoggerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggerU3Ek__BackingField_1), value);
	}
};

struct RetryPolicy_t1476739446_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.RetryPolicy::clockSkewErrorCodes
	HashSet_1_t362681087 * ___clockSkewErrorCodes_2;
	// System.TimeSpan Amazon.Runtime.RetryPolicy::clockSkewMaxThreshold
	TimeSpan_t3430258949  ___clockSkewMaxThreshold_3;

public:
	inline static int32_t get_offset_of_clockSkewErrorCodes_2() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446_StaticFields, ___clockSkewErrorCodes_2)); }
	inline HashSet_1_t362681087 * get_clockSkewErrorCodes_2() const { return ___clockSkewErrorCodes_2; }
	inline HashSet_1_t362681087 ** get_address_of_clockSkewErrorCodes_2() { return &___clockSkewErrorCodes_2; }
	inline void set_clockSkewErrorCodes_2(HashSet_1_t362681087 * value)
	{
		___clockSkewErrorCodes_2 = value;
		Il2CppCodeGenWriteBarrier((&___clockSkewErrorCodes_2), value);
	}

	inline static int32_t get_offset_of_clockSkewMaxThreshold_3() { return static_cast<int32_t>(offsetof(RetryPolicy_t1476739446_StaticFields, ___clockSkewMaxThreshold_3)); }
	inline TimeSpan_t3430258949  get_clockSkewMaxThreshold_3() const { return ___clockSkewMaxThreshold_3; }
	inline TimeSpan_t3430258949 * get_address_of_clockSkewMaxThreshold_3() { return &___clockSkewMaxThreshold_3; }
	inline void set_clockSkewMaxThreshold_3(TimeSpan_t3430258949  value)
	{
		___clockSkewMaxThreshold_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYPOLICY_T1476739446_H
#ifndef PARTIALWRAPPERSTREAM_T1744080210_H
#define PARTIALWRAPPERSTREAM_T1744080210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.PartialWrapperStream
struct  PartialWrapperStream_t1744080210  : public WrapperStream_t816765491
{
public:
	// System.Int64 Amazon.Runtime.Internal.Util.PartialWrapperStream::initialPosition
	int64_t ___initialPosition_2;
	// System.Int64 Amazon.Runtime.Internal.Util.PartialWrapperStream::partSize
	int64_t ___partSize_3;

public:
	inline static int32_t get_offset_of_initialPosition_2() { return static_cast<int32_t>(offsetof(PartialWrapperStream_t1744080210, ___initialPosition_2)); }
	inline int64_t get_initialPosition_2() const { return ___initialPosition_2; }
	inline int64_t* get_address_of_initialPosition_2() { return &___initialPosition_2; }
	inline void set_initialPosition_2(int64_t value)
	{
		___initialPosition_2 = value;
	}

	inline static int32_t get_offset_of_partSize_3() { return static_cast<int32_t>(offsetof(PartialWrapperStream_t1744080210, ___partSize_3)); }
	inline int64_t get_partSize_3() const { return ___partSize_3; }
	inline int64_t* get_address_of_partSize_3() { return &___partSize_3; }
	inline void set_partSize_3(int64_t value)
	{
		___partSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTIALWRAPPERSTREAM_T1744080210_H
#ifndef XMLNODETYPE_T739504597_H
#define XMLNODETYPE_T739504597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_t739504597 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeType_t739504597, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_T739504597_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef JSONTOKEN_T2445581255_H
#define JSONTOKEN_T2445581255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonToken
struct  JsonToken_t2445581255 
{
public:
	// System.Int32 ThirdParty.Json.LitJson.JsonToken::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonToken_t2445581255, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T2445581255_H
#ifndef METRIC_T3273440202_H
#define METRIC_T3273440202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Metric
struct  Metric_t3273440202 
{
public:
	// System.Int32 Amazon.Runtime.Metric::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Metric_t3273440202, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METRIC_T3273440202_H
#ifndef HTTPSTATUSCODE_T1898409641_H
#define HTTPSTATUSCODE_T1898409641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_t1898409641 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t1898409641, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T1898409641_H
#ifndef NETWORKSTATUS_T879095062_H
#define NETWORKSTATUS_T879095062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.PlatformServices.NetworkStatus
struct  NetworkStatus_t879095062 
{
public:
	// System.Int32 Amazon.Util.Internal.PlatformServices.NetworkStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NetworkStatus_t879095062, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKSTATUS_T879095062_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef EVENTHANDLE_T942672932_H
#define EVENTHANDLE_T942672932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.EventHandle
struct  EventHandle_t942672932 
{
public:
	// System.Int32 UnityEngine.EventSystems.EventHandle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventHandle_t942672932, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLE_T942672932_H
#ifndef LOADSTATE_T3882482337_H
#define LOADSTATE_T3882482337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.InternalLog4netLogger/LoadState
struct  LoadState_t3882482337 
{
public:
	// System.Int32 Amazon.Runtime.Internal.Util.InternalLog4netLogger/LoadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoadState_t3882482337, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSTATE_T3882482337_H
#ifndef CLIENTPROTOCOL_T4005895111_H
#define CLIENTPROTOCOL_T4005895111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.ClientProtocol
struct  ClientProtocol_t4005895111 
{
public:
	// System.Int32 Amazon.Runtime.Internal.Auth.ClientProtocol::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ClientProtocol_t4005895111, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTPROTOCOL_T4005895111_H
#ifndef LOGLEVEL_T527408354_H
#define LOGLEVEL_T527408354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.InternalConsoleLogger/LogLevel
struct  LogLevel_t527408354 
{
public:
	// System.Int32 Amazon.Runtime.Internal.Util.InternalConsoleLogger/LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t527408354, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T527408354_H
#ifndef HASHSTREAM_T2318308192_H
#define HASHSTREAM_T2318308192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.HashStream
struct  HashStream_t2318308192  : public WrapperStream_t816765491
{
public:
	// Amazon.Runtime.Internal.Util.IHashingWrapper Amazon.Runtime.Internal.Util.HashStream::<Algorithm>k__BackingField
	RuntimeObject* ___U3CAlgorithmU3Ek__BackingField_2;
	// System.Int64 Amazon.Runtime.Internal.Util.HashStream::<CurrentPosition>k__BackingField
	int64_t ___U3CCurrentPositionU3Ek__BackingField_3;
	// System.Byte[] Amazon.Runtime.Internal.Util.HashStream::<CalculatedHash>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CCalculatedHashU3Ek__BackingField_4;
	// System.Byte[] Amazon.Runtime.Internal.Util.HashStream::<ExpectedHash>k__BackingField
	ByteU5BU5D_t3397334013* ___U3CExpectedHashU3Ek__BackingField_5;
	// System.Int64 Amazon.Runtime.Internal.Util.HashStream::<ExpectedLength>k__BackingField
	int64_t ___U3CExpectedLengthU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CAlgorithmU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CAlgorithmU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CAlgorithmU3Ek__BackingField_2() const { return ___U3CAlgorithmU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CAlgorithmU3Ek__BackingField_2() { return &___U3CAlgorithmU3Ek__BackingField_2; }
	inline void set_U3CAlgorithmU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CAlgorithmU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAlgorithmU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCurrentPositionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CCurrentPositionU3Ek__BackingField_3)); }
	inline int64_t get_U3CCurrentPositionU3Ek__BackingField_3() const { return ___U3CCurrentPositionU3Ek__BackingField_3; }
	inline int64_t* get_address_of_U3CCurrentPositionU3Ek__BackingField_3() { return &___U3CCurrentPositionU3Ek__BackingField_3; }
	inline void set_U3CCurrentPositionU3Ek__BackingField_3(int64_t value)
	{
		___U3CCurrentPositionU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCalculatedHashU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CCalculatedHashU3Ek__BackingField_4)); }
	inline ByteU5BU5D_t3397334013* get_U3CCalculatedHashU3Ek__BackingField_4() const { return ___U3CCalculatedHashU3Ek__BackingField_4; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CCalculatedHashU3Ek__BackingField_4() { return &___U3CCalculatedHashU3Ek__BackingField_4; }
	inline void set_U3CCalculatedHashU3Ek__BackingField_4(ByteU5BU5D_t3397334013* value)
	{
		___U3CCalculatedHashU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCalculatedHashU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CExpectedHashU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CExpectedHashU3Ek__BackingField_5)); }
	inline ByteU5BU5D_t3397334013* get_U3CExpectedHashU3Ek__BackingField_5() const { return ___U3CExpectedHashU3Ek__BackingField_5; }
	inline ByteU5BU5D_t3397334013** get_address_of_U3CExpectedHashU3Ek__BackingField_5() { return &___U3CExpectedHashU3Ek__BackingField_5; }
	inline void set_U3CExpectedHashU3Ek__BackingField_5(ByteU5BU5D_t3397334013* value)
	{
		___U3CExpectedHashU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExpectedHashU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CExpectedLengthU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HashStream_t2318308192, ___U3CExpectedLengthU3Ek__BackingField_6)); }
	inline int64_t get_U3CExpectedLengthU3Ek__BackingField_6() const { return ___U3CExpectedLengthU3Ek__BackingField_6; }
	inline int64_t* get_address_of_U3CExpectedLengthU3Ek__BackingField_6() { return &___U3CExpectedLengthU3Ek__BackingField_6; }
	inline void set_U3CExpectedLengthU3Ek__BackingField_6(int64_t value)
	{
		___U3CExpectedLengthU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSTREAM_T2318308192_H
#ifndef CACHINGWRAPPERSTREAM_T380166576_H
#define CACHINGWRAPPERSTREAM_T380166576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.CachingWrapperStream
struct  CachingWrapperStream_t380166576  : public WrapperStream_t816765491
{
public:
	// System.Int32 Amazon.Runtime.Internal.Util.CachingWrapperStream::_cacheLimit
	int32_t ____cacheLimit_2;
	// System.Int32 Amazon.Runtime.Internal.Util.CachingWrapperStream::_cachedBytes
	int32_t ____cachedBytes_3;
	// System.Collections.Generic.List`1<System.Byte> Amazon.Runtime.Internal.Util.CachingWrapperStream::<AllReadBytes>k__BackingField
	List_1_t3052225568 * ___U3CAllReadBytesU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__cacheLimit_2() { return static_cast<int32_t>(offsetof(CachingWrapperStream_t380166576, ____cacheLimit_2)); }
	inline int32_t get__cacheLimit_2() const { return ____cacheLimit_2; }
	inline int32_t* get_address_of__cacheLimit_2() { return &____cacheLimit_2; }
	inline void set__cacheLimit_2(int32_t value)
	{
		____cacheLimit_2 = value;
	}

	inline static int32_t get_offset_of__cachedBytes_3() { return static_cast<int32_t>(offsetof(CachingWrapperStream_t380166576, ____cachedBytes_3)); }
	inline int32_t get__cachedBytes_3() const { return ____cachedBytes_3; }
	inline int32_t* get_address_of__cachedBytes_3() { return &____cachedBytes_3; }
	inline void set__cachedBytes_3(int32_t value)
	{
		____cachedBytes_3 = value;
	}

	inline static int32_t get_offset_of_U3CAllReadBytesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CachingWrapperStream_t380166576, ___U3CAllReadBytesU3Ek__BackingField_4)); }
	inline List_1_t3052225568 * get_U3CAllReadBytesU3Ek__BackingField_4() const { return ___U3CAllReadBytesU3Ek__BackingField_4; }
	inline List_1_t3052225568 ** get_address_of_U3CAllReadBytesU3Ek__BackingField_4() { return &___U3CAllReadBytesU3Ek__BackingField_4; }
	inline void set_U3CAllReadBytesU3Ek__BackingField_4(List_1_t3052225568 * value)
	{
		___U3CAllReadBytesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAllReadBytesU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGWRAPPERSTREAM_T380166576_H
#ifndef READSTRATEGY_T4036775975_H
#define READSTRATEGY_T4036775975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/ReadStrategy
struct  ReadStrategy_t4036775975 
{
public:
	// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/ReadStrategy::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadStrategy_t4036775975, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTRATEGY_T4036775975_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=112 <PrivateImplementationDetails>::50B1635D1FB2907A171B71751E1A3FA79423CA17
	__StaticArrayInitTypeSizeU3D112_t2631791551  ___50B1635D1FB2907A171B71751E1A3FA79423CA17_0;

public:
	inline static int32_t get_offset_of_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___50B1635D1FB2907A171B71751E1A3FA79423CA17_0)); }
	inline __StaticArrayInitTypeSizeU3D112_t2631791551  get_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0() const { return ___50B1635D1FB2907A171B71751E1A3FA79423CA17_0; }
	inline __StaticArrayInitTypeSizeU3D112_t2631791551 * get_address_of_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0() { return &___50B1635D1FB2907A171B71751E1A3FA79423CA17_0; }
	inline void set_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0(__StaticArrayInitTypeSizeU3D112_t2631791551  value)
	{
		___50B1635D1FB2907A171B71751E1A3FA79423CA17_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#ifndef UNITYDEBUGTRACELISTENER_T2078152869_H
#define UNITYDEBUGTRACELISTENER_T2078152869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.UnityDebugTraceListener
struct  UnityDebugTraceListener_t2078152869  : public TraceListener_t3414949279
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYDEBUGTRACELISTENER_T2078152869_H
#ifndef NULLABLE_1_T708647570_H
#define NULLABLE_1_T708647570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<ThirdParty.Json.LitJson.JsonToken>
struct  Nullable_1_t708647570 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t708647570, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t708647570, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T708647570_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef INTERNALLOG4NETLOGGER_T1777422022_H
#define INTERNALLOG4NETLOGGER_T1777422022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.InternalLog4netLogger
struct  InternalLog4netLogger_t1777422022  : public InternalLogger_t2972373343
{
public:
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::internalLogger
	RuntimeObject * ___internalLogger_18;
	// System.Nullable`1<System.Boolean> Amazon.Runtime.Internal.Util.InternalLog4netLogger::isErrorEnabled
	Nullable_1_t2088641033  ___isErrorEnabled_19;
	// System.Nullable`1<System.Boolean> Amazon.Runtime.Internal.Util.InternalLog4netLogger::isDebugEnabled
	Nullable_1_t2088641033  ___isDebugEnabled_20;
	// System.Nullable`1<System.Boolean> Amazon.Runtime.Internal.Util.InternalLog4netLogger::isInfoEnabled
	Nullable_1_t2088641033  ___isInfoEnabled_21;

public:
	inline static int32_t get_offset_of_internalLogger_18() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022, ___internalLogger_18)); }
	inline RuntimeObject * get_internalLogger_18() const { return ___internalLogger_18; }
	inline RuntimeObject ** get_address_of_internalLogger_18() { return &___internalLogger_18; }
	inline void set_internalLogger_18(RuntimeObject * value)
	{
		___internalLogger_18 = value;
		Il2CppCodeGenWriteBarrier((&___internalLogger_18), value);
	}

	inline static int32_t get_offset_of_isErrorEnabled_19() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022, ___isErrorEnabled_19)); }
	inline Nullable_1_t2088641033  get_isErrorEnabled_19() const { return ___isErrorEnabled_19; }
	inline Nullable_1_t2088641033 * get_address_of_isErrorEnabled_19() { return &___isErrorEnabled_19; }
	inline void set_isErrorEnabled_19(Nullable_1_t2088641033  value)
	{
		___isErrorEnabled_19 = value;
	}

	inline static int32_t get_offset_of_isDebugEnabled_20() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022, ___isDebugEnabled_20)); }
	inline Nullable_1_t2088641033  get_isDebugEnabled_20() const { return ___isDebugEnabled_20; }
	inline Nullable_1_t2088641033 * get_address_of_isDebugEnabled_20() { return &___isDebugEnabled_20; }
	inline void set_isDebugEnabled_20(Nullable_1_t2088641033  value)
	{
		___isDebugEnabled_20 = value;
	}

	inline static int32_t get_offset_of_isInfoEnabled_21() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022, ___isInfoEnabled_21)); }
	inline Nullable_1_t2088641033  get_isInfoEnabled_21() const { return ___isInfoEnabled_21; }
	inline Nullable_1_t2088641033 * get_address_of_isInfoEnabled_21() { return &___isInfoEnabled_21; }
	inline void set_isInfoEnabled_21(Nullable_1_t2088641033  value)
	{
		___isInfoEnabled_21 = value;
	}
};

struct InternalLog4netLogger_t1777422022_StaticFields
{
public:
	// Amazon.Runtime.Internal.Util.InternalLog4netLogger/LoadState Amazon.Runtime.Internal.Util.InternalLog4netLogger::loadState
	int32_t ___loadState_2;
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::LOCK
	RuntimeObject * ___LOCK_3;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::logMangerType
	Type_t * ___logMangerType_4;
	// Amazon.Util.Internal.ITypeInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::logMangerTypeInfo
	RuntimeObject* ___logMangerTypeInfo_5;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::getLoggerWithTypeMethod
	MethodInfo_t * ___getLoggerWithTypeMethod_6;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::logType
	Type_t * ___logType_7;
	// Amazon.Util.Internal.ITypeInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::logTypeInfo
	RuntimeObject* ___logTypeInfo_8;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::logMethod
	MethodInfo_t * ___logMethod_9;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::levelType
	Type_t * ___levelType_10;
	// Amazon.Util.Internal.ITypeInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::levelTypeInfo
	RuntimeObject* ___levelTypeInfo_11;
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::debugLevelPropertyValue
	RuntimeObject * ___debugLevelPropertyValue_12;
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::infoLevelPropertyValue
	RuntimeObject * ___infoLevelPropertyValue_13;
	// System.Object Amazon.Runtime.Internal.Util.InternalLog4netLogger::errorLevelPropertyValue
	RuntimeObject * ___errorLevelPropertyValue_14;
	// System.Reflection.MethodInfo Amazon.Runtime.Internal.Util.InternalLog4netLogger::isEnabledForMethod
	MethodInfo_t * ___isEnabledForMethod_15;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::systemStringFormatType
	Type_t * ___systemStringFormatType_16;
	// System.Type Amazon.Runtime.Internal.Util.InternalLog4netLogger::loggerType
	Type_t * ___loggerType_17;

public:
	inline static int32_t get_offset_of_loadState_2() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___loadState_2)); }
	inline int32_t get_loadState_2() const { return ___loadState_2; }
	inline int32_t* get_address_of_loadState_2() { return &___loadState_2; }
	inline void set_loadState_2(int32_t value)
	{
		___loadState_2 = value;
	}

	inline static int32_t get_offset_of_LOCK_3() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___LOCK_3)); }
	inline RuntimeObject * get_LOCK_3() const { return ___LOCK_3; }
	inline RuntimeObject ** get_address_of_LOCK_3() { return &___LOCK_3; }
	inline void set_LOCK_3(RuntimeObject * value)
	{
		___LOCK_3 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_3), value);
	}

	inline static int32_t get_offset_of_logMangerType_4() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logMangerType_4)); }
	inline Type_t * get_logMangerType_4() const { return ___logMangerType_4; }
	inline Type_t ** get_address_of_logMangerType_4() { return &___logMangerType_4; }
	inline void set_logMangerType_4(Type_t * value)
	{
		___logMangerType_4 = value;
		Il2CppCodeGenWriteBarrier((&___logMangerType_4), value);
	}

	inline static int32_t get_offset_of_logMangerTypeInfo_5() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logMangerTypeInfo_5)); }
	inline RuntimeObject* get_logMangerTypeInfo_5() const { return ___logMangerTypeInfo_5; }
	inline RuntimeObject** get_address_of_logMangerTypeInfo_5() { return &___logMangerTypeInfo_5; }
	inline void set_logMangerTypeInfo_5(RuntimeObject* value)
	{
		___logMangerTypeInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___logMangerTypeInfo_5), value);
	}

	inline static int32_t get_offset_of_getLoggerWithTypeMethod_6() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___getLoggerWithTypeMethod_6)); }
	inline MethodInfo_t * get_getLoggerWithTypeMethod_6() const { return ___getLoggerWithTypeMethod_6; }
	inline MethodInfo_t ** get_address_of_getLoggerWithTypeMethod_6() { return &___getLoggerWithTypeMethod_6; }
	inline void set_getLoggerWithTypeMethod_6(MethodInfo_t * value)
	{
		___getLoggerWithTypeMethod_6 = value;
		Il2CppCodeGenWriteBarrier((&___getLoggerWithTypeMethod_6), value);
	}

	inline static int32_t get_offset_of_logType_7() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logType_7)); }
	inline Type_t * get_logType_7() const { return ___logType_7; }
	inline Type_t ** get_address_of_logType_7() { return &___logType_7; }
	inline void set_logType_7(Type_t * value)
	{
		___logType_7 = value;
		Il2CppCodeGenWriteBarrier((&___logType_7), value);
	}

	inline static int32_t get_offset_of_logTypeInfo_8() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logTypeInfo_8)); }
	inline RuntimeObject* get_logTypeInfo_8() const { return ___logTypeInfo_8; }
	inline RuntimeObject** get_address_of_logTypeInfo_8() { return &___logTypeInfo_8; }
	inline void set_logTypeInfo_8(RuntimeObject* value)
	{
		___logTypeInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___logTypeInfo_8), value);
	}

	inline static int32_t get_offset_of_logMethod_9() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___logMethod_9)); }
	inline MethodInfo_t * get_logMethod_9() const { return ___logMethod_9; }
	inline MethodInfo_t ** get_address_of_logMethod_9() { return &___logMethod_9; }
	inline void set_logMethod_9(MethodInfo_t * value)
	{
		___logMethod_9 = value;
		Il2CppCodeGenWriteBarrier((&___logMethod_9), value);
	}

	inline static int32_t get_offset_of_levelType_10() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___levelType_10)); }
	inline Type_t * get_levelType_10() const { return ___levelType_10; }
	inline Type_t ** get_address_of_levelType_10() { return &___levelType_10; }
	inline void set_levelType_10(Type_t * value)
	{
		___levelType_10 = value;
		Il2CppCodeGenWriteBarrier((&___levelType_10), value);
	}

	inline static int32_t get_offset_of_levelTypeInfo_11() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___levelTypeInfo_11)); }
	inline RuntimeObject* get_levelTypeInfo_11() const { return ___levelTypeInfo_11; }
	inline RuntimeObject** get_address_of_levelTypeInfo_11() { return &___levelTypeInfo_11; }
	inline void set_levelTypeInfo_11(RuntimeObject* value)
	{
		___levelTypeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___levelTypeInfo_11), value);
	}

	inline static int32_t get_offset_of_debugLevelPropertyValue_12() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___debugLevelPropertyValue_12)); }
	inline RuntimeObject * get_debugLevelPropertyValue_12() const { return ___debugLevelPropertyValue_12; }
	inline RuntimeObject ** get_address_of_debugLevelPropertyValue_12() { return &___debugLevelPropertyValue_12; }
	inline void set_debugLevelPropertyValue_12(RuntimeObject * value)
	{
		___debugLevelPropertyValue_12 = value;
		Il2CppCodeGenWriteBarrier((&___debugLevelPropertyValue_12), value);
	}

	inline static int32_t get_offset_of_infoLevelPropertyValue_13() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___infoLevelPropertyValue_13)); }
	inline RuntimeObject * get_infoLevelPropertyValue_13() const { return ___infoLevelPropertyValue_13; }
	inline RuntimeObject ** get_address_of_infoLevelPropertyValue_13() { return &___infoLevelPropertyValue_13; }
	inline void set_infoLevelPropertyValue_13(RuntimeObject * value)
	{
		___infoLevelPropertyValue_13 = value;
		Il2CppCodeGenWriteBarrier((&___infoLevelPropertyValue_13), value);
	}

	inline static int32_t get_offset_of_errorLevelPropertyValue_14() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___errorLevelPropertyValue_14)); }
	inline RuntimeObject * get_errorLevelPropertyValue_14() const { return ___errorLevelPropertyValue_14; }
	inline RuntimeObject ** get_address_of_errorLevelPropertyValue_14() { return &___errorLevelPropertyValue_14; }
	inline void set_errorLevelPropertyValue_14(RuntimeObject * value)
	{
		___errorLevelPropertyValue_14 = value;
		Il2CppCodeGenWriteBarrier((&___errorLevelPropertyValue_14), value);
	}

	inline static int32_t get_offset_of_isEnabledForMethod_15() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___isEnabledForMethod_15)); }
	inline MethodInfo_t * get_isEnabledForMethod_15() const { return ___isEnabledForMethod_15; }
	inline MethodInfo_t ** get_address_of_isEnabledForMethod_15() { return &___isEnabledForMethod_15; }
	inline void set_isEnabledForMethod_15(MethodInfo_t * value)
	{
		___isEnabledForMethod_15 = value;
		Il2CppCodeGenWriteBarrier((&___isEnabledForMethod_15), value);
	}

	inline static int32_t get_offset_of_systemStringFormatType_16() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___systemStringFormatType_16)); }
	inline Type_t * get_systemStringFormatType_16() const { return ___systemStringFormatType_16; }
	inline Type_t ** get_address_of_systemStringFormatType_16() { return &___systemStringFormatType_16; }
	inline void set_systemStringFormatType_16(Type_t * value)
	{
		___systemStringFormatType_16 = value;
		Il2CppCodeGenWriteBarrier((&___systemStringFormatType_16), value);
	}

	inline static int32_t get_offset_of_loggerType_17() { return static_cast<int32_t>(offsetof(InternalLog4netLogger_t1777422022_StaticFields, ___loggerType_17)); }
	inline Type_t * get_loggerType_17() const { return ___loggerType_17; }
	inline Type_t ** get_address_of_loggerType_17() { return &___loggerType_17; }
	inline void set_loggerType_17(Type_t * value)
	{
		___loggerType_17 = value;
		Il2CppCodeGenWriteBarrier((&___loggerType_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALLOG4NETLOGGER_T1777422022_H
#ifndef TIMINGEVENT_T181853090_H
#define TIMINGEVENT_T181853090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.TimingEvent
struct  TimingEvent_t181853090  : public RuntimeObject
{
public:
	// Amazon.Runtime.Metric Amazon.Runtime.Internal.Util.TimingEvent::metric
	int32_t ___metric_0;
	// Amazon.Runtime.Internal.Util.RequestMetrics Amazon.Runtime.Internal.Util.TimingEvent::metrics
	RequestMetrics_t218029284 * ___metrics_1;
	// System.Boolean Amazon.Runtime.Internal.Util.TimingEvent::disposed
	bool ___disposed_2;

public:
	inline static int32_t get_offset_of_metric_0() { return static_cast<int32_t>(offsetof(TimingEvent_t181853090, ___metric_0)); }
	inline int32_t get_metric_0() const { return ___metric_0; }
	inline int32_t* get_address_of_metric_0() { return &___metric_0; }
	inline void set_metric_0(int32_t value)
	{
		___metric_0 = value;
	}

	inline static int32_t get_offset_of_metrics_1() { return static_cast<int32_t>(offsetof(TimingEvent_t181853090, ___metrics_1)); }
	inline RequestMetrics_t218029284 * get_metrics_1() const { return ___metrics_1; }
	inline RequestMetrics_t218029284 ** get_address_of_metrics_1() { return &___metrics_1; }
	inline void set_metrics_1(RequestMetrics_t218029284 * value)
	{
		___metrics_1 = value;
		Il2CppCodeGenWriteBarrier((&___metrics_1), value);
	}

	inline static int32_t get_offset_of_disposed_2() { return static_cast<int32_t>(offsetof(TimingEvent_t181853090, ___disposed_2)); }
	inline bool get_disposed_2() const { return ___disposed_2; }
	inline bool* get_address_of_disposed_2() { return &___disposed_2; }
	inline void set_disposed_2(bool value)
	{
		___disposed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMINGEVENT_T181853090_H
#ifndef DEFAULTRETRYPOLICY_T2207644155_H
#define DEFAULTRETRYPOLICY_T2207644155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.DefaultRetryPolicy
struct  DefaultRetryPolicy_t2207644155  : public RetryPolicy_t1476739446
{
public:
	// System.Int32 Amazon.Runtime.Internal.DefaultRetryPolicy::_maxBackoffInMilliseconds
	int32_t ____maxBackoffInMilliseconds_5;
	// Amazon.Runtime.Internal.RetryCapacity Amazon.Runtime.Internal.DefaultRetryPolicy::_retryCapacity
	RetryCapacity_t2794668894 * ____retryCapacity_6;
	// System.Collections.Generic.ICollection`1<System.Net.HttpStatusCode> Amazon.Runtime.Internal.DefaultRetryPolicy::_httpStatusCodesToRetryOn
	RuntimeObject* ____httpStatusCodesToRetryOn_7;
	// System.Collections.Generic.ICollection`1<System.Net.WebExceptionStatus> Amazon.Runtime.Internal.DefaultRetryPolicy::_webExceptionStatusesToRetryOn
	RuntimeObject* ____webExceptionStatusesToRetryOn_8;
	// System.Collections.Generic.ICollection`1<System.String> Amazon.Runtime.Internal.DefaultRetryPolicy::_errorCodesToRetryOn
	RuntimeObject* ____errorCodesToRetryOn_10;

public:
	inline static int32_t get_offset_of__maxBackoffInMilliseconds_5() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____maxBackoffInMilliseconds_5)); }
	inline int32_t get__maxBackoffInMilliseconds_5() const { return ____maxBackoffInMilliseconds_5; }
	inline int32_t* get_address_of__maxBackoffInMilliseconds_5() { return &____maxBackoffInMilliseconds_5; }
	inline void set__maxBackoffInMilliseconds_5(int32_t value)
	{
		____maxBackoffInMilliseconds_5 = value;
	}

	inline static int32_t get_offset_of__retryCapacity_6() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____retryCapacity_6)); }
	inline RetryCapacity_t2794668894 * get__retryCapacity_6() const { return ____retryCapacity_6; }
	inline RetryCapacity_t2794668894 ** get_address_of__retryCapacity_6() { return &____retryCapacity_6; }
	inline void set__retryCapacity_6(RetryCapacity_t2794668894 * value)
	{
		____retryCapacity_6 = value;
		Il2CppCodeGenWriteBarrier((&____retryCapacity_6), value);
	}

	inline static int32_t get_offset_of__httpStatusCodesToRetryOn_7() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____httpStatusCodesToRetryOn_7)); }
	inline RuntimeObject* get__httpStatusCodesToRetryOn_7() const { return ____httpStatusCodesToRetryOn_7; }
	inline RuntimeObject** get_address_of__httpStatusCodesToRetryOn_7() { return &____httpStatusCodesToRetryOn_7; }
	inline void set__httpStatusCodesToRetryOn_7(RuntimeObject* value)
	{
		____httpStatusCodesToRetryOn_7 = value;
		Il2CppCodeGenWriteBarrier((&____httpStatusCodesToRetryOn_7), value);
	}

	inline static int32_t get_offset_of__webExceptionStatusesToRetryOn_8() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____webExceptionStatusesToRetryOn_8)); }
	inline RuntimeObject* get__webExceptionStatusesToRetryOn_8() const { return ____webExceptionStatusesToRetryOn_8; }
	inline RuntimeObject** get_address_of__webExceptionStatusesToRetryOn_8() { return &____webExceptionStatusesToRetryOn_8; }
	inline void set__webExceptionStatusesToRetryOn_8(RuntimeObject* value)
	{
		____webExceptionStatusesToRetryOn_8 = value;
		Il2CppCodeGenWriteBarrier((&____webExceptionStatusesToRetryOn_8), value);
	}

	inline static int32_t get_offset_of__errorCodesToRetryOn_10() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155, ____errorCodesToRetryOn_10)); }
	inline RuntimeObject* get__errorCodesToRetryOn_10() const { return ____errorCodesToRetryOn_10; }
	inline RuntimeObject** get_address_of__errorCodesToRetryOn_10() { return &____errorCodesToRetryOn_10; }
	inline void set__errorCodesToRetryOn_10(RuntimeObject* value)
	{
		____errorCodesToRetryOn_10 = value;
		Il2CppCodeGenWriteBarrier((&____errorCodesToRetryOn_10), value);
	}
};

struct DefaultRetryPolicy_t2207644155_StaticFields
{
public:
	// Amazon.Runtime.Internal.CapacityManager Amazon.Runtime.Internal.DefaultRetryPolicy::_capacityManagerInstance
	CapacityManager_t2181902141 * ____capacityManagerInstance_4;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.Internal.DefaultRetryPolicy::_coreCLRRetryErrorMessages
	HashSet_1_t362681087 * ____coreCLRRetryErrorMessages_9;

public:
	inline static int32_t get_offset_of__capacityManagerInstance_4() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155_StaticFields, ____capacityManagerInstance_4)); }
	inline CapacityManager_t2181902141 * get__capacityManagerInstance_4() const { return ____capacityManagerInstance_4; }
	inline CapacityManager_t2181902141 ** get_address_of__capacityManagerInstance_4() { return &____capacityManagerInstance_4; }
	inline void set__capacityManagerInstance_4(CapacityManager_t2181902141 * value)
	{
		____capacityManagerInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&____capacityManagerInstance_4), value);
	}

	inline static int32_t get_offset_of__coreCLRRetryErrorMessages_9() { return static_cast<int32_t>(offsetof(DefaultRetryPolicy_t2207644155_StaticFields, ____coreCLRRetryErrorMessages_9)); }
	inline HashSet_1_t362681087 * get__coreCLRRetryErrorMessages_9() const { return ____coreCLRRetryErrorMessages_9; }
	inline HashSet_1_t362681087 ** get_address_of__coreCLRRetryErrorMessages_9() { return &____coreCLRRetryErrorMessages_9; }
	inline void set__coreCLRRetryErrorMessages_9(HashSet_1_t362681087 * value)
	{
		____coreCLRRetryErrorMessages_9 = value;
		Il2CppCodeGenWriteBarrier((&____coreCLRRetryErrorMessages_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTRETRYPOLICY_T2207644155_H
#ifndef XMLUNMARSHALLERCONTEXT_T1179575220_H
#define XMLUNMARSHALLERCONTEXT_T1179575220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext
struct  XmlUnmarshallerContext_t1179575220  : public UnmarshallerContext_t1608322995
{
public:
	// System.IO.StreamReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::streamReader
	StreamReader_t2360341767 * ___streamReader_8;
	// System.Xml.XmlReader Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::_xmlReader
	XmlReader_t3675626668 * ____xmlReader_9;
	// System.Collections.Generic.Stack`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::stack
	Stack_1_t3116948387 * ___stack_10;
	// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::stackString
	String_t* ___stackString_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeValues
	Dictionary_2_t3943999495 * ___attributeValues_12;
	// System.Collections.Generic.List`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeNames
	List_1_t1398341365 * ___attributeNames_13;
	// System.Collections.Generic.IEnumerator`1<System.String> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::attributeEnumerator
	RuntimeObject* ___attributeEnumerator_14;
	// System.Xml.XmlNodeType Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodeType
	int32_t ___nodeType_15;
	// System.String Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodeContent
	String_t* ___nodeContent_16;
	// System.Boolean Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::disposed
	bool ___disposed_17;

public:
	inline static int32_t get_offset_of_streamReader_8() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___streamReader_8)); }
	inline StreamReader_t2360341767 * get_streamReader_8() const { return ___streamReader_8; }
	inline StreamReader_t2360341767 ** get_address_of_streamReader_8() { return &___streamReader_8; }
	inline void set_streamReader_8(StreamReader_t2360341767 * value)
	{
		___streamReader_8 = value;
		Il2CppCodeGenWriteBarrier((&___streamReader_8), value);
	}

	inline static int32_t get_offset_of__xmlReader_9() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ____xmlReader_9)); }
	inline XmlReader_t3675626668 * get__xmlReader_9() const { return ____xmlReader_9; }
	inline XmlReader_t3675626668 ** get_address_of__xmlReader_9() { return &____xmlReader_9; }
	inline void set__xmlReader_9(XmlReader_t3675626668 * value)
	{
		____xmlReader_9 = value;
		Il2CppCodeGenWriteBarrier((&____xmlReader_9), value);
	}

	inline static int32_t get_offset_of_stack_10() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___stack_10)); }
	inline Stack_1_t3116948387 * get_stack_10() const { return ___stack_10; }
	inline Stack_1_t3116948387 ** get_address_of_stack_10() { return &___stack_10; }
	inline void set_stack_10(Stack_1_t3116948387 * value)
	{
		___stack_10 = value;
		Il2CppCodeGenWriteBarrier((&___stack_10), value);
	}

	inline static int32_t get_offset_of_stackString_11() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___stackString_11)); }
	inline String_t* get_stackString_11() const { return ___stackString_11; }
	inline String_t** get_address_of_stackString_11() { return &___stackString_11; }
	inline void set_stackString_11(String_t* value)
	{
		___stackString_11 = value;
		Il2CppCodeGenWriteBarrier((&___stackString_11), value);
	}

	inline static int32_t get_offset_of_attributeValues_12() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___attributeValues_12)); }
	inline Dictionary_2_t3943999495 * get_attributeValues_12() const { return ___attributeValues_12; }
	inline Dictionary_2_t3943999495 ** get_address_of_attributeValues_12() { return &___attributeValues_12; }
	inline void set_attributeValues_12(Dictionary_2_t3943999495 * value)
	{
		___attributeValues_12 = value;
		Il2CppCodeGenWriteBarrier((&___attributeValues_12), value);
	}

	inline static int32_t get_offset_of_attributeNames_13() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___attributeNames_13)); }
	inline List_1_t1398341365 * get_attributeNames_13() const { return ___attributeNames_13; }
	inline List_1_t1398341365 ** get_address_of_attributeNames_13() { return &___attributeNames_13; }
	inline void set_attributeNames_13(List_1_t1398341365 * value)
	{
		___attributeNames_13 = value;
		Il2CppCodeGenWriteBarrier((&___attributeNames_13), value);
	}

	inline static int32_t get_offset_of_attributeEnumerator_14() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___attributeEnumerator_14)); }
	inline RuntimeObject* get_attributeEnumerator_14() const { return ___attributeEnumerator_14; }
	inline RuntimeObject** get_address_of_attributeEnumerator_14() { return &___attributeEnumerator_14; }
	inline void set_attributeEnumerator_14(RuntimeObject* value)
	{
		___attributeEnumerator_14 = value;
		Il2CppCodeGenWriteBarrier((&___attributeEnumerator_14), value);
	}

	inline static int32_t get_offset_of_nodeType_15() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___nodeType_15)); }
	inline int32_t get_nodeType_15() const { return ___nodeType_15; }
	inline int32_t* get_address_of_nodeType_15() { return &___nodeType_15; }
	inline void set_nodeType_15(int32_t value)
	{
		___nodeType_15 = value;
	}

	inline static int32_t get_offset_of_nodeContent_16() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___nodeContent_16)); }
	inline String_t* get_nodeContent_16() const { return ___nodeContent_16; }
	inline String_t** get_address_of_nodeContent_16() { return &___nodeContent_16; }
	inline void set_nodeContent_16(String_t* value)
	{
		___nodeContent_16 = value;
		Il2CppCodeGenWriteBarrier((&___nodeContent_16), value);
	}

	inline static int32_t get_offset_of_disposed_17() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220, ___disposed_17)); }
	inline bool get_disposed_17() const { return ___disposed_17; }
	inline bool* get_address_of_disposed_17() { return &___disposed_17; }
	inline void set_disposed_17(bool value)
	{
		___disposed_17 = value;
	}
};

struct XmlUnmarshallerContext_t1179575220_StaticFields
{
public:
	// System.Xml.XmlReaderSettings Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::READER_SETTINGS
	XmlReaderSettings_t1578612233 * ___READER_SETTINGS_6;
	// System.Collections.Generic.HashSet`1<System.Xml.XmlNodeType> Amazon.Runtime.Internal.Transform.XmlUnmarshallerContext::nodesToSkip
	HashSet_1_t3367932747 * ___nodesToSkip_7;

public:
	inline static int32_t get_offset_of_READER_SETTINGS_6() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220_StaticFields, ___READER_SETTINGS_6)); }
	inline XmlReaderSettings_t1578612233 * get_READER_SETTINGS_6() const { return ___READER_SETTINGS_6; }
	inline XmlReaderSettings_t1578612233 ** get_address_of_READER_SETTINGS_6() { return &___READER_SETTINGS_6; }
	inline void set_READER_SETTINGS_6(XmlReaderSettings_t1578612233 * value)
	{
		___READER_SETTINGS_6 = value;
		Il2CppCodeGenWriteBarrier((&___READER_SETTINGS_6), value);
	}

	inline static int32_t get_offset_of_nodesToSkip_7() { return static_cast<int32_t>(offsetof(XmlUnmarshallerContext_t1179575220_StaticFields, ___nodesToSkip_7)); }
	inline HashSet_1_t3367932747 * get_nodesToSkip_7() const { return ___nodesToSkip_7; }
	inline HashSet_1_t3367932747 ** get_address_of_nodesToSkip_7() { return &___nodesToSkip_7; }
	inline void set_nodesToSkip_7(HashSet_1_t3367932747 * value)
	{
		___nodesToSkip_7 = value;
		Il2CppCodeGenWriteBarrier((&___nodesToSkip_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUNMARSHALLERCONTEXT_T1179575220_H
#ifndef HASHSTREAM_1_T2762322779_H
#define HASHSTREAM_1_T2762322779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.HashStream`1<Amazon.Runtime.Internal.Util.HashingWrapperMD5>
struct  HashStream_1_t2762322779  : public HashStream_t2318308192
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHSTREAM_1_T2762322779_H
#ifndef UNITYWEBRESPONSEDATA_T3141113362_H
#define UNITYWEBRESPONSEDATA_T3141113362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.UnityWebResponseData
struct  UnityWebResponseData_t3141113362  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Amazon.Runtime.Internal.Transform.UnityWebResponseData::_headers
	Dictionary_2_t3943999495 * ____headers_0;
	// System.String[] Amazon.Runtime.Internal.Transform.UnityWebResponseData::_headerNames
	StringU5BU5D_t1642385972* ____headerNames_1;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Runtime.Internal.Transform.UnityWebResponseData::_headerNamesSet
	HashSet_1_t362681087 * ____headerNamesSet_2;
	// System.IO.Stream Amazon.Runtime.Internal.Transform.UnityWebResponseData::_responseStream
	Stream_t3255436806 * ____responseStream_3;
	// System.Byte[] Amazon.Runtime.Internal.Transform.UnityWebResponseData::_responseBody
	ByteU5BU5D_t3397334013* ____responseBody_4;
	// Amazon.Runtime.Internal.Util.ILogger Amazon.Runtime.Internal.Transform.UnityWebResponseData::_logger
	RuntimeObject* ____logger_5;
	// System.Int64 Amazon.Runtime.Internal.Transform.UnityWebResponseData::<ContentLength>k__BackingField
	int64_t ___U3CContentLengthU3Ek__BackingField_6;
	// System.String Amazon.Runtime.Internal.Transform.UnityWebResponseData::<ContentType>k__BackingField
	String_t* ___U3CContentTypeU3Ek__BackingField_7;
	// System.Net.HttpStatusCode Amazon.Runtime.Internal.Transform.UnityWebResponseData::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_8;
	// System.Boolean Amazon.Runtime.Internal.Transform.UnityWebResponseData::<IsSuccessStatusCode>k__BackingField
	bool ___U3CIsSuccessStatusCodeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of__headers_0() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____headers_0)); }
	inline Dictionary_2_t3943999495 * get__headers_0() const { return ____headers_0; }
	inline Dictionary_2_t3943999495 ** get_address_of__headers_0() { return &____headers_0; }
	inline void set__headers_0(Dictionary_2_t3943999495 * value)
	{
		____headers_0 = value;
		Il2CppCodeGenWriteBarrier((&____headers_0), value);
	}

	inline static int32_t get_offset_of__headerNames_1() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____headerNames_1)); }
	inline StringU5BU5D_t1642385972* get__headerNames_1() const { return ____headerNames_1; }
	inline StringU5BU5D_t1642385972** get_address_of__headerNames_1() { return &____headerNames_1; }
	inline void set__headerNames_1(StringU5BU5D_t1642385972* value)
	{
		____headerNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____headerNames_1), value);
	}

	inline static int32_t get_offset_of__headerNamesSet_2() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____headerNamesSet_2)); }
	inline HashSet_1_t362681087 * get__headerNamesSet_2() const { return ____headerNamesSet_2; }
	inline HashSet_1_t362681087 ** get_address_of__headerNamesSet_2() { return &____headerNamesSet_2; }
	inline void set__headerNamesSet_2(HashSet_1_t362681087 * value)
	{
		____headerNamesSet_2 = value;
		Il2CppCodeGenWriteBarrier((&____headerNamesSet_2), value);
	}

	inline static int32_t get_offset_of__responseStream_3() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____responseStream_3)); }
	inline Stream_t3255436806 * get__responseStream_3() const { return ____responseStream_3; }
	inline Stream_t3255436806 ** get_address_of__responseStream_3() { return &____responseStream_3; }
	inline void set__responseStream_3(Stream_t3255436806 * value)
	{
		____responseStream_3 = value;
		Il2CppCodeGenWriteBarrier((&____responseStream_3), value);
	}

	inline static int32_t get_offset_of__responseBody_4() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____responseBody_4)); }
	inline ByteU5BU5D_t3397334013* get__responseBody_4() const { return ____responseBody_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__responseBody_4() { return &____responseBody_4; }
	inline void set__responseBody_4(ByteU5BU5D_t3397334013* value)
	{
		____responseBody_4 = value;
		Il2CppCodeGenWriteBarrier((&____responseBody_4), value);
	}

	inline static int32_t get_offset_of__logger_5() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ____logger_5)); }
	inline RuntimeObject* get__logger_5() const { return ____logger_5; }
	inline RuntimeObject** get_address_of__logger_5() { return &____logger_5; }
	inline void set__logger_5(RuntimeObject* value)
	{
		____logger_5 = value;
		Il2CppCodeGenWriteBarrier((&____logger_5), value);
	}

	inline static int32_t get_offset_of_U3CContentLengthU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ___U3CContentLengthU3Ek__BackingField_6)); }
	inline int64_t get_U3CContentLengthU3Ek__BackingField_6() const { return ___U3CContentLengthU3Ek__BackingField_6; }
	inline int64_t* get_address_of_U3CContentLengthU3Ek__BackingField_6() { return &___U3CContentLengthU3Ek__BackingField_6; }
	inline void set_U3CContentLengthU3Ek__BackingField_6(int64_t value)
	{
		___U3CContentLengthU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CContentTypeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ___U3CContentTypeU3Ek__BackingField_7)); }
	inline String_t* get_U3CContentTypeU3Ek__BackingField_7() const { return ___U3CContentTypeU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CContentTypeU3Ek__BackingField_7() { return &___U3CContentTypeU3Ek__BackingField_7; }
	inline void set_U3CContentTypeU3Ek__BackingField_7(String_t* value)
	{
		___U3CContentTypeU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentTypeU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ___U3CStatusCodeU3Ek__BackingField_8)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_8() const { return ___U3CStatusCodeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_8() { return &___U3CStatusCodeU3Ek__BackingField_8; }
	inline void set_U3CStatusCodeU3Ek__BackingField_8(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CIsSuccessStatusCodeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(UnityWebResponseData_t3141113362, ___U3CIsSuccessStatusCodeU3Ek__BackingField_9)); }
	inline bool get_U3CIsSuccessStatusCodeU3Ek__BackingField_9() const { return ___U3CIsSuccessStatusCodeU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsSuccessStatusCodeU3Ek__BackingField_9() { return &___U3CIsSuccessStatusCodeU3Ek__BackingField_9; }
	inline void set_U3CIsSuccessStatusCodeU3Ek__BackingField_9(bool value)
	{
		___U3CIsSuccessStatusCodeU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYWEBRESPONSEDATA_T3141113362_H
#ifndef CHUNKEDUPLOADWRAPPERSTREAM_T779979984_H
#define CHUNKEDUPLOADWRAPPERSTREAM_T779979984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream
struct  ChunkedUploadWrapperStream_t779979984  : public WrapperStream_t816765491
{
public:
	// System.Byte[] Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_inputBuffer
	ByteU5BU5D_t3397334013* ____inputBuffer_3;
	// System.Byte[] Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_outputBuffer
	ByteU5BU5D_t3397334013* ____outputBuffer_4;
	// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_outputBufferPos
	int32_t ____outputBufferPos_5;
	// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_outputBufferDataLen
	int32_t ____outputBufferDataLen_6;
	// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_wrappedStreamBufferSize
	int32_t ____wrappedStreamBufferSize_7;
	// System.Boolean Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_wrappedStreamConsumed
	bool ____wrappedStreamConsumed_8;
	// System.Boolean Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_outputBufferIsTerminatingChunk
	bool ____outputBufferIsTerminatingChunk_9;
	// Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream/ReadStrategy Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::_readStrategy
	int32_t ____readStrategy_10;
	// Amazon.Runtime.Internal.Auth.AWS4SigningResult Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::<HeaderSigningResult>k__BackingField
	AWS4SigningResult_t430803065 * ___U3CHeaderSigningResultU3Ek__BackingField_11;
	// System.String Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::<PreviousChunkSignature>k__BackingField
	String_t* ___U3CPreviousChunkSignatureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of__inputBuffer_3() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____inputBuffer_3)); }
	inline ByteU5BU5D_t3397334013* get__inputBuffer_3() const { return ____inputBuffer_3; }
	inline ByteU5BU5D_t3397334013** get_address_of__inputBuffer_3() { return &____inputBuffer_3; }
	inline void set__inputBuffer_3(ByteU5BU5D_t3397334013* value)
	{
		____inputBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&____inputBuffer_3), value);
	}

	inline static int32_t get_offset_of__outputBuffer_4() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____outputBuffer_4)); }
	inline ByteU5BU5D_t3397334013* get__outputBuffer_4() const { return ____outputBuffer_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__outputBuffer_4() { return &____outputBuffer_4; }
	inline void set__outputBuffer_4(ByteU5BU5D_t3397334013* value)
	{
		____outputBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&____outputBuffer_4), value);
	}

	inline static int32_t get_offset_of__outputBufferPos_5() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____outputBufferPos_5)); }
	inline int32_t get__outputBufferPos_5() const { return ____outputBufferPos_5; }
	inline int32_t* get_address_of__outputBufferPos_5() { return &____outputBufferPos_5; }
	inline void set__outputBufferPos_5(int32_t value)
	{
		____outputBufferPos_5 = value;
	}

	inline static int32_t get_offset_of__outputBufferDataLen_6() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____outputBufferDataLen_6)); }
	inline int32_t get__outputBufferDataLen_6() const { return ____outputBufferDataLen_6; }
	inline int32_t* get_address_of__outputBufferDataLen_6() { return &____outputBufferDataLen_6; }
	inline void set__outputBufferDataLen_6(int32_t value)
	{
		____outputBufferDataLen_6 = value;
	}

	inline static int32_t get_offset_of__wrappedStreamBufferSize_7() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____wrappedStreamBufferSize_7)); }
	inline int32_t get__wrappedStreamBufferSize_7() const { return ____wrappedStreamBufferSize_7; }
	inline int32_t* get_address_of__wrappedStreamBufferSize_7() { return &____wrappedStreamBufferSize_7; }
	inline void set__wrappedStreamBufferSize_7(int32_t value)
	{
		____wrappedStreamBufferSize_7 = value;
	}

	inline static int32_t get_offset_of__wrappedStreamConsumed_8() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____wrappedStreamConsumed_8)); }
	inline bool get__wrappedStreamConsumed_8() const { return ____wrappedStreamConsumed_8; }
	inline bool* get_address_of__wrappedStreamConsumed_8() { return &____wrappedStreamConsumed_8; }
	inline void set__wrappedStreamConsumed_8(bool value)
	{
		____wrappedStreamConsumed_8 = value;
	}

	inline static int32_t get_offset_of__outputBufferIsTerminatingChunk_9() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____outputBufferIsTerminatingChunk_9)); }
	inline bool get__outputBufferIsTerminatingChunk_9() const { return ____outputBufferIsTerminatingChunk_9; }
	inline bool* get_address_of__outputBufferIsTerminatingChunk_9() { return &____outputBufferIsTerminatingChunk_9; }
	inline void set__outputBufferIsTerminatingChunk_9(bool value)
	{
		____outputBufferIsTerminatingChunk_9 = value;
	}

	inline static int32_t get_offset_of__readStrategy_10() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ____readStrategy_10)); }
	inline int32_t get__readStrategy_10() const { return ____readStrategy_10; }
	inline int32_t* get_address_of__readStrategy_10() { return &____readStrategy_10; }
	inline void set__readStrategy_10(int32_t value)
	{
		____readStrategy_10 = value;
	}

	inline static int32_t get_offset_of_U3CHeaderSigningResultU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ___U3CHeaderSigningResultU3Ek__BackingField_11)); }
	inline AWS4SigningResult_t430803065 * get_U3CHeaderSigningResultU3Ek__BackingField_11() const { return ___U3CHeaderSigningResultU3Ek__BackingField_11; }
	inline AWS4SigningResult_t430803065 ** get_address_of_U3CHeaderSigningResultU3Ek__BackingField_11() { return &___U3CHeaderSigningResultU3Ek__BackingField_11; }
	inline void set_U3CHeaderSigningResultU3Ek__BackingField_11(AWS4SigningResult_t430803065 * value)
	{
		___U3CHeaderSigningResultU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHeaderSigningResultU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CPreviousChunkSignatureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984, ___U3CPreviousChunkSignatureU3Ek__BackingField_12)); }
	inline String_t* get_U3CPreviousChunkSignatureU3Ek__BackingField_12() const { return ___U3CPreviousChunkSignatureU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CPreviousChunkSignatureU3Ek__BackingField_12() { return &___U3CPreviousChunkSignatureU3Ek__BackingField_12; }
	inline void set_U3CPreviousChunkSignatureU3Ek__BackingField_12(String_t* value)
	{
		___U3CPreviousChunkSignatureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPreviousChunkSignatureU3Ek__BackingField_12), value);
	}
};

struct ChunkedUploadWrapperStream_t779979984_StaticFields
{
public:
	// System.Int32 Amazon.Runtime.Internal.Util.ChunkedUploadWrapperStream::DefaultChunkSize
	int32_t ___DefaultChunkSize_2;

public:
	inline static int32_t get_offset_of_DefaultChunkSize_2() { return static_cast<int32_t>(offsetof(ChunkedUploadWrapperStream_t779979984_StaticFields, ___DefaultChunkSize_2)); }
	inline int32_t get_DefaultChunkSize_2() const { return ___DefaultChunkSize_2; }
	inline int32_t* get_address_of_DefaultChunkSize_2() { return &___DefaultChunkSize_2; }
	inline void set_DefaultChunkSize_2(int32_t value)
	{
		___DefaultChunkSize_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNKEDUPLOADWRAPPERSTREAM_T779979984_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef METRICERROR_T964444806_H
#define METRICERROR_T964444806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.MetricError
struct  MetricError_t964444806  : public RuntimeObject
{
public:
	// Amazon.Runtime.Metric Amazon.Runtime.Internal.Util.MetricError::<Metric>k__BackingField
	int32_t ___U3CMetricU3Ek__BackingField_0;
	// System.String Amazon.Runtime.Internal.Util.MetricError::<Message>k__BackingField
	String_t* ___U3CMessageU3Ek__BackingField_1;
	// System.Exception Amazon.Runtime.Internal.Util.MetricError::<Exception>k__BackingField
	Exception_t1927440687 * ___U3CExceptionU3Ek__BackingField_2;
	// System.DateTime Amazon.Runtime.Internal.Util.MetricError::<Time>k__BackingField
	DateTime_t693205669  ___U3CTimeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CMetricU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MetricError_t964444806, ___U3CMetricU3Ek__BackingField_0)); }
	inline int32_t get_U3CMetricU3Ek__BackingField_0() const { return ___U3CMetricU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CMetricU3Ek__BackingField_0() { return &___U3CMetricU3Ek__BackingField_0; }
	inline void set_U3CMetricU3Ek__BackingField_0(int32_t value)
	{
		___U3CMetricU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MetricError_t964444806, ___U3CMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CMessageU3Ek__BackingField_1() const { return ___U3CMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CMessageU3Ek__BackingField_1() { return &___U3CMessageU3Ek__BackingField_1; }
	inline void set_U3CMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMessageU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CExceptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MetricError_t964444806, ___U3CExceptionU3Ek__BackingField_2)); }
	inline Exception_t1927440687 * get_U3CExceptionU3Ek__BackingField_2() const { return ___U3CExceptionU3Ek__BackingField_2; }
	inline Exception_t1927440687 ** get_address_of_U3CExceptionU3Ek__BackingField_2() { return &___U3CExceptionU3Ek__BackingField_2; }
	inline void set_U3CExceptionU3Ek__BackingField_2(Exception_t1927440687 * value)
	{
		___U3CExceptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CTimeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MetricError_t964444806, ___U3CTimeU3Ek__BackingField_3)); }
	inline DateTime_t693205669  get_U3CTimeU3Ek__BackingField_3() const { return ___U3CTimeU3Ek__BackingField_3; }
	inline DateTime_t693205669 * get_address_of_U3CTimeU3Ek__BackingField_3() { return &___U3CTimeU3Ek__BackingField_3; }
	inline void set_U3CTimeU3Ek__BackingField_3(DateTime_t693205669  value)
	{
		___U3CTimeU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METRICERROR_T964444806_H
#ifndef AWS4SIGNINGRESULT_T430803065_H
#define AWS4SIGNINGRESULT_T430803065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Auth.AWS4SigningResult
struct  AWS4SigningResult_t430803065  : public RuntimeObject
{
public:
	// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::_awsAccessKeyId
	String_t* ____awsAccessKeyId_0;
	// System.DateTime Amazon.Runtime.Internal.Auth.AWS4SigningResult::_originalDateTime
	DateTime_t693205669  ____originalDateTime_1;
	// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::_signedHeaders
	String_t* ____signedHeaders_2;
	// System.String Amazon.Runtime.Internal.Auth.AWS4SigningResult::_scope
	String_t* ____scope_3;
	// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4SigningResult::_signingKey
	ByteU5BU5D_t3397334013* ____signingKey_4;
	// System.Byte[] Amazon.Runtime.Internal.Auth.AWS4SigningResult::_signature
	ByteU5BU5D_t3397334013* ____signature_5;

public:
	inline static int32_t get_offset_of__awsAccessKeyId_0() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____awsAccessKeyId_0)); }
	inline String_t* get__awsAccessKeyId_0() const { return ____awsAccessKeyId_0; }
	inline String_t** get_address_of__awsAccessKeyId_0() { return &____awsAccessKeyId_0; }
	inline void set__awsAccessKeyId_0(String_t* value)
	{
		____awsAccessKeyId_0 = value;
		Il2CppCodeGenWriteBarrier((&____awsAccessKeyId_0), value);
	}

	inline static int32_t get_offset_of__originalDateTime_1() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____originalDateTime_1)); }
	inline DateTime_t693205669  get__originalDateTime_1() const { return ____originalDateTime_1; }
	inline DateTime_t693205669 * get_address_of__originalDateTime_1() { return &____originalDateTime_1; }
	inline void set__originalDateTime_1(DateTime_t693205669  value)
	{
		____originalDateTime_1 = value;
	}

	inline static int32_t get_offset_of__signedHeaders_2() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____signedHeaders_2)); }
	inline String_t* get__signedHeaders_2() const { return ____signedHeaders_2; }
	inline String_t** get_address_of__signedHeaders_2() { return &____signedHeaders_2; }
	inline void set__signedHeaders_2(String_t* value)
	{
		____signedHeaders_2 = value;
		Il2CppCodeGenWriteBarrier((&____signedHeaders_2), value);
	}

	inline static int32_t get_offset_of__scope_3() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____scope_3)); }
	inline String_t* get__scope_3() const { return ____scope_3; }
	inline String_t** get_address_of__scope_3() { return &____scope_3; }
	inline void set__scope_3(String_t* value)
	{
		____scope_3 = value;
		Il2CppCodeGenWriteBarrier((&____scope_3), value);
	}

	inline static int32_t get_offset_of__signingKey_4() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____signingKey_4)); }
	inline ByteU5BU5D_t3397334013* get__signingKey_4() const { return ____signingKey_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__signingKey_4() { return &____signingKey_4; }
	inline void set__signingKey_4(ByteU5BU5D_t3397334013* value)
	{
		____signingKey_4 = value;
		Il2CppCodeGenWriteBarrier((&____signingKey_4), value);
	}

	inline static int32_t get_offset_of__signature_5() { return static_cast<int32_t>(offsetof(AWS4SigningResult_t430803065, ____signature_5)); }
	inline ByteU5BU5D_t3397334013* get__signature_5() const { return ____signature_5; }
	inline ByteU5BU5D_t3397334013** get_address_of__signature_5() { return &____signature_5; }
	inline void set__signature_5(ByteU5BU5D_t3397334013* value)
	{
		____signature_5 = value;
		Il2CppCodeGenWriteBarrier((&____signature_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AWS4SIGNINGRESULT_T430803065_H
#ifndef JSONUNMARSHALLERCONTEXT_T456235889_H
#define JSONUNMARSHALLERCONTEXT_T456235889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext
struct  JsonUnmarshallerContext_t456235889  : public UnmarshallerContext_t1608322995
{
public:
	// System.IO.StreamReader Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::streamReader
	StreamReader_t2360341767 * ___streamReader_6;
	// ThirdParty.Json.LitJson.JsonReader Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::jsonReader
	JsonReader_t354941621 * ___jsonReader_7;
	// Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext/JsonPathStack Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::stack
	JsonPathStack_t805090354 * ___stack_8;
	// System.String Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::currentField
	String_t* ___currentField_9;
	// System.Nullable`1<ThirdParty.Json.LitJson.JsonToken> Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::currentToken
	Nullable_1_t708647570  ___currentToken_10;
	// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::disposed
	bool ___disposed_11;
	// System.Boolean Amazon.Runtime.Internal.Transform.JsonUnmarshallerContext::wasPeeked
	bool ___wasPeeked_12;

public:
	inline static int32_t get_offset_of_streamReader_6() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___streamReader_6)); }
	inline StreamReader_t2360341767 * get_streamReader_6() const { return ___streamReader_6; }
	inline StreamReader_t2360341767 ** get_address_of_streamReader_6() { return &___streamReader_6; }
	inline void set_streamReader_6(StreamReader_t2360341767 * value)
	{
		___streamReader_6 = value;
		Il2CppCodeGenWriteBarrier((&___streamReader_6), value);
	}

	inline static int32_t get_offset_of_jsonReader_7() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___jsonReader_7)); }
	inline JsonReader_t354941621 * get_jsonReader_7() const { return ___jsonReader_7; }
	inline JsonReader_t354941621 ** get_address_of_jsonReader_7() { return &___jsonReader_7; }
	inline void set_jsonReader_7(JsonReader_t354941621 * value)
	{
		___jsonReader_7 = value;
		Il2CppCodeGenWriteBarrier((&___jsonReader_7), value);
	}

	inline static int32_t get_offset_of_stack_8() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___stack_8)); }
	inline JsonPathStack_t805090354 * get_stack_8() const { return ___stack_8; }
	inline JsonPathStack_t805090354 ** get_address_of_stack_8() { return &___stack_8; }
	inline void set_stack_8(JsonPathStack_t805090354 * value)
	{
		___stack_8 = value;
		Il2CppCodeGenWriteBarrier((&___stack_8), value);
	}

	inline static int32_t get_offset_of_currentField_9() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___currentField_9)); }
	inline String_t* get_currentField_9() const { return ___currentField_9; }
	inline String_t** get_address_of_currentField_9() { return &___currentField_9; }
	inline void set_currentField_9(String_t* value)
	{
		___currentField_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentField_9), value);
	}

	inline static int32_t get_offset_of_currentToken_10() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___currentToken_10)); }
	inline Nullable_1_t708647570  get_currentToken_10() const { return ___currentToken_10; }
	inline Nullable_1_t708647570 * get_address_of_currentToken_10() { return &___currentToken_10; }
	inline void set_currentToken_10(Nullable_1_t708647570  value)
	{
		___currentToken_10 = value;
	}

	inline static int32_t get_offset_of_disposed_11() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___disposed_11)); }
	inline bool get_disposed_11() const { return ___disposed_11; }
	inline bool* get_address_of_disposed_11() { return &___disposed_11; }
	inline void set_disposed_11(bool value)
	{
		___disposed_11 = value;
	}

	inline static int32_t get_offset_of_wasPeeked_12() { return static_cast<int32_t>(offsetof(JsonUnmarshallerContext_t456235889, ___wasPeeked_12)); }
	inline bool get_wasPeeked_12() const { return ___wasPeeked_12; }
	inline bool* get_address_of_wasPeeked_12() { return &___wasPeeked_12; }
	inline void set_wasPeeked_12(bool value)
	{
		___wasPeeked_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONUNMARSHALLERCONTEXT_T456235889_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef MD5STREAM_T3419057718_H
#define MD5STREAM_T3419057718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.Util.MD5Stream
struct  MD5Stream_t3419057718  : public HashStream_1_t2762322779
{
public:
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.Util.MD5Stream::_logger
	Logger_t2262497814 * ____logger_7;

public:
	inline static int32_t get_offset_of__logger_7() { return static_cast<int32_t>(offsetof(MD5Stream_t3419057718, ____logger_7)); }
	inline Logger_t2262497814 * get__logger_7() const { return ____logger_7; }
	inline Logger_t2262497814 ** get_address_of__logger_7() { return &____logger_7; }
	inline void set__logger_7(Logger_t2262497814 * value)
	{
		____logger_7 = value;
		Il2CppCodeGenWriteBarrier((&____logger_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5STREAM_T3419057718_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef UNITYMAINTHREADDISPATCHER_T4098072663_H
#define UNITYMAINTHREADDISPATCHER_T4098072663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Runtime.Internal.UnityMainThreadDispatcher
struct  UnityMainThreadDispatcher_t4098072663  : public MonoBehaviour_t1158329972
{
public:
	// Amazon.Runtime.Internal.Util.Logger Amazon.Runtime.Internal.UnityMainThreadDispatcher::_logger
	Logger_t2262497814 * ____logger_2;
	// System.Single Amazon.Runtime.Internal.UnityMainThreadDispatcher::_nextUpdateTime
	float ____nextUpdateTime_3;
	// System.Single Amazon.Runtime.Internal.UnityMainThreadDispatcher::_updateInterval
	float ____updateInterval_4;
	// Amazon.Util.Internal.PlatformServices.NetworkStatus Amazon.Runtime.Internal.UnityMainThreadDispatcher::_currentNetworkStatus
	int32_t ____currentNetworkStatus_5;

public:
	inline static int32_t get_offset_of__logger_2() { return static_cast<int32_t>(offsetof(UnityMainThreadDispatcher_t4098072663, ____logger_2)); }
	inline Logger_t2262497814 * get__logger_2() const { return ____logger_2; }
	inline Logger_t2262497814 ** get_address_of__logger_2() { return &____logger_2; }
	inline void set__logger_2(Logger_t2262497814 * value)
	{
		____logger_2 = value;
		Il2CppCodeGenWriteBarrier((&____logger_2), value);
	}

	inline static int32_t get_offset_of__nextUpdateTime_3() { return static_cast<int32_t>(offsetof(UnityMainThreadDispatcher_t4098072663, ____nextUpdateTime_3)); }
	inline float get__nextUpdateTime_3() const { return ____nextUpdateTime_3; }
	inline float* get_address_of__nextUpdateTime_3() { return &____nextUpdateTime_3; }
	inline void set__nextUpdateTime_3(float value)
	{
		____nextUpdateTime_3 = value;
	}

	inline static int32_t get_offset_of__updateInterval_4() { return static_cast<int32_t>(offsetof(UnityMainThreadDispatcher_t4098072663, ____updateInterval_4)); }
	inline float get__updateInterval_4() const { return ____updateInterval_4; }
	inline float* get_address_of__updateInterval_4() { return &____updateInterval_4; }
	inline void set__updateInterval_4(float value)
	{
		____updateInterval_4 = value;
	}

	inline static int32_t get_offset_of__currentNetworkStatus_5() { return static_cast<int32_t>(offsetof(UnityMainThreadDispatcher_t4098072663, ____currentNetworkStatus_5)); }
	inline int32_t get__currentNetworkStatus_5() const { return ____currentNetworkStatus_5; }
	inline int32_t* get_address_of__currentNetworkStatus_5() { return &____currentNetworkStatus_5; }
	inline void set__currentNetworkStatus_5(int32_t value)
	{
		____currentNetworkStatus_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYMAINTHREADDISPATCHER_T4098072663_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (PipelineHandler_t1486422324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[3] = 
{
	PipelineHandler_t1486422324::get_offset_of_U3CLoggerU3Ek__BackingField_0(),
	PipelineHandler_t1486422324::get_offset_of_U3CInnerHandlerU3Ek__BackingField_1(),
	PipelineHandler_t1486422324::get_offset_of_U3COuterHandlerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (RuntimePipeline_t3355992556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[3] = 
{
	RuntimePipeline_t3355992556::get_offset_of__disposed_0(),
	RuntimePipeline_t3355992556::get_offset_of__logger_1(),
	RuntimePipeline_t3355992556::get_offset_of__handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (ErrorHandler_t1573534074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[1] = 
{
	ErrorHandler_t1573534074::get_offset_of__exceptionHandlers_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (HttpErrorResponseExceptionHandler_t3899688054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (CallbackHandler_t174745619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[2] = 
{
	CallbackHandler_t174745619::get_offset_of_U3COnPreInvokeU3Ek__BackingField_3(),
	CallbackHandler_t174745619::get_offset_of_U3COnPostInvokeU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (CredentialsRetriever_t208022274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[1] = 
{
	CredentialsRetriever_t208022274::get_offset_of_U3CCredentialsU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (EndpointResolver_t2369326703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (ErrorCallbackHandler_t394665619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[1] = 
{
	ErrorCallbackHandler_t394665619::get_offset_of_U3COnErrorU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (Marshaller_t3371889241), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (MetricsHandler_t1107617423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (RedirectHandler_t32990664), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (Signer_t1810841758), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (Unmarshaller_t1994457618), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[1] = 
{
	Unmarshaller_t1994457618::get_offset_of__supportsResponseLogging_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (UnityWebRequestFactory_t752669660), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[1] = 
{
	UnityWebRequestFactory_t752669660::get_offset_of__unityRequest_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (UnityRequest_t4218620704), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[13] = 
{
	UnityRequest_t4218620704::get_offset_of_U3CRequestUriU3Ek__BackingField_0(),
	UnityRequest_t4218620704::get_offset_of_U3CWwwRequestU3Ek__BackingField_1(),
	UnityRequest_t4218620704::get_offset_of_U3CRequestContentU3Ek__BackingField_2(),
	UnityRequest_t4218620704::get_offset_of_U3CHeadersU3Ek__BackingField_3(),
	UnityRequest_t4218620704::get_offset_of_U3CCallbackU3Ek__BackingField_4(),
	UnityRequest_t4218620704::get_offset_of_U3CAsyncResultU3Ek__BackingField_5(),
	UnityRequest_t4218620704::get_offset_of_U3CWaitHandleU3Ek__BackingField_6(),
	UnityRequest_t4218620704::get_offset_of_U3CIsSyncU3Ek__BackingField_7(),
	UnityRequest_t4218620704::get_offset_of_U3CResponseU3Ek__BackingField_8(),
	UnityRequest_t4218620704::get_offset_of_U3CExceptionU3Ek__BackingField_9(),
	UnityRequest_t4218620704::get_offset_of_U3CMethodU3Ek__BackingField_10(),
	UnityRequest_t4218620704::get_offset_of__disposed_11(),
	UnityRequest_t4218620704::get_offset_of_U3CTrackerU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (UnityWwwRequestFactory_t3206525961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[1] = 
{
	UnityWwwRequestFactory_t3206525961::get_offset_of__unityWwwRequest_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (UnityWwwRequest_t118609659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[13] = 
{
	UnityWwwRequest_t118609659::get_offset_of_U3CRequestUriU3Ek__BackingField_0(),
	UnityWwwRequest_t118609659::get_offset_of_U3CWwwRequestU3Ek__BackingField_1(),
	UnityWwwRequest_t118609659::get_offset_of_U3CRequestContentU3Ek__BackingField_2(),
	UnityWwwRequest_t118609659::get_offset_of_U3CHeadersU3Ek__BackingField_3(),
	UnityWwwRequest_t118609659::get_offset_of_U3CCallbackU3Ek__BackingField_4(),
	UnityWwwRequest_t118609659::get_offset_of_U3CAsyncResultU3Ek__BackingField_5(),
	UnityWwwRequest_t118609659::get_offset_of_U3CWaitHandleU3Ek__BackingField_6(),
	UnityWwwRequest_t118609659::get_offset_of_U3CIsSyncU3Ek__BackingField_7(),
	UnityWwwRequest_t118609659::get_offset_of_U3CResponseU3Ek__BackingField_8(),
	UnityWwwRequest_t118609659::get_offset_of_U3CExceptionU3Ek__BackingField_9(),
	UnityWwwRequest_t118609659::get_offset_of_U3CMethodU3Ek__BackingField_10(),
	UnityWwwRequest_t118609659::get_offset_of_U3CTrackerU3Ek__BackingField_11(),
	UnityWwwRequest_t118609659::get_offset_of__disposed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (SimpleAsyncResult_t4203640901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[1] = 
{
	SimpleAsyncResult_t4203640901::get_offset_of_U3CAsyncStateU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (DefaultRetryPolicy_t2207644155), -1, sizeof(DefaultRetryPolicy_t2207644155_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2320[7] = 
{
	DefaultRetryPolicy_t2207644155_StaticFields::get_offset_of__capacityManagerInstance_4(),
	DefaultRetryPolicy_t2207644155::get_offset_of__maxBackoffInMilliseconds_5(),
	DefaultRetryPolicy_t2207644155::get_offset_of__retryCapacity_6(),
	DefaultRetryPolicy_t2207644155::get_offset_of__httpStatusCodesToRetryOn_7(),
	DefaultRetryPolicy_t2207644155::get_offset_of__webExceptionStatusesToRetryOn_8(),
	DefaultRetryPolicy_t2207644155_StaticFields::get_offset_of__coreCLRRetryErrorMessages_9(),
	DefaultRetryPolicy_t2207644155::get_offset_of__errorCodesToRetryOn_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (RetryHandler_t3257318146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[2] = 
{
	RetryHandler_t3257318146::get_offset_of__logger_3(),
	RetryHandler_t3257318146::get_offset_of_U3CRetryPolicyU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (RuntimeAsyncResult_t4279356013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[14] = 
{
	RuntimeAsyncResult_t4279356013::get_offset_of__lockObj_0(),
	RuntimeAsyncResult_t4279356013::get_offset_of__waitHandle_1(),
	RuntimeAsyncResult_t4279356013::get_offset_of__disposed_2(),
	RuntimeAsyncResult_t4279356013::get_offset_of__callbackInvoked_3(),
	RuntimeAsyncResult_t4279356013::get_offset_of__logger_4(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CAsyncCallbackU3Ek__BackingField_5(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CAsyncStateU3Ek__BackingField_6(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CCompletedSynchronouslyU3Ek__BackingField_7(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CIsCompletedU3Ek__BackingField_8(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CExceptionU3Ek__BackingField_9(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CResponseU3Ek__BackingField_10(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CRequestU3Ek__BackingField_11(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CActionU3Ek__BackingField_12(),
	RuntimeAsyncResult_t4279356013::get_offset_of_U3CAsyncOptionsU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (ThreadPoolExecutionHandler_t2707466110), -1, sizeof(ThreadPoolExecutionHandler_t2707466110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2323[2] = 
{
	ThreadPoolExecutionHandler_t2707466110_StaticFields::get_offset_of__throttler_3(),
	ThreadPoolExecutionHandler_t2707466110_StaticFields::get_offset_of__lock_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (UnityMainThreadDispatcher_t4098072663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2324[4] = 
{
	UnityMainThreadDispatcher_t4098072663::get_offset_of__logger_2(),
	UnityMainThreadDispatcher_t4098072663::get_offset_of__nextUpdateTime_3(),
	UnityMainThreadDispatcher_t4098072663::get_offset_of__updateInterval_4(),
	UnityMainThreadDispatcher_t4098072663::get_offset_of__currentNetworkStatus_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (U3CU3Ec__DisplayClass7_0_t142100588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[2] = 
{
	U3CU3Ec__DisplayClass7_0_t142100588::get_offset_of_request_0(),
	U3CU3Ec__DisplayClass7_0_t142100588::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (U3CInvokeRequestU3Ed__7_t3085820275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[11] = 
{
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CU3E1__state_0(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CU3E2__current_1(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CU3E4__this_2(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_request_3(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CwwwRequestU3E5__1_4(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CuploadCompletedU3E5__2_5(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CU3E8__3_6(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CunityRequestU3E5__4_7(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CoperationU3E5__5_8(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CuploadCompletedU3E5__6_9(),
	U3CInvokeRequestU3Ed__7_t3085820275::get_offset_of_U3CunityWebRequestU3E5__7_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (UnityRequestQueue_t3684366031), -1, sizeof(UnityRequestQueue_t3684366031_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2327[7] = 
{
	UnityRequestQueue_t3684366031_StaticFields::get_offset_of__instance_0(),
	UnityRequestQueue_t3684366031_StaticFields::get_offset_of__requestsLock_1(),
	UnityRequestQueue_t3684366031_StaticFields::get_offset_of__callbacksLock_2(),
	UnityRequestQueue_t3684366031_StaticFields::get_offset_of__mainThreadCallbackLock_3(),
	UnityRequestQueue_t3684366031::get_offset_of__requests_4(),
	UnityRequestQueue_t3684366031::get_offset_of__callbacks_5(),
	UnityRequestQueue_t3684366031::get_offset_of__mainThreadCallbacks_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (UnityWebRequestWrapper_t1542496577), -1, sizeof(UnityWebRequestWrapper_t1542496577_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2328[19] = 
{
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_unityWebRequestType_0(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_unityWebRequestProperties_1(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_unityWebRequestMethods_2(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_setRequestHeaderMethod_3(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_sendMethod_4(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_getResponseHeadersMethod_5(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_isDoneGetMethod_6(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_downloadProgressGetMethod_7(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_uploadProgressGetMethod_8(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_isErrorGetMethod_9(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_downloadedBytesGetMethod_10(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_responseCodeGetMethod_11(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_downloadHandlerSetMethod_12(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_uploadHandlerSetMethod_13(),
	UnityWebRequestWrapper_t1542496577_StaticFields::get_offset_of_errorGetMethod_14(),
	UnityWebRequestWrapper_t1542496577::get_offset_of_unityWebRequestInstance_15(),
	UnityWebRequestWrapper_t1542496577::get_offset_of_downloadHandler_16(),
	UnityWebRequestWrapper_t1542496577::get_offset_of_uploadHandler_17(),
	UnityWebRequestWrapper_t1542496577::get_offset_of_disposedValue_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (DownloadHandlerBufferWrapper_t1800693841), -1, sizeof(DownloadHandlerBufferWrapper_t1800693841_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2329[7] = 
{
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_downloadHandlerBufferType_0(),
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_downloadHandlerBufferProperties_1(),
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_downloadHandlerBufferMethods_2(),
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_dataProperty_3(),
	DownloadHandlerBufferWrapper_t1800693841_StaticFields::get_offset_of_dataGetMethod_4(),
	DownloadHandlerBufferWrapper_t1800693841::get_offset_of_U3CInstanceU3Ek__BackingField_5(),
	DownloadHandlerBufferWrapper_t1800693841::get_offset_of_disposedValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (UploadHandlerRawWrapper_t3528989734), -1, sizeof(UploadHandlerRawWrapper_t3528989734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2330[3] = 
{
	UploadHandlerRawWrapper_t3528989734_StaticFields::get_offset_of_uploadHandlerRawType_0(),
	UploadHandlerRawWrapper_t3528989734::get_offset_of_U3CInstanceU3Ek__BackingField_1(),
	UploadHandlerRawWrapper_t3528989734::get_offset_of_disposedValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (CachingWrapperStream_t380166576), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[3] = 
{
	CachingWrapperStream_t380166576::get_offset_of__cacheLimit_2(),
	CachingWrapperStream_t380166576::get_offset_of__cachedBytes_3(),
	CachingWrapperStream_t380166576::get_offset_of_U3CAllReadBytesU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (ChunkedUploadWrapperStream_t779979984), -1, sizeof(ChunkedUploadWrapperStream_t779979984_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2332[11] = 
{
	ChunkedUploadWrapperStream_t779979984_StaticFields::get_offset_of_DefaultChunkSize_2(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__inputBuffer_3(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__outputBuffer_4(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__outputBufferPos_5(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__outputBufferDataLen_6(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__wrappedStreamBufferSize_7(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__wrappedStreamConsumed_8(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__outputBufferIsTerminatingChunk_9(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of__readStrategy_10(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of_U3CHeaderSigningResultU3Ek__BackingField_11(),
	ChunkedUploadWrapperStream_t779979984::get_offset_of_U3CPreviousChunkSignatureU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (ReadStrategy_t4036775975)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2333[3] = 
{
	ReadStrategy_t4036775975::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (BackgroundInvoker_t1722929158), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (U3CU3Ec_t464705533), -1, sizeof(U3CU3Ec_t464705533_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2336[2] = 
{
	U3CU3Ec_t464705533_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t464705533_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (Extensions_t2019041272), -1, sizeof(Extensions_t2019041272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2337[2] = 
{
	Extensions_t2019041272_StaticFields::get_offset_of_ticksPerSecond_0(),
	Extensions_t2019041272_StaticFields::get_offset_of_tickFrequency_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (Hashing_t180418764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (U3CU3Ec_t3244221195), -1, sizeof(U3CU3Ec_t3244221195_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2339[2] = 
{
	U3CU3Ec_t3244221195_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3244221195_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (HashingWrapper_t516426937), -1, sizeof(HashingWrapper_t516426937_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2340[2] = 
{
	HashingWrapper_t516426937_StaticFields::get_offset_of_MD5ManagedName_0(),
	HashingWrapper_t516426937::get_offset_of__algorithm_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (HashStream_t2318308192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[5] = 
{
	HashStream_t2318308192::get_offset_of_U3CAlgorithmU3Ek__BackingField_2(),
	HashStream_t2318308192::get_offset_of_U3CCurrentPositionU3Ek__BackingField_3(),
	HashStream_t2318308192::get_offset_of_U3CCalculatedHashU3Ek__BackingField_4(),
	HashStream_t2318308192::get_offset_of_U3CExpectedHashU3Ek__BackingField_5(),
	HashStream_t2318308192::get_offset_of_U3CExpectedLengthU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (MD5Stream_t3419057718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[1] = 
{
	MD5Stream_t3419057718::get_offset_of__logger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (InternalConsoleLogger_t4018195642), -1, sizeof(InternalConsoleLogger_t4018195642_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2346[1] = 
{
	InternalConsoleLogger_t4018195642_StaticFields::get_offset_of__sequanceId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (LogLevel_t527408354)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2347[7] = 
{
	LogLevel_t527408354::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (Logger_t2262497814), -1, sizeof(Logger_t2262497814_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2348[3] = 
{
	Logger_t2262497814_StaticFields::get_offset_of_cachedLoggers_0(),
	Logger_t2262497814::get_offset_of_loggers_1(),
	Logger_t2262497814_StaticFields::get_offset_of_emptyLogger_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (InternalLogger_t2972373343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[2] = 
{
	InternalLogger_t2972373343::get_offset_of_U3CDeclaringTypeU3Ek__BackingField_0(),
	InternalLogger_t2972373343::get_offset_of_U3CIsEnabledU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (InternalLog4netLogger_t1777422022), -1, sizeof(InternalLog4netLogger_t1777422022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2350[20] = 
{
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_loadState_2(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_LOCK_3(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logMangerType_4(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logMangerTypeInfo_5(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_getLoggerWithTypeMethod_6(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logType_7(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logTypeInfo_8(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_logMethod_9(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_levelType_10(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_levelTypeInfo_11(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_debugLevelPropertyValue_12(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_infoLevelPropertyValue_13(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_errorLevelPropertyValue_14(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_isEnabledForMethod_15(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_systemStringFormatType_16(),
	InternalLog4netLogger_t1777422022_StaticFields::get_offset_of_loggerType_17(),
	InternalLog4netLogger_t1777422022::get_offset_of_internalLogger_18(),
	InternalLog4netLogger_t1777422022::get_offset_of_isErrorEnabled_19(),
	InternalLog4netLogger_t1777422022::get_offset_of_isDebugEnabled_20(),
	InternalLog4netLogger_t1777422022::get_offset_of_isInfoEnabled_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (LoadState_t3882482337)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2351[5] = 
{
	LoadState_t3882482337::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (LogMessage_t4004523899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[3] = 
{
	LogMessage_t4004523899::get_offset_of_U3CArgsU3Ek__BackingField_0(),
	LogMessage_t4004523899::get_offset_of_U3CProviderU3Ek__BackingField_1(),
	LogMessage_t4004523899::get_offset_of_U3CFormatU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (RequestMetrics_t218029284), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[8] = 
{
	RequestMetrics_t218029284::get_offset_of_metricsLock_0(),
	RequestMetrics_t218029284::get_offset_of_stopWatch_1(),
	RequestMetrics_t218029284::get_offset_of_inFlightTimings_2(),
	RequestMetrics_t218029284::get_offset_of_errors_3(),
	RequestMetrics_t218029284::get_offset_of_U3CPropertiesU3Ek__BackingField_4(),
	RequestMetrics_t218029284::get_offset_of_U3CTimingsU3Ek__BackingField_5(),
	RequestMetrics_t218029284::get_offset_of_U3CCountersU3Ek__BackingField_6(),
	RequestMetrics_t218029284::get_offset_of_U3CIsEnabledU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (U3CU3Ec_t963174459), -1, sizeof(U3CU3Ec_t963174459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2357[2] = 
{
	U3CU3Ec_t963174459_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t963174459_StaticFields::get_offset_of_U3CU3E9__33_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (Timing_t847194262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[3] = 
{
	Timing_t847194262::get_offset_of_startTime_0(),
	Timing_t847194262::get_offset_of_endTime_1(),
	Timing_t847194262::get_offset_of_U3CIsFinishedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (TimingEvent_t181853090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[3] = 
{
	TimingEvent_t181853090::get_offset_of_metric_0(),
	TimingEvent_t181853090::get_offset_of_metrics_1(),
	TimingEvent_t181853090::get_offset_of_disposed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (MetricError_t964444806), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[4] = 
{
	MetricError_t964444806::get_offset_of_U3CMetricU3Ek__BackingField_0(),
	MetricError_t964444806::get_offset_of_U3CMessageU3Ek__BackingField_1(),
	MetricError_t964444806::get_offset_of_U3CExceptionU3Ek__BackingField_2(),
	MetricError_t964444806::get_offset_of_U3CTimeU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (PartialWrapperStream_t1744080210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[2] = 
{
	PartialWrapperStream_t1744080210::get_offset_of_initialPosition_2(),
	PartialWrapperStream_t1744080210::get_offset_of_partSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (S3Uri_t3301881392), -1, sizeof(S3Uri_t3301881392_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2362[1] = 
{
	S3Uri_t3301881392_StaticFields::get_offset_of_S3EndpointRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (WrapperStream_t816765491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[1] = 
{
	WrapperStream_t816765491::get_offset_of_U3CBaseStreamU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (HashingWrapperMD5_t3486550061), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (UnityDebugLogger_t3216514868), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (UnityDebugTraceListener_t2078152869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (JsonUnmarshallerContext_t456235889), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[7] = 
{
	JsonUnmarshallerContext_t456235889::get_offset_of_streamReader_6(),
	JsonUnmarshallerContext_t456235889::get_offset_of_jsonReader_7(),
	JsonUnmarshallerContext_t456235889::get_offset_of_stack_8(),
	JsonUnmarshallerContext_t456235889::get_offset_of_currentField_9(),
	JsonUnmarshallerContext_t456235889::get_offset_of_currentToken_10(),
	JsonUnmarshallerContext_t456235889::get_offset_of_disposed_11(),
	JsonUnmarshallerContext_t456235889::get_offset_of_wasPeeked_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (JsonPathStack_t805090354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[4] = 
{
	JsonPathStack_t805090354::get_offset_of_stack_0(),
	JsonPathStack_t805090354::get_offset_of_currentDepth_1(),
	JsonPathStack_t805090354::get_offset_of_stackStringBuilder_2(),
	JsonPathStack_t805090354::get_offset_of_stackString_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (ResponseUnmarshaller_t3934041557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (XmlResponseUnmarshaller_t760346204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (IntUnmarshaller_t2174001701), -1, sizeof(IntUnmarshaller_t2174001701_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2379[1] = 
{
	IntUnmarshaller_t2174001701_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (LongUnmarshaller_t2567652076), -1, sizeof(LongUnmarshaller_t2567652076_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2380[1] = 
{
	LongUnmarshaller_t2567652076_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (BoolUnmarshaller_t2985984772), -1, sizeof(BoolUnmarshaller_t2985984772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2381[1] = 
{
	BoolUnmarshaller_t2985984772_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (StringUnmarshaller_t3953260147), -1, sizeof(StringUnmarshaller_t3953260147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2382[1] = 
{
	StringUnmarshaller_t3953260147_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (DateTimeUnmarshaller_t1723598451), -1, sizeof(DateTimeUnmarshaller_t1723598451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2383[1] = 
{
	DateTimeUnmarshaller_t1723598451_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (UnmarshallerExtensions_t2325150308), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (UnmarshallerContext_t1608322995), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2385[6] = 
{
	UnmarshallerContext_t1608322995::get_offset_of_disposed_0(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CMaintainResponseBodyU3Ek__BackingField_1(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CCrcStreamU3Ek__BackingField_2(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CCrc32ResultU3Ek__BackingField_3(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CWebResponseDataU3Ek__BackingField_4(),
	UnmarshallerContext_t1608322995::get_offset_of_U3CWrappingStreamU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (XmlUnmarshallerContext_t1179575220), -1, sizeof(XmlUnmarshallerContext_t1179575220_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2386[12] = 
{
	XmlUnmarshallerContext_t1179575220_StaticFields::get_offset_of_READER_SETTINGS_6(),
	XmlUnmarshallerContext_t1179575220_StaticFields::get_offset_of_nodesToSkip_7(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_streamReader_8(),
	XmlUnmarshallerContext_t1179575220::get_offset_of__xmlReader_9(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_stack_10(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_stackString_11(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_attributeValues_12(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_attributeNames_13(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_attributeEnumerator_14(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_nodeType_15(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_nodeContent_16(),
	XmlUnmarshallerContext_t1179575220::get_offset_of_disposed_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (UnityWebResponseData_t3141113362), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[10] = 
{
	UnityWebResponseData_t3141113362::get_offset_of__headers_0(),
	UnityWebResponseData_t3141113362::get_offset_of__headerNames_1(),
	UnityWebResponseData_t3141113362::get_offset_of__headerNamesSet_2(),
	UnityWebResponseData_t3141113362::get_offset_of__responseStream_3(),
	UnityWebResponseData_t3141113362::get_offset_of__responseBody_4(),
	UnityWebResponseData_t3141113362::get_offset_of__logger_5(),
	UnityWebResponseData_t3141113362::get_offset_of_U3CContentLengthU3Ek__BackingField_6(),
	UnityWebResponseData_t3141113362::get_offset_of_U3CContentTypeU3Ek__BackingField_7(),
	UnityWebResponseData_t3141113362::get_offset_of_U3CStatusCodeU3Ek__BackingField_8(),
	UnityWebResponseData_t3141113362::get_offset_of_U3CIsSuccessStatusCodeU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (ClientProtocol_t4005895111)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2388[4] = 
{
	ClientProtocol_t4005895111::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (AbstractAWSSigner_t2114314031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[1] = 
{
	AbstractAWSSigner_t2114314031::get_offset_of__aws4Signer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (AWS4Signer_t1011449585), -1, sizeof(AWS4Signer_t1011449585_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2390[4] = 
{
	AWS4Signer_t1011449585_StaticFields::get_offset_of_TerminatorBytes_1(),
	AWS4Signer_t1011449585_StaticFields::get_offset_of_CompressWhitespaceRegex_2(),
	AWS4Signer_t1011449585_StaticFields::get_offset_of__headersToIgnoreWhenSigning_3(),
	AWS4Signer_t1011449585::get_offset_of_U3CSignPayloadU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (U3CU3Ec_t2266868118), -1, sizeof(U3CU3Ec_t2266868118_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2391[5] = 
{
	U3CU3Ec_t2266868118_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2266868118_StaticFields::get_offset_of_U3CU3E9__46_0_1(),
	U3CU3Ec_t2266868118_StaticFields::get_offset_of_U3CU3E9__47_0_2(),
	U3CU3Ec_t2266868118_StaticFields::get_offset_of_U3CU3E9__48_0_3(),
	U3CU3Ec_t2266868118_StaticFields::get_offset_of_U3CU3E9__52_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (AWS4SigningResult_t430803065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[6] = 
{
	AWS4SigningResult_t430803065::get_offset_of__awsAccessKeyId_0(),
	AWS4SigningResult_t430803065::get_offset_of__originalDateTime_1(),
	AWS4SigningResult_t430803065::get_offset_of__signedHeaders_2(),
	AWS4SigningResult_t430803065::get_offset_of__scope_3(),
	AWS4SigningResult_t430803065::get_offset_of__signingKey_4(),
	AWS4SigningResult_t430803065::get_offset_of__signature_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2393[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U350B1635D1FB2907A171B71751E1A3FA79423CA17_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (__StaticArrayInitTypeSizeU3D112_t2631791551)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D112_t2631791551 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (U3CModuleU3E_t3783534236), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (EventHandle_t942672932)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2396[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
