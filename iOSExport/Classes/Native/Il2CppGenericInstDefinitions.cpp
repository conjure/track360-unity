﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"




extern const Il2CppType RuntimeObject_0_0_0;
extern const Il2CppType Int32_t2071877448_0_0_0;
extern const Il2CppType Char_t3454481338_0_0_0;
extern const Il2CppType Int64_t909078037_0_0_0;
extern const Il2CppType UInt32_t2149682021_0_0_0;
extern const Il2CppType UInt64_t2909196914_0_0_0;
extern const Il2CppType Byte_t3683104436_0_0_0;
extern const Il2CppType SByte_t454417549_0_0_0;
extern const Il2CppType Int16_t4041245914_0_0_0;
extern const Il2CppType UInt16_t986882611_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IConvertible_t908092482_0_0_0;
extern const Il2CppType IComparable_t1857082765_0_0_0;
extern const Il2CppType IEnumerable_t2911409499_0_0_0;
extern const Il2CppType ICloneable_t3853279282_0_0_0;
extern const Il2CppType IComparable_1_t3861059456_0_0_0;
extern const Il2CppType IEquatable_1_t4233202402_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IReflect_t3412036974_0_0_0;
extern const Il2CppType _Type_t102776839_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t502202687_0_0_0;
extern const Il2CppType _MemberInfo_t332722161_0_0_0;
extern const Il2CppType Double_t4078015681_0_0_0;
extern const Il2CppType Single_t2076509932_0_0_0;
extern const Il2CppType Decimal_t724701077_0_0_0;
extern const Il2CppType Boolean_t3825574718_0_0_0;
extern const Il2CppType Delegate_t3022476291_0_0_0;
extern const Il2CppType ISerializable_t1245643778_0_0_0;
extern const Il2CppType ParameterInfo_t2249040075_0_0_0;
extern const Il2CppType _ParameterInfo_t470209990_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType _FieldInfo_t2511231167_0_0_0;
extern const Il2CppType ParameterModifier_t1820634920_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType _MethodInfo_t3642518830_0_0_0;
extern const Il2CppType MethodBase_t904190842_0_0_0;
extern const Il2CppType _MethodBase_t1935530873_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType _PropertyInfo_t1567586598_0_0_0;
extern const Il2CppType ConstructorInfo_t2851816542_0_0_0;
extern const Il2CppType _ConstructorInfo_t3269099341_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType TableRange_t2011406615_0_0_0;
extern const Il2CppType TailoringInfo_t1449609243_0_0_0;
extern const Il2CppType KeyValuePair_2_t3716250094_0_0_0;
extern const Il2CppType Link_t2723257478_0_0_0;
extern const Il2CppType DictionaryEntry_t3048875398_0_0_0;
extern const Il2CppType KeyValuePair_2_t1744001932_0_0_0;
extern const Il2CppType Contraction_t1673853792_0_0_0;
extern const Il2CppType Level2Map_t3322505726_0_0_0;
extern const Il2CppType BigInteger_t925946152_0_0_0;
extern const Il2CppType KeySizes_t3144736271_0_0_0;
extern const Il2CppType KeyValuePair_2_t38854645_0_0_0;
extern const Il2CppType Slot_t2022531261_0_0_0;
extern const Il2CppType Slot_t2267560602_0_0_0;
extern const Il2CppType StackFrame_t2050294881_0_0_0;
extern const Il2CppType Calendar_t585061108_0_0_0;
extern const Il2CppType ModuleBuilder_t4156028127_0_0_0;
extern const Il2CppType _ModuleBuilder_t1075102050_0_0_0;
extern const Il2CppType Module_t4282841206_0_0_0;
extern const Il2CppType _Module_t2144668161_0_0_0;
extern const Il2CppType ParameterBuilder_t3344728474_0_0_0;
extern const Il2CppType _ParameterBuilder_t2251638747_0_0_0;
extern const Il2CppType TypeU5BU5D_t1664964607_0_0_0;
extern const Il2CppType RuntimeArray_0_0_0;
extern const Il2CppType ICollection_t91669223_0_0_0;
extern const Il2CppType IList_t3321498491_0_0_0;
extern const Il2CppType IList_1_t1844743827_0_0_0;
extern const Il2CppType ICollection_1_t2255878531_0_0_0;
extern const Il2CppType IEnumerable_1_t1595930271_0_0_0;
extern const Il2CppType IList_1_t3952977575_0_0_0;
extern const Il2CppType ICollection_1_t69144983_0_0_0;
extern const Il2CppType IEnumerable_1_t3704164019_0_0_0;
extern const Il2CppType IList_1_t643717440_0_0_0;
extern const Il2CppType ICollection_1_t1054852144_0_0_0;
extern const Il2CppType IEnumerable_1_t394903884_0_0_0;
extern const Il2CppType IList_1_t289070565_0_0_0;
extern const Il2CppType ICollection_1_t700205269_0_0_0;
extern const Il2CppType IEnumerable_1_t40257009_0_0_0;
extern const Il2CppType IList_1_t1043143288_0_0_0;
extern const Il2CppType ICollection_1_t1454277992_0_0_0;
extern const Il2CppType IEnumerable_1_t794329732_0_0_0;
extern const Il2CppType IList_1_t873662762_0_0_0;
extern const Il2CppType ICollection_1_t1284797466_0_0_0;
extern const Il2CppType IEnumerable_1_t624849206_0_0_0;
extern const Il2CppType IList_1_t3230389896_0_0_0;
extern const Il2CppType ICollection_1_t3641524600_0_0_0;
extern const Il2CppType IEnumerable_1_t2981576340_0_0_0;
extern const Il2CppType ILTokenInfo_t149559338_0_0_0;
extern const Il2CppType LabelData_t3712112744_0_0_0;
extern const Il2CppType LabelFixup_t4090909514_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t1370236603_0_0_0;
extern const Il2CppType CustomAttributeBuilder_t2970303184_0_0_0;
extern const Il2CppType _CustomAttributeBuilder_t3917123293_0_0_0;
extern const Il2CppType RefEmitPermissionSet_t2708608433_0_0_0;
extern const Il2CppType TypeBuilder_t3308873219_0_0_0;
extern const Il2CppType _TypeBuilder_t2783404358_0_0_0;
extern const Il2CppType MethodBuilder_t644187984_0_0_0;
extern const Il2CppType _MethodBuilder_t3932949077_0_0_0;
extern const Il2CppType FieldBuilder_t2784804005_0_0_0;
extern const Il2CppType _FieldBuilder_t1895266044_0_0_0;
extern const Il2CppType MonoResource_t3127387157_0_0_0;
extern const Il2CppType ConstructorBuilder_t700974433_0_0_0;
extern const Il2CppType _ConstructorBuilder_t1236878896_0_0_0;
extern const Il2CppType PropertyBuilder_t3694255912_0_0_0;
extern const Il2CppType _PropertyBuilder_t3341912621_0_0_0;
extern const Il2CppType EventBuilder_t2927243889_0_0_0;
extern const Il2CppType _EventBuilder_t801715496_0_0_0;
extern const Il2CppType CustomAttributeTypedArgument_t1498197914_0_0_0;
extern const Il2CppType CustomAttributeNamedArgument_t94157543_0_0_0;
extern const Il2CppType CustomAttributeData_t3093286891_0_0_0;
extern const Il2CppType ResourceInfo_t3933049236_0_0_0;
extern const Il2CppType ResourceCacheItem_t333236149_0_0_0;
extern const Il2CppType IContextProperty_t287246399_0_0_0;
extern const Il2CppType Header_t2756440555_0_0_0;
extern const Il2CppType ITrackingHandler_t2759960940_0_0_0;
extern const Il2CppType IContextAttribute_t2439121372_0_0_0;
extern const Il2CppType DateTime_t693205669_0_0_0;
extern const Il2CppType TimeSpan_t3430258949_0_0_0;
extern const Il2CppType TypeTag_t141209596_0_0_0;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType StrongName_t2988747270_0_0_0;
extern const Il2CppType IBuiltInEvidence_t1114073477_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t2988326850_0_0_0;
extern const Il2CppType DateTimeOffset_t1362988906_0_0_0;
extern const Il2CppType Guid_t_0_0_0;
extern const Il2CppType Version_t1755874712_0_0_0;
extern const Il2CppType Node_t2499136326_0_0_0;
extern const Il2CppType X509ChainStatus_t4278378721_0_0_0;
extern const Il2CppType IPAddress_t1399971723_0_0_0;
extern const Il2CppType ArraySegment_1_t2594217482_0_0_0;
extern const Il2CppType Cookie_t3154017544_0_0_0;
extern const Il2CppType KeyValuePair_2_t1174980068_0_0_0;
extern const Il2CppType KeyValuePair_2_t3497699202_0_0_0;
extern const Il2CppType X509Certificate_t283079845_0_0_0;
extern const Il2CppType IDeserializationCallback_t327125377_0_0_0;
extern const Il2CppType Capture_t4157900610_0_0_0;
extern const Il2CppType Group_t3761430853_0_0_0;
extern const Il2CppType KeyValuePair_2_t3132015601_0_0_0;
extern const Il2CppType Mark_t2724874473_0_0_0;
extern const Il2CppType UriScheme_t1876590943_0_0_0;
extern const Il2CppType BigInteger_t925946153_0_0_0;
extern const Il2CppType ByteU5BU5D_t3397334013_0_0_0;
extern const Il2CppType IList_1_t4224045037_0_0_0;
extern const Il2CppType ICollection_1_t340212445_0_0_0;
extern const Il2CppType IEnumerable_1_t3975231481_0_0_0;
extern const Il2CppType ClientCertificateType_t4001384466_0_0_0;
extern const Il2CppType Link_t865133271_0_0_0;
extern const Il2CppType LockDetails_t2550015005_0_0_0;
extern const Il2CppType AsyncOperation_t3814632279_0_0_0;
extern const Il2CppType Camera_t189460977_0_0_0;
extern const Il2CppType Behaviour_t955675639_0_0_0;
extern const Il2CppType Component_t3819376471_0_0_0;
extern const Il2CppType Object_t1021602117_0_0_0;
extern const Il2CppType Display_t3666191348_0_0_0;
extern const Il2CppType Keyframe_t1449471340_0_0_0;
extern const Il2CppType Vector3_t2243707580_0_0_0;
extern const Il2CppType Vector4_t2243707581_0_0_0;
extern const Il2CppType Vector2_t2243707579_0_0_0;
extern const Il2CppType Color32_t874517518_0_0_0;
extern const Il2CppType Playable_t1896841784_0_0_0;
extern const Il2CppType PlayableOutput_t988259697_0_0_0;
extern const Il2CppType Scene_t1684909666_0_0_0;
extern const Il2CppType LoadSceneMode_t2981886439_0_0_0;
extern const Il2CppType SpriteAtlas_t3132429450_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t2656950_0_0_0;
extern const Il2CppType Attribute_t542643598_0_0_0;
extern const Il2CppType _Attribute_t1557664299_0_0_0;
extern const Il2CppType ExecuteInEditMode_t3043633143_0_0_0;
extern const Il2CppType RequireComponent_t864575032_0_0_0;
extern const Il2CppType HitInfo_t1761367055_0_0_0;
extern const Il2CppType PersistentCall_t3793436469_0_0_0;
extern const Il2CppType BaseInvokableCall_t2229564840_0_0_0;
extern const Il2CppType WorkRequest_t1154022482_0_0_0;
extern const Il2CppType PlayableBinding_t2498078091_0_0_0;
extern const Il2CppType MessageTypeSubscribers_t2291506050_0_0_0;
extern const Il2CppType MessageEventArgs_t301283622_0_0_0;
extern const Il2CppType WeakReference_t1077405567_0_0_0;
extern const Il2CppType KeyValuePair_2_t888819835_0_0_0;
extern const Il2CppType KeyValuePair_2_t3571743403_0_0_0;
extern const Il2CppType AudioSpatializerExtensionDefinition_t3666104158_0_0_0;
extern const Il2CppType AudioAmbisonicExtensionDefinition_t52665437_0_0_0;
extern const Il2CppType AudioSourceExtension_t1813599864_0_0_0;
extern const Il2CppType ScriptableObject_t1975622470_0_0_0;
extern const Il2CppType AudioMixerPlayable_t1831808911_0_0_0;
extern const Il2CppType AudioClipPlayable_t192218916_0_0_0;
extern const Il2CppType Rigidbody2D_t502193897_0_0_0;
extern const Il2CppType Font_t4239498691_0_0_0;
extern const Il2CppType UIVertex_t1204258818_0_0_0;
extern const Il2CppType UICharInfo_t3056636800_0_0_0;
extern const Il2CppType UILineInfo_t3621277874_0_0_0;
extern const Il2CppType KeyValuePair_2_t1701344717_0_0_0;
extern const Il2CppType XmlSchemaAttribute_t4015859774_0_0_0;
extern const Il2CppType XmlSchemaAnnotated_t2082486936_0_0_0;
extern const Il2CppType XmlSchemaObject_t2050913741_0_0_0;
extern const Il2CppType XsdIdentityPath_t2037874_0_0_0;
extern const Il2CppType XsdIdentityField_t2563516441_0_0_0;
extern const Il2CppType XsdIdentityStep_t452377251_0_0_0;
extern const Il2CppType XmlAttribute_t175731005_0_0_0;
extern const Il2CppType IHasXmlChildNode_t2048545686_0_0_0;
extern const Il2CppType XmlNode_t616554813_0_0_0;
extern const Il2CppType IXPathNavigable_t845515791_0_0_0;
extern const Il2CppType XmlQualifiedName_t1944712516_0_0_0;
extern const Il2CppType Regex_t1803876613_0_0_0;
extern const Il2CppType XmlSchemaSimpleType_t248156492_0_0_0;
extern const Il2CppType XmlSchemaType_t1795078578_0_0_0;
extern const Il2CppType XmlException_t4188277960_0_0_0;
extern const Il2CppType SystemException_t3877406272_0_0_0;
extern const Il2CppType Exception_t1927440687_0_0_0;
extern const Il2CppType _Exception_t3026971024_0_0_0;
extern const Il2CppType KeyValuePair_2_t1430411454_0_0_0;
extern const Il2CppType DTDNode_t1758286970_0_0_0;
extern const Il2CppType IXmlLineInfo_t135184468_0_0_0;
extern const Il2CppType AttributeSlot_t1499247213_0_0_0;
extern const Il2CppType Entry_t2583369454_0_0_0;
extern const Il2CppType NsDecl_t3210081295_0_0_0;
extern const Il2CppType NsScope_t2513625351_0_0_0;
extern const Il2CppType XmlAttributeTokenInfo_t3353594030_0_0_0;
extern const Il2CppType XmlTokenInfo_t254587324_0_0_0;
extern const Il2CppType TagName_t2340974457_0_0_0;
extern const Il2CppType XmlNodeInfo_t3709371029_0_0_0;
extern const Il2CppType AnimationClipPlayable_t4099382200_0_0_0;
extern const Il2CppType AnimationLayerMixerPlayable_t3057952312_0_0_0;
extern const Il2CppType AnimationMixerPlayable_t1343787797_0_0_0;
extern const Il2CppType AnimationOffsetPlayable_t1019600543_0_0_0;
extern const Il2CppType AnimatorControllerPlayable_t1744083903_0_0_0;
extern const Il2CppType AchievementDescription_t3110978151_0_0_0;
extern const Il2CppType IAchievementDescription_t3498529102_0_0_0;
extern const Il2CppType UserProfile_t3365630962_0_0_0;
extern const Il2CppType IUserProfile_t4108565527_0_0_0;
extern const Il2CppType GcLeaderboard_t453887929_0_0_0;
extern const Il2CppType IAchievementDescriptionU5BU5D_t4083280315_0_0_0;
extern const Il2CppType IAchievementU5BU5D_t2709554645_0_0_0;
extern const Il2CppType IAchievement_t1752291260_0_0_0;
extern const Il2CppType GcAchievementData_t1754866149_0_0_0;
extern const Il2CppType Achievement_t1333316625_0_0_0;
extern const Il2CppType IScoreU5BU5D_t3237304636_0_0_0;
extern const Il2CppType IScore_t513966369_0_0_0;
extern const Il2CppType GcScoreData_t3676783238_0_0_0;
extern const Il2CppType Score_t2307748940_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t3461248430_0_0_0;
extern const Il2CppType GUILayoutOption_t4183744904_0_0_0;
extern const Il2CppType LayoutCache_t3120781045_0_0_0;
extern const Il2CppType KeyValuePair_2_t3749587448_0_0_0;
extern const Il2CppType KeyValuePair_2_t4180919198_0_0_0;
extern const Il2CppType GUILayoutEntry_t3828586629_0_0_0;
extern const Il2CppType GUIStyle_t1799908754_0_0_0;
extern const Il2CppType KeyValuePair_2_t1472033238_0_0_0;
extern const Il2CppType RaycastHit_t87180320_0_0_0;
extern const Il2CppType ContactPoint_t1376425630_0_0_0;
extern const Il2CppType XAttribute_t3858477518_0_0_0;
extern const Il2CppType XObject_t3550811009_0_0_0;
extern const Il2CppType XNode_t2707504214_0_0_0;
extern const Il2CppType XElement_t553821050_0_0_0;
extern const Il2CppType IXmlSerializable_t2623090263_0_0_0;
extern const Il2CppType XContainer_t1445911831_0_0_0;
extern const Il2CppType XName_t785190363_0_0_0;
extern const Il2CppType XNamespace_t1613015075_0_0_0;
extern const Il2CppType KeyValuePair_2_t1285139559_0_0_0;
extern const Il2CppType IEquatable_1_t2989172532_0_0_0;
extern const Il2CppType KeyValuePair_2_t457314847_0_0_0;
extern const Il2CppType Func_2_t3675469371_0_0_0;
extern const Il2CppType MulticastDelegate_t3201952435_0_0_0;
extern const Il2CppType KeyValuePair_2_t3370172490_0_0_0;
extern const Il2CppType KeyValuePair_2_t2694600775_0_0_0;
extern const Il2CppType Dictionary_2_t1629922792_0_0_0;
extern const Il2CppType Methods_t1187897474_0_0_0;
extern const Il2CppType KeyValuePair_2_t3682235310_0_0_0;
extern const Il2CppType IDictionary_t596158605_0_0_0;
extern const Il2CppType KeyValuePair_2_t4124260628_0_0_0;
extern const Il2CppType JsonData_t4263252052_0_0_0;
extern const Il2CppType KeyValuePair_2_t3935376536_0_0_0;
extern const Il2CppType IJsonWrapper_t3095378610_0_0_0;
extern const Il2CppType IOrderedDictionary_t252115128_0_0_0;
extern const Il2CppType IEquatable_1_t2172266925_0_0_0;
extern const Il2CppType PropertyMetadata_t3287739986_0_0_0;
extern const Il2CppType KeyValuePair_2_t637145336_0_0_0;
extern const Il2CppType ExporterFunc_t173265409_0_0_0;
extern const Il2CppType IDictionary_2_t787128596_0_0_0;
extern const Il2CppType ImporterFunc_t850687278_0_0_0;
extern const Il2CppType ArrayMetadata_t1135078014_0_0_0;
extern const Il2CppType KeyValuePair_2_t2779450660_0_0_0;
extern const Il2CppType IDictionary_2_t3266987655_0_0_0;
extern const Il2CppType ObjectMetadata_t4058137740_0_0_0;
extern const Il2CppType KeyValuePair_2_t1407543090_0_0_0;
extern const Il2CppType IList_1_t3828680587_0_0_0;
extern const Il2CppType Link_t204904209_0_0_0;
extern const Il2CppType KeyValuePair_2_t829781133_0_0_0;
extern const Il2CppType KeyValuePair_2_t2961690774_0_0_0;
extern const Il2CppType KeyValuePair_2_t3752840859_0_0_0;
extern const Il2CppType KeyValuePair_2_t3523383706_0_0_0;
extern const Il2CppType KeyValuePair_2_t4162935824_0_0_0;
extern const Il2CppType KeyValuePair_2_t481831715_0_0_0;
extern const Il2CppType KeyValuePair_2_t2959864470_0_0_0;
extern const Il2CppType KeyValuePair_2_t3025249456_0_0_0;
extern const Il2CppType ITypeInfo_t3592676621_0_0_0;
extern const Il2CppType KeyValuePair_2_t545390397_0_0_0;
extern const Il2CppType JsonToken_t2445581255_0_0_0;
extern const Il2CppType WriterContext_t1209007092_0_0_0;
extern const Il2CppType StateHandler_t3489987002_0_0_0;
extern const Il2CppType List_1_t2784070411_0_0_0;
extern const Il2CppType TraceListener_t3414949279_0_0_0;
extern const Il2CppType IDisposable_t2427283555_0_0_0;
extern const Il2CppType MarshalByRefObject_t1285298191_0_0_0;
extern const Il2CppType KeyValuePair_2_t2456194895_0_0_0;
extern const Il2CppType KeyValuePair_2_t225945534_0_0_0;
extern const Il2CppType RegionEndpoint_t661522805_0_0_0;
extern const Il2CppType KeyValuePair_2_t333647289_0_0_0;
extern const Il2CppType RegionEndpoint_t1885241449_0_0_0;
extern const Il2CppType IRegionEndpoint_t544433888_0_0_0;
extern const Il2CppType KeyValuePair_2_t1557365933_0_0_0;
extern const Il2CppType Endpoint_t3348692641_0_0_0;
extern const Il2CppType KeyValuePair_2_t3020817125_0_0_0;
extern const Il2CppType KeyValuePair_2_t216558372_0_0_0;
extern const Il2CppType KeyValuePair_2_t3089358386_0_0_0;
extern const Il2CppType Action_t3226471752_0_0_0;
extern const Il2CppType InstantiationModel_t1632807378_0_0_0;
extern const Il2CppType KeyValuePair_2_t3277180024_0_0_0;
extern const Il2CppType KeyValuePair_2_t1327510497_0_0_0;
extern const Il2CppType KeyValuePair_2_t2384152414_0_0_0;
extern const Il2CppType KeyValuePair_2_t998506345_0_0_0;
extern const Il2CppType NetworkStatusEventArgs_t1607167653_0_0_0;
extern const Il2CppType IRequest_t2400804350_0_0_0;
extern const Il2CppType AmazonWebServiceRequest_t3384026212_0_0_0;
extern const Il2CppType AmazonWebServiceResponse_t529043356_0_0_0;
extern const Il2CppType AsyncOptions_t558351272_0_0_0;
extern const Il2CppType StreamTransferProgressArgs_t2639638063_0_0_0;
extern const Il2CppType IExecutionContext_t2477130752_0_0_0;
extern const Il2CppType IPipelineHandler_t2320756001_0_0_0;
extern const Il2CppType KeyValuePair_2_t2361573779_0_0_0;
extern const Il2CppType Dictionary_2_t1620371852_0_0_0;
extern const Il2CppType ConstantClass_t4000559886_0_0_0;
extern const Il2CppType KeyValuePair_2_t3672684370_0_0_0;
extern const Il2CppType KeyValuePair_2_t1315074971_0_0_0;
extern const Il2CppType KeyValuePair_2_t779664299_0_0_0;
extern const Il2CppType KeyValuePair_2_t3102383433_0_0_0;
extern const Il2CppType RetryCapacity_t2794668894_0_0_0;
extern const Il2CppType KeyValuePair_2_t2466793378_0_0_0;
extern const Il2CppType ParameterValue_t2426009594_0_0_0;
extern const Il2CppType KeyValuePair_2_t2098134078_0_0_0;
extern const Il2CppType Stream_t3255436806_0_0_0;
extern const Il2CppType IRuntimePipelineCustomizer_t1222812694_0_0_0;
extern const Il2CppType IExceptionHandler_t1411014698_0_0_0;
extern const Il2CppType KeyValuePair_2_t1105717817_0_0_0;
extern const Il2CppType HttpErrorResponseException_t3191555406_0_0_0;
extern const Il2CppType HttpStatusCode_t1898409641_0_0_0;
extern const Il2CppType WebExceptionStatus_t1169373531_0_0_0;
extern const Il2CppType Link_t74093617_0_0_0;
extern const Il2CppType Link_t3640024803_0_0_0;
extern const Il2CppType IAsyncExecutionContext_t3792344986_0_0_0;
extern const Il2CppType ThreadPoolOptions_1_t1583506835_0_0_0;
extern const Il2CppType ThreadPoolOptions_1_t3222238215_0_0_0;
extern const Il2CppType IUnityHttpRequest_t1859903397_0_0_0;
extern const Il2CppType RuntimeAsyncResult_t4279356013_0_0_0;
extern const Il2CppType IAsyncResult_t1999651008_0_0_0;
extern const Il2CppType HashingWrapperMD5_t3486550061_0_0_0;
extern const Il2CppType Logger_t2262497814_0_0_0;
extern const Il2CppType InternalLogger_t2972373343_0_0_0;
extern const Il2CppType ILogger_t676853571_0_0_0;
extern const Il2CppType KeyValuePair_2_t1957200933_0_0_0;
extern const Il2CppType LruListItem_2_t38602651_0_0_0;
extern const Il2CppType KeyValuePair_2_t1682975297_0_0_0;
extern const Il2CppType Metric_t3273440202_0_0_0;
extern const Il2CppType List_1_t2058570427_0_0_0;
extern const Il2CppType KeyValuePair_2_t3196873006_0_0_0;
extern const Il2CppType KeyValuePair_2_t2565994138_0_0_0;
extern const Il2CppType List_1_t3837499352_0_0_0;
extern const Il2CppType IMetricsTiming_t173410924_0_0_0;
extern const Il2CppType KeyValuePair_2_t49955767_0_0_0;
extern const Il2CppType KeyValuePair_2_t1416501748_0_0_0;
extern const Il2CppType Timing_t847194262_0_0_0;
extern const Il2CppType KeyValuePair_2_t1354617973_0_0_0;
extern const Il2CppType MetricError_t964444806_0_0_0;
extern const Il2CppType UnmarshallerContext_t1608322995_0_0_0;
extern const Il2CppType XmlUnmarshallerContext_t1179575220_0_0_0;
extern const Il2CppType JsonUnmarshallerContext_t456235889_0_0_0;
extern const Il2CppType XmlNodeType_t739504597_0_0_0;
extern const Il2CppType Link_t3210155869_0_0_0;
extern const Il2CppType EventSystem_t3466835263_0_0_0;
extern const Il2CppType UIBehaviour_t3960014691_0_0_0;
extern const Il2CppType MonoBehaviour_t1158329972_0_0_0;
extern const Il2CppType BaseInputModule_t1295781545_0_0_0;
extern const Il2CppType RaycastResult_t21186376_0_0_0;
extern const Il2CppType IDeselectHandler_t3182198310_0_0_0;
extern const Il2CppType IEventSystemHandler_t2741188318_0_0_0;
extern const Il2CppType List_1_t2110309450_0_0_0;
extern const Il2CppType List_1_t3188497603_0_0_0;
extern const Il2CppType ISelectHandler_t2812555161_0_0_0;
extern const Il2CppType BaseRaycaster_t2336171397_0_0_0;
extern const Il2CppType Entry_t3365010046_0_0_0;
extern const Il2CppType BaseEventData_t2681005625_0_0_0;
extern const Il2CppType IPointerEnterHandler_t193164956_0_0_0;
extern const Il2CppType IPointerExitHandler_t461019860_0_0_0;
extern const Il2CppType IPointerDownHandler_t3929046918_0_0_0;
extern const Il2CppType IPointerUpHandler_t1847764461_0_0_0;
extern const Il2CppType IPointerClickHandler_t96169666_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t3350809087_0_0_0;
extern const Il2CppType IBeginDragHandler_t3135127860_0_0_0;
extern const Il2CppType IDragHandler_t2583993319_0_0_0;
extern const Il2CppType IEndDragHandler_t1349123600_0_0_0;
extern const Il2CppType IDropHandler_t2390101210_0_0_0;
extern const Il2CppType IScrollHandler_t3834677510_0_0_0;
extern const Il2CppType IUpdateSelectedHandler_t3778909353_0_0_0;
extern const Il2CppType IMoveHandler_t2611925506_0_0_0;
extern const Il2CppType ISubmitHandler_t525803901_0_0_0;
extern const Il2CppType ICancelHandler_t1980319651_0_0_0;
extern const Il2CppType Transform_t3275118058_0_0_0;
extern const Il2CppType GameObject_t1756533147_0_0_0;
extern const Il2CppType BaseInput_t621514313_0_0_0;
extern const Il2CppType PointerEventData_t1599784723_0_0_0;
extern const Il2CppType AbstractEventData_t1333959294_0_0_0;
extern const Il2CppType KeyValuePair_2_t2659922876_0_0_0;
extern const Il2CppType ButtonState_t2688375492_0_0_0;
extern const Il2CppType RaycastHit2D_t4063908774_0_0_0;
extern const Il2CppType Color_t2020392075_0_0_0;
extern const Il2CppType ICanvasElement_t986520779_0_0_0;
extern const Il2CppType ColorBlock_t2652774230_0_0_0;
extern const Il2CppType OptionData_t2420267500_0_0_0;
extern const Il2CppType DropdownItem_t4139978805_0_0_0;
extern const Il2CppType FloatTween_t2986189219_0_0_0;
extern const Il2CppType Sprite_t309593783_0_0_0;
extern const Il2CppType Canvas_t209405766_0_0_0;
extern const Il2CppType List_1_t3873494194_0_0_0;
extern const Il2CppType HashSet_1_t2984649583_0_0_0;
extern const Il2CppType Text_t356221433_0_0_0;
extern const Il2CppType Link_t2826872705_0_0_0;
extern const Il2CppType ILayoutElement_t1975293769_0_0_0;
extern const Il2CppType MaskableGraphic_t540192618_0_0_0;
extern const Il2CppType IClippable_t1941276057_0_0_0;
extern const Il2CppType IMaskable_t1431842707_0_0_0;
extern const Il2CppType IMaterialModifier_t3028564983_0_0_0;
extern const Il2CppType Graphic_t2426225576_0_0_0;
extern const Il2CppType KeyValuePair_2_t850112849_0_0_0;
extern const Il2CppType ColorTween_t3438117476_0_0_0;
extern const Il2CppType IndexedSet_1_t286373651_0_0_0;
extern const Il2CppType KeyValuePair_2_t2391682566_0_0_0;
extern const Il2CppType KeyValuePair_2_t3010968081_0_0_0;
extern const Il2CppType KeyValuePair_2_t1912381698_0_0_0;
extern const Il2CppType Type_t3352948571_0_0_0;
extern const Il2CppType FillMethod_t1640962579_0_0_0;
extern const Il2CppType ContentType_t1028629049_0_0_0;
extern const Il2CppType LineType_t2931319356_0_0_0;
extern const Il2CppType InputType_t1274231802_0_0_0;
extern const Il2CppType TouchScreenKeyboardType_t875112366_0_0_0;
extern const Il2CppType CharacterValidation_t3437478890_0_0_0;
extern const Il2CppType Mask_t2977958238_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t1367822892_0_0_0;
extern const Il2CppType List_1_t2347079370_0_0_0;
extern const Il2CppType RectMask2D_t1156185964_0_0_0;
extern const Il2CppType IClipper_t900477982_0_0_0;
extern const Il2CppType List_1_t525307096_0_0_0;
extern const Il2CppType Navigation_t1571958496_0_0_0;
extern const Il2CppType Link_t116960033_0_0_0;
extern const Il2CppType Direction_t3696775921_0_0_0;
extern const Il2CppType Selectable_t1490392188_0_0_0;
extern const Il2CppType Transition_t605142169_0_0_0;
extern const Il2CppType SpriteState_t1353336012_0_0_0;
extern const Il2CppType CanvasGroup_t3296560743_0_0_0;
extern const Il2CppType Direction_t1525323322_0_0_0;
extern const Il2CppType MatEntry_t3157325053_0_0_0;
extern const Il2CppType Toggle_t3976754468_0_0_0;
extern const Il2CppType KeyValuePair_2_t379984643_0_0_0;
extern const Il2CppType AspectMode_t1166448724_0_0_0;
extern const Il2CppType FitMode_t4030874534_0_0_0;
extern const Il2CppType RectTransform_t3349966182_0_0_0;
extern const Il2CppType LayoutRebuilder_t2155218138_0_0_0;
extern const Il2CppType List_1_t1612828712_0_0_0;
extern const Il2CppType List_1_t243638650_0_0_0;
extern const Il2CppType List_1_t1612828711_0_0_0;
extern const Il2CppType List_1_t1612828713_0_0_0;
extern const Il2CppType List_1_t1440998580_0_0_0;
extern const Il2CppType List_1_t573379950_0_0_0;
extern const Il2CppType GetObjectRequest_t109865576_0_0_0;
extern const Il2CppType GetObjectResponse_t653598534_0_0_0;
extern const Il2CppType ListObjectsRequest_t3715455147_0_0_0;
extern const Il2CppType ListObjectsResponse_t2587169543_0_0_0;
extern const Il2CppType DeletedObject_t2664222218_0_0_0;
extern const Il2CppType DeleteError_t3399861423_0_0_0;
extern const Il2CppType LruListItem_2_t333395295_0_0_0;
extern const Il2CppType S3Object_t1836716019_0_0_0;
extern const Il2CppType Owner_t97740679_0_0_0;
extern const Il2CppType S3ErrorResponse_t623087499_0_0_0;
extern const Il2CppType Link_t3774454498_0_0_0;
extern const Il2CppType LinkedListNode_1_t2122577665_0_0_0;
extern const Il2CppType KeyValuePair_2_t239539626_0_0_0;
extern const Il2CppType Action_1_t2491248677_0_0_0;
extern const Il2CppType LinkedListNode_1_t1387354590_0_0_0;
extern const Il2CppType KeyValuePair_2_t3584681366_0_0_0;
extern const Il2CppType Action_2_t2572051853_0_0_0;
extern const Il2CppType LinkedListNode_1_t1468157766_0_0_0;
extern const Il2CppType KeyValuePair_2_t1008245494_0_0_0;
extern const Il2CppType Action_3_t1115657183_0_0_0;
extern const Il2CppType LinkedListNode_1_t11763096_0_0_0;
extern const Il2CppType KeyValuePair_2_t4123020654_0_0_0;
extern const Il2CppType Material_t193706927_0_0_0;
extern const Il2CppType KeyValuePair_2_t1253845080_0_0_0;
extern const Il2CppType TMP_FontAsset_t2530419979_0_0_0;
extern const Il2CppType TMP_Asset_t1084708044_0_0_0;
extern const Il2CppType KeyValuePair_2_t3590558132_0_0_0;
extern const Il2CppType TMP_SpriteAsset_t2641813093_0_0_0;
extern const Il2CppType KeyValuePair_2_t3701951246_0_0_0;
extern const Il2CppType TMP_ColorGradient_t1159837347_0_0_0;
extern const Il2CppType KeyValuePair_2_t2219975500_0_0_0;
extern const Il2CppType MaterialReference_t2854353496_0_0_0;
extern const Il2CppType TMP_SubMesh_t3507543655_0_0_0;
extern const Il2CppType TMP_MeshInfo_t1297308317_0_0_0;
extern const Il2CppType TMP_Glyph_t909793902_0_0_0;
extern const Il2CppType TMP_TextElement_t2285620223_0_0_0;
extern const Il2CppType KeyValuePair_2_t1969932055_0_0_0;
extern const Il2CppType TMP_CharacterInfo_t1421302791_0_0_0;
extern const Il2CppType TextAlignmentOptions_t1466788324_0_0_0;
extern const Il2CppType TMP_PageInfo_t3845132337_0_0_0;
extern const Il2CppType TMP_Sprite_t104383505_0_0_0;
extern const Il2CppType KerningPair_t1577753922_0_0_0;
extern const Il2CppType KeyValuePair_2_t2637892075_0_0_0;
extern const Il2CppType TMP_LineInfo_t2320418126_0_0_0;
extern const Il2CppType KeyValuePair_2_t219652195_0_0_0;
extern const Il2CppType TMP_WordInfo_t3807457612_0_0_0;
extern const Il2CppType TMP_SubMeshUI_t1983202343_0_0_0;
extern const Il2CppType Compute_DT_EventArgs_t4231491594_0_0_0;
extern const Il2CppType Action_2_t4114094152_0_0_0;
extern const Il2CppType LinkedListNode_1_t3010200065_0_0_0;
extern const Il2CppType Action_2_t2525452034_0_0_0;
extern const Il2CppType LinkedListNode_1_t1421557947_0_0_0;
extern const Il2CppType KeyValuePair_2_t2048271746_0_0_0;
extern const Il2CppType Action_2_t29709666_0_0_0;
extern const Il2CppType LinkedListNode_1_t3220782875_0_0_0;
extern const Il2CppType Action_2_t2366422718_0_0_0;
extern const Il2CppType LinkedListNode_1_t1262528631_0_0_0;
extern const Il2CppType Action_2_t857604856_0_0_0;
extern const Il2CppType LinkedListNode_1_t4048678065_0_0_0;
extern const Il2CppType TextMeshPro_t2521834357_0_0_0;
extern const Il2CppType Action_2_t2357837096_0_0_0;
extern const Il2CppType LinkedListNode_1_t1253943009_0_0_0;
extern const Il2CppType Action_3_t408506571_0_0_0;
extern const Il2CppType LinkedListNode_1_t3599579780_0_0_0;
extern const Il2CppType Action_1_t3627374100_0_0_0;
extern const Il2CppType LinkedListNode_1_t2523480013_0_0_0;
extern const Il2CppType KeyValuePair_2_t379239674_0_0_0;
extern const Il2CppType Action_1_t961636729_0_0_0;
extern const Il2CppType LinkedListNode_1_t4152709938_0_0_0;
extern const Il2CppType TextMeshProUGUI_t934157183_0_0_0;
extern const Il2CppType Action_2_t770159922_0_0_0;
extern const Il2CppType LinkedListNode_1_t3961233131_0_0_0;
extern const Il2CppType Action_1_t823401499_0_0_0;
extern const Il2CppType LinkedListNode_1_t4014474708_0_0_0;
extern const Il2CppType OptionData_t234712921_0_0_0;
extern const Il2CppType DropdownItem_t1251916390_0_0_0;
extern const Il2CppType FloatTween_t1652887471_0_0_0;
extern const Il2CppType TMP_FontWeights_t1564168302_0_0_0;
extern const Il2CppType ContentType_t4294436424_0_0_0;
extern const Il2CppType MaskingMaterial_t590070688_0_0_0;
extern const Il2CppType FallbackMaterial_t3285989240_0_0_0;
extern const Il2CppType KeyValuePair_2_t1436312919_0_0_0;
extern const Il2CppType KeyValuePair_2_t2032852864_0_0_0;
extern const Il2CppType KeyValuePair_2_t1969216190_0_0_0;
extern const Il2CppType List_1_t2397686115_0_0_0;
extern const Il2CppType KeyValuePair_2_t590745575_0_0_0;
extern const Il2CppType SpriteData_t257854902_0_0_0;
extern const Il2CppType TMP_Style_t69737451_0_0_0;
extern const Il2CppType KeyValuePair_2_t1129875604_0_0_0;
extern const Il2CppType XML_TagAttribute_t1879784992_0_0_0;
extern const Il2CppType TMP_LinkInfo_t3626894960_0_0_0;
extern const Il2CppType TMP_Text_t1920000777_0_0_0;
extern const Il2CppType StringU5BU5D_t1642385972_0_0_0;
extern const Il2CppType IList_1_t2570160834_0_0_0;
extern const Il2CppType ICollection_1_t2981295538_0_0_0;
extern const Il2CppType IEnumerable_1_t2321347278_0_0_0;
extern const Il2CppType IList_1_t1449033083_0_0_0;
extern const Il2CppType ICollection_1_t1860167787_0_0_0;
extern const Il2CppType IEnumerable_1_t1200219527_0_0_0;
extern const Il2CppType IList_1_t2398023366_0_0_0;
extern const Il2CppType ICollection_1_t2809158070_0_0_0;
extern const Il2CppType IEnumerable_1_t2149209810_0_0_0;
extern const Il2CppType IList_1_t3452350100_0_0_0;
extern const Il2CppType ICollection_1_t3863484804_0_0_0;
extern const Il2CppType IEnumerable_1_t3203536544_0_0_0;
extern const Il2CppType IList_1_t99252587_0_0_0;
extern const Il2CppType ICollection_1_t510387291_0_0_0;
extern const Il2CppType IEnumerable_1_t4145406327_0_0_0;
extern const Il2CppType IList_1_t107032761_0_0_0;
extern const Il2CppType ICollection_1_t518167465_0_0_0;
extern const Il2CppType IEnumerable_1_t4153186501_0_0_0;
extern const Il2CppType IList_1_t479175707_0_0_0;
extern const Il2CppType ICollection_1_t890310411_0_0_0;
extern const Il2CppType IEnumerable_1_t230362151_0_0_0;
extern const Il2CppType VertexAnim_t2147777005_0_0_0;
extern const Il2CppType Vector3U5BU5D_t1172311765_0_0_0;
extern const Il2CppType IList_1_t2784648181_0_0_0;
extern const Il2CppType ICollection_1_t3195782885_0_0_0;
extern const Il2CppType IEnumerable_1_t2535834625_0_0_0;
extern const Il2CppType IEnumerable_1_t4048664256_gp_0_0_0_0;
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2069139338_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1999601238_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1999601238_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m4168899348_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1711256281_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1711256281_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m714603628_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m4057794348_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m4057794348_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m2544329074_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m54102389_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m54102389_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m2673640633_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m285045864_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m965712368_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m965712368_gp_1_0_0_0;
extern const Il2CppType Array_compare_m1681301885_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m2216502740_gp_0_0_0_0;
extern const Il2CppType Array_Resize_m2875826811_gp_0_0_0_0;
extern const Il2CppType Array_TrueForAll_m1718874735_gp_0_0_0_0;
extern const Il2CppType Array_ForEach_m1854649314_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m4274024367_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m4274024367_gp_1_0_0_0;
extern const Il2CppType Array_FindLastIndex_m2314439434_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m3151928313_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m2323782676_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m1557343438_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m1631399507_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m3415315332_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m2990936537_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m2009305197_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m2427841765_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3348121441_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m2010744443_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m4126518092_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m3127862871_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m1523107937_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m2200535596_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m555775493_gp_0_0_0_0;
extern const Il2CppType Array_FindAll_m2331462890_gp_0_0_0_0;
extern const Il2CppType Array_Exists_m976807351_gp_0_0_0_0;
extern const Il2CppType Array_AsReadOnly_m884706992_gp_0_0_0_0;
extern const Il2CppType Array_Find_m1125132281_gp_0_0_0_0;
extern const Il2CppType Array_FindLast_m4092932075_gp_0_0_0_0;
extern const Il2CppType InternalEnumerator_1_t3582267753_gp_0_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0;
extern const Il2CppType IList_1_t3737699284_gp_0_0_0_0;
extern const Il2CppType ICollection_1_t1552160836_gp_0_0_0_0;
extern const Il2CppType Nullable_1_t1398937014_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t1036860714_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t3074655092_gp_0_0_0_0;
extern const Il2CppType GenericComparer_1_t1787398723_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t2276497324_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t2276497324_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t3180694294_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3895203923_gp_1_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2089681430_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t3434615342_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t1229212677_gp_1_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_0_0_0_0;
extern const Il2CppType Enumerator_t83320710_gp_1_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2262344653_gp_1_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3111723616_gp_1_0_0_0;
extern const Il2CppType EqualityComparer_1_t2066709010_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t1766400012_gp_0_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t2202941003_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t3502329323_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t4174120762_0_0_0;
extern const Il2CppType IDictionary_2_t3502329323_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1988958766_gp_1_0_0_0;
extern const Il2CppType List_1_t1169184319_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1292967705_gp_0_0_0_0;
extern const Il2CppType Collection_1_t686054069_gp_0_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t3540981679_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0;
extern const Il2CppType ArraySegment_1_t1001032761_gp_0_0_0_0;
extern const Il2CppType LinkedList_1_t3556217344_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4145643798_gp_0_0_0_0;
extern const Il2CppType LinkedListNode_1_t2172356692_gp_0_0_0_0;
extern const Il2CppType Queue_1_t1458930734_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4000919638_gp_0_0_0_0;
extern const Il2CppType RBTree_Intern_m1863612280_gp_0_0_0_0;
extern const Il2CppType RBTree_Remove_m2489856008_gp_0_0_0_0;
extern const Il2CppType RBTree_Lookup_m3821366094_gp_0_0_0_0;
extern const Il2CppType RBTree_find_key_m3679421040_gp_0_0_0_0;
extern const Il2CppType SortedDictionary_2_t3426860295_gp_0_0_0_0;
extern const Il2CppType SortedDictionary_2_t3426860295_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t4073929546_0_0_0;
extern const Il2CppType Node_t1473068371_gp_0_0_0_0;
extern const Il2CppType Node_t1473068371_gp_1_0_0_0;
extern const Il2CppType NodeHelper_t357096749_gp_0_0_0_0;
extern const Il2CppType NodeHelper_t357096749_gp_1_0_0_0;
extern const Il2CppType ValueCollection_t2735575576_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2735575576_gp_1_0_0_0;
extern const Il2CppType Enumerator_t1353355221_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1353355221_gp_1_0_0_0;
extern const Il2CppType KeyCollection_t3620397270_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t3620397270_gp_1_0_0_0;
extern const Il2CppType Enumerator_t4275149413_gp_0_0_0_0;
extern const Il2CppType Enumerator_t4275149413_gp_1_0_0_0;
extern const Il2CppType Enumerator_t824916987_gp_0_0_0_0;
extern const Il2CppType Enumerator_t824916987_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t404405498_0_0_0;
extern const Il2CppType Stack_1_t4016656541_gp_0_0_0_0;
extern const Il2CppType Enumerator_t546412149_gp_0_0_0_0;
extern const Il2CppType HashSet_1_t2624254809_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2109956843_gp_0_0_0_0;
extern const Il2CppType PrimeHelper_t3424417428_gp_0_0_0_0;
extern const Il2CppType Enumerable_Any_m1395534165_gp_0_0_0_0;
extern const Il2CppType Enumerable_Contains_m1886513113_gp_0_0_0_0;
extern const Il2CppType Enumerable_Contains_m1629730982_gp_0_0_0_0;
extern const Il2CppType Enumerable_Count_m2109803174_gp_0_0_0_0;
extern const Il2CppType Enumerable_First_m385027450_gp_0_0_0_0;
extern const Il2CppType Enumerable_First_m2440166523_gp_0_0_0_0;
extern const Il2CppType Enumerable_First_m3310377166_gp_0_0_0_0;
extern const Il2CppType Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0;
extern const Il2CppType Enumerable_Last_m1051445487_gp_0_0_0_0;
extern const Il2CppType Enumerable_OrderBy_m1178781222_gp_0_0_0_0;
extern const Il2CppType Enumerable_OrderBy_m1178781222_gp_1_0_0_0;
extern const Il2CppType Enumerable_OrderBy_m1935324147_gp_0_0_0_0;
extern const Il2CppType Enumerable_OrderBy_m1935324147_gp_1_0_0_0;
extern const Il2CppType Enumerable_Select_m791198632_gp_0_0_0_0;
extern const Il2CppType Enumerable_Select_m791198632_gp_1_0_0_0;
extern const Il2CppType Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0;
extern const Il2CppType Enumerable_Single_m2573711234_gp_0_0_0_0;
extern const Il2CppType Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0;
extern const Il2CppType Enumerable_ThenBy_m1431519242_gp_0_0_0_0;
extern const Il2CppType Enumerable_ThenBy_m1431519242_gp_1_0_0_0;
extern const Il2CppType Enumerable_ThenBy_m3860233309_gp_0_0_0_0;
extern const Il2CppType Enumerable_ThenBy_m3860233309_gp_1_0_0_0;
extern const Il2CppType Enumerable_ToArray_m747609077_gp_0_0_0_0;
extern const Il2CppType Enumerable_ToList_m2644368746_gp_0_0_0_0;
extern const Il2CppType Enumerable_Where_m1602106588_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0;
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0;
extern const Il2CppType U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0;
extern const Il2CppType IOrderedEnumerable_1_t641749975_gp_0_0_0_0;
extern const Il2CppType IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0;
extern const Il2CppType OrderedEnumerable_1_t753306046_gp_0_0_0_0;
extern const Il2CppType OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0;
extern const Il2CppType OrderedSequence_2_t1023848160_gp_0_0_0_0;
extern const Il2CppType OrderedSequence_2_t1023848160_gp_1_0_0_0;
extern const Il2CppType QuickSort_1_t1290221672_gp_0_0_0_0;
extern const Il2CppType U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0;
extern const Il2CppType SortContext_1_t4088581714_gp_0_0_0_0;
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_0_0_0_0;
extern const Il2CppType SortSequenceContext_2_t3419387730_gp_1_0_0_0;
extern const Il2CppType Component_GetComponentInChildren_m2766208945_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m3431627251_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m2508569808_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m1332352624_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m3637577495_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m1487060311_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m1502796979_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m2277214384_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentInChildren_m2459662209_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponents_m2891535275_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInChildren_m2576466756_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInParent_m2457452615_gp_0_0_0_0;
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m4152119927_gp_0_0_0_0;
extern const Il2CppType Mesh_SafeLength_m4002341710_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m3461755521_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m3202302135_gp_0_0_0_0;
extern const Il2CppType Mesh_SetUvsImpl_m311612863_gp_0_0_0_0;
extern const Il2CppType Object_Instantiate_m2665790055_gp_0_0_0_0;
extern const Il2CppType InvokableCall_1_t476640868_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t2490859068_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2042724809_gp_1_0_0_0;
extern const Il2CppType UnityAction_2_t601835599_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3608808750_gp_2_0_0_0;
extern const Il2CppType UnityAction_3_t155920421_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t879925395_gp_3_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t224769006_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t4075366602_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t4075366599_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t4075366600_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t4075366597_gp_3_0_0_0;
extern const Il2CppType Runtime_GetNSObject_m3692132737_gp_0_0_0_0;
extern const Il2CppType AWSConfigs_GetConfigEnum_m3032820163_gp_0_0_0_0;
extern const Il2CppType AWSConfigs_ParseEnum_m3861189188_gp_0_0_0_0;
extern const Il2CppType AWSConfigs_GetSection_m1660313627_gp_0_0_0_0;
extern const Il2CppType AWSSDKUtils_InvokeInBackground_m573940766_gp_0_0_0_0;
extern const Il2CppType U3CU3Ec__DisplayClass39_0_1_t500610588_gp_0_0_0_0;
extern const Il2CppType AndroidInterop_CallMethod_m713902838_gp_0_0_0_0;
extern const Il2CppType AndroidInterop_GetJavaField_m1179772153_gp_0_0_0_0;
extern const Il2CppType U3CU3Ec__2_1_t1696963520_gp_0_0_0_0;
extern const Il2CppType U3CU3Ec__6_1_t2261613524_gp_0_0_0_0;
extern const Il2CppType ServiceFactory_GetService_m2302760766_gp_0_0_0_0;
extern const Il2CppType IHttpRequestFactory_1_t4193720315_gp_0_0_0_0;
extern const Il2CppType AmazonServiceCallback_2_t3530050132_gp_0_0_0_0;
extern const Il2CppType AmazonServiceCallback_2_t3530050132_gp_1_0_0_0;
extern const Il2CppType AmazonServiceResult_2_t529501990_gp_0_0_0_0;
extern const Il2CppType AmazonServiceResult_2_t529501990_gp_1_0_0_0;
extern const Il2CppType ExceptionHandler_1_t3179100912_gp_0_0_0_0;
extern const Il2CppType HttpHandler_1_t1383532101_gp_0_0_0_0;
extern const Il2CppType DefaultRetryPolicy_IsInnerException_m149306988_gp_0_0_0_0;
extern const Il2CppType BackgroundDispatcher_1_t295417742_gp_0_0_0_0;
extern const Il2CppType HashStream_1_t458673105_gp_0_0_0_0;
extern const Il2CppType LruCache_2_t2774710777_gp_0_0_0_0;
extern const Il2CppType LruListItem_2_t3884836584_0_0_0;
extern const Il2CppType LruCache_2_t2774710777_gp_1_0_0_0;
extern const Il2CppType LruList_2_t2819118215_gp_0_0_0_0;
extern const Il2CppType LruList_2_t2819118215_gp_1_0_0_0;
extern const Il2CppType LruListItem_2_t4061194576_gp_0_0_0_0;
extern const Il2CppType LruListItem_2_t4061194576_gp_1_0_0_0;
extern const Il2CppType ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0;
extern const Il2CppType ThreadPoolOptions_1_t170443295_0_0_0;
extern const Il2CppType ThreadPoolOptions_1_t2545192696_gp_1_0_0_0;
extern const Il2CppType IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0;
extern const Il2CppType IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0;
extern const Il2CppType UnmarshallerExtensions_Add_m421616549_gp_0_0_0_0;
extern const Il2CppType UnmarshallerExtensions_Add_m421616549_gp_1_0_0_0;
extern const Il2CppType ExecuteEvents_Execute_m1744114673_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0;
extern const Il2CppType TweenRunner_1_t2584777480_gp_0_0_0_0;
extern const Il2CppType Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t573160278_gp_0_0_0_0;
extern const Il2CppType ListPool_1_t1984115411_gp_0_0_0_0;
extern const Il2CppType List_1_t2000868992_0_0_0;
extern const Il2CppType ObjectPool_1_t4265859154_gp_0_0_0_0;
extern const Il2CppType FastAction_1_t1338576437_gp_0_0_0_0;
extern const Il2CppType Action_1_t1788008268_0_0_0;
extern const Il2CppType LinkedListNode_1_t684114181_0_0_0;
extern const Il2CppType FastAction_2_t1741860964_gp_0_0_0_0;
extern const Il2CppType FastAction_2_t1741860964_gp_1_0_0_0;
extern const Il2CppType Action_2_t3375677134_0_0_0;
extern const Il2CppType LinkedListNode_1_t2271783047_0_0_0;
extern const Il2CppType FastAction_3_t175777023_gp_0_0_0_0;
extern const Il2CppType FastAction_3_t175777023_gp_1_0_0_0;
extern const Il2CppType FastAction_3_t175777023_gp_2_0_0_0;
extern const Il2CppType Action_3_t3712260761_0_0_0;
extern const Il2CppType LinkedListNode_1_t2608366674_0_0_0;
extern const Il2CppType TMPro_ExtensionMethods_FindInstanceID_m238029140_gp_0_0_0_0;
extern const Il2CppType TweenRunner_1_t579810420_gp_0_0_0_0;
extern const Il2CppType TMP_Dropdown_GetOrAddComponent_m214373678_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetEquatableStruct_m1986669591_gp_0_0_0_0;
extern const Il2CppType TMP_ListPool_1_t700915143_gp_0_0_0_0;
extern const Il2CppType List_1_t717668724_0_0_0;
extern const Il2CppType TMP_ObjectPool_1_t3742925414_gp_0_0_0_0;
extern const Il2CppType TMP_Text_ResizeInternalArray_m2848200466_gp_0_0_0_0;
extern const Il2CppType TMP_TextInfo_Resize_m2071664037_gp_0_0_0_0;
extern const Il2CppType TMP_TextInfo_Resize_m3363474848_gp_0_0_0_0;
extern const Il2CppType TMP_XmlTagStack_1_t558189074_gp_0_0_0_0;
extern const Il2CppType DefaultExecutionOrder_t2717914595_0_0_0;
extern const Il2CppType PlayerConnection_t3517219175_0_0_0;
extern const Il2CppType GUILayer_t3254902478_0_0_0;
extern const Il2CppType LoggingOptions_t2865640765_0_0_0;
extern const Il2CppType ResponseLoggingOption_t3443611737_0_0_0;
extern const Il2CppType ThreadAbortException_t1150575753_0_0_0;
extern const Il2CppType WebException_t3368933679_0_0_0;
extern const Il2CppType List_1_t1398341365_0_0_0;
extern const Il2CppType INetworkReachability_t2670889314_0_0_0;
extern const Il2CppType UnityInitializer_t2778189483_0_0_0;
extern const Il2CppType UnityMainThreadDispatcher_t4098072663_0_0_0;
extern const Il2CppType NSDictionary_t3598984691_0_0_0;
extern const Il2CppType IEnvironmentInfo_t2314249358_0_0_0;
extern const Il2CppType AWSSection_t3096528870_0_0_0;
extern const Il2CppType NSBundle_t3599697655_0_0_0;
extern const Il2CppType NSObject_t1518098886_0_0_0;
extern const Il2CppType NSLocale_t2224424797_0_0_0;
extern const Il2CppType UIDevice_t860038178_0_0_0;
extern const Il2CppType AxisEventData_t1524870173_0_0_0;
extern const Il2CppType SpriteRenderer_t1209076198_0_0_0;
extern const Il2CppType Image_t2042527209_0_0_0;
extern const Il2CppType Button_t2872111280_0_0_0;
extern const Il2CppType RawImage_t2749640213_0_0_0;
extern const Il2CppType Slider_t297367283_0_0_0;
extern const Il2CppType Scrollbar_t3248359358_0_0_0;
extern const Il2CppType InputField_t1631627530_0_0_0;
extern const Il2CppType ScrollRect_t1199013257_0_0_0;
extern const Il2CppType Dropdown_t1985816271_0_0_0;
extern const Il2CppType GraphicRaycaster_t410733016_0_0_0;
extern const Il2CppType CanvasRenderer_t261436805_0_0_0;
extern const Il2CppType Corner_t1077473318_0_0_0;
extern const Il2CppType Axis_t1431825778_0_0_0;
extern const Il2CppType Constraint_t3558160636_0_0_0;
extern const Il2CppType SubmitEvent_t907918422_0_0_0;
extern const Il2CppType OnChangeEvent_t2863344003_0_0_0;
extern const Il2CppType OnValidateInput_t1946318473_0_0_0;
extern const Il2CppType LayoutElement_t2808691390_0_0_0;
extern const Il2CppType RectOffset_t3387826427_0_0_0;
extern const Il2CppType TextAnchor_t112990806_0_0_0;
extern const Il2CppType AnimationTriggers_t3244928895_0_0_0;
extern const Il2CppType Animator_t69676727_0_0_0;
extern const Il2CppType Marshaller_t3371889241_0_0_0;
extern const Il2CppType EndpointResolver_t2369326703_0_0_0;
extern const Il2CppType Unmarshaller_t1994457618_0_0_0;
extern const Il2CppType ErrorCallbackHandler_t394665619_0_0_0;
extern const Il2CppType RetryHandler_t3257318146_0_0_0;
extern const Il2CppType ReplicationStatus_t1991002226_0_0_0;
extern const Il2CppType RequestCharged_t2438105727_0_0_0;
extern const Il2CppType S3Region_t723338532_0_0_0;
extern const Il2CppType S3StorageClass_t454477475_0_0_0;
extern const Il2CppType ServerSideEncryptionCustomerMethod_t3201425490_0_0_0;
extern const Il2CppType ServerSideEncryptionMethod_t608782770_0_0_0;
extern const Il2CppType V4ClientSection_t2149061698_0_0_0;
extern const Il2CppType Renderer_t257310565_0_0_0;
extern const Il2CppType MeshFilter_t3026937449_0_0_0;
extern const Il2CppType TMP_InputField_t1778301588_0_0_0;
extern const Il2CppType TMP_Dropdown_t1768193147_0_0_0;
extern const Il2CppType SubmitEvent_t3359162065_0_0_0;
extern const Il2CppType SelectionEvent_t3883897865_0_0_0;
extern const Il2CppType TextSelectionEvent_t3887183206_0_0_0;
extern const Il2CppType OnChangeEvent_t2139251414_0_0_0;
extern const Il2CppType OnValidateInput_t3285190392_0_0_0;
extern const Il2CppType LineType_t2475898675_0_0_0;
extern const Il2CppType InputType_t379073331_0_0_0;
extern const Il2CppType CharacterValidation_t487603955_0_0_0;
extern const Il2CppType TMP_InputValidator_t3726817866_0_0_0;
extern const Il2CppType TMP_SelectionCaret_t3802037885_0_0_0;
extern const Il2CppType BoxCollider_t22920061_0_0_0;
extern const Il2CppType Rigidbody_t4233889191_0_0_0;
extern const Il2CppType TMP_SpriteAnimator_t2347923044_0_0_0;
extern const Il2CppType EntryLink_t1717819552_0_0_0;
extern const Il2CppType PathCreator_t1368626459_0_0_0;
extern const Il2CppType RoadCreator_t1201856826_0_0_0;
extern const Il2CppType MeshRenderer_t1268241104_0_0_0;
extern const Il2CppType RacingWidgets_t915614521_0_0_0;
extern const Il2CppType TextMesh_t1641806576_0_0_0;
extern const Il2CppType TextMeshProFloatingText_t6181308_0_0_0;
extern const Il2CppType Light_t494725636_0_0_0;
extern const Il2CppType TextContainer_t4263764796_0_0_0;
extern const Il2CppType LoggingSection_t905443946_0_0_0;




static const RuntimeType* GenInst_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0 = { 1, GenInst_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0 = { 1, GenInst_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Char_t3454481338_0_0_0_Types[] = { (&Char_t3454481338_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0 = { 1, GenInst_Char_t3454481338_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_Types[] = { (&Int64_t909078037_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0 = { 1, GenInst_Int64_t909078037_0_0_0_Types };
static const RuntimeType* GenInst_UInt32_t2149682021_0_0_0_Types[] = { (&UInt32_t2149682021_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt32_t2149682021_0_0_0 = { 1, GenInst_UInt32_t2149682021_0_0_0_Types };
static const RuntimeType* GenInst_UInt64_t2909196914_0_0_0_Types[] = { (&UInt64_t2909196914_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt64_t2909196914_0_0_0 = { 1, GenInst_UInt64_t2909196914_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t3683104436_0_0_0_Types[] = { (&Byte_t3683104436_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0 = { 1, GenInst_Byte_t3683104436_0_0_0_Types };
static const RuntimeType* GenInst_SByte_t454417549_0_0_0_Types[] = { (&SByte_t454417549_0_0_0) };
extern const Il2CppGenericInst GenInst_SByte_t454417549_0_0_0 = { 1, GenInst_SByte_t454417549_0_0_0_Types };
static const RuntimeType* GenInst_Int16_t4041245914_0_0_0_Types[] = { (&Int16_t4041245914_0_0_0) };
extern const Il2CppGenericInst GenInst_Int16_t4041245914_0_0_0 = { 1, GenInst_Int16_t4041245914_0_0_0_Types };
static const RuntimeType* GenInst_UInt16_t986882611_0_0_0_Types[] = { (&UInt16_t986882611_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt16_t986882611_0_0_0 = { 1, GenInst_UInt16_t986882611_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Types[] = { (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
static const RuntimeType* GenInst_IConvertible_t908092482_0_0_0_Types[] = { (&IConvertible_t908092482_0_0_0) };
extern const Il2CppGenericInst GenInst_IConvertible_t908092482_0_0_0 = { 1, GenInst_IConvertible_t908092482_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_t1857082765_0_0_0_Types[] = { (&IComparable_t1857082765_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_t1857082765_0_0_0 = { 1, GenInst_IComparable_t1857082765_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_t2911409499_0_0_0_Types[] = { (&IEnumerable_t2911409499_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_t2911409499_0_0_0 = { 1, GenInst_IEnumerable_t2911409499_0_0_0_Types };
static const RuntimeType* GenInst_ICloneable_t3853279282_0_0_0_Types[] = { (&ICloneable_t3853279282_0_0_0) };
extern const Il2CppGenericInst GenInst_ICloneable_t3853279282_0_0_0 = { 1, GenInst_ICloneable_t3853279282_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_1_t3861059456_0_0_0_Types[] = { (&IComparable_1_t3861059456_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_1_t3861059456_0_0_0 = { 1, GenInst_IComparable_1_t3861059456_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t4233202402_0_0_0_Types[] = { (&IEquatable_1_t4233202402_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t4233202402_0_0_0 = { 1, GenInst_IEquatable_1_t4233202402_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Types[] = { (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_IReflect_t3412036974_0_0_0_Types[] = { (&IReflect_t3412036974_0_0_0) };
extern const Il2CppGenericInst GenInst_IReflect_t3412036974_0_0_0 = { 1, GenInst_IReflect_t3412036974_0_0_0_Types };
static const RuntimeType* GenInst__Type_t102776839_0_0_0_Types[] = { (&_Type_t102776839_0_0_0) };
extern const Il2CppGenericInst GenInst__Type_t102776839_0_0_0 = { 1, GenInst__Type_t102776839_0_0_0_Types };
static const RuntimeType* GenInst_MemberInfo_t_0_0_0_Types[] = { (&MemberInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
static const RuntimeType* GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types[] = { (&ICustomAttributeProvider_t502202687_0_0_0) };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t502202687_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t502202687_0_0_0_Types };
static const RuntimeType* GenInst__MemberInfo_t332722161_0_0_0_Types[] = { (&_MemberInfo_t332722161_0_0_0) };
extern const Il2CppGenericInst GenInst__MemberInfo_t332722161_0_0_0 = { 1, GenInst__MemberInfo_t332722161_0_0_0_Types };
static const RuntimeType* GenInst_Double_t4078015681_0_0_0_Types[] = { (&Double_t4078015681_0_0_0) };
extern const Il2CppGenericInst GenInst_Double_t4078015681_0_0_0 = { 1, GenInst_Double_t4078015681_0_0_0_Types };
static const RuntimeType* GenInst_Single_t2076509932_0_0_0_Types[] = { (&Single_t2076509932_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0 = { 1, GenInst_Single_t2076509932_0_0_0_Types };
static const RuntimeType* GenInst_Decimal_t724701077_0_0_0_Types[] = { (&Decimal_t724701077_0_0_0) };
extern const Il2CppGenericInst GenInst_Decimal_t724701077_0_0_0 = { 1, GenInst_Decimal_t724701077_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0 = { 1, GenInst_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Delegate_t3022476291_0_0_0_Types[] = { (&Delegate_t3022476291_0_0_0) };
extern const Il2CppGenericInst GenInst_Delegate_t3022476291_0_0_0 = { 1, GenInst_Delegate_t3022476291_0_0_0_Types };
static const RuntimeType* GenInst_ISerializable_t1245643778_0_0_0_Types[] = { (&ISerializable_t1245643778_0_0_0) };
extern const Il2CppGenericInst GenInst_ISerializable_t1245643778_0_0_0 = { 1, GenInst_ISerializable_t1245643778_0_0_0_Types };
static const RuntimeType* GenInst_ParameterInfo_t2249040075_0_0_0_Types[] = { (&ParameterInfo_t2249040075_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterInfo_t2249040075_0_0_0 = { 1, GenInst_ParameterInfo_t2249040075_0_0_0_Types };
static const RuntimeType* GenInst__ParameterInfo_t470209990_0_0_0_Types[] = { (&_ParameterInfo_t470209990_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterInfo_t470209990_0_0_0 = { 1, GenInst__ParameterInfo_t470209990_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_FieldInfo_t_0_0_0_Types[] = { (&FieldInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__FieldInfo_t2511231167_0_0_0_Types[] = { (&_FieldInfo_t2511231167_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldInfo_t2511231167_0_0_0 = { 1, GenInst__FieldInfo_t2511231167_0_0_0_Types };
static const RuntimeType* GenInst_ParameterModifier_t1820634920_0_0_0_Types[] = { (&ParameterModifier_t1820634920_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1820634920_0_0_0 = { 1, GenInst_ParameterModifier_t1820634920_0_0_0_Types };
static const RuntimeType* GenInst_MethodInfo_t_0_0_0_Types[] = { (&MethodInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__MethodInfo_t3642518830_0_0_0_Types[] = { (&_MethodInfo_t3642518830_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodInfo_t3642518830_0_0_0 = { 1, GenInst__MethodInfo_t3642518830_0_0_0_Types };
static const RuntimeType* GenInst_MethodBase_t904190842_0_0_0_Types[] = { (&MethodBase_t904190842_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBase_t904190842_0_0_0 = { 1, GenInst_MethodBase_t904190842_0_0_0_Types };
static const RuntimeType* GenInst__MethodBase_t1935530873_0_0_0_Types[] = { (&_MethodBase_t1935530873_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBase_t1935530873_0_0_0 = { 1, GenInst__MethodBase_t1935530873_0_0_0_Types };
static const RuntimeType* GenInst_PropertyInfo_t_0_0_0_Types[] = { (&PropertyInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__PropertyInfo_t1567586598_0_0_0_Types[] = { (&_PropertyInfo_t1567586598_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1567586598_0_0_0 = { 1, GenInst__PropertyInfo_t1567586598_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorInfo_t2851816542_0_0_0_Types[] = { (&ConstructorInfo_t2851816542_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t2851816542_0_0_0 = { 1, GenInst_ConstructorInfo_t2851816542_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorInfo_t3269099341_0_0_0_Types[] = { (&_ConstructorInfo_t3269099341_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3269099341_0_0_0 = { 1, GenInst__ConstructorInfo_t3269099341_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_TableRange_t2011406615_0_0_0_Types[] = { (&TableRange_t2011406615_0_0_0) };
extern const Il2CppGenericInst GenInst_TableRange_t2011406615_0_0_0 = { 1, GenInst_TableRange_t2011406615_0_0_0_Types };
static const RuntimeType* GenInst_TailoringInfo_t1449609243_0_0_0_Types[] = { (&TailoringInfo_t1449609243_0_0_0) };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1449609243_0_0_0 = { 1, GenInst_TailoringInfo_t1449609243_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3716250094_0_0_0_Types[] = { (&KeyValuePair_2_t3716250094_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0 = { 1, GenInst_KeyValuePair_2_t3716250094_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2723257478_0_0_0_Types[] = { (&Link_t2723257478_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2723257478_0_0_0 = { 1, GenInst_Link_t2723257478_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0 = { 1, GenInst_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t3716250094_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1744001932_0_0_0_Types[] = { (&KeyValuePair_2_t1744001932_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1744001932_0_0_0 = { 1, GenInst_KeyValuePair_2_t1744001932_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t1744001932_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0_Types };
static const RuntimeType* GenInst_Contraction_t1673853792_0_0_0_Types[] = { (&Contraction_t1673853792_0_0_0) };
extern const Il2CppGenericInst GenInst_Contraction_t1673853792_0_0_0 = { 1, GenInst_Contraction_t1673853792_0_0_0_Types };
static const RuntimeType* GenInst_Level2Map_t3322505726_0_0_0_Types[] = { (&Level2Map_t3322505726_0_0_0) };
extern const Il2CppGenericInst GenInst_Level2Map_t3322505726_0_0_0 = { 1, GenInst_Level2Map_t3322505726_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t925946152_0_0_0_Types[] = { (&BigInteger_t925946152_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t925946152_0_0_0 = { 1, GenInst_BigInteger_t925946152_0_0_0_Types };
static const RuntimeType* GenInst_KeySizes_t3144736271_0_0_0_Types[] = { (&KeySizes_t3144736271_0_0_0) };
extern const Il2CppGenericInst GenInst_KeySizes_t3144736271_0_0_0 = { 1, GenInst_KeySizes_t3144736271_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t38854645_0_0_0_Types[] = { (&KeyValuePair_2_t38854645_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0 = { 1, GenInst_KeyValuePair_2_t38854645_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t38854645_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t2022531261_0_0_0_Types[] = { (&Slot_t2022531261_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t2022531261_0_0_0 = { 1, GenInst_Slot_t2022531261_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t2267560602_0_0_0_Types[] = { (&Slot_t2267560602_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t2267560602_0_0_0 = { 1, GenInst_Slot_t2267560602_0_0_0_Types };
static const RuntimeType* GenInst_StackFrame_t2050294881_0_0_0_Types[] = { (&StackFrame_t2050294881_0_0_0) };
extern const Il2CppGenericInst GenInst_StackFrame_t2050294881_0_0_0 = { 1, GenInst_StackFrame_t2050294881_0_0_0_Types };
static const RuntimeType* GenInst_Calendar_t585061108_0_0_0_Types[] = { (&Calendar_t585061108_0_0_0) };
extern const Il2CppGenericInst GenInst_Calendar_t585061108_0_0_0 = { 1, GenInst_Calendar_t585061108_0_0_0_Types };
static const RuntimeType* GenInst_ModuleBuilder_t4156028127_0_0_0_Types[] = { (&ModuleBuilder_t4156028127_0_0_0) };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t4156028127_0_0_0 = { 1, GenInst_ModuleBuilder_t4156028127_0_0_0_Types };
static const RuntimeType* GenInst__ModuleBuilder_t1075102050_0_0_0_Types[] = { (&_ModuleBuilder_t1075102050_0_0_0) };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1075102050_0_0_0 = { 1, GenInst__ModuleBuilder_t1075102050_0_0_0_Types };
static const RuntimeType* GenInst_Module_t4282841206_0_0_0_Types[] = { (&Module_t4282841206_0_0_0) };
extern const Il2CppGenericInst GenInst_Module_t4282841206_0_0_0 = { 1, GenInst_Module_t4282841206_0_0_0_Types };
static const RuntimeType* GenInst__Module_t2144668161_0_0_0_Types[] = { (&_Module_t2144668161_0_0_0) };
extern const Il2CppGenericInst GenInst__Module_t2144668161_0_0_0 = { 1, GenInst__Module_t2144668161_0_0_0_Types };
static const RuntimeType* GenInst_ParameterBuilder_t3344728474_0_0_0_Types[] = { (&ParameterBuilder_t3344728474_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t3344728474_0_0_0 = { 1, GenInst_ParameterBuilder_t3344728474_0_0_0_Types };
static const RuntimeType* GenInst__ParameterBuilder_t2251638747_0_0_0_Types[] = { (&_ParameterBuilder_t2251638747_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2251638747_0_0_0 = { 1, GenInst__ParameterBuilder_t2251638747_0_0_0_Types };
static const RuntimeType* GenInst_TypeU5BU5D_t1664964607_0_0_0_Types[] = { (&TypeU5BU5D_t1664964607_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t1664964607_0_0_0 = { 1, GenInst_TypeU5BU5D_t1664964607_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeArray_0_0_0_Types[] = { (&RuntimeArray_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeArray_0_0_0 = { 1, GenInst_RuntimeArray_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_t91669223_0_0_0_Types[] = { (&ICollection_t91669223_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_t91669223_0_0_0 = { 1, GenInst_ICollection_t91669223_0_0_0_Types };
static const RuntimeType* GenInst_IList_t3321498491_0_0_0_Types[] = { (&IList_t3321498491_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_t3321498491_0_0_0 = { 1, GenInst_IList_t3321498491_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1844743827_0_0_0_Types[] = { (&IList_1_t1844743827_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1844743827_0_0_0 = { 1, GenInst_IList_1_t1844743827_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2255878531_0_0_0_Types[] = { (&ICollection_1_t2255878531_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2255878531_0_0_0 = { 1, GenInst_ICollection_1_t2255878531_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1595930271_0_0_0_Types[] = { (&IEnumerable_1_t1595930271_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1595930271_0_0_0 = { 1, GenInst_IEnumerable_1_t1595930271_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3952977575_0_0_0_Types[] = { (&IList_1_t3952977575_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3952977575_0_0_0 = { 1, GenInst_IList_1_t3952977575_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t69144983_0_0_0_Types[] = { (&ICollection_1_t69144983_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t69144983_0_0_0 = { 1, GenInst_ICollection_1_t69144983_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3704164019_0_0_0_Types[] = { (&IEnumerable_1_t3704164019_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3704164019_0_0_0 = { 1, GenInst_IEnumerable_1_t3704164019_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t643717440_0_0_0_Types[] = { (&IList_1_t643717440_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t643717440_0_0_0 = { 1, GenInst_IList_1_t643717440_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1054852144_0_0_0_Types[] = { (&ICollection_1_t1054852144_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1054852144_0_0_0 = { 1, GenInst_ICollection_1_t1054852144_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t394903884_0_0_0_Types[] = { (&IEnumerable_1_t394903884_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t394903884_0_0_0 = { 1, GenInst_IEnumerable_1_t394903884_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t289070565_0_0_0_Types[] = { (&IList_1_t289070565_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t289070565_0_0_0 = { 1, GenInst_IList_1_t289070565_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t700205269_0_0_0_Types[] = { (&ICollection_1_t700205269_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t700205269_0_0_0 = { 1, GenInst_ICollection_1_t700205269_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t40257009_0_0_0_Types[] = { (&IEnumerable_1_t40257009_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t40257009_0_0_0 = { 1, GenInst_IEnumerable_1_t40257009_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1043143288_0_0_0_Types[] = { (&IList_1_t1043143288_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1043143288_0_0_0 = { 1, GenInst_IList_1_t1043143288_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1454277992_0_0_0_Types[] = { (&ICollection_1_t1454277992_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1454277992_0_0_0 = { 1, GenInst_ICollection_1_t1454277992_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t794329732_0_0_0_Types[] = { (&IEnumerable_1_t794329732_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t794329732_0_0_0 = { 1, GenInst_IEnumerable_1_t794329732_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t873662762_0_0_0_Types[] = { (&IList_1_t873662762_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t873662762_0_0_0 = { 1, GenInst_IList_1_t873662762_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1284797466_0_0_0_Types[] = { (&ICollection_1_t1284797466_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1284797466_0_0_0 = { 1, GenInst_ICollection_1_t1284797466_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t624849206_0_0_0_Types[] = { (&IEnumerable_1_t624849206_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t624849206_0_0_0 = { 1, GenInst_IEnumerable_1_t624849206_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3230389896_0_0_0_Types[] = { (&IList_1_t3230389896_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3230389896_0_0_0 = { 1, GenInst_IList_1_t3230389896_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3641524600_0_0_0_Types[] = { (&ICollection_1_t3641524600_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3641524600_0_0_0 = { 1, GenInst_ICollection_1_t3641524600_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2981576340_0_0_0_Types[] = { (&IEnumerable_1_t2981576340_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2981576340_0_0_0 = { 1, GenInst_IEnumerable_1_t2981576340_0_0_0_Types };
static const RuntimeType* GenInst_ILTokenInfo_t149559338_0_0_0_Types[] = { (&ILTokenInfo_t149559338_0_0_0) };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t149559338_0_0_0 = { 1, GenInst_ILTokenInfo_t149559338_0_0_0_Types };
static const RuntimeType* GenInst_LabelData_t3712112744_0_0_0_Types[] = { (&LabelData_t3712112744_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelData_t3712112744_0_0_0 = { 1, GenInst_LabelData_t3712112744_0_0_0_Types };
static const RuntimeType* GenInst_LabelFixup_t4090909514_0_0_0_Types[] = { (&LabelFixup_t4090909514_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelFixup_t4090909514_0_0_0 = { 1, GenInst_LabelFixup_t4090909514_0_0_0_Types };
static const RuntimeType* GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types[] = { (&GenericTypeParameterBuilder_t1370236603_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeBuilder_t2970303184_0_0_0_Types[] = { (&CustomAttributeBuilder_t2970303184_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeBuilder_t2970303184_0_0_0 = { 1, GenInst_CustomAttributeBuilder_t2970303184_0_0_0_Types };
static const RuntimeType* GenInst__CustomAttributeBuilder_t3917123293_0_0_0_Types[] = { (&_CustomAttributeBuilder_t3917123293_0_0_0) };
extern const Il2CppGenericInst GenInst__CustomAttributeBuilder_t3917123293_0_0_0 = { 1, GenInst__CustomAttributeBuilder_t3917123293_0_0_0_Types };
static const RuntimeType* GenInst_RefEmitPermissionSet_t2708608433_0_0_0_Types[] = { (&RefEmitPermissionSet_t2708608433_0_0_0) };
extern const Il2CppGenericInst GenInst_RefEmitPermissionSet_t2708608433_0_0_0 = { 1, GenInst_RefEmitPermissionSet_t2708608433_0_0_0_Types };
static const RuntimeType* GenInst_TypeBuilder_t3308873219_0_0_0_Types[] = { (&TypeBuilder_t3308873219_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeBuilder_t3308873219_0_0_0 = { 1, GenInst_TypeBuilder_t3308873219_0_0_0_Types };
static const RuntimeType* GenInst__TypeBuilder_t2783404358_0_0_0_Types[] = { (&_TypeBuilder_t2783404358_0_0_0) };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2783404358_0_0_0 = { 1, GenInst__TypeBuilder_t2783404358_0_0_0_Types };
static const RuntimeType* GenInst_MethodBuilder_t644187984_0_0_0_Types[] = { (&MethodBuilder_t644187984_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBuilder_t644187984_0_0_0 = { 1, GenInst_MethodBuilder_t644187984_0_0_0_Types };
static const RuntimeType* GenInst__MethodBuilder_t3932949077_0_0_0_Types[] = { (&_MethodBuilder_t3932949077_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3932949077_0_0_0 = { 1, GenInst__MethodBuilder_t3932949077_0_0_0_Types };
static const RuntimeType* GenInst_FieldBuilder_t2784804005_0_0_0_Types[] = { (&FieldBuilder_t2784804005_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2784804005_0_0_0 = { 1, GenInst_FieldBuilder_t2784804005_0_0_0_Types };
static const RuntimeType* GenInst__FieldBuilder_t1895266044_0_0_0_Types[] = { (&_FieldBuilder_t1895266044_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1895266044_0_0_0 = { 1, GenInst__FieldBuilder_t1895266044_0_0_0_Types };
static const RuntimeType* GenInst_MonoResource_t3127387157_0_0_0_Types[] = { (&MonoResource_t3127387157_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoResource_t3127387157_0_0_0 = { 1, GenInst_MonoResource_t3127387157_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorBuilder_t700974433_0_0_0_Types[] = { (&ConstructorBuilder_t700974433_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t700974433_0_0_0 = { 1, GenInst_ConstructorBuilder_t700974433_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorBuilder_t1236878896_0_0_0_Types[] = { (&_ConstructorBuilder_t1236878896_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1236878896_0_0_0 = { 1, GenInst__ConstructorBuilder_t1236878896_0_0_0_Types };
static const RuntimeType* GenInst_PropertyBuilder_t3694255912_0_0_0_Types[] = { (&PropertyBuilder_t3694255912_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t3694255912_0_0_0 = { 1, GenInst_PropertyBuilder_t3694255912_0_0_0_Types };
static const RuntimeType* GenInst__PropertyBuilder_t3341912621_0_0_0_Types[] = { (&_PropertyBuilder_t3341912621_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t3341912621_0_0_0 = { 1, GenInst__PropertyBuilder_t3341912621_0_0_0_Types };
static const RuntimeType* GenInst_EventBuilder_t2927243889_0_0_0_Types[] = { (&EventBuilder_t2927243889_0_0_0) };
extern const Il2CppGenericInst GenInst_EventBuilder_t2927243889_0_0_0 = { 1, GenInst_EventBuilder_t2927243889_0_0_0_Types };
static const RuntimeType* GenInst__EventBuilder_t801715496_0_0_0_Types[] = { (&_EventBuilder_t801715496_0_0_0) };
extern const Il2CppGenericInst GenInst__EventBuilder_t801715496_0_0_0 = { 1, GenInst__EventBuilder_t801715496_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t1498197914_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t94157543_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeData_t3093286891_0_0_0_Types[] = { (&CustomAttributeData_t3093286891_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3093286891_0_0_0 = { 1, GenInst_CustomAttributeData_t3093286891_0_0_0_Types };
static const RuntimeType* GenInst_ResourceInfo_t3933049236_0_0_0_Types[] = { (&ResourceInfo_t3933049236_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceInfo_t3933049236_0_0_0 = { 1, GenInst_ResourceInfo_t3933049236_0_0_0_Types };
static const RuntimeType* GenInst_ResourceCacheItem_t333236149_0_0_0_Types[] = { (&ResourceCacheItem_t333236149_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t333236149_0_0_0 = { 1, GenInst_ResourceCacheItem_t333236149_0_0_0_Types };
static const RuntimeType* GenInst_IContextProperty_t287246399_0_0_0_Types[] = { (&IContextProperty_t287246399_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextProperty_t287246399_0_0_0 = { 1, GenInst_IContextProperty_t287246399_0_0_0_Types };
static const RuntimeType* GenInst_Header_t2756440555_0_0_0_Types[] = { (&Header_t2756440555_0_0_0) };
extern const Il2CppGenericInst GenInst_Header_t2756440555_0_0_0 = { 1, GenInst_Header_t2756440555_0_0_0_Types };
static const RuntimeType* GenInst_ITrackingHandler_t2759960940_0_0_0_Types[] = { (&ITrackingHandler_t2759960940_0_0_0) };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t2759960940_0_0_0 = { 1, GenInst_ITrackingHandler_t2759960940_0_0_0_Types };
static const RuntimeType* GenInst_IContextAttribute_t2439121372_0_0_0_Types[] = { (&IContextAttribute_t2439121372_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextAttribute_t2439121372_0_0_0 = { 1, GenInst_IContextAttribute_t2439121372_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t693205669_0_0_0_Types[] = { (&DateTime_t693205669_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0 = { 1, GenInst_DateTime_t693205669_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t3430258949_0_0_0_Types[] = { (&TimeSpan_t3430258949_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0 = { 1, GenInst_TimeSpan_t3430258949_0_0_0_Types };
static const RuntimeType* GenInst_TypeTag_t141209596_0_0_0_Types[] = { (&TypeTag_t141209596_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeTag_t141209596_0_0_0 = { 1, GenInst_TypeTag_t141209596_0_0_0_Types };
static const RuntimeType* GenInst_MonoType_t_0_0_0_Types[] = { (&MonoType_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
static const RuntimeType* GenInst_StrongName_t2988747270_0_0_0_Types[] = { (&StrongName_t2988747270_0_0_0) };
extern const Il2CppGenericInst GenInst_StrongName_t2988747270_0_0_0 = { 1, GenInst_StrongName_t2988747270_0_0_0_Types };
static const RuntimeType* GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types[] = { (&IBuiltInEvidence_t1114073477_0_0_0) };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t1114073477_0_0_0 = { 1, GenInst_IBuiltInEvidence_t1114073477_0_0_0_Types };
static const RuntimeType* GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types[] = { (&IIdentityPermissionFactory_t2988326850_0_0_0) };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t2988326850_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t2988326850_0_0_0_Types };
static const RuntimeType* GenInst_DateTimeOffset_t1362988906_0_0_0_Types[] = { (&DateTimeOffset_t1362988906_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1362988906_0_0_0 = { 1, GenInst_DateTimeOffset_t1362988906_0_0_0_Types };
static const RuntimeType* GenInst_Guid_t_0_0_0_Types[] = { (&Guid_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
static const RuntimeType* GenInst_Version_t1755874712_0_0_0_Types[] = { (&Version_t1755874712_0_0_0) };
extern const Il2CppGenericInst GenInst_Version_t1755874712_0_0_0 = { 1, GenInst_Version_t1755874712_0_0_0_Types };
static const RuntimeType* GenInst_Node_t2499136326_0_0_0_Types[] = { (&Node_t2499136326_0_0_0) };
extern const Il2CppGenericInst GenInst_Node_t2499136326_0_0_0 = { 1, GenInst_Node_t2499136326_0_0_0_Types };
static const RuntimeType* GenInst_X509ChainStatus_t4278378721_0_0_0_Types[] = { (&X509ChainStatus_t4278378721_0_0_0) };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t4278378721_0_0_0 = { 1, GenInst_X509ChainStatus_t4278378721_0_0_0_Types };
static const RuntimeType* GenInst_IPAddress_t1399971723_0_0_0_Types[] = { (&IPAddress_t1399971723_0_0_0) };
extern const Il2CppGenericInst GenInst_IPAddress_t1399971723_0_0_0 = { 1, GenInst_IPAddress_t1399971723_0_0_0_Types };
static const RuntimeType* GenInst_ArraySegment_1_t2594217482_0_0_0_Types[] = { (&ArraySegment_1_t2594217482_0_0_0) };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t2594217482_0_0_0 = { 1, GenInst_ArraySegment_1_t2594217482_0_0_0_Types };
static const RuntimeType* GenInst_Cookie_t3154017544_0_0_0_Types[] = { (&Cookie_t3154017544_0_0_0) };
extern const Il2CppGenericInst GenInst_Cookie_t3154017544_0_0_0 = { 1, GenInst_Cookie_t3154017544_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1174980068_0_0_0_Types[] = { (&KeyValuePair_2_t1174980068_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0 = { 1, GenInst_KeyValuePair_2_t1174980068_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t3825574718_0_0_0), (&KeyValuePair_2_t1174980068_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t3825574718_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3497699202_0_0_0_Types[] = { (&KeyValuePair_2_t3497699202_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3497699202_0_0_0 = { 1, GenInst_KeyValuePair_2_t3497699202_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t3825574718_0_0_0), (&KeyValuePair_2_t3497699202_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0_Types };
static const RuntimeType* GenInst_X509Certificate_t283079845_0_0_0_Types[] = { (&X509Certificate_t283079845_0_0_0) };
extern const Il2CppGenericInst GenInst_X509Certificate_t283079845_0_0_0 = { 1, GenInst_X509Certificate_t283079845_0_0_0_Types };
static const RuntimeType* GenInst_IDeserializationCallback_t327125377_0_0_0_Types[] = { (&IDeserializationCallback_t327125377_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t327125377_0_0_0 = { 1, GenInst_IDeserializationCallback_t327125377_0_0_0_Types };
static const RuntimeType* GenInst_Capture_t4157900610_0_0_0_Types[] = { (&Capture_t4157900610_0_0_0) };
extern const Il2CppGenericInst GenInst_Capture_t4157900610_0_0_0 = { 1, GenInst_Capture_t4157900610_0_0_0_Types };
static const RuntimeType* GenInst_Group_t3761430853_0_0_0_Types[] = { (&Group_t3761430853_0_0_0) };
extern const Il2CppGenericInst GenInst_Group_t3761430853_0_0_0 = { 1, GenInst_Group_t3761430853_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3132015601_0_0_0_Types[] = { (&KeyValuePair_2_t3132015601_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0 = { 1, GenInst_KeyValuePair_2_t3132015601_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t3132015601_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const RuntimeType* GenInst_Mark_t2724874473_0_0_0_Types[] = { (&Mark_t2724874473_0_0_0) };
extern const Il2CppGenericInst GenInst_Mark_t2724874473_0_0_0 = { 1, GenInst_Mark_t2724874473_0_0_0_Types };
static const RuntimeType* GenInst_UriScheme_t1876590943_0_0_0_Types[] = { (&UriScheme_t1876590943_0_0_0) };
extern const Il2CppGenericInst GenInst_UriScheme_t1876590943_0_0_0 = { 1, GenInst_UriScheme_t1876590943_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t925946153_0_0_0_Types[] = { (&BigInteger_t925946153_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t925946153_0_0_0 = { 1, GenInst_BigInteger_t925946153_0_0_0_Types };
static const RuntimeType* GenInst_ByteU5BU5D_t3397334013_0_0_0_Types[] = { (&ByteU5BU5D_t3397334013_0_0_0) };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t3397334013_0_0_0 = { 1, GenInst_ByteU5BU5D_t3397334013_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t4224045037_0_0_0_Types[] = { (&IList_1_t4224045037_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t4224045037_0_0_0 = { 1, GenInst_IList_1_t4224045037_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t340212445_0_0_0_Types[] = { (&ICollection_1_t340212445_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t340212445_0_0_0 = { 1, GenInst_ICollection_1_t340212445_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3975231481_0_0_0_Types[] = { (&IEnumerable_1_t3975231481_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3975231481_0_0_0 = { 1, GenInst_IEnumerable_1_t3975231481_0_0_0_Types };
static const RuntimeType* GenInst_ClientCertificateType_t4001384466_0_0_0_Types[] = { (&ClientCertificateType_t4001384466_0_0_0) };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t4001384466_0_0_0 = { 1, GenInst_ClientCertificateType_t4001384466_0_0_0_Types };
static const RuntimeType* GenInst_Link_t865133271_0_0_0_Types[] = { (&Link_t865133271_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t865133271_0_0_0 = { 1, GenInst_Link_t865133271_0_0_0_Types };
static const RuntimeType* GenInst_LockDetails_t2550015005_0_0_0_Types[] = { (&LockDetails_t2550015005_0_0_0) };
extern const Il2CppGenericInst GenInst_LockDetails_t2550015005_0_0_0 = { 1, GenInst_LockDetails_t2550015005_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 4, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AsyncOperation_t3814632279_0_0_0_Types[] = { (&AsyncOperation_t3814632279_0_0_0) };
extern const Il2CppGenericInst GenInst_AsyncOperation_t3814632279_0_0_0 = { 1, GenInst_AsyncOperation_t3814632279_0_0_0_Types };
static const RuntimeType* GenInst_Camera_t189460977_0_0_0_Types[] = { (&Camera_t189460977_0_0_0) };
extern const Il2CppGenericInst GenInst_Camera_t189460977_0_0_0 = { 1, GenInst_Camera_t189460977_0_0_0_Types };
static const RuntimeType* GenInst_Behaviour_t955675639_0_0_0_Types[] = { (&Behaviour_t955675639_0_0_0) };
extern const Il2CppGenericInst GenInst_Behaviour_t955675639_0_0_0 = { 1, GenInst_Behaviour_t955675639_0_0_0_Types };
static const RuntimeType* GenInst_Component_t3819376471_0_0_0_Types[] = { (&Component_t3819376471_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_t3819376471_0_0_0 = { 1, GenInst_Component_t3819376471_0_0_0_Types };
static const RuntimeType* GenInst_Object_t1021602117_0_0_0_Types[] = { (&Object_t1021602117_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_t1021602117_0_0_0 = { 1, GenInst_Object_t1021602117_0_0_0_Types };
static const RuntimeType* GenInst_Display_t3666191348_0_0_0_Types[] = { (&Display_t3666191348_0_0_0) };
extern const Il2CppGenericInst GenInst_Display_t3666191348_0_0_0 = { 1, GenInst_Display_t3666191348_0_0_0_Types };
static const RuntimeType* GenInst_Keyframe_t1449471340_0_0_0_Types[] = { (&Keyframe_t1449471340_0_0_0) };
extern const Il2CppGenericInst GenInst_Keyframe_t1449471340_0_0_0 = { 1, GenInst_Keyframe_t1449471340_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t2243707580_0_0_0_Types[] = { (&Vector3_t2243707580_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0 = { 1, GenInst_Vector3_t2243707580_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t2243707581_0_0_0_Types[] = { (&Vector4_t2243707581_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0 = { 1, GenInst_Vector4_t2243707581_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2243707579_0_0_0_Types[] = { (&Vector2_t2243707579_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0 = { 1, GenInst_Vector2_t2243707579_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t874517518_0_0_0_Types[] = { (&Color32_t874517518_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0 = { 1, GenInst_Color32_t874517518_0_0_0_Types };
static const RuntimeType* GenInst_Playable_t1896841784_0_0_0_Types[] = { (&Playable_t1896841784_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_t1896841784_0_0_0 = { 1, GenInst_Playable_t1896841784_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_t988259697_0_0_0_Types[] = { (&PlayableOutput_t988259697_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_t988259697_0_0_0 = { 1, GenInst_PlayableOutput_t988259697_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types[] = { (&Scene_t1684909666_0_0_0), (&LoadSceneMode_t2981886439_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1684909666_0_0_0_Types[] = { (&Scene_t1684909666_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0 = { 1, GenInst_Scene_t1684909666_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types[] = { (&Scene_t1684909666_0_0_0), (&Scene_t1684909666_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0 = { 2, GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0_Types };
static const RuntimeType* GenInst_SpriteAtlas_t3132429450_0_0_0_Types[] = { (&SpriteAtlas_t3132429450_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteAtlas_t3132429450_0_0_0 = { 1, GenInst_SpriteAtlas_t3132429450_0_0_0_Types };
static const RuntimeType* GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types[] = { (&DisallowMultipleComponent_t2656950_0_0_0) };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2656950_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2656950_0_0_0_Types };
static const RuntimeType* GenInst_Attribute_t542643598_0_0_0_Types[] = { (&Attribute_t542643598_0_0_0) };
extern const Il2CppGenericInst GenInst_Attribute_t542643598_0_0_0 = { 1, GenInst_Attribute_t542643598_0_0_0_Types };
static const RuntimeType* GenInst__Attribute_t1557664299_0_0_0_Types[] = { (&_Attribute_t1557664299_0_0_0) };
extern const Il2CppGenericInst GenInst__Attribute_t1557664299_0_0_0 = { 1, GenInst__Attribute_t1557664299_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types[] = { (&ExecuteInEditMode_t3043633143_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3043633143_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3043633143_0_0_0_Types };
static const RuntimeType* GenInst_RequireComponent_t864575032_0_0_0_Types[] = { (&RequireComponent_t864575032_0_0_0) };
extern const Il2CppGenericInst GenInst_RequireComponent_t864575032_0_0_0 = { 1, GenInst_RequireComponent_t864575032_0_0_0_Types };
static const RuntimeType* GenInst_HitInfo_t1761367055_0_0_0_Types[] = { (&HitInfo_t1761367055_0_0_0) };
extern const Il2CppGenericInst GenInst_HitInfo_t1761367055_0_0_0 = { 1, GenInst_HitInfo_t1761367055_0_0_0_Types };
static const RuntimeType* GenInst_PersistentCall_t3793436469_0_0_0_Types[] = { (&PersistentCall_t3793436469_0_0_0) };
extern const Il2CppGenericInst GenInst_PersistentCall_t3793436469_0_0_0 = { 1, GenInst_PersistentCall_t3793436469_0_0_0_Types };
static const RuntimeType* GenInst_BaseInvokableCall_t2229564840_0_0_0_Types[] = { (&BaseInvokableCall_t2229564840_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2229564840_0_0_0 = { 1, GenInst_BaseInvokableCall_t2229564840_0_0_0_Types };
static const RuntimeType* GenInst_WorkRequest_t1154022482_0_0_0_Types[] = { (&WorkRequest_t1154022482_0_0_0) };
extern const Il2CppGenericInst GenInst_WorkRequest_t1154022482_0_0_0 = { 1, GenInst_WorkRequest_t1154022482_0_0_0_Types };
static const RuntimeType* GenInst_PlayableBinding_t2498078091_0_0_0_Types[] = { (&PlayableBinding_t2498078091_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableBinding_t2498078091_0_0_0 = { 1, GenInst_PlayableBinding_t2498078091_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types[] = { (&MessageTypeSubscribers_t2291506050_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&MessageTypeSubscribers_t2291506050_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_MessageEventArgs_t301283622_0_0_0_Types[] = { (&MessageEventArgs_t301283622_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t301283622_0_0_0 = { 1, GenInst_MessageEventArgs_t301283622_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1077405567_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t888819835_0_0_0_Types[] = { (&KeyValuePair_2_t888819835_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0 = { 1, GenInst_KeyValuePair_2_t888819835_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t888819835_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const RuntimeType* GenInst_WeakReference_t1077405567_0_0_0_Types[] = { (&WeakReference_t1077405567_0_0_0) };
extern const Il2CppGenericInst GenInst_WeakReference_t1077405567_0_0_0 = { 1, GenInst_WeakReference_t1077405567_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1077405567_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3571743403_0_0_0_Types[] = { (&KeyValuePair_2_t3571743403_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3571743403_0_0_0 = { 1, GenInst_KeyValuePair_2_t3571743403_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_KeyValuePair_2_t3571743403_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t1077405567_0_0_0), (&KeyValuePair_2_t3571743403_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_KeyValuePair_2_t3571743403_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_KeyValuePair_2_t3571743403_0_0_0_Types };
static const RuntimeType* GenInst_AudioSpatializerExtensionDefinition_t3666104158_0_0_0_Types[] = { (&AudioSpatializerExtensionDefinition_t3666104158_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioSpatializerExtensionDefinition_t3666104158_0_0_0 = { 1, GenInst_AudioSpatializerExtensionDefinition_t3666104158_0_0_0_Types };
static const RuntimeType* GenInst_AudioAmbisonicExtensionDefinition_t52665437_0_0_0_Types[] = { (&AudioAmbisonicExtensionDefinition_t52665437_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioAmbisonicExtensionDefinition_t52665437_0_0_0 = { 1, GenInst_AudioAmbisonicExtensionDefinition_t52665437_0_0_0_Types };
static const RuntimeType* GenInst_AudioSourceExtension_t1813599864_0_0_0_Types[] = { (&AudioSourceExtension_t1813599864_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioSourceExtension_t1813599864_0_0_0 = { 1, GenInst_AudioSourceExtension_t1813599864_0_0_0_Types };
static const RuntimeType* GenInst_ScriptableObject_t1975622470_0_0_0_Types[] = { (&ScriptableObject_t1975622470_0_0_0) };
extern const Il2CppGenericInst GenInst_ScriptableObject_t1975622470_0_0_0 = { 1, GenInst_ScriptableObject_t1975622470_0_0_0_Types };
static const RuntimeType* GenInst_AudioMixerPlayable_t1831808911_0_0_0_Types[] = { (&AudioMixerPlayable_t1831808911_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioMixerPlayable_t1831808911_0_0_0 = { 1, GenInst_AudioMixerPlayable_t1831808911_0_0_0_Types };
static const RuntimeType* GenInst_AudioClipPlayable_t192218916_0_0_0_Types[] = { (&AudioClipPlayable_t192218916_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioClipPlayable_t192218916_0_0_0 = { 1, GenInst_AudioClipPlayable_t192218916_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody2D_t502193897_0_0_0_Types[] = { (&Rigidbody2D_t502193897_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t502193897_0_0_0 = { 1, GenInst_Rigidbody2D_t502193897_0_0_0_Types };
static const RuntimeType* GenInst_Font_t4239498691_0_0_0_Types[] = { (&Font_t4239498691_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0 = { 1, GenInst_Font_t4239498691_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t1204258818_0_0_0_Types[] = { (&UIVertex_t1204258818_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0 = { 1, GenInst_UIVertex_t1204258818_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t3056636800_0_0_0_Types[] = { (&UICharInfo_t3056636800_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0 = { 1, GenInst_UICharInfo_t3056636800_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t3621277874_0_0_0_Types[] = { (&UILineInfo_t3621277874_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0 = { 1, GenInst_UILineInfo_t3621277874_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { (&String_t_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&String_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Types[] = { (&KeyValuePair_2_t1701344717_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0 = { 1, GenInst_KeyValuePair_2_t1701344717_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0_Types[] = { (&String_t_0_0_0), (&String_t_0_0_0), (&KeyValuePair_2_t1701344717_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0_Types };
static const RuntimeType* GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types[] = { (&XmlSchemaAttribute_t4015859774_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlSchemaAttribute_t4015859774_0_0_0 = { 1, GenInst_XmlSchemaAttribute_t4015859774_0_0_0_Types };
static const RuntimeType* GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types[] = { (&XmlSchemaAnnotated_t2082486936_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlSchemaAnnotated_t2082486936_0_0_0 = { 1, GenInst_XmlSchemaAnnotated_t2082486936_0_0_0_Types };
static const RuntimeType* GenInst_XmlSchemaObject_t2050913741_0_0_0_Types[] = { (&XmlSchemaObject_t2050913741_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlSchemaObject_t2050913741_0_0_0 = { 1, GenInst_XmlSchemaObject_t2050913741_0_0_0_Types };
static const RuntimeType* GenInst_XsdIdentityPath_t2037874_0_0_0_Types[] = { (&XsdIdentityPath_t2037874_0_0_0) };
extern const Il2CppGenericInst GenInst_XsdIdentityPath_t2037874_0_0_0 = { 1, GenInst_XsdIdentityPath_t2037874_0_0_0_Types };
static const RuntimeType* GenInst_XsdIdentityField_t2563516441_0_0_0_Types[] = { (&XsdIdentityField_t2563516441_0_0_0) };
extern const Il2CppGenericInst GenInst_XsdIdentityField_t2563516441_0_0_0 = { 1, GenInst_XsdIdentityField_t2563516441_0_0_0_Types };
static const RuntimeType* GenInst_XsdIdentityStep_t452377251_0_0_0_Types[] = { (&XsdIdentityStep_t452377251_0_0_0) };
extern const Il2CppGenericInst GenInst_XsdIdentityStep_t452377251_0_0_0 = { 1, GenInst_XsdIdentityStep_t452377251_0_0_0_Types };
static const RuntimeType* GenInst_XmlAttribute_t175731005_0_0_0_Types[] = { (&XmlAttribute_t175731005_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlAttribute_t175731005_0_0_0 = { 1, GenInst_XmlAttribute_t175731005_0_0_0_Types };
static const RuntimeType* GenInst_IHasXmlChildNode_t2048545686_0_0_0_Types[] = { (&IHasXmlChildNode_t2048545686_0_0_0) };
extern const Il2CppGenericInst GenInst_IHasXmlChildNode_t2048545686_0_0_0 = { 1, GenInst_IHasXmlChildNode_t2048545686_0_0_0_Types };
static const RuntimeType* GenInst_XmlNode_t616554813_0_0_0_Types[] = { (&XmlNode_t616554813_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlNode_t616554813_0_0_0 = { 1, GenInst_XmlNode_t616554813_0_0_0_Types };
static const RuntimeType* GenInst_IXPathNavigable_t845515791_0_0_0_Types[] = { (&IXPathNavigable_t845515791_0_0_0) };
extern const Il2CppGenericInst GenInst_IXPathNavigable_t845515791_0_0_0 = { 1, GenInst_IXPathNavigable_t845515791_0_0_0_Types };
static const RuntimeType* GenInst_XmlQualifiedName_t1944712516_0_0_0_Types[] = { (&XmlQualifiedName_t1944712516_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlQualifiedName_t1944712516_0_0_0 = { 1, GenInst_XmlQualifiedName_t1944712516_0_0_0_Types };
static const RuntimeType* GenInst_Regex_t1803876613_0_0_0_Types[] = { (&Regex_t1803876613_0_0_0) };
extern const Il2CppGenericInst GenInst_Regex_t1803876613_0_0_0 = { 1, GenInst_Regex_t1803876613_0_0_0_Types };
static const RuntimeType* GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types[] = { (&XmlSchemaSimpleType_t248156492_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlSchemaSimpleType_t248156492_0_0_0 = { 1, GenInst_XmlSchemaSimpleType_t248156492_0_0_0_Types };
static const RuntimeType* GenInst_XmlSchemaType_t1795078578_0_0_0_Types[] = { (&XmlSchemaType_t1795078578_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlSchemaType_t1795078578_0_0_0 = { 1, GenInst_XmlSchemaType_t1795078578_0_0_0_Types };
static const RuntimeType* GenInst_XmlException_t4188277960_0_0_0_Types[] = { (&XmlException_t4188277960_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlException_t4188277960_0_0_0 = { 1, GenInst_XmlException_t4188277960_0_0_0_Types };
static const RuntimeType* GenInst_SystemException_t3877406272_0_0_0_Types[] = { (&SystemException_t3877406272_0_0_0) };
extern const Il2CppGenericInst GenInst_SystemException_t3877406272_0_0_0 = { 1, GenInst_SystemException_t3877406272_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t1927440687_0_0_0_Types[] = { (&Exception_t1927440687_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0 = { 1, GenInst_Exception_t1927440687_0_0_0_Types };
static const RuntimeType* GenInst__Exception_t3026971024_0_0_0_Types[] = { (&_Exception_t3026971024_0_0_0) };
extern const Il2CppGenericInst GenInst__Exception_t3026971024_0_0_0 = { 1, GenInst__Exception_t3026971024_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1430411454_0_0_0_Types[] = { (&KeyValuePair_2_t1430411454_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1430411454_0_0_0 = { 1, GenInst_KeyValuePair_2_t1430411454_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types[] = { (&String_t_0_0_0), (&DTDNode_t1758286970_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0 = { 2, GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0_Types };
static const RuntimeType* GenInst_DTDNode_t1758286970_0_0_0_Types[] = { (&DTDNode_t1758286970_0_0_0) };
extern const Il2CppGenericInst GenInst_DTDNode_t1758286970_0_0_0 = { 1, GenInst_DTDNode_t1758286970_0_0_0_Types };
static const RuntimeType* GenInst_IXmlLineInfo_t135184468_0_0_0_Types[] = { (&IXmlLineInfo_t135184468_0_0_0) };
extern const Il2CppGenericInst GenInst_IXmlLineInfo_t135184468_0_0_0 = { 1, GenInst_IXmlLineInfo_t135184468_0_0_0_Types };
static const RuntimeType* GenInst_AttributeSlot_t1499247213_0_0_0_Types[] = { (&AttributeSlot_t1499247213_0_0_0) };
extern const Il2CppGenericInst GenInst_AttributeSlot_t1499247213_0_0_0 = { 1, GenInst_AttributeSlot_t1499247213_0_0_0_Types };
static const RuntimeType* GenInst_Entry_t2583369454_0_0_0_Types[] = { (&Entry_t2583369454_0_0_0) };
extern const Il2CppGenericInst GenInst_Entry_t2583369454_0_0_0 = { 1, GenInst_Entry_t2583369454_0_0_0_Types };
static const RuntimeType* GenInst_NsDecl_t3210081295_0_0_0_Types[] = { (&NsDecl_t3210081295_0_0_0) };
extern const Il2CppGenericInst GenInst_NsDecl_t3210081295_0_0_0 = { 1, GenInst_NsDecl_t3210081295_0_0_0_Types };
static const RuntimeType* GenInst_NsScope_t2513625351_0_0_0_Types[] = { (&NsScope_t2513625351_0_0_0) };
extern const Il2CppGenericInst GenInst_NsScope_t2513625351_0_0_0 = { 1, GenInst_NsScope_t2513625351_0_0_0_Types };
static const RuntimeType* GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types[] = { (&XmlAttributeTokenInfo_t3353594030_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0 = { 1, GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0_Types };
static const RuntimeType* GenInst_XmlTokenInfo_t254587324_0_0_0_Types[] = { (&XmlTokenInfo_t254587324_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlTokenInfo_t254587324_0_0_0 = { 1, GenInst_XmlTokenInfo_t254587324_0_0_0_Types };
static const RuntimeType* GenInst_TagName_t2340974457_0_0_0_Types[] = { (&TagName_t2340974457_0_0_0) };
extern const Il2CppGenericInst GenInst_TagName_t2340974457_0_0_0 = { 1, GenInst_TagName_t2340974457_0_0_0_Types };
static const RuntimeType* GenInst_XmlNodeInfo_t3709371029_0_0_0_Types[] = { (&XmlNodeInfo_t3709371029_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlNodeInfo_t3709371029_0_0_0 = { 1, GenInst_XmlNodeInfo_t3709371029_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClipPlayable_t4099382200_0_0_0_Types[] = { (&AnimationClipPlayable_t4099382200_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClipPlayable_t4099382200_0_0_0 = { 1, GenInst_AnimationClipPlayable_t4099382200_0_0_0_Types };
static const RuntimeType* GenInst_AnimationLayerMixerPlayable_t3057952312_0_0_0_Types[] = { (&AnimationLayerMixerPlayable_t3057952312_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationLayerMixerPlayable_t3057952312_0_0_0 = { 1, GenInst_AnimationLayerMixerPlayable_t3057952312_0_0_0_Types };
static const RuntimeType* GenInst_AnimationMixerPlayable_t1343787797_0_0_0_Types[] = { (&AnimationMixerPlayable_t1343787797_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationMixerPlayable_t1343787797_0_0_0 = { 1, GenInst_AnimationMixerPlayable_t1343787797_0_0_0_Types };
static const RuntimeType* GenInst_AnimationOffsetPlayable_t1019600543_0_0_0_Types[] = { (&AnimationOffsetPlayable_t1019600543_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationOffsetPlayable_t1019600543_0_0_0 = { 1, GenInst_AnimationOffsetPlayable_t1019600543_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerPlayable_t1744083903_0_0_0_Types[] = { (&AnimatorControllerPlayable_t1744083903_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerPlayable_t1744083903_0_0_0 = { 1, GenInst_AnimatorControllerPlayable_t1744083903_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AchievementDescription_t3110978151_0_0_0_Types[] = { (&AchievementDescription_t3110978151_0_0_0) };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3110978151_0_0_0 = { 1, GenInst_AchievementDescription_t3110978151_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescription_t3498529102_0_0_0_Types[] = { (&IAchievementDescription_t3498529102_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t3498529102_0_0_0 = { 1, GenInst_IAchievementDescription_t3498529102_0_0_0_Types };
static const RuntimeType* GenInst_UserProfile_t3365630962_0_0_0_Types[] = { (&UserProfile_t3365630962_0_0_0) };
extern const Il2CppGenericInst GenInst_UserProfile_t3365630962_0_0_0 = { 1, GenInst_UserProfile_t3365630962_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfile_t4108565527_0_0_0_Types[] = { (&IUserProfile_t4108565527_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfile_t4108565527_0_0_0 = { 1, GenInst_IUserProfile_t4108565527_0_0_0_Types };
static const RuntimeType* GenInst_GcLeaderboard_t453887929_0_0_0_Types[] = { (&GcLeaderboard_t453887929_0_0_0) };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453887929_0_0_0 = { 1, GenInst_GcLeaderboard_t453887929_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types[] = { (&IAchievementDescriptionU5BU5D_t4083280315_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types[] = { (&IAchievementU5BU5D_t2709554645_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t2709554645_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t2709554645_0_0_0_Types };
static const RuntimeType* GenInst_IAchievement_t1752291260_0_0_0_Types[] = { (&IAchievement_t1752291260_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievement_t1752291260_0_0_0 = { 1, GenInst_IAchievement_t1752291260_0_0_0_Types };
static const RuntimeType* GenInst_GcAchievementData_t1754866149_0_0_0_Types[] = { (&GcAchievementData_t1754866149_0_0_0) };
extern const Il2CppGenericInst GenInst_GcAchievementData_t1754866149_0_0_0 = { 1, GenInst_GcAchievementData_t1754866149_0_0_0_Types };
static const RuntimeType* GenInst_Achievement_t1333316625_0_0_0_Types[] = { (&Achievement_t1333316625_0_0_0) };
extern const Il2CppGenericInst GenInst_Achievement_t1333316625_0_0_0 = { 1, GenInst_Achievement_t1333316625_0_0_0_Types };
static const RuntimeType* GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types[] = { (&IScoreU5BU5D_t3237304636_0_0_0) };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3237304636_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3237304636_0_0_0_Types };
static const RuntimeType* GenInst_IScore_t513966369_0_0_0_Types[] = { (&IScore_t513966369_0_0_0) };
extern const Il2CppGenericInst GenInst_IScore_t513966369_0_0_0 = { 1, GenInst_IScore_t513966369_0_0_0_Types };
static const RuntimeType* GenInst_GcScoreData_t3676783238_0_0_0_Types[] = { (&GcScoreData_t3676783238_0_0_0) };
extern const Il2CppGenericInst GenInst_GcScoreData_t3676783238_0_0_0 = { 1, GenInst_GcScoreData_t3676783238_0_0_0_Types };
static const RuntimeType* GenInst_Score_t2307748940_0_0_0_Types[] = { (&Score_t2307748940_0_0_0) };
extern const Il2CppGenericInst GenInst_Score_t2307748940_0_0_0 = { 1, GenInst_Score_t2307748940_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types[] = { (&IUserProfileU5BU5D_t3461248430_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t3461248430_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t3461248430_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutOption_t4183744904_0_0_0_Types[] = { (&GUILayoutOption_t4183744904_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t4183744904_0_0_0 = { 1, GenInst_GUILayoutOption_t4183744904_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&LayoutCache_t3120781045_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3749587448_0_0_0_Types[] = { (&KeyValuePair_2_t3749587448_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0 = { 1, GenInst_KeyValuePair_2_t3749587448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t3749587448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const RuntimeType* GenInst_LayoutCache_t3120781045_0_0_0_Types[] = { (&LayoutCache_t3120781045_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutCache_t3120781045_0_0_0 = { 1, GenInst_LayoutCache_t3120781045_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&LayoutCache_t3120781045_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4180919198_0_0_0_Types[] = { (&KeyValuePair_2_t4180919198_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4180919198_0_0_0 = { 1, GenInst_KeyValuePair_2_t4180919198_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&LayoutCache_t3120781045_0_0_0), (&KeyValuePair_2_t4180919198_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutEntry_t3828586629_0_0_0_Types[] = { (&GUILayoutEntry_t3828586629_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3828586629_0_0_0 = { 1, GenInst_GUILayoutEntry_t3828586629_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&IntPtr_t_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t1927440687_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Exception_t1927440687_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_GUIStyle_t1799908754_0_0_0_Types[] = { (&GUIStyle_t1799908754_0_0_0) };
extern const Il2CppGenericInst GenInst_GUIStyle_t1799908754_0_0_0 = { 1, GenInst_GUIStyle_t1799908754_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t1799908754_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t1799908754_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1472033238_0_0_0_Types[] = { (&KeyValuePair_2_t1472033238_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1472033238_0_0_0 = { 1, GenInst_KeyValuePair_2_t1472033238_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t1799908754_0_0_0), (&KeyValuePair_2_t1472033238_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit_t87180320_0_0_0_Types[] = { (&RaycastHit_t87180320_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit_t87180320_0_0_0 = { 1, GenInst_RaycastHit_t87180320_0_0_0_Types };
static const RuntimeType* GenInst_ContactPoint_t1376425630_0_0_0_Types[] = { (&ContactPoint_t1376425630_0_0_0) };
extern const Il2CppGenericInst GenInst_ContactPoint_t1376425630_0_0_0 = { 1, GenInst_ContactPoint_t1376425630_0_0_0_Types };
static const RuntimeType* GenInst_XAttribute_t3858477518_0_0_0_Types[] = { (&XAttribute_t3858477518_0_0_0) };
extern const Il2CppGenericInst GenInst_XAttribute_t3858477518_0_0_0 = { 1, GenInst_XAttribute_t3858477518_0_0_0_Types };
static const RuntimeType* GenInst_XObject_t3550811009_0_0_0_Types[] = { (&XObject_t3550811009_0_0_0) };
extern const Il2CppGenericInst GenInst_XObject_t3550811009_0_0_0 = { 1, GenInst_XObject_t3550811009_0_0_0_Types };
static const RuntimeType* GenInst_XNode_t2707504214_0_0_0_Types[] = { (&XNode_t2707504214_0_0_0) };
extern const Il2CppGenericInst GenInst_XNode_t2707504214_0_0_0 = { 1, GenInst_XNode_t2707504214_0_0_0_Types };
static const RuntimeType* GenInst_XElement_t553821050_0_0_0_Types[] = { (&XElement_t553821050_0_0_0) };
extern const Il2CppGenericInst GenInst_XElement_t553821050_0_0_0 = { 1, GenInst_XElement_t553821050_0_0_0_Types };
static const RuntimeType* GenInst_IXmlSerializable_t2623090263_0_0_0_Types[] = { (&IXmlSerializable_t2623090263_0_0_0) };
extern const Il2CppGenericInst GenInst_IXmlSerializable_t2623090263_0_0_0 = { 1, GenInst_IXmlSerializable_t2623090263_0_0_0_Types };
static const RuntimeType* GenInst_XContainer_t1445911831_0_0_0_Types[] = { (&XContainer_t1445911831_0_0_0) };
extern const Il2CppGenericInst GenInst_XContainer_t1445911831_0_0_0 = { 1, GenInst_XContainer_t1445911831_0_0_0_Types };
static const RuntimeType* GenInst_XName_t785190363_0_0_0_Types[] = { (&XName_t785190363_0_0_0) };
extern const Il2CppGenericInst GenInst_XName_t785190363_0_0_0 = { 1, GenInst_XName_t785190363_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_Types[] = { (&String_t_0_0_0), (&XNamespace_t1613015075_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0 = { 2, GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_Types };
static const RuntimeType* GenInst_XNamespace_t1613015075_0_0_0_Types[] = { (&XNamespace_t1613015075_0_0_0) };
extern const Il2CppGenericInst GenInst_XNamespace_t1613015075_0_0_0 = { 1, GenInst_XNamespace_t1613015075_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&XNamespace_t1613015075_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1285139559_0_0_0_Types[] = { (&KeyValuePair_2_t1285139559_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1285139559_0_0_0 = { 1, GenInst_KeyValuePair_2_t1285139559_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_KeyValuePair_2_t1285139559_0_0_0_Types[] = { (&String_t_0_0_0), (&XNamespace_t1613015075_0_0_0), (&KeyValuePair_2_t1285139559_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_KeyValuePair_2_t1285139559_0_0_0 = { 3, GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_KeyValuePair_2_t1285139559_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XName_t785190363_0_0_0_Types[] = { (&String_t_0_0_0), (&XName_t785190363_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XName_t785190363_0_0_0 = { 2, GenInst_String_t_0_0_0_XName_t785190363_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t2989172532_0_0_0_Types[] = { (&IEquatable_1_t2989172532_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2989172532_0_0_0 = { 1, GenInst_IEquatable_1_t2989172532_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&XName_t785190363_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t457314847_0_0_0_Types[] = { (&KeyValuePair_2_t457314847_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t457314847_0_0_0 = { 1, GenInst_KeyValuePair_2_t457314847_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XName_t785190363_0_0_0_KeyValuePair_2_t457314847_0_0_0_Types[] = { (&String_t_0_0_0), (&XName_t785190363_0_0_0), (&KeyValuePair_2_t457314847_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XName_t785190363_0_0_0_KeyValuePair_2_t457314847_0_0_0 = { 3, GenInst_String_t_0_0_0_XName_t785190363_0_0_0_KeyValuePair_2_t457314847_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_Types[] = { (&Type_t_0_0_0), (&Func_2_t3675469371_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0 = { 2, GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_Types };
static const RuntimeType* GenInst_Func_2_t3675469371_0_0_0_Types[] = { (&Func_2_t3675469371_0_0_0) };
extern const Il2CppGenericInst GenInst_Func_2_t3675469371_0_0_0 = { 1, GenInst_Func_2_t3675469371_0_0_0_Types };
static const RuntimeType* GenInst_MulticastDelegate_t3201952435_0_0_0_Types[] = { (&MulticastDelegate_t3201952435_0_0_0) };
extern const Il2CppGenericInst GenInst_MulticastDelegate_t3201952435_0_0_0 = { 1, GenInst_MulticastDelegate_t3201952435_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&Func_2_t3675469371_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3370172490_0_0_0_Types[] = { (&KeyValuePair_2_t3370172490_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3370172490_0_0_0 = { 1, GenInst_KeyValuePair_2_t3370172490_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_KeyValuePair_2_t3370172490_0_0_0_Types[] = { (&Type_t_0_0_0), (&Func_2_t3675469371_0_0_0), (&KeyValuePair_2_t3370172490_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_KeyValuePair_2_t3370172490_0_0_0 = { 3, GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_KeyValuePair_2_t3370172490_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_Types[] = { (&String_t_0_0_0), (&Delegate_t3022476291_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0 = { 2, GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Delegate_t3022476291_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2694600775_0_0_0_Types[] = { (&KeyValuePair_2_t2694600775_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2694600775_0_0_0 = { 1, GenInst_KeyValuePair_2_t2694600775_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_KeyValuePair_2_t2694600775_0_0_0_Types[] = { (&String_t_0_0_0), (&Delegate_t3022476291_0_0_0), (&KeyValuePair_2_t2694600775_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_KeyValuePair_2_t2694600775_0_0_0 = { 3, GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_KeyValuePair_2_t2694600775_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&Dictionary_2_t1629922792_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&Methods_t1187897474_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_Types };
static const RuntimeType* GenInst_Methods_t1187897474_0_0_0_Types[] = { (&Methods_t1187897474_0_0_0) };
extern const Il2CppGenericInst GenInst_Methods_t1187897474_0_0_0 = { 1, GenInst_Methods_t1187897474_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&Methods_t1187897474_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3682235310_0_0_0_Types[] = { (&KeyValuePair_2_t3682235310_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3682235310_0_0_0 = { 1, GenInst_KeyValuePair_2_t3682235310_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_KeyValuePair_2_t3682235310_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&Methods_t1187897474_0_0_0), (&KeyValuePair_2_t3682235310_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_KeyValuePair_2_t3682235310_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_KeyValuePair_2_t3682235310_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t1629922792_0_0_0_Types[] = { (&Dictionary_2_t1629922792_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1629922792_0_0_0 = { 1, GenInst_Dictionary_2_t1629922792_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_t596158605_0_0_0_Types[] = { (&IDictionary_t596158605_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_t596158605_0_0_0 = { 1, GenInst_IDictionary_t596158605_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&Dictionary_2_t1629922792_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4124260628_0_0_0_Types[] = { (&KeyValuePair_2_t4124260628_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4124260628_0_0_0 = { 1, GenInst_KeyValuePair_2_t4124260628_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_KeyValuePair_2_t4124260628_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&Dictionary_2_t1629922792_0_0_0), (&KeyValuePair_2_t4124260628_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_KeyValuePair_2_t4124260628_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_KeyValuePair_2_t4124260628_0_0_0_Types };
static const RuntimeType* GenInst_JsonData_t4263252052_0_0_0_Types[] = { (&JsonData_t4263252052_0_0_0) };
extern const Il2CppGenericInst GenInst_JsonData_t4263252052_0_0_0 = { 1, GenInst_JsonData_t4263252052_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3935376536_0_0_0_Types[] = { (&KeyValuePair_2_t3935376536_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3935376536_0_0_0 = { 1, GenInst_KeyValuePair_2_t3935376536_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_Types[] = { (&String_t_0_0_0), (&JsonData_t4263252052_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_Types };
static const RuntimeType* GenInst_IJsonWrapper_t3095378610_0_0_0_Types[] = { (&IJsonWrapper_t3095378610_0_0_0) };
extern const Il2CppGenericInst GenInst_IJsonWrapper_t3095378610_0_0_0 = { 1, GenInst_IJsonWrapper_t3095378610_0_0_0_Types };
static const RuntimeType* GenInst_IOrderedDictionary_t252115128_0_0_0_Types[] = { (&IOrderedDictionary_t252115128_0_0_0) };
extern const Il2CppGenericInst GenInst_IOrderedDictionary_t252115128_0_0_0 = { 1, GenInst_IOrderedDictionary_t252115128_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t2172266925_0_0_0_Types[] = { (&IEquatable_1_t2172266925_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2172266925_0_0_0 = { 1, GenInst_IEquatable_1_t2172266925_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&JsonData_t4263252052_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_KeyValuePair_2_t3935376536_0_0_0_Types[] = { (&String_t_0_0_0), (&JsonData_t4263252052_0_0_0), (&KeyValuePair_2_t3935376536_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_KeyValuePair_2_t3935376536_0_0_0 = { 3, GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_KeyValuePair_2_t3935376536_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types[] = { (&String_t_0_0_0), (&PropertyMetadata_t3287739986_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0 = { 2, GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&PropertyMetadata_t3287739986_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t637145336_0_0_0_Types[] = { (&KeyValuePair_2_t637145336_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t637145336_0_0_0 = { 1, GenInst_KeyValuePair_2_t637145336_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_Types[] = { (&Type_t_0_0_0), (&ExporterFunc_t173265409_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0 = { 2, GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_Types[] = { (&Type_t_0_0_0), (&IDictionary_2_t787128596_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_Types[] = { (&Type_t_0_0_0), (&ImporterFunc_t850687278_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0 = { 2, GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types[] = { (&Type_t_0_0_0), (&ArrayMetadata_t1135078014_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0 = { 2, GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ArrayMetadata_t1135078014_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2779450660_0_0_0_Types[] = { (&KeyValuePair_2_t2779450660_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2779450660_0_0_0 = { 1, GenInst_KeyValuePair_2_t2779450660_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_Types[] = { (&Type_t_0_0_0), (&IDictionary_2_t3266987655_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types[] = { (&Type_t_0_0_0), (&MethodInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types[] = { (&Type_t_0_0_0), (&ObjectMetadata_t4058137740_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0 = { 2, GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ObjectMetadata_t4058137740_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1407543090_0_0_0_Types[] = { (&KeyValuePair_2_t1407543090_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1407543090_0_0_0 = { 1, GenInst_KeyValuePair_2_t1407543090_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_Types[] = { (&Type_t_0_0_0), (&IList_1_t3828680587_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0 = { 2, GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_Types };
static const RuntimeType* GenInst_PropertyMetadata_t3287739986_0_0_0_Types[] = { (&PropertyMetadata_t3287739986_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3287739986_0_0_0 = { 1, GenInst_PropertyMetadata_t3287739986_0_0_0_Types };
static const RuntimeType* GenInst_Link_t204904209_0_0_0_Types[] = { (&Link_t204904209_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t204904209_0_0_0 = { 1, GenInst_Link_t204904209_0_0_0_Types };
static const RuntimeType* GenInst_ArrayMetadata_t1135078014_0_0_0_Types[] = { (&ArrayMetadata_t1135078014_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t1135078014_0_0_0 = { 1, GenInst_ArrayMetadata_t1135078014_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ArrayMetadata_t1135078014_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ArrayMetadata_t1135078014_0_0_0), (&ArrayMetadata_t1135078014_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ArrayMetadata_t1135078014_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t2779450660_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ArrayMetadata_t1135078014_0_0_0), (&KeyValuePair_2_t2779450660_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t2779450660_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t2779450660_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&ArrayMetadata_t1135078014_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t829781133_0_0_0_Types[] = { (&KeyValuePair_2_t829781133_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t829781133_0_0_0 = { 1, GenInst_KeyValuePair_2_t829781133_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t829781133_0_0_0_Types[] = { (&Type_t_0_0_0), (&ArrayMetadata_t1135078014_0_0_0), (&KeyValuePair_2_t829781133_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t829781133_0_0_0 = { 3, GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t829781133_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t3266987655_0_0_0_Types[] = { (&IDictionary_2_t3266987655_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3266987655_0_0_0 = { 1, GenInst_IDictionary_2_t3266987655_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&IDictionary_2_t3266987655_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2961690774_0_0_0_Types[] = { (&KeyValuePair_2_t2961690774_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2961690774_0_0_0 = { 1, GenInst_KeyValuePair_2_t2961690774_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_KeyValuePair_2_t2961690774_0_0_0_Types[] = { (&Type_t_0_0_0), (&IDictionary_2_t3266987655_0_0_0), (&KeyValuePair_2_t2961690774_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_KeyValuePair_2_t2961690774_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_KeyValuePair_2_t2961690774_0_0_0_Types };
static const RuntimeType* GenInst_ObjectMetadata_t4058137740_0_0_0_Types[] = { (&ObjectMetadata_t4058137740_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t4058137740_0_0_0 = { 1, GenInst_ObjectMetadata_t4058137740_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ObjectMetadata_t4058137740_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ObjectMetadata_t4058137740_0_0_0), (&ObjectMetadata_t4058137740_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ObjectMetadata_t4058137740_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t1407543090_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&ObjectMetadata_t4058137740_0_0_0), (&KeyValuePair_2_t1407543090_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t1407543090_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t1407543090_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&ObjectMetadata_t4058137740_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3752840859_0_0_0_Types[] = { (&KeyValuePair_2_t3752840859_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3752840859_0_0_0 = { 1, GenInst_KeyValuePair_2_t3752840859_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t3752840859_0_0_0_Types[] = { (&Type_t_0_0_0), (&ObjectMetadata_t4058137740_0_0_0), (&KeyValuePair_2_t3752840859_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t3752840859_0_0_0 = { 3, GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t3752840859_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3828680587_0_0_0_Types[] = { (&IList_1_t3828680587_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3828680587_0_0_0 = { 1, GenInst_IList_1_t3828680587_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&IList_1_t3828680587_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3523383706_0_0_0_Types[] = { (&KeyValuePair_2_t3523383706_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3523383706_0_0_0 = { 1, GenInst_KeyValuePair_2_t3523383706_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_KeyValuePair_2_t3523383706_0_0_0_Types[] = { (&Type_t_0_0_0), (&IList_1_t3828680587_0_0_0), (&KeyValuePair_2_t3523383706_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_KeyValuePair_2_t3523383706_0_0_0 = { 3, GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_KeyValuePair_2_t3523383706_0_0_0_Types };
static const RuntimeType* GenInst_ExporterFunc_t173265409_0_0_0_Types[] = { (&ExporterFunc_t173265409_0_0_0) };
extern const Il2CppGenericInst GenInst_ExporterFunc_t173265409_0_0_0 = { 1, GenInst_ExporterFunc_t173265409_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&ExporterFunc_t173265409_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4162935824_0_0_0_Types[] = { (&KeyValuePair_2_t4162935824_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4162935824_0_0_0 = { 1, GenInst_KeyValuePair_2_t4162935824_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_KeyValuePair_2_t4162935824_0_0_0_Types[] = { (&Type_t_0_0_0), (&ExporterFunc_t173265409_0_0_0), (&KeyValuePair_2_t4162935824_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_KeyValuePair_2_t4162935824_0_0_0 = { 3, GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_KeyValuePair_2_t4162935824_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t787128596_0_0_0_Types[] = { (&IDictionary_2_t787128596_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t787128596_0_0_0 = { 1, GenInst_IDictionary_2_t787128596_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&IDictionary_2_t787128596_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t481831715_0_0_0_Types[] = { (&KeyValuePair_2_t481831715_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t481831715_0_0_0 = { 1, GenInst_KeyValuePair_2_t481831715_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_KeyValuePair_2_t481831715_0_0_0_Types[] = { (&Type_t_0_0_0), (&IDictionary_2_t787128596_0_0_0), (&KeyValuePair_2_t481831715_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_KeyValuePair_2_t481831715_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_KeyValuePair_2_t481831715_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&PropertyMetadata_t3287739986_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&PropertyMetadata_t3287739986_0_0_0), (&PropertyMetadata_t3287739986_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_PropertyMetadata_t3287739986_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&PropertyMetadata_t3287739986_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_KeyValuePair_2_t637145336_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&PropertyMetadata_t3287739986_0_0_0), (&KeyValuePair_2_t637145336_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_KeyValuePair_2_t637145336_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_KeyValuePair_2_t637145336_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&PropertyMetadata_t3287739986_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2959864470_0_0_0_Types[] = { (&KeyValuePair_2_t2959864470_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2959864470_0_0_0 = { 1, GenInst_KeyValuePair_2_t2959864470_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_KeyValuePair_2_t2959864470_0_0_0_Types[] = { (&String_t_0_0_0), (&PropertyMetadata_t3287739986_0_0_0), (&KeyValuePair_2_t2959864470_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_KeyValuePair_2_t2959864470_0_0_0 = { 3, GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_KeyValuePair_2_t2959864470_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&MethodInfo_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3025249456_0_0_0_Types[] = { (&KeyValuePair_2_t3025249456_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3025249456_0_0_0 = { 1, GenInst_KeyValuePair_2_t3025249456_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_KeyValuePair_2_t3025249456_0_0_0_Types[] = { (&Type_t_0_0_0), (&MethodInfo_t_0_0_0), (&KeyValuePair_2_t3025249456_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_KeyValuePair_2_t3025249456_0_0_0 = { 3, GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_KeyValuePair_2_t3025249456_0_0_0_Types };
static const RuntimeType* GenInst_ITypeInfo_t3592676621_0_0_0_Types[] = { (&ITypeInfo_t3592676621_0_0_0) };
extern const Il2CppGenericInst GenInst_ITypeInfo_t3592676621_0_0_0 = { 1, GenInst_ITypeInfo_t3592676621_0_0_0_Types };
static const RuntimeType* GenInst_ImporterFunc_t850687278_0_0_0_Types[] = { (&ImporterFunc_t850687278_0_0_0) };
extern const Il2CppGenericInst GenInst_ImporterFunc_t850687278_0_0_0 = { 1, GenInst_ImporterFunc_t850687278_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&ImporterFunc_t850687278_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t545390397_0_0_0_Types[] = { (&KeyValuePair_2_t545390397_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t545390397_0_0_0 = { 1, GenInst_KeyValuePair_2_t545390397_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_KeyValuePair_2_t545390397_0_0_0_Types[] = { (&Type_t_0_0_0), (&ImporterFunc_t850687278_0_0_0), (&KeyValuePair_2_t545390397_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_KeyValuePair_2_t545390397_0_0_0 = { 3, GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_KeyValuePair_2_t545390397_0_0_0_Types };
static const RuntimeType* GenInst_JsonToken_t2445581255_0_0_0_Types[] = { (&JsonToken_t2445581255_0_0_0) };
extern const Il2CppGenericInst GenInst_JsonToken_t2445581255_0_0_0 = { 1, GenInst_JsonToken_t2445581255_0_0_0_Types };
static const RuntimeType* GenInst_WriterContext_t1209007092_0_0_0_Types[] = { (&WriterContext_t1209007092_0_0_0) };
extern const Il2CppGenericInst GenInst_WriterContext_t1209007092_0_0_0 = { 1, GenInst_WriterContext_t1209007092_0_0_0_Types };
static const RuntimeType* GenInst_StateHandler_t3489987002_0_0_0_Types[] = { (&StateHandler_t3489987002_0_0_0) };
extern const Il2CppGenericInst GenInst_StateHandler_t3489987002_0_0_0 = { 1, GenInst_StateHandler_t3489987002_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t2784070411_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0 = { 2, GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_Types };
static const RuntimeType* GenInst_TraceListener_t3414949279_0_0_0_Types[] = { (&TraceListener_t3414949279_0_0_0) };
extern const Il2CppGenericInst GenInst_TraceListener_t3414949279_0_0_0 = { 1, GenInst_TraceListener_t3414949279_0_0_0_Types };
static const RuntimeType* GenInst_IDisposable_t2427283555_0_0_0_Types[] = { (&IDisposable_t2427283555_0_0_0) };
extern const Il2CppGenericInst GenInst_IDisposable_t2427283555_0_0_0 = { 1, GenInst_IDisposable_t2427283555_0_0_0_Types };
static const RuntimeType* GenInst_MarshalByRefObject_t1285298191_0_0_0_Types[] = { (&MarshalByRefObject_t1285298191_0_0_0) };
extern const Il2CppGenericInst GenInst_MarshalByRefObject_t1285298191_0_0_0 = { 1, GenInst_MarshalByRefObject_t1285298191_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2784070411_0_0_0_Types[] = { (&List_1_t2784070411_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2784070411_0_0_0 = { 1, GenInst_List_1_t2784070411_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t2784070411_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2456194895_0_0_0_Types[] = { (&KeyValuePair_2_t2456194895_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2456194895_0_0_0 = { 1, GenInst_KeyValuePair_2_t2456194895_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_KeyValuePair_2_t2456194895_0_0_0_Types[] = { (&String_t_0_0_0), (&List_1_t2784070411_0_0_0), (&KeyValuePair_2_t2456194895_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_KeyValuePair_2_t2456194895_0_0_0 = { 3, GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_KeyValuePair_2_t2456194895_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_Types[] = { (&String_t_0_0_0), (&XElement_t553821050_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XElement_t553821050_0_0_0 = { 2, GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_Types };
static const RuntimeType* GenInst_XAttribute_t3858477518_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&XAttribute_t3858477518_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_XAttribute_t3858477518_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_XAttribute_t3858477518_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&XElement_t553821050_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&XElement_t553821050_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t225945534_0_0_0_Types[] = { (&KeyValuePair_2_t225945534_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t225945534_0_0_0 = { 1, GenInst_KeyValuePair_2_t225945534_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_KeyValuePair_2_t225945534_0_0_0_Types[] = { (&String_t_0_0_0), (&XElement_t553821050_0_0_0), (&KeyValuePair_2_t225945534_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_KeyValuePair_2_t225945534_0_0_0 = { 3, GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_KeyValuePair_2_t225945534_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_Types[] = { (&String_t_0_0_0), (&RegionEndpoint_t661522805_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0 = { 2, GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_Types };
static const RuntimeType* GenInst_RegionEndpoint_t661522805_0_0_0_Types[] = { (&RegionEndpoint_t661522805_0_0_0) };
extern const Il2CppGenericInst GenInst_RegionEndpoint_t661522805_0_0_0 = { 1, GenInst_RegionEndpoint_t661522805_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&RegionEndpoint_t661522805_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t333647289_0_0_0_Types[] = { (&KeyValuePair_2_t333647289_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t333647289_0_0_0 = { 1, GenInst_KeyValuePair_2_t333647289_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_KeyValuePair_2_t333647289_0_0_0_Types[] = { (&String_t_0_0_0), (&RegionEndpoint_t661522805_0_0_0), (&KeyValuePair_2_t333647289_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_KeyValuePair_2_t333647289_0_0_0 = { 3, GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_KeyValuePair_2_t333647289_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_Types[] = { (&String_t_0_0_0), (&RegionEndpoint_t1885241449_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0 = { 2, GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_Types };
static const RuntimeType* GenInst_RegionEndpoint_t1885241449_0_0_0_Types[] = { (&RegionEndpoint_t1885241449_0_0_0) };
extern const Il2CppGenericInst GenInst_RegionEndpoint_t1885241449_0_0_0 = { 1, GenInst_RegionEndpoint_t1885241449_0_0_0_Types };
static const RuntimeType* GenInst_IRegionEndpoint_t544433888_0_0_0_Types[] = { (&IRegionEndpoint_t544433888_0_0_0) };
extern const Il2CppGenericInst GenInst_IRegionEndpoint_t544433888_0_0_0 = { 1, GenInst_IRegionEndpoint_t544433888_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&RegionEndpoint_t1885241449_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1557365933_0_0_0_Types[] = { (&KeyValuePair_2_t1557365933_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1557365933_0_0_0 = { 1, GenInst_KeyValuePair_2_t1557365933_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_KeyValuePair_2_t1557365933_0_0_0_Types[] = { (&String_t_0_0_0), (&RegionEndpoint_t1885241449_0_0_0), (&KeyValuePair_2_t1557365933_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_KeyValuePair_2_t1557365933_0_0_0 = { 3, GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_KeyValuePair_2_t1557365933_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_Types[] = { (&String_t_0_0_0), (&Endpoint_t3348692641_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0 = { 2, GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_Types };
static const RuntimeType* GenInst_Endpoint_t3348692641_0_0_0_Types[] = { (&Endpoint_t3348692641_0_0_0) };
extern const Il2CppGenericInst GenInst_Endpoint_t3348692641_0_0_0 = { 1, GenInst_Endpoint_t3348692641_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&Endpoint_t3348692641_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3020817125_0_0_0_Types[] = { (&KeyValuePair_2_t3020817125_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3020817125_0_0_0 = { 1, GenInst_KeyValuePair_2_t3020817125_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_KeyValuePair_2_t3020817125_0_0_0_Types[] = { (&String_t_0_0_0), (&Endpoint_t3348692641_0_0_0), (&KeyValuePair_2_t3020817125_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_KeyValuePair_2_t3020817125_0_0_0 = { 3, GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_KeyValuePair_2_t3020817125_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_Types[] = { (&String_t_0_0_0), (&IRegionEndpoint_t544433888_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0 = { 2, GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&IRegionEndpoint_t544433888_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t216558372_0_0_0_Types[] = { (&KeyValuePair_2_t216558372_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t216558372_0_0_0 = { 1, GenInst_KeyValuePair_2_t216558372_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_KeyValuePair_2_t216558372_0_0_0_Types[] = { (&String_t_0_0_0), (&IRegionEndpoint_t544433888_0_0_0), (&KeyValuePair_2_t216558372_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_KeyValuePair_2_t216558372_0_0_0 = { 3, GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_KeyValuePair_2_t216558372_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&String_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3089358386_0_0_0_Types[] = { (&KeyValuePair_2_t3089358386_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3089358386_0_0_0 = { 1, GenInst_KeyValuePair_2_t3089358386_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&String_t_0_0_0), (&KeyValuePair_2_t3089358386_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0_Types };
static const RuntimeType* GenInst_Action_t3226471752_0_0_0_Types[] = { (&Action_t3226471752_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0 = { 1, GenInst_Action_t3226471752_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_Types[] = { (&Type_t_0_0_0), (&InstantiationModel_t1632807378_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0 = { 2, GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&InstantiationModel_t1632807378_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3277180024_0_0_0_Types[] = { (&KeyValuePair_2_t3277180024_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3277180024_0_0_0 = { 1, GenInst_KeyValuePair_2_t3277180024_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Type_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Type_t_0_0_0_Types[] = { (&Type_t_0_0_0), (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0 = { 2, GenInst_Type_t_0_0_0_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Type_t_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_MethodInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&MethodInfo_t_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_MethodInfo_t_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1327510497_0_0_0_Types[] = { (&KeyValuePair_2_t1327510497_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1327510497_0_0_0 = { 1, GenInst_KeyValuePair_2_t1327510497_0_0_0_Types };
static const RuntimeType* GenInst_InstantiationModel_t1632807378_0_0_0_Types[] = { (&InstantiationModel_t1632807378_0_0_0) };
extern const Il2CppGenericInst GenInst_InstantiationModel_t1632807378_0_0_0 = { 1, GenInst_InstantiationModel_t1632807378_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&InstantiationModel_t1632807378_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&InstantiationModel_t1632807378_0_0_0), (&InstantiationModel_t1632807378_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&InstantiationModel_t1632807378_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t3277180024_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&InstantiationModel_t1632807378_0_0_0), (&KeyValuePair_2_t3277180024_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t3277180024_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t3277180024_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&InstantiationModel_t1632807378_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t1327510497_0_0_0_Types[] = { (&Type_t_0_0_0), (&InstantiationModel_t1632807378_0_0_0), (&KeyValuePair_2_t1327510497_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t1327510497_0_0_0 = { 3, GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t1327510497_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2384152414_0_0_0_Types[] = { (&KeyValuePair_2_t2384152414_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2384152414_0_0_0 = { 1, GenInst_KeyValuePair_2_t2384152414_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2384152414_0_0_0_Types[] = { (&Type_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2384152414_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2384152414_0_0_0 = { 3, GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2384152414_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&Type_t_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t998506345_0_0_0_Types[] = { (&KeyValuePair_2_t998506345_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t998506345_0_0_0 = { 1, GenInst_KeyValuePair_2_t998506345_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t998506345_0_0_0_Types[] = { (&Type_t_0_0_0), (&Type_t_0_0_0), (&KeyValuePair_2_t998506345_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t998506345_0_0_0 = { 3, GenInst_Type_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t998506345_0_0_0_Types };
static const RuntimeType* GenInst_NetworkStatusEventArgs_t1607167653_0_0_0_Types[] = { (&NetworkStatusEventArgs_t1607167653_0_0_0) };
extern const Il2CppGenericInst GenInst_NetworkStatusEventArgs_t1607167653_0_0_0 = { 1, GenInst_NetworkStatusEventArgs_t1607167653_0_0_0_Types };
static const RuntimeType* GenInst_IRequest_t2400804350_0_0_0_AmazonWebServiceRequest_t3384026212_0_0_0_Types[] = { (&IRequest_t2400804350_0_0_0), (&AmazonWebServiceRequest_t3384026212_0_0_0) };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_AmazonWebServiceRequest_t3384026212_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_AmazonWebServiceRequest_t3384026212_0_0_0_Types };
static const RuntimeType* GenInst_AmazonWebServiceRequest_t3384026212_0_0_0_AmazonWebServiceResponse_t529043356_0_0_0_Exception_t1927440687_0_0_0_AsyncOptions_t558351272_0_0_0_Types[] = { (&AmazonWebServiceRequest_t3384026212_0_0_0), (&AmazonWebServiceResponse_t529043356_0_0_0), (&Exception_t1927440687_0_0_0), (&AsyncOptions_t558351272_0_0_0) };
extern const Il2CppGenericInst GenInst_AmazonWebServiceRequest_t3384026212_0_0_0_AmazonWebServiceResponse_t529043356_0_0_0_Exception_t1927440687_0_0_0_AsyncOptions_t558351272_0_0_0 = { 4, GenInst_AmazonWebServiceRequest_t3384026212_0_0_0_AmazonWebServiceResponse_t529043356_0_0_0_Exception_t1927440687_0_0_0_AsyncOptions_t558351272_0_0_0_Types };
static const RuntimeType* GenInst_StreamTransferProgressArgs_t2639638063_0_0_0_Types[] = { (&StreamTransferProgressArgs_t2639638063_0_0_0) };
extern const Il2CppGenericInst GenInst_StreamTransferProgressArgs_t2639638063_0_0_0 = { 1, GenInst_StreamTransferProgressArgs_t2639638063_0_0_0_Types };
static const RuntimeType* GenInst_IExecutionContext_t2477130752_0_0_0_Types[] = { (&IExecutionContext_t2477130752_0_0_0) };
extern const Il2CppGenericInst GenInst_IExecutionContext_t2477130752_0_0_0 = { 1, GenInst_IExecutionContext_t2477130752_0_0_0_Types };
static const RuntimeType* GenInst_IExecutionContext_t2477130752_0_0_0_Exception_t1927440687_0_0_0_Types[] = { (&IExecutionContext_t2477130752_0_0_0), (&Exception_t1927440687_0_0_0) };
extern const Il2CppGenericInst GenInst_IExecutionContext_t2477130752_0_0_0_Exception_t1927440687_0_0_0 = { 2, GenInst_IExecutionContext_t2477130752_0_0_0_Exception_t1927440687_0_0_0_Types };
static const RuntimeType* GenInst_IPipelineHandler_t2320756001_0_0_0_Types[] = { (&IPipelineHandler_t2320756001_0_0_0) };
extern const Il2CppGenericInst GenInst_IPipelineHandler_t2320756001_0_0_0 = { 1, GenInst_IPipelineHandler_t2320756001_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2361573779_0_0_0_Types[] = { (&KeyValuePair_2_t2361573779_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2361573779_0_0_0 = { 1, GenInst_KeyValuePair_2_t2361573779_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2361573779_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0 = { 3, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_Types[] = { (&Type_t_0_0_0), (&Dictionary_2_t1620371852_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0 = { 2, GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_Types[] = { (&String_t_0_0_0), (&ConstantClass_t4000559886_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0 = { 2, GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_Types };
static const RuntimeType* GenInst_ConstantClass_t4000559886_0_0_0_Types[] = { (&ConstantClass_t4000559886_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstantClass_t4000559886_0_0_0 = { 1, GenInst_ConstantClass_t4000559886_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&ConstantClass_t4000559886_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3672684370_0_0_0_Types[] = { (&KeyValuePair_2_t3672684370_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3672684370_0_0_0 = { 1, GenInst_KeyValuePair_2_t3672684370_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_KeyValuePair_2_t3672684370_0_0_0_Types[] = { (&String_t_0_0_0), (&ConstantClass_t4000559886_0_0_0), (&KeyValuePair_2_t3672684370_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_KeyValuePair_2_t3672684370_0_0_0 = { 3, GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_KeyValuePair_2_t3672684370_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t1620371852_0_0_0_Types[] = { (&Dictionary_2_t1620371852_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1620371852_0_0_0 = { 1, GenInst_Dictionary_2_t1620371852_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&Dictionary_2_t1620371852_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1315074971_0_0_0_Types[] = { (&KeyValuePair_2_t1315074971_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1315074971_0_0_0 = { 1, GenInst_KeyValuePair_2_t1315074971_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_KeyValuePair_2_t1315074971_0_0_0_Types[] = { (&Type_t_0_0_0), (&Dictionary_2_t1620371852_0_0_0), (&KeyValuePair_2_t1315074971_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_KeyValuePair_2_t1315074971_0_0_0 = { 3, GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_KeyValuePair_2_t1315074971_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_Types[] = { (&String_t_0_0_0), (&TimeSpan_t3430258949_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0 = { 2, GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&TimeSpan_t3430258949_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t779664299_0_0_0_Types[] = { (&KeyValuePair_2_t779664299_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t779664299_0_0_0 = { 1, GenInst_KeyValuePair_2_t779664299_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&TimeSpan_t3430258949_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_TimeSpan_t3430258949_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&TimeSpan_t3430258949_0_0_0), (&TimeSpan_t3430258949_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_TimeSpan_t3430258949_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_TimeSpan_t3430258949_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&TimeSpan_t3430258949_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_KeyValuePair_2_t779664299_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&TimeSpan_t3430258949_0_0_0), (&KeyValuePair_2_t779664299_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_KeyValuePair_2_t779664299_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_KeyValuePair_2_t779664299_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&TimeSpan_t3430258949_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3102383433_0_0_0_Types[] = { (&KeyValuePair_2_t3102383433_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3102383433_0_0_0 = { 1, GenInst_KeyValuePair_2_t3102383433_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_KeyValuePair_2_t3102383433_0_0_0_Types[] = { (&String_t_0_0_0), (&TimeSpan_t3430258949_0_0_0), (&KeyValuePair_2_t3102383433_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_KeyValuePair_2_t3102383433_0_0_0 = { 3, GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_KeyValuePair_2_t3102383433_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_Types[] = { (&String_t_0_0_0), (&RetryCapacity_t2794668894_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0 = { 2, GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_Types };
static const RuntimeType* GenInst_RetryCapacity_t2794668894_0_0_0_Types[] = { (&RetryCapacity_t2794668894_0_0_0) };
extern const Il2CppGenericInst GenInst_RetryCapacity_t2794668894_0_0_0 = { 1, GenInst_RetryCapacity_t2794668894_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&RetryCapacity_t2794668894_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2466793378_0_0_0_Types[] = { (&KeyValuePair_2_t2466793378_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2466793378_0_0_0 = { 1, GenInst_KeyValuePair_2_t2466793378_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_KeyValuePair_2_t2466793378_0_0_0_Types[] = { (&String_t_0_0_0), (&RetryCapacity_t2794668894_0_0_0), (&KeyValuePair_2_t2466793378_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_KeyValuePair_2_t2466793378_0_0_0 = { 3, GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_KeyValuePair_2_t2466793378_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_ParameterValue_t2426009594_0_0_0_Types[] = { (&String_t_0_0_0), (&ParameterValue_t2426009594_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ParameterValue_t2426009594_0_0_0 = { 2, GenInst_String_t_0_0_0_ParameterValue_t2426009594_0_0_0_Types };
static const RuntimeType* GenInst_ParameterValue_t2426009594_0_0_0_Types[] = { (&ParameterValue_t2426009594_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterValue_t2426009594_0_0_0 = { 1, GenInst_ParameterValue_t2426009594_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2098134078_0_0_0_Types[] = { (&KeyValuePair_2_t2098134078_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2098134078_0_0_0 = { 1, GenInst_KeyValuePair_2_t2098134078_0_0_0_Types };
static const RuntimeType* GenInst_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Stream_t3255436806_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_IRuntimePipelineCustomizer_t1222812694_0_0_0_Types[] = { (&IRuntimePipelineCustomizer_t1222812694_0_0_0) };
extern const Il2CppGenericInst GenInst_IRuntimePipelineCustomizer_t1222812694_0_0_0 = { 1, GenInst_IRuntimePipelineCustomizer_t1222812694_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_Types[] = { (&Type_t_0_0_0), (&IExceptionHandler_t1411014698_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0 = { 2, GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_Types };
static const RuntimeType* GenInst_IExceptionHandler_t1411014698_0_0_0_Types[] = { (&IExceptionHandler_t1411014698_0_0_0) };
extern const Il2CppGenericInst GenInst_IExceptionHandler_t1411014698_0_0_0 = { 1, GenInst_IExceptionHandler_t1411014698_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&IExceptionHandler_t1411014698_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1105717817_0_0_0_Types[] = { (&KeyValuePair_2_t1105717817_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1105717817_0_0_0 = { 1, GenInst_KeyValuePair_2_t1105717817_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_KeyValuePair_2_t1105717817_0_0_0_Types[] = { (&Type_t_0_0_0), (&IExceptionHandler_t1411014698_0_0_0), (&KeyValuePair_2_t1105717817_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_KeyValuePair_2_t1105717817_0_0_0 = { 3, GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_KeyValuePair_2_t1105717817_0_0_0_Types };
static const RuntimeType* GenInst_HttpErrorResponseException_t3191555406_0_0_0_Types[] = { (&HttpErrorResponseException_t3191555406_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpErrorResponseException_t3191555406_0_0_0 = { 1, GenInst_HttpErrorResponseException_t3191555406_0_0_0_Types };
static const RuntimeType* GenInst_HttpStatusCode_t1898409641_0_0_0_Types[] = { (&HttpStatusCode_t1898409641_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpStatusCode_t1898409641_0_0_0 = { 1, GenInst_HttpStatusCode_t1898409641_0_0_0_Types };
static const RuntimeType* GenInst_WebExceptionStatus_t1169373531_0_0_0_Types[] = { (&WebExceptionStatus_t1169373531_0_0_0) };
extern const Il2CppGenericInst GenInst_WebExceptionStatus_t1169373531_0_0_0 = { 1, GenInst_WebExceptionStatus_t1169373531_0_0_0_Types };
static const RuntimeType* GenInst_Link_t74093617_0_0_0_Types[] = { (&Link_t74093617_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t74093617_0_0_0 = { 1, GenInst_Link_t74093617_0_0_0_Types };
static const RuntimeType* GenInst_Link_t3640024803_0_0_0_Types[] = { (&Link_t3640024803_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t3640024803_0_0_0 = { 1, GenInst_Link_t3640024803_0_0_0_Types };
static const RuntimeType* GenInst_IAsyncExecutionContext_t3792344986_0_0_0_Types[] = { (&IAsyncExecutionContext_t3792344986_0_0_0) };
extern const Il2CppGenericInst GenInst_IAsyncExecutionContext_t3792344986_0_0_0 = { 1, GenInst_IAsyncExecutionContext_t3792344986_0_0_0_Types };
static const RuntimeType* GenInst_ThreadPoolOptions_1_t1583506835_0_0_0_Types[] = { (&ThreadPoolOptions_1_t1583506835_0_0_0) };
extern const Il2CppGenericInst GenInst_ThreadPoolOptions_1_t1583506835_0_0_0 = { 1, GenInst_ThreadPoolOptions_1_t1583506835_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t1927440687_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Exception_t1927440687_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_ThreadPoolOptions_1_t3222238215_0_0_0_Types[] = { (&ThreadPoolOptions_1_t3222238215_0_0_0) };
extern const Il2CppGenericInst GenInst_ThreadPoolOptions_1_t3222238215_0_0_0 = { 1, GenInst_ThreadPoolOptions_1_t3222238215_0_0_0_Types };
static const RuntimeType* GenInst_IAsyncExecutionContext_t3792344986_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0_Types[] = { (&IAsyncExecutionContext_t3792344986_0_0_0), (&IAsyncExecutionContext_t3792344986_0_0_0) };
extern const Il2CppGenericInst GenInst_IAsyncExecutionContext_t3792344986_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0 = { 2, GenInst_IAsyncExecutionContext_t3792344986_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t1927440687_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0_Types[] = { (&Exception_t1927440687_0_0_0), (&IAsyncExecutionContext_t3792344986_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0_Types };
static const RuntimeType* GenInst_IUnityHttpRequest_t1859903397_0_0_0_Types[] = { (&IUnityHttpRequest_t1859903397_0_0_0) };
extern const Il2CppGenericInst GenInst_IUnityHttpRequest_t1859903397_0_0_0 = { 1, GenInst_IUnityHttpRequest_t1859903397_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeAsyncResult_t4279356013_0_0_0_Types[] = { (&RuntimeAsyncResult_t4279356013_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeAsyncResult_t4279356013_0_0_0 = { 1, GenInst_RuntimeAsyncResult_t4279356013_0_0_0_Types };
static const RuntimeType* GenInst_IAsyncResult_t1999651008_0_0_0_Types[] = { (&IAsyncResult_t1999651008_0_0_0) };
extern const Il2CppGenericInst GenInst_IAsyncResult_t1999651008_0_0_0 = { 1, GenInst_IAsyncResult_t1999651008_0_0_0_Types };
static const RuntimeType* GenInst_HashingWrapperMD5_t3486550061_0_0_0_Types[] = { (&HashingWrapperMD5_t3486550061_0_0_0) };
extern const Il2CppGenericInst GenInst_HashingWrapperMD5_t3486550061_0_0_0 = { 1, GenInst_HashingWrapperMD5_t3486550061_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_Types[] = { (&Type_t_0_0_0), (&Logger_t2262497814_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0 = { 2, GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_Types };
static const RuntimeType* GenInst_InternalLogger_t2972373343_0_0_0_Types[] = { (&InternalLogger_t2972373343_0_0_0) };
extern const Il2CppGenericInst GenInst_InternalLogger_t2972373343_0_0_0 = { 1, GenInst_InternalLogger_t2972373343_0_0_0_Types };
static const RuntimeType* GenInst_Logger_t2262497814_0_0_0_Types[] = { (&Logger_t2262497814_0_0_0) };
extern const Il2CppGenericInst GenInst_Logger_t2262497814_0_0_0 = { 1, GenInst_Logger_t2262497814_0_0_0_Types };
static const RuntimeType* GenInst_ILogger_t676853571_0_0_0_Types[] = { (&ILogger_t676853571_0_0_0) };
extern const Il2CppGenericInst GenInst_ILogger_t676853571_0_0_0 = { 1, GenInst_ILogger_t676853571_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Type_t_0_0_0), (&Logger_t2262497814_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1957200933_0_0_0_Types[] = { (&KeyValuePair_2_t1957200933_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1957200933_0_0_0 = { 1, GenInst_KeyValuePair_2_t1957200933_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_KeyValuePair_2_t1957200933_0_0_0_Types[] = { (&Type_t_0_0_0), (&Logger_t2262497814_0_0_0), (&KeyValuePair_2_t1957200933_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_KeyValuePair_2_t1957200933_0_0_0 = { 3, GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_KeyValuePair_2_t1957200933_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&LruListItem_2_t38602651_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_Types };
static const RuntimeType* GenInst_LruListItem_2_t38602651_0_0_0_Types[] = { (&LruListItem_2_t38602651_0_0_0) };
extern const Il2CppGenericInst GenInst_LruListItem_2_t38602651_0_0_0 = { 1, GenInst_LruListItem_2_t38602651_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&LruListItem_2_t38602651_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1682975297_0_0_0_Types[] = { (&KeyValuePair_2_t1682975297_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1682975297_0_0_0 = { 1, GenInst_KeyValuePair_2_t1682975297_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_KeyValuePair_2_t1682975297_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&LruListItem_2_t38602651_0_0_0), (&KeyValuePair_2_t1682975297_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_KeyValuePair_2_t1682975297_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_KeyValuePair_2_t1682975297_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&List_1_t2058570427_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3196873006_0_0_0_Types[] = { (&KeyValuePair_2_t3196873006_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3196873006_0_0_0 = { 1, GenInst_KeyValuePair_2_t3196873006_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0 = { 1, GenInst_Metric_t3273440202_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_Metric_t3273440202_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&RuntimeObject_0_0_0), (&Metric_t3273440202_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_Metric_t3273440202_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_Metric_t3273440202_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3196873006_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t3196873006_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3196873006_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3196873006_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2058570427_0_0_0_Types[] = { (&List_1_t2058570427_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2058570427_0_0_0 = { 1, GenInst_List_1_t2058570427_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&List_1_t2058570427_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2565994138_0_0_0_Types[] = { (&KeyValuePair_2_t2565994138_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2565994138_0_0_0 = { 1, GenInst_KeyValuePair_2_t2565994138_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_KeyValuePair_2_t2565994138_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&List_1_t2058570427_0_0_0), (&KeyValuePair_2_t2565994138_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_KeyValuePair_2_t2565994138_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_KeyValuePair_2_t2565994138_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&List_1_t3837499352_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_Types };
static const RuntimeType* GenInst_IMetricsTiming_t173410924_0_0_0_Types[] = { (&IMetricsTiming_t173410924_0_0_0) };
extern const Il2CppGenericInst GenInst_IMetricsTiming_t173410924_0_0_0 = { 1, GenInst_IMetricsTiming_t173410924_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3837499352_0_0_0_Types[] = { (&List_1_t3837499352_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3837499352_0_0_0 = { 1, GenInst_List_1_t3837499352_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&List_1_t3837499352_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t49955767_0_0_0_Types[] = { (&KeyValuePair_2_t49955767_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t49955767_0_0_0 = { 1, GenInst_KeyValuePair_2_t49955767_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_KeyValuePair_2_t49955767_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&List_1_t3837499352_0_0_0), (&KeyValuePair_2_t49955767_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_KeyValuePair_2_t49955767_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_KeyValuePair_2_t49955767_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&Int64_t909078037_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1416501748_0_0_0_Types[] = { (&KeyValuePair_2_t1416501748_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1416501748_0_0_0 = { 1, GenInst_KeyValuePair_2_t1416501748_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Metric_t3273440202_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&Int64_t909078037_0_0_0), (&Metric_t3273440202_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Metric_t3273440202_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Metric_t3273440202_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&Int64_t909078037_0_0_0), (&Int64_t909078037_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&Int64_t909078037_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1416501748_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&Int64_t909078037_0_0_0), (&KeyValuePair_2_t1416501748_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1416501748_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1416501748_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&Timing_t847194262_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_Types };
static const RuntimeType* GenInst_Timing_t847194262_0_0_0_Types[] = { (&Timing_t847194262_0_0_0) };
extern const Il2CppGenericInst GenInst_Timing_t847194262_0_0_0 = { 1, GenInst_Timing_t847194262_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&Timing_t847194262_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1354617973_0_0_0_Types[] = { (&KeyValuePair_2_t1354617973_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1354617973_0_0_0 = { 1, GenInst_KeyValuePair_2_t1354617973_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_KeyValuePair_2_t1354617973_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&Timing_t847194262_0_0_0), (&KeyValuePair_2_t1354617973_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_KeyValuePair_2_t1354617973_0_0_0 = { 3, GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_KeyValuePair_2_t1354617973_0_0_0_Types };
static const RuntimeType* GenInst_MetricError_t964444806_0_0_0_Types[] = { (&MetricError_t964444806_0_0_0) };
extern const Il2CppGenericInst GenInst_MetricError_t964444806_0_0_0 = { 1, GenInst_MetricError_t964444806_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_String_t_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_String_t_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_AmazonWebServiceResponse_t529043356_0_0_0_UnmarshallerContext_t1608322995_0_0_0_Types[] = { (&AmazonWebServiceResponse_t529043356_0_0_0), (&UnmarshallerContext_t1608322995_0_0_0) };
extern const Il2CppGenericInst GenInst_AmazonWebServiceResponse_t529043356_0_0_0_UnmarshallerContext_t1608322995_0_0_0 = { 2, GenInst_AmazonWebServiceResponse_t529043356_0_0_0_UnmarshallerContext_t1608322995_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&XmlUnmarshallerContext_t1179575220_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&JsonUnmarshallerContext_t456235889_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&XmlUnmarshallerContext_t1179575220_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&JsonUnmarshallerContext_t456235889_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&XmlUnmarshallerContext_t1179575220_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&JsonUnmarshallerContext_t456235889_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { (&String_t_0_0_0), (&XmlUnmarshallerContext_t1179575220_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_String_t_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { (&String_t_0_0_0), (&JsonUnmarshallerContext_t456235889_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_String_t_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t693205669_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { (&DateTime_t693205669_0_0_0), (&XmlUnmarshallerContext_t1179575220_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_DateTime_t693205669_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t693205669_0_0_0_RuntimeObject_0_0_0_Types[] = { (&DateTime_t693205669_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_DateTime_t693205669_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t693205669_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { (&DateTime_t693205669_0_0_0), (&JsonUnmarshallerContext_t456235889_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t693205669_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_DateTime_t693205669_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const RuntimeType* GenInst_XmlNodeType_t739504597_0_0_0_Types[] = { (&XmlNodeType_t739504597_0_0_0) };
extern const Il2CppGenericInst GenInst_XmlNodeType_t739504597_0_0_0 = { 1, GenInst_XmlNodeType_t739504597_0_0_0_Types };
static const RuntimeType* GenInst_Link_t3210155869_0_0_0_Types[] = { (&Link_t3210155869_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t3210155869_0_0_0 = { 1, GenInst_Link_t3210155869_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1701344717_0_0_0_String_t_0_0_0_Types[] = { (&KeyValuePair_2_t1701344717_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0_String_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1701344717_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t38854645_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t38854645_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1701344717_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&KeyValuePair_2_t1701344717_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1701344717_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t1701344717_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&KeyValuePair_2_t38854645_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_EventSystem_t3466835263_0_0_0_Types[] = { (&EventSystem_t3466835263_0_0_0) };
extern const Il2CppGenericInst GenInst_EventSystem_t3466835263_0_0_0 = { 1, GenInst_EventSystem_t3466835263_0_0_0_Types };
static const RuntimeType* GenInst_UIBehaviour_t3960014691_0_0_0_Types[] = { (&UIBehaviour_t3960014691_0_0_0) };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3960014691_0_0_0 = { 1, GenInst_UIBehaviour_t3960014691_0_0_0_Types };
static const RuntimeType* GenInst_MonoBehaviour_t1158329972_0_0_0_Types[] = { (&MonoBehaviour_t1158329972_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t1158329972_0_0_0 = { 1, GenInst_MonoBehaviour_t1158329972_0_0_0_Types };
static const RuntimeType* GenInst_BaseInputModule_t1295781545_0_0_0_Types[] = { (&BaseInputModule_t1295781545_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInputModule_t1295781545_0_0_0 = { 1, GenInst_BaseInputModule_t1295781545_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t21186376_0_0_0_Types[] = { (&RaycastResult_t21186376_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0 = { 1, GenInst_RaycastResult_t21186376_0_0_0_Types };
static const RuntimeType* GenInst_IDeselectHandler_t3182198310_0_0_0_Types[] = { (&IDeselectHandler_t3182198310_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t3182198310_0_0_0 = { 1, GenInst_IDeselectHandler_t3182198310_0_0_0_Types };
static const RuntimeType* GenInst_IEventSystemHandler_t2741188318_0_0_0_Types[] = { (&IEventSystemHandler_t2741188318_0_0_0) };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t2741188318_0_0_0 = { 1, GenInst_IEventSystemHandler_t2741188318_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2110309450_0_0_0_Types[] = { (&List_1_t2110309450_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2110309450_0_0_0 = { 1, GenInst_List_1_t2110309450_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3188497603_0_0_0_Types[] = { (&List_1_t3188497603_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3188497603_0_0_0 = { 1, GenInst_List_1_t3188497603_0_0_0_Types };
static const RuntimeType* GenInst_ISelectHandler_t2812555161_0_0_0_Types[] = { (&ISelectHandler_t2812555161_0_0_0) };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2812555161_0_0_0 = { 1, GenInst_ISelectHandler_t2812555161_0_0_0_Types };
static const RuntimeType* GenInst_BaseRaycaster_t2336171397_0_0_0_Types[] = { (&BaseRaycaster_t2336171397_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t2336171397_0_0_0 = { 1, GenInst_BaseRaycaster_t2336171397_0_0_0_Types };
static const RuntimeType* GenInst_Entry_t3365010046_0_0_0_Types[] = { (&Entry_t3365010046_0_0_0) };
extern const Il2CppGenericInst GenInst_Entry_t3365010046_0_0_0 = { 1, GenInst_Entry_t3365010046_0_0_0_Types };
static const RuntimeType* GenInst_BaseEventData_t2681005625_0_0_0_Types[] = { (&BaseEventData_t2681005625_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseEventData_t2681005625_0_0_0 = { 1, GenInst_BaseEventData_t2681005625_0_0_0_Types };
static const RuntimeType* GenInst_IPointerEnterHandler_t193164956_0_0_0_Types[] = { (&IPointerEnterHandler_t193164956_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t193164956_0_0_0 = { 1, GenInst_IPointerEnterHandler_t193164956_0_0_0_Types };
static const RuntimeType* GenInst_IPointerExitHandler_t461019860_0_0_0_Types[] = { (&IPointerExitHandler_t461019860_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t461019860_0_0_0 = { 1, GenInst_IPointerExitHandler_t461019860_0_0_0_Types };
static const RuntimeType* GenInst_IPointerDownHandler_t3929046918_0_0_0_Types[] = { (&IPointerDownHandler_t3929046918_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t3929046918_0_0_0 = { 1, GenInst_IPointerDownHandler_t3929046918_0_0_0_Types };
static const RuntimeType* GenInst_IPointerUpHandler_t1847764461_0_0_0_Types[] = { (&IPointerUpHandler_t1847764461_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t1847764461_0_0_0 = { 1, GenInst_IPointerUpHandler_t1847764461_0_0_0_Types };
static const RuntimeType* GenInst_IPointerClickHandler_t96169666_0_0_0_Types[] = { (&IPointerClickHandler_t96169666_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t96169666_0_0_0 = { 1, GenInst_IPointerClickHandler_t96169666_0_0_0_Types };
static const RuntimeType* GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types[] = { (&IInitializePotentialDragHandler_t3350809087_0_0_0) };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0_Types };
static const RuntimeType* GenInst_IBeginDragHandler_t3135127860_0_0_0_Types[] = { (&IBeginDragHandler_t3135127860_0_0_0) };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3135127860_0_0_0 = { 1, GenInst_IBeginDragHandler_t3135127860_0_0_0_Types };
static const RuntimeType* GenInst_IDragHandler_t2583993319_0_0_0_Types[] = { (&IDragHandler_t2583993319_0_0_0) };
extern const Il2CppGenericInst GenInst_IDragHandler_t2583993319_0_0_0 = { 1, GenInst_IDragHandler_t2583993319_0_0_0_Types };
static const RuntimeType* GenInst_IEndDragHandler_t1349123600_0_0_0_Types[] = { (&IEndDragHandler_t1349123600_0_0_0) };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t1349123600_0_0_0 = { 1, GenInst_IEndDragHandler_t1349123600_0_0_0_Types };
static const RuntimeType* GenInst_IDropHandler_t2390101210_0_0_0_Types[] = { (&IDropHandler_t2390101210_0_0_0) };
extern const Il2CppGenericInst GenInst_IDropHandler_t2390101210_0_0_0 = { 1, GenInst_IDropHandler_t2390101210_0_0_0_Types };
static const RuntimeType* GenInst_IScrollHandler_t3834677510_0_0_0_Types[] = { (&IScrollHandler_t3834677510_0_0_0) };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3834677510_0_0_0 = { 1, GenInst_IScrollHandler_t3834677510_0_0_0_Types };
static const RuntimeType* GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types[] = { (&IUpdateSelectedHandler_t3778909353_0_0_0) };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3778909353_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3778909353_0_0_0_Types };
static const RuntimeType* GenInst_IMoveHandler_t2611925506_0_0_0_Types[] = { (&IMoveHandler_t2611925506_0_0_0) };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2611925506_0_0_0 = { 1, GenInst_IMoveHandler_t2611925506_0_0_0_Types };
static const RuntimeType* GenInst_ISubmitHandler_t525803901_0_0_0_Types[] = { (&ISubmitHandler_t525803901_0_0_0) };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t525803901_0_0_0 = { 1, GenInst_ISubmitHandler_t525803901_0_0_0_Types };
static const RuntimeType* GenInst_ICancelHandler_t1980319651_0_0_0_Types[] = { (&ICancelHandler_t1980319651_0_0_0) };
extern const Il2CppGenericInst GenInst_ICancelHandler_t1980319651_0_0_0 = { 1, GenInst_ICancelHandler_t1980319651_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t3275118058_0_0_0_Types[] = { (&Transform_t3275118058_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t3275118058_0_0_0 = { 1, GenInst_Transform_t3275118058_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_t1756533147_0_0_0_Types[] = { (&GameObject_t1756533147_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0 = { 1, GenInst_GameObject_t1756533147_0_0_0_Types };
static const RuntimeType* GenInst_BaseInput_t621514313_0_0_0_Types[] = { (&BaseInput_t621514313_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInput_t621514313_0_0_0 = { 1, GenInst_BaseInput_t621514313_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&PointerEventData_t1599784723_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const RuntimeType* GenInst_PointerEventData_t1599784723_0_0_0_Types[] = { (&PointerEventData_t1599784723_0_0_0) };
extern const Il2CppGenericInst GenInst_PointerEventData_t1599784723_0_0_0 = { 1, GenInst_PointerEventData_t1599784723_0_0_0_Types };
static const RuntimeType* GenInst_AbstractEventData_t1333959294_0_0_0_Types[] = { (&AbstractEventData_t1333959294_0_0_0) };
extern const Il2CppGenericInst GenInst_AbstractEventData_t1333959294_0_0_0 = { 1, GenInst_AbstractEventData_t1333959294_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&PointerEventData_t1599784723_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2659922876_0_0_0_Types[] = { (&KeyValuePair_2_t2659922876_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2659922876_0_0_0 = { 1, GenInst_KeyValuePair_2_t2659922876_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&PointerEventData_t1599784723_0_0_0), (&KeyValuePair_2_t2659922876_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&PointerEventData_t1599784723_0_0_0), (&PointerEventData_t1599784723_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0_Types };
static const RuntimeType* GenInst_ButtonState_t2688375492_0_0_0_Types[] = { (&ButtonState_t2688375492_0_0_0) };
extern const Il2CppGenericInst GenInst_ButtonState_t2688375492_0_0_0 = { 1, GenInst_ButtonState_t2688375492_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit2D_t4063908774_0_0_0_Types[] = { (&RaycastHit2D_t4063908774_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t4063908774_0_0_0 = { 1, GenInst_RaycastHit2D_t4063908774_0_0_0_Types };
static const RuntimeType* GenInst_Color_t2020392075_0_0_0_Types[] = { (&Color_t2020392075_0_0_0) };
extern const Il2CppGenericInst GenInst_Color_t2020392075_0_0_0 = { 1, GenInst_Color_t2020392075_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t986520779_0_0_0_Types[] = { (&ICanvasElement_t986520779_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0 = { 1, GenInst_ICanvasElement_t986520779_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&ICanvasElement_t986520779_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&ICanvasElement_t986520779_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_ColorBlock_t2652774230_0_0_0_Types[] = { (&ColorBlock_t2652774230_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorBlock_t2652774230_0_0_0 = { 1, GenInst_ColorBlock_t2652774230_0_0_0_Types };
static const RuntimeType* GenInst_OptionData_t2420267500_0_0_0_Types[] = { (&OptionData_t2420267500_0_0_0) };
extern const Il2CppGenericInst GenInst_OptionData_t2420267500_0_0_0 = { 1, GenInst_OptionData_t2420267500_0_0_0_Types };
static const RuntimeType* GenInst_DropdownItem_t4139978805_0_0_0_Types[] = { (&DropdownItem_t4139978805_0_0_0) };
extern const Il2CppGenericInst GenInst_DropdownItem_t4139978805_0_0_0 = { 1, GenInst_DropdownItem_t4139978805_0_0_0_Types };
static const RuntimeType* GenInst_FloatTween_t2986189219_0_0_0_Types[] = { (&FloatTween_t2986189219_0_0_0) };
extern const Il2CppGenericInst GenInst_FloatTween_t2986189219_0_0_0 = { 1, GenInst_FloatTween_t2986189219_0_0_0_Types };
static const RuntimeType* GenInst_Sprite_t309593783_0_0_0_Types[] = { (&Sprite_t309593783_0_0_0) };
extern const Il2CppGenericInst GenInst_Sprite_t309593783_0_0_0 = { 1, GenInst_Sprite_t309593783_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0 = { 1, GenInst_Canvas_t209405766_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3873494194_0_0_0_Types[] = { (&List_1_t3873494194_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3873494194_0_0_0 = { 1, GenInst_List_1_t3873494194_0_0_0_Types };
static const RuntimeType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types[] = { (&Font_t4239498691_0_0_0), (&HashSet_1_t2984649583_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0 = { 2, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_Types };
static const RuntimeType* GenInst_Text_t356221433_0_0_0_Types[] = { (&Text_t356221433_0_0_0) };
extern const Il2CppGenericInst GenInst_Text_t356221433_0_0_0 = { 1, GenInst_Text_t356221433_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2826872705_0_0_0_Types[] = { (&Link_t2826872705_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2826872705_0_0_0 = { 1, GenInst_Link_t2826872705_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t1975293769_0_0_0_Types[] = { (&ILayoutElement_t1975293769_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0 = { 1, GenInst_ILayoutElement_t1975293769_0_0_0_Types };
static const RuntimeType* GenInst_MaskableGraphic_t540192618_0_0_0_Types[] = { (&MaskableGraphic_t540192618_0_0_0) };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t540192618_0_0_0 = { 1, GenInst_MaskableGraphic_t540192618_0_0_0_Types };
static const RuntimeType* GenInst_IClippable_t1941276057_0_0_0_Types[] = { (&IClippable_t1941276057_0_0_0) };
extern const Il2CppGenericInst GenInst_IClippable_t1941276057_0_0_0 = { 1, GenInst_IClippable_t1941276057_0_0_0_Types };
static const RuntimeType* GenInst_IMaskable_t1431842707_0_0_0_Types[] = { (&IMaskable_t1431842707_0_0_0) };
extern const Il2CppGenericInst GenInst_IMaskable_t1431842707_0_0_0 = { 1, GenInst_IMaskable_t1431842707_0_0_0_Types };
static const RuntimeType* GenInst_IMaterialModifier_t3028564983_0_0_0_Types[] = { (&IMaterialModifier_t3028564983_0_0_0) };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t3028564983_0_0_0 = { 1, GenInst_IMaterialModifier_t3028564983_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2426225576_0_0_0_Types[] = { (&Graphic_t2426225576_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0 = { 1, GenInst_Graphic_t2426225576_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t2984649583_0_0_0_Types[] = { (&HashSet_1_t2984649583_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t2984649583_0_0_0 = { 1, GenInst_HashSet_1_t2984649583_0_0_0_Types };
static const RuntimeType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Font_t4239498691_0_0_0), (&HashSet_1_t2984649583_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t850112849_0_0_0_Types[] = { (&KeyValuePair_2_t850112849_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t850112849_0_0_0 = { 1, GenInst_KeyValuePair_2_t850112849_0_0_0_Types };
static const RuntimeType* GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0_Types[] = { (&Font_t4239498691_0_0_0), (&HashSet_1_t2984649583_0_0_0), (&KeyValuePair_2_t850112849_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0 = { 3, GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0_Types };
static const RuntimeType* GenInst_ColorTween_t3438117476_0_0_0_Types[] = { (&ColorTween_t3438117476_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorTween_t3438117476_0_0_0 = { 1, GenInst_ColorTween_t3438117476_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0), (&IndexedSet_1_t286373651_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0 = { 2, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Graphic_t2426225576_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Graphic_t2426225576_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t286373651_0_0_0_Types[] = { (&IndexedSet_1_t286373651_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t286373651_0_0_0 = { 1, GenInst_IndexedSet_1_t286373651_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0), (&IndexedSet_1_t286373651_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2391682566_0_0_0_Types[] = { (&KeyValuePair_2_t2391682566_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2391682566_0_0_0 = { 1, GenInst_KeyValuePair_2_t2391682566_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0_Types[] = { (&Canvas_t209405766_0_0_0), (&IndexedSet_1_t286373651_0_0_0), (&KeyValuePair_2_t2391682566_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0 = { 3, GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3010968081_0_0_0_Types[] = { (&KeyValuePair_2_t3010968081_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3010968081_0_0_0 = { 1, GenInst_KeyValuePair_2_t3010968081_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0_Types[] = { (&Graphic_t2426225576_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t3010968081_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0 = { 3, GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1912381698_0_0_0_Types[] = { (&KeyValuePair_2_t1912381698_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1912381698_0_0_0 = { 1, GenInst_KeyValuePair_2_t1912381698_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0_Types[] = { (&ICanvasElement_t986520779_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t1912381698_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0 = { 3, GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0_Types };
static const RuntimeType* GenInst_Type_t3352948571_0_0_0_Types[] = { (&Type_t3352948571_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t3352948571_0_0_0 = { 1, GenInst_Type_t3352948571_0_0_0_Types };
static const RuntimeType* GenInst_FillMethod_t1640962579_0_0_0_Types[] = { (&FillMethod_t1640962579_0_0_0) };
extern const Il2CppGenericInst GenInst_FillMethod_t1640962579_0_0_0 = { 1, GenInst_FillMethod_t1640962579_0_0_0_Types };
static const RuntimeType* GenInst_ContentType_t1028629049_0_0_0_Types[] = { (&ContentType_t1028629049_0_0_0) };
extern const Il2CppGenericInst GenInst_ContentType_t1028629049_0_0_0 = { 1, GenInst_ContentType_t1028629049_0_0_0_Types };
static const RuntimeType* GenInst_LineType_t2931319356_0_0_0_Types[] = { (&LineType_t2931319356_0_0_0) };
extern const Il2CppGenericInst GenInst_LineType_t2931319356_0_0_0 = { 1, GenInst_LineType_t2931319356_0_0_0_Types };
static const RuntimeType* GenInst_InputType_t1274231802_0_0_0_Types[] = { (&InputType_t1274231802_0_0_0) };
extern const Il2CppGenericInst GenInst_InputType_t1274231802_0_0_0 = { 1, GenInst_InputType_t1274231802_0_0_0_Types };
static const RuntimeType* GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types[] = { (&TouchScreenKeyboardType_t875112366_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t875112366_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t875112366_0_0_0_Types };
static const RuntimeType* GenInst_CharacterValidation_t3437478890_0_0_0_Types[] = { (&CharacterValidation_t3437478890_0_0_0) };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3437478890_0_0_0 = { 1, GenInst_CharacterValidation_t3437478890_0_0_0_Types };
static const RuntimeType* GenInst_Mask_t2977958238_0_0_0_Types[] = { (&Mask_t2977958238_0_0_0) };
extern const Il2CppGenericInst GenInst_Mask_t2977958238_0_0_0 = { 1, GenInst_Mask_t2977958238_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasRaycastFilter_t1367822892_0_0_0_Types[] = { (&ICanvasRaycastFilter_t1367822892_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t1367822892_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t1367822892_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2347079370_0_0_0_Types[] = { (&List_1_t2347079370_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2347079370_0_0_0 = { 1, GenInst_List_1_t2347079370_0_0_0_Types };
static const RuntimeType* GenInst_RectMask2D_t1156185964_0_0_0_Types[] = { (&RectMask2D_t1156185964_0_0_0) };
extern const Il2CppGenericInst GenInst_RectMask2D_t1156185964_0_0_0 = { 1, GenInst_RectMask2D_t1156185964_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t900477982_0_0_0_Types[] = { (&IClipper_t900477982_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0 = { 1, GenInst_IClipper_t900477982_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t525307096_0_0_0_Types[] = { (&List_1_t525307096_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t525307096_0_0_0 = { 1, GenInst_List_1_t525307096_0_0_0_Types };
static const RuntimeType* GenInst_Navigation_t1571958496_0_0_0_Types[] = { (&Navigation_t1571958496_0_0_0) };
extern const Il2CppGenericInst GenInst_Navigation_t1571958496_0_0_0 = { 1, GenInst_Navigation_t1571958496_0_0_0_Types };
static const RuntimeType* GenInst_Link_t116960033_0_0_0_Types[] = { (&Link_t116960033_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t116960033_0_0_0 = { 1, GenInst_Link_t116960033_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t3696775921_0_0_0_Types[] = { (&Direction_t3696775921_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t3696775921_0_0_0 = { 1, GenInst_Direction_t3696775921_0_0_0_Types };
static const RuntimeType* GenInst_Selectable_t1490392188_0_0_0_Types[] = { (&Selectable_t1490392188_0_0_0) };
extern const Il2CppGenericInst GenInst_Selectable_t1490392188_0_0_0 = { 1, GenInst_Selectable_t1490392188_0_0_0_Types };
static const RuntimeType* GenInst_Transition_t605142169_0_0_0_Types[] = { (&Transition_t605142169_0_0_0) };
extern const Il2CppGenericInst GenInst_Transition_t605142169_0_0_0 = { 1, GenInst_Transition_t605142169_0_0_0_Types };
static const RuntimeType* GenInst_SpriteState_t1353336012_0_0_0_Types[] = { (&SpriteState_t1353336012_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteState_t1353336012_0_0_0 = { 1, GenInst_SpriteState_t1353336012_0_0_0_Types };
static const RuntimeType* GenInst_CanvasGroup_t3296560743_0_0_0_Types[] = { (&CanvasGroup_t3296560743_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasGroup_t3296560743_0_0_0 = { 1, GenInst_CanvasGroup_t3296560743_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t1525323322_0_0_0_Types[] = { (&Direction_t1525323322_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t1525323322_0_0_0 = { 1, GenInst_Direction_t1525323322_0_0_0_Types };
static const RuntimeType* GenInst_MatEntry_t3157325053_0_0_0_Types[] = { (&MatEntry_t3157325053_0_0_0) };
extern const Il2CppGenericInst GenInst_MatEntry_t3157325053_0_0_0 = { 1, GenInst_MatEntry_t3157325053_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t3976754468_0_0_0_Types[] = { (&Toggle_t3976754468_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0 = { 1, GenInst_Toggle_t3976754468_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Toggle_t3976754468_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&IClipper_t900477982_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&IClipper_t900477982_0_0_0), (&Int32_t2071877448_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t379984643_0_0_0_Types[] = { (&KeyValuePair_2_t379984643_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379984643_0_0_0 = { 1, GenInst_KeyValuePair_2_t379984643_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0_Types[] = { (&IClipper_t900477982_0_0_0), (&Int32_t2071877448_0_0_0), (&KeyValuePair_2_t379984643_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0 = { 3, GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0_Types };
static const RuntimeType* GenInst_AspectMode_t1166448724_0_0_0_Types[] = { (&AspectMode_t1166448724_0_0_0) };
extern const Il2CppGenericInst GenInst_AspectMode_t1166448724_0_0_0 = { 1, GenInst_AspectMode_t1166448724_0_0_0_Types };
static const RuntimeType* GenInst_FitMode_t4030874534_0_0_0_Types[] = { (&FitMode_t4030874534_0_0_0) };
extern const Il2CppGenericInst GenInst_FitMode_t4030874534_0_0_0 = { 1, GenInst_FitMode_t4030874534_0_0_0_Types };
static const RuntimeType* GenInst_RectTransform_t3349966182_0_0_0_Types[] = { (&RectTransform_t3349966182_0_0_0) };
extern const Il2CppGenericInst GenInst_RectTransform_t3349966182_0_0_0 = { 1, GenInst_RectTransform_t3349966182_0_0_0_Types };
static const RuntimeType* GenInst_LayoutRebuilder_t2155218138_0_0_0_Types[] = { (&LayoutRebuilder_t2155218138_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t2155218138_0_0_0 = { 1, GenInst_LayoutRebuilder_t2155218138_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types[] = { (&ILayoutElement_t1975293769_0_0_0), (&Single_t2076509932_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t2076509932_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t2076509932_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Single_t2076509932_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1612828712_0_0_0_Types[] = { (&List_1_t1612828712_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1612828712_0_0_0 = { 1, GenInst_List_1_t1612828712_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t243638650_0_0_0_Types[] = { (&List_1_t243638650_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t243638650_0_0_0 = { 1, GenInst_List_1_t243638650_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1612828711_0_0_0_Types[] = { (&List_1_t1612828711_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1612828711_0_0_0 = { 1, GenInst_List_1_t1612828711_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1612828713_0_0_0_Types[] = { (&List_1_t1612828713_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1612828713_0_0_0 = { 1, GenInst_List_1_t1612828713_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1440998580_0_0_0_Types[] = { (&List_1_t1440998580_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1440998580_0_0_0 = { 1, GenInst_List_1_t1440998580_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t573379950_0_0_0_Types[] = { (&List_1_t573379950_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t573379950_0_0_0 = { 1, GenInst_List_1_t573379950_0_0_0_Types };
static const RuntimeType* GenInst_GetObjectRequest_t109865576_0_0_0_GetObjectResponse_t653598534_0_0_0_Types[] = { (&GetObjectRequest_t109865576_0_0_0), (&GetObjectResponse_t653598534_0_0_0) };
extern const Il2CppGenericInst GenInst_GetObjectRequest_t109865576_0_0_0_GetObjectResponse_t653598534_0_0_0 = { 2, GenInst_GetObjectRequest_t109865576_0_0_0_GetObjectResponse_t653598534_0_0_0_Types };
static const RuntimeType* GenInst_ListObjectsRequest_t3715455147_0_0_0_ListObjectsResponse_t2587169543_0_0_0_Types[] = { (&ListObjectsRequest_t3715455147_0_0_0), (&ListObjectsResponse_t2587169543_0_0_0) };
extern const Il2CppGenericInst GenInst_ListObjectsRequest_t3715455147_0_0_0_ListObjectsResponse_t2587169543_0_0_0 = { 2, GenInst_ListObjectsRequest_t3715455147_0_0_0_ListObjectsResponse_t2587169543_0_0_0_Types };
static const RuntimeType* GenInst_DeletedObject_t2664222218_0_0_0_Types[] = { (&DeletedObject_t2664222218_0_0_0) };
extern const Il2CppGenericInst GenInst_DeletedObject_t2664222218_0_0_0 = { 1, GenInst_DeletedObject_t2664222218_0_0_0_Types };
static const RuntimeType* GenInst_DeleteError_t3399861423_0_0_0_Types[] = { (&DeleteError_t3399861423_0_0_0) };
extern const Il2CppGenericInst GenInst_DeleteError_t3399861423_0_0_0 = { 1, GenInst_DeleteError_t3399861423_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_LruListItem_2_t333395295_0_0_0_Types[] = { (&String_t_0_0_0), (&LruListItem_2_t333395295_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LruListItem_2_t333395295_0_0_0 = { 2, GenInst_String_t_0_0_0_LruListItem_2_t333395295_0_0_0_Types };
static const RuntimeType* GenInst_LruListItem_2_t333395295_0_0_0_Types[] = { (&LruListItem_2_t333395295_0_0_0) };
extern const Il2CppGenericInst GenInst_LruListItem_2_t333395295_0_0_0 = { 1, GenInst_LruListItem_2_t333395295_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_LruListItem_2_t333395295_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&String_t_0_0_0), (&LruListItem_2_t333395295_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_LruListItem_2_t333395295_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_String_t_0_0_0_LruListItem_2_t333395295_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_S3Object_t1836716019_0_0_0_Types[] = { (&S3Object_t1836716019_0_0_0) };
extern const Il2CppGenericInst GenInst_S3Object_t1836716019_0_0_0 = { 1, GenInst_S3Object_t1836716019_0_0_0_Types };
static const RuntimeType* GenInst_S3Object_t1836716019_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { (&S3Object_t1836716019_0_0_0), (&XmlUnmarshallerContext_t1179575220_0_0_0) };
extern const Il2CppGenericInst GenInst_S3Object_t1836716019_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_S3Object_t1836716019_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const RuntimeType* GenInst_S3Object_t1836716019_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { (&S3Object_t1836716019_0_0_0), (&JsonUnmarshallerContext_t456235889_0_0_0) };
extern const Il2CppGenericInst GenInst_S3Object_t1836716019_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_S3Object_t1836716019_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const RuntimeType* GenInst_IRequest_t2400804350_0_0_0_GetObjectRequest_t109865576_0_0_0_Types[] = { (&IRequest_t2400804350_0_0_0), (&GetObjectRequest_t109865576_0_0_0) };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_GetObjectRequest_t109865576_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_GetObjectRequest_t109865576_0_0_0_Types };
static const RuntimeType* GenInst_IRequest_t2400804350_0_0_0_ListObjectsRequest_t3715455147_0_0_0_Types[] = { (&IRequest_t2400804350_0_0_0), (&ListObjectsRequest_t3715455147_0_0_0) };
extern const Il2CppGenericInst GenInst_IRequest_t2400804350_0_0_0_ListObjectsRequest_t3715455147_0_0_0 = { 2, GenInst_IRequest_t2400804350_0_0_0_ListObjectsRequest_t3715455147_0_0_0_Types };
static const RuntimeType* GenInst_Owner_t97740679_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { (&Owner_t97740679_0_0_0), (&XmlUnmarshallerContext_t1179575220_0_0_0) };
extern const Il2CppGenericInst GenInst_Owner_t97740679_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_Owner_t97740679_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const RuntimeType* GenInst_Owner_t97740679_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types[] = { (&Owner_t97740679_0_0_0), (&JsonUnmarshallerContext_t456235889_0_0_0) };
extern const Il2CppGenericInst GenInst_Owner_t97740679_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0 = { 2, GenInst_Owner_t97740679_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0_Types };
static const RuntimeType* GenInst_S3ErrorResponse_t623087499_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types[] = { (&S3ErrorResponse_t623087499_0_0_0), (&XmlUnmarshallerContext_t1179575220_0_0_0) };
extern const Il2CppGenericInst GenInst_S3ErrorResponse_t623087499_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0 = { 2, GenInst_S3ErrorResponse_t623087499_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0_Types };
static const RuntimeType* GenInst_Link_t3774454498_0_0_0_Types[] = { (&Link_t3774454498_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t3774454498_0_0_0 = { 1, GenInst_Link_t3774454498_0_0_0_Types };
static const RuntimeType* GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_Types[] = { (&Action_t3226471752_0_0_0), (&LinkedListNode_1_t2122577665_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0 = { 2, GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t2122577665_0_0_0_Types[] = { (&LinkedListNode_1_t2122577665_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2122577665_0_0_0 = { 1, GenInst_LinkedListNode_1_t2122577665_0_0_0_Types };
static const RuntimeType* GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_t3226471752_0_0_0), (&LinkedListNode_1_t2122577665_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t239539626_0_0_0_Types[] = { (&KeyValuePair_2_t239539626_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t239539626_0_0_0 = { 1, GenInst_KeyValuePair_2_t239539626_0_0_0_Types };
static const RuntimeType* GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_KeyValuePair_2_t239539626_0_0_0_Types[] = { (&Action_t3226471752_0_0_0), (&LinkedListNode_1_t2122577665_0_0_0), (&KeyValuePair_2_t239539626_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_KeyValuePair_2_t239539626_0_0_0 = { 3, GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_KeyValuePair_2_t239539626_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t2491248677_0_0_0_Types[] = { (&Action_1_t2491248677_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t2491248677_0_0_0 = { 1, GenInst_Action_1_t2491248677_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_Types[] = { (&Action_1_t2491248677_0_0_0), (&LinkedListNode_1_t1387354590_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0 = { 2, GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t1387354590_0_0_0_Types[] = { (&LinkedListNode_1_t1387354590_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t1387354590_0_0_0 = { 1, GenInst_LinkedListNode_1_t1387354590_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_1_t2491248677_0_0_0), (&LinkedListNode_1_t1387354590_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3584681366_0_0_0_Types[] = { (&KeyValuePair_2_t3584681366_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3584681366_0_0_0 = { 1, GenInst_KeyValuePair_2_t3584681366_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_KeyValuePair_2_t3584681366_0_0_0_Types[] = { (&Action_1_t2491248677_0_0_0), (&LinkedListNode_1_t1387354590_0_0_0), (&KeyValuePair_2_t3584681366_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_KeyValuePair_2_t3584681366_0_0_0 = { 3, GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_KeyValuePair_2_t3584681366_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2572051853_0_0_0_Types[] = { (&Action_2_t2572051853_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2572051853_0_0_0 = { 1, GenInst_Action_2_t2572051853_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_Types[] = { (&Action_2_t2572051853_0_0_0), (&LinkedListNode_1_t1468157766_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0 = { 2, GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t1468157766_0_0_0_Types[] = { (&LinkedListNode_1_t1468157766_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t1468157766_0_0_0 = { 1, GenInst_LinkedListNode_1_t1468157766_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_2_t2572051853_0_0_0), (&LinkedListNode_1_t1468157766_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1008245494_0_0_0_Types[] = { (&KeyValuePair_2_t1008245494_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1008245494_0_0_0 = { 1, GenInst_KeyValuePair_2_t1008245494_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_KeyValuePair_2_t1008245494_0_0_0_Types[] = { (&Action_2_t2572051853_0_0_0), (&LinkedListNode_1_t1468157766_0_0_0), (&KeyValuePair_2_t1008245494_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_KeyValuePair_2_t1008245494_0_0_0 = { 3, GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_KeyValuePair_2_t1008245494_0_0_0_Types };
static const RuntimeType* GenInst_Action_3_t1115657183_0_0_0_Types[] = { (&Action_3_t1115657183_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_3_t1115657183_0_0_0 = { 1, GenInst_Action_3_t1115657183_0_0_0_Types };
static const RuntimeType* GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_Types[] = { (&Action_3_t1115657183_0_0_0), (&LinkedListNode_1_t11763096_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0 = { 2, GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t11763096_0_0_0_Types[] = { (&LinkedListNode_1_t11763096_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t11763096_0_0_0 = { 1, GenInst_LinkedListNode_1_t11763096_0_0_0_Types };
static const RuntimeType* GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_3_t1115657183_0_0_0), (&LinkedListNode_1_t11763096_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4123020654_0_0_0_Types[] = { (&KeyValuePair_2_t4123020654_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4123020654_0_0_0 = { 1, GenInst_KeyValuePair_2_t4123020654_0_0_0_Types };
static const RuntimeType* GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_KeyValuePair_2_t4123020654_0_0_0_Types[] = { (&Action_3_t1115657183_0_0_0), (&LinkedListNode_1_t11763096_0_0_0), (&KeyValuePair_2_t4123020654_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_KeyValuePair_2_t4123020654_0_0_0 = { 3, GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_KeyValuePair_2_t4123020654_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Material_t193706927_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_Types };
static const RuntimeType* GenInst_Material_t193706927_0_0_0_Types[] = { (&Material_t193706927_0_0_0) };
extern const Il2CppGenericInst GenInst_Material_t193706927_0_0_0 = { 1, GenInst_Material_t193706927_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Material_t193706927_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1253845080_0_0_0_Types[] = { (&KeyValuePair_2_t1253845080_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1253845080_0_0_0 = { 1, GenInst_KeyValuePair_2_t1253845080_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_KeyValuePair_2_t1253845080_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Material_t193706927_0_0_0), (&KeyValuePair_2_t1253845080_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_KeyValuePair_2_t1253845080_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_KeyValuePair_2_t1253845080_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_FontAsset_t2530419979_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_Types };
static const RuntimeType* GenInst_TMP_FontAsset_t2530419979_0_0_0_Types[] = { (&TMP_FontAsset_t2530419979_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_FontAsset_t2530419979_0_0_0 = { 1, GenInst_TMP_FontAsset_t2530419979_0_0_0_Types };
static const RuntimeType* GenInst_TMP_Asset_t1084708044_0_0_0_Types[] = { (&TMP_Asset_t1084708044_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_Asset_t1084708044_0_0_0 = { 1, GenInst_TMP_Asset_t1084708044_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_FontAsset_t2530419979_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3590558132_0_0_0_Types[] = { (&KeyValuePair_2_t3590558132_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3590558132_0_0_0 = { 1, GenInst_KeyValuePair_2_t3590558132_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_KeyValuePair_2_t3590558132_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_FontAsset_t2530419979_0_0_0), (&KeyValuePair_2_t3590558132_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_KeyValuePair_2_t3590558132_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_KeyValuePair_2_t3590558132_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_SpriteAsset_t2641813093_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_Types };
static const RuntimeType* GenInst_TMP_SpriteAsset_t2641813093_0_0_0_Types[] = { (&TMP_SpriteAsset_t2641813093_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_SpriteAsset_t2641813093_0_0_0 = { 1, GenInst_TMP_SpriteAsset_t2641813093_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_SpriteAsset_t2641813093_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3701951246_0_0_0_Types[] = { (&KeyValuePair_2_t3701951246_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3701951246_0_0_0 = { 1, GenInst_KeyValuePair_2_t3701951246_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_KeyValuePair_2_t3701951246_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_SpriteAsset_t2641813093_0_0_0), (&KeyValuePair_2_t3701951246_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_KeyValuePair_2_t3701951246_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_KeyValuePair_2_t3701951246_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_ColorGradient_t1159837347_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_Types };
static const RuntimeType* GenInst_TMP_ColorGradient_t1159837347_0_0_0_Types[] = { (&TMP_ColorGradient_t1159837347_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_ColorGradient_t1159837347_0_0_0 = { 1, GenInst_TMP_ColorGradient_t1159837347_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_ColorGradient_t1159837347_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2219975500_0_0_0_Types[] = { (&KeyValuePair_2_t2219975500_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2219975500_0_0_0 = { 1, GenInst_KeyValuePair_2_t2219975500_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_KeyValuePair_2_t2219975500_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_ColorGradient_t1159837347_0_0_0), (&KeyValuePair_2_t2219975500_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_KeyValuePair_2_t2219975500_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_KeyValuePair_2_t2219975500_0_0_0_Types };
static const RuntimeType* GenInst_MaterialReference_t2854353496_0_0_0_Types[] = { (&MaterialReference_t2854353496_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialReference_t2854353496_0_0_0 = { 1, GenInst_MaterialReference_t2854353496_0_0_0_Types };
static const RuntimeType* GenInst_TMP_SubMesh_t3507543655_0_0_0_Types[] = { (&TMP_SubMesh_t3507543655_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_SubMesh_t3507543655_0_0_0 = { 1, GenInst_TMP_SubMesh_t3507543655_0_0_0_Types };
static const RuntimeType* GenInst_TMP_MeshInfo_t1297308317_0_0_0_Types[] = { (&TMP_MeshInfo_t1297308317_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_MeshInfo_t1297308317_0_0_0 = { 1, GenInst_TMP_MeshInfo_t1297308317_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_Glyph_t909793902_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_Types };
static const RuntimeType* GenInst_TMP_Glyph_t909793902_0_0_0_Types[] = { (&TMP_Glyph_t909793902_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_Glyph_t909793902_0_0_0 = { 1, GenInst_TMP_Glyph_t909793902_0_0_0_Types };
static const RuntimeType* GenInst_TMP_TextElement_t2285620223_0_0_0_Types[] = { (&TMP_TextElement_t2285620223_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_TextElement_t2285620223_0_0_0 = { 1, GenInst_TMP_TextElement_t2285620223_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_Glyph_t909793902_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1969932055_0_0_0_Types[] = { (&KeyValuePair_2_t1969932055_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1969932055_0_0_0 = { 1, GenInst_KeyValuePair_2_t1969932055_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_KeyValuePair_2_t1969932055_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_Glyph_t909793902_0_0_0), (&KeyValuePair_2_t1969932055_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_KeyValuePair_2_t1969932055_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_KeyValuePair_2_t1969932055_0_0_0_Types };
static const RuntimeType* GenInst_TMP_CharacterInfo_t1421302791_0_0_0_Types[] = { (&TMP_CharacterInfo_t1421302791_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_CharacterInfo_t1421302791_0_0_0 = { 1, GenInst_TMP_CharacterInfo_t1421302791_0_0_0_Types };
static const RuntimeType* GenInst_TextAlignmentOptions_t1466788324_0_0_0_Types[] = { (&TextAlignmentOptions_t1466788324_0_0_0) };
extern const Il2CppGenericInst GenInst_TextAlignmentOptions_t1466788324_0_0_0 = { 1, GenInst_TextAlignmentOptions_t1466788324_0_0_0_Types };
static const RuntimeType* GenInst_TMP_PageInfo_t3845132337_0_0_0_Types[] = { (&TMP_PageInfo_t3845132337_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_PageInfo_t3845132337_0_0_0 = { 1, GenInst_TMP_PageInfo_t3845132337_0_0_0_Types };
static const RuntimeType* GenInst_TMP_Sprite_t104383505_0_0_0_Types[] = { (&TMP_Sprite_t104383505_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_Sprite_t104383505_0_0_0 = { 1, GenInst_TMP_Sprite_t104383505_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&KerningPair_t1577753922_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_Types };
static const RuntimeType* GenInst_KerningPair_t1577753922_0_0_0_Types[] = { (&KerningPair_t1577753922_0_0_0) };
extern const Il2CppGenericInst GenInst_KerningPair_t1577753922_0_0_0 = { 1, GenInst_KerningPair_t1577753922_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&KerningPair_t1577753922_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2637892075_0_0_0_Types[] = { (&KeyValuePair_2_t2637892075_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2637892075_0_0_0 = { 1, GenInst_KeyValuePair_2_t2637892075_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_KeyValuePair_2_t2637892075_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&KerningPair_t1577753922_0_0_0), (&KeyValuePair_2_t2637892075_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_KeyValuePair_2_t2637892075_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_KeyValuePair_2_t2637892075_0_0_0_Types };
static const RuntimeType* GenInst_TMP_LineInfo_t2320418126_0_0_0_Types[] = { (&TMP_LineInfo_t2320418126_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_LineInfo_t2320418126_0_0_0 = { 1, GenInst_TMP_LineInfo_t2320418126_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Char_t3454481338_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t219652195_0_0_0_Types[] = { (&KeyValuePair_2_t219652195_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t219652195_0_0_0 = { 1, GenInst_KeyValuePair_2_t219652195_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Char_t3454481338_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Char_t3454481338_0_0_0), (&Char_t3454481338_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Char_t3454481338_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t219652195_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Char_t3454481338_0_0_0), (&KeyValuePair_2_t219652195_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t219652195_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t219652195_0_0_0_Types };
static const RuntimeType* GenInst_TMP_WordInfo_t3807457612_0_0_0_Types[] = { (&TMP_WordInfo_t3807457612_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_WordInfo_t3807457612_0_0_0 = { 1, GenInst_TMP_WordInfo_t3807457612_0_0_0_Types };
static const RuntimeType* GenInst_TMP_SubMeshUI_t1983202343_0_0_0_Types[] = { (&TMP_SubMeshUI_t1983202343_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_SubMeshUI_t1983202343_0_0_0 = { 1, GenInst_TMP_SubMeshUI_t1983202343_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Compute_DT_EventArgs_t4231491594_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Compute_DT_EventArgs_t4231491594_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Compute_DT_EventArgs_t4231491594_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Compute_DT_EventArgs_t4231491594_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t4114094152_0_0_0_Types[] = { (&Action_2_t4114094152_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t4114094152_0_0_0 = { 1, GenInst_Action_2_t4114094152_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t4114094152_0_0_0_LinkedListNode_1_t3010200065_0_0_0_Types[] = { (&Action_2_t4114094152_0_0_0), (&LinkedListNode_1_t3010200065_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t4114094152_0_0_0_LinkedListNode_1_t3010200065_0_0_0 = { 2, GenInst_Action_2_t4114094152_0_0_0_LinkedListNode_1_t3010200065_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t3010200065_0_0_0_Types[] = { (&LinkedListNode_1_t3010200065_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3010200065_0_0_0 = { 1, GenInst_LinkedListNode_1_t3010200065_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t4114094152_0_0_0_LinkedListNode_1_t3010200065_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_2_t4114094152_0_0_0), (&LinkedListNode_1_t3010200065_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t4114094152_0_0_0_LinkedListNode_1_t3010200065_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_2_t4114094152_0_0_0_LinkedListNode_1_t3010200065_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_Material_t193706927_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&Material_t193706927_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Material_t193706927_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Material_t193706927_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2525452034_0_0_0_Types[] = { (&Action_2_t2525452034_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2525452034_0_0_0 = { 1, GenInst_Action_2_t2525452034_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_Types[] = { (&Action_2_t2525452034_0_0_0), (&LinkedListNode_1_t1421557947_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0 = { 2, GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t1421557947_0_0_0_Types[] = { (&LinkedListNode_1_t1421557947_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t1421557947_0_0_0 = { 1, GenInst_LinkedListNode_1_t1421557947_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_2_t2525452034_0_0_0), (&LinkedListNode_1_t1421557947_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2048271746_0_0_0_Types[] = { (&KeyValuePair_2_t2048271746_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2048271746_0_0_0 = { 1, GenInst_KeyValuePair_2_t2048271746_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_KeyValuePair_2_t2048271746_0_0_0_Types[] = { (&Action_2_t2525452034_0_0_0), (&LinkedListNode_1_t1421557947_0_0_0), (&KeyValuePair_2_t2048271746_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_KeyValuePair_2_t2048271746_0_0_0 = { 3, GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_KeyValuePair_2_t2048271746_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t29709666_0_0_0_Types[] = { (&Action_2_t29709666_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t29709666_0_0_0 = { 1, GenInst_Action_2_t29709666_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t29709666_0_0_0_LinkedListNode_1_t3220782875_0_0_0_Types[] = { (&Action_2_t29709666_0_0_0), (&LinkedListNode_1_t3220782875_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t29709666_0_0_0_LinkedListNode_1_t3220782875_0_0_0 = { 2, GenInst_Action_2_t29709666_0_0_0_LinkedListNode_1_t3220782875_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t3220782875_0_0_0_Types[] = { (&LinkedListNode_1_t3220782875_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3220782875_0_0_0 = { 1, GenInst_LinkedListNode_1_t3220782875_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t29709666_0_0_0_LinkedListNode_1_t3220782875_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_2_t29709666_0_0_0), (&LinkedListNode_1_t3220782875_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t29709666_0_0_0_LinkedListNode_1_t3220782875_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_2_t29709666_0_0_0_LinkedListNode_1_t3220782875_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_TMP_FontAsset_t2530419979_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&TMP_FontAsset_t2530419979_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_TMP_FontAsset_t2530419979_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_TMP_FontAsset_t2530419979_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2366422718_0_0_0_Types[] = { (&Action_2_t2366422718_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2366422718_0_0_0 = { 1, GenInst_Action_2_t2366422718_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2366422718_0_0_0_LinkedListNode_1_t1262528631_0_0_0_Types[] = { (&Action_2_t2366422718_0_0_0), (&LinkedListNode_1_t1262528631_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2366422718_0_0_0_LinkedListNode_1_t1262528631_0_0_0 = { 2, GenInst_Action_2_t2366422718_0_0_0_LinkedListNode_1_t1262528631_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t1262528631_0_0_0_Types[] = { (&LinkedListNode_1_t1262528631_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t1262528631_0_0_0 = { 1, GenInst_LinkedListNode_1_t1262528631_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2366422718_0_0_0_LinkedListNode_1_t1262528631_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_2_t2366422718_0_0_0), (&LinkedListNode_1_t1262528631_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2366422718_0_0_0_LinkedListNode_1_t1262528631_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_2_t2366422718_0_0_0_LinkedListNode_1_t1262528631_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_Object_t1021602117_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&Object_t1021602117_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Object_t1021602117_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Object_t1021602117_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t857604856_0_0_0_Types[] = { (&Action_2_t857604856_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t857604856_0_0_0 = { 1, GenInst_Action_2_t857604856_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t857604856_0_0_0_LinkedListNode_1_t4048678065_0_0_0_Types[] = { (&Action_2_t857604856_0_0_0), (&LinkedListNode_1_t4048678065_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t857604856_0_0_0_LinkedListNode_1_t4048678065_0_0_0 = { 2, GenInst_Action_2_t857604856_0_0_0_LinkedListNode_1_t4048678065_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t4048678065_0_0_0_Types[] = { (&LinkedListNode_1_t4048678065_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t4048678065_0_0_0 = { 1, GenInst_LinkedListNode_1_t4048678065_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t857604856_0_0_0_LinkedListNode_1_t4048678065_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_2_t857604856_0_0_0), (&LinkedListNode_1_t4048678065_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t857604856_0_0_0_LinkedListNode_1_t4048678065_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_2_t857604856_0_0_0_LinkedListNode_1_t4048678065_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_TextMeshPro_t2521834357_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&TextMeshPro_t2521834357_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_TextMeshPro_t2521834357_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_TextMeshPro_t2521834357_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2357837096_0_0_0_Types[] = { (&Action_2_t2357837096_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2357837096_0_0_0 = { 1, GenInst_Action_2_t2357837096_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2357837096_0_0_0_LinkedListNode_1_t1253943009_0_0_0_Types[] = { (&Action_2_t2357837096_0_0_0), (&LinkedListNode_1_t1253943009_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2357837096_0_0_0_LinkedListNode_1_t1253943009_0_0_0 = { 2, GenInst_Action_2_t2357837096_0_0_0_LinkedListNode_1_t1253943009_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t1253943009_0_0_0_Types[] = { (&LinkedListNode_1_t1253943009_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t1253943009_0_0_0 = { 1, GenInst_LinkedListNode_1_t1253943009_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t2357837096_0_0_0_LinkedListNode_1_t1253943009_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_2_t2357837096_0_0_0), (&LinkedListNode_1_t1253943009_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t2357837096_0_0_0_LinkedListNode_1_t1253943009_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_2_t2357837096_0_0_0_LinkedListNode_1_t1253943009_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_t1756533147_0_0_0_Material_t193706927_0_0_0_Material_t193706927_0_0_0_Types[] = { (&GameObject_t1756533147_0_0_0), (&Material_t193706927_0_0_0), (&Material_t193706927_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_t1756533147_0_0_0_Material_t193706927_0_0_0_Material_t193706927_0_0_0 = { 3, GenInst_GameObject_t1756533147_0_0_0_Material_t193706927_0_0_0_Material_t193706927_0_0_0_Types };
static const RuntimeType* GenInst_Action_3_t408506571_0_0_0_Types[] = { (&Action_3_t408506571_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_3_t408506571_0_0_0 = { 1, GenInst_Action_3_t408506571_0_0_0_Types };
static const RuntimeType* GenInst_Action_3_t408506571_0_0_0_LinkedListNode_1_t3599579780_0_0_0_Types[] = { (&Action_3_t408506571_0_0_0), (&LinkedListNode_1_t3599579780_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_3_t408506571_0_0_0_LinkedListNode_1_t3599579780_0_0_0 = { 2, GenInst_Action_3_t408506571_0_0_0_LinkedListNode_1_t3599579780_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t3599579780_0_0_0_Types[] = { (&LinkedListNode_1_t3599579780_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3599579780_0_0_0 = { 1, GenInst_LinkedListNode_1_t3599579780_0_0_0_Types };
static const RuntimeType* GenInst_Action_3_t408506571_0_0_0_LinkedListNode_1_t3599579780_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_3_t408506571_0_0_0), (&LinkedListNode_1_t3599579780_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_3_t408506571_0_0_0_LinkedListNode_1_t3599579780_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_3_t408506571_0_0_0_LinkedListNode_1_t3599579780_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t3627374100_0_0_0_Types[] = { (&Action_1_t3627374100_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t3627374100_0_0_0 = { 1, GenInst_Action_1_t3627374100_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_Types[] = { (&Action_1_t3627374100_0_0_0), (&LinkedListNode_1_t2523480013_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0 = { 2, GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t2523480013_0_0_0_Types[] = { (&LinkedListNode_1_t2523480013_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2523480013_0_0_0 = { 1, GenInst_LinkedListNode_1_t2523480013_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_1_t3627374100_0_0_0), (&LinkedListNode_1_t2523480013_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t379239674_0_0_0_Types[] = { (&KeyValuePair_2_t379239674_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t379239674_0_0_0 = { 1, GenInst_KeyValuePair_2_t379239674_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_KeyValuePair_2_t379239674_0_0_0_Types[] = { (&Action_1_t3627374100_0_0_0), (&LinkedListNode_1_t2523480013_0_0_0), (&KeyValuePair_2_t379239674_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_KeyValuePair_2_t379239674_0_0_0 = { 3, GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_KeyValuePair_2_t379239674_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t961636729_0_0_0_Types[] = { (&Action_1_t961636729_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t961636729_0_0_0 = { 1, GenInst_Action_1_t961636729_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t961636729_0_0_0_LinkedListNode_1_t4152709938_0_0_0_Types[] = { (&Action_1_t961636729_0_0_0), (&LinkedListNode_1_t4152709938_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t961636729_0_0_0_LinkedListNode_1_t4152709938_0_0_0 = { 2, GenInst_Action_1_t961636729_0_0_0_LinkedListNode_1_t4152709938_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t4152709938_0_0_0_Types[] = { (&LinkedListNode_1_t4152709938_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t4152709938_0_0_0 = { 1, GenInst_LinkedListNode_1_t4152709938_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t961636729_0_0_0_LinkedListNode_1_t4152709938_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_1_t961636729_0_0_0), (&LinkedListNode_1_t4152709938_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t961636729_0_0_0_LinkedListNode_1_t4152709938_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_1_t961636729_0_0_0_LinkedListNode_1_t4152709938_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_TextMeshProUGUI_t934157183_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&TextMeshProUGUI_t934157183_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_TextMeshProUGUI_t934157183_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_TextMeshProUGUI_t934157183_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t770159922_0_0_0_Types[] = { (&Action_2_t770159922_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t770159922_0_0_0 = { 1, GenInst_Action_2_t770159922_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t770159922_0_0_0_LinkedListNode_1_t3961233131_0_0_0_Types[] = { (&Action_2_t770159922_0_0_0), (&LinkedListNode_1_t3961233131_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t770159922_0_0_0_LinkedListNode_1_t3961233131_0_0_0 = { 2, GenInst_Action_2_t770159922_0_0_0_LinkedListNode_1_t3961233131_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t3961233131_0_0_0_Types[] = { (&LinkedListNode_1_t3961233131_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3961233131_0_0_0 = { 1, GenInst_LinkedListNode_1_t3961233131_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t770159922_0_0_0_LinkedListNode_1_t3961233131_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_2_t770159922_0_0_0), (&LinkedListNode_1_t3961233131_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t770159922_0_0_0_LinkedListNode_1_t3961233131_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_2_t770159922_0_0_0_LinkedListNode_1_t3961233131_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t823401499_0_0_0_Types[] = { (&Action_1_t823401499_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t823401499_0_0_0 = { 1, GenInst_Action_1_t823401499_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t823401499_0_0_0_LinkedListNode_1_t4014474708_0_0_0_Types[] = { (&Action_1_t823401499_0_0_0), (&LinkedListNode_1_t4014474708_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t823401499_0_0_0_LinkedListNode_1_t4014474708_0_0_0 = { 2, GenInst_Action_1_t823401499_0_0_0_LinkedListNode_1_t4014474708_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t4014474708_0_0_0_Types[] = { (&LinkedListNode_1_t4014474708_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t4014474708_0_0_0 = { 1, GenInst_LinkedListNode_1_t4014474708_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t823401499_0_0_0_LinkedListNode_1_t4014474708_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Action_1_t823401499_0_0_0), (&LinkedListNode_1_t4014474708_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t823401499_0_0_0_LinkedListNode_1_t4014474708_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Action_1_t823401499_0_0_0_LinkedListNode_1_t4014474708_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KerningPair_t1577753922_0_0_0_UInt32_t2149682021_0_0_0_Types[] = { (&KerningPair_t1577753922_0_0_0), (&UInt32_t2149682021_0_0_0) };
extern const Il2CppGenericInst GenInst_KerningPair_t1577753922_0_0_0_UInt32_t2149682021_0_0_0 = { 2, GenInst_KerningPair_t1577753922_0_0_0_UInt32_t2149682021_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_UInt32_t2149682021_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&UInt32_t2149682021_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_UInt32_t2149682021_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_UInt32_t2149682021_0_0_0_Types };
static const RuntimeType* GenInst_OptionData_t234712921_0_0_0_Types[] = { (&OptionData_t234712921_0_0_0) };
extern const Il2CppGenericInst GenInst_OptionData_t234712921_0_0_0 = { 1, GenInst_OptionData_t234712921_0_0_0_Types };
static const RuntimeType* GenInst_DropdownItem_t1251916390_0_0_0_Types[] = { (&DropdownItem_t1251916390_0_0_0) };
extern const Il2CppGenericInst GenInst_DropdownItem_t1251916390_0_0_0 = { 1, GenInst_DropdownItem_t1251916390_0_0_0_Types };
static const RuntimeType* GenInst_FloatTween_t1652887471_0_0_0_Types[] = { (&FloatTween_t1652887471_0_0_0) };
extern const Il2CppGenericInst GenInst_FloatTween_t1652887471_0_0_0 = { 1, GenInst_FloatTween_t1652887471_0_0_0_Types };
static const RuntimeType* GenInst_TMP_FontWeights_t1564168302_0_0_0_Types[] = { (&TMP_FontWeights_t1564168302_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_FontWeights_t1564168302_0_0_0 = { 1, GenInst_TMP_FontWeights_t1564168302_0_0_0_Types };
static const RuntimeType* GenInst_TMP_Glyph_t909793902_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&TMP_Glyph_t909793902_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_Glyph_t909793902_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_TMP_Glyph_t909793902_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_ContentType_t4294436424_0_0_0_Types[] = { (&ContentType_t4294436424_0_0_0) };
extern const Il2CppGenericInst GenInst_ContentType_t4294436424_0_0_0 = { 1, GenInst_ContentType_t4294436424_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t2071877448_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_MaskingMaterial_t590070688_0_0_0_Types[] = { (&MaskingMaterial_t590070688_0_0_0) };
extern const Il2CppGenericInst GenInst_MaskingMaterial_t590070688_0_0_0 = { 1, GenInst_MaskingMaterial_t590070688_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&FallbackMaterial_t3285989240_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1436312919_0_0_0_Types[] = { (&KeyValuePair_2_t1436312919_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1436312919_0_0_0 = { 1, GenInst_KeyValuePair_2_t1436312919_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_Int64_t909078037_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&RuntimeObject_0_0_0), (&Int64_t909078037_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_Int64_t909078037_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_Int64_t909078037_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t1436312919_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types };
static const RuntimeType* GenInst_FallbackMaterial_t3285989240_0_0_0_Types[] = { (&FallbackMaterial_t3285989240_0_0_0) };
extern const Il2CppGenericInst GenInst_FallbackMaterial_t3285989240_0_0_0 = { 1, GenInst_FallbackMaterial_t3285989240_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&FallbackMaterial_t3285989240_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2032852864_0_0_0_Types[] = { (&KeyValuePair_2_t2032852864_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2032852864_0_0_0 = { 1, GenInst_KeyValuePair_2_t2032852864_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_KeyValuePair_2_t2032852864_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&FallbackMaterial_t3285989240_0_0_0), (&KeyValuePair_2_t2032852864_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_KeyValuePair_2_t2032852864_0_0_0 = { 3, GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_KeyValuePair_2_t2032852864_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int64_t909078037_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1969216190_0_0_0_Types[] = { (&KeyValuePair_2_t1969216190_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1969216190_0_0_0 = { 1, GenInst_KeyValuePair_2_t1969216190_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int64_t909078037_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int64_t909078037_0_0_0), (&Int64_t909078037_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int64_t909078037_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1969216190_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Int64_t909078037_0_0_0), (&KeyValuePair_2_t1969216190_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1969216190_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1969216190_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2397686115_0_0_0_Types[] = { (&List_1_t2397686115_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2397686115_0_0_0 = { 1, GenInst_List_1_t2397686115_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t590745575_0_0_0_Types[] = { (&KeyValuePair_2_t590745575_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t590745575_0_0_0 = { 1, GenInst_KeyValuePair_2_t590745575_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Boolean_t3825574718_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Boolean_t3825574718_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Boolean_t3825574718_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t590745575_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&Boolean_t3825574718_0_0_0), (&KeyValuePair_2_t590745575_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t590745575_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t590745575_0_0_0_Types };
static const RuntimeType* GenInst_SpriteData_t257854902_0_0_0_Types[] = { (&SpriteData_t257854902_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteData_t257854902_0_0_0 = { 1, GenInst_SpriteData_t257854902_0_0_0_Types };
static const RuntimeType* GenInst_TMP_Style_t69737451_0_0_0_Types[] = { (&TMP_Style_t69737451_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_Style_t69737451_0_0_0 = { 1, GenInst_TMP_Style_t69737451_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_Style_t69737451_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0 = { 2, GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_Style_t69737451_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1129875604_0_0_0_Types[] = { (&KeyValuePair_2_t1129875604_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1129875604_0_0_0 = { 1, GenInst_KeyValuePair_2_t1129875604_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_KeyValuePair_2_t1129875604_0_0_0_Types[] = { (&Int32_t2071877448_0_0_0), (&TMP_Style_t69737451_0_0_0), (&KeyValuePair_2_t1129875604_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_KeyValuePair_2_t1129875604_0_0_0 = { 3, GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_KeyValuePair_2_t1129875604_0_0_0_Types };
static const RuntimeType* GenInst_XML_TagAttribute_t1879784992_0_0_0_Types[] = { (&XML_TagAttribute_t1879784992_0_0_0) };
extern const Il2CppGenericInst GenInst_XML_TagAttribute_t1879784992_0_0_0 = { 1, GenInst_XML_TagAttribute_t1879784992_0_0_0_Types };
static const RuntimeType* GenInst_TMP_LinkInfo_t3626894960_0_0_0_Types[] = { (&TMP_LinkInfo_t3626894960_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_LinkInfo_t3626894960_0_0_0 = { 1, GenInst_TMP_LinkInfo_t3626894960_0_0_0_Types };
static const RuntimeType* GenInst_TMP_Text_t1920000777_0_0_0_Types[] = { (&TMP_Text_t1920000777_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_Text_t1920000777_0_0_0 = { 1, GenInst_TMP_Text_t1920000777_0_0_0_Types };
static const RuntimeType* GenInst_StringU5BU5D_t1642385972_0_0_0_Types[] = { (&StringU5BU5D_t1642385972_0_0_0) };
extern const Il2CppGenericInst GenInst_StringU5BU5D_t1642385972_0_0_0 = { 1, GenInst_StringU5BU5D_t1642385972_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2570160834_0_0_0_Types[] = { (&IList_1_t2570160834_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2570160834_0_0_0 = { 1, GenInst_IList_1_t2570160834_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2981295538_0_0_0_Types[] = { (&ICollection_1_t2981295538_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2981295538_0_0_0 = { 1, GenInst_ICollection_1_t2981295538_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2321347278_0_0_0_Types[] = { (&IEnumerable_1_t2321347278_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2321347278_0_0_0 = { 1, GenInst_IEnumerable_1_t2321347278_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1449033083_0_0_0_Types[] = { (&IList_1_t1449033083_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1449033083_0_0_0 = { 1, GenInst_IList_1_t1449033083_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1860167787_0_0_0_Types[] = { (&ICollection_1_t1860167787_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1860167787_0_0_0 = { 1, GenInst_ICollection_1_t1860167787_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1200219527_0_0_0_Types[] = { (&IEnumerable_1_t1200219527_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1200219527_0_0_0 = { 1, GenInst_IEnumerable_1_t1200219527_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2398023366_0_0_0_Types[] = { (&IList_1_t2398023366_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2398023366_0_0_0 = { 1, GenInst_IList_1_t2398023366_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2809158070_0_0_0_Types[] = { (&ICollection_1_t2809158070_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2809158070_0_0_0 = { 1, GenInst_ICollection_1_t2809158070_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2149209810_0_0_0_Types[] = { (&IEnumerable_1_t2149209810_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2149209810_0_0_0 = { 1, GenInst_IEnumerable_1_t2149209810_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3452350100_0_0_0_Types[] = { (&IList_1_t3452350100_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3452350100_0_0_0 = { 1, GenInst_IList_1_t3452350100_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3863484804_0_0_0_Types[] = { (&ICollection_1_t3863484804_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3863484804_0_0_0 = { 1, GenInst_ICollection_1_t3863484804_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3203536544_0_0_0_Types[] = { (&IEnumerable_1_t3203536544_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3203536544_0_0_0 = { 1, GenInst_IEnumerable_1_t3203536544_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t99252587_0_0_0_Types[] = { (&IList_1_t99252587_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t99252587_0_0_0 = { 1, GenInst_IList_1_t99252587_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t510387291_0_0_0_Types[] = { (&ICollection_1_t510387291_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t510387291_0_0_0 = { 1, GenInst_ICollection_1_t510387291_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t4145406327_0_0_0_Types[] = { (&IEnumerable_1_t4145406327_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4145406327_0_0_0 = { 1, GenInst_IEnumerable_1_t4145406327_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t107032761_0_0_0_Types[] = { (&IList_1_t107032761_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t107032761_0_0_0 = { 1, GenInst_IList_1_t107032761_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t518167465_0_0_0_Types[] = { (&ICollection_1_t518167465_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t518167465_0_0_0 = { 1, GenInst_ICollection_1_t518167465_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t4153186501_0_0_0_Types[] = { (&IEnumerable_1_t4153186501_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4153186501_0_0_0 = { 1, GenInst_IEnumerable_1_t4153186501_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t479175707_0_0_0_Types[] = { (&IList_1_t479175707_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t479175707_0_0_0 = { 1, GenInst_IList_1_t479175707_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t890310411_0_0_0_Types[] = { (&ICollection_1_t890310411_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t890310411_0_0_0 = { 1, GenInst_ICollection_1_t890310411_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t230362151_0_0_0_Types[] = { (&IEnumerable_1_t230362151_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t230362151_0_0_0 = { 1, GenInst_IEnumerable_1_t230362151_0_0_0_Types };
static const RuntimeType* GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&Char_t3454481338_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&String_t_0_0_0), (&String_t_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_VertexAnim_t2147777005_0_0_0_Types[] = { (&VertexAnim_t2147777005_0_0_0) };
extern const Il2CppGenericInst GenInst_VertexAnim_t2147777005_0_0_0 = { 1, GenInst_VertexAnim_t2147777005_0_0_0_Types };
static const RuntimeType* GenInst_Vector3U5BU5D_t1172311765_0_0_0_Types[] = { (&Vector3U5BU5D_t1172311765_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3U5BU5D_t1172311765_0_0_0 = { 1, GenInst_Vector3U5BU5D_t1172311765_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2784648181_0_0_0_Types[] = { (&IList_1_t2784648181_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2784648181_0_0_0 = { 1, GenInst_IList_1_t2784648181_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3195782885_0_0_0_Types[] = { (&ICollection_1_t3195782885_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3195782885_0_0_0 = { 1, GenInst_ICollection_1_t3195782885_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2535834625_0_0_0_Types[] = { (&IEnumerable_1_t2535834625_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2535834625_0_0_0 = { 1, GenInst_IEnumerable_1_t2535834625_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types[] = { (&IEnumerable_1_t4048664256_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0_Types[] = { (&Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2069139338_gp_0_0_0_0_Array_Sort_m2069139338_gp_0_0_0_0_Types[] = { (&Array_Sort_m2069139338_gp_0_0_0_0), (&Array_Sort_m2069139338_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2069139338_gp_0_0_0_0_Array_Sort_m2069139338_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2069139338_gp_0_0_0_0_Array_Sort_m2069139338_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1999601238_gp_0_0_0_0_Array_Sort_m1999601238_gp_1_0_0_0_Types[] = { (&Array_Sort_m1999601238_gp_0_0_0_0), (&Array_Sort_m1999601238_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1999601238_gp_0_0_0_0_Array_Sort_m1999601238_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1999601238_gp_0_0_0_0_Array_Sort_m1999601238_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Types[] = { (&Array_Sort_m4168899348_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m4168899348_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Array_Sort_m4168899348_gp_0_0_0_0_Types[] = { (&Array_Sort_m4168899348_gp_0_0_0_0), (&Array_Sort_m4168899348_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Array_Sort_m4168899348_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Array_Sort_m4168899348_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Types[] = { (&Array_Sort_m1711256281_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1711256281_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Array_Sort_m1711256281_gp_1_0_0_0_Types[] = { (&Array_Sort_m1711256281_gp_0_0_0_0), (&Array_Sort_m1711256281_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Array_Sort_m1711256281_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Array_Sort_m1711256281_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m714603628_gp_0_0_0_0_Array_Sort_m714603628_gp_0_0_0_0_Types[] = { (&Array_Sort_m714603628_gp_0_0_0_0), (&Array_Sort_m714603628_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m714603628_gp_0_0_0_0_Array_Sort_m714603628_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m714603628_gp_0_0_0_0_Array_Sort_m714603628_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m4057794348_gp_0_0_0_0_Array_Sort_m4057794348_gp_1_0_0_0_Types[] = { (&Array_Sort_m4057794348_gp_0_0_0_0), (&Array_Sort_m4057794348_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m4057794348_gp_0_0_0_0_Array_Sort_m4057794348_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m4057794348_gp_0_0_0_0_Array_Sort_m4057794348_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Types[] = { (&Array_Sort_m2544329074_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2544329074_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Array_Sort_m2544329074_gp_0_0_0_0_Types[] = { (&Array_Sort_m2544329074_gp_0_0_0_0), (&Array_Sort_m2544329074_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Array_Sort_m2544329074_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Array_Sort_m2544329074_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m54102389_gp_0_0_0_0_Types[] = { (&Array_Sort_m54102389_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m54102389_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m54102389_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m54102389_gp_1_0_0_0_Types[] = { (&Array_Sort_m54102389_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m54102389_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m54102389_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m54102389_gp_0_0_0_0_Array_Sort_m54102389_gp_1_0_0_0_Types[] = { (&Array_Sort_m54102389_gp_0_0_0_0), (&Array_Sort_m54102389_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m54102389_gp_0_0_0_0_Array_Sort_m54102389_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m54102389_gp_0_0_0_0_Array_Sort_m54102389_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2673640633_gp_0_0_0_0_Types[] = { (&Array_Sort_m2673640633_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2673640633_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2673640633_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m285045864_gp_0_0_0_0_Types[] = { (&Array_Sort_m285045864_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m285045864_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m285045864_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m965712368_gp_0_0_0_0_Types[] = { (&Array_qsort_m965712368_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m965712368_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m965712368_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m965712368_gp_0_0_0_0_Array_qsort_m965712368_gp_1_0_0_0_Types[] = { (&Array_qsort_m965712368_gp_0_0_0_0), (&Array_qsort_m965712368_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m965712368_gp_0_0_0_0_Array_qsort_m965712368_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m965712368_gp_0_0_0_0_Array_qsort_m965712368_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_compare_m1681301885_gp_0_0_0_0_Types[] = { (&Array_compare_m1681301885_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_compare_m1681301885_gp_0_0_0_0 = { 1, GenInst_Array_compare_m1681301885_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m2216502740_gp_0_0_0_0_Types[] = { (&Array_qsort_m2216502740_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m2216502740_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m2216502740_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Resize_m2875826811_gp_0_0_0_0_Types[] = { (&Array_Resize_m2875826811_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Resize_m2875826811_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m2875826811_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_TrueForAll_m1718874735_gp_0_0_0_0_Types[] = { (&Array_TrueForAll_m1718874735_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m1718874735_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m1718874735_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ForEach_m1854649314_gp_0_0_0_0_Types[] = { (&Array_ForEach_m1854649314_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1854649314_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1854649314_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ConvertAll_m4274024367_gp_0_0_0_0_Array_ConvertAll_m4274024367_gp_1_0_0_0_Types[] = { (&Array_ConvertAll_m4274024367_gp_0_0_0_0), (&Array_ConvertAll_m4274024367_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m4274024367_gp_0_0_0_0_Array_ConvertAll_m4274024367_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m4274024367_gp_0_0_0_0_Array_ConvertAll_m4274024367_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m2314439434_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m2314439434_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m2314439434_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m2314439434_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m3151928313_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m3151928313_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m3151928313_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m3151928313_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m2323782676_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m2323782676_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m2323782676_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m2323782676_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m1557343438_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m1557343438_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1557343438_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1557343438_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m1631399507_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m1631399507_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1631399507_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1631399507_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m3415315332_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m3415315332_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3415315332_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3415315332_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m2990936537_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m2990936537_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m2990936537_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m2990936537_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m2009305197_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m2009305197_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m2009305197_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m2009305197_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m2427841765_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m2427841765_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m2427841765_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m2427841765_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3348121441_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3348121441_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3348121441_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3348121441_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m2010744443_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m2010744443_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m2010744443_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m2010744443_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m4126518092_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m4126518092_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m4126518092_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m4126518092_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m3127862871_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m3127862871_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3127862871_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3127862871_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m1523107937_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m1523107937_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1523107937_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1523107937_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m2200535596_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m2200535596_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m2200535596_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m2200535596_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m555775493_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m555775493_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m555775493_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m555775493_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindAll_m2331462890_gp_0_0_0_0_Types[] = { (&Array_FindAll_m2331462890_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindAll_m2331462890_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m2331462890_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Exists_m976807351_gp_0_0_0_0_Types[] = { (&Array_Exists_m976807351_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Exists_m976807351_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m976807351_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_AsReadOnly_m884706992_gp_0_0_0_0_Types[] = { (&Array_AsReadOnly_m884706992_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m884706992_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m884706992_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Find_m1125132281_gp_0_0_0_0_Types[] = { (&Array_Find_m1125132281_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Find_m1125132281_gp_0_0_0_0 = { 1, GenInst_Array_Find_m1125132281_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLast_m4092932075_gp_0_0_0_0_Types[] = { (&Array_FindLast_m4092932075_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLast_m4092932075_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m4092932075_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types[] = { (&InternalEnumerator_1_t3582267753_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types[] = { (&ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3737699284_gp_0_0_0_0_Types[] = { (&IList_1_t3737699284_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3737699284_gp_0_0_0_0 = { 1, GenInst_IList_1_t3737699284_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types[] = { (&ICollection_1_t1552160836_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1552160836_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1552160836_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types[] = { (&Nullable_1_t1398937014_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Nullable_1_t1398937014_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1398937014_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types[] = { (&Comparer_1_t1036860714_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Comparer_1_t1036860714_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1036860714_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types[] = { (&DefaultComparer_t3074655092_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3074655092_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3074655092_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types[] = { (&GenericComparer_1_t1787398723_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3180694294_0_0_0_Types[] = { (&KeyValuePair_2_t3180694294_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0 = { 1, GenInst_KeyValuePair_2_t3180694294_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0), (&Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0), (&Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types[] = { (&ShimEnumerator_t3895203923_gp_0_0_0_0), (&ShimEnumerator_t3895203923_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types[] = { (&Enumerator_t2089681430_gp_0_0_0_0), (&Enumerator_t2089681430_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3434615342_0_0_0_Types[] = { (&KeyValuePair_2_t3434615342_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3434615342_0_0_0 = { 1, GenInst_KeyValuePair_2_t3434615342_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types[] = { (&KeyCollection_t1229212677_gp_0_0_0_0), (&KeyCollection_t1229212677_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { (&KeyCollection_t1229212677_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types[] = { (&Enumerator_t83320710_gp_0_0_0_0), (&Enumerator_t83320710_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0 = { 2, GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t83320710_gp_0_0_0_0_Types[] = { (&Enumerator_t83320710_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t83320710_gp_0_0_0_0 = { 1, GenInst_Enumerator_t83320710_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { (&KeyCollection_t1229212677_gp_0_0_0_0), (&KeyCollection_t1229212677_gp_1_0_0_0), (&KeyCollection_t1229212677_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types[] = { (&KeyCollection_t1229212677_gp_0_0_0_0), (&KeyCollection_t1229212677_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { (&ValueCollection_t2262344653_gp_0_0_0_0), (&ValueCollection_t2262344653_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { (&ValueCollection_t2262344653_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { (&Enumerator_t3111723616_gp_0_0_0_0), (&Enumerator_t3111723616_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0 = { 2, GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types[] = { (&Enumerator_t3111723616_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3111723616_gp_1_0_0_0 = { 1, GenInst_Enumerator_t3111723616_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { (&ValueCollection_t2262344653_gp_0_0_0_0), (&ValueCollection_t2262344653_gp_1_0_0_0), (&ValueCollection_t2262344653_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types[] = { (&ValueCollection_t2262344653_gp_1_0_0_0), (&ValueCollection_t2262344653_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types[] = { (&DictionaryEntry_t3048875398_0_0_0), (&DictionaryEntry_t3048875398_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0 = { 2, GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_0_0_0_0), (&Dictionary_2_t2276497324_gp_1_0_0_0), (&KeyValuePair_2_t3180694294_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 3, GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types[] = { (&KeyValuePair_2_t3180694294_0_0_0), (&KeyValuePair_2_t3180694294_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0 = { 2, GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types[] = { (&Dictionary_2_t2276497324_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2276497324_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2276497324_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types[] = { (&EqualityComparer_1_t2066709010_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types[] = { (&DefaultComparer_t1766400012_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1766400012_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1766400012_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types[] = { (&GenericEqualityComparer_1_t2202941003_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types[] = { (&IDictionary_2_t3502329323_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4174120762_0_0_0_Types[] = { (&KeyValuePair_2_t4174120762_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4174120762_0_0_0 = { 1, GenInst_KeyValuePair_2_t4174120762_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types[] = { (&IDictionary_2_t3502329323_gp_0_0_0_0), (&IDictionary_2_t3502329323_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types[] = { (&KeyValuePair_2_t1988958766_gp_0_0_0_0), (&KeyValuePair_2_t1988958766_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1169184319_gp_0_0_0_0_Types[] = { (&List_1_t1169184319_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1169184319_gp_0_0_0_0 = { 1, GenInst_List_1_t1169184319_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types[] = { (&Enumerator_t1292967705_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1292967705_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1292967705_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Collection_1_t686054069_gp_0_0_0_0_Types[] = { (&Collection_1_t686054069_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Collection_1_t686054069_gp_0_0_0_0 = { 1, GenInst_Collection_1_t686054069_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types[] = { (&ReadOnlyCollection_1_t3540981679_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0_Types[] = { (&MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0), (&MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0_Types[] = { (&MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types[] = { (&ArraySegment_1_t1001032761_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0 = { 1, GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types[] = { (&LinkedList_1_t3556217344_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedList_1_t3556217344_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t3556217344_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types[] = { (&Enumerator_t4145643798_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t4145643798_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4145643798_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types[] = { (&LinkedListNode_1_t2172356692_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types[] = { (&Queue_1_t1458930734_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Queue_1_t1458930734_gp_0_0_0_0 = { 1, GenInst_Queue_1_t1458930734_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types[] = { (&Enumerator_t4000919638_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t4000919638_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4000919638_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_RBTree_Intern_m1863612280_gp_0_0_0_0_Types[] = { (&RBTree_Intern_m1863612280_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_RBTree_Intern_m1863612280_gp_0_0_0_0 = { 1, GenInst_RBTree_Intern_m1863612280_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_RBTree_Remove_m2489856008_gp_0_0_0_0_Types[] = { (&RBTree_Remove_m2489856008_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_RBTree_Remove_m2489856008_gp_0_0_0_0 = { 1, GenInst_RBTree_Remove_m2489856008_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_RBTree_Lookup_m3821366094_gp_0_0_0_0_Types[] = { (&RBTree_Lookup_m3821366094_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_RBTree_Lookup_m3821366094_gp_0_0_0_0 = { 1, GenInst_RBTree_Lookup_m3821366094_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_RBTree_find_key_m3679421040_gp_0_0_0_0_Types[] = { (&RBTree_find_key_m3679421040_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_RBTree_find_key_m3679421040_gp_0_0_0_0 = { 1, GenInst_RBTree_find_key_m3679421040_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_Types[] = { (&SortedDictionary_2_t3426860295_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0 = { 1, GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types[] = { (&SortedDictionary_2_t3426860295_gp_0_0_0_0), (&SortedDictionary_2_t3426860295_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0 = { 2, GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4073929546_0_0_0_Types[] = { (&KeyValuePair_2_t4073929546_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4073929546_0_0_0 = { 1, GenInst_KeyValuePair_2_t4073929546_0_0_0_Types };
static const RuntimeType* GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0_Types[] = { (&Node_t1473068371_gp_0_0_0_0), (&Node_t1473068371_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0 = { 2, GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_NodeHelper_t357096749_gp_0_0_0_0_Types[] = { (&NodeHelper_t357096749_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_NodeHelper_t357096749_gp_0_0_0_0 = { 1, GenInst_NodeHelper_t357096749_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0_Types[] = { (&NodeHelper_t357096749_gp_0_0_0_0), (&NodeHelper_t357096749_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0 = { 2, GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0_Types[] = { (&ValueCollection_t2735575576_gp_0_0_0_0), (&ValueCollection_t2735575576_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t2735575576_gp_1_0_0_0_Types[] = { (&ValueCollection_t2735575576_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t2735575576_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2735575576_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0_Types[] = { (&Enumerator_t1353355221_gp_0_0_0_0), (&Enumerator_t1353355221_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1353355221_gp_1_0_0_0_Types[] = { (&Enumerator_t1353355221_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1353355221_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1353355221_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0_Types[] = { (&KeyCollection_t3620397270_gp_0_0_0_0), (&KeyCollection_t3620397270_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t3620397270_gp_0_0_0_0_Types[] = { (&KeyCollection_t3620397270_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t3620397270_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t3620397270_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0_Types[] = { (&Enumerator_t4275149413_gp_0_0_0_0), (&Enumerator_t4275149413_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0 = { 2, GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t4275149413_gp_0_0_0_0_Types[] = { (&Enumerator_t4275149413_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t4275149413_gp_0_0_0_0 = { 1, GenInst_Enumerator_t4275149413_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0_Types[] = { (&Enumerator_t824916987_gp_0_0_0_0), (&Enumerator_t824916987_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0 = { 2, GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t404405498_0_0_0_Types[] = { (&KeyValuePair_2_t404405498_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t404405498_0_0_0 = { 1, GenInst_KeyValuePair_2_t404405498_0_0_0_Types };
static const RuntimeType* GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types[] = { (&SortedDictionary_2_t3426860295_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0 = { 1, GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types[] = { (&Stack_1_t4016656541_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Stack_1_t4016656541_gp_0_0_0_0 = { 1, GenInst_Stack_1_t4016656541_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t546412149_gp_0_0_0_0_Types[] = { (&Enumerator_t546412149_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t546412149_gp_0_0_0_0 = { 1, GenInst_Enumerator_t546412149_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types[] = { (&HashSet_1_t2624254809_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t2624254809_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t2624254809_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types[] = { (&Enumerator_t2109956843_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2109956843_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2109956843_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types[] = { (&PrimeHelper_t3424417428_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PrimeHelper_t3424417428_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t3424417428_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m1395534165_gp_0_0_0_0_Types[] = { (&Enumerable_Any_m1395534165_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m1395534165_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m1395534165_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Contains_m1886513113_gp_0_0_0_0_Types[] = { (&Enumerable_Contains_m1886513113_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m1886513113_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m1886513113_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Contains_m1629730982_gp_0_0_0_0_Types[] = { (&Enumerable_Contains_m1629730982_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m1629730982_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m1629730982_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Count_m2109803174_gp_0_0_0_0_Types[] = { (&Enumerable_Count_m2109803174_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Count_m2109803174_gp_0_0_0_0 = { 1, GenInst_Enumerable_Count_m2109803174_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_First_m385027450_gp_0_0_0_0_Types[] = { (&Enumerable_First_m385027450_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_First_m385027450_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m385027450_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_First_m385027450_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_First_m385027450_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_First_m385027450_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_First_m385027450_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_First_m2440166523_gp_0_0_0_0_Types[] = { (&Enumerable_First_m2440166523_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_First_m2440166523_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m2440166523_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_First_m3310377166_gp_0_0_0_0_Types[] = { (&Enumerable_First_m3310377166_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_First_m3310377166_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m3310377166_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_First_m3310377166_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_First_m3310377166_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_First_m3310377166_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_First_m3310377166_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0_Types[] = { (&Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Last_m1051445487_gp_0_0_0_0_Types[] = { (&Enumerable_Last_m1051445487_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Last_m1051445487_gp_0_0_0_0 = { 1, GenInst_Enumerable_Last_m1051445487_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_OrderBy_m1178781222_gp_0_0_0_0_Types[] = { (&Enumerable_OrderBy_m1178781222_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m1178781222_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m1178781222_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_OrderBy_m1178781222_gp_0_0_0_0_Enumerable_OrderBy_m1178781222_gp_1_0_0_0_Types[] = { (&Enumerable_OrderBy_m1178781222_gp_0_0_0_0), (&Enumerable_OrderBy_m1178781222_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m1178781222_gp_0_0_0_0_Enumerable_OrderBy_m1178781222_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m1178781222_gp_0_0_0_0_Enumerable_OrderBy_m1178781222_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_OrderBy_m1935324147_gp_0_0_0_0_Types[] = { (&Enumerable_OrderBy_m1935324147_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m1935324147_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m1935324147_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_OrderBy_m1935324147_gp_0_0_0_0_Enumerable_OrderBy_m1935324147_gp_1_0_0_0_Types[] = { (&Enumerable_OrderBy_m1935324147_gp_0_0_0_0), (&Enumerable_OrderBy_m1935324147_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m1935324147_gp_0_0_0_0_Enumerable_OrderBy_m1935324147_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m1935324147_gp_0_0_0_0_Enumerable_OrderBy_m1935324147_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_OrderBy_m1935324147_gp_1_0_0_0_Types[] = { (&Enumerable_OrderBy_m1935324147_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m1935324147_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m1935324147_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Types[] = { (&Enumerable_Select_m791198632_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m791198632_gp_0_0_0_0 = { 1, GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Enumerable_Select_m791198632_gp_1_0_0_0_Types[] = { (&Enumerable_Select_m791198632_gp_0_0_0_0), (&Enumerable_Select_m791198632_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Enumerable_Select_m791198632_gp_1_0_0_0 = { 2, GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Enumerable_Select_m791198632_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Select_m791198632_gp_1_0_0_0_Types[] = { (&Enumerable_Select_m791198632_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Select_m791198632_gp_1_0_0_0 = { 1, GenInst_Enumerable_Select_m791198632_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Types[] = { (&Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0_Types[] = { (&Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0), (&Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0 = { 2, GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0_Types[] = { (&Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0 = { 1, GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Single_m2573711234_gp_0_0_0_0_Types[] = { (&Enumerable_Single_m2573711234_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2573711234_gp_0_0_0_0 = { 1, GenInst_Enumerable_Single_m2573711234_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Single_m2573711234_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_Single_m2573711234_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Single_m2573711234_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Single_m2573711234_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0_Types[] = { (&Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0 = { 1, GenInst_Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ThenBy_m1431519242_gp_0_0_0_0_Types[] = { (&Enumerable_ThenBy_m1431519242_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m1431519242_gp_0_0_0_0 = { 1, GenInst_Enumerable_ThenBy_m1431519242_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ThenBy_m1431519242_gp_0_0_0_0_Enumerable_ThenBy_m1431519242_gp_1_0_0_0_Types[] = { (&Enumerable_ThenBy_m1431519242_gp_0_0_0_0), (&Enumerable_ThenBy_m1431519242_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m1431519242_gp_0_0_0_0_Enumerable_ThenBy_m1431519242_gp_1_0_0_0 = { 2, GenInst_Enumerable_ThenBy_m1431519242_gp_0_0_0_0_Enumerable_ThenBy_m1431519242_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ThenBy_m3860233309_gp_0_0_0_0_Types[] = { (&Enumerable_ThenBy_m3860233309_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m3860233309_gp_0_0_0_0 = { 1, GenInst_Enumerable_ThenBy_m3860233309_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ThenBy_m3860233309_gp_0_0_0_0_Enumerable_ThenBy_m3860233309_gp_1_0_0_0_Types[] = { (&Enumerable_ThenBy_m3860233309_gp_0_0_0_0), (&Enumerable_ThenBy_m3860233309_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m3860233309_gp_0_0_0_0_Enumerable_ThenBy_m3860233309_gp_1_0_0_0 = { 2, GenInst_Enumerable_ThenBy_m3860233309_gp_0_0_0_0_Enumerable_ThenBy_m3860233309_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ThenBy_m3860233309_gp_1_0_0_0_Types[] = { (&Enumerable_ThenBy_m3860233309_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m3860233309_gp_1_0_0_0 = { 1, GenInst_Enumerable_ThenBy_m3860233309_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ToArray_m747609077_gp_0_0_0_0_Types[] = { (&Enumerable_ToArray_m747609077_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m747609077_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m747609077_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_ToList_m2644368746_gp_0_0_0_0_Types[] = { (&Enumerable_ToList_m2644368746_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m2644368746_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m2644368746_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Types[] = { (&Enumerable_Where_m1602106588_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_Where_m1602106588_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { (&U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types[] = { (&U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0 = { 1, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types[] = { (&U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0), (&U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0 = { 2, GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0_Types[] = { (&IOrderedEnumerable_1_t641749975_gp_0_0_0_0), (&IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0 = { 2, GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0_Types[] = { (&IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types[] = { (&IOrderedEnumerable_1_t641749975_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types[] = { (&OrderedEnumerable_1_t753306046_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0_Types[] = { (&OrderedEnumerable_1_t753306046_gp_0_0_0_0), (&OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0 = { 2, GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0_Types[] = { (&OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types[] = { (&OrderedSequence_2_t1023848160_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { (&OrderedSequence_2_t1023848160_gp_0_0_0_0), (&OrderedSequence_2_t1023848160_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 2, GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types[] = { (&OrderedSequence_2_t1023848160_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0 = { 1, GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types[] = { (&QuickSort_1_t1290221672_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_QuickSort_1_t1290221672_gp_0_0_0_0 = { 1, GenInst_QuickSort_1_t1290221672_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types[] = { (&U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0 = { 1, GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types[] = { (&SortContext_1_t4088581714_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SortContext_1_t4088581714_gp_0_0_0_0 = { 1, GenInst_SortContext_1_t4088581714_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types[] = { (&SortSequenceContext_2_t3419387730_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { (&SortSequenceContext_2_t3419387730_gp_0_0_0_0), (&SortSequenceContext_2_t3419387730_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 2, GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types[] = { (&SortSequenceContext_2_t3419387730_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0 = { 1, GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentInChildren_m2766208945_gp_0_0_0_0_Types[] = { (&Component_GetComponentInChildren_m2766208945_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m2766208945_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m2766208945_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m3431627251_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m3431627251_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3431627251_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3431627251_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m2508569808_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m2508569808_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2508569808_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2508569808_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m1332352624_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m1332352624_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1332352624_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1332352624_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m3637577495_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m3637577495_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3637577495_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3637577495_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m1487060311_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m1487060311_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m1487060311_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m1487060311_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m1502796979_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m1502796979_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m1502796979_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m1502796979_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m2277214384_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m2277214384_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m2277214384_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m2277214384_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentInChildren_m2459662209_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentInChildren_m2459662209_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m2459662209_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m2459662209_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponents_m2891535275_gp_0_0_0_0_Types[] = { (&GameObject_GetComponents_m2891535275_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2891535275_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2891535275_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInChildren_m2576466756_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInChildren_m2576466756_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m2576466756_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m2576466756_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInParent_m2457452615_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInParent_m2457452615_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m2457452615_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m2457452615_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_GetAllocArrayFromChannel_m4152119927_gp_0_0_0_0_Types[] = { (&Mesh_GetAllocArrayFromChannel_m4152119927_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m4152119927_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m4152119927_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SafeLength_m4002341710_gp_0_0_0_0_Types[] = { (&Mesh_SafeLength_m4002341710_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m4002341710_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m4002341710_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m3461755521_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m3461755521_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3461755521_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3461755521_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m3202302135_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m3202302135_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m3202302135_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m3202302135_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetUvsImpl_m311612863_gp_0_0_0_0_Types[] = { (&Mesh_SetUvsImpl_m311612863_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m311612863_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m311612863_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_Instantiate_m2665790055_gp_0_0_0_0_Types[] = { (&Object_Instantiate_m2665790055_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m2665790055_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m2665790055_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types[] = { (&InvokableCall_1_t476640868_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t476640868_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t476640868_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_1_t2490859068_0_0_0_Types[] = { (&UnityAction_1_t2490859068_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_1_t2490859068_0_0_0 = { 1, GenInst_UnityAction_1_t2490859068_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t2042724809_gp_0_0_0_0), (&InvokableCall_2_t2042724809_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_2_t601835599_0_0_0_Types[] = { (&UnityAction_2_t601835599_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_2_t601835599_0_0_0 = { 1, GenInst_UnityAction_2_t601835599_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types[] = { (&InvokableCall_2_t2042724809_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t2042724809_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3608808750_gp_0_0_0_0), (&InvokableCall_3_t3608808750_gp_1_0_0_0), (&InvokableCall_3_t3608808750_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_3_t155920421_0_0_0_Types[] = { (&UnityAction_3_t155920421_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_3_t155920421_0_0_0 = { 1, GenInst_UnityAction_3_t155920421_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types[] = { (&InvokableCall_3_t3608808750_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types[] = { (&InvokableCall_3_t3608808750_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t3608808750_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_0_0_0_0), (&InvokableCall_4_t879925395_gp_1_0_0_0), (&InvokableCall_4_t879925395_gp_2_0_0_0), (&InvokableCall_4_t879925395_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t879925395_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t879925395_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t879925395_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types[] = { (&CachedInvokableCall_1_t224769006_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types[] = { (&UnityEvent_1_t4075366602_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types[] = { (&UnityEvent_2_t4075366599_gp_0_0_0_0), (&UnityEvent_2_t4075366599_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types[] = { (&UnityEvent_3_t4075366600_gp_0_0_0_0), (&UnityEvent_3_t4075366600_gp_1_0_0_0), (&UnityEvent_3_t4075366600_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types[] = { (&UnityEvent_4_t4075366597_gp_0_0_0_0), (&UnityEvent_4_t4075366597_gp_1_0_0_0), (&UnityEvent_4_t4075366597_gp_2_0_0_0), (&UnityEvent_4_t4075366597_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_Runtime_GetNSObject_m3692132737_gp_0_0_0_0_Types[] = { (&Runtime_GetNSObject_m3692132737_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Runtime_GetNSObject_m3692132737_gp_0_0_0_0 = { 1, GenInst_Runtime_GetNSObject_m3692132737_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AWSConfigs_GetConfigEnum_m3032820163_gp_0_0_0_0_Types[] = { (&AWSConfigs_GetConfigEnum_m3032820163_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_AWSConfigs_GetConfigEnum_m3032820163_gp_0_0_0_0 = { 1, GenInst_AWSConfigs_GetConfigEnum_m3032820163_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AWSConfigs_ParseEnum_m3861189188_gp_0_0_0_0_Types[] = { (&AWSConfigs_ParseEnum_m3861189188_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_AWSConfigs_ParseEnum_m3861189188_gp_0_0_0_0 = { 1, GenInst_AWSConfigs_ParseEnum_m3861189188_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AWSConfigs_GetSection_m1660313627_gp_0_0_0_0_Types[] = { (&AWSConfigs_GetSection_m1660313627_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_AWSConfigs_GetSection_m1660313627_gp_0_0_0_0 = { 1, GenInst_AWSConfigs_GetSection_m1660313627_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AWSSDKUtils_InvokeInBackground_m573940766_gp_0_0_0_0_Types[] = { (&AWSSDKUtils_InvokeInBackground_m573940766_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_AWSSDKUtils_InvokeInBackground_m573940766_gp_0_0_0_0 = { 1, GenInst_AWSSDKUtils_InvokeInBackground_m573940766_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CU3Ec__DisplayClass39_0_1_t500610588_gp_0_0_0_0_Types[] = { (&U3CU3Ec__DisplayClass39_0_1_t500610588_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CU3Ec__DisplayClass39_0_1_t500610588_gp_0_0_0_0 = { 1, GenInst_U3CU3Ec__DisplayClass39_0_1_t500610588_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AndroidInterop_CallMethod_m713902838_gp_0_0_0_0_Types[] = { (&AndroidInterop_CallMethod_m713902838_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_AndroidInterop_CallMethod_m713902838_gp_0_0_0_0 = { 1, GenInst_AndroidInterop_CallMethod_m713902838_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AndroidInterop_GetJavaField_m1179772153_gp_0_0_0_0_Types[] = { (&AndroidInterop_GetJavaField_m1179772153_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_AndroidInterop_GetJavaField_m1179772153_gp_0_0_0_0 = { 1, GenInst_AndroidInterop_GetJavaField_m1179772153_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CU3Ec__2_1_t1696963520_gp_0_0_0_0_Types[] = { (&U3CU3Ec__2_1_t1696963520_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CU3Ec__2_1_t1696963520_gp_0_0_0_0 = { 1, GenInst_U3CU3Ec__2_1_t1696963520_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CU3Ec__6_1_t2261613524_gp_0_0_0_0_Types[] = { (&U3CU3Ec__6_1_t2261613524_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CU3Ec__6_1_t2261613524_gp_0_0_0_0 = { 1, GenInst_U3CU3Ec__6_1_t2261613524_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ServiceFactory_GetService_m2302760766_gp_0_0_0_0_Types[] = { (&ServiceFactory_GetService_m2302760766_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ServiceFactory_GetService_m2302760766_gp_0_0_0_0 = { 1, GenInst_ServiceFactory_GetService_m2302760766_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IHttpRequestFactory_1_t4193720315_gp_0_0_0_0_Types[] = { (&IHttpRequestFactory_1_t4193720315_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IHttpRequestFactory_1_t4193720315_gp_0_0_0_0 = { 1, GenInst_IHttpRequestFactory_1_t4193720315_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AmazonServiceCallback_2_t3530050132_gp_0_0_0_0_AmazonServiceCallback_2_t3530050132_gp_1_0_0_0_Types[] = { (&AmazonServiceCallback_2_t3530050132_gp_0_0_0_0), (&AmazonServiceCallback_2_t3530050132_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_AmazonServiceCallback_2_t3530050132_gp_0_0_0_0_AmazonServiceCallback_2_t3530050132_gp_1_0_0_0 = { 2, GenInst_AmazonServiceCallback_2_t3530050132_gp_0_0_0_0_AmazonServiceCallback_2_t3530050132_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_AmazonServiceResult_2_t529501990_gp_0_0_0_0_AmazonServiceResult_2_t529501990_gp_1_0_0_0_Types[] = { (&AmazonServiceResult_2_t529501990_gp_0_0_0_0), (&AmazonServiceResult_2_t529501990_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_AmazonServiceResult_2_t529501990_gp_0_0_0_0_AmazonServiceResult_2_t529501990_gp_1_0_0_0 = { 2, GenInst_AmazonServiceResult_2_t529501990_gp_0_0_0_0_AmazonServiceResult_2_t529501990_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ExceptionHandler_1_t3179100912_gp_0_0_0_0_Types[] = { (&ExceptionHandler_1_t3179100912_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExceptionHandler_1_t3179100912_gp_0_0_0_0 = { 1, GenInst_ExceptionHandler_1_t3179100912_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HttpHandler_1_t1383532101_gp_0_0_0_0_Types[] = { (&HttpHandler_1_t1383532101_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HttpHandler_1_t1383532101_gp_0_0_0_0 = { 1, GenInst_HttpHandler_1_t1383532101_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultRetryPolicy_IsInnerException_m149306988_gp_0_0_0_0_Types[] = { (&DefaultRetryPolicy_IsInnerException_m149306988_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultRetryPolicy_IsInnerException_m149306988_gp_0_0_0_0 = { 1, GenInst_DefaultRetryPolicy_IsInnerException_m149306988_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_BackgroundDispatcher_1_t295417742_gp_0_0_0_0_Types[] = { (&BackgroundDispatcher_1_t295417742_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_BackgroundDispatcher_1_t295417742_gp_0_0_0_0 = { 1, GenInst_BackgroundDispatcher_1_t295417742_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HashStream_1_t458673105_gp_0_0_0_0_Types[] = { (&HashStream_1_t458673105_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HashStream_1_t458673105_gp_0_0_0_0 = { 1, GenInst_HashStream_1_t458673105_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_LruCache_2_t2774710777_gp_0_0_0_0_LruListItem_2_t3884836584_0_0_0_Types[] = { (&LruCache_2_t2774710777_gp_0_0_0_0), (&LruListItem_2_t3884836584_0_0_0) };
extern const Il2CppGenericInst GenInst_LruCache_2_t2774710777_gp_0_0_0_0_LruListItem_2_t3884836584_0_0_0 = { 2, GenInst_LruCache_2_t2774710777_gp_0_0_0_0_LruListItem_2_t3884836584_0_0_0_Types };
static const RuntimeType* GenInst_LruCache_2_t2774710777_gp_0_0_0_0_LruCache_2_t2774710777_gp_1_0_0_0_Types[] = { (&LruCache_2_t2774710777_gp_0_0_0_0), (&LruCache_2_t2774710777_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_LruCache_2_t2774710777_gp_0_0_0_0_LruCache_2_t2774710777_gp_1_0_0_0 = { 2, GenInst_LruCache_2_t2774710777_gp_0_0_0_0_LruCache_2_t2774710777_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_LruList_2_t2819118215_gp_0_0_0_0_LruList_2_t2819118215_gp_1_0_0_0_Types[] = { (&LruList_2_t2819118215_gp_0_0_0_0), (&LruList_2_t2819118215_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_LruList_2_t2819118215_gp_0_0_0_0_LruList_2_t2819118215_gp_1_0_0_0 = { 2, GenInst_LruList_2_t2819118215_gp_0_0_0_0_LruList_2_t2819118215_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_LruListItem_2_t4061194576_gp_0_0_0_0_LruListItem_2_t4061194576_gp_1_0_0_0_Types[] = { (&LruListItem_2_t4061194576_gp_0_0_0_0), (&LruListItem_2_t4061194576_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_LruListItem_2_t4061194576_gp_0_0_0_0_LruListItem_2_t4061194576_gp_1_0_0_0 = { 2, GenInst_LruListItem_2_t4061194576_gp_0_0_0_0_LruListItem_2_t4061194576_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types[] = { (&ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0 = { 1, GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t1927440687_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types[] = { (&Exception_t1927440687_0_0_0), (&ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ThreadPoolOptions_1_t170443295_0_0_0_Types[] = { (&ThreadPoolOptions_1_t170443295_0_0_0) };
extern const Il2CppGenericInst GenInst_ThreadPoolOptions_1_t170443295_0_0_0 = { 1, GenInst_ThreadPoolOptions_1_t170443295_0_0_0_Types };
static const RuntimeType* GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types[] = { (&ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0), (&ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0 = { 2, GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0_Types[] = { (&ThreadPoolOptions_1_t2545192696_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0 = { 1, GenInst_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t1927440687_0_0_0_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0_Types[] = { (&Exception_t1927440687_0_0_0), (&ThreadPoolOptions_1_t2545192696_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t1927440687_0_0_0_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0 = { 2, GenInst_Exception_t1927440687_0_0_0_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0_IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0_Types[] = { (&IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0), (&IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0_IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0 = { 2, GenInst_IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0_IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnmarshallerExtensions_Add_m421616549_gp_0_0_0_0_UnmarshallerExtensions_Add_m421616549_gp_1_0_0_0_Types[] = { (&UnmarshallerExtensions_Add_m421616549_gp_0_0_0_0), (&UnmarshallerExtensions_Add_m421616549_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_UnmarshallerExtensions_Add_m421616549_gp_0_0_0_0_UnmarshallerExtensions_Add_m421616549_gp_1_0_0_0 = { 2, GenInst_UnmarshallerExtensions_Add_m421616549_gp_0_0_0_0_UnmarshallerExtensions_Add_m421616549_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_Execute_m1744114673_gp_0_0_0_0_Types[] = { (&ExecuteEvents_Execute_m1744114673_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m1744114673_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m1744114673_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0_Types[] = { (&ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0_Types[] = { (&ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types[] = { (&TweenRunner_1_t2584777480_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0_Types[] = { (&Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0_Types[] = { (&SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types[] = { (&IndexedSet_1_t573160278_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types[] = { (&IndexedSet_1_t573160278_gp_0_0_0_0), (&Int32_t2071877448_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0 = { 2, GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0_Types };
static const RuntimeType* GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types[] = { (&ListPool_1_t1984115411_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ListPool_1_t1984115411_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t1984115411_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2000868992_0_0_0_Types[] = { (&List_1_t2000868992_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2000868992_0_0_0 = { 1, GenInst_List_1_t2000868992_0_0_0_Types };
static const RuntimeType* GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types[] = { (&ObjectPool_1_t4265859154_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_FastAction_1_t1338576437_gp_0_0_0_0_Types[] = { (&FastAction_1_t1338576437_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_FastAction_1_t1338576437_gp_0_0_0_0 = { 1, GenInst_FastAction_1_t1338576437_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t1788008268_0_0_0_Types[] = { (&Action_1_t1788008268_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t1788008268_0_0_0 = { 1, GenInst_Action_1_t1788008268_0_0_0_Types };
static const RuntimeType* GenInst_Action_1_t1788008268_0_0_0_LinkedListNode_1_t684114181_0_0_0_Types[] = { (&Action_1_t1788008268_0_0_0), (&LinkedListNode_1_t684114181_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_1_t1788008268_0_0_0_LinkedListNode_1_t684114181_0_0_0 = { 2, GenInst_Action_1_t1788008268_0_0_0_LinkedListNode_1_t684114181_0_0_0_Types };
static const RuntimeType* GenInst_FastAction_2_t1741860964_gp_0_0_0_0_FastAction_2_t1741860964_gp_1_0_0_0_Types[] = { (&FastAction_2_t1741860964_gp_0_0_0_0), (&FastAction_2_t1741860964_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_FastAction_2_t1741860964_gp_0_0_0_0_FastAction_2_t1741860964_gp_1_0_0_0 = { 2, GenInst_FastAction_2_t1741860964_gp_0_0_0_0_FastAction_2_t1741860964_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t3375677134_0_0_0_Types[] = { (&Action_2_t3375677134_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t3375677134_0_0_0 = { 1, GenInst_Action_2_t3375677134_0_0_0_Types };
static const RuntimeType* GenInst_Action_2_t3375677134_0_0_0_LinkedListNode_1_t2271783047_0_0_0_Types[] = { (&Action_2_t3375677134_0_0_0), (&LinkedListNode_1_t2271783047_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_2_t3375677134_0_0_0_LinkedListNode_1_t2271783047_0_0_0 = { 2, GenInst_Action_2_t3375677134_0_0_0_LinkedListNode_1_t2271783047_0_0_0_Types };
static const RuntimeType* GenInst_FastAction_3_t175777023_gp_0_0_0_0_FastAction_3_t175777023_gp_1_0_0_0_FastAction_3_t175777023_gp_2_0_0_0_Types[] = { (&FastAction_3_t175777023_gp_0_0_0_0), (&FastAction_3_t175777023_gp_1_0_0_0), (&FastAction_3_t175777023_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_FastAction_3_t175777023_gp_0_0_0_0_FastAction_3_t175777023_gp_1_0_0_0_FastAction_3_t175777023_gp_2_0_0_0 = { 3, GenInst_FastAction_3_t175777023_gp_0_0_0_0_FastAction_3_t175777023_gp_1_0_0_0_FastAction_3_t175777023_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_Action_3_t3712260761_0_0_0_Types[] = { (&Action_3_t3712260761_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_3_t3712260761_0_0_0 = { 1, GenInst_Action_3_t3712260761_0_0_0_Types };
static const RuntimeType* GenInst_Action_3_t3712260761_0_0_0_LinkedListNode_1_t2608366674_0_0_0_Types[] = { (&Action_3_t3712260761_0_0_0), (&LinkedListNode_1_t2608366674_0_0_0) };
extern const Il2CppGenericInst GenInst_Action_3_t3712260761_0_0_0_LinkedListNode_1_t2608366674_0_0_0 = { 2, GenInst_Action_3_t3712260761_0_0_0_LinkedListNode_1_t2608366674_0_0_0_Types };
static const RuntimeType* GenInst_TMPro_ExtensionMethods_FindInstanceID_m238029140_gp_0_0_0_0_Types[] = { (&TMPro_ExtensionMethods_FindInstanceID_m238029140_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TMPro_ExtensionMethods_FindInstanceID_m238029140_gp_0_0_0_0 = { 1, GenInst_TMPro_ExtensionMethods_FindInstanceID_m238029140_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TweenRunner_1_t579810420_gp_0_0_0_0_Types[] = { (&TweenRunner_1_t579810420_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t579810420_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t579810420_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TMP_Dropdown_GetOrAddComponent_m214373678_gp_0_0_0_0_Types[] = { (&TMP_Dropdown_GetOrAddComponent_m214373678_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_Dropdown_GetOrAddComponent_m214373678_gp_0_0_0_0 = { 1, GenInst_TMP_Dropdown_GetOrAddComponent_m214373678_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SetPropertyUtility_SetEquatableStruct_m1986669591_gp_0_0_0_0_Types[] = { (&SetPropertyUtility_SetEquatableStruct_m1986669591_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetEquatableStruct_m1986669591_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetEquatableStruct_m1986669591_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TMP_ListPool_1_t700915143_gp_0_0_0_0_Types[] = { (&TMP_ListPool_1_t700915143_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_ListPool_1_t700915143_gp_0_0_0_0 = { 1, GenInst_TMP_ListPool_1_t700915143_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t717668724_0_0_0_Types[] = { (&List_1_t717668724_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t717668724_0_0_0 = { 1, GenInst_List_1_t717668724_0_0_0_Types };
static const RuntimeType* GenInst_TMP_ObjectPool_1_t3742925414_gp_0_0_0_0_Types[] = { (&TMP_ObjectPool_1_t3742925414_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_ObjectPool_1_t3742925414_gp_0_0_0_0 = { 1, GenInst_TMP_ObjectPool_1_t3742925414_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TMP_Text_ResizeInternalArray_m2848200466_gp_0_0_0_0_Types[] = { (&TMP_Text_ResizeInternalArray_m2848200466_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_Text_ResizeInternalArray_m2848200466_gp_0_0_0_0 = { 1, GenInst_TMP_Text_ResizeInternalArray_m2848200466_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TMP_TextInfo_Resize_m2071664037_gp_0_0_0_0_Types[] = { (&TMP_TextInfo_Resize_m2071664037_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_TextInfo_Resize_m2071664037_gp_0_0_0_0 = { 1, GenInst_TMP_TextInfo_Resize_m2071664037_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TMP_TextInfo_Resize_m3363474848_gp_0_0_0_0_Types[] = { (&TMP_TextInfo_Resize_m3363474848_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_TextInfo_Resize_m3363474848_gp_0_0_0_0 = { 1, GenInst_TMP_TextInfo_Resize_m3363474848_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TMP_XmlTagStack_1_t558189074_gp_0_0_0_0_Types[] = { (&TMP_XmlTagStack_1_t558189074_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_XmlTagStack_1_t558189074_gp_0_0_0_0 = { 1, GenInst_TMP_XmlTagStack_1_t558189074_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types[] = { (&DefaultExecutionOrder_t2717914595_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t2717914595_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t2717914595_0_0_0_Types };
static const RuntimeType* GenInst_PlayerConnection_t3517219175_0_0_0_Types[] = { (&PlayerConnection_t3517219175_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3517219175_0_0_0 = { 1, GenInst_PlayerConnection_t3517219175_0_0_0_Types };
static const RuntimeType* GenInst_GUILayer_t3254902478_0_0_0_Types[] = { (&GUILayer_t3254902478_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayer_t3254902478_0_0_0 = { 1, GenInst_GUILayer_t3254902478_0_0_0_Types };
static const RuntimeType* GenInst_LoggingOptions_t2865640765_0_0_0_Types[] = { (&LoggingOptions_t2865640765_0_0_0) };
extern const Il2CppGenericInst GenInst_LoggingOptions_t2865640765_0_0_0 = { 1, GenInst_LoggingOptions_t2865640765_0_0_0_Types };
static const RuntimeType* GenInst_ResponseLoggingOption_t3443611737_0_0_0_Types[] = { (&ResponseLoggingOption_t3443611737_0_0_0) };
extern const Il2CppGenericInst GenInst_ResponseLoggingOption_t3443611737_0_0_0 = { 1, GenInst_ResponseLoggingOption_t3443611737_0_0_0_Types };
static const RuntimeType* GenInst_ThreadAbortException_t1150575753_0_0_0_Types[] = { (&ThreadAbortException_t1150575753_0_0_0) };
extern const Il2CppGenericInst GenInst_ThreadAbortException_t1150575753_0_0_0 = { 1, GenInst_ThreadAbortException_t1150575753_0_0_0_Types };
static const RuntimeType* GenInst_WebException_t3368933679_0_0_0_Types[] = { (&WebException_t3368933679_0_0_0) };
extern const Il2CppGenericInst GenInst_WebException_t3368933679_0_0_0 = { 1, GenInst_WebException_t3368933679_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1398341365_0_0_0_Types[] = { (&List_1_t1398341365_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1398341365_0_0_0 = { 1, GenInst_List_1_t1398341365_0_0_0_Types };
static const RuntimeType* GenInst_INetworkReachability_t2670889314_0_0_0_Types[] = { (&INetworkReachability_t2670889314_0_0_0) };
extern const Il2CppGenericInst GenInst_INetworkReachability_t2670889314_0_0_0 = { 1, GenInst_INetworkReachability_t2670889314_0_0_0_Types };
static const RuntimeType* GenInst_UnityInitializer_t2778189483_0_0_0_Types[] = { (&UnityInitializer_t2778189483_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityInitializer_t2778189483_0_0_0 = { 1, GenInst_UnityInitializer_t2778189483_0_0_0_Types };
static const RuntimeType* GenInst_UnityMainThreadDispatcher_t4098072663_0_0_0_Types[] = { (&UnityMainThreadDispatcher_t4098072663_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityMainThreadDispatcher_t4098072663_0_0_0 = { 1, GenInst_UnityMainThreadDispatcher_t4098072663_0_0_0_Types };
static const RuntimeType* GenInst_NSDictionary_t3598984691_0_0_0_Types[] = { (&NSDictionary_t3598984691_0_0_0) };
extern const Il2CppGenericInst GenInst_NSDictionary_t3598984691_0_0_0 = { 1, GenInst_NSDictionary_t3598984691_0_0_0_Types };
static const RuntimeType* GenInst_IEnvironmentInfo_t2314249358_0_0_0_Types[] = { (&IEnvironmentInfo_t2314249358_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnvironmentInfo_t2314249358_0_0_0 = { 1, GenInst_IEnvironmentInfo_t2314249358_0_0_0_Types };
static const RuntimeType* GenInst_AWSSection_t3096528870_0_0_0_Types[] = { (&AWSSection_t3096528870_0_0_0) };
extern const Il2CppGenericInst GenInst_AWSSection_t3096528870_0_0_0 = { 1, GenInst_AWSSection_t3096528870_0_0_0_Types };
static const RuntimeType* GenInst_NSBundle_t3599697655_0_0_0_Types[] = { (&NSBundle_t3599697655_0_0_0) };
extern const Il2CppGenericInst GenInst_NSBundle_t3599697655_0_0_0 = { 1, GenInst_NSBundle_t3599697655_0_0_0_Types };
static const RuntimeType* GenInst_NSObject_t1518098886_0_0_0_Types[] = { (&NSObject_t1518098886_0_0_0) };
extern const Il2CppGenericInst GenInst_NSObject_t1518098886_0_0_0 = { 1, GenInst_NSObject_t1518098886_0_0_0_Types };
static const RuntimeType* GenInst_NSLocale_t2224424797_0_0_0_Types[] = { (&NSLocale_t2224424797_0_0_0) };
extern const Il2CppGenericInst GenInst_NSLocale_t2224424797_0_0_0 = { 1, GenInst_NSLocale_t2224424797_0_0_0_Types };
static const RuntimeType* GenInst_UIDevice_t860038178_0_0_0_Types[] = { (&UIDevice_t860038178_0_0_0) };
extern const Il2CppGenericInst GenInst_UIDevice_t860038178_0_0_0 = { 1, GenInst_UIDevice_t860038178_0_0_0_Types };
static const RuntimeType* GenInst_AxisEventData_t1524870173_0_0_0_Types[] = { (&AxisEventData_t1524870173_0_0_0) };
extern const Il2CppGenericInst GenInst_AxisEventData_t1524870173_0_0_0 = { 1, GenInst_AxisEventData_t1524870173_0_0_0_Types };
static const RuntimeType* GenInst_SpriteRenderer_t1209076198_0_0_0_Types[] = { (&SpriteRenderer_t1209076198_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1209076198_0_0_0 = { 1, GenInst_SpriteRenderer_t1209076198_0_0_0_Types };
static const RuntimeType* GenInst_Image_t2042527209_0_0_0_Types[] = { (&Image_t2042527209_0_0_0) };
extern const Il2CppGenericInst GenInst_Image_t2042527209_0_0_0 = { 1, GenInst_Image_t2042527209_0_0_0_Types };
static const RuntimeType* GenInst_Button_t2872111280_0_0_0_Types[] = { (&Button_t2872111280_0_0_0) };
extern const Il2CppGenericInst GenInst_Button_t2872111280_0_0_0 = { 1, GenInst_Button_t2872111280_0_0_0_Types };
static const RuntimeType* GenInst_RawImage_t2749640213_0_0_0_Types[] = { (&RawImage_t2749640213_0_0_0) };
extern const Il2CppGenericInst GenInst_RawImage_t2749640213_0_0_0 = { 1, GenInst_RawImage_t2749640213_0_0_0_Types };
static const RuntimeType* GenInst_Slider_t297367283_0_0_0_Types[] = { (&Slider_t297367283_0_0_0) };
extern const Il2CppGenericInst GenInst_Slider_t297367283_0_0_0 = { 1, GenInst_Slider_t297367283_0_0_0_Types };
static const RuntimeType* GenInst_Scrollbar_t3248359358_0_0_0_Types[] = { (&Scrollbar_t3248359358_0_0_0) };
extern const Il2CppGenericInst GenInst_Scrollbar_t3248359358_0_0_0 = { 1, GenInst_Scrollbar_t3248359358_0_0_0_Types };
static const RuntimeType* GenInst_InputField_t1631627530_0_0_0_Types[] = { (&InputField_t1631627530_0_0_0) };
extern const Il2CppGenericInst GenInst_InputField_t1631627530_0_0_0 = { 1, GenInst_InputField_t1631627530_0_0_0_Types };
static const RuntimeType* GenInst_ScrollRect_t1199013257_0_0_0_Types[] = { (&ScrollRect_t1199013257_0_0_0) };
extern const Il2CppGenericInst GenInst_ScrollRect_t1199013257_0_0_0 = { 1, GenInst_ScrollRect_t1199013257_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_t1985816271_0_0_0_Types[] = { (&Dropdown_t1985816271_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_t1985816271_0_0_0 = { 1, GenInst_Dropdown_t1985816271_0_0_0_Types };
static const RuntimeType* GenInst_GraphicRaycaster_t410733016_0_0_0_Types[] = { (&GraphicRaycaster_t410733016_0_0_0) };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t410733016_0_0_0 = { 1, GenInst_GraphicRaycaster_t410733016_0_0_0_Types };
static const RuntimeType* GenInst_CanvasRenderer_t261436805_0_0_0_Types[] = { (&CanvasRenderer_t261436805_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t261436805_0_0_0 = { 1, GenInst_CanvasRenderer_t261436805_0_0_0_Types };
static const RuntimeType* GenInst_Corner_t1077473318_0_0_0_Types[] = { (&Corner_t1077473318_0_0_0) };
extern const Il2CppGenericInst GenInst_Corner_t1077473318_0_0_0 = { 1, GenInst_Corner_t1077473318_0_0_0_Types };
static const RuntimeType* GenInst_Axis_t1431825778_0_0_0_Types[] = { (&Axis_t1431825778_0_0_0) };
extern const Il2CppGenericInst GenInst_Axis_t1431825778_0_0_0 = { 1, GenInst_Axis_t1431825778_0_0_0_Types };
static const RuntimeType* GenInst_Constraint_t3558160636_0_0_0_Types[] = { (&Constraint_t3558160636_0_0_0) };
extern const Il2CppGenericInst GenInst_Constraint_t3558160636_0_0_0 = { 1, GenInst_Constraint_t3558160636_0_0_0_Types };
static const RuntimeType* GenInst_SubmitEvent_t907918422_0_0_0_Types[] = { (&SubmitEvent_t907918422_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmitEvent_t907918422_0_0_0 = { 1, GenInst_SubmitEvent_t907918422_0_0_0_Types };
static const RuntimeType* GenInst_OnChangeEvent_t2863344003_0_0_0_Types[] = { (&OnChangeEvent_t2863344003_0_0_0) };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2863344003_0_0_0 = { 1, GenInst_OnChangeEvent_t2863344003_0_0_0_Types };
static const RuntimeType* GenInst_OnValidateInput_t1946318473_0_0_0_Types[] = { (&OnValidateInput_t1946318473_0_0_0) };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1946318473_0_0_0 = { 1, GenInst_OnValidateInput_t1946318473_0_0_0_Types };
static const RuntimeType* GenInst_LayoutElement_t2808691390_0_0_0_Types[] = { (&LayoutElement_t2808691390_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutElement_t2808691390_0_0_0 = { 1, GenInst_LayoutElement_t2808691390_0_0_0_Types };
static const RuntimeType* GenInst_RectOffset_t3387826427_0_0_0_Types[] = { (&RectOffset_t3387826427_0_0_0) };
extern const Il2CppGenericInst GenInst_RectOffset_t3387826427_0_0_0 = { 1, GenInst_RectOffset_t3387826427_0_0_0_Types };
static const RuntimeType* GenInst_TextAnchor_t112990806_0_0_0_Types[] = { (&TextAnchor_t112990806_0_0_0) };
extern const Il2CppGenericInst GenInst_TextAnchor_t112990806_0_0_0 = { 1, GenInst_TextAnchor_t112990806_0_0_0_Types };
static const RuntimeType* GenInst_AnimationTriggers_t3244928895_0_0_0_Types[] = { (&AnimationTriggers_t3244928895_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t3244928895_0_0_0 = { 1, GenInst_AnimationTriggers_t3244928895_0_0_0_Types };
static const RuntimeType* GenInst_Animator_t69676727_0_0_0_Types[] = { (&Animator_t69676727_0_0_0) };
extern const Il2CppGenericInst GenInst_Animator_t69676727_0_0_0 = { 1, GenInst_Animator_t69676727_0_0_0_Types };
static const RuntimeType* GenInst_Marshaller_t3371889241_0_0_0_Types[] = { (&Marshaller_t3371889241_0_0_0) };
extern const Il2CppGenericInst GenInst_Marshaller_t3371889241_0_0_0 = { 1, GenInst_Marshaller_t3371889241_0_0_0_Types };
static const RuntimeType* GenInst_EndpointResolver_t2369326703_0_0_0_Types[] = { (&EndpointResolver_t2369326703_0_0_0) };
extern const Il2CppGenericInst GenInst_EndpointResolver_t2369326703_0_0_0 = { 1, GenInst_EndpointResolver_t2369326703_0_0_0_Types };
static const RuntimeType* GenInst_Unmarshaller_t1994457618_0_0_0_Types[] = { (&Unmarshaller_t1994457618_0_0_0) };
extern const Il2CppGenericInst GenInst_Unmarshaller_t1994457618_0_0_0 = { 1, GenInst_Unmarshaller_t1994457618_0_0_0_Types };
static const RuntimeType* GenInst_ErrorCallbackHandler_t394665619_0_0_0_Types[] = { (&ErrorCallbackHandler_t394665619_0_0_0) };
extern const Il2CppGenericInst GenInst_ErrorCallbackHandler_t394665619_0_0_0 = { 1, GenInst_ErrorCallbackHandler_t394665619_0_0_0_Types };
static const RuntimeType* GenInst_RetryHandler_t3257318146_0_0_0_Types[] = { (&RetryHandler_t3257318146_0_0_0) };
extern const Il2CppGenericInst GenInst_RetryHandler_t3257318146_0_0_0 = { 1, GenInst_RetryHandler_t3257318146_0_0_0_Types };
static const RuntimeType* GenInst_GetObjectRequest_t109865576_0_0_0_Types[] = { (&GetObjectRequest_t109865576_0_0_0) };
extern const Il2CppGenericInst GenInst_GetObjectRequest_t109865576_0_0_0 = { 1, GenInst_GetObjectRequest_t109865576_0_0_0_Types };
static const RuntimeType* GenInst_ListObjectsRequest_t3715455147_0_0_0_Types[] = { (&ListObjectsRequest_t3715455147_0_0_0) };
extern const Il2CppGenericInst GenInst_ListObjectsRequest_t3715455147_0_0_0 = { 1, GenInst_ListObjectsRequest_t3715455147_0_0_0_Types };
static const RuntimeType* GenInst_ReplicationStatus_t1991002226_0_0_0_Types[] = { (&ReplicationStatus_t1991002226_0_0_0) };
extern const Il2CppGenericInst GenInst_ReplicationStatus_t1991002226_0_0_0 = { 1, GenInst_ReplicationStatus_t1991002226_0_0_0_Types };
static const RuntimeType* GenInst_RequestCharged_t2438105727_0_0_0_Types[] = { (&RequestCharged_t2438105727_0_0_0) };
extern const Il2CppGenericInst GenInst_RequestCharged_t2438105727_0_0_0 = { 1, GenInst_RequestCharged_t2438105727_0_0_0_Types };
static const RuntimeType* GenInst_S3Region_t723338532_0_0_0_Types[] = { (&S3Region_t723338532_0_0_0) };
extern const Il2CppGenericInst GenInst_S3Region_t723338532_0_0_0 = { 1, GenInst_S3Region_t723338532_0_0_0_Types };
static const RuntimeType* GenInst_S3StorageClass_t454477475_0_0_0_Types[] = { (&S3StorageClass_t454477475_0_0_0) };
extern const Il2CppGenericInst GenInst_S3StorageClass_t454477475_0_0_0 = { 1, GenInst_S3StorageClass_t454477475_0_0_0_Types };
static const RuntimeType* GenInst_ServerSideEncryptionCustomerMethod_t3201425490_0_0_0_Types[] = { (&ServerSideEncryptionCustomerMethod_t3201425490_0_0_0) };
extern const Il2CppGenericInst GenInst_ServerSideEncryptionCustomerMethod_t3201425490_0_0_0 = { 1, GenInst_ServerSideEncryptionCustomerMethod_t3201425490_0_0_0_Types };
static const RuntimeType* GenInst_ServerSideEncryptionMethod_t608782770_0_0_0_Types[] = { (&ServerSideEncryptionMethod_t608782770_0_0_0) };
extern const Il2CppGenericInst GenInst_ServerSideEncryptionMethod_t608782770_0_0_0 = { 1, GenInst_ServerSideEncryptionMethod_t608782770_0_0_0_Types };
static const RuntimeType* GenInst_V4ClientSection_t2149061698_0_0_0_Types[] = { (&V4ClientSection_t2149061698_0_0_0) };
extern const Il2CppGenericInst GenInst_V4ClientSection_t2149061698_0_0_0 = { 1, GenInst_V4ClientSection_t2149061698_0_0_0_Types };
static const RuntimeType* GenInst_TextMeshPro_t2521834357_0_0_0_Types[] = { (&TextMeshPro_t2521834357_0_0_0) };
extern const Il2CppGenericInst GenInst_TextMeshPro_t2521834357_0_0_0 = { 1, GenInst_TextMeshPro_t2521834357_0_0_0_Types };
static const RuntimeType* GenInst_Renderer_t257310565_0_0_0_Types[] = { (&Renderer_t257310565_0_0_0) };
extern const Il2CppGenericInst GenInst_Renderer_t257310565_0_0_0 = { 1, GenInst_Renderer_t257310565_0_0_0_Types };
static const RuntimeType* GenInst_MeshFilter_t3026937449_0_0_0_Types[] = { (&MeshFilter_t3026937449_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshFilter_t3026937449_0_0_0 = { 1, GenInst_MeshFilter_t3026937449_0_0_0_Types };
static const RuntimeType* GenInst_TMP_InputField_t1778301588_0_0_0_Types[] = { (&TMP_InputField_t1778301588_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_InputField_t1778301588_0_0_0 = { 1, GenInst_TMP_InputField_t1778301588_0_0_0_Types };
static const RuntimeType* GenInst_TextMeshProUGUI_t934157183_0_0_0_Types[] = { (&TextMeshProUGUI_t934157183_0_0_0) };
extern const Il2CppGenericInst GenInst_TextMeshProUGUI_t934157183_0_0_0 = { 1, GenInst_TextMeshProUGUI_t934157183_0_0_0_Types };
static const RuntimeType* GenInst_TMP_Dropdown_t1768193147_0_0_0_Types[] = { (&TMP_Dropdown_t1768193147_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_Dropdown_t1768193147_0_0_0 = { 1, GenInst_TMP_Dropdown_t1768193147_0_0_0_Types };
static const RuntimeType* GenInst_SubmitEvent_t3359162065_0_0_0_Types[] = { (&SubmitEvent_t3359162065_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmitEvent_t3359162065_0_0_0 = { 1, GenInst_SubmitEvent_t3359162065_0_0_0_Types };
static const RuntimeType* GenInst_SelectionEvent_t3883897865_0_0_0_Types[] = { (&SelectionEvent_t3883897865_0_0_0) };
extern const Il2CppGenericInst GenInst_SelectionEvent_t3883897865_0_0_0 = { 1, GenInst_SelectionEvent_t3883897865_0_0_0_Types };
static const RuntimeType* GenInst_TextSelectionEvent_t3887183206_0_0_0_Types[] = { (&TextSelectionEvent_t3887183206_0_0_0) };
extern const Il2CppGenericInst GenInst_TextSelectionEvent_t3887183206_0_0_0 = { 1, GenInst_TextSelectionEvent_t3887183206_0_0_0_Types };
static const RuntimeType* GenInst_OnChangeEvent_t2139251414_0_0_0_Types[] = { (&OnChangeEvent_t2139251414_0_0_0) };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t2139251414_0_0_0 = { 1, GenInst_OnChangeEvent_t2139251414_0_0_0_Types };
static const RuntimeType* GenInst_OnValidateInput_t3285190392_0_0_0_Types[] = { (&OnValidateInput_t3285190392_0_0_0) };
extern const Il2CppGenericInst GenInst_OnValidateInput_t3285190392_0_0_0 = { 1, GenInst_OnValidateInput_t3285190392_0_0_0_Types };
static const RuntimeType* GenInst_LineType_t2475898675_0_0_0_Types[] = { (&LineType_t2475898675_0_0_0) };
extern const Il2CppGenericInst GenInst_LineType_t2475898675_0_0_0 = { 1, GenInst_LineType_t2475898675_0_0_0_Types };
static const RuntimeType* GenInst_InputType_t379073331_0_0_0_Types[] = { (&InputType_t379073331_0_0_0) };
extern const Il2CppGenericInst GenInst_InputType_t379073331_0_0_0 = { 1, GenInst_InputType_t379073331_0_0_0_Types };
static const RuntimeType* GenInst_CharacterValidation_t487603955_0_0_0_Types[] = { (&CharacterValidation_t487603955_0_0_0) };
extern const Il2CppGenericInst GenInst_CharacterValidation_t487603955_0_0_0 = { 1, GenInst_CharacterValidation_t487603955_0_0_0_Types };
static const RuntimeType* GenInst_TMP_InputValidator_t3726817866_0_0_0_Types[] = { (&TMP_InputValidator_t3726817866_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_InputValidator_t3726817866_0_0_0 = { 1, GenInst_TMP_InputValidator_t3726817866_0_0_0_Types };
static const RuntimeType* GenInst_TMP_SelectionCaret_t3802037885_0_0_0_Types[] = { (&TMP_SelectionCaret_t3802037885_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_SelectionCaret_t3802037885_0_0_0 = { 1, GenInst_TMP_SelectionCaret_t3802037885_0_0_0_Types };
static const RuntimeType* GenInst_BoxCollider_t22920061_0_0_0_Types[] = { (&BoxCollider_t22920061_0_0_0) };
extern const Il2CppGenericInst GenInst_BoxCollider_t22920061_0_0_0 = { 1, GenInst_BoxCollider_t22920061_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody_t4233889191_0_0_0_Types[] = { (&Rigidbody_t4233889191_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody_t4233889191_0_0_0 = { 1, GenInst_Rigidbody_t4233889191_0_0_0_Types };
static const RuntimeType* GenInst_TMP_SpriteAnimator_t2347923044_0_0_0_Types[] = { (&TMP_SpriteAnimator_t2347923044_0_0_0) };
extern const Il2CppGenericInst GenInst_TMP_SpriteAnimator_t2347923044_0_0_0 = { 1, GenInst_TMP_SpriteAnimator_t2347923044_0_0_0_Types };
static const RuntimeType* GenInst_EntryLink_t1717819552_0_0_0_Types[] = { (&EntryLink_t1717819552_0_0_0) };
extern const Il2CppGenericInst GenInst_EntryLink_t1717819552_0_0_0 = { 1, GenInst_EntryLink_t1717819552_0_0_0_Types };
static const RuntimeType* GenInst_PathCreator_t1368626459_0_0_0_Types[] = { (&PathCreator_t1368626459_0_0_0) };
extern const Il2CppGenericInst GenInst_PathCreator_t1368626459_0_0_0 = { 1, GenInst_PathCreator_t1368626459_0_0_0_Types };
static const RuntimeType* GenInst_RoadCreator_t1201856826_0_0_0_Types[] = { (&RoadCreator_t1201856826_0_0_0) };
extern const Il2CppGenericInst GenInst_RoadCreator_t1201856826_0_0_0 = { 1, GenInst_RoadCreator_t1201856826_0_0_0_Types };
static const RuntimeType* GenInst_MeshRenderer_t1268241104_0_0_0_Types[] = { (&MeshRenderer_t1268241104_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshRenderer_t1268241104_0_0_0 = { 1, GenInst_MeshRenderer_t1268241104_0_0_0_Types };
static const RuntimeType* GenInst_RacingWidgets_t915614521_0_0_0_Types[] = { (&RacingWidgets_t915614521_0_0_0) };
extern const Il2CppGenericInst GenInst_RacingWidgets_t915614521_0_0_0 = { 1, GenInst_RacingWidgets_t915614521_0_0_0_Types };
static const RuntimeType* GenInst_TextMesh_t1641806576_0_0_0_Types[] = { (&TextMesh_t1641806576_0_0_0) };
extern const Il2CppGenericInst GenInst_TextMesh_t1641806576_0_0_0 = { 1, GenInst_TextMesh_t1641806576_0_0_0_Types };
static const RuntimeType* GenInst_TextMeshProFloatingText_t6181308_0_0_0_Types[] = { (&TextMeshProFloatingText_t6181308_0_0_0) };
extern const Il2CppGenericInst GenInst_TextMeshProFloatingText_t6181308_0_0_0 = { 1, GenInst_TextMeshProFloatingText_t6181308_0_0_0_Types };
static const RuntimeType* GenInst_Light_t494725636_0_0_0_Types[] = { (&Light_t494725636_0_0_0) };
extern const Il2CppGenericInst GenInst_Light_t494725636_0_0_0 = { 1, GenInst_Light_t494725636_0_0_0_Types };
static const RuntimeType* GenInst_TextContainer_t4263764796_0_0_0_Types[] = { (&TextContainer_t4263764796_0_0_0) };
extern const Il2CppGenericInst GenInst_TextContainer_t4263764796_0_0_0 = { 1, GenInst_TextContainer_t4263764796_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types[] = { (&Byte_t3683104436_0_0_0), (&Byte_t3683104436_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0 = { 2, GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0_Types };
static const RuntimeType* GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types[] = { (&Char_t3454481338_0_0_0), (&Char_t3454481338_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types[] = { (&KeyValuePair_2_t38854645_0_0_0), (&KeyValuePair_2_t38854645_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0 = { 2, GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t94157543_0_0_0), (&CustomAttributeNamedArgument_t94157543_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t1498197914_0_0_0), (&CustomAttributeTypedArgument_t1498197914_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0_Types };
static const RuntimeType* GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types[] = { (&Single_t2076509932_0_0_0), (&Single_t2076509932_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0 = { 2, GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0_Types };
static const RuntimeType* GenInst_PropertyMetadata_t3287739986_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types[] = { (&PropertyMetadata_t3287739986_0_0_0), (&PropertyMetadata_t3287739986_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3287739986_0_0_0_PropertyMetadata_t3287739986_0_0_0 = { 2, GenInst_PropertyMetadata_t3287739986_0_0_0_PropertyMetadata_t3287739986_0_0_0_Types };
static const RuntimeType* GenInst_SpriteData_t257854902_0_0_0_SpriteData_t257854902_0_0_0_Types[] = { (&SpriteData_t257854902_0_0_0), (&SpriteData_t257854902_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteData_t257854902_0_0_0_SpriteData_t257854902_0_0_0 = { 2, GenInst_SpriteData_t257854902_0_0_0_SpriteData_t257854902_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types[] = { (&Color32_t874517518_0_0_0), (&Color32_t874517518_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0 = { 2, GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types[] = { (&RaycastResult_t21186376_0_0_0), (&RaycastResult_t21186376_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0 = { 2, GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types[] = { (&UICharInfo_t3056636800_0_0_0), (&UICharInfo_t3056636800_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0 = { 2, GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types[] = { (&UILineInfo_t3621277874_0_0_0), (&UILineInfo_t3621277874_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0 = { 2, GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types[] = { (&UIVertex_t1204258818_0_0_0), (&UIVertex_t1204258818_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0 = { 2, GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types[] = { (&Vector2_t2243707579_0_0_0), (&Vector2_t2243707579_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0 = { 2, GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types[] = { (&Vector3_t2243707580_0_0_0), (&Vector3_t2243707580_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0 = { 2, GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types[] = { (&Vector4_t2243707581_0_0_0), (&Vector4_t2243707581_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0 = { 2, GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0_Types };
static const RuntimeType* GenInst_Metric_t3273440202_0_0_0_Metric_t3273440202_0_0_0_Types[] = { (&Metric_t3273440202_0_0_0), (&Metric_t3273440202_0_0_0) };
extern const Il2CppGenericInst GenInst_Metric_t3273440202_0_0_0_Metric_t3273440202_0_0_0 = { 2, GenInst_Metric_t3273440202_0_0_0_Metric_t3273440202_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1416501748_0_0_0_KeyValuePair_2_t1416501748_0_0_0_Types[] = { (&KeyValuePair_2_t1416501748_0_0_0), (&KeyValuePair_2_t1416501748_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1416501748_0_0_0_KeyValuePair_2_t1416501748_0_0_0 = { 2, GenInst_KeyValuePair_2_t1416501748_0_0_0_KeyValuePair_2_t1416501748_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1416501748_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1416501748_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1416501748_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1416501748_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types[] = { (&Int64_t909078037_0_0_0), (&Int64_t909078037_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0 = { 2, GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3196873006_0_0_0_KeyValuePair_2_t3196873006_0_0_0_Types[] = { (&KeyValuePair_2_t3196873006_0_0_0), (&KeyValuePair_2_t3196873006_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3196873006_0_0_0_KeyValuePair_2_t3196873006_0_0_0 = { 2, GenInst_KeyValuePair_2_t3196873006_0_0_0_KeyValuePair_2_t3196873006_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3196873006_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3196873006_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3196873006_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3196873006_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types[] = { (&Boolean_t3825574718_0_0_0), (&Boolean_t3825574718_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0 = { 2, GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t590745575_0_0_0_KeyValuePair_2_t590745575_0_0_0_Types[] = { (&KeyValuePair_2_t590745575_0_0_0), (&KeyValuePair_2_t590745575_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t590745575_0_0_0_KeyValuePair_2_t590745575_0_0_0 = { 2, GenInst_KeyValuePair_2_t590745575_0_0_0_KeyValuePair_2_t590745575_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t590745575_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t590745575_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t590745575_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t590745575_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Char_t3454481338_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Char_t3454481338_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t3454481338_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Char_t3454481338_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t219652195_0_0_0_KeyValuePair_2_t219652195_0_0_0_Types[] = { (&KeyValuePair_2_t219652195_0_0_0), (&KeyValuePair_2_t219652195_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t219652195_0_0_0_KeyValuePair_2_t219652195_0_0_0 = { 2, GenInst_KeyValuePair_2_t219652195_0_0_0_KeyValuePair_2_t219652195_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t219652195_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t219652195_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t219652195_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t219652195_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types[] = { (&KeyValuePair_2_t3132015601_0_0_0), (&KeyValuePair_2_t3132015601_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3132015601_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3132015601_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3132015601_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3132015601_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1969216190_0_0_0_KeyValuePair_2_t1969216190_0_0_0_Types[] = { (&KeyValuePair_2_t1969216190_0_0_0), (&KeyValuePair_2_t1969216190_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1969216190_0_0_0_KeyValuePair_2_t1969216190_0_0_0 = { 2, GenInst_KeyValuePair_2_t1969216190_0_0_0_KeyValuePair_2_t1969216190_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1969216190_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1969216190_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1969216190_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1969216190_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types[] = { (&KeyValuePair_2_t3749587448_0_0_0), (&KeyValuePair_2_t3749587448_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3749587448_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3749587448_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3749587448_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3749587448_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types[] = { (&KeyValuePair_2_t1436312919_0_0_0), (&KeyValuePair_2_t1436312919_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0 = { 2, GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1436312919_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1436312919_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1436312919_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1436312919_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types[] = { (&KeyValuePair_2_t888819835_0_0_0), (&KeyValuePair_2_t888819835_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t888819835_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t888819835_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t888819835_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t888819835_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0_Types[] = { (&InstantiationModel_t1632807378_0_0_0), (&InstantiationModel_t1632807378_0_0_0) };
extern const Il2CppGenericInst GenInst_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0 = { 2, GenInst_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0_Types };
static const RuntimeType* GenInst_InstantiationModel_t1632807378_0_0_0_RuntimeObject_0_0_0_Types[] = { (&InstantiationModel_t1632807378_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_InstantiationModel_t1632807378_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_InstantiationModel_t1632807378_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3277180024_0_0_0_KeyValuePair_2_t3277180024_0_0_0_Types[] = { (&KeyValuePair_2_t3277180024_0_0_0), (&KeyValuePair_2_t3277180024_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3277180024_0_0_0_KeyValuePair_2_t3277180024_0_0_0 = { 2, GenInst_KeyValuePair_2_t3277180024_0_0_0_KeyValuePair_2_t3277180024_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3277180024_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3277180024_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3277180024_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3277180024_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types[] = { (&KeyValuePair_2_t1174980068_0_0_0), (&KeyValuePair_2_t1174980068_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1174980068_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1174980068_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1174980068_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1174980068_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types[] = { (&KeyValuePair_2_t3716250094_0_0_0), (&KeyValuePair_2_t3716250094_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3716250094_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3716250094_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3716250094_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3716250094_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t779664299_0_0_0_KeyValuePair_2_t779664299_0_0_0_Types[] = { (&KeyValuePair_2_t779664299_0_0_0), (&KeyValuePair_2_t779664299_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t779664299_0_0_0_KeyValuePair_2_t779664299_0_0_0 = { 2, GenInst_KeyValuePair_2_t779664299_0_0_0_KeyValuePair_2_t779664299_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t779664299_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t779664299_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t779664299_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t779664299_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t3430258949_0_0_0_RuntimeObject_0_0_0_Types[] = { (&TimeSpan_t3430258949_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_TimeSpan_t3430258949_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t3430258949_0_0_0_TimeSpan_t3430258949_0_0_0_Types[] = { (&TimeSpan_t3430258949_0_0_0), (&TimeSpan_t3430258949_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t3430258949_0_0_0_TimeSpan_t3430258949_0_0_0 = { 2, GenInst_TimeSpan_t3430258949_0_0_0_TimeSpan_t3430258949_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2779450660_0_0_0_KeyValuePair_2_t2779450660_0_0_0_Types[] = { (&KeyValuePair_2_t2779450660_0_0_0), (&KeyValuePair_2_t2779450660_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2779450660_0_0_0_KeyValuePair_2_t2779450660_0_0_0 = { 2, GenInst_KeyValuePair_2_t2779450660_0_0_0_KeyValuePair_2_t2779450660_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2779450660_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2779450660_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2779450660_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2779450660_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_ArrayMetadata_t1135078014_0_0_0_RuntimeObject_0_0_0_Types[] = { (&ArrayMetadata_t1135078014_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t1135078014_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_ArrayMetadata_t1135078014_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types[] = { (&ArrayMetadata_t1135078014_0_0_0), (&ArrayMetadata_t1135078014_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0 = { 2, GenInst_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1407543090_0_0_0_KeyValuePair_2_t1407543090_0_0_0_Types[] = { (&KeyValuePair_2_t1407543090_0_0_0), (&KeyValuePair_2_t1407543090_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1407543090_0_0_0_KeyValuePair_2_t1407543090_0_0_0 = { 2, GenInst_KeyValuePair_2_t1407543090_0_0_0_KeyValuePair_2_t1407543090_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1407543090_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1407543090_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1407543090_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1407543090_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_ObjectMetadata_t4058137740_0_0_0_RuntimeObject_0_0_0_Types[] = { (&ObjectMetadata_t4058137740_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t4058137740_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_ObjectMetadata_t4058137740_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types[] = { (&ObjectMetadata_t4058137740_0_0_0), (&ObjectMetadata_t4058137740_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0 = { 2, GenInst_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t637145336_0_0_0_KeyValuePair_2_t637145336_0_0_0_Types[] = { (&KeyValuePair_2_t637145336_0_0_0), (&KeyValuePair_2_t637145336_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t637145336_0_0_0_KeyValuePair_2_t637145336_0_0_0 = { 2, GenInst_KeyValuePair_2_t637145336_0_0_0_KeyValuePair_2_t637145336_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t637145336_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t637145336_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t637145336_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t637145336_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_PropertyMetadata_t3287739986_0_0_0_RuntimeObject_0_0_0_Types[] = { (&PropertyMetadata_t3287739986_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyMetadata_t3287739986_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_PropertyMetadata_t3287739986_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_LoggingSection_t905443946_0_0_0_Types[] = { (&LoggingSection_t905443946_0_0_0) };
extern const Il2CppGenericInst GenInst_LoggingSection_t905443946_0_0_0 = { 1, GenInst_LoggingSection_t905443946_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1364] = 
{
	&GenInst_RuntimeObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0,
	&GenInst_Char_t3454481338_0_0_0,
	&GenInst_Int64_t909078037_0_0_0,
	&GenInst_UInt32_t2149682021_0_0_0,
	&GenInst_UInt64_t2909196914_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0,
	&GenInst_SByte_t454417549_0_0_0,
	&GenInst_Int16_t4041245914_0_0_0,
	&GenInst_UInt16_t986882611_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t908092482_0_0_0,
	&GenInst_IComparable_t1857082765_0_0_0,
	&GenInst_IEnumerable_t2911409499_0_0_0,
	&GenInst_ICloneable_t3853279282_0_0_0,
	&GenInst_IComparable_1_t3861059456_0_0_0,
	&GenInst_IEquatable_1_t4233202402_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3412036974_0_0_0,
	&GenInst__Type_t102776839_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t502202687_0_0_0,
	&GenInst__MemberInfo_t332722161_0_0_0,
	&GenInst_Double_t4078015681_0_0_0,
	&GenInst_Single_t2076509932_0_0_0,
	&GenInst_Decimal_t724701077_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0,
	&GenInst_Delegate_t3022476291_0_0_0,
	&GenInst_ISerializable_t1245643778_0_0_0,
	&GenInst_ParameterInfo_t2249040075_0_0_0,
	&GenInst__ParameterInfo_t470209990_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2511231167_0_0_0,
	&GenInst_ParameterModifier_t1820634920_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3642518830_0_0_0,
	&GenInst_MethodBase_t904190842_0_0_0,
	&GenInst__MethodBase_t1935530873_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1567586598_0_0_0,
	&GenInst_ConstructorInfo_t2851816542_0_0_0,
	&GenInst__ConstructorInfo_t3269099341_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t2011406615_0_0_0,
	&GenInst_TailoringInfo_t1449609243_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_Link_t2723257478_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1744001932_0_0_0,
	&GenInst_Contraction_t1673853792_0_0_0,
	&GenInst_Level2Map_t3322505726_0_0_0,
	&GenInst_BigInteger_t925946152_0_0_0,
	&GenInst_KeySizes_t3144736271_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_Slot_t2022531261_0_0_0,
	&GenInst_Slot_t2267560602_0_0_0,
	&GenInst_StackFrame_t2050294881_0_0_0,
	&GenInst_Calendar_t585061108_0_0_0,
	&GenInst_ModuleBuilder_t4156028127_0_0_0,
	&GenInst__ModuleBuilder_t1075102050_0_0_0,
	&GenInst_Module_t4282841206_0_0_0,
	&GenInst__Module_t2144668161_0_0_0,
	&GenInst_ParameterBuilder_t3344728474_0_0_0,
	&GenInst__ParameterBuilder_t2251638747_0_0_0,
	&GenInst_TypeU5BU5D_t1664964607_0_0_0,
	&GenInst_RuntimeArray_0_0_0,
	&GenInst_ICollection_t91669223_0_0_0,
	&GenInst_IList_t3321498491_0_0_0,
	&GenInst_IList_1_t1844743827_0_0_0,
	&GenInst_ICollection_1_t2255878531_0_0_0,
	&GenInst_IEnumerable_1_t1595930271_0_0_0,
	&GenInst_IList_1_t3952977575_0_0_0,
	&GenInst_ICollection_1_t69144983_0_0_0,
	&GenInst_IEnumerable_1_t3704164019_0_0_0,
	&GenInst_IList_1_t643717440_0_0_0,
	&GenInst_ICollection_1_t1054852144_0_0_0,
	&GenInst_IEnumerable_1_t394903884_0_0_0,
	&GenInst_IList_1_t289070565_0_0_0,
	&GenInst_ICollection_1_t700205269_0_0_0,
	&GenInst_IEnumerable_1_t40257009_0_0_0,
	&GenInst_IList_1_t1043143288_0_0_0,
	&GenInst_ICollection_1_t1454277992_0_0_0,
	&GenInst_IEnumerable_1_t794329732_0_0_0,
	&GenInst_IList_1_t873662762_0_0_0,
	&GenInst_ICollection_1_t1284797466_0_0_0,
	&GenInst_IEnumerable_1_t624849206_0_0_0,
	&GenInst_IList_1_t3230389896_0_0_0,
	&GenInst_ICollection_1_t3641524600_0_0_0,
	&GenInst_IEnumerable_1_t2981576340_0_0_0,
	&GenInst_ILTokenInfo_t149559338_0_0_0,
	&GenInst_LabelData_t3712112744_0_0_0,
	&GenInst_LabelFixup_t4090909514_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1370236603_0_0_0,
	&GenInst_CustomAttributeBuilder_t2970303184_0_0_0,
	&GenInst__CustomAttributeBuilder_t3917123293_0_0_0,
	&GenInst_RefEmitPermissionSet_t2708608433_0_0_0,
	&GenInst_TypeBuilder_t3308873219_0_0_0,
	&GenInst__TypeBuilder_t2783404358_0_0_0,
	&GenInst_MethodBuilder_t644187984_0_0_0,
	&GenInst__MethodBuilder_t3932949077_0_0_0,
	&GenInst_FieldBuilder_t2784804005_0_0_0,
	&GenInst__FieldBuilder_t1895266044_0_0_0,
	&GenInst_MonoResource_t3127387157_0_0_0,
	&GenInst_ConstructorBuilder_t700974433_0_0_0,
	&GenInst__ConstructorBuilder_t1236878896_0_0_0,
	&GenInst_PropertyBuilder_t3694255912_0_0_0,
	&GenInst__PropertyBuilder_t3341912621_0_0_0,
	&GenInst_EventBuilder_t2927243889_0_0_0,
	&GenInst__EventBuilder_t801715496_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeData_t3093286891_0_0_0,
	&GenInst_ResourceInfo_t3933049236_0_0_0,
	&GenInst_ResourceCacheItem_t333236149_0_0_0,
	&GenInst_IContextProperty_t287246399_0_0_0,
	&GenInst_Header_t2756440555_0_0_0,
	&GenInst_ITrackingHandler_t2759960940_0_0_0,
	&GenInst_IContextAttribute_t2439121372_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0,
	&GenInst_TypeTag_t141209596_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t2988747270_0_0_0,
	&GenInst_IBuiltInEvidence_t1114073477_0_0_0,
	&GenInst_IIdentityPermissionFactory_t2988326850_0_0_0,
	&GenInst_DateTimeOffset_t1362988906_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t1755874712_0_0_0,
	&GenInst_Node_t2499136326_0_0_0,
	&GenInst_X509ChainStatus_t4278378721_0_0_0,
	&GenInst_IPAddress_t1399971723_0_0_0,
	&GenInst_ArraySegment_1_t2594217482_0_0_0,
	&GenInst_Cookie_t3154017544_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t3497699202_0_0_0,
	&GenInst_X509Certificate_t283079845_0_0_0,
	&GenInst_IDeserializationCallback_t327125377_0_0_0,
	&GenInst_Capture_t4157900610_0_0_0,
	&GenInst_Group_t3761430853_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_Mark_t2724874473_0_0_0,
	&GenInst_UriScheme_t1876590943_0_0_0,
	&GenInst_BigInteger_t925946153_0_0_0,
	&GenInst_ByteU5BU5D_t3397334013_0_0_0,
	&GenInst_IList_1_t4224045037_0_0_0,
	&GenInst_ICollection_1_t340212445_0_0_0,
	&GenInst_IEnumerable_1_t3975231481_0_0_0,
	&GenInst_ClientCertificateType_t4001384466_0_0_0,
	&GenInst_Link_t865133271_0_0_0,
	&GenInst_LockDetails_t2550015005_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AsyncOperation_t3814632279_0_0_0,
	&GenInst_Camera_t189460977_0_0_0,
	&GenInst_Behaviour_t955675639_0_0_0,
	&GenInst_Component_t3819376471_0_0_0,
	&GenInst_Object_t1021602117_0_0_0,
	&GenInst_Display_t3666191348_0_0_0,
	&GenInst_Keyframe_t1449471340_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0,
	&GenInst_Color32_t874517518_0_0_0,
	&GenInst_Playable_t1896841784_0_0_0,
	&GenInst_PlayableOutput_t988259697_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_LoadSceneMode_t2981886439_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0,
	&GenInst_Scene_t1684909666_0_0_0_Scene_t1684909666_0_0_0,
	&GenInst_SpriteAtlas_t3132429450_0_0_0,
	&GenInst_DisallowMultipleComponent_t2656950_0_0_0,
	&GenInst_Attribute_t542643598_0_0_0,
	&GenInst__Attribute_t1557664299_0_0_0,
	&GenInst_ExecuteInEditMode_t3043633143_0_0_0,
	&GenInst_RequireComponent_t864575032_0_0_0,
	&GenInst_HitInfo_t1761367055_0_0_0,
	&GenInst_PersistentCall_t3793436469_0_0_0,
	&GenInst_BaseInvokableCall_t2229564840_0_0_0,
	&GenInst_WorkRequest_t1154022482_0_0_0,
	&GenInst_PlayableBinding_t2498078091_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0,
	&GenInst_MessageTypeSubscribers_t2291506050_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_MessageEventArgs_t301283622_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_WeakReference_t1077405567_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3571743403_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t1077405567_0_0_0_KeyValuePair_2_t3571743403_0_0_0,
	&GenInst_AudioSpatializerExtensionDefinition_t3666104158_0_0_0,
	&GenInst_AudioAmbisonicExtensionDefinition_t52665437_0_0_0,
	&GenInst_AudioSourceExtension_t1813599864_0_0_0,
	&GenInst_ScriptableObject_t1975622470_0_0_0,
	&GenInst_AudioMixerPlayable_t1831808911_0_0_0,
	&GenInst_AudioClipPlayable_t192218916_0_0_0,
	&GenInst_Rigidbody2D_t502193897_0_0_0,
	&GenInst_Font_t4239498691_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t1701344717_0_0_0,
	&GenInst_XmlSchemaAttribute_t4015859774_0_0_0,
	&GenInst_XmlSchemaAnnotated_t2082486936_0_0_0,
	&GenInst_XmlSchemaObject_t2050913741_0_0_0,
	&GenInst_XsdIdentityPath_t2037874_0_0_0,
	&GenInst_XsdIdentityField_t2563516441_0_0_0,
	&GenInst_XsdIdentityStep_t452377251_0_0_0,
	&GenInst_XmlAttribute_t175731005_0_0_0,
	&GenInst_IHasXmlChildNode_t2048545686_0_0_0,
	&GenInst_XmlNode_t616554813_0_0_0,
	&GenInst_IXPathNavigable_t845515791_0_0_0,
	&GenInst_XmlQualifiedName_t1944712516_0_0_0,
	&GenInst_Regex_t1803876613_0_0_0,
	&GenInst_XmlSchemaSimpleType_t248156492_0_0_0,
	&GenInst_XmlSchemaType_t1795078578_0_0_0,
	&GenInst_XmlException_t4188277960_0_0_0,
	&GenInst_SystemException_t3877406272_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0,
	&GenInst__Exception_t3026971024_0_0_0,
	&GenInst_KeyValuePair_2_t1430411454_0_0_0,
	&GenInst_String_t_0_0_0_DTDNode_t1758286970_0_0_0,
	&GenInst_DTDNode_t1758286970_0_0_0,
	&GenInst_IXmlLineInfo_t135184468_0_0_0,
	&GenInst_AttributeSlot_t1499247213_0_0_0,
	&GenInst_Entry_t2583369454_0_0_0,
	&GenInst_NsDecl_t3210081295_0_0_0,
	&GenInst_NsScope_t2513625351_0_0_0,
	&GenInst_XmlAttributeTokenInfo_t3353594030_0_0_0,
	&GenInst_XmlTokenInfo_t254587324_0_0_0,
	&GenInst_TagName_t2340974457_0_0_0,
	&GenInst_XmlNodeInfo_t3709371029_0_0_0,
	&GenInst_AnimationClipPlayable_t4099382200_0_0_0,
	&GenInst_AnimationLayerMixerPlayable_t3057952312_0_0_0,
	&GenInst_AnimationMixerPlayable_t1343787797_0_0_0,
	&GenInst_AnimationOffsetPlayable_t1019600543_0_0_0,
	&GenInst_AnimatorControllerPlayable_t1744083903_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AchievementDescription_t3110978151_0_0_0,
	&GenInst_IAchievementDescription_t3498529102_0_0_0,
	&GenInst_UserProfile_t3365630962_0_0_0,
	&GenInst_IUserProfile_t4108565527_0_0_0,
	&GenInst_GcLeaderboard_t453887929_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t4083280315_0_0_0,
	&GenInst_IAchievementU5BU5D_t2709554645_0_0_0,
	&GenInst_IAchievement_t1752291260_0_0_0,
	&GenInst_GcAchievementData_t1754866149_0_0_0,
	&GenInst_Achievement_t1333316625_0_0_0,
	&GenInst_IScoreU5BU5D_t3237304636_0_0_0,
	&GenInst_IScore_t513966369_0_0_0,
	&GenInst_GcScoreData_t3676783238_0_0_0,
	&GenInst_Score_t2307748940_0_0_0,
	&GenInst_IUserProfileU5BU5D_t3461248430_0_0_0,
	&GenInst_GUILayoutOption_t4183744904_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_LayoutCache_t3120781045_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_LayoutCache_t3120781045_0_0_0_KeyValuePair_2_t4180919198_0_0_0,
	&GenInst_GUILayoutEntry_t3828586629_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_IntPtr_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t1799908754_0_0_0_KeyValuePair_2_t1472033238_0_0_0,
	&GenInst_RaycastHit_t87180320_0_0_0,
	&GenInst_ContactPoint_t1376425630_0_0_0,
	&GenInst_XAttribute_t3858477518_0_0_0,
	&GenInst_XObject_t3550811009_0_0_0,
	&GenInst_XNode_t2707504214_0_0_0,
	&GenInst_XElement_t553821050_0_0_0,
	&GenInst_IXmlSerializable_t2623090263_0_0_0,
	&GenInst_XContainer_t1445911831_0_0_0,
	&GenInst_XName_t785190363_0_0_0,
	&GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0,
	&GenInst_XNamespace_t1613015075_0_0_0,
	&GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1285139559_0_0_0,
	&GenInst_String_t_0_0_0_XNamespace_t1613015075_0_0_0_KeyValuePair_2_t1285139559_0_0_0,
	&GenInst_String_t_0_0_0_XName_t785190363_0_0_0,
	&GenInst_IEquatable_1_t2989172532_0_0_0,
	&GenInst_String_t_0_0_0_XName_t785190363_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t457314847_0_0_0,
	&GenInst_String_t_0_0_0_XName_t785190363_0_0_0_KeyValuePair_2_t457314847_0_0_0,
	&GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0,
	&GenInst_Func_2_t3675469371_0_0_0,
	&GenInst_MulticastDelegate_t3201952435_0_0_0,
	&GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3370172490_0_0_0,
	&GenInst_Type_t_0_0_0_Func_2_t3675469371_0_0_0_KeyValuePair_2_t3370172490_0_0_0,
	&GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0,
	&GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2694600775_0_0_0,
	&GenInst_String_t_0_0_0_Delegate_t3022476291_0_0_0_KeyValuePair_2_t2694600775_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0,
	&GenInst_Methods_t1187897474_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3682235310_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Methods_t1187897474_0_0_0_KeyValuePair_2_t3682235310_0_0_0,
	&GenInst_Dictionary_2_t1629922792_0_0_0,
	&GenInst_IDictionary_t596158605_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4124260628_0_0_0,
	&GenInst_IntPtr_t_0_0_0_Dictionary_2_t1629922792_0_0_0_KeyValuePair_2_t4124260628_0_0_0,
	&GenInst_JsonData_t4263252052_0_0_0,
	&GenInst_KeyValuePair_2_t3935376536_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0,
	&GenInst_IJsonWrapper_t3095378610_0_0_0,
	&GenInst_IOrderedDictionary_t252115128_0_0_0,
	&GenInst_IEquatable_1_t2172266925_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_String_t_0_0_0_JsonData_t4263252052_0_0_0_KeyValuePair_2_t3935376536_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0,
	&GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0,
	&GenInst_KeyValuePair_2_t637145336_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_KeyValuePair_2_t2779450660_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_KeyValuePair_2_t1407543090_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0,
	&GenInst_PropertyMetadata_t3287739986_0_0_0,
	&GenInst_Link_t204904209_0_0_0,
	&GenInst_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t2779450660_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t829781133_0_0_0,
	&GenInst_Type_t_0_0_0_ArrayMetadata_t1135078014_0_0_0_KeyValuePair_2_t829781133_0_0_0,
	&GenInst_IDictionary_2_t3266987655_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2961690774_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t3266987655_0_0_0_KeyValuePair_2_t2961690774_0_0_0,
	&GenInst_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t1407543090_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3752840859_0_0_0,
	&GenInst_Type_t_0_0_0_ObjectMetadata_t4058137740_0_0_0_KeyValuePair_2_t3752840859_0_0_0,
	&GenInst_IList_1_t3828680587_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3523383706_0_0_0,
	&GenInst_Type_t_0_0_0_IList_1_t3828680587_0_0_0_KeyValuePair_2_t3523383706_0_0_0,
	&GenInst_ExporterFunc_t173265409_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4162935824_0_0_0,
	&GenInst_Type_t_0_0_0_ExporterFunc_t173265409_0_0_0_KeyValuePair_2_t4162935824_0_0_0,
	&GenInst_IDictionary_2_t787128596_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t481831715_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t787128596_0_0_0_KeyValuePair_2_t481831715_0_0_0,
	&GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_PropertyMetadata_t3287739986_0_0_0,
	&GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_PropertyMetadata_t3287739986_0_0_0_KeyValuePair_2_t637145336_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2959864470_0_0_0,
	&GenInst_String_t_0_0_0_PropertyMetadata_t3287739986_0_0_0_KeyValuePair_2_t2959864470_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3025249456_0_0_0,
	&GenInst_Type_t_0_0_0_MethodInfo_t_0_0_0_KeyValuePair_2_t3025249456_0_0_0,
	&GenInst_ITypeInfo_t3592676621_0_0_0,
	&GenInst_ImporterFunc_t850687278_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t545390397_0_0_0,
	&GenInst_Type_t_0_0_0_ImporterFunc_t850687278_0_0_0_KeyValuePair_2_t545390397_0_0_0,
	&GenInst_JsonToken_t2445581255_0_0_0,
	&GenInst_WriterContext_t1209007092_0_0_0,
	&GenInst_StateHandler_t3489987002_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0,
	&GenInst_TraceListener_t3414949279_0_0_0,
	&GenInst_IDisposable_t2427283555_0_0_0,
	&GenInst_MarshalByRefObject_t1285298191_0_0_0,
	&GenInst_List_1_t2784070411_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2456194895_0_0_0,
	&GenInst_String_t_0_0_0_List_1_t2784070411_0_0_0_KeyValuePair_2_t2456194895_0_0_0,
	&GenInst_String_t_0_0_0_XElement_t553821050_0_0_0,
	&GenInst_XAttribute_t3858477518_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_XElement_t553821050_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t225945534_0_0_0,
	&GenInst_String_t_0_0_0_XElement_t553821050_0_0_0_KeyValuePair_2_t225945534_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0,
	&GenInst_RegionEndpoint_t661522805_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t333647289_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t661522805_0_0_0_KeyValuePair_2_t333647289_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0,
	&GenInst_RegionEndpoint_t1885241449_0_0_0,
	&GenInst_IRegionEndpoint_t544433888_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1557365933_0_0_0,
	&GenInst_String_t_0_0_0_RegionEndpoint_t1885241449_0_0_0_KeyValuePair_2_t1557365933_0_0_0,
	&GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0,
	&GenInst_Endpoint_t3348692641_0_0_0,
	&GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3020817125_0_0_0,
	&GenInst_String_t_0_0_0_Endpoint_t3348692641_0_0_0_KeyValuePair_2_t3020817125_0_0_0,
	&GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0,
	&GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t216558372_0_0_0,
	&GenInst_String_t_0_0_0_IRegionEndpoint_t544433888_0_0_0_KeyValuePair_2_t216558372_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_String_t_0_0_0_KeyValuePair_2_t3089358386_0_0_0,
	&GenInst_Action_t3226471752_0_0_0,
	&GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0,
	&GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0,
	&GenInst_KeyValuePair_2_t3277180024_0_0_0,
	&GenInst_Type_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0,
	&GenInst_Type_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_MethodInfo_t_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t1327510497_0_0_0,
	&GenInst_InstantiationModel_t1632807378_0_0_0,
	&GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0,
	&GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t3277180024_0_0_0,
	&GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Type_t_0_0_0_InstantiationModel_t1632807378_0_0_0_KeyValuePair_2_t1327510497_0_0_0,
	&GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2384152414_0_0_0,
	&GenInst_Type_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2384152414_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t998506345_0_0_0,
	&GenInst_Type_t_0_0_0_Type_t_0_0_0_KeyValuePair_2_t998506345_0_0_0,
	&GenInst_NetworkStatusEventArgs_t1607167653_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_AmazonWebServiceRequest_t3384026212_0_0_0,
	&GenInst_AmazonWebServiceRequest_t3384026212_0_0_0_AmazonWebServiceResponse_t529043356_0_0_0_Exception_t1927440687_0_0_0_AsyncOptions_t558351272_0_0_0,
	&GenInst_StreamTransferProgressArgs_t2639638063_0_0_0,
	&GenInst_IExecutionContext_t2477130752_0_0_0,
	&GenInst_IExecutionContext_t2477130752_0_0_0_Exception_t1927440687_0_0_0,
	&GenInst_IPipelineHandler_t2320756001_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2361573779_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0,
	&GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0,
	&GenInst_ConstantClass_t4000559886_0_0_0,
	&GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3672684370_0_0_0,
	&GenInst_String_t_0_0_0_ConstantClass_t4000559886_0_0_0_KeyValuePair_2_t3672684370_0_0_0,
	&GenInst_Dictionary_2_t1620371852_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1315074971_0_0_0,
	&GenInst_Type_t_0_0_0_Dictionary_2_t1620371852_0_0_0_KeyValuePair_2_t1315074971_0_0_0,
	&GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0,
	&GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0,
	&GenInst_KeyValuePair_2_t779664299_0_0_0,
	&GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_TimeSpan_t3430258949_0_0_0,
	&GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_RuntimeObject_0_0_0_TimeSpan_t3430258949_0_0_0_KeyValuePair_2_t779664299_0_0_0,
	&GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3102383433_0_0_0,
	&GenInst_String_t_0_0_0_TimeSpan_t3430258949_0_0_0_KeyValuePair_2_t3102383433_0_0_0,
	&GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0,
	&GenInst_RetryCapacity_t2794668894_0_0_0,
	&GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2466793378_0_0_0,
	&GenInst_String_t_0_0_0_RetryCapacity_t2794668894_0_0_0_KeyValuePair_2_t2466793378_0_0_0,
	&GenInst_String_t_0_0_0_ParameterValue_t2426009594_0_0_0,
	&GenInst_ParameterValue_t2426009594_0_0_0,
	&GenInst_KeyValuePair_2_t2098134078_0_0_0,
	&GenInst_Stream_t3255436806_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IRuntimePipelineCustomizer_t1222812694_0_0_0,
	&GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0,
	&GenInst_IExceptionHandler_t1411014698_0_0_0,
	&GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1105717817_0_0_0,
	&GenInst_Type_t_0_0_0_IExceptionHandler_t1411014698_0_0_0_KeyValuePair_2_t1105717817_0_0_0,
	&GenInst_HttpErrorResponseException_t3191555406_0_0_0,
	&GenInst_HttpStatusCode_t1898409641_0_0_0,
	&GenInst_WebExceptionStatus_t1169373531_0_0_0,
	&GenInst_Link_t74093617_0_0_0,
	&GenInst_Link_t3640024803_0_0_0,
	&GenInst_IAsyncExecutionContext_t3792344986_0_0_0,
	&GenInst_ThreadPoolOptions_1_t1583506835_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_RuntimeObject_0_0_0,
	&GenInst_ThreadPoolOptions_1_t3222238215_0_0_0,
	&GenInst_IAsyncExecutionContext_t3792344986_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_IAsyncExecutionContext_t3792344986_0_0_0,
	&GenInst_IUnityHttpRequest_t1859903397_0_0_0,
	&GenInst_RuntimeAsyncResult_t4279356013_0_0_0,
	&GenInst_IAsyncResult_t1999651008_0_0_0,
	&GenInst_HashingWrapperMD5_t3486550061_0_0_0,
	&GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0,
	&GenInst_InternalLogger_t2972373343_0_0_0,
	&GenInst_Logger_t2262497814_0_0_0,
	&GenInst_ILogger_t676853571_0_0_0,
	&GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1957200933_0_0_0,
	&GenInst_Type_t_0_0_0_Logger_t2262497814_0_0_0_KeyValuePair_2_t1957200933_0_0_0,
	&GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0,
	&GenInst_LruListItem_2_t38602651_0_0_0,
	&GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1682975297_0_0_0,
	&GenInst_RuntimeObject_0_0_0_LruListItem_2_t38602651_0_0_0_KeyValuePair_2_t1682975297_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3196873006_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_Metric_t3273440202_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3196873006_0_0_0,
	&GenInst_List_1_t2058570427_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2565994138_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t2058570427_0_0_0_KeyValuePair_2_t2565994138_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0,
	&GenInst_IMetricsTiming_t173410924_0_0_0,
	&GenInst_List_1_t3837499352_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t49955767_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_List_1_t3837499352_0_0_0_KeyValuePair_2_t49955767_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_KeyValuePair_2_t1416501748_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Metric_t3273440202_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1416501748_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0,
	&GenInst_Timing_t847194262_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1354617973_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Timing_t847194262_0_0_0_KeyValuePair_2_t1354617973_0_0_0,
	&GenInst_MetricError_t964444806_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_String_t_0_0_0,
	&GenInst_AmazonWebServiceResponse_t529043356_0_0_0_UnmarshallerContext_t1608322995_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_String_t_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_String_t_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0_RuntimeObject_0_0_0,
	&GenInst_DateTime_t693205669_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_XmlNodeType_t739504597_0_0_0,
	&GenInst_Link_t3210155869_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0_String_t_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1701344717_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_EventSystem_t3466835263_0_0_0,
	&GenInst_UIBehaviour_t3960014691_0_0_0,
	&GenInst_MonoBehaviour_t1158329972_0_0_0,
	&GenInst_BaseInputModule_t1295781545_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0,
	&GenInst_IDeselectHandler_t3182198310_0_0_0,
	&GenInst_IEventSystemHandler_t2741188318_0_0_0,
	&GenInst_List_1_t2110309450_0_0_0,
	&GenInst_List_1_t3188497603_0_0_0,
	&GenInst_ISelectHandler_t2812555161_0_0_0,
	&GenInst_BaseRaycaster_t2336171397_0_0_0,
	&GenInst_Entry_t3365010046_0_0_0,
	&GenInst_BaseEventData_t2681005625_0_0_0,
	&GenInst_IPointerEnterHandler_t193164956_0_0_0,
	&GenInst_IPointerExitHandler_t461019860_0_0_0,
	&GenInst_IPointerDownHandler_t3929046918_0_0_0,
	&GenInst_IPointerUpHandler_t1847764461_0_0_0,
	&GenInst_IPointerClickHandler_t96169666_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3350809087_0_0_0,
	&GenInst_IBeginDragHandler_t3135127860_0_0_0,
	&GenInst_IDragHandler_t2583993319_0_0_0,
	&GenInst_IEndDragHandler_t1349123600_0_0_0,
	&GenInst_IDropHandler_t2390101210_0_0_0,
	&GenInst_IScrollHandler_t3834677510_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3778909353_0_0_0,
	&GenInst_IMoveHandler_t2611925506_0_0_0,
	&GenInst_ISubmitHandler_t525803901_0_0_0,
	&GenInst_ICancelHandler_t1980319651_0_0_0,
	&GenInst_Transform_t3275118058_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0,
	&GenInst_BaseInput_t621514313_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_PointerEventData_t1599784723_0_0_0,
	&GenInst_AbstractEventData_t1333959294_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_KeyValuePair_2_t2659922876_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_PointerEventData_t1599784723_0_0_0_PointerEventData_t1599784723_0_0_0,
	&GenInst_ButtonState_t2688375492_0_0_0,
	&GenInst_RaycastHit2D_t4063908774_0_0_0,
	&GenInst_Color_t2020392075_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ColorBlock_t2652774230_0_0_0,
	&GenInst_OptionData_t2420267500_0_0_0,
	&GenInst_DropdownItem_t4139978805_0_0_0,
	&GenInst_FloatTween_t2986189219_0_0_0,
	&GenInst_Sprite_t309593783_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0,
	&GenInst_List_1_t3873494194_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0,
	&GenInst_Text_t356221433_0_0_0,
	&GenInst_Link_t2826872705_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0,
	&GenInst_MaskableGraphic_t540192618_0_0_0,
	&GenInst_IClippable_t1941276057_0_0_0,
	&GenInst_IMaskable_t1431842707_0_0_0,
	&GenInst_IMaterialModifier_t3028564983_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0,
	&GenInst_HashSet_1_t2984649583_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_Font_t4239498691_0_0_0_HashSet_1_t2984649583_0_0_0_KeyValuePair_2_t850112849_0_0_0,
	&GenInst_ColorTween_t3438117476_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_IndexedSet_1_t286373651_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_Canvas_t209405766_0_0_0_IndexedSet_1_t286373651_0_0_0_KeyValuePair_2_t2391682566_0_0_0,
	&GenInst_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_Graphic_t2426225576_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t3010968081_0_0_0,
	&GenInst_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_ICanvasElement_t986520779_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t1912381698_0_0_0,
	&GenInst_Type_t3352948571_0_0_0,
	&GenInst_FillMethod_t1640962579_0_0_0,
	&GenInst_ContentType_t1028629049_0_0_0,
	&GenInst_LineType_t2931319356_0_0_0,
	&GenInst_InputType_t1274231802_0_0_0,
	&GenInst_TouchScreenKeyboardType_t875112366_0_0_0,
	&GenInst_CharacterValidation_t3437478890_0_0_0,
	&GenInst_Mask_t2977958238_0_0_0,
	&GenInst_ICanvasRaycastFilter_t1367822892_0_0_0,
	&GenInst_List_1_t2347079370_0_0_0,
	&GenInst_RectMask2D_t1156185964_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0,
	&GenInst_List_1_t525307096_0_0_0,
	&GenInst_Navigation_t1571958496_0_0_0,
	&GenInst_Link_t116960033_0_0_0,
	&GenInst_Direction_t3696775921_0_0_0,
	&GenInst_Selectable_t1490392188_0_0_0,
	&GenInst_Transition_t605142169_0_0_0,
	&GenInst_SpriteState_t1353336012_0_0_0,
	&GenInst_CanvasGroup_t3296560743_0_0_0,
	&GenInst_Direction_t1525323322_0_0_0,
	&GenInst_MatEntry_t3157325053_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0,
	&GenInst_Toggle_t3976754468_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_IClipper_t900477982_0_0_0_Int32_t2071877448_0_0_0_KeyValuePair_2_t379984643_0_0_0,
	&GenInst_AspectMode_t1166448724_0_0_0,
	&GenInst_FitMode_t4030874534_0_0_0,
	&GenInst_RectTransform_t3349966182_0_0_0,
	&GenInst_LayoutRebuilder_t2155218138_0_0_0,
	&GenInst_ILayoutElement_t1975293769_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_List_1_t1612828712_0_0_0,
	&GenInst_List_1_t243638650_0_0_0,
	&GenInst_List_1_t1612828711_0_0_0,
	&GenInst_List_1_t1612828713_0_0_0,
	&GenInst_List_1_t1440998580_0_0_0,
	&GenInst_List_1_t573379950_0_0_0,
	&GenInst_GetObjectRequest_t109865576_0_0_0_GetObjectResponse_t653598534_0_0_0,
	&GenInst_ListObjectsRequest_t3715455147_0_0_0_ListObjectsResponse_t2587169543_0_0_0,
	&GenInst_DeletedObject_t2664222218_0_0_0,
	&GenInst_DeleteError_t3399861423_0_0_0,
	&GenInst_String_t_0_0_0_LruListItem_2_t333395295_0_0_0,
	&GenInst_LruListItem_2_t333395295_0_0_0,
	&GenInst_String_t_0_0_0_LruListItem_2_t333395295_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_S3Object_t1836716019_0_0_0,
	&GenInst_S3Object_t1836716019_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_S3Object_t1836716019_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_GetObjectRequest_t109865576_0_0_0,
	&GenInst_IRequest_t2400804350_0_0_0_ListObjectsRequest_t3715455147_0_0_0,
	&GenInst_Owner_t97740679_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Owner_t97740679_0_0_0_JsonUnmarshallerContext_t456235889_0_0_0,
	&GenInst_S3ErrorResponse_t623087499_0_0_0_XmlUnmarshallerContext_t1179575220_0_0_0,
	&GenInst_Link_t3774454498_0_0_0,
	&GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0,
	&GenInst_LinkedListNode_1_t2122577665_0_0_0,
	&GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t239539626_0_0_0,
	&GenInst_Action_t3226471752_0_0_0_LinkedListNode_1_t2122577665_0_0_0_KeyValuePair_2_t239539626_0_0_0,
	&GenInst_Action_1_t2491248677_0_0_0,
	&GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0,
	&GenInst_LinkedListNode_1_t1387354590_0_0_0,
	&GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3584681366_0_0_0,
	&GenInst_Action_1_t2491248677_0_0_0_LinkedListNode_1_t1387354590_0_0_0_KeyValuePair_2_t3584681366_0_0_0,
	&GenInst_Action_2_t2572051853_0_0_0,
	&GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0,
	&GenInst_LinkedListNode_1_t1468157766_0_0_0,
	&GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1008245494_0_0_0,
	&GenInst_Action_2_t2572051853_0_0_0_LinkedListNode_1_t1468157766_0_0_0_KeyValuePair_2_t1008245494_0_0_0,
	&GenInst_Action_3_t1115657183_0_0_0,
	&GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0,
	&GenInst_LinkedListNode_1_t11763096_0_0_0,
	&GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t4123020654_0_0_0,
	&GenInst_Action_3_t1115657183_0_0_0_LinkedListNode_1_t11763096_0_0_0_KeyValuePair_2_t4123020654_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0,
	&GenInst_Material_t193706927_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1253845080_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Material_t193706927_0_0_0_KeyValuePair_2_t1253845080_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0,
	&GenInst_TMP_FontAsset_t2530419979_0_0_0,
	&GenInst_TMP_Asset_t1084708044_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3590558132_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_FontAsset_t2530419979_0_0_0_KeyValuePair_2_t3590558132_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0,
	&GenInst_TMP_SpriteAsset_t2641813093_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t3701951246_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_SpriteAsset_t2641813093_0_0_0_KeyValuePair_2_t3701951246_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0,
	&GenInst_TMP_ColorGradient_t1159837347_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2219975500_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_ColorGradient_t1159837347_0_0_0_KeyValuePair_2_t2219975500_0_0_0,
	&GenInst_MaterialReference_t2854353496_0_0_0,
	&GenInst_TMP_SubMesh_t3507543655_0_0_0,
	&GenInst_TMP_MeshInfo_t1297308317_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0,
	&GenInst_TMP_Glyph_t909793902_0_0_0,
	&GenInst_TMP_TextElement_t2285620223_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1969932055_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_Glyph_t909793902_0_0_0_KeyValuePair_2_t1969932055_0_0_0,
	&GenInst_TMP_CharacterInfo_t1421302791_0_0_0,
	&GenInst_TextAlignmentOptions_t1466788324_0_0_0,
	&GenInst_TMP_PageInfo_t3845132337_0_0_0,
	&GenInst_TMP_Sprite_t104383505_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0,
	&GenInst_KerningPair_t1577753922_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2637892075_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_KerningPair_t1577753922_0_0_0_KeyValuePair_2_t2637892075_0_0_0,
	&GenInst_TMP_LineInfo_t2320418126_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_KeyValuePair_2_t219652195_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Char_t3454481338_0_0_0_KeyValuePair_2_t219652195_0_0_0,
	&GenInst_TMP_WordInfo_t3807457612_0_0_0,
	&GenInst_TMP_SubMeshUI_t1983202343_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Compute_DT_EventArgs_t4231491594_0_0_0,
	&GenInst_Action_2_t4114094152_0_0_0,
	&GenInst_Action_2_t4114094152_0_0_0_LinkedListNode_1_t3010200065_0_0_0,
	&GenInst_LinkedListNode_1_t3010200065_0_0_0,
	&GenInst_Action_2_t4114094152_0_0_0_LinkedListNode_1_t3010200065_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Material_t193706927_0_0_0,
	&GenInst_Action_2_t2525452034_0_0_0,
	&GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0,
	&GenInst_LinkedListNode_1_t1421557947_0_0_0,
	&GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2048271746_0_0_0,
	&GenInst_Action_2_t2525452034_0_0_0_LinkedListNode_1_t1421557947_0_0_0_KeyValuePair_2_t2048271746_0_0_0,
	&GenInst_Action_2_t29709666_0_0_0,
	&GenInst_Action_2_t29709666_0_0_0_LinkedListNode_1_t3220782875_0_0_0,
	&GenInst_LinkedListNode_1_t3220782875_0_0_0,
	&GenInst_Action_2_t29709666_0_0_0_LinkedListNode_1_t3220782875_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_TMP_FontAsset_t2530419979_0_0_0,
	&GenInst_Action_2_t2366422718_0_0_0,
	&GenInst_Action_2_t2366422718_0_0_0_LinkedListNode_1_t1262528631_0_0_0,
	&GenInst_LinkedListNode_1_t1262528631_0_0_0,
	&GenInst_Action_2_t2366422718_0_0_0_LinkedListNode_1_t1262528631_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Object_t1021602117_0_0_0,
	&GenInst_Action_2_t857604856_0_0_0,
	&GenInst_Action_2_t857604856_0_0_0_LinkedListNode_1_t4048678065_0_0_0,
	&GenInst_LinkedListNode_1_t4048678065_0_0_0,
	&GenInst_Action_2_t857604856_0_0_0_LinkedListNode_1_t4048678065_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_TextMeshPro_t2521834357_0_0_0,
	&GenInst_Action_2_t2357837096_0_0_0,
	&GenInst_Action_2_t2357837096_0_0_0_LinkedListNode_1_t1253943009_0_0_0,
	&GenInst_LinkedListNode_1_t1253943009_0_0_0,
	&GenInst_Action_2_t2357837096_0_0_0_LinkedListNode_1_t1253943009_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_GameObject_t1756533147_0_0_0_Material_t193706927_0_0_0_Material_t193706927_0_0_0,
	&GenInst_Action_3_t408506571_0_0_0,
	&GenInst_Action_3_t408506571_0_0_0_LinkedListNode_1_t3599579780_0_0_0,
	&GenInst_LinkedListNode_1_t3599579780_0_0_0,
	&GenInst_Action_3_t408506571_0_0_0_LinkedListNode_1_t3599579780_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Action_1_t3627374100_0_0_0,
	&GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0,
	&GenInst_LinkedListNode_1_t2523480013_0_0_0,
	&GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t379239674_0_0_0,
	&GenInst_Action_1_t3627374100_0_0_0_LinkedListNode_1_t2523480013_0_0_0_KeyValuePair_2_t379239674_0_0_0,
	&GenInst_Action_1_t961636729_0_0_0,
	&GenInst_Action_1_t961636729_0_0_0_LinkedListNode_1_t4152709938_0_0_0,
	&GenInst_LinkedListNode_1_t4152709938_0_0_0,
	&GenInst_Action_1_t961636729_0_0_0_LinkedListNode_1_t4152709938_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_TextMeshProUGUI_t934157183_0_0_0,
	&GenInst_Action_2_t770159922_0_0_0,
	&GenInst_Action_2_t770159922_0_0_0_LinkedListNode_1_t3961233131_0_0_0,
	&GenInst_LinkedListNode_1_t3961233131_0_0_0,
	&GenInst_Action_2_t770159922_0_0_0_LinkedListNode_1_t3961233131_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Action_1_t823401499_0_0_0,
	&GenInst_Action_1_t823401499_0_0_0_LinkedListNode_1_t4014474708_0_0_0,
	&GenInst_LinkedListNode_1_t4014474708_0_0_0,
	&GenInst_Action_1_t823401499_0_0_0_LinkedListNode_1_t4014474708_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KerningPair_t1577753922_0_0_0_UInt32_t2149682021_0_0_0,
	&GenInst_RuntimeObject_0_0_0_UInt32_t2149682021_0_0_0,
	&GenInst_OptionData_t234712921_0_0_0,
	&GenInst_DropdownItem_t1251916390_0_0_0,
	&GenInst_FloatTween_t1652887471_0_0_0,
	&GenInst_TMP_FontWeights_t1564168302_0_0_0,
	&GenInst_TMP_Glyph_t909793902_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ContentType_t4294436424_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2071877448_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_MaskingMaterial_t590070688_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0,
	&GenInst_KeyValuePair_2_t1436312919_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t1436312919_0_0_0,
	&GenInst_FallbackMaterial_t3285989240_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t2032852864_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_FallbackMaterial_t3285989240_0_0_0_KeyValuePair_2_t2032852864_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_KeyValuePair_2_t1969216190_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Int64_t909078037_0_0_0_KeyValuePair_2_t1969216190_0_0_0,
	&GenInst_List_1_t2397686115_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t590745575_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_Boolean_t3825574718_0_0_0_KeyValuePair_2_t590745575_0_0_0,
	&GenInst_SpriteData_t257854902_0_0_0,
	&GenInst_TMP_Style_t69737451_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_KeyValuePair_2_t1129875604_0_0_0,
	&GenInst_Int32_t2071877448_0_0_0_TMP_Style_t69737451_0_0_0_KeyValuePair_2_t1129875604_0_0_0,
	&GenInst_XML_TagAttribute_t1879784992_0_0_0,
	&GenInst_TMP_LinkInfo_t3626894960_0_0_0,
	&GenInst_TMP_Text_t1920000777_0_0_0,
	&GenInst_StringU5BU5D_t1642385972_0_0_0,
	&GenInst_IList_1_t2570160834_0_0_0,
	&GenInst_ICollection_1_t2981295538_0_0_0,
	&GenInst_IEnumerable_1_t2321347278_0_0_0,
	&GenInst_IList_1_t1449033083_0_0_0,
	&GenInst_ICollection_1_t1860167787_0_0_0,
	&GenInst_IEnumerable_1_t1200219527_0_0_0,
	&GenInst_IList_1_t2398023366_0_0_0,
	&GenInst_ICollection_1_t2809158070_0_0_0,
	&GenInst_IEnumerable_1_t2149209810_0_0_0,
	&GenInst_IList_1_t3452350100_0_0_0,
	&GenInst_ICollection_1_t3863484804_0_0_0,
	&GenInst_IEnumerable_1_t3203536544_0_0_0,
	&GenInst_IList_1_t99252587_0_0_0,
	&GenInst_ICollection_1_t510387291_0_0_0,
	&GenInst_IEnumerable_1_t4145406327_0_0_0,
	&GenInst_IList_1_t107032761_0_0_0,
	&GenInst_ICollection_1_t518167465_0_0_0,
	&GenInst_IEnumerable_1_t4153186501_0_0_0,
	&GenInst_IList_1_t479175707_0_0_0,
	&GenInst_ICollection_1_t890310411_0_0_0,
	&GenInst_IEnumerable_1_t230362151_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_VertexAnim_t2147777005_0_0_0,
	&GenInst_Vector3U5BU5D_t1172311765_0_0_0,
	&GenInst_IList_1_t2784648181_0_0_0,
	&GenInst_ICollection_1_t3195782885_0_0_0,
	&GenInst_IEnumerable_1_t2535834625_0_0_0,
	&GenInst_IEnumerable_1_t4048664256_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m801276902_gp_0_0_0_0,
	&GenInst_Array_Sort_m2069139338_gp_0_0_0_0_Array_Sort_m2069139338_gp_0_0_0_0,
	&GenInst_Array_Sort_m1999601238_gp_0_0_0_0_Array_Sort_m1999601238_gp_1_0_0_0,
	&GenInst_Array_Sort_m4168899348_gp_0_0_0_0,
	&GenInst_Array_Sort_m4168899348_gp_0_0_0_0_Array_Sort_m4168899348_gp_0_0_0_0,
	&GenInst_Array_Sort_m1711256281_gp_0_0_0_0,
	&GenInst_Array_Sort_m1711256281_gp_0_0_0_0_Array_Sort_m1711256281_gp_1_0_0_0,
	&GenInst_Array_Sort_m714603628_gp_0_0_0_0_Array_Sort_m714603628_gp_0_0_0_0,
	&GenInst_Array_Sort_m4057794348_gp_0_0_0_0_Array_Sort_m4057794348_gp_1_0_0_0,
	&GenInst_Array_Sort_m2544329074_gp_0_0_0_0,
	&GenInst_Array_Sort_m2544329074_gp_0_0_0_0_Array_Sort_m2544329074_gp_0_0_0_0,
	&GenInst_Array_Sort_m54102389_gp_0_0_0_0,
	&GenInst_Array_Sort_m54102389_gp_1_0_0_0,
	&GenInst_Array_Sort_m54102389_gp_0_0_0_0_Array_Sort_m54102389_gp_1_0_0_0,
	&GenInst_Array_Sort_m2673640633_gp_0_0_0_0,
	&GenInst_Array_Sort_m285045864_gp_0_0_0_0,
	&GenInst_Array_qsort_m965712368_gp_0_0_0_0,
	&GenInst_Array_qsort_m965712368_gp_0_0_0_0_Array_qsort_m965712368_gp_1_0_0_0,
	&GenInst_Array_compare_m1681301885_gp_0_0_0_0,
	&GenInst_Array_qsort_m2216502740_gp_0_0_0_0,
	&GenInst_Array_Resize_m2875826811_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m1718874735_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1854649314_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m4274024367_gp_0_0_0_0_Array_ConvertAll_m4274024367_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m2314439434_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m3151928313_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m2323782676_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1557343438_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1631399507_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3415315332_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m2990936537_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m2009305197_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m2427841765_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3348121441_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m2010744443_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m4126518092_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3127862871_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1523107937_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m2200535596_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m555775493_gp_0_0_0_0,
	&GenInst_Array_FindAll_m2331462890_gp_0_0_0_0,
	&GenInst_Array_Exists_m976807351_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m884706992_gp_0_0_0_0,
	&GenInst_Array_Find_m1125132281_gp_0_0_0_0,
	&GenInst_Array_FindLast_m4092932075_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t3582267753_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t3367196019_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1667190089_gp_0_0_0_0,
	&GenInst_IList_1_t3737699284_gp_0_0_0_0,
	&GenInst_ICollection_1_t1552160836_gp_0_0_0_0,
	&GenInst_Nullable_1_t1398937014_gp_0_0_0_0,
	&GenInst_Comparer_1_t1036860714_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3074655092_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1787398723_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m3006662979_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m1628523394_gp_0_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_ShimEnumerator_t3895203923_gp_0_0_0_0_ShimEnumerator_t3895203923_gp_1_0_0_0,
	&GenInst_Enumerator_t2089681430_gp_0_0_0_0_Enumerator_t2089681430_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3434615342_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0_Enumerator_t83320710_gp_1_0_0_0,
	&GenInst_Enumerator_t83320710_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_1_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_KeyCollection_t1229212677_gp_0_0_0_0_KeyCollection_t1229212677_gp_0_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_0_0_0_0_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_Enumerator_t3111723616_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_0_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_ValueCollection_t2262344653_gp_1_0_0_0_ValueCollection_t2262344653_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3048875398_0_0_0_DictionaryEntry_t3048875398_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_0_0_0_0_Dictionary_2_t2276497324_gp_1_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_KeyValuePair_2_t3180694294_0_0_0_KeyValuePair_2_t3180694294_0_0_0,
	&GenInst_Dictionary_2_t2276497324_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2066709010_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1766400012_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2202941003_gp_0_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t4174120762_0_0_0,
	&GenInst_IDictionary_2_t3502329323_gp_0_0_0_0_IDictionary_2_t3502329323_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1988958766_gp_0_0_0_0_KeyValuePair_2_t1988958766_gp_1_0_0_0,
	&GenInst_List_1_t1169184319_gp_0_0_0_0,
	&GenInst_Enumerator_t1292967705_gp_0_0_0_0,
	&GenInst_Collection_1_t686054069_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t3540981679_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m609993266_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m609993266_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m781526467_gp_0_0_0_0,
	&GenInst_ArraySegment_1_t1001032761_gp_0_0_0_0,
	&GenInst_LinkedList_1_t3556217344_gp_0_0_0_0,
	&GenInst_Enumerator_t4145643798_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t2172356692_gp_0_0_0_0,
	&GenInst_Queue_1_t1458930734_gp_0_0_0_0,
	&GenInst_Enumerator_t4000919638_gp_0_0_0_0,
	&GenInst_RBTree_Intern_m1863612280_gp_0_0_0_0,
	&GenInst_RBTree_Remove_m2489856008_gp_0_0_0_0,
	&GenInst_RBTree_Lookup_m3821366094_gp_0_0_0_0,
	&GenInst_RBTree_find_key_m3679421040_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_0_0_0_0_SortedDictionary_2_t3426860295_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4073929546_0_0_0,
	&GenInst_Node_t1473068371_gp_0_0_0_0_Node_t1473068371_gp_1_0_0_0,
	&GenInst_NodeHelper_t357096749_gp_0_0_0_0,
	&GenInst_NodeHelper_t357096749_gp_0_0_0_0_NodeHelper_t357096749_gp_1_0_0_0,
	&GenInst_ValueCollection_t2735575576_gp_0_0_0_0_ValueCollection_t2735575576_gp_1_0_0_0,
	&GenInst_ValueCollection_t2735575576_gp_1_0_0_0,
	&GenInst_Enumerator_t1353355221_gp_0_0_0_0_Enumerator_t1353355221_gp_1_0_0_0,
	&GenInst_Enumerator_t1353355221_gp_1_0_0_0,
	&GenInst_KeyCollection_t3620397270_gp_0_0_0_0_KeyCollection_t3620397270_gp_1_0_0_0,
	&GenInst_KeyCollection_t3620397270_gp_0_0_0_0,
	&GenInst_Enumerator_t4275149413_gp_0_0_0_0_Enumerator_t4275149413_gp_1_0_0_0,
	&GenInst_Enumerator_t4275149413_gp_0_0_0_0,
	&GenInst_Enumerator_t824916987_gp_0_0_0_0_Enumerator_t824916987_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t404405498_0_0_0,
	&GenInst_SortedDictionary_2_t3426860295_gp_1_0_0_0,
	&GenInst_Stack_1_t4016656541_gp_0_0_0_0,
	&GenInst_Enumerator_t546412149_gp_0_0_0_0,
	&GenInst_HashSet_1_t2624254809_gp_0_0_0_0,
	&GenInst_Enumerator_t2109956843_gp_0_0_0_0,
	&GenInst_PrimeHelper_t3424417428_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m1395534165_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m1886513113_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m1629730982_gp_0_0_0_0,
	&GenInst_Enumerable_Count_m2109803174_gp_0_0_0_0,
	&GenInst_Enumerable_First_m385027450_gp_0_0_0_0,
	&GenInst_Enumerable_First_m385027450_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_First_m2440166523_gp_0_0_0_0,
	&GenInst_Enumerable_First_m3310377166_gp_0_0_0_0,
	&GenInst_Enumerable_First_m3310377166_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m3479775130_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_Last_m1051445487_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m1178781222_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m1178781222_gp_0_0_0_0_Enumerable_OrderBy_m1178781222_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m1935324147_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m1935324147_gp_0_0_0_0_Enumerable_OrderBy_m1935324147_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m1935324147_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m791198632_gp_0_0_0_0,
	&GenInst_Enumerable_Select_m791198632_gp_0_0_0_0_Enumerable_Select_m791198632_gp_1_0_0_0,
	&GenInst_Enumerable_Select_m791198632_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_0_0_0_0_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0,
	&GenInst_Enumerable_CreateSelectIterator_m3979771306_gp_1_0_0_0,
	&GenInst_Enumerable_Single_m2573711234_gp_0_0_0_0,
	&GenInst_Enumerable_Single_m2573711234_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0,
	&GenInst_Enumerable_SingleOrDefault_m647575666_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_ThenBy_m1431519242_gp_0_0_0_0,
	&GenInst_Enumerable_ThenBy_m1431519242_gp_0_0_0_0_Enumerable_ThenBy_m1431519242_gp_1_0_0_0,
	&GenInst_Enumerable_ThenBy_m3860233309_gp_0_0_0_0,
	&GenInst_Enumerable_ThenBy_m3860233309_gp_0_0_0_0_Enumerable_ThenBy_m3860233309_gp_1_0_0_0,
	&GenInst_Enumerable_ThenBy_m3860233309_gp_1_0_0_0,
	&GenInst_Enumerable_ToArray_m747609077_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m2644368746_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m1602106588_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3985242862_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0,
	&GenInst_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_0_0_0_0_U3CCreateSelectIteratorU3Ec__Iterator10_2_t2434383396_gp_1_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2523986802_gp_0_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0,
	&GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m1989554272_gp_0_0_0_0,
	&GenInst_IOrderedEnumerable_1_t641749975_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t753306046_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m3295759113_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_0_0_0_0_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_OrderedSequence_2_t1023848160_gp_1_0_0_0,
	&GenInst_QuickSort_1_t1290221672_gp_0_0_0_0,
	&GenInst_U3CSortU3Ec__Iterator21_t163525460_gp_0_0_0_0,
	&GenInst_SortContext_1_t4088581714_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_0_0_0_0_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_SortSequenceContext_2_t3419387730_gp_1_0_0_0,
	&GenInst_Component_GetComponentInChildren_m2766208945_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3431627251_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2508569808_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1332352624_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3637577495_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m1487060311_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m1502796979_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m2277214384_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m2459662209_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2891535275_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m2576466756_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m2457452615_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m4152119927_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m4002341710_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3461755521_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m3202302135_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m311612863_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m2665790055_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t476640868_gp_0_0_0_0,
	&GenInst_UnityAction_1_t2490859068_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_UnityAction_2_t601835599_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2042724809_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0_InvokableCall_3_t3608808750_gp_1_0_0_0_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_UnityAction_3_t155920421_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3608808750_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0_InvokableCall_4_t879925395_gp_1_0_0_0_InvokableCall_4_t879925395_gp_2_0_0_0_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t879925395_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t224769006_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t4075366602_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t4075366599_gp_0_0_0_0_UnityEvent_2_t4075366599_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t4075366600_gp_0_0_0_0_UnityEvent_3_t4075366600_gp_1_0_0_0_UnityEvent_3_t4075366600_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t4075366597_gp_0_0_0_0_UnityEvent_4_t4075366597_gp_1_0_0_0_UnityEvent_4_t4075366597_gp_2_0_0_0_UnityEvent_4_t4075366597_gp_3_0_0_0,
	&GenInst_Runtime_GetNSObject_m3692132737_gp_0_0_0_0,
	&GenInst_AWSConfigs_GetConfigEnum_m3032820163_gp_0_0_0_0,
	&GenInst_AWSConfigs_ParseEnum_m3861189188_gp_0_0_0_0,
	&GenInst_AWSConfigs_GetSection_m1660313627_gp_0_0_0_0,
	&GenInst_AWSSDKUtils_InvokeInBackground_m573940766_gp_0_0_0_0,
	&GenInst_U3CU3Ec__DisplayClass39_0_1_t500610588_gp_0_0_0_0,
	&GenInst_AndroidInterop_CallMethod_m713902838_gp_0_0_0_0,
	&GenInst_AndroidInterop_GetJavaField_m1179772153_gp_0_0_0_0,
	&GenInst_U3CU3Ec__2_1_t1696963520_gp_0_0_0_0,
	&GenInst_U3CU3Ec__6_1_t2261613524_gp_0_0_0_0,
	&GenInst_ServiceFactory_GetService_m2302760766_gp_0_0_0_0,
	&GenInst_IHttpRequestFactory_1_t4193720315_gp_0_0_0_0,
	&GenInst_AmazonServiceCallback_2_t3530050132_gp_0_0_0_0_AmazonServiceCallback_2_t3530050132_gp_1_0_0_0,
	&GenInst_AmazonServiceResult_2_t529501990_gp_0_0_0_0_AmazonServiceResult_2_t529501990_gp_1_0_0_0,
	&GenInst_ExceptionHandler_1_t3179100912_gp_0_0_0_0,
	&GenInst_HttpHandler_1_t1383532101_gp_0_0_0_0,
	&GenInst_DefaultRetryPolicy_IsInnerException_m149306988_gp_0_0_0_0,
	&GenInst_BackgroundDispatcher_1_t295417742_gp_0_0_0_0,
	&GenInst_HashStream_1_t458673105_gp_0_0_0_0,
	&GenInst_LruCache_2_t2774710777_gp_0_0_0_0_LruListItem_2_t3884836584_0_0_0,
	&GenInst_LruCache_2_t2774710777_gp_0_0_0_0_LruCache_2_t2774710777_gp_1_0_0_0,
	&GenInst_LruList_2_t2819118215_gp_0_0_0_0_LruList_2_t2819118215_gp_1_0_0_0,
	&GenInst_LruListItem_2_t4061194576_gp_0_0_0_0_LruListItem_2_t4061194576_gp_1_0_0_0,
	&GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0,
	&GenInst_ThreadPoolOptions_1_t170443295_0_0_0,
	&GenInst_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0_ThreadPoolThrottler_1_t1982498819_gp_0_0_0_0,
	&GenInst_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0,
	&GenInst_Exception_t1927440687_0_0_0_ThreadPoolOptions_1_t2545192696_gp_1_0_0_0,
	&GenInst_IResponseUnmarshaller_2_t2708327486_gp_0_0_0_0_IResponseUnmarshaller_2_t2708327486_gp_1_0_0_0,
	&GenInst_UnmarshallerExtensions_Add_m421616549_gp_0_0_0_0_UnmarshallerExtensions_Add_m421616549_gp_1_0_0_0,
	&GenInst_ExecuteEvents_Execute_m1744114673_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m3091757213_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m2263439269_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m3190794379_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m3792320302_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2584777480_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m41832880_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m1212286169_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t573160278_gp_0_0_0_0_Int32_t2071877448_0_0_0,
	&GenInst_ListPool_1_t1984115411_gp_0_0_0_0,
	&GenInst_List_1_t2000868992_0_0_0,
	&GenInst_ObjectPool_1_t4265859154_gp_0_0_0_0,
	&GenInst_FastAction_1_t1338576437_gp_0_0_0_0,
	&GenInst_Action_1_t1788008268_0_0_0,
	&GenInst_Action_1_t1788008268_0_0_0_LinkedListNode_1_t684114181_0_0_0,
	&GenInst_FastAction_2_t1741860964_gp_0_0_0_0_FastAction_2_t1741860964_gp_1_0_0_0,
	&GenInst_Action_2_t3375677134_0_0_0,
	&GenInst_Action_2_t3375677134_0_0_0_LinkedListNode_1_t2271783047_0_0_0,
	&GenInst_FastAction_3_t175777023_gp_0_0_0_0_FastAction_3_t175777023_gp_1_0_0_0_FastAction_3_t175777023_gp_2_0_0_0,
	&GenInst_Action_3_t3712260761_0_0_0,
	&GenInst_Action_3_t3712260761_0_0_0_LinkedListNode_1_t2608366674_0_0_0,
	&GenInst_TMPro_ExtensionMethods_FindInstanceID_m238029140_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t579810420_gp_0_0_0_0,
	&GenInst_TMP_Dropdown_GetOrAddComponent_m214373678_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetEquatableStruct_m1986669591_gp_0_0_0_0,
	&GenInst_TMP_ListPool_1_t700915143_gp_0_0_0_0,
	&GenInst_List_1_t717668724_0_0_0,
	&GenInst_TMP_ObjectPool_1_t3742925414_gp_0_0_0_0,
	&GenInst_TMP_Text_ResizeInternalArray_m2848200466_gp_0_0_0_0,
	&GenInst_TMP_TextInfo_Resize_m2071664037_gp_0_0_0_0,
	&GenInst_TMP_TextInfo_Resize_m3363474848_gp_0_0_0_0,
	&GenInst_TMP_XmlTagStack_1_t558189074_gp_0_0_0_0,
	&GenInst_DefaultExecutionOrder_t2717914595_0_0_0,
	&GenInst_PlayerConnection_t3517219175_0_0_0,
	&GenInst_GUILayer_t3254902478_0_0_0,
	&GenInst_LoggingOptions_t2865640765_0_0_0,
	&GenInst_ResponseLoggingOption_t3443611737_0_0_0,
	&GenInst_ThreadAbortException_t1150575753_0_0_0,
	&GenInst_WebException_t3368933679_0_0_0,
	&GenInst_List_1_t1398341365_0_0_0,
	&GenInst_INetworkReachability_t2670889314_0_0_0,
	&GenInst_UnityInitializer_t2778189483_0_0_0,
	&GenInst_UnityMainThreadDispatcher_t4098072663_0_0_0,
	&GenInst_NSDictionary_t3598984691_0_0_0,
	&GenInst_IEnvironmentInfo_t2314249358_0_0_0,
	&GenInst_AWSSection_t3096528870_0_0_0,
	&GenInst_NSBundle_t3599697655_0_0_0,
	&GenInst_NSObject_t1518098886_0_0_0,
	&GenInst_NSLocale_t2224424797_0_0_0,
	&GenInst_UIDevice_t860038178_0_0_0,
	&GenInst_AxisEventData_t1524870173_0_0_0,
	&GenInst_SpriteRenderer_t1209076198_0_0_0,
	&GenInst_Image_t2042527209_0_0_0,
	&GenInst_Button_t2872111280_0_0_0,
	&GenInst_RawImage_t2749640213_0_0_0,
	&GenInst_Slider_t297367283_0_0_0,
	&GenInst_Scrollbar_t3248359358_0_0_0,
	&GenInst_InputField_t1631627530_0_0_0,
	&GenInst_ScrollRect_t1199013257_0_0_0,
	&GenInst_Dropdown_t1985816271_0_0_0,
	&GenInst_GraphicRaycaster_t410733016_0_0_0,
	&GenInst_CanvasRenderer_t261436805_0_0_0,
	&GenInst_Corner_t1077473318_0_0_0,
	&GenInst_Axis_t1431825778_0_0_0,
	&GenInst_Constraint_t3558160636_0_0_0,
	&GenInst_SubmitEvent_t907918422_0_0_0,
	&GenInst_OnChangeEvent_t2863344003_0_0_0,
	&GenInst_OnValidateInput_t1946318473_0_0_0,
	&GenInst_LayoutElement_t2808691390_0_0_0,
	&GenInst_RectOffset_t3387826427_0_0_0,
	&GenInst_TextAnchor_t112990806_0_0_0,
	&GenInst_AnimationTriggers_t3244928895_0_0_0,
	&GenInst_Animator_t69676727_0_0_0,
	&GenInst_Marshaller_t3371889241_0_0_0,
	&GenInst_EndpointResolver_t2369326703_0_0_0,
	&GenInst_Unmarshaller_t1994457618_0_0_0,
	&GenInst_ErrorCallbackHandler_t394665619_0_0_0,
	&GenInst_RetryHandler_t3257318146_0_0_0,
	&GenInst_GetObjectRequest_t109865576_0_0_0,
	&GenInst_ListObjectsRequest_t3715455147_0_0_0,
	&GenInst_ReplicationStatus_t1991002226_0_0_0,
	&GenInst_RequestCharged_t2438105727_0_0_0,
	&GenInst_S3Region_t723338532_0_0_0,
	&GenInst_S3StorageClass_t454477475_0_0_0,
	&GenInst_ServerSideEncryptionCustomerMethod_t3201425490_0_0_0,
	&GenInst_ServerSideEncryptionMethod_t608782770_0_0_0,
	&GenInst_V4ClientSection_t2149061698_0_0_0,
	&GenInst_TextMeshPro_t2521834357_0_0_0,
	&GenInst_Renderer_t257310565_0_0_0,
	&GenInst_MeshFilter_t3026937449_0_0_0,
	&GenInst_TMP_InputField_t1778301588_0_0_0,
	&GenInst_TextMeshProUGUI_t934157183_0_0_0,
	&GenInst_TMP_Dropdown_t1768193147_0_0_0,
	&GenInst_SubmitEvent_t3359162065_0_0_0,
	&GenInst_SelectionEvent_t3883897865_0_0_0,
	&GenInst_TextSelectionEvent_t3887183206_0_0_0,
	&GenInst_OnChangeEvent_t2139251414_0_0_0,
	&GenInst_OnValidateInput_t3285190392_0_0_0,
	&GenInst_LineType_t2475898675_0_0_0,
	&GenInst_InputType_t379073331_0_0_0,
	&GenInst_CharacterValidation_t487603955_0_0_0,
	&GenInst_TMP_InputValidator_t3726817866_0_0_0,
	&GenInst_TMP_SelectionCaret_t3802037885_0_0_0,
	&GenInst_BoxCollider_t22920061_0_0_0,
	&GenInst_Rigidbody_t4233889191_0_0_0,
	&GenInst_TMP_SpriteAnimator_t2347923044_0_0_0,
	&GenInst_EntryLink_t1717819552_0_0_0,
	&GenInst_PathCreator_t1368626459_0_0_0,
	&GenInst_RoadCreator_t1201856826_0_0_0,
	&GenInst_MeshRenderer_t1268241104_0_0_0,
	&GenInst_RacingWidgets_t915614521_0_0_0,
	&GenInst_TextMesh_t1641806576_0_0_0,
	&GenInst_TextMeshProFloatingText_t6181308_0_0_0,
	&GenInst_Light_t494725636_0_0_0,
	&GenInst_TextContainer_t4263764796_0_0_0,
	&GenInst_Byte_t3683104436_0_0_0_Byte_t3683104436_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_Char_t3454481338_0_0_0,
	&GenInst_KeyValuePair_2_t38854645_0_0_0_KeyValuePair_2_t38854645_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t94157543_0_0_0_CustomAttributeNamedArgument_t94157543_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1498197914_0_0_0_CustomAttributeTypedArgument_t1498197914_0_0_0,
	&GenInst_Single_t2076509932_0_0_0_Single_t2076509932_0_0_0,
	&GenInst_PropertyMetadata_t3287739986_0_0_0_PropertyMetadata_t3287739986_0_0_0,
	&GenInst_SpriteData_t257854902_0_0_0_SpriteData_t257854902_0_0_0,
	&GenInst_Color32_t874517518_0_0_0_Color32_t874517518_0_0_0,
	&GenInst_RaycastResult_t21186376_0_0_0_RaycastResult_t21186376_0_0_0,
	&GenInst_UICharInfo_t3056636800_0_0_0_UICharInfo_t3056636800_0_0_0,
	&GenInst_UILineInfo_t3621277874_0_0_0_UILineInfo_t3621277874_0_0_0,
	&GenInst_UIVertex_t1204258818_0_0_0_UIVertex_t1204258818_0_0_0,
	&GenInst_Vector2_t2243707579_0_0_0_Vector2_t2243707579_0_0_0,
	&GenInst_Vector3_t2243707580_0_0_0_Vector3_t2243707580_0_0_0,
	&GenInst_Vector4_t2243707581_0_0_0_Vector4_t2243707581_0_0_0,
	&GenInst_Metric_t3273440202_0_0_0_Metric_t3273440202_0_0_0,
	&GenInst_KeyValuePair_2_t1416501748_0_0_0_KeyValuePair_2_t1416501748_0_0_0,
	&GenInst_KeyValuePair_2_t1416501748_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int64_t909078037_0_0_0_Int64_t909078037_0_0_0,
	&GenInst_KeyValuePair_2_t3196873006_0_0_0_KeyValuePair_2_t3196873006_0_0_0,
	&GenInst_KeyValuePair_2_t3196873006_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Boolean_t3825574718_0_0_0_Boolean_t3825574718_0_0_0,
	&GenInst_KeyValuePair_2_t590745575_0_0_0_KeyValuePair_2_t590745575_0_0_0,
	&GenInst_KeyValuePair_2_t590745575_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Char_t3454481338_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t219652195_0_0_0_KeyValuePair_2_t219652195_0_0_0,
	&GenInst_KeyValuePair_2_t219652195_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_KeyValuePair_2_t3132015601_0_0_0,
	&GenInst_KeyValuePair_2_t3132015601_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1969216190_0_0_0_KeyValuePair_2_t1969216190_0_0_0,
	&GenInst_KeyValuePair_2_t1969216190_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_KeyValuePair_2_t3749587448_0_0_0,
	&GenInst_KeyValuePair_2_t3749587448_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1436312919_0_0_0_KeyValuePair_2_t1436312919_0_0_0,
	&GenInst_KeyValuePair_2_t1436312919_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_KeyValuePair_2_t888819835_0_0_0,
	&GenInst_KeyValuePair_2_t888819835_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0,
	&GenInst_InstantiationModel_t1632807378_0_0_0_InstantiationModel_t1632807378_0_0_0,
	&GenInst_InstantiationModel_t1632807378_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3277180024_0_0_0_KeyValuePair_2_t3277180024_0_0_0,
	&GenInst_KeyValuePair_2_t3277180024_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_KeyValuePair_2_t1174980068_0_0_0,
	&GenInst_KeyValuePair_2_t1174980068_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_KeyValuePair_2_t3716250094_0_0_0,
	&GenInst_KeyValuePair_2_t3716250094_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t779664299_0_0_0_KeyValuePair_2_t779664299_0_0_0,
	&GenInst_KeyValuePair_2_t779664299_0_0_0_RuntimeObject_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0_RuntimeObject_0_0_0,
	&GenInst_TimeSpan_t3430258949_0_0_0_TimeSpan_t3430258949_0_0_0,
	&GenInst_KeyValuePair_2_t2779450660_0_0_0_KeyValuePair_2_t2779450660_0_0_0,
	&GenInst_KeyValuePair_2_t2779450660_0_0_0_RuntimeObject_0_0_0,
	&GenInst_ArrayMetadata_t1135078014_0_0_0_RuntimeObject_0_0_0,
	&GenInst_ArrayMetadata_t1135078014_0_0_0_ArrayMetadata_t1135078014_0_0_0,
	&GenInst_KeyValuePair_2_t1407543090_0_0_0_KeyValuePair_2_t1407543090_0_0_0,
	&GenInst_KeyValuePair_2_t1407543090_0_0_0_RuntimeObject_0_0_0,
	&GenInst_ObjectMetadata_t4058137740_0_0_0_RuntimeObject_0_0_0,
	&GenInst_ObjectMetadata_t4058137740_0_0_0_ObjectMetadata_t4058137740_0_0_0,
	&GenInst_KeyValuePair_2_t637145336_0_0_0_KeyValuePair_2_t637145336_0_0_0,
	&GenInst_KeyValuePair_2_t637145336_0_0_0_RuntimeObject_0_0_0,
	&GenInst_PropertyMetadata_t3287739986_0_0_0_RuntimeObject_0_0_0,
	&GenInst_LoggingSection_t905443946_0_0_0,
};
