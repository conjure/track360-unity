﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Collections.Generic.Dictionary`2<System.String,ThirdParty.Json.LitJson.JsonData>
struct Dictionary_2_t1883064018;
// System.Collections.Generic.Dictionary`2<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint>
struct Dictionary_2_t3800020711;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint>
struct Dictionary_2_t2576302067;
// Amazon.Internal.IRegionEndpointProvider
struct IRegionEndpointProvider_t2524767261;
// Amazon.Internal.RegionEndpointV3/ServiceMap
struct ServiceMap_t3203095671;
// ThirdParty.Json.LitJson.JsonData
struct JsonData_t4263252052;
// System.Func`2<System.String,System.String>
struct Func_2_t193026957;
// Amazon.Util.CryptoUtilFactory/CryptoUtil
struct CryptoUtil_t1025015063;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;
// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint/Endpoint>
struct Dictionary_2_t968504607;
// System.Collections.Generic.Dictionary`2<System.String,Amazon.Internal.IRegionEndpoint>
struct Dictionary_2_t2459213150;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// ThirdParty.Json.LitJson.Lexer/StateHandler[]
struct StateHandlerU5BU5D_t1374508575;
// ThirdParty.Json.LitJson.FsmContext
struct FsmContext_t4275593467;
// System.IO.TextReader
struct TextReader_t1561828458;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.UInt32[]
struct UInt32U5BU5D_t59386216;
// ThirdParty.Json.LitJson.ExporterFunc
struct ExporterFunc_t173265409;
// ThirdParty.Json.LitJson.ImporterFunc
struct ImporterFunc_t850687278;
// ThirdParty.Json.LitJson.WrapperFactory
struct WrapperFactory_t327905379;
// ThirdParty.Json.LitJson.Lexer
struct Lexer_t954714164;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t104580544;
// ThirdParty.Json.LitJson.WriterContext
struct WriterContext_t1209007092;
// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext>
struct Stack_1_t2296735246;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.IO.TextWriter
struct TextWriter_t4027217640;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t926074657;
// Amazon.AWSConfigs/<>c__DisplayClass94_0
struct U3CU3Ec__DisplayClass94_0_t1056790044;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.IFormatProvider
struct IFormatProvider_t2849799027;
// System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc>
struct IDictionary_2_t109706727;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>>
struct IDictionary_2_t723569914;
// System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ArrayMetadata>
struct IDictionary_2_t1071519332;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>
struct IDictionary_2_t3203428973;
// System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ObjectMetadata>
struct IDictionary_2_t3994579058;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<ThirdParty.Json.LitJson.PropertyMetadata>>
struct IDictionary_2_t3765121905;
// ThirdParty.Json.LitJson.JsonWriter
struct JsonWriter_t3014444111;
// UnityEngine.TextAsset
struct TextAsset_t3973159845;
// System.Xml.Linq.XDocument
struct XDocument_t2733326047;
// System.Action
struct Action_t3226471752;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>
struct IEnumerator_1_t1410900363;
// ThirdParty.iOS4Unity.Selector
struct Selector_t1939762325;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.IntPtr,System.Object>>
struct Dictionary_2_t1317859972;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object>
struct Dictionary_2_t3131474613;
// System.Collections.Generic.Dictionary`2<System.String,System.Delegate>
struct Dictionary_2_t642288257;
// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>>
struct Dictionary_2_t2071948110;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// Amazon.Runtime.Internal.Util.Logger
struct Logger_t2262497814;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t2624936259;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.IO.Stream
struct Stream_t3255436806;
// ThirdParty.Ionic.Zlib.CRC32
struct CRC32_t619824607;
// System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.PropertyMetadata>
struct IDictionary_2_t3201602669;
// System.Void
struct Void_t1841601450;
// Amazon.LoggingSection
struct LoggingSection_t905443946;
// Amazon.ProxySection
struct ProxySection_t4280332351;
// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement>
struct IDictionary_2_t467683733;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t15112628;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken>
struct Stack_1_t3533309409;
// System.Collections.Generic.IList`1<ThirdParty.Json.LitJson.JsonData>
struct IList_1_t509225357;
// System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.JsonData>
struct IDictionary_2_t4177114735;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>>
struct IList_1_t181349841;
// System.Func`1<System.DateTime>
struct Func_1_t2647598351;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Amazon.Util.Internal.RootConfig
struct RootConfig_t4046630774;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t3042952059;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>>
struct Dictionary_2_t403882377;
// Amazon.Runtime.IMetricsFormatter
struct IMetricsFormatter_t495884108;
// ThirdParty.Json.LitJson.IJsonWrapper
struct IJsonWrapper_t3095378610;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t1037045868;
// Amazon.Runtime.Internal.Util.BackgroundInvoker
struct BackgroundInvoker_t1722929158;
// System.Threading.Thread
struct Thread_t241561612;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T3783534235_H
#define U3CMODULEU3E_T3783534235_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534235 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534235_H
#ifndef U3CMODULEU3E_T3783534234_H
#define U3CMODULEU3E_T3783534234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534234 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534234_H
#ifndef REGIONENDPOINT_T1885241449_H
#define REGIONENDPOINT_T1885241449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint
struct  RegionEndpoint_t1885241449  : public RuntimeObject
{
public:
	// System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::<SystemName>k__BackingField
	String_t* ___U3CSystemNameU3Ek__BackingField_4;
	// System.String Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CSystemNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449, ___U3CSystemNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CSystemNameU3Ek__BackingField_4() const { return ___U3CSystemNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CSystemNameU3Ek__BackingField_4() { return &___U3CSystemNameU3Ek__BackingField_4; }
	inline void set_U3CSystemNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CSystemNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSystemNameU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449, ___U3CDisplayNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_5() const { return ___U3CDisplayNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_5() { return &___U3CDisplayNameU3Ek__BackingField_5; }
	inline void set_U3CDisplayNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_5), value);
	}
};

struct RegionEndpoint_t1885241449_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,ThirdParty.Json.LitJson.JsonData> Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::_documentEndpoints
	Dictionary_2_t1883064018 * ____documentEndpoints_0;
	// System.Boolean Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::loaded
	bool ___loaded_1;
	// System.Object Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::LOCK_OBJECT
	RuntimeObject * ___LOCK_OBJECT_2;
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint> Amazon.Internal.RegionEndpointProviderV2/RegionEndpoint::hashBySystemName
	Dictionary_2_t3800020711 * ___hashBySystemName_3;

public:
	inline static int32_t get_offset_of__documentEndpoints_0() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449_StaticFields, ____documentEndpoints_0)); }
	inline Dictionary_2_t1883064018 * get__documentEndpoints_0() const { return ____documentEndpoints_0; }
	inline Dictionary_2_t1883064018 ** get_address_of__documentEndpoints_0() { return &____documentEndpoints_0; }
	inline void set__documentEndpoints_0(Dictionary_2_t1883064018 * value)
	{
		____documentEndpoints_0 = value;
		Il2CppCodeGenWriteBarrier((&____documentEndpoints_0), value);
	}

	inline static int32_t get_offset_of_loaded_1() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449_StaticFields, ___loaded_1)); }
	inline bool get_loaded_1() const { return ___loaded_1; }
	inline bool* get_address_of_loaded_1() { return &___loaded_1; }
	inline void set_loaded_1(bool value)
	{
		___loaded_1 = value;
	}

	inline static int32_t get_offset_of_LOCK_OBJECT_2() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449_StaticFields, ___LOCK_OBJECT_2)); }
	inline RuntimeObject * get_LOCK_OBJECT_2() const { return ___LOCK_OBJECT_2; }
	inline RuntimeObject ** get_address_of_LOCK_OBJECT_2() { return &___LOCK_OBJECT_2; }
	inline void set_LOCK_OBJECT_2(RuntimeObject * value)
	{
		___LOCK_OBJECT_2 = value;
		Il2CppCodeGenWriteBarrier((&___LOCK_OBJECT_2), value);
	}

	inline static int32_t get_offset_of_hashBySystemName_3() { return static_cast<int32_t>(offsetof(RegionEndpoint_t1885241449_StaticFields, ___hashBySystemName_3)); }
	inline Dictionary_2_t3800020711 * get_hashBySystemName_3() const { return ___hashBySystemName_3; }
	inline Dictionary_2_t3800020711 ** get_address_of_hashBySystemName_3() { return &___hashBySystemName_3; }
	inline void set_hashBySystemName_3(Dictionary_2_t3800020711 * value)
	{
		___hashBySystemName_3 = value;
		Il2CppCodeGenWriteBarrier((&___hashBySystemName_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONENDPOINT_T1885241449_H
#ifndef REGIONENDPOINTPROVIDERV2_T1909011008_H
#define REGIONENDPOINTPROVIDERV2_T1909011008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Internal.RegionEndpointProviderV2
struct  RegionEndpointProviderV2_t1909011008  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONENDPOINTPROVIDERV2_T1909011008_H
#ifndef PROXYSECTION_T4280332351_H
#define PROXYSECTION_T4280332351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.ProxySection
struct  ProxySection_t4280332351  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXYSECTION_T4280332351_H
#ifndef REGIONENDPOINT_T661522805_H
#define REGIONENDPOINT_T661522805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.RegionEndpoint
struct  RegionEndpoint_t661522805  : public RuntimeObject
{
public:
	// System.String Amazon.RegionEndpoint::<SystemName>k__BackingField
	String_t* ___U3CSystemNameU3Ek__BackingField_20;
	// System.String Amazon.RegionEndpoint::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CSystemNameU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805, ___U3CSystemNameU3Ek__BackingField_20)); }
	inline String_t* get_U3CSystemNameU3Ek__BackingField_20() const { return ___U3CSystemNameU3Ek__BackingField_20; }
	inline String_t** get_address_of_U3CSystemNameU3Ek__BackingField_20() { return &___U3CSystemNameU3Ek__BackingField_20; }
	inline void set_U3CSystemNameU3Ek__BackingField_20(String_t* value)
	{
		___U3CSystemNameU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSystemNameU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805, ___U3CDisplayNameU3Ek__BackingField_21)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_21() const { return ___U3CDisplayNameU3Ek__BackingField_21; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_21() { return &___U3CDisplayNameU3Ek__BackingField_21; }
	inline void set_U3CDisplayNameU3Ek__BackingField_21(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_21), value);
	}
};

struct RegionEndpoint_t661522805_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint> Amazon.RegionEndpoint::_hashBySystemName
	Dictionary_2_t2576302067 * ____hashBySystemName_0;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USEast1
	RegionEndpoint_t661522805 * ___USEast1_1;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USEast2
	RegionEndpoint_t661522805 * ___USEast2_2;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USWest1
	RegionEndpoint_t661522805 * ___USWest1_3;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USWest2
	RegionEndpoint_t661522805 * ___USWest2_4;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUWest1
	RegionEndpoint_t661522805 * ___EUWest1_5;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUWest2
	RegionEndpoint_t661522805 * ___EUWest2_6;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUWest3
	RegionEndpoint_t661522805 * ___EUWest3_7;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::EUCentral1
	RegionEndpoint_t661522805 * ___EUCentral1_8;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APNortheast1
	RegionEndpoint_t661522805 * ___APNortheast1_9;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APNortheast2
	RegionEndpoint_t661522805 * ___APNortheast2_10;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APSouth1
	RegionEndpoint_t661522805 * ___APSouth1_11;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APSoutheast1
	RegionEndpoint_t661522805 * ___APSoutheast1_12;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::APSoutheast2
	RegionEndpoint_t661522805 * ___APSoutheast2_13;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::SAEast1
	RegionEndpoint_t661522805 * ___SAEast1_14;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::USGovCloudWest1
	RegionEndpoint_t661522805 * ___USGovCloudWest1_15;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::CNNorth1
	RegionEndpoint_t661522805 * ___CNNorth1_16;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::CNNorthWest1
	RegionEndpoint_t661522805 * ___CNNorthWest1_17;
	// Amazon.RegionEndpoint Amazon.RegionEndpoint::CACentral1
	RegionEndpoint_t661522805 * ___CACentral1_18;
	// Amazon.Internal.IRegionEndpointProvider Amazon.RegionEndpoint::_regionEndpointProvider
	RuntimeObject* ____regionEndpointProvider_19;

public:
	inline static int32_t get_offset_of__hashBySystemName_0() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ____hashBySystemName_0)); }
	inline Dictionary_2_t2576302067 * get__hashBySystemName_0() const { return ____hashBySystemName_0; }
	inline Dictionary_2_t2576302067 ** get_address_of__hashBySystemName_0() { return &____hashBySystemName_0; }
	inline void set__hashBySystemName_0(Dictionary_2_t2576302067 * value)
	{
		____hashBySystemName_0 = value;
		Il2CppCodeGenWriteBarrier((&____hashBySystemName_0), value);
	}

	inline static int32_t get_offset_of_USEast1_1() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USEast1_1)); }
	inline RegionEndpoint_t661522805 * get_USEast1_1() const { return ___USEast1_1; }
	inline RegionEndpoint_t661522805 ** get_address_of_USEast1_1() { return &___USEast1_1; }
	inline void set_USEast1_1(RegionEndpoint_t661522805 * value)
	{
		___USEast1_1 = value;
		Il2CppCodeGenWriteBarrier((&___USEast1_1), value);
	}

	inline static int32_t get_offset_of_USEast2_2() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USEast2_2)); }
	inline RegionEndpoint_t661522805 * get_USEast2_2() const { return ___USEast2_2; }
	inline RegionEndpoint_t661522805 ** get_address_of_USEast2_2() { return &___USEast2_2; }
	inline void set_USEast2_2(RegionEndpoint_t661522805 * value)
	{
		___USEast2_2 = value;
		Il2CppCodeGenWriteBarrier((&___USEast2_2), value);
	}

	inline static int32_t get_offset_of_USWest1_3() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USWest1_3)); }
	inline RegionEndpoint_t661522805 * get_USWest1_3() const { return ___USWest1_3; }
	inline RegionEndpoint_t661522805 ** get_address_of_USWest1_3() { return &___USWest1_3; }
	inline void set_USWest1_3(RegionEndpoint_t661522805 * value)
	{
		___USWest1_3 = value;
		Il2CppCodeGenWriteBarrier((&___USWest1_3), value);
	}

	inline static int32_t get_offset_of_USWest2_4() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USWest2_4)); }
	inline RegionEndpoint_t661522805 * get_USWest2_4() const { return ___USWest2_4; }
	inline RegionEndpoint_t661522805 ** get_address_of_USWest2_4() { return &___USWest2_4; }
	inline void set_USWest2_4(RegionEndpoint_t661522805 * value)
	{
		___USWest2_4 = value;
		Il2CppCodeGenWriteBarrier((&___USWest2_4), value);
	}

	inline static int32_t get_offset_of_EUWest1_5() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___EUWest1_5)); }
	inline RegionEndpoint_t661522805 * get_EUWest1_5() const { return ___EUWest1_5; }
	inline RegionEndpoint_t661522805 ** get_address_of_EUWest1_5() { return &___EUWest1_5; }
	inline void set_EUWest1_5(RegionEndpoint_t661522805 * value)
	{
		___EUWest1_5 = value;
		Il2CppCodeGenWriteBarrier((&___EUWest1_5), value);
	}

	inline static int32_t get_offset_of_EUWest2_6() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___EUWest2_6)); }
	inline RegionEndpoint_t661522805 * get_EUWest2_6() const { return ___EUWest2_6; }
	inline RegionEndpoint_t661522805 ** get_address_of_EUWest2_6() { return &___EUWest2_6; }
	inline void set_EUWest2_6(RegionEndpoint_t661522805 * value)
	{
		___EUWest2_6 = value;
		Il2CppCodeGenWriteBarrier((&___EUWest2_6), value);
	}

	inline static int32_t get_offset_of_EUWest3_7() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___EUWest3_7)); }
	inline RegionEndpoint_t661522805 * get_EUWest3_7() const { return ___EUWest3_7; }
	inline RegionEndpoint_t661522805 ** get_address_of_EUWest3_7() { return &___EUWest3_7; }
	inline void set_EUWest3_7(RegionEndpoint_t661522805 * value)
	{
		___EUWest3_7 = value;
		Il2CppCodeGenWriteBarrier((&___EUWest3_7), value);
	}

	inline static int32_t get_offset_of_EUCentral1_8() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___EUCentral1_8)); }
	inline RegionEndpoint_t661522805 * get_EUCentral1_8() const { return ___EUCentral1_8; }
	inline RegionEndpoint_t661522805 ** get_address_of_EUCentral1_8() { return &___EUCentral1_8; }
	inline void set_EUCentral1_8(RegionEndpoint_t661522805 * value)
	{
		___EUCentral1_8 = value;
		Il2CppCodeGenWriteBarrier((&___EUCentral1_8), value);
	}

	inline static int32_t get_offset_of_APNortheast1_9() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APNortheast1_9)); }
	inline RegionEndpoint_t661522805 * get_APNortheast1_9() const { return ___APNortheast1_9; }
	inline RegionEndpoint_t661522805 ** get_address_of_APNortheast1_9() { return &___APNortheast1_9; }
	inline void set_APNortheast1_9(RegionEndpoint_t661522805 * value)
	{
		___APNortheast1_9 = value;
		Il2CppCodeGenWriteBarrier((&___APNortheast1_9), value);
	}

	inline static int32_t get_offset_of_APNortheast2_10() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APNortheast2_10)); }
	inline RegionEndpoint_t661522805 * get_APNortheast2_10() const { return ___APNortheast2_10; }
	inline RegionEndpoint_t661522805 ** get_address_of_APNortheast2_10() { return &___APNortheast2_10; }
	inline void set_APNortheast2_10(RegionEndpoint_t661522805 * value)
	{
		___APNortheast2_10 = value;
		Il2CppCodeGenWriteBarrier((&___APNortheast2_10), value);
	}

	inline static int32_t get_offset_of_APSouth1_11() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APSouth1_11)); }
	inline RegionEndpoint_t661522805 * get_APSouth1_11() const { return ___APSouth1_11; }
	inline RegionEndpoint_t661522805 ** get_address_of_APSouth1_11() { return &___APSouth1_11; }
	inline void set_APSouth1_11(RegionEndpoint_t661522805 * value)
	{
		___APSouth1_11 = value;
		Il2CppCodeGenWriteBarrier((&___APSouth1_11), value);
	}

	inline static int32_t get_offset_of_APSoutheast1_12() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APSoutheast1_12)); }
	inline RegionEndpoint_t661522805 * get_APSoutheast1_12() const { return ___APSoutheast1_12; }
	inline RegionEndpoint_t661522805 ** get_address_of_APSoutheast1_12() { return &___APSoutheast1_12; }
	inline void set_APSoutheast1_12(RegionEndpoint_t661522805 * value)
	{
		___APSoutheast1_12 = value;
		Il2CppCodeGenWriteBarrier((&___APSoutheast1_12), value);
	}

	inline static int32_t get_offset_of_APSoutheast2_13() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___APSoutheast2_13)); }
	inline RegionEndpoint_t661522805 * get_APSoutheast2_13() const { return ___APSoutheast2_13; }
	inline RegionEndpoint_t661522805 ** get_address_of_APSoutheast2_13() { return &___APSoutheast2_13; }
	inline void set_APSoutheast2_13(RegionEndpoint_t661522805 * value)
	{
		___APSoutheast2_13 = value;
		Il2CppCodeGenWriteBarrier((&___APSoutheast2_13), value);
	}

	inline static int32_t get_offset_of_SAEast1_14() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___SAEast1_14)); }
	inline RegionEndpoint_t661522805 * get_SAEast1_14() const { return ___SAEast1_14; }
	inline RegionEndpoint_t661522805 ** get_address_of_SAEast1_14() { return &___SAEast1_14; }
	inline void set_SAEast1_14(RegionEndpoint_t661522805 * value)
	{
		___SAEast1_14 = value;
		Il2CppCodeGenWriteBarrier((&___SAEast1_14), value);
	}

	inline static int32_t get_offset_of_USGovCloudWest1_15() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___USGovCloudWest1_15)); }
	inline RegionEndpoint_t661522805 * get_USGovCloudWest1_15() const { return ___USGovCloudWest1_15; }
	inline RegionEndpoint_t661522805 ** get_address_of_USGovCloudWest1_15() { return &___USGovCloudWest1_15; }
	inline void set_USGovCloudWest1_15(RegionEndpoint_t661522805 * value)
	{
		___USGovCloudWest1_15 = value;
		Il2CppCodeGenWriteBarrier((&___USGovCloudWest1_15), value);
	}

	inline static int32_t get_offset_of_CNNorth1_16() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___CNNorth1_16)); }
	inline RegionEndpoint_t661522805 * get_CNNorth1_16() const { return ___CNNorth1_16; }
	inline RegionEndpoint_t661522805 ** get_address_of_CNNorth1_16() { return &___CNNorth1_16; }
	inline void set_CNNorth1_16(RegionEndpoint_t661522805 * value)
	{
		___CNNorth1_16 = value;
		Il2CppCodeGenWriteBarrier((&___CNNorth1_16), value);
	}

	inline static int32_t get_offset_of_CNNorthWest1_17() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___CNNorthWest1_17)); }
	inline RegionEndpoint_t661522805 * get_CNNorthWest1_17() const { return ___CNNorthWest1_17; }
	inline RegionEndpoint_t661522805 ** get_address_of_CNNorthWest1_17() { return &___CNNorthWest1_17; }
	inline void set_CNNorthWest1_17(RegionEndpoint_t661522805 * value)
	{
		___CNNorthWest1_17 = value;
		Il2CppCodeGenWriteBarrier((&___CNNorthWest1_17), value);
	}

	inline static int32_t get_offset_of_CACentral1_18() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ___CACentral1_18)); }
	inline RegionEndpoint_t661522805 * get_CACentral1_18() const { return ___CACentral1_18; }
	inline RegionEndpoint_t661522805 ** get_address_of_CACentral1_18() { return &___CACentral1_18; }
	inline void set_CACentral1_18(RegionEndpoint_t661522805 * value)
	{
		___CACentral1_18 = value;
		Il2CppCodeGenWriteBarrier((&___CACentral1_18), value);
	}

	inline static int32_t get_offset_of__regionEndpointProvider_19() { return static_cast<int32_t>(offsetof(RegionEndpoint_t661522805_StaticFields, ____regionEndpointProvider_19)); }
	inline RuntimeObject* get__regionEndpointProvider_19() const { return ____regionEndpointProvider_19; }
	inline RuntimeObject** get_address_of__regionEndpointProvider_19() { return &____regionEndpointProvider_19; }
	inline void set__regionEndpointProvider_19(RuntimeObject* value)
	{
		____regionEndpointProvider_19 = value;
		Il2CppCodeGenWriteBarrier((&____regionEndpointProvider_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONENDPOINT_T661522805_H
#ifndef ENDPOINT_T3348692641_H
#define ENDPOINT_T3348692641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.RegionEndpoint/Endpoint
struct  Endpoint_t3348692641  : public RuntimeObject
{
public:
	// System.String Amazon.RegionEndpoint/Endpoint::<Hostname>k__BackingField
	String_t* ___U3CHostnameU3Ek__BackingField_0;
	// System.String Amazon.RegionEndpoint/Endpoint::<AuthRegion>k__BackingField
	String_t* ___U3CAuthRegionU3Ek__BackingField_1;
	// System.String Amazon.RegionEndpoint/Endpoint::<SignatureVersionOverride>k__BackingField
	String_t* ___U3CSignatureVersionOverrideU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CHostnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Endpoint_t3348692641, ___U3CHostnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CHostnameU3Ek__BackingField_0() const { return ___U3CHostnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CHostnameU3Ek__BackingField_0() { return &___U3CHostnameU3Ek__BackingField_0; }
	inline void set_U3CHostnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CHostnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHostnameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAuthRegionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Endpoint_t3348692641, ___U3CAuthRegionU3Ek__BackingField_1)); }
	inline String_t* get_U3CAuthRegionU3Ek__BackingField_1() const { return ___U3CAuthRegionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAuthRegionU3Ek__BackingField_1() { return &___U3CAuthRegionU3Ek__BackingField_1; }
	inline void set_U3CAuthRegionU3Ek__BackingField_1(String_t* value)
	{
		___U3CAuthRegionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthRegionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSignatureVersionOverrideU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Endpoint_t3348692641, ___U3CSignatureVersionOverrideU3Ek__BackingField_2)); }
	inline String_t* get_U3CSignatureVersionOverrideU3Ek__BackingField_2() const { return ___U3CSignatureVersionOverrideU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CSignatureVersionOverrideU3Ek__BackingField_2() { return &___U3CSignatureVersionOverrideU3Ek__BackingField_2; }
	inline void set_U3CSignatureVersionOverrideU3Ek__BackingField_2(String_t* value)
	{
		___U3CSignatureVersionOverrideU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSignatureVersionOverrideU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINT_T3348692641_H
#ifndef REGIONENDPOINTV3_T2255552612_H
#define REGIONENDPOINTV3_T2255552612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Internal.RegionEndpointV3
struct  RegionEndpointV3_t2255552612  : public RuntimeObject
{
public:
	// Amazon.Internal.RegionEndpointV3/ServiceMap Amazon.Internal.RegionEndpointV3::_serviceMap
	ServiceMap_t3203095671 * ____serviceMap_0;
	// System.String Amazon.Internal.RegionEndpointV3::<RegionName>k__BackingField
	String_t* ___U3CRegionNameU3Ek__BackingField_1;
	// System.String Amazon.Internal.RegionEndpointV3::<DisplayName>k__BackingField
	String_t* ___U3CDisplayNameU3Ek__BackingField_2;
	// ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointV3::_partitionJsonData
	JsonData_t4263252052 * ____partitionJsonData_3;
	// ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointV3::_servicesJsonData
	JsonData_t4263252052 * ____servicesJsonData_4;
	// System.Boolean Amazon.Internal.RegionEndpointV3::_servicesLoaded
	bool ____servicesLoaded_5;

public:
	inline static int32_t get_offset_of__serviceMap_0() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ____serviceMap_0)); }
	inline ServiceMap_t3203095671 * get__serviceMap_0() const { return ____serviceMap_0; }
	inline ServiceMap_t3203095671 ** get_address_of__serviceMap_0() { return &____serviceMap_0; }
	inline void set__serviceMap_0(ServiceMap_t3203095671 * value)
	{
		____serviceMap_0 = value;
		Il2CppCodeGenWriteBarrier((&____serviceMap_0), value);
	}

	inline static int32_t get_offset_of_U3CRegionNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ___U3CRegionNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CRegionNameU3Ek__BackingField_1() const { return ___U3CRegionNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CRegionNameU3Ek__BackingField_1() { return &___U3CRegionNameU3Ek__BackingField_1; }
	inline void set_U3CRegionNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CRegionNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRegionNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDisplayNameU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ___U3CDisplayNameU3Ek__BackingField_2)); }
	inline String_t* get_U3CDisplayNameU3Ek__BackingField_2() const { return ___U3CDisplayNameU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CDisplayNameU3Ek__BackingField_2() { return &___U3CDisplayNameU3Ek__BackingField_2; }
	inline void set_U3CDisplayNameU3Ek__BackingField_2(String_t* value)
	{
		___U3CDisplayNameU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDisplayNameU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of__partitionJsonData_3() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ____partitionJsonData_3)); }
	inline JsonData_t4263252052 * get__partitionJsonData_3() const { return ____partitionJsonData_3; }
	inline JsonData_t4263252052 ** get_address_of__partitionJsonData_3() { return &____partitionJsonData_3; }
	inline void set__partitionJsonData_3(JsonData_t4263252052 * value)
	{
		____partitionJsonData_3 = value;
		Il2CppCodeGenWriteBarrier((&____partitionJsonData_3), value);
	}

	inline static int32_t get_offset_of__servicesJsonData_4() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ____servicesJsonData_4)); }
	inline JsonData_t4263252052 * get__servicesJsonData_4() const { return ____servicesJsonData_4; }
	inline JsonData_t4263252052 ** get_address_of__servicesJsonData_4() { return &____servicesJsonData_4; }
	inline void set__servicesJsonData_4(JsonData_t4263252052 * value)
	{
		____servicesJsonData_4 = value;
		Il2CppCodeGenWriteBarrier((&____servicesJsonData_4), value);
	}

	inline static int32_t get_offset_of__servicesLoaded_5() { return static_cast<int32_t>(offsetof(RegionEndpointV3_t2255552612, ____servicesLoaded_5)); }
	inline bool get__servicesLoaded_5() const { return ____servicesLoaded_5; }
	inline bool* get_address_of__servicesLoaded_5() { return &____servicesLoaded_5; }
	inline void set__servicesLoaded_5(bool value)
	{
		____servicesLoaded_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONENDPOINTV3_T2255552612_H
#ifndef U3CU3EC_T3667845516_H
#define U3CU3EC_T3667845516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.AWSSDKUtils/<>c
struct  U3CU3Ec_t3667845516  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3667845516_StaticFields
{
public:
	// Amazon.Util.AWSSDKUtils/<>c Amazon.Util.AWSSDKUtils/<>c::<>9
	U3CU3Ec_t3667845516 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.String> Amazon.Util.AWSSDKUtils/<>c::<>9__30_0
	Func_2_t193026957 * ___U3CU3E9__30_0_1;
	// System.Func`2<System.String,System.String> Amazon.Util.AWSSDKUtils/<>c::<>9__30_1
	Func_2_t193026957 * ___U3CU3E9__30_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3667845516_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3667845516 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3667845516 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3667845516 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3667845516_StaticFields, ___U3CU3E9__30_0_1)); }
	inline Func_2_t193026957 * get_U3CU3E9__30_0_1() const { return ___U3CU3E9__30_0_1; }
	inline Func_2_t193026957 ** get_address_of_U3CU3E9__30_0_1() { return &___U3CU3E9__30_0_1; }
	inline void set_U3CU3E9__30_0_1(Func_2_t193026957 * value)
	{
		___U3CU3E9__30_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3667845516_StaticFields, ___U3CU3E9__30_1_2)); }
	inline Func_2_t193026957 * get_U3CU3E9__30_1_2() const { return ___U3CU3E9__30_1_2; }
	inline Func_2_t193026957 ** get_address_of_U3CU3E9__30_1_2() { return &___U3CU3E9__30_1_2; }
	inline void set_U3CU3E9__30_1_2(Func_2_t193026957 * value)
	{
		___U3CU3E9__30_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3667845516_H
#ifndef CRYPTOUTILFACTORY_T2421970493_H
#define CRYPTOUTILFACTORY_T2421970493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.CryptoUtilFactory
struct  CryptoUtilFactory_t2421970493  : public RuntimeObject
{
public:

public:
};

struct CryptoUtilFactory_t2421970493_StaticFields
{
public:
	// Amazon.Util.CryptoUtilFactory/CryptoUtil Amazon.Util.CryptoUtilFactory::util
	CryptoUtil_t1025015063 * ___util_0;
	// System.Collections.Generic.HashSet`1<System.String> Amazon.Util.CryptoUtilFactory::_initializedAlgorithmNames
	HashSet_1_t362681087 * ____initializedAlgorithmNames_1;
	// System.Object Amazon.Util.CryptoUtilFactory::_keyedHashAlgorithmCreationLock
	RuntimeObject * ____keyedHashAlgorithmCreationLock_2;

public:
	inline static int32_t get_offset_of_util_0() { return static_cast<int32_t>(offsetof(CryptoUtilFactory_t2421970493_StaticFields, ___util_0)); }
	inline CryptoUtil_t1025015063 * get_util_0() const { return ___util_0; }
	inline CryptoUtil_t1025015063 ** get_address_of_util_0() { return &___util_0; }
	inline void set_util_0(CryptoUtil_t1025015063 * value)
	{
		___util_0 = value;
		Il2CppCodeGenWriteBarrier((&___util_0), value);
	}

	inline static int32_t get_offset_of__initializedAlgorithmNames_1() { return static_cast<int32_t>(offsetof(CryptoUtilFactory_t2421970493_StaticFields, ____initializedAlgorithmNames_1)); }
	inline HashSet_1_t362681087 * get__initializedAlgorithmNames_1() const { return ____initializedAlgorithmNames_1; }
	inline HashSet_1_t362681087 ** get_address_of__initializedAlgorithmNames_1() { return &____initializedAlgorithmNames_1; }
	inline void set__initializedAlgorithmNames_1(HashSet_1_t362681087 * value)
	{
		____initializedAlgorithmNames_1 = value;
		Il2CppCodeGenWriteBarrier((&____initializedAlgorithmNames_1), value);
	}

	inline static int32_t get_offset_of__keyedHashAlgorithmCreationLock_2() { return static_cast<int32_t>(offsetof(CryptoUtilFactory_t2421970493_StaticFields, ____keyedHashAlgorithmCreationLock_2)); }
	inline RuntimeObject * get__keyedHashAlgorithmCreationLock_2() const { return ____keyedHashAlgorithmCreationLock_2; }
	inline RuntimeObject ** get_address_of__keyedHashAlgorithmCreationLock_2() { return &____keyedHashAlgorithmCreationLock_2; }
	inline void set__keyedHashAlgorithmCreationLock_2(RuntimeObject * value)
	{
		____keyedHashAlgorithmCreationLock_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyedHashAlgorithmCreationLock_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOUTILFACTORY_T2421970493_H
#ifndef PROXYCONFIG_T2693849256_H
#define PROXYCONFIG_T2693849256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.ProxyConfig
struct  ProxyConfig_t2693849256  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROXYCONFIG_T2693849256_H
#ifndef SERVICEMAP_T3203095671_H
#define SERVICEMAP_T3203095671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Internal.RegionEndpointV3/ServiceMap
struct  ServiceMap_t3203095671  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint/Endpoint> Amazon.Internal.RegionEndpointV3/ServiceMap::_serviceMap
	Dictionary_2_t968504607 * ____serviceMap_0;
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.RegionEndpoint/Endpoint> Amazon.Internal.RegionEndpointV3/ServiceMap::_dualServiceMap
	Dictionary_2_t968504607 * ____dualServiceMap_1;

public:
	inline static int32_t get_offset_of__serviceMap_0() { return static_cast<int32_t>(offsetof(ServiceMap_t3203095671, ____serviceMap_0)); }
	inline Dictionary_2_t968504607 * get__serviceMap_0() const { return ____serviceMap_0; }
	inline Dictionary_2_t968504607 ** get_address_of__serviceMap_0() { return &____serviceMap_0; }
	inline void set__serviceMap_0(Dictionary_2_t968504607 * value)
	{
		____serviceMap_0 = value;
		Il2CppCodeGenWriteBarrier((&____serviceMap_0), value);
	}

	inline static int32_t get_offset_of__dualServiceMap_1() { return static_cast<int32_t>(offsetof(ServiceMap_t3203095671, ____dualServiceMap_1)); }
	inline Dictionary_2_t968504607 * get__dualServiceMap_1() const { return ____dualServiceMap_1; }
	inline Dictionary_2_t968504607 ** get_address_of__dualServiceMap_1() { return &____dualServiceMap_1; }
	inline void set__dualServiceMap_1(Dictionary_2_t968504607 * value)
	{
		____dualServiceMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____dualServiceMap_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVICEMAP_T3203095671_H
#ifndef REGIONENDPOINTPROVIDERV3_T342927067_H
#define REGIONENDPOINTPROVIDERV3_T342927067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Internal.RegionEndpointProviderV3
struct  RegionEndpointProviderV3_t342927067  : public RuntimeObject
{
public:
	// ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointProviderV3::_root
	JsonData_t4263252052 * ____root_0;
	// System.Collections.Generic.Dictionary`2<System.String,Amazon.Internal.IRegionEndpoint> Amazon.Internal.RegionEndpointProviderV3::_regionEndpointMap
	Dictionary_2_t2459213150 * ____regionEndpointMap_1;
	// System.Object Amazon.Internal.RegionEndpointProviderV3::_regionEndpointMapLock
	RuntimeObject * ____regionEndpointMapLock_2;
	// System.Object Amazon.Internal.RegionEndpointProviderV3::_allRegionEndpointsLock
	RuntimeObject * ____allRegionEndpointsLock_3;

public:
	inline static int32_t get_offset_of__root_0() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067, ____root_0)); }
	inline JsonData_t4263252052 * get__root_0() const { return ____root_0; }
	inline JsonData_t4263252052 ** get_address_of__root_0() { return &____root_0; }
	inline void set__root_0(JsonData_t4263252052 * value)
	{
		____root_0 = value;
		Il2CppCodeGenWriteBarrier((&____root_0), value);
	}

	inline static int32_t get_offset_of__regionEndpointMap_1() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067, ____regionEndpointMap_1)); }
	inline Dictionary_2_t2459213150 * get__regionEndpointMap_1() const { return ____regionEndpointMap_1; }
	inline Dictionary_2_t2459213150 ** get_address_of__regionEndpointMap_1() { return &____regionEndpointMap_1; }
	inline void set__regionEndpointMap_1(Dictionary_2_t2459213150 * value)
	{
		____regionEndpointMap_1 = value;
		Il2CppCodeGenWriteBarrier((&____regionEndpointMap_1), value);
	}

	inline static int32_t get_offset_of__regionEndpointMapLock_2() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067, ____regionEndpointMapLock_2)); }
	inline RuntimeObject * get__regionEndpointMapLock_2() const { return ____regionEndpointMapLock_2; }
	inline RuntimeObject ** get_address_of__regionEndpointMapLock_2() { return &____regionEndpointMapLock_2; }
	inline void set__regionEndpointMapLock_2(RuntimeObject * value)
	{
		____regionEndpointMapLock_2 = value;
		Il2CppCodeGenWriteBarrier((&____regionEndpointMapLock_2), value);
	}

	inline static int32_t get_offset_of__allRegionEndpointsLock_3() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067, ____allRegionEndpointsLock_3)); }
	inline RuntimeObject * get__allRegionEndpointsLock_3() const { return ____allRegionEndpointsLock_3; }
	inline RuntimeObject ** get_address_of__allRegionEndpointsLock_3() { return &____allRegionEndpointsLock_3; }
	inline void set__allRegionEndpointsLock_3(RuntimeObject * value)
	{
		____allRegionEndpointsLock_3 = value;
		Il2CppCodeGenWriteBarrier((&____allRegionEndpointsLock_3), value);
	}
};

struct RegionEndpointProviderV3_t342927067_StaticFields
{
public:
	// ThirdParty.Json.LitJson.JsonData Amazon.Internal.RegionEndpointProviderV3::_emptyDictionaryJsonData
	JsonData_t4263252052 * ____emptyDictionaryJsonData_4;

public:
	inline static int32_t get_offset_of__emptyDictionaryJsonData_4() { return static_cast<int32_t>(offsetof(RegionEndpointProviderV3_t342927067_StaticFields, ____emptyDictionaryJsonData_4)); }
	inline JsonData_t4263252052 * get__emptyDictionaryJsonData_4() const { return ____emptyDictionaryJsonData_4; }
	inline JsonData_t4263252052 ** get_address_of__emptyDictionaryJsonData_4() { return &____emptyDictionaryJsonData_4; }
	inline void set__emptyDictionaryJsonData_4(JsonData_t4263252052 * value)
	{
		____emptyDictionaryJsonData_4 = value;
		Il2CppCodeGenWriteBarrier((&____emptyDictionaryJsonData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONENDPOINTPROVIDERV3_T342927067_H
#ifndef LEXER_T954714164_H
#define LEXER_T954714164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.Lexer
struct  Lexer_t954714164  : public RuntimeObject
{
public:
	// System.Boolean ThirdParty.Json.LitJson.Lexer::allow_comments
	bool ___allow_comments_2;
	// System.Boolean ThirdParty.Json.LitJson.Lexer::allow_single_quoted_strings
	bool ___allow_single_quoted_strings_3;
	// System.Boolean ThirdParty.Json.LitJson.Lexer::end_of_input
	bool ___end_of_input_4;
	// ThirdParty.Json.LitJson.FsmContext ThirdParty.Json.LitJson.Lexer::fsm_context
	FsmContext_t4275593467 * ___fsm_context_5;
	// System.Int32 ThirdParty.Json.LitJson.Lexer::input_buffer
	int32_t ___input_buffer_6;
	// System.Int32 ThirdParty.Json.LitJson.Lexer::input_char
	int32_t ___input_char_7;
	// System.IO.TextReader ThirdParty.Json.LitJson.Lexer::reader
	TextReader_t1561828458 * ___reader_8;
	// System.Int32 ThirdParty.Json.LitJson.Lexer::state
	int32_t ___state_9;
	// System.Text.StringBuilder ThirdParty.Json.LitJson.Lexer::string_buffer
	StringBuilder_t1221177846 * ___string_buffer_10;
	// System.String ThirdParty.Json.LitJson.Lexer::string_value
	String_t* ___string_value_11;
	// System.Int32 ThirdParty.Json.LitJson.Lexer::token
	int32_t ___token_12;
	// System.Int32 ThirdParty.Json.LitJson.Lexer::unichar
	int32_t ___unichar_13;

public:
	inline static int32_t get_offset_of_allow_comments_2() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___allow_comments_2)); }
	inline bool get_allow_comments_2() const { return ___allow_comments_2; }
	inline bool* get_address_of_allow_comments_2() { return &___allow_comments_2; }
	inline void set_allow_comments_2(bool value)
	{
		___allow_comments_2 = value;
	}

	inline static int32_t get_offset_of_allow_single_quoted_strings_3() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___allow_single_quoted_strings_3)); }
	inline bool get_allow_single_quoted_strings_3() const { return ___allow_single_quoted_strings_3; }
	inline bool* get_address_of_allow_single_quoted_strings_3() { return &___allow_single_quoted_strings_3; }
	inline void set_allow_single_quoted_strings_3(bool value)
	{
		___allow_single_quoted_strings_3 = value;
	}

	inline static int32_t get_offset_of_end_of_input_4() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___end_of_input_4)); }
	inline bool get_end_of_input_4() const { return ___end_of_input_4; }
	inline bool* get_address_of_end_of_input_4() { return &___end_of_input_4; }
	inline void set_end_of_input_4(bool value)
	{
		___end_of_input_4 = value;
	}

	inline static int32_t get_offset_of_fsm_context_5() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___fsm_context_5)); }
	inline FsmContext_t4275593467 * get_fsm_context_5() const { return ___fsm_context_5; }
	inline FsmContext_t4275593467 ** get_address_of_fsm_context_5() { return &___fsm_context_5; }
	inline void set_fsm_context_5(FsmContext_t4275593467 * value)
	{
		___fsm_context_5 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_context_5), value);
	}

	inline static int32_t get_offset_of_input_buffer_6() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___input_buffer_6)); }
	inline int32_t get_input_buffer_6() const { return ___input_buffer_6; }
	inline int32_t* get_address_of_input_buffer_6() { return &___input_buffer_6; }
	inline void set_input_buffer_6(int32_t value)
	{
		___input_buffer_6 = value;
	}

	inline static int32_t get_offset_of_input_char_7() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___input_char_7)); }
	inline int32_t get_input_char_7() const { return ___input_char_7; }
	inline int32_t* get_address_of_input_char_7() { return &___input_char_7; }
	inline void set_input_char_7(int32_t value)
	{
		___input_char_7 = value;
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___reader_8)); }
	inline TextReader_t1561828458 * get_reader_8() const { return ___reader_8; }
	inline TextReader_t1561828458 ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(TextReader_t1561828458 * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_state_9() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___state_9)); }
	inline int32_t get_state_9() const { return ___state_9; }
	inline int32_t* get_address_of_state_9() { return &___state_9; }
	inline void set_state_9(int32_t value)
	{
		___state_9 = value;
	}

	inline static int32_t get_offset_of_string_buffer_10() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___string_buffer_10)); }
	inline StringBuilder_t1221177846 * get_string_buffer_10() const { return ___string_buffer_10; }
	inline StringBuilder_t1221177846 ** get_address_of_string_buffer_10() { return &___string_buffer_10; }
	inline void set_string_buffer_10(StringBuilder_t1221177846 * value)
	{
		___string_buffer_10 = value;
		Il2CppCodeGenWriteBarrier((&___string_buffer_10), value);
	}

	inline static int32_t get_offset_of_string_value_11() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___string_value_11)); }
	inline String_t* get_string_value_11() const { return ___string_value_11; }
	inline String_t** get_address_of_string_value_11() { return &___string_value_11; }
	inline void set_string_value_11(String_t* value)
	{
		___string_value_11 = value;
		Il2CppCodeGenWriteBarrier((&___string_value_11), value);
	}

	inline static int32_t get_offset_of_token_12() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___token_12)); }
	inline int32_t get_token_12() const { return ___token_12; }
	inline int32_t* get_address_of_token_12() { return &___token_12; }
	inline void set_token_12(int32_t value)
	{
		___token_12 = value;
	}

	inline static int32_t get_offset_of_unichar_13() { return static_cast<int32_t>(offsetof(Lexer_t954714164, ___unichar_13)); }
	inline int32_t get_unichar_13() const { return ___unichar_13; }
	inline int32_t* get_address_of_unichar_13() { return &___unichar_13; }
	inline void set_unichar_13(int32_t value)
	{
		___unichar_13 = value;
	}
};

struct Lexer_t954714164_StaticFields
{
public:
	// System.Int32[] ThirdParty.Json.LitJson.Lexer::fsm_return_table
	Int32U5BU5D_t3030399641* ___fsm_return_table_0;
	// ThirdParty.Json.LitJson.Lexer/StateHandler[] ThirdParty.Json.LitJson.Lexer::fsm_handler_table
	StateHandlerU5BU5D_t1374508575* ___fsm_handler_table_1;

public:
	inline static int32_t get_offset_of_fsm_return_table_0() { return static_cast<int32_t>(offsetof(Lexer_t954714164_StaticFields, ___fsm_return_table_0)); }
	inline Int32U5BU5D_t3030399641* get_fsm_return_table_0() const { return ___fsm_return_table_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_fsm_return_table_0() { return &___fsm_return_table_0; }
	inline void set_fsm_return_table_0(Int32U5BU5D_t3030399641* value)
	{
		___fsm_return_table_0 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_return_table_0), value);
	}

	inline static int32_t get_offset_of_fsm_handler_table_1() { return static_cast<int32_t>(offsetof(Lexer_t954714164_StaticFields, ___fsm_handler_table_1)); }
	inline StateHandlerU5BU5D_t1374508575* get_fsm_handler_table_1() const { return ___fsm_handler_table_1; }
	inline StateHandlerU5BU5D_t1374508575** get_address_of_fsm_handler_table_1() { return &___fsm_handler_table_1; }
	inline void set_fsm_handler_table_1(StateHandlerU5BU5D_t1374508575* value)
	{
		___fsm_handler_table_1 = value;
		Il2CppCodeGenWriteBarrier((&___fsm_handler_table_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXER_T954714164_H
#ifndef CRC32_T619824607_H
#define CRC32_T619824607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Ionic.Zlib.CRC32
struct  CRC32_t619824607  : public RuntimeObject
{
public:
	// System.Int64 ThirdParty.Ionic.Zlib.CRC32::_TotalBytesRead
	int64_t ____TotalBytesRead_0;
	// System.UInt32 ThirdParty.Ionic.Zlib.CRC32::_RunningCrc32Result
	uint32_t ____RunningCrc32Result_2;

public:
	inline static int32_t get_offset_of__TotalBytesRead_0() { return static_cast<int32_t>(offsetof(CRC32_t619824607, ____TotalBytesRead_0)); }
	inline int64_t get__TotalBytesRead_0() const { return ____TotalBytesRead_0; }
	inline int64_t* get_address_of__TotalBytesRead_0() { return &____TotalBytesRead_0; }
	inline void set__TotalBytesRead_0(int64_t value)
	{
		____TotalBytesRead_0 = value;
	}

	inline static int32_t get_offset_of__RunningCrc32Result_2() { return static_cast<int32_t>(offsetof(CRC32_t619824607, ____RunningCrc32Result_2)); }
	inline uint32_t get__RunningCrc32Result_2() const { return ____RunningCrc32Result_2; }
	inline uint32_t* get_address_of__RunningCrc32Result_2() { return &____RunningCrc32Result_2; }
	inline void set__RunningCrc32Result_2(uint32_t value)
	{
		____RunningCrc32Result_2 = value;
	}
};

struct CRC32_t619824607_StaticFields
{
public:
	// System.UInt32[] ThirdParty.Ionic.Zlib.CRC32::crc32Table
	UInt32U5BU5D_t59386216* ___crc32Table_1;

public:
	inline static int32_t get_offset_of_crc32Table_1() { return static_cast<int32_t>(offsetof(CRC32_t619824607_StaticFields, ___crc32Table_1)); }
	inline UInt32U5BU5D_t59386216* get_crc32Table_1() const { return ___crc32Table_1; }
	inline UInt32U5BU5D_t59386216** get_address_of_crc32Table_1() { return &___crc32Table_1; }
	inline void set_crc32Table_1(UInt32U5BU5D_t59386216* value)
	{
		___crc32Table_1 = value;
		Il2CppCodeGenWriteBarrier((&___crc32Table_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRC32_T619824607_H
#ifndef U3CU3EC_T1591091833_H
#define U3CU3EC_T1591091833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonMapper/<>c
struct  U3CU3Ec_t1591091833  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1591091833_StaticFields
{
public:
	// ThirdParty.Json.LitJson.JsonMapper/<>c ThirdParty.Json.LitJson.JsonMapper/<>c::<>9
	U3CU3Ec_t1591091833 * ___U3CU3E9_0;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_0
	ExporterFunc_t173265409 * ___U3CU3E9__24_0_1;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_1
	ExporterFunc_t173265409 * ___U3CU3E9__24_1_2;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_2
	ExporterFunc_t173265409 * ___U3CU3E9__24_2_3;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_3
	ExporterFunc_t173265409 * ___U3CU3E9__24_3_4;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_4
	ExporterFunc_t173265409 * ___U3CU3E9__24_4_5;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_5
	ExporterFunc_t173265409 * ___U3CU3E9__24_5_6;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_6
	ExporterFunc_t173265409 * ___U3CU3E9__24_6_7;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_7
	ExporterFunc_t173265409 * ___U3CU3E9__24_7_8;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_8
	ExporterFunc_t173265409 * ___U3CU3E9__24_8_9;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_9
	ExporterFunc_t173265409 * ___U3CU3E9__24_9_10;
	// ThirdParty.Json.LitJson.ExporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__24_10
	ExporterFunc_t173265409 * ___U3CU3E9__24_10_11;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_0
	ImporterFunc_t850687278 * ___U3CU3E9__25_0_12;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_1
	ImporterFunc_t850687278 * ___U3CU3E9__25_1_13;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_2
	ImporterFunc_t850687278 * ___U3CU3E9__25_2_14;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_3
	ImporterFunc_t850687278 * ___U3CU3E9__25_3_15;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_4
	ImporterFunc_t850687278 * ___U3CU3E9__25_4_16;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_5
	ImporterFunc_t850687278 * ___U3CU3E9__25_5_17;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_6
	ImporterFunc_t850687278 * ___U3CU3E9__25_6_18;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_7
	ImporterFunc_t850687278 * ___U3CU3E9__25_7_19;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_8
	ImporterFunc_t850687278 * ___U3CU3E9__25_8_20;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_9
	ImporterFunc_t850687278 * ___U3CU3E9__25_9_21;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_10
	ImporterFunc_t850687278 * ___U3CU3E9__25_10_22;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_11
	ImporterFunc_t850687278 * ___U3CU3E9__25_11_23;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_12
	ImporterFunc_t850687278 * ___U3CU3E9__25_12_24;
	// ThirdParty.Json.LitJson.ImporterFunc ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__25_13
	ImporterFunc_t850687278 * ___U3CU3E9__25_13_25;
	// ThirdParty.Json.LitJson.WrapperFactory ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__31_0
	WrapperFactory_t327905379 * ___U3CU3E9__31_0_26;
	// ThirdParty.Json.LitJson.WrapperFactory ThirdParty.Json.LitJson.JsonMapper/<>c::<>9__32_0
	WrapperFactory_t327905379 * ___U3CU3E9__32_0_27;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1591091833 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1591091833 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1591091833 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_0_1)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_0_1() const { return ___U3CU3E9__24_0_1; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_0_1() { return &___U3CU3E9__24_0_1; }
	inline void set_U3CU3E9__24_0_1(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_1_2)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_1_2() const { return ___U3CU3E9__24_1_2; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_1_2() { return &___U3CU3E9__24_1_2; }
	inline void set_U3CU3E9__24_1_2(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_2_3)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_2_3() const { return ___U3CU3E9__24_2_3; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_2_3() { return &___U3CU3E9__24_2_3; }
	inline void set_U3CU3E9__24_2_3(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_2_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_3_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_3_4)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_3_4() const { return ___U3CU3E9__24_3_4; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_3_4() { return &___U3CU3E9__24_3_4; }
	inline void set_U3CU3E9__24_3_4(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_3_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_4_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_4_5)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_4_5() const { return ___U3CU3E9__24_4_5; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_4_5() { return &___U3CU3E9__24_4_5; }
	inline void set_U3CU3E9__24_4_5(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_4_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_5_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_5_6)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_5_6() const { return ___U3CU3E9__24_5_6; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_5_6() { return &___U3CU3E9__24_5_6; }
	inline void set_U3CU3E9__24_5_6(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_5_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_6_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_6_7)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_6_7() const { return ___U3CU3E9__24_6_7; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_6_7() { return &___U3CU3E9__24_6_7; }
	inline void set_U3CU3E9__24_6_7(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_6_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_6_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_7_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_7_8)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_7_8() const { return ___U3CU3E9__24_7_8; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_7_8() { return &___U3CU3E9__24_7_8; }
	inline void set_U3CU3E9__24_7_8(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_7_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_7_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_8_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_8_9)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_8_9() const { return ___U3CU3E9__24_8_9; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_8_9() { return &___U3CU3E9__24_8_9; }
	inline void set_U3CU3E9__24_8_9(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_8_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_8_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_9_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_9_10)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_9_10() const { return ___U3CU3E9__24_9_10; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_9_10() { return &___U3CU3E9__24_9_10; }
	inline void set_U3CU3E9__24_9_10(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_9_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_9_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_10_11() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__24_10_11)); }
	inline ExporterFunc_t173265409 * get_U3CU3E9__24_10_11() const { return ___U3CU3E9__24_10_11; }
	inline ExporterFunc_t173265409 ** get_address_of_U3CU3E9__24_10_11() { return &___U3CU3E9__24_10_11; }
	inline void set_U3CU3E9__24_10_11(ExporterFunc_t173265409 * value)
	{
		___U3CU3E9__24_10_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__24_10_11), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_0_12() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_0_12)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_0_12() const { return ___U3CU3E9__25_0_12; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_0_12() { return &___U3CU3E9__25_0_12; }
	inline void set_U3CU3E9__25_0_12(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_0_12), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_1_13() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_1_13)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_1_13() const { return ___U3CU3E9__25_1_13; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_1_13() { return &___U3CU3E9__25_1_13; }
	inline void set_U3CU3E9__25_1_13(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_1_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_1_13), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_2_14() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_2_14)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_2_14() const { return ___U3CU3E9__25_2_14; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_2_14() { return &___U3CU3E9__25_2_14; }
	inline void set_U3CU3E9__25_2_14(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_2_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_2_14), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_3_15() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_3_15)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_3_15() const { return ___U3CU3E9__25_3_15; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_3_15() { return &___U3CU3E9__25_3_15; }
	inline void set_U3CU3E9__25_3_15(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_3_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_3_15), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_4_16() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_4_16)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_4_16() const { return ___U3CU3E9__25_4_16; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_4_16() { return &___U3CU3E9__25_4_16; }
	inline void set_U3CU3E9__25_4_16(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_4_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_4_16), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_5_17() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_5_17)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_5_17() const { return ___U3CU3E9__25_5_17; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_5_17() { return &___U3CU3E9__25_5_17; }
	inline void set_U3CU3E9__25_5_17(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_5_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_5_17), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_6_18() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_6_18)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_6_18() const { return ___U3CU3E9__25_6_18; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_6_18() { return &___U3CU3E9__25_6_18; }
	inline void set_U3CU3E9__25_6_18(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_6_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_6_18), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_7_19() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_7_19)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_7_19() const { return ___U3CU3E9__25_7_19; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_7_19() { return &___U3CU3E9__25_7_19; }
	inline void set_U3CU3E9__25_7_19(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_7_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_7_19), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_8_20() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_8_20)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_8_20() const { return ___U3CU3E9__25_8_20; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_8_20() { return &___U3CU3E9__25_8_20; }
	inline void set_U3CU3E9__25_8_20(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_8_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_8_20), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_9_21() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_9_21)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_9_21() const { return ___U3CU3E9__25_9_21; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_9_21() { return &___U3CU3E9__25_9_21; }
	inline void set_U3CU3E9__25_9_21(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_9_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_9_21), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_10_22() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_10_22)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_10_22() const { return ___U3CU3E9__25_10_22; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_10_22() { return &___U3CU3E9__25_10_22; }
	inline void set_U3CU3E9__25_10_22(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_10_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_10_22), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_11_23() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_11_23)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_11_23() const { return ___U3CU3E9__25_11_23; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_11_23() { return &___U3CU3E9__25_11_23; }
	inline void set_U3CU3E9__25_11_23(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_11_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_11_23), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_12_24() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_12_24)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_12_24() const { return ___U3CU3E9__25_12_24; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_12_24() { return &___U3CU3E9__25_12_24; }
	inline void set_U3CU3E9__25_12_24(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_12_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_12_24), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_13_25() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__25_13_25)); }
	inline ImporterFunc_t850687278 * get_U3CU3E9__25_13_25() const { return ___U3CU3E9__25_13_25; }
	inline ImporterFunc_t850687278 ** get_address_of_U3CU3E9__25_13_25() { return &___U3CU3E9__25_13_25; }
	inline void set_U3CU3E9__25_13_25(ImporterFunc_t850687278 * value)
	{
		___U3CU3E9__25_13_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_13_25), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_0_26() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__31_0_26)); }
	inline WrapperFactory_t327905379 * get_U3CU3E9__31_0_26() const { return ___U3CU3E9__31_0_26; }
	inline WrapperFactory_t327905379 ** get_address_of_U3CU3E9__31_0_26() { return &___U3CU3E9__31_0_26; }
	inline void set_U3CU3E9__31_0_26(WrapperFactory_t327905379 * value)
	{
		___U3CU3E9__31_0_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_0_26), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_0_27() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1591091833_StaticFields, ___U3CU3E9__32_0_27)); }
	inline WrapperFactory_t327905379 * get_U3CU3E9__32_0_27() const { return ___U3CU3E9__32_0_27; }
	inline WrapperFactory_t327905379 ** get_address_of_U3CU3E9__32_0_27() { return &___U3CU3E9__32_0_27; }
	inline void set_U3CU3E9__32_0_27(WrapperFactory_t327905379 * value)
	{
		___U3CU3E9__32_0_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_0_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1591091833_H
#ifndef FSMCONTEXT_T4275593467_H
#define FSMCONTEXT_T4275593467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.FsmContext
struct  FsmContext_t4275593467  : public RuntimeObject
{
public:
	// System.Boolean ThirdParty.Json.LitJson.FsmContext::Return
	bool ___Return_0;
	// System.Int32 ThirdParty.Json.LitJson.FsmContext::NextState
	int32_t ___NextState_1;
	// ThirdParty.Json.LitJson.Lexer ThirdParty.Json.LitJson.FsmContext::L
	Lexer_t954714164 * ___L_2;
	// System.Int32 ThirdParty.Json.LitJson.FsmContext::StateStack
	int32_t ___StateStack_3;

public:
	inline static int32_t get_offset_of_Return_0() { return static_cast<int32_t>(offsetof(FsmContext_t4275593467, ___Return_0)); }
	inline bool get_Return_0() const { return ___Return_0; }
	inline bool* get_address_of_Return_0() { return &___Return_0; }
	inline void set_Return_0(bool value)
	{
		___Return_0 = value;
	}

	inline static int32_t get_offset_of_NextState_1() { return static_cast<int32_t>(offsetof(FsmContext_t4275593467, ___NextState_1)); }
	inline int32_t get_NextState_1() const { return ___NextState_1; }
	inline int32_t* get_address_of_NextState_1() { return &___NextState_1; }
	inline void set_NextState_1(int32_t value)
	{
		___NextState_1 = value;
	}

	inline static int32_t get_offset_of_L_2() { return static_cast<int32_t>(offsetof(FsmContext_t4275593467, ___L_2)); }
	inline Lexer_t954714164 * get_L_2() const { return ___L_2; }
	inline Lexer_t954714164 ** get_address_of_L_2() { return &___L_2; }
	inline void set_L_2(Lexer_t954714164 * value)
	{
		___L_2 = value;
		Il2CppCodeGenWriteBarrier((&___L_2), value);
	}

	inline static int32_t get_offset_of_StateStack_3() { return static_cast<int32_t>(offsetof(FsmContext_t4275593467, ___StateStack_3)); }
	inline int32_t get_StateStack_3() const { return ___StateStack_3; }
	inline int32_t* get_address_of_StateStack_3() { return &___StateStack_3; }
	inline void set_StateStack_3(int32_t value)
	{
		___StateStack_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FSMCONTEXT_T4275593467_H
#ifndef JSONWRITER_T3014444111_H
#define JSONWRITER_T3014444111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonWriter
struct  JsonWriter_t3014444111  : public RuntimeObject
{
public:
	// ThirdParty.Json.LitJson.WriterContext ThirdParty.Json.LitJson.JsonWriter::context
	WriterContext_t1209007092 * ___context_1;
	// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.WriterContext> ThirdParty.Json.LitJson.JsonWriter::ctx_stack
	Stack_1_t2296735246 * ___ctx_stack_2;
	// System.Boolean ThirdParty.Json.LitJson.JsonWriter::has_reached_end
	bool ___has_reached_end_3;
	// System.Char[] ThirdParty.Json.LitJson.JsonWriter::hex_seq
	CharU5BU5D_t1328083999* ___hex_seq_4;
	// System.Int32 ThirdParty.Json.LitJson.JsonWriter::indentation
	int32_t ___indentation_5;
	// System.Int32 ThirdParty.Json.LitJson.JsonWriter::indent_value
	int32_t ___indent_value_6;
	// System.Text.StringBuilder ThirdParty.Json.LitJson.JsonWriter::inst_string_builder
	StringBuilder_t1221177846 * ___inst_string_builder_7;
	// System.Boolean ThirdParty.Json.LitJson.JsonWriter::pretty_print
	bool ___pretty_print_8;
	// System.Boolean ThirdParty.Json.LitJson.JsonWriter::validate
	bool ___validate_9;
	// System.IO.TextWriter ThirdParty.Json.LitJson.JsonWriter::writer
	TextWriter_t4027217640 * ___writer_10;

public:
	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___context_1)); }
	inline WriterContext_t1209007092 * get_context_1() const { return ___context_1; }
	inline WriterContext_t1209007092 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(WriterContext_t1209007092 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}

	inline static int32_t get_offset_of_ctx_stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___ctx_stack_2)); }
	inline Stack_1_t2296735246 * get_ctx_stack_2() const { return ___ctx_stack_2; }
	inline Stack_1_t2296735246 ** get_address_of_ctx_stack_2() { return &___ctx_stack_2; }
	inline void set_ctx_stack_2(Stack_1_t2296735246 * value)
	{
		___ctx_stack_2 = value;
		Il2CppCodeGenWriteBarrier((&___ctx_stack_2), value);
	}

	inline static int32_t get_offset_of_has_reached_end_3() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___has_reached_end_3)); }
	inline bool get_has_reached_end_3() const { return ___has_reached_end_3; }
	inline bool* get_address_of_has_reached_end_3() { return &___has_reached_end_3; }
	inline void set_has_reached_end_3(bool value)
	{
		___has_reached_end_3 = value;
	}

	inline static int32_t get_offset_of_hex_seq_4() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___hex_seq_4)); }
	inline CharU5BU5D_t1328083999* get_hex_seq_4() const { return ___hex_seq_4; }
	inline CharU5BU5D_t1328083999** get_address_of_hex_seq_4() { return &___hex_seq_4; }
	inline void set_hex_seq_4(CharU5BU5D_t1328083999* value)
	{
		___hex_seq_4 = value;
		Il2CppCodeGenWriteBarrier((&___hex_seq_4), value);
	}

	inline static int32_t get_offset_of_indentation_5() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___indentation_5)); }
	inline int32_t get_indentation_5() const { return ___indentation_5; }
	inline int32_t* get_address_of_indentation_5() { return &___indentation_5; }
	inline void set_indentation_5(int32_t value)
	{
		___indentation_5 = value;
	}

	inline static int32_t get_offset_of_indent_value_6() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___indent_value_6)); }
	inline int32_t get_indent_value_6() const { return ___indent_value_6; }
	inline int32_t* get_address_of_indent_value_6() { return &___indent_value_6; }
	inline void set_indent_value_6(int32_t value)
	{
		___indent_value_6 = value;
	}

	inline static int32_t get_offset_of_inst_string_builder_7() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___inst_string_builder_7)); }
	inline StringBuilder_t1221177846 * get_inst_string_builder_7() const { return ___inst_string_builder_7; }
	inline StringBuilder_t1221177846 ** get_address_of_inst_string_builder_7() { return &___inst_string_builder_7; }
	inline void set_inst_string_builder_7(StringBuilder_t1221177846 * value)
	{
		___inst_string_builder_7 = value;
		Il2CppCodeGenWriteBarrier((&___inst_string_builder_7), value);
	}

	inline static int32_t get_offset_of_pretty_print_8() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___pretty_print_8)); }
	inline bool get_pretty_print_8() const { return ___pretty_print_8; }
	inline bool* get_address_of_pretty_print_8() { return &___pretty_print_8; }
	inline void set_pretty_print_8(bool value)
	{
		___pretty_print_8 = value;
	}

	inline static int32_t get_offset_of_validate_9() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___validate_9)); }
	inline bool get_validate_9() const { return ___validate_9; }
	inline bool* get_address_of_validate_9() { return &___validate_9; }
	inline void set_validate_9(bool value)
	{
		___validate_9 = value;
	}

	inline static int32_t get_offset_of_writer_10() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111, ___writer_10)); }
	inline TextWriter_t4027217640 * get_writer_10() const { return ___writer_10; }
	inline TextWriter_t4027217640 ** get_address_of_writer_10() { return &___writer_10; }
	inline void set_writer_10(TextWriter_t4027217640 * value)
	{
		___writer_10 = value;
		Il2CppCodeGenWriteBarrier((&___writer_10), value);
	}
};

struct JsonWriter_t3014444111_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo ThirdParty.Json.LitJson.JsonWriter::number_format
	NumberFormatInfo_t104580544 * ___number_format_0;

public:
	inline static int32_t get_offset_of_number_format_0() { return static_cast<int32_t>(offsetof(JsonWriter_t3014444111_StaticFields, ___number_format_0)); }
	inline NumberFormatInfo_t104580544 * get_number_format_0() const { return ___number_format_0; }
	inline NumberFormatInfo_t104580544 ** get_address_of_number_format_0() { return &___number_format_0; }
	inline void set_number_format_0(NumberFormatInfo_t104580544 * value)
	{
		___number_format_0 = value;
		Il2CppCodeGenWriteBarrier((&___number_format_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T3014444111_H
#ifndef WRITERCONTEXT_T1209007092_H
#define WRITERCONTEXT_T1209007092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.WriterContext
struct  WriterContext_t1209007092  : public RuntimeObject
{
public:
	// System.Int32 ThirdParty.Json.LitJson.WriterContext::Count
	int32_t ___Count_0;
	// System.Boolean ThirdParty.Json.LitJson.WriterContext::InArray
	bool ___InArray_1;
	// System.Boolean ThirdParty.Json.LitJson.WriterContext::InObject
	bool ___InObject_2;
	// System.Boolean ThirdParty.Json.LitJson.WriterContext::ExpectingValue
	bool ___ExpectingValue_3;
	// System.Int32 ThirdParty.Json.LitJson.WriterContext::Padding
	int32_t ___Padding_4;

public:
	inline static int32_t get_offset_of_Count_0() { return static_cast<int32_t>(offsetof(WriterContext_t1209007092, ___Count_0)); }
	inline int32_t get_Count_0() const { return ___Count_0; }
	inline int32_t* get_address_of_Count_0() { return &___Count_0; }
	inline void set_Count_0(int32_t value)
	{
		___Count_0 = value;
	}

	inline static int32_t get_offset_of_InArray_1() { return static_cast<int32_t>(offsetof(WriterContext_t1209007092, ___InArray_1)); }
	inline bool get_InArray_1() const { return ___InArray_1; }
	inline bool* get_address_of_InArray_1() { return &___InArray_1; }
	inline void set_InArray_1(bool value)
	{
		___InArray_1 = value;
	}

	inline static int32_t get_offset_of_InObject_2() { return static_cast<int32_t>(offsetof(WriterContext_t1209007092, ___InObject_2)); }
	inline bool get_InObject_2() const { return ___InObject_2; }
	inline bool* get_address_of_InObject_2() { return &___InObject_2; }
	inline void set_InObject_2(bool value)
	{
		___InObject_2 = value;
	}

	inline static int32_t get_offset_of_ExpectingValue_3() { return static_cast<int32_t>(offsetof(WriterContext_t1209007092, ___ExpectingValue_3)); }
	inline bool get_ExpectingValue_3() const { return ___ExpectingValue_3; }
	inline bool* get_address_of_ExpectingValue_3() { return &___ExpectingValue_3; }
	inline void set_ExpectingValue_3(bool value)
	{
		___ExpectingValue_3 = value;
	}

	inline static int32_t get_offset_of_Padding_4() { return static_cast<int32_t>(offsetof(WriterContext_t1209007092, ___Padding_4)); }
	inline int32_t get_Padding_4() const { return ___Padding_4; }
	inline int32_t* get_address_of_Padding_4() { return &___Padding_4; }
	inline void set_Padding_4(int32_t value)
	{
		___Padding_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITERCONTEXT_T1209007092_H
#ifndef U3CU3EC__DISPLAYCLASS94_1_T1056790043_H
#define U3CU3EC__DISPLAYCLASS94_1_T1056790043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSConfigs/<>c__DisplayClass94_1
struct  U3CU3Ec__DisplayClass94_1_t1056790043  : public RuntimeObject
{
public:
	// System.Threading.ManualResetEvent Amazon.AWSConfigs/<>c__DisplayClass94_1::e
	ManualResetEvent_t926074657 * ___e_0;
	// Amazon.AWSConfigs/<>c__DisplayClass94_0 Amazon.AWSConfigs/<>c__DisplayClass94_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass94_0_t1056790044 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass94_1_t1056790043, ___e_0)); }
	inline ManualResetEvent_t926074657 * get_e_0() const { return ___e_0; }
	inline ManualResetEvent_t926074657 ** get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(ManualResetEvent_t926074657 * value)
	{
		___e_0 = value;
		Il2CppCodeGenWriteBarrier((&___e_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass94_1_t1056790043, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass94_0_t1056790044 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass94_0_t1056790044 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass94_0_t1056790044 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS94_1_T1056790043_H
#ifndef U3CU3EC__DISPLAYCLASS96_0_T1056790110_H
#define U3CU3EC__DISPLAYCLASS96_0_T1056790110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSConfigs/<>c__DisplayClass96_0
struct  U3CU3Ec__DisplayClass96_0_t1056790110  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Amazon.AWSConfigs/<>c__DisplayClass96_0::propertyInfo
	PropertyInfo_t * ___propertyInfo_0;

public:
	inline static int32_t get_offset_of_propertyInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass96_0_t1056790110, ___propertyInfo_0)); }
	inline PropertyInfo_t * get_propertyInfo_0() const { return ___propertyInfo_0; }
	inline PropertyInfo_t ** get_address_of_propertyInfo_0() { return &___propertyInfo_0; }
	inline void set_propertyInfo_0(PropertyInfo_t * value)
	{
		___propertyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS96_0_T1056790110_H
#ifndef MD5CORE_T1840877875_H
#define MD5CORE_T1840877875_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.MD5.MD5Core
struct  MD5Core_t1840877875  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5CORE_T1840877875_H
#ifndef JSONMAPPER_T77474459_H
#define JSONMAPPER_T77474459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonMapper
struct  JsonMapper_t77474459  : public RuntimeObject
{
public:

public:
};

struct JsonMapper_t77474459_StaticFields
{
public:
	// System.Int32 ThirdParty.Json.LitJson.JsonMapper::max_nesting_depth
	int32_t ___max_nesting_depth_0;
	// System.IFormatProvider ThirdParty.Json.LitJson.JsonMapper::datetime_format
	RuntimeObject* ___datetime_format_1;
	// System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc> ThirdParty.Json.LitJson.JsonMapper::base_exporters_table
	RuntimeObject* ___base_exporters_table_2;
	// System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ExporterFunc> ThirdParty.Json.LitJson.JsonMapper::custom_exporters_table
	RuntimeObject* ___custom_exporters_table_3;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>> ThirdParty.Json.LitJson.JsonMapper::base_importers_table
	RuntimeObject* ___base_importers_table_4;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ImporterFunc>> ThirdParty.Json.LitJson.JsonMapper::custom_importers_table
	RuntimeObject* ___custom_importers_table_5;
	// System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ArrayMetadata> ThirdParty.Json.LitJson.JsonMapper::array_metadata
	RuntimeObject* ___array_metadata_6;
	// System.Object ThirdParty.Json.LitJson.JsonMapper::array_metadata_lock
	RuntimeObject * ___array_metadata_lock_7;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>> ThirdParty.Json.LitJson.JsonMapper::conv_ops
	RuntimeObject* ___conv_ops_8;
	// System.Object ThirdParty.Json.LitJson.JsonMapper::conv_ops_lock
	RuntimeObject * ___conv_ops_lock_9;
	// System.Collections.Generic.IDictionary`2<System.Type,ThirdParty.Json.LitJson.ObjectMetadata> ThirdParty.Json.LitJson.JsonMapper::object_metadata
	RuntimeObject* ___object_metadata_10;
	// System.Object ThirdParty.Json.LitJson.JsonMapper::object_metadata_lock
	RuntimeObject * ___object_metadata_lock_11;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IList`1<ThirdParty.Json.LitJson.PropertyMetadata>> ThirdParty.Json.LitJson.JsonMapper::type_properties
	RuntimeObject* ___type_properties_12;
	// System.Object ThirdParty.Json.LitJson.JsonMapper::type_properties_lock
	RuntimeObject * ___type_properties_lock_13;
	// ThirdParty.Json.LitJson.JsonWriter ThirdParty.Json.LitJson.JsonMapper::static_writer
	JsonWriter_t3014444111 * ___static_writer_14;
	// System.Object ThirdParty.Json.LitJson.JsonMapper::static_writer_lock
	RuntimeObject * ___static_writer_lock_15;
	// System.Collections.Generic.HashSet`1<System.String> ThirdParty.Json.LitJson.JsonMapper::dictionary_properties_to_ignore
	HashSet_1_t362681087 * ___dictionary_properties_to_ignore_16;

public:
	inline static int32_t get_offset_of_max_nesting_depth_0() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___max_nesting_depth_0)); }
	inline int32_t get_max_nesting_depth_0() const { return ___max_nesting_depth_0; }
	inline int32_t* get_address_of_max_nesting_depth_0() { return &___max_nesting_depth_0; }
	inline void set_max_nesting_depth_0(int32_t value)
	{
		___max_nesting_depth_0 = value;
	}

	inline static int32_t get_offset_of_datetime_format_1() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___datetime_format_1)); }
	inline RuntimeObject* get_datetime_format_1() const { return ___datetime_format_1; }
	inline RuntimeObject** get_address_of_datetime_format_1() { return &___datetime_format_1; }
	inline void set_datetime_format_1(RuntimeObject* value)
	{
		___datetime_format_1 = value;
		Il2CppCodeGenWriteBarrier((&___datetime_format_1), value);
	}

	inline static int32_t get_offset_of_base_exporters_table_2() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___base_exporters_table_2)); }
	inline RuntimeObject* get_base_exporters_table_2() const { return ___base_exporters_table_2; }
	inline RuntimeObject** get_address_of_base_exporters_table_2() { return &___base_exporters_table_2; }
	inline void set_base_exporters_table_2(RuntimeObject* value)
	{
		___base_exporters_table_2 = value;
		Il2CppCodeGenWriteBarrier((&___base_exporters_table_2), value);
	}

	inline static int32_t get_offset_of_custom_exporters_table_3() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___custom_exporters_table_3)); }
	inline RuntimeObject* get_custom_exporters_table_3() const { return ___custom_exporters_table_3; }
	inline RuntimeObject** get_address_of_custom_exporters_table_3() { return &___custom_exporters_table_3; }
	inline void set_custom_exporters_table_3(RuntimeObject* value)
	{
		___custom_exporters_table_3 = value;
		Il2CppCodeGenWriteBarrier((&___custom_exporters_table_3), value);
	}

	inline static int32_t get_offset_of_base_importers_table_4() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___base_importers_table_4)); }
	inline RuntimeObject* get_base_importers_table_4() const { return ___base_importers_table_4; }
	inline RuntimeObject** get_address_of_base_importers_table_4() { return &___base_importers_table_4; }
	inline void set_base_importers_table_4(RuntimeObject* value)
	{
		___base_importers_table_4 = value;
		Il2CppCodeGenWriteBarrier((&___base_importers_table_4), value);
	}

	inline static int32_t get_offset_of_custom_importers_table_5() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___custom_importers_table_5)); }
	inline RuntimeObject* get_custom_importers_table_5() const { return ___custom_importers_table_5; }
	inline RuntimeObject** get_address_of_custom_importers_table_5() { return &___custom_importers_table_5; }
	inline void set_custom_importers_table_5(RuntimeObject* value)
	{
		___custom_importers_table_5 = value;
		Il2CppCodeGenWriteBarrier((&___custom_importers_table_5), value);
	}

	inline static int32_t get_offset_of_array_metadata_6() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___array_metadata_6)); }
	inline RuntimeObject* get_array_metadata_6() const { return ___array_metadata_6; }
	inline RuntimeObject** get_address_of_array_metadata_6() { return &___array_metadata_6; }
	inline void set_array_metadata_6(RuntimeObject* value)
	{
		___array_metadata_6 = value;
		Il2CppCodeGenWriteBarrier((&___array_metadata_6), value);
	}

	inline static int32_t get_offset_of_array_metadata_lock_7() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___array_metadata_lock_7)); }
	inline RuntimeObject * get_array_metadata_lock_7() const { return ___array_metadata_lock_7; }
	inline RuntimeObject ** get_address_of_array_metadata_lock_7() { return &___array_metadata_lock_7; }
	inline void set_array_metadata_lock_7(RuntimeObject * value)
	{
		___array_metadata_lock_7 = value;
		Il2CppCodeGenWriteBarrier((&___array_metadata_lock_7), value);
	}

	inline static int32_t get_offset_of_conv_ops_8() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___conv_ops_8)); }
	inline RuntimeObject* get_conv_ops_8() const { return ___conv_ops_8; }
	inline RuntimeObject** get_address_of_conv_ops_8() { return &___conv_ops_8; }
	inline void set_conv_ops_8(RuntimeObject* value)
	{
		___conv_ops_8 = value;
		Il2CppCodeGenWriteBarrier((&___conv_ops_8), value);
	}

	inline static int32_t get_offset_of_conv_ops_lock_9() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___conv_ops_lock_9)); }
	inline RuntimeObject * get_conv_ops_lock_9() const { return ___conv_ops_lock_9; }
	inline RuntimeObject ** get_address_of_conv_ops_lock_9() { return &___conv_ops_lock_9; }
	inline void set_conv_ops_lock_9(RuntimeObject * value)
	{
		___conv_ops_lock_9 = value;
		Il2CppCodeGenWriteBarrier((&___conv_ops_lock_9), value);
	}

	inline static int32_t get_offset_of_object_metadata_10() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___object_metadata_10)); }
	inline RuntimeObject* get_object_metadata_10() const { return ___object_metadata_10; }
	inline RuntimeObject** get_address_of_object_metadata_10() { return &___object_metadata_10; }
	inline void set_object_metadata_10(RuntimeObject* value)
	{
		___object_metadata_10 = value;
		Il2CppCodeGenWriteBarrier((&___object_metadata_10), value);
	}

	inline static int32_t get_offset_of_object_metadata_lock_11() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___object_metadata_lock_11)); }
	inline RuntimeObject * get_object_metadata_lock_11() const { return ___object_metadata_lock_11; }
	inline RuntimeObject ** get_address_of_object_metadata_lock_11() { return &___object_metadata_lock_11; }
	inline void set_object_metadata_lock_11(RuntimeObject * value)
	{
		___object_metadata_lock_11 = value;
		Il2CppCodeGenWriteBarrier((&___object_metadata_lock_11), value);
	}

	inline static int32_t get_offset_of_type_properties_12() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___type_properties_12)); }
	inline RuntimeObject* get_type_properties_12() const { return ___type_properties_12; }
	inline RuntimeObject** get_address_of_type_properties_12() { return &___type_properties_12; }
	inline void set_type_properties_12(RuntimeObject* value)
	{
		___type_properties_12 = value;
		Il2CppCodeGenWriteBarrier((&___type_properties_12), value);
	}

	inline static int32_t get_offset_of_type_properties_lock_13() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___type_properties_lock_13)); }
	inline RuntimeObject * get_type_properties_lock_13() const { return ___type_properties_lock_13; }
	inline RuntimeObject ** get_address_of_type_properties_lock_13() { return &___type_properties_lock_13; }
	inline void set_type_properties_lock_13(RuntimeObject * value)
	{
		___type_properties_lock_13 = value;
		Il2CppCodeGenWriteBarrier((&___type_properties_lock_13), value);
	}

	inline static int32_t get_offset_of_static_writer_14() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___static_writer_14)); }
	inline JsonWriter_t3014444111 * get_static_writer_14() const { return ___static_writer_14; }
	inline JsonWriter_t3014444111 ** get_address_of_static_writer_14() { return &___static_writer_14; }
	inline void set_static_writer_14(JsonWriter_t3014444111 * value)
	{
		___static_writer_14 = value;
		Il2CppCodeGenWriteBarrier((&___static_writer_14), value);
	}

	inline static int32_t get_offset_of_static_writer_lock_15() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___static_writer_lock_15)); }
	inline RuntimeObject * get_static_writer_lock_15() const { return ___static_writer_lock_15; }
	inline RuntimeObject ** get_address_of_static_writer_lock_15() { return &___static_writer_lock_15; }
	inline void set_static_writer_lock_15(RuntimeObject * value)
	{
		___static_writer_lock_15 = value;
		Il2CppCodeGenWriteBarrier((&___static_writer_lock_15), value);
	}

	inline static int32_t get_offset_of_dictionary_properties_to_ignore_16() { return static_cast<int32_t>(offsetof(JsonMapper_t77474459_StaticFields, ___dictionary_properties_to_ignore_16)); }
	inline HashSet_1_t362681087 * get_dictionary_properties_to_ignore_16() const { return ___dictionary_properties_to_ignore_16; }
	inline HashSet_1_t362681087 ** get_address_of_dictionary_properties_to_ignore_16() { return &___dictionary_properties_to_ignore_16; }
	inline void set_dictionary_properties_to_ignore_16(HashSet_1_t362681087 * value)
	{
		___dictionary_properties_to_ignore_16 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_properties_to_ignore_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONMAPPER_T77474459_H
#ifndef U3CU3EC__DISPLAYCLASS94_0_T1056790044_H
#define U3CU3EC__DISPLAYCLASS94_0_T1056790044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSConfigs/<>c__DisplayClass94_0
struct  U3CU3Ec__DisplayClass94_0_t1056790044  : public RuntimeObject
{
public:
	// UnityEngine.TextAsset Amazon.AWSConfigs/<>c__DisplayClass94_0::awsConfig
	TextAsset_t3973159845 * ___awsConfig_0;
	// System.Xml.Linq.XDocument Amazon.AWSConfigs/<>c__DisplayClass94_0::xDoc
	XDocument_t2733326047 * ___xDoc_1;
	// System.Action Amazon.AWSConfigs/<>c__DisplayClass94_0::action
	Action_t3226471752 * ___action_2;

public:
	inline static int32_t get_offset_of_awsConfig_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass94_0_t1056790044, ___awsConfig_0)); }
	inline TextAsset_t3973159845 * get_awsConfig_0() const { return ___awsConfig_0; }
	inline TextAsset_t3973159845 ** get_address_of_awsConfig_0() { return &___awsConfig_0; }
	inline void set_awsConfig_0(TextAsset_t3973159845 * value)
	{
		___awsConfig_0 = value;
		Il2CppCodeGenWriteBarrier((&___awsConfig_0), value);
	}

	inline static int32_t get_offset_of_xDoc_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass94_0_t1056790044, ___xDoc_1)); }
	inline XDocument_t2733326047 * get_xDoc_1() const { return ___xDoc_1; }
	inline XDocument_t2733326047 ** get_address_of_xDoc_1() { return &___xDoc_1; }
	inline void set_xDoc_1(XDocument_t2733326047 * value)
	{
		___xDoc_1 = value;
		Il2CppCodeGenWriteBarrier((&___xDoc_1), value);
	}

	inline static int32_t get_offset_of_action_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass94_0_t1056790044, ___action_2)); }
	inline Action_t3226471752 * get_action_2() const { return ___action_2; }
	inline Action_t3226471752 ** get_address_of_action_2() { return &___action_2; }
	inline void set_action_2(Action_t3226471752 * value)
	{
		___action_2 = value;
		Il2CppCodeGenWriteBarrier((&___action_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS94_0_T1056790044_H
#ifndef ORDEREDDICTIONARYENUMERATOR_T1664192327_H
#define ORDEREDDICTIONARYENUMERATOR_T1664192327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.OrderedDictionaryEnumerator
struct  OrderedDictionaryEnumerator_t1664192327  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>> ThirdParty.Json.LitJson.OrderedDictionaryEnumerator::list_enumerator
	RuntimeObject* ___list_enumerator_0;

public:
	inline static int32_t get_offset_of_list_enumerator_0() { return static_cast<int32_t>(offsetof(OrderedDictionaryEnumerator_t1664192327, ___list_enumerator_0)); }
	inline RuntimeObject* get_list_enumerator_0() const { return ___list_enumerator_0; }
	inline RuntimeObject** get_address_of_list_enumerator_0() { return &___list_enumerator_0; }
	inline void set_list_enumerator_0(RuntimeObject* value)
	{
		___list_enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORDEREDDICTIONARYENUMERATOR_T1664192327_H
#ifndef OBJC_T468753822_H
#define OBJC_T468753822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.ObjC
struct  ObjC_t468753822  : public RuntimeObject
{
public:

public:
};

struct ObjC_t468753822_StaticFields
{
public:
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::TimeIntervalSinceReferenceDateSelector
	Selector_t1939762325 * ___TimeIntervalSinceReferenceDateSelector_0;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::AbsoluteStringSelector
	Selector_t1939762325 * ___AbsoluteStringSelector_1;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::DoubleValueSelector
	Selector_t1939762325 * ___DoubleValueSelector_2;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::CountSelector
	Selector_t1939762325 * ___CountSelector_3;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::ObjectAtIndexSelector
	Selector_t1939762325 * ___ObjectAtIndexSelector_4;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::AllObjectsSelector
	Selector_t1939762325 * ___AllObjectsSelector_5;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::ArrayWithObjects_CountSelector
	Selector_t1939762325 * ___ArrayWithObjects_CountSelector_6;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::SetWithArraySelector
	Selector_t1939762325 * ___SetWithArraySelector_7;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::InitWithCharacters_lengthSelector
	Selector_t1939762325 * ___InitWithCharacters_lengthSelector_8;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::URLWithStringSelector
	Selector_t1939762325 * ___URLWithStringSelector_9;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::DateWithTimeIntervalSinceReferenceDateSelector
	Selector_t1939762325 * ___DateWithTimeIntervalSinceReferenceDateSelector_10;
	// ThirdParty.iOS4Unity.Selector ThirdParty.iOS4Unity.ObjC::InitWithDoubleSelector
	Selector_t1939762325 * ___InitWithDoubleSelector_11;

public:
	inline static int32_t get_offset_of_TimeIntervalSinceReferenceDateSelector_0() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___TimeIntervalSinceReferenceDateSelector_0)); }
	inline Selector_t1939762325 * get_TimeIntervalSinceReferenceDateSelector_0() const { return ___TimeIntervalSinceReferenceDateSelector_0; }
	inline Selector_t1939762325 ** get_address_of_TimeIntervalSinceReferenceDateSelector_0() { return &___TimeIntervalSinceReferenceDateSelector_0; }
	inline void set_TimeIntervalSinceReferenceDateSelector_0(Selector_t1939762325 * value)
	{
		___TimeIntervalSinceReferenceDateSelector_0 = value;
		Il2CppCodeGenWriteBarrier((&___TimeIntervalSinceReferenceDateSelector_0), value);
	}

	inline static int32_t get_offset_of_AbsoluteStringSelector_1() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___AbsoluteStringSelector_1)); }
	inline Selector_t1939762325 * get_AbsoluteStringSelector_1() const { return ___AbsoluteStringSelector_1; }
	inline Selector_t1939762325 ** get_address_of_AbsoluteStringSelector_1() { return &___AbsoluteStringSelector_1; }
	inline void set_AbsoluteStringSelector_1(Selector_t1939762325 * value)
	{
		___AbsoluteStringSelector_1 = value;
		Il2CppCodeGenWriteBarrier((&___AbsoluteStringSelector_1), value);
	}

	inline static int32_t get_offset_of_DoubleValueSelector_2() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___DoubleValueSelector_2)); }
	inline Selector_t1939762325 * get_DoubleValueSelector_2() const { return ___DoubleValueSelector_2; }
	inline Selector_t1939762325 ** get_address_of_DoubleValueSelector_2() { return &___DoubleValueSelector_2; }
	inline void set_DoubleValueSelector_2(Selector_t1939762325 * value)
	{
		___DoubleValueSelector_2 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleValueSelector_2), value);
	}

	inline static int32_t get_offset_of_CountSelector_3() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___CountSelector_3)); }
	inline Selector_t1939762325 * get_CountSelector_3() const { return ___CountSelector_3; }
	inline Selector_t1939762325 ** get_address_of_CountSelector_3() { return &___CountSelector_3; }
	inline void set_CountSelector_3(Selector_t1939762325 * value)
	{
		___CountSelector_3 = value;
		Il2CppCodeGenWriteBarrier((&___CountSelector_3), value);
	}

	inline static int32_t get_offset_of_ObjectAtIndexSelector_4() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___ObjectAtIndexSelector_4)); }
	inline Selector_t1939762325 * get_ObjectAtIndexSelector_4() const { return ___ObjectAtIndexSelector_4; }
	inline Selector_t1939762325 ** get_address_of_ObjectAtIndexSelector_4() { return &___ObjectAtIndexSelector_4; }
	inline void set_ObjectAtIndexSelector_4(Selector_t1939762325 * value)
	{
		___ObjectAtIndexSelector_4 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectAtIndexSelector_4), value);
	}

	inline static int32_t get_offset_of_AllObjectsSelector_5() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___AllObjectsSelector_5)); }
	inline Selector_t1939762325 * get_AllObjectsSelector_5() const { return ___AllObjectsSelector_5; }
	inline Selector_t1939762325 ** get_address_of_AllObjectsSelector_5() { return &___AllObjectsSelector_5; }
	inline void set_AllObjectsSelector_5(Selector_t1939762325 * value)
	{
		___AllObjectsSelector_5 = value;
		Il2CppCodeGenWriteBarrier((&___AllObjectsSelector_5), value);
	}

	inline static int32_t get_offset_of_ArrayWithObjects_CountSelector_6() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___ArrayWithObjects_CountSelector_6)); }
	inline Selector_t1939762325 * get_ArrayWithObjects_CountSelector_6() const { return ___ArrayWithObjects_CountSelector_6; }
	inline Selector_t1939762325 ** get_address_of_ArrayWithObjects_CountSelector_6() { return &___ArrayWithObjects_CountSelector_6; }
	inline void set_ArrayWithObjects_CountSelector_6(Selector_t1939762325 * value)
	{
		___ArrayWithObjects_CountSelector_6 = value;
		Il2CppCodeGenWriteBarrier((&___ArrayWithObjects_CountSelector_6), value);
	}

	inline static int32_t get_offset_of_SetWithArraySelector_7() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___SetWithArraySelector_7)); }
	inline Selector_t1939762325 * get_SetWithArraySelector_7() const { return ___SetWithArraySelector_7; }
	inline Selector_t1939762325 ** get_address_of_SetWithArraySelector_7() { return &___SetWithArraySelector_7; }
	inline void set_SetWithArraySelector_7(Selector_t1939762325 * value)
	{
		___SetWithArraySelector_7 = value;
		Il2CppCodeGenWriteBarrier((&___SetWithArraySelector_7), value);
	}

	inline static int32_t get_offset_of_InitWithCharacters_lengthSelector_8() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___InitWithCharacters_lengthSelector_8)); }
	inline Selector_t1939762325 * get_InitWithCharacters_lengthSelector_8() const { return ___InitWithCharacters_lengthSelector_8; }
	inline Selector_t1939762325 ** get_address_of_InitWithCharacters_lengthSelector_8() { return &___InitWithCharacters_lengthSelector_8; }
	inline void set_InitWithCharacters_lengthSelector_8(Selector_t1939762325 * value)
	{
		___InitWithCharacters_lengthSelector_8 = value;
		Il2CppCodeGenWriteBarrier((&___InitWithCharacters_lengthSelector_8), value);
	}

	inline static int32_t get_offset_of_URLWithStringSelector_9() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___URLWithStringSelector_9)); }
	inline Selector_t1939762325 * get_URLWithStringSelector_9() const { return ___URLWithStringSelector_9; }
	inline Selector_t1939762325 ** get_address_of_URLWithStringSelector_9() { return &___URLWithStringSelector_9; }
	inline void set_URLWithStringSelector_9(Selector_t1939762325 * value)
	{
		___URLWithStringSelector_9 = value;
		Il2CppCodeGenWriteBarrier((&___URLWithStringSelector_9), value);
	}

	inline static int32_t get_offset_of_DateWithTimeIntervalSinceReferenceDateSelector_10() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___DateWithTimeIntervalSinceReferenceDateSelector_10)); }
	inline Selector_t1939762325 * get_DateWithTimeIntervalSinceReferenceDateSelector_10() const { return ___DateWithTimeIntervalSinceReferenceDateSelector_10; }
	inline Selector_t1939762325 ** get_address_of_DateWithTimeIntervalSinceReferenceDateSelector_10() { return &___DateWithTimeIntervalSinceReferenceDateSelector_10; }
	inline void set_DateWithTimeIntervalSinceReferenceDateSelector_10(Selector_t1939762325 * value)
	{
		___DateWithTimeIntervalSinceReferenceDateSelector_10 = value;
		Il2CppCodeGenWriteBarrier((&___DateWithTimeIntervalSinceReferenceDateSelector_10), value);
	}

	inline static int32_t get_offset_of_InitWithDoubleSelector_11() { return static_cast<int32_t>(offsetof(ObjC_t468753822_StaticFields, ___InitWithDoubleSelector_11)); }
	inline Selector_t1939762325 * get_InitWithDoubleSelector_11() const { return ___InitWithDoubleSelector_11; }
	inline Selector_t1939762325 ** get_address_of_InitWithDoubleSelector_11() { return &___InitWithDoubleSelector_11; }
	inline void set_InitWithDoubleSelector_11(Selector_t1939762325 * value)
	{
		___InitWithDoubleSelector_11 = value;
		Il2CppCodeGenWriteBarrier((&___InitWithDoubleSelector_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJC_T468753822_H
#ifndef STREAM_T3255436806_H
#define STREAM_T3255436806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t3255436806  : public RuntimeObject
{
public:

public:
};

struct Stream_t3255436806_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t3255436806 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t3255436806_StaticFields, ___Null_0)); }
	inline Stream_t3255436806 * get_Null_0() const { return ___Null_0; }
	inline Stream_t3255436806 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t3255436806 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T3255436806_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef METHODS_T1187897474_H
#define METHODS_T1187897474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.Callbacks/Methods
struct  Methods_t1187897474  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODS_T1187897474_H
#ifndef RUNTIME_T1145037850_H
#define RUNTIME_T1145037850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.Runtime
struct  Runtime_t1145037850  : public RuntimeObject
{
public:

public:
};

struct Runtime_t1145037850_StaticFields
{
public:
	// System.Object ThirdParty.iOS4Unity.Runtime::_contructorLock
	RuntimeObject * ____contructorLock_0;
	// System.Object ThirdParty.iOS4Unity.Runtime::_objectLock
	RuntimeObject * ____objectLock_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Func`2<System.IntPtr,System.Object>> ThirdParty.iOS4Unity.Runtime::_constructors
	Dictionary_2_t1317859972 * ____constructors_2;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Object> ThirdParty.iOS4Unity.Runtime::_objects
	Dictionary_2_t3131474613 * ____objects_3;

public:
	inline static int32_t get_offset_of__contructorLock_0() { return static_cast<int32_t>(offsetof(Runtime_t1145037850_StaticFields, ____contructorLock_0)); }
	inline RuntimeObject * get__contructorLock_0() const { return ____contructorLock_0; }
	inline RuntimeObject ** get_address_of__contructorLock_0() { return &____contructorLock_0; }
	inline void set__contructorLock_0(RuntimeObject * value)
	{
		____contructorLock_0 = value;
		Il2CppCodeGenWriteBarrier((&____contructorLock_0), value);
	}

	inline static int32_t get_offset_of__objectLock_1() { return static_cast<int32_t>(offsetof(Runtime_t1145037850_StaticFields, ____objectLock_1)); }
	inline RuntimeObject * get__objectLock_1() const { return ____objectLock_1; }
	inline RuntimeObject ** get_address_of__objectLock_1() { return &____objectLock_1; }
	inline void set__objectLock_1(RuntimeObject * value)
	{
		____objectLock_1 = value;
		Il2CppCodeGenWriteBarrier((&____objectLock_1), value);
	}

	inline static int32_t get_offset_of__constructors_2() { return static_cast<int32_t>(offsetof(Runtime_t1145037850_StaticFields, ____constructors_2)); }
	inline Dictionary_2_t1317859972 * get__constructors_2() const { return ____constructors_2; }
	inline Dictionary_2_t1317859972 ** get_address_of__constructors_2() { return &____constructors_2; }
	inline void set__constructors_2(Dictionary_2_t1317859972 * value)
	{
		____constructors_2 = value;
		Il2CppCodeGenWriteBarrier((&____constructors_2), value);
	}

	inline static int32_t get_offset_of__objects_3() { return static_cast<int32_t>(offsetof(Runtime_t1145037850_StaticFields, ____objects_3)); }
	inline Dictionary_2_t3131474613 * get__objects_3() const { return ____objects_3; }
	inline Dictionary_2_t3131474613 ** get_address_of__objects_3() { return &____objects_3; }
	inline void set__objects_3(Dictionary_2_t3131474613 * value)
	{
		____objects_3 = value;
		Il2CppCodeGenWriteBarrier((&____objects_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIME_T1145037850_H
#ifndef U3CU3EC_T88230275_H
#define U3CU3EC_T88230275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.Runtime/<>c
struct  U3CU3Ec_t88230275  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t88230275_StaticFields
{
public:
	// ThirdParty.iOS4Unity.Runtime/<>c ThirdParty.iOS4Unity.Runtime/<>c::<>9
	U3CU3Ec_t88230275 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t88230275_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t88230275 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t88230275 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t88230275 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T88230275_H
#ifndef CALLBACKS_T3084417356_H
#define CALLBACKS_T3084417356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.Callbacks
struct  Callbacks_t3084417356  : public RuntimeObject
{
public:

public:
};

struct Callbacks_t3084417356_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Delegate> ThirdParty.iOS4Unity.Callbacks::_delegates
	Dictionary_2_t642288257 * ____delegates_0;
	// System.Collections.Generic.Dictionary`2<System.IntPtr,System.Collections.Generic.Dictionary`2<System.IntPtr,ThirdParty.iOS4Unity.Callbacks/Methods>> ThirdParty.iOS4Unity.Callbacks::_callbacks
	Dictionary_2_t2071948110 * ____callbacks_1;

public:
	inline static int32_t get_offset_of__delegates_0() { return static_cast<int32_t>(offsetof(Callbacks_t3084417356_StaticFields, ____delegates_0)); }
	inline Dictionary_2_t642288257 * get__delegates_0() const { return ____delegates_0; }
	inline Dictionary_2_t642288257 ** get_address_of__delegates_0() { return &____delegates_0; }
	inline void set__delegates_0(Dictionary_2_t642288257 * value)
	{
		____delegates_0 = value;
		Il2CppCodeGenWriteBarrier((&____delegates_0), value);
	}

	inline static int32_t get_offset_of__callbacks_1() { return static_cast<int32_t>(offsetof(Callbacks_t3084417356_StaticFields, ____callbacks_1)); }
	inline Dictionary_2_t2071948110 * get__callbacks_1() const { return ____callbacks_1; }
	inline Dictionary_2_t2071948110 ** get_address_of__callbacks_1() { return &____callbacks_1; }
	inline void set__callbacks_1(Dictionary_2_t2071948110 * value)
	{
		____callbacks_1 = value;
		Il2CppCodeGenWriteBarrier((&____callbacks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKS_T3084417356_H
#ifndef NETWORKINFO_T3319294148_H
#define NETWORKINFO_T3319294148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Storage.Internal.NetworkInfo
struct  NetworkInfo_t3319294148  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKINFO_T3319294148_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef HASHALGORITHM_T2624936259_H
#define HASHALGORITHM_T2624936259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t2624936259  : public RuntimeObject
{
public:
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_t3397334013* ___HashValue_0;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::disposed
	bool ___disposed_3;

public:
	inline static int32_t get_offset_of_HashValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t2624936259, ___HashValue_0)); }
	inline ByteU5BU5D_t3397334013* get_HashValue_0() const { return ___HashValue_0; }
	inline ByteU5BU5D_t3397334013** get_address_of_HashValue_0() { return &___HashValue_0; }
	inline void set_HashValue_0(ByteU5BU5D_t3397334013* value)
	{
		___HashValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_0), value);
	}

	inline static int32_t get_offset_of_HashSizeValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t2624936259, ___HashSizeValue_1)); }
	inline int32_t get_HashSizeValue_1() const { return ___HashSizeValue_1; }
	inline int32_t* get_address_of_HashSizeValue_1() { return &___HashSizeValue_1; }
	inline void set_HashSizeValue_1(int32_t value)
	{
		___HashSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t2624936259, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t2624936259, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T2624936259_H
#ifndef INTERNALSDKUTILS_T3074264706_H
#define INTERNALSDKUTILS_T3074264706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Internal.InternalSDKUtils
struct  InternalSDKUtils_t3074264706  : public RuntimeObject
{
public:

public:
};

struct InternalSDKUtils_t3074264706_StaticFields
{
public:
	// System.String Amazon.Util.Internal.InternalSDKUtils::_customSdkUserAgent
	String_t* ____customSdkUserAgent_0;
	// System.String Amazon.Util.Internal.InternalSDKUtils::_customData
	String_t* ____customData_1;
	// System.String Amazon.Util.Internal.InternalSDKUtils::_userAgentBaseName
	String_t* ____userAgentBaseName_2;
	// Amazon.Runtime.Internal.Util.Logger Amazon.Util.Internal.InternalSDKUtils::_logger
	Logger_t2262497814 * ____logger_3;

public:
	inline static int32_t get_offset_of__customSdkUserAgent_0() { return static_cast<int32_t>(offsetof(InternalSDKUtils_t3074264706_StaticFields, ____customSdkUserAgent_0)); }
	inline String_t* get__customSdkUserAgent_0() const { return ____customSdkUserAgent_0; }
	inline String_t** get_address_of__customSdkUserAgent_0() { return &____customSdkUserAgent_0; }
	inline void set__customSdkUserAgent_0(String_t* value)
	{
		____customSdkUserAgent_0 = value;
		Il2CppCodeGenWriteBarrier((&____customSdkUserAgent_0), value);
	}

	inline static int32_t get_offset_of__customData_1() { return static_cast<int32_t>(offsetof(InternalSDKUtils_t3074264706_StaticFields, ____customData_1)); }
	inline String_t* get__customData_1() const { return ____customData_1; }
	inline String_t** get_address_of__customData_1() { return &____customData_1; }
	inline void set__customData_1(String_t* value)
	{
		____customData_1 = value;
		Il2CppCodeGenWriteBarrier((&____customData_1), value);
	}

	inline static int32_t get_offset_of__userAgentBaseName_2() { return static_cast<int32_t>(offsetof(InternalSDKUtils_t3074264706_StaticFields, ____userAgentBaseName_2)); }
	inline String_t* get__userAgentBaseName_2() const { return ____userAgentBaseName_2; }
	inline String_t** get_address_of__userAgentBaseName_2() { return &____userAgentBaseName_2; }
	inline void set__userAgentBaseName_2(String_t* value)
	{
		____userAgentBaseName_2 = value;
		Il2CppCodeGenWriteBarrier((&____userAgentBaseName_2), value);
	}

	inline static int32_t get_offset_of__logger_3() { return static_cast<int32_t>(offsetof(InternalSDKUtils_t3074264706_StaticFields, ____logger_3)); }
	inline Logger_t2262497814 * get__logger_3() const { return ____logger_3; }
	inline Logger_t2262497814 ** get_address_of__logger_3() { return &____logger_3; }
	inline void set__logger_3(Logger_t2262497814 * value)
	{
		____logger_3 = value;
		Il2CppCodeGenWriteBarrier((&____logger_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALSDKUTILS_T3074264706_H
#ifndef CRYPTOUTIL_T1025015063_H
#define CRYPTOUTIL_T1025015063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.CryptoUtilFactory/CryptoUtil
struct  CryptoUtil_t1025015063  : public RuntimeObject
{
public:

public:
};

struct CryptoUtil_t1025015063_ThreadStaticFields
{
public:
	// System.Security.Cryptography.HashAlgorithm Amazon.Util.CryptoUtilFactory/CryptoUtil::_hashAlgorithm
	HashAlgorithm_t2624936259 * ____hashAlgorithm_0;

public:
	inline static int32_t get_offset_of__hashAlgorithm_0() { return static_cast<int32_t>(offsetof(CryptoUtil_t1025015063_ThreadStaticFields, ____hashAlgorithm_0)); }
	inline HashAlgorithm_t2624936259 * get__hashAlgorithm_0() const { return ____hashAlgorithm_0; }
	inline HashAlgorithm_t2624936259 ** get_address_of__hashAlgorithm_0() { return &____hashAlgorithm_0; }
	inline void set__hashAlgorithm_0(HashAlgorithm_t2624936259 * value)
	{
		____hashAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&____hashAlgorithm_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOUTIL_T1025015063_H
#ifndef ARRAYMETADATA_T1135078014_H
#define ARRAYMETADATA_T1135078014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.ArrayMetadata
struct  ArrayMetadata_t1135078014 
{
public:
	// System.Type ThirdParty.Json.LitJson.ArrayMetadata::element_type
	Type_t * ___element_type_0;
	// System.Boolean ThirdParty.Json.LitJson.ArrayMetadata::is_array
	bool ___is_array_1;
	// System.Boolean ThirdParty.Json.LitJson.ArrayMetadata::is_list
	bool ___is_list_2;

public:
	inline static int32_t get_offset_of_element_type_0() { return static_cast<int32_t>(offsetof(ArrayMetadata_t1135078014, ___element_type_0)); }
	inline Type_t * get_element_type_0() const { return ___element_type_0; }
	inline Type_t ** get_address_of_element_type_0() { return &___element_type_0; }
	inline void set_element_type_0(Type_t * value)
	{
		___element_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_type_0), value);
	}

	inline static int32_t get_offset_of_is_array_1() { return static_cast<int32_t>(offsetof(ArrayMetadata_t1135078014, ___is_array_1)); }
	inline bool get_is_array_1() const { return ___is_array_1; }
	inline bool* get_address_of_is_array_1() { return &___is_array_1; }
	inline void set_is_array_1(bool value)
	{
		___is_array_1 = value;
	}

	inline static int32_t get_offset_of_is_list_2() { return static_cast<int32_t>(offsetof(ArrayMetadata_t1135078014, ___is_list_2)); }
	inline bool get_is_list_2() const { return ___is_list_2; }
	inline bool* get_address_of_is_list_2() { return &___is_list_2; }
	inline void set_is_list_2(bool value)
	{
		___is_list_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ThirdParty.Json.LitJson.ArrayMetadata
struct ArrayMetadata_t1135078014_marshaled_pinvoke
{
	Type_t * ___element_type_0;
	int32_t ___is_array_1;
	int32_t ___is_list_2;
};
// Native definition for COM marshalling of ThirdParty.Json.LitJson.ArrayMetadata
struct ArrayMetadata_t1135078014_marshaled_com
{
	Type_t * ___element_type_0;
	int32_t ___is_array_1;
	int32_t ___is_list_2;
};
#endif // ARRAYMETADATA_T1135078014_H
#ifndef PROPERTYMETADATA_T3287739986_H
#define PROPERTYMETADATA_T3287739986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.PropertyMetadata
struct  PropertyMetadata_t3287739986 
{
public:
	// System.Reflection.MemberInfo ThirdParty.Json.LitJson.PropertyMetadata::Info
	MemberInfo_t * ___Info_0;
	// System.Boolean ThirdParty.Json.LitJson.PropertyMetadata::IsField
	bool ___IsField_1;
	// System.Type ThirdParty.Json.LitJson.PropertyMetadata::Type
	Type_t * ___Type_2;

public:
	inline static int32_t get_offset_of_Info_0() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3287739986, ___Info_0)); }
	inline MemberInfo_t * get_Info_0() const { return ___Info_0; }
	inline MemberInfo_t ** get_address_of_Info_0() { return &___Info_0; }
	inline void set_Info_0(MemberInfo_t * value)
	{
		___Info_0 = value;
		Il2CppCodeGenWriteBarrier((&___Info_0), value);
	}

	inline static int32_t get_offset_of_IsField_1() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3287739986, ___IsField_1)); }
	inline bool get_IsField_1() const { return ___IsField_1; }
	inline bool* get_address_of_IsField_1() { return &___IsField_1; }
	inline void set_IsField_1(bool value)
	{
		___IsField_1 = value;
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3287739986, ___Type_2)); }
	inline Type_t * get_Type_2() const { return ___Type_2; }
	inline Type_t ** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(Type_t * value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ThirdParty.Json.LitJson.PropertyMetadata
struct PropertyMetadata_t3287739986_marshaled_pinvoke
{
	MemberInfo_t * ___Info_0;
	int32_t ___IsField_1;
	Type_t * ___Type_2;
};
// Native definition for COM marshalling of ThirdParty.Json.LitJson.PropertyMetadata
struct PropertyMetadata_t3287739986_marshaled_com
{
	MemberInfo_t * ___Info_0;
	int32_t ___IsField_1;
	Type_t * ___Type_2;
};
#endif // PROPERTYMETADATA_T3287739986_H
#ifndef JSONEXCEPTION_T1457213491_H
#define JSONEXCEPTION_T1457213491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonException
struct  JsonException_t1457213491  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONEXCEPTION_T1457213491_H
#ifndef NULLABLE_1_T2088641033_H
#define NULLABLE_1_T2088641033_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t2088641033 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2088641033, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2088641033_H
#ifndef CRCCALCULATORSTREAM_T2228597532_H
#define CRCCALCULATORSTREAM_T2228597532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Ionic.Zlib.CrcCalculatorStream
struct  CrcCalculatorStream_t2228597532  : public Stream_t3255436806
{
public:
	// System.IO.Stream ThirdParty.Ionic.Zlib.CrcCalculatorStream::_InnerStream
	Stream_t3255436806 * ____InnerStream_1;
	// ThirdParty.Ionic.Zlib.CRC32 ThirdParty.Ionic.Zlib.CrcCalculatorStream::_Crc32
	CRC32_t619824607 * ____Crc32_2;
	// System.Int64 ThirdParty.Ionic.Zlib.CrcCalculatorStream::_length
	int64_t ____length_3;

public:
	inline static int32_t get_offset_of__InnerStream_1() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t2228597532, ____InnerStream_1)); }
	inline Stream_t3255436806 * get__InnerStream_1() const { return ____InnerStream_1; }
	inline Stream_t3255436806 ** get_address_of__InnerStream_1() { return &____InnerStream_1; }
	inline void set__InnerStream_1(Stream_t3255436806 * value)
	{
		____InnerStream_1 = value;
		Il2CppCodeGenWriteBarrier((&____InnerStream_1), value);
	}

	inline static int32_t get_offset_of__Crc32_2() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t2228597532, ____Crc32_2)); }
	inline CRC32_t619824607 * get__Crc32_2() const { return ____Crc32_2; }
	inline CRC32_t619824607 ** get_address_of__Crc32_2() { return &____Crc32_2; }
	inline void set__Crc32_2(CRC32_t619824607 * value)
	{
		____Crc32_2 = value;
		Il2CppCodeGenWriteBarrier((&____Crc32_2), value);
	}

	inline static int32_t get_offset_of__length_3() { return static_cast<int32_t>(offsetof(CrcCalculatorStream_t2228597532, ____length_3)); }
	inline int64_t get__length_3() const { return ____length_3; }
	inline int64_t* get_address_of__length_3() { return &____length_3; }
	inline void set__length_3(int64_t value)
	{
		____length_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRCCALCULATORSTREAM_T2228597532_H
#ifndef OBJECTMETADATA_T4058137740_H
#define OBJECTMETADATA_T4058137740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.ObjectMetadata
struct  ObjectMetadata_t4058137740 
{
public:
	// System.Type ThirdParty.Json.LitJson.ObjectMetadata::element_type
	Type_t * ___element_type_0;
	// System.Boolean ThirdParty.Json.LitJson.ObjectMetadata::is_dictionary
	bool ___is_dictionary_1;
	// System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.PropertyMetadata> ThirdParty.Json.LitJson.ObjectMetadata::properties
	RuntimeObject* ___properties_2;

public:
	inline static int32_t get_offset_of_element_type_0() { return static_cast<int32_t>(offsetof(ObjectMetadata_t4058137740, ___element_type_0)); }
	inline Type_t * get_element_type_0() const { return ___element_type_0; }
	inline Type_t ** get_address_of_element_type_0() { return &___element_type_0; }
	inline void set_element_type_0(Type_t * value)
	{
		___element_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___element_type_0), value);
	}

	inline static int32_t get_offset_of_is_dictionary_1() { return static_cast<int32_t>(offsetof(ObjectMetadata_t4058137740, ___is_dictionary_1)); }
	inline bool get_is_dictionary_1() const { return ___is_dictionary_1; }
	inline bool* get_address_of_is_dictionary_1() { return &___is_dictionary_1; }
	inline void set_is_dictionary_1(bool value)
	{
		___is_dictionary_1 = value;
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(ObjectMetadata_t4058137740, ___properties_2)); }
	inline RuntimeObject* get_properties_2() const { return ___properties_2; }
	inline RuntimeObject** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(RuntimeObject* value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier((&___properties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of ThirdParty.Json.LitJson.ObjectMetadata
struct ObjectMetadata_t4058137740_marshaled_pinvoke
{
	Type_t * ___element_type_0;
	int32_t ___is_dictionary_1;
	RuntimeObject* ___properties_2;
};
// Native definition for COM marshalling of ThirdParty.Json.LitJson.ObjectMetadata
struct ObjectMetadata_t4058137740_marshaled_com
{
	Type_t * ___element_type_0;
	int32_t ___is_dictionary_1;
	RuntimeObject* ___properties_2;
};
#endif // OBJECTMETADATA_T4058137740_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ABCDSTRUCT_T676331327_H
#define ABCDSTRUCT_T676331327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.MD5.ABCDStruct
struct  ABCDStruct_t676331327 
{
public:
	// System.UInt32 ThirdParty.MD5.ABCDStruct::A
	uint32_t ___A_0;
	// System.UInt32 ThirdParty.MD5.ABCDStruct::B
	uint32_t ___B_1;
	// System.UInt32 ThirdParty.MD5.ABCDStruct::C
	uint32_t ___C_2;
	// System.UInt32 ThirdParty.MD5.ABCDStruct::D
	uint32_t ___D_3;

public:
	inline static int32_t get_offset_of_A_0() { return static_cast<int32_t>(offsetof(ABCDStruct_t676331327, ___A_0)); }
	inline uint32_t get_A_0() const { return ___A_0; }
	inline uint32_t* get_address_of_A_0() { return &___A_0; }
	inline void set_A_0(uint32_t value)
	{
		___A_0 = value;
	}

	inline static int32_t get_offset_of_B_1() { return static_cast<int32_t>(offsetof(ABCDStruct_t676331327, ___B_1)); }
	inline uint32_t get_B_1() const { return ___B_1; }
	inline uint32_t* get_address_of_B_1() { return &___B_1; }
	inline void set_B_1(uint32_t value)
	{
		___B_1 = value;
	}

	inline static int32_t get_offset_of_C_2() { return static_cast<int32_t>(offsetof(ABCDStruct_t676331327, ___C_2)); }
	inline uint32_t get_C_2() const { return ___C_2; }
	inline uint32_t* get_address_of_C_2() { return &___C_2; }
	inline void set_C_2(uint32_t value)
	{
		___C_2 = value;
	}

	inline static int32_t get_offset_of_D_3() { return static_cast<int32_t>(offsetof(ABCDStruct_t676331327, ___D_3)); }
	inline uint32_t get_D_3() const { return ___D_3; }
	inline uint32_t* get_address_of_D_3() { return &___D_3; }
	inline void set_D_3(uint32_t value)
	{
		___D_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABCDSTRUCT_T676331327_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef TIMESPAN_T3430258949_H
#define TIMESPAN_T3430258949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t3430258949 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t3430258949_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t3430258949  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t3430258949  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t3430258949  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t3430258949  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t3430258949 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t3430258949  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t3430258949  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t3430258949 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t3430258949  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t3430258949_StaticFields, ___Zero_7)); }
	inline TimeSpan_t3430258949  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t3430258949 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t3430258949  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T3430258949_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef NULLABLE_1_T334943763_H
#define NULLABLE_1_T334943763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t334943763 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t334943763, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t334943763, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T334943763_H
#ifndef AWSSECTION_T3096528870_H
#define AWSSECTION_T3096528870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSSection
struct  AWSSection_t3096528870  : public RuntimeObject
{
public:
	// Amazon.LoggingSection Amazon.AWSSection::<Logging>k__BackingField
	LoggingSection_t905443946 * ___U3CLoggingU3Ek__BackingField_0;
	// System.String Amazon.AWSSection::<EndpointDefinition>k__BackingField
	String_t* ___U3CEndpointDefinitionU3Ek__BackingField_1;
	// System.String Amazon.AWSSection::<Region>k__BackingField
	String_t* ___U3CRegionU3Ek__BackingField_2;
	// System.Nullable`1<System.Boolean> Amazon.AWSSection::<UseSdkCache>k__BackingField
	Nullable_1_t2088641033  ___U3CUseSdkCacheU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> Amazon.AWSSection::<CorrectForClockSkew>k__BackingField
	Nullable_1_t2088641033  ___U3CCorrectForClockSkewU3Ek__BackingField_4;
	// Amazon.ProxySection Amazon.AWSSection::<Proxy>k__BackingField
	ProxySection_t4280332351 * ___U3CProxyU3Ek__BackingField_5;
	// System.String Amazon.AWSSection::<ProfileName>k__BackingField
	String_t* ___U3CProfileNameU3Ek__BackingField_6;
	// System.String Amazon.AWSSection::<ProfilesLocation>k__BackingField
	String_t* ___U3CProfilesLocationU3Ek__BackingField_7;
	// System.String Amazon.AWSSection::<ApplicationName>k__BackingField
	String_t* ___U3CApplicationNameU3Ek__BackingField_8;
	// System.Collections.Generic.IDictionary`2<System.String,System.Xml.Linq.XElement> Amazon.AWSSection::_serviceSections
	RuntimeObject* ____serviceSections_9;

public:
	inline static int32_t get_offset_of_U3CLoggingU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CLoggingU3Ek__BackingField_0)); }
	inline LoggingSection_t905443946 * get_U3CLoggingU3Ek__BackingField_0() const { return ___U3CLoggingU3Ek__BackingField_0; }
	inline LoggingSection_t905443946 ** get_address_of_U3CLoggingU3Ek__BackingField_0() { return &___U3CLoggingU3Ek__BackingField_0; }
	inline void set_U3CLoggingU3Ek__BackingField_0(LoggingSection_t905443946 * value)
	{
		___U3CLoggingU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoggingU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CEndpointDefinitionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CEndpointDefinitionU3Ek__BackingField_1)); }
	inline String_t* get_U3CEndpointDefinitionU3Ek__BackingField_1() const { return ___U3CEndpointDefinitionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CEndpointDefinitionU3Ek__BackingField_1() { return &___U3CEndpointDefinitionU3Ek__BackingField_1; }
	inline void set_U3CEndpointDefinitionU3Ek__BackingField_1(String_t* value)
	{
		___U3CEndpointDefinitionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEndpointDefinitionU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRegionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CRegionU3Ek__BackingField_2)); }
	inline String_t* get_U3CRegionU3Ek__BackingField_2() const { return ___U3CRegionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRegionU3Ek__BackingField_2() { return &___U3CRegionU3Ek__BackingField_2; }
	inline void set_U3CRegionU3Ek__BackingField_2(String_t* value)
	{
		___U3CRegionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRegionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CUseSdkCacheU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CUseSdkCacheU3Ek__BackingField_3)); }
	inline Nullable_1_t2088641033  get_U3CUseSdkCacheU3Ek__BackingField_3() const { return ___U3CUseSdkCacheU3Ek__BackingField_3; }
	inline Nullable_1_t2088641033 * get_address_of_U3CUseSdkCacheU3Ek__BackingField_3() { return &___U3CUseSdkCacheU3Ek__BackingField_3; }
	inline void set_U3CUseSdkCacheU3Ek__BackingField_3(Nullable_1_t2088641033  value)
	{
		___U3CUseSdkCacheU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCorrectForClockSkewU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CCorrectForClockSkewU3Ek__BackingField_4)); }
	inline Nullable_1_t2088641033  get_U3CCorrectForClockSkewU3Ek__BackingField_4() const { return ___U3CCorrectForClockSkewU3Ek__BackingField_4; }
	inline Nullable_1_t2088641033 * get_address_of_U3CCorrectForClockSkewU3Ek__BackingField_4() { return &___U3CCorrectForClockSkewU3Ek__BackingField_4; }
	inline void set_U3CCorrectForClockSkewU3Ek__BackingField_4(Nullable_1_t2088641033  value)
	{
		___U3CCorrectForClockSkewU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CProxyU3Ek__BackingField_5)); }
	inline ProxySection_t4280332351 * get_U3CProxyU3Ek__BackingField_5() const { return ___U3CProxyU3Ek__BackingField_5; }
	inline ProxySection_t4280332351 ** get_address_of_U3CProxyU3Ek__BackingField_5() { return &___U3CProxyU3Ek__BackingField_5; }
	inline void set_U3CProxyU3Ek__BackingField_5(ProxySection_t4280332351 * value)
	{
		___U3CProxyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CProfileNameU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CProfileNameU3Ek__BackingField_6)); }
	inline String_t* get_U3CProfileNameU3Ek__BackingField_6() const { return ___U3CProfileNameU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CProfileNameU3Ek__BackingField_6() { return &___U3CProfileNameU3Ek__BackingField_6; }
	inline void set_U3CProfileNameU3Ek__BackingField_6(String_t* value)
	{
		___U3CProfileNameU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProfileNameU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CProfilesLocationU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CProfilesLocationU3Ek__BackingField_7)); }
	inline String_t* get_U3CProfilesLocationU3Ek__BackingField_7() const { return ___U3CProfilesLocationU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CProfilesLocationU3Ek__BackingField_7() { return &___U3CProfilesLocationU3Ek__BackingField_7; }
	inline void set_U3CProfilesLocationU3Ek__BackingField_7(String_t* value)
	{
		___U3CProfilesLocationU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProfilesLocationU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CApplicationNameU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ___U3CApplicationNameU3Ek__BackingField_8)); }
	inline String_t* get_U3CApplicationNameU3Ek__BackingField_8() const { return ___U3CApplicationNameU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CApplicationNameU3Ek__BackingField_8() { return &___U3CApplicationNameU3Ek__BackingField_8; }
	inline void set_U3CApplicationNameU3Ek__BackingField_8(String_t* value)
	{
		___U3CApplicationNameU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CApplicationNameU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of__serviceSections_9() { return static_cast<int32_t>(offsetof(AWSSection_t3096528870, ____serviceSections_9)); }
	inline RuntimeObject* get__serviceSections_9() const { return ____serviceSections_9; }
	inline RuntimeObject** get_address_of__serviceSections_9() { return &____serviceSections_9; }
	inline void set__serviceSections_9(RuntimeObject* value)
	{
		____serviceSections_9 = value;
		Il2CppCodeGenWriteBarrier((&____serviceSections_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AWSSECTION_T3096528870_H
#ifndef DATETIMEKIND_T2186819611_H
#define DATETIMEKIND_T2186819611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t2186819611 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t2186819611, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T2186819611_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef NETWORKREACHABILITY_T1092747145_H
#define NETWORKREACHABILITY_T1092747145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.NetworkReachability
struct  NetworkReachability_t1092747145 
{
public:
	// System.Int32 UnityEngine.NetworkReachability::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NetworkReachability_t1092747145, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKREACHABILITY_T1092747145_H
#ifndef HTTPCLIENTOPTION_T4250830711_H
#define HTTPCLIENTOPTION_T4250830711_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSConfigs/HttpClientOption
struct  HttpClientOption_t4250830711 
{
public:
	// System.Int32 Amazon.AWSConfigs/HttpClientOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpClientOption_t4250830711, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCLIENTOPTION_T4250830711_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef LOGGINGOPTIONS_T2865640765_H
#define LOGGINGOPTIONS_T2865640765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.LoggingOptions
struct  LoggingOptions_t2865640765 
{
public:
	// System.Int32 Amazon.LoggingOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoggingOptions_t2865640765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGINGOPTIONS_T2865640765_H
#ifndef LOGMETRICSFORMATOPTION_T97749509_H
#define LOGMETRICSFORMATOPTION_T97749509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.LogMetricsFormatOption
struct  LogMetricsFormatOption_t97749509 
{
public:
	// System.Int32 Amazon.LogMetricsFormatOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogMetricsFormatOption_t97749509, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGMETRICSFORMATOPTION_T97749509_H
#ifndef RESPONSELOGGINGOPTION_T3443611737_H
#define RESPONSELOGGINGOPTION_T3443611737_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.ResponseLoggingOption
struct  ResponseLoggingOption_t3443611737 
{
public:
	// System.Int32 Amazon.ResponseLoggingOption::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ResponseLoggingOption_t3443611737, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSELOGGINGOPTION_T3443611737_H
#ifndef INVALIDDATAEXCEPTION_T1057132120_H
#define INVALIDDATAEXCEPTION_T1057132120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.InvalidDataException
struct  InvalidDataException_t1057132120  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDDATAEXCEPTION_T1057132120_H
#ifndef MD5MANAGED_T1233897219_H
#define MD5MANAGED_T1233897219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.MD5.MD5Managed
struct  MD5Managed_t1233897219  : public HashAlgorithm_t2624936259
{
public:
	// System.Byte[] ThirdParty.MD5.MD5Managed::_data
	ByteU5BU5D_t3397334013* ____data_4;
	// ThirdParty.MD5.ABCDStruct ThirdParty.MD5.MD5Managed::_abcd
	ABCDStruct_t676331327  ____abcd_5;
	// System.Int64 ThirdParty.MD5.MD5Managed::_totalLength
	int64_t ____totalLength_6;
	// System.Int32 ThirdParty.MD5.MD5Managed::_dataSize
	int32_t ____dataSize_7;

public:
	inline static int32_t get_offset_of__data_4() { return static_cast<int32_t>(offsetof(MD5Managed_t1233897219, ____data_4)); }
	inline ByteU5BU5D_t3397334013* get__data_4() const { return ____data_4; }
	inline ByteU5BU5D_t3397334013** get_address_of__data_4() { return &____data_4; }
	inline void set__data_4(ByteU5BU5D_t3397334013* value)
	{
		____data_4 = value;
		Il2CppCodeGenWriteBarrier((&____data_4), value);
	}

	inline static int32_t get_offset_of__abcd_5() { return static_cast<int32_t>(offsetof(MD5Managed_t1233897219, ____abcd_5)); }
	inline ABCDStruct_t676331327  get__abcd_5() const { return ____abcd_5; }
	inline ABCDStruct_t676331327 * get_address_of__abcd_5() { return &____abcd_5; }
	inline void set__abcd_5(ABCDStruct_t676331327  value)
	{
		____abcd_5 = value;
	}

	inline static int32_t get_offset_of__totalLength_6() { return static_cast<int32_t>(offsetof(MD5Managed_t1233897219, ____totalLength_6)); }
	inline int64_t get__totalLength_6() const { return ____totalLength_6; }
	inline int64_t* get_address_of__totalLength_6() { return &____totalLength_6; }
	inline void set__totalLength_6(int64_t value)
	{
		____totalLength_6 = value;
	}

	inline static int32_t get_offset_of__dataSize_7() { return static_cast<int32_t>(offsetof(MD5Managed_t1233897219, ____dataSize_7)); }
	inline int32_t get__dataSize_7() const { return ____dataSize_7; }
	inline int32_t* get_address_of__dataSize_7() { return &____dataSize_7; }
	inline void set__dataSize_7(int32_t value)
	{
		____dataSize_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5MANAGED_T1233897219_H
#ifndef JSONTYPE_T808352724_H
#define JSONTYPE_T808352724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonType
struct  JsonType_t808352724 
{
public:
	// System.Int32 ThirdParty.Json.LitJson.JsonType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonType_t808352724, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTYPE_T808352724_H
#ifndef LIBRARIES_T4218932343_H
#define LIBRARIES_T4218932343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.ObjC/Libraries
struct  Libraries_t4218932343  : public RuntimeObject
{
public:

public:
};

struct Libraries_t4218932343_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.ObjC/Libraries::Foundation
	intptr_t ___Foundation_0;
	// System.IntPtr ThirdParty.iOS4Unity.ObjC/Libraries::UIKit
	intptr_t ___UIKit_1;

public:
	inline static int32_t get_offset_of_Foundation_0() { return static_cast<int32_t>(offsetof(Libraries_t4218932343_StaticFields, ___Foundation_0)); }
	inline intptr_t get_Foundation_0() const { return ___Foundation_0; }
	inline intptr_t* get_address_of_Foundation_0() { return &___Foundation_0; }
	inline void set_Foundation_0(intptr_t value)
	{
		___Foundation_0 = value;
	}

	inline static int32_t get_offset_of_UIKit_1() { return static_cast<int32_t>(offsetof(Libraries_t4218932343_StaticFields, ___UIKit_1)); }
	inline intptr_t get_UIKit_1() const { return ___UIKit_1; }
	inline intptr_t* get_address_of_UIKit_1() { return &___UIKit_1; }
	inline void set_UIKit_1(intptr_t value)
	{
		___UIKit_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIBRARIES_T4218932343_H
#ifndef SELECTOR_T1939762325_H
#define SELECTOR_T1939762325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.Selector
struct  Selector_t1939762325  : public RuntimeObject
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.Selector::handle
	intptr_t ___handle_15;

public:
	inline static int32_t get_offset_of_handle_15() { return static_cast<int32_t>(offsetof(Selector_t1939762325, ___handle_15)); }
	inline intptr_t get_handle_15() const { return ___handle_15; }
	inline intptr_t* get_address_of_handle_15() { return &___handle_15; }
	inline void set_handle_15(intptr_t value)
	{
		___handle_15 = value;
	}
};

struct Selector_t1939762325_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.Selector::Init
	intptr_t ___Init_0;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::InitWithCoder
	intptr_t ___InitWithCoder_1;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::InitWithName
	intptr_t ___InitWithName_2;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::InitWithFrame
	intptr_t ___InitWithFrame_3;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::MethodSignatureForSelector
	intptr_t ___MethodSignatureForSelector_4;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::FrameLength
	intptr_t ___FrameLength_5;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::RetainCount
	intptr_t ___RetainCount_6;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::AllocHandle
	intptr_t ___AllocHandle_7;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::ReleaseHandle
	intptr_t ___ReleaseHandle_8;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::RetainHandle
	intptr_t ___RetainHandle_9;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::AutoreleaseHandle
	intptr_t ___AutoreleaseHandle_10;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::DoesNotRecognizeSelectorHandle
	intptr_t ___DoesNotRecognizeSelectorHandle_11;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle
	intptr_t ___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::PerformSelectorWithObjectAfterDelayHandle
	intptr_t ___PerformSelectorWithObjectAfterDelayHandle_13;
	// System.IntPtr ThirdParty.iOS4Unity.Selector::UTF8StringHandle
	intptr_t ___UTF8StringHandle_14;

public:
	inline static int32_t get_offset_of_Init_0() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___Init_0)); }
	inline intptr_t get_Init_0() const { return ___Init_0; }
	inline intptr_t* get_address_of_Init_0() { return &___Init_0; }
	inline void set_Init_0(intptr_t value)
	{
		___Init_0 = value;
	}

	inline static int32_t get_offset_of_InitWithCoder_1() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___InitWithCoder_1)); }
	inline intptr_t get_InitWithCoder_1() const { return ___InitWithCoder_1; }
	inline intptr_t* get_address_of_InitWithCoder_1() { return &___InitWithCoder_1; }
	inline void set_InitWithCoder_1(intptr_t value)
	{
		___InitWithCoder_1 = value;
	}

	inline static int32_t get_offset_of_InitWithName_2() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___InitWithName_2)); }
	inline intptr_t get_InitWithName_2() const { return ___InitWithName_2; }
	inline intptr_t* get_address_of_InitWithName_2() { return &___InitWithName_2; }
	inline void set_InitWithName_2(intptr_t value)
	{
		___InitWithName_2 = value;
	}

	inline static int32_t get_offset_of_InitWithFrame_3() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___InitWithFrame_3)); }
	inline intptr_t get_InitWithFrame_3() const { return ___InitWithFrame_3; }
	inline intptr_t* get_address_of_InitWithFrame_3() { return &___InitWithFrame_3; }
	inline void set_InitWithFrame_3(intptr_t value)
	{
		___InitWithFrame_3 = value;
	}

	inline static int32_t get_offset_of_MethodSignatureForSelector_4() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___MethodSignatureForSelector_4)); }
	inline intptr_t get_MethodSignatureForSelector_4() const { return ___MethodSignatureForSelector_4; }
	inline intptr_t* get_address_of_MethodSignatureForSelector_4() { return &___MethodSignatureForSelector_4; }
	inline void set_MethodSignatureForSelector_4(intptr_t value)
	{
		___MethodSignatureForSelector_4 = value;
	}

	inline static int32_t get_offset_of_FrameLength_5() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___FrameLength_5)); }
	inline intptr_t get_FrameLength_5() const { return ___FrameLength_5; }
	inline intptr_t* get_address_of_FrameLength_5() { return &___FrameLength_5; }
	inline void set_FrameLength_5(intptr_t value)
	{
		___FrameLength_5 = value;
	}

	inline static int32_t get_offset_of_RetainCount_6() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___RetainCount_6)); }
	inline intptr_t get_RetainCount_6() const { return ___RetainCount_6; }
	inline intptr_t* get_address_of_RetainCount_6() { return &___RetainCount_6; }
	inline void set_RetainCount_6(intptr_t value)
	{
		___RetainCount_6 = value;
	}

	inline static int32_t get_offset_of_AllocHandle_7() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___AllocHandle_7)); }
	inline intptr_t get_AllocHandle_7() const { return ___AllocHandle_7; }
	inline intptr_t* get_address_of_AllocHandle_7() { return &___AllocHandle_7; }
	inline void set_AllocHandle_7(intptr_t value)
	{
		___AllocHandle_7 = value;
	}

	inline static int32_t get_offset_of_ReleaseHandle_8() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___ReleaseHandle_8)); }
	inline intptr_t get_ReleaseHandle_8() const { return ___ReleaseHandle_8; }
	inline intptr_t* get_address_of_ReleaseHandle_8() { return &___ReleaseHandle_8; }
	inline void set_ReleaseHandle_8(intptr_t value)
	{
		___ReleaseHandle_8 = value;
	}

	inline static int32_t get_offset_of_RetainHandle_9() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___RetainHandle_9)); }
	inline intptr_t get_RetainHandle_9() const { return ___RetainHandle_9; }
	inline intptr_t* get_address_of_RetainHandle_9() { return &___RetainHandle_9; }
	inline void set_RetainHandle_9(intptr_t value)
	{
		___RetainHandle_9 = value;
	}

	inline static int32_t get_offset_of_AutoreleaseHandle_10() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___AutoreleaseHandle_10)); }
	inline intptr_t get_AutoreleaseHandle_10() const { return ___AutoreleaseHandle_10; }
	inline intptr_t* get_address_of_AutoreleaseHandle_10() { return &___AutoreleaseHandle_10; }
	inline void set_AutoreleaseHandle_10(intptr_t value)
	{
		___AutoreleaseHandle_10 = value;
	}

	inline static int32_t get_offset_of_DoesNotRecognizeSelectorHandle_11() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___DoesNotRecognizeSelectorHandle_11)); }
	inline intptr_t get_DoesNotRecognizeSelectorHandle_11() const { return ___DoesNotRecognizeSelectorHandle_11; }
	inline intptr_t* get_address_of_DoesNotRecognizeSelectorHandle_11() { return &___DoesNotRecognizeSelectorHandle_11; }
	inline void set_DoesNotRecognizeSelectorHandle_11(intptr_t value)
	{
		___DoesNotRecognizeSelectorHandle_11 = value;
	}

	inline static int32_t get_offset_of_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12)); }
	inline intptr_t get_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12() const { return ___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12; }
	inline intptr_t* get_address_of_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12() { return &___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12; }
	inline void set_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12(intptr_t value)
	{
		___PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12 = value;
	}

	inline static int32_t get_offset_of_PerformSelectorWithObjectAfterDelayHandle_13() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___PerformSelectorWithObjectAfterDelayHandle_13)); }
	inline intptr_t get_PerformSelectorWithObjectAfterDelayHandle_13() const { return ___PerformSelectorWithObjectAfterDelayHandle_13; }
	inline intptr_t* get_address_of_PerformSelectorWithObjectAfterDelayHandle_13() { return &___PerformSelectorWithObjectAfterDelayHandle_13; }
	inline void set_PerformSelectorWithObjectAfterDelayHandle_13(intptr_t value)
	{
		___PerformSelectorWithObjectAfterDelayHandle_13 = value;
	}

	inline static int32_t get_offset_of_UTF8StringHandle_14() { return static_cast<int32_t>(offsetof(Selector_t1939762325_StaticFields, ___UTF8StringHandle_14)); }
	inline intptr_t get_UTF8StringHandle_14() const { return ___UTF8StringHandle_14; }
	inline intptr_t* get_address_of_UTF8StringHandle_14() { return &___UTF8StringHandle_14; }
	inline void set_UTF8StringHandle_14(intptr_t value)
	{
		___UTF8StringHandle_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SELECTOR_T1939762325_H
#ifndef NSOBJECT_T1518098886_H
#define NSOBJECT_T1518098886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSObject
struct  NSObject_t1518098886  : public RuntimeObject
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSObject::Handle
	intptr_t ___Handle_1;
	// System.Boolean ThirdParty.iOS4Unity.NSObject::_shouldRelease
	bool ____shouldRelease_2;

public:
	inline static int32_t get_offset_of_Handle_1() { return static_cast<int32_t>(offsetof(NSObject_t1518098886, ___Handle_1)); }
	inline intptr_t get_Handle_1() const { return ___Handle_1; }
	inline intptr_t* get_address_of_Handle_1() { return &___Handle_1; }
	inline void set_Handle_1(intptr_t value)
	{
		___Handle_1 = value;
	}

	inline static int32_t get_offset_of__shouldRelease_2() { return static_cast<int32_t>(offsetof(NSObject_t1518098886, ____shouldRelease_2)); }
	inline bool get__shouldRelease_2() const { return ____shouldRelease_2; }
	inline bool* get_address_of__shouldRelease_2() { return &____shouldRelease_2; }
	inline void set__shouldRelease_2(bool value)
	{
		____shouldRelease_2 = value;
	}
};

struct NSObject_t1518098886_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSObject::_classHandle
	intptr_t ____classHandle_0;

public:
	inline static int32_t get_offset_of__classHandle_0() { return static_cast<int32_t>(offsetof(NSObject_t1518098886_StaticFields, ____classHandle_0)); }
	inline intptr_t get__classHandle_0() const { return ____classHandle_0; }
	inline intptr_t* get_address_of__classHandle_0() { return &____classHandle_0; }
	inline void set__classHandle_0(intptr_t value)
	{
		____classHandle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSOBJECT_T1518098886_H
#ifndef CONDITION_T2230619687_H
#define CONDITION_T2230619687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.Condition
struct  Condition_t2230619687 
{
public:
	// System.Int32 ThirdParty.Json.LitJson.Condition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Condition_t2230619687, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITION_T2230619687_H
#ifndef JSONTOKEN_T2445581255_H
#define JSONTOKEN_T2445581255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonToken
struct  JsonToken_t2445581255 
{
public:
	// System.Int32 ThirdParty.Json.LitJson.JsonToken::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JsonToken_t2445581255, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T2445581255_H
#ifndef SKPAYMENT_T4242751664_H
#define SKPAYMENT_T4242751664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.SKPayment
struct  SKPayment_t4242751664  : public NSObject_t1518098886
{
public:

public:
};

struct SKPayment_t4242751664_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.SKPayment::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(SKPayment_t4242751664_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKPAYMENT_T4242751664_H
#ifndef SKPAYMENTQUEUE_T538265951_H
#define SKPAYMENTQUEUE_T538265951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.SKPaymentQueue
struct  SKPaymentQueue_t538265951  : public NSObject_t1518098886
{
public:

public:
};

struct SKPaymentQueue_t538265951_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.SKPaymentQueue::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(SKPaymentQueue_t538265951_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKPAYMENTQUEUE_T538265951_H
#ifndef NSNUMBERFORMATTER_T3902210636_H
#define NSNUMBERFORMATTER_T3902210636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSNumberFormatter
struct  NSNumberFormatter_t3902210636  : public NSObject_t1518098886
{
public:

public:
};

struct NSNumberFormatter_t3902210636_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSNumberFormatter::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(NSNumberFormatter_t3902210636_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSNUMBERFORMATTER_T3902210636_H
#ifndef NSTIMEZONE_T416177812_H
#define NSTIMEZONE_T416177812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSTimeZone
struct  NSTimeZone_t416177812  : public NSObject_t1518098886
{
public:

public:
};

struct NSTimeZone_t416177812_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSTimeZone::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(NSTimeZone_t416177812_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSTIMEZONE_T416177812_H
#ifndef SKPAYMENTTRANSACTION_T2918057744_H
#define SKPAYMENTTRANSACTION_T2918057744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.SKPaymentTransaction
struct  SKPaymentTransaction_t2918057744  : public NSObject_t1518098886
{
public:

public:
};

struct SKPaymentTransaction_t2918057744_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.SKPaymentTransaction::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(SKPaymentTransaction_t2918057744_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKPAYMENTTRANSACTION_T2918057744_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_T2945196168_H
#define U3CU3EC__DISPLAYCLASS1_0_T2945196168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.Storage.Internal.NetworkInfo/<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t2945196168  : public RuntimeObject
{
public:
	// UnityEngine.NetworkReachability Amazon.Util.Storage.Internal.NetworkInfo/<>c__DisplayClass1_0::_networkReachability
	int32_t ____networkReachability_0;
	// System.Threading.AutoResetEvent Amazon.Util.Storage.Internal.NetworkInfo/<>c__DisplayClass1_0::asyncEvent
	AutoResetEvent_t15112628 * ___asyncEvent_1;

public:
	inline static int32_t get_offset_of__networkReachability_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t2945196168, ____networkReachability_0)); }
	inline int32_t get__networkReachability_0() const { return ____networkReachability_0; }
	inline int32_t* get_address_of__networkReachability_0() { return &____networkReachability_0; }
	inline void set__networkReachability_0(int32_t value)
	{
		____networkReachability_0 = value;
	}

	inline static int32_t get_offset_of_asyncEvent_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t2945196168, ___asyncEvent_1)); }
	inline AutoResetEvent_t15112628 * get_asyncEvent_1() const { return ___asyncEvent_1; }
	inline AutoResetEvent_t15112628 ** get_address_of_asyncEvent_1() { return &___asyncEvent_1; }
	inline void set_asyncEvent_1(AutoResetEvent_t15112628 * value)
	{
		___asyncEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___asyncEvent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_T2945196168_H
#ifndef SKPRODUCTSREQUEST_T3969930329_H
#define SKPRODUCTSREQUEST_T3969930329_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.SKProductsRequest
struct  SKProductsRequest_t3969930329  : public NSObject_t1518098886
{
public:

public:
};

struct SKProductsRequest_t3969930329_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.SKProductsRequest::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(SKProductsRequest_t3969930329_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKPRODUCTSREQUEST_T3969930329_H
#ifndef SKPRODUCTSRESPONSE_T3540529521_H
#define SKPRODUCTSRESPONSE_T3540529521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.SKProductsResponse
struct  SKProductsResponse_t3540529521  : public NSObject_t1518098886
{
public:

public:
};

struct SKProductsResponse_t3540529521_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.SKProductsResponse::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(SKProductsResponse_t3540529521_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKPRODUCTSRESPONSE_T3540529521_H
#ifndef SKPRODUCT_T3309436227_H
#define SKPRODUCT_T3309436227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.SKProduct
struct  SKProduct_t3309436227  : public NSObject_t1518098886
{
public:

public:
};

struct SKProduct_t3309436227_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.SKProduct::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(SKProduct_t3309436227_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKPRODUCT_T3309436227_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef NSDATA_T1166097363_H
#define NSDATA_T1166097363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSData
struct  NSData_t1166097363  : public NSObject_t1518098886
{
public:

public:
};

struct NSData_t1166097363_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSData::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(NSData_t1166097363_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSDATA_T1166097363_H
#ifndef DATETIME_T693205669_H
#define DATETIME_T693205669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t693205669 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t3430258949  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___ticks_10)); }
	inline TimeSpan_t3430258949  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t3430258949 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t3430258949  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t693205669, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t693205669_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t693205669  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t693205669  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1642385972* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1642385972* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1642385972* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1642385972* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1642385972* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1642385972* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1642385972* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t3030399641* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t3030399641* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MaxValue_12)); }
	inline DateTime_t693205669  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t693205669 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t693205669  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MinValue_13)); }
	inline DateTime_t693205669  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t693205669 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t693205669  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1642385972* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1642385972* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1642385972* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1642385972* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1642385972* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1642385972* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1642385972* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1642385972* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1642385972* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1642385972** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1642385972* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1642385972* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1642385972** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1642385972* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1642385972* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1642385972** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1642385972* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t3030399641* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t3030399641* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t3030399641* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t3030399641** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t3030399641* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t693205669_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T693205669_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef NSBUNDLE_T3599697655_H
#define NSBUNDLE_T3599697655_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSBundle
struct  NSBundle_t3599697655  : public NSObject_t1518098886
{
public:

public:
};

struct NSBundle_t3599697655_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSBundle::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(NSBundle_t3599697655_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSBUNDLE_T3599697655_H
#ifndef NSDICTIONARY_T3598984691_H
#define NSDICTIONARY_T3598984691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSDictionary
struct  NSDictionary_t3598984691  : public NSObject_t1518098886
{
public:

public:
};

struct NSDictionary_t3598984691_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSDictionary::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(NSDictionary_t3598984691_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSDICTIONARY_T3598984691_H
#ifndef NSNOTIFICATION_T1437913478_H
#define NSNOTIFICATION_T1437913478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSNotification
struct  NSNotification_t1437913478  : public NSObject_t1518098886
{
public:

public:
};

struct NSNotification_t1437913478_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSNotification::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(NSNotification_t1437913478_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSNOTIFICATION_T1437913478_H
#ifndef NSNOTIFICATIONCENTER_T3366908717_H
#define NSNOTIFICATIONCENTER_T3366908717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSNotificationCenter
struct  NSNotificationCenter_t3366908717  : public NSObject_t1518098886
{
public:

public:
};

struct NSNotificationCenter_t3366908717_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSNotificationCenter::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(NSNotificationCenter_t3366908717_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSNOTIFICATIONCENTER_T3366908717_H
#ifndef NSERROR_T1370871221_H
#define NSERROR_T1370871221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSError
struct  NSError_t1370871221  : public NSObject_t1518098886
{
public:

public:
};

struct NSError_t1370871221_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSError::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(NSError_t1370871221_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSERROR_T1370871221_H
#ifndef NSLOCALE_T2224424797_H
#define NSLOCALE_T2224424797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSLocale
struct  NSLocale_t2224424797  : public NSObject_t1518098886
{
public:

public:
};

struct NSLocale_t2224424797_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSLocale::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(NSLocale_t2224424797_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSLOCALE_T2224424797_H
#ifndef JSONREADER_T354941621_H
#define JSONREADER_T354941621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonReader
struct  JsonReader_t354941621  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<ThirdParty.Json.LitJson.JsonToken> ThirdParty.Json.LitJson.JsonReader::depth
	Stack_1_t3533309409 * ___depth_0;
	// System.Int32 ThirdParty.Json.LitJson.JsonReader::current_input
	int32_t ___current_input_1;
	// System.Int32 ThirdParty.Json.LitJson.JsonReader::current_symbol
	int32_t ___current_symbol_2;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::end_of_json
	bool ___end_of_json_3;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::end_of_input
	bool ___end_of_input_4;
	// ThirdParty.Json.LitJson.Lexer ThirdParty.Json.LitJson.JsonReader::lexer
	Lexer_t954714164 * ___lexer_5;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::parser_in_string
	bool ___parser_in_string_6;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::parser_return
	bool ___parser_return_7;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::read_started
	bool ___read_started_8;
	// System.IO.TextReader ThirdParty.Json.LitJson.JsonReader::reader
	TextReader_t1561828458 * ___reader_9;
	// System.Boolean ThirdParty.Json.LitJson.JsonReader::reader_is_owned
	bool ___reader_is_owned_10;
	// System.Object ThirdParty.Json.LitJson.JsonReader::token_value
	RuntimeObject * ___token_value_11;
	// ThirdParty.Json.LitJson.JsonToken ThirdParty.Json.LitJson.JsonReader::token
	int32_t ___token_12;

public:
	inline static int32_t get_offset_of_depth_0() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___depth_0)); }
	inline Stack_1_t3533309409 * get_depth_0() const { return ___depth_0; }
	inline Stack_1_t3533309409 ** get_address_of_depth_0() { return &___depth_0; }
	inline void set_depth_0(Stack_1_t3533309409 * value)
	{
		___depth_0 = value;
		Il2CppCodeGenWriteBarrier((&___depth_0), value);
	}

	inline static int32_t get_offset_of_current_input_1() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___current_input_1)); }
	inline int32_t get_current_input_1() const { return ___current_input_1; }
	inline int32_t* get_address_of_current_input_1() { return &___current_input_1; }
	inline void set_current_input_1(int32_t value)
	{
		___current_input_1 = value;
	}

	inline static int32_t get_offset_of_current_symbol_2() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___current_symbol_2)); }
	inline int32_t get_current_symbol_2() const { return ___current_symbol_2; }
	inline int32_t* get_address_of_current_symbol_2() { return &___current_symbol_2; }
	inline void set_current_symbol_2(int32_t value)
	{
		___current_symbol_2 = value;
	}

	inline static int32_t get_offset_of_end_of_json_3() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___end_of_json_3)); }
	inline bool get_end_of_json_3() const { return ___end_of_json_3; }
	inline bool* get_address_of_end_of_json_3() { return &___end_of_json_3; }
	inline void set_end_of_json_3(bool value)
	{
		___end_of_json_3 = value;
	}

	inline static int32_t get_offset_of_end_of_input_4() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___end_of_input_4)); }
	inline bool get_end_of_input_4() const { return ___end_of_input_4; }
	inline bool* get_address_of_end_of_input_4() { return &___end_of_input_4; }
	inline void set_end_of_input_4(bool value)
	{
		___end_of_input_4 = value;
	}

	inline static int32_t get_offset_of_lexer_5() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___lexer_5)); }
	inline Lexer_t954714164 * get_lexer_5() const { return ___lexer_5; }
	inline Lexer_t954714164 ** get_address_of_lexer_5() { return &___lexer_5; }
	inline void set_lexer_5(Lexer_t954714164 * value)
	{
		___lexer_5 = value;
		Il2CppCodeGenWriteBarrier((&___lexer_5), value);
	}

	inline static int32_t get_offset_of_parser_in_string_6() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___parser_in_string_6)); }
	inline bool get_parser_in_string_6() const { return ___parser_in_string_6; }
	inline bool* get_address_of_parser_in_string_6() { return &___parser_in_string_6; }
	inline void set_parser_in_string_6(bool value)
	{
		___parser_in_string_6 = value;
	}

	inline static int32_t get_offset_of_parser_return_7() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___parser_return_7)); }
	inline bool get_parser_return_7() const { return ___parser_return_7; }
	inline bool* get_address_of_parser_return_7() { return &___parser_return_7; }
	inline void set_parser_return_7(bool value)
	{
		___parser_return_7 = value;
	}

	inline static int32_t get_offset_of_read_started_8() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___read_started_8)); }
	inline bool get_read_started_8() const { return ___read_started_8; }
	inline bool* get_address_of_read_started_8() { return &___read_started_8; }
	inline void set_read_started_8(bool value)
	{
		___read_started_8 = value;
	}

	inline static int32_t get_offset_of_reader_9() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___reader_9)); }
	inline TextReader_t1561828458 * get_reader_9() const { return ___reader_9; }
	inline TextReader_t1561828458 ** get_address_of_reader_9() { return &___reader_9; }
	inline void set_reader_9(TextReader_t1561828458 * value)
	{
		___reader_9 = value;
		Il2CppCodeGenWriteBarrier((&___reader_9), value);
	}

	inline static int32_t get_offset_of_reader_is_owned_10() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___reader_is_owned_10)); }
	inline bool get_reader_is_owned_10() const { return ___reader_is_owned_10; }
	inline bool* get_address_of_reader_is_owned_10() { return &___reader_is_owned_10; }
	inline void set_reader_is_owned_10(bool value)
	{
		___reader_is_owned_10 = value;
	}

	inline static int32_t get_offset_of_token_value_11() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___token_value_11)); }
	inline RuntimeObject * get_token_value_11() const { return ___token_value_11; }
	inline RuntimeObject ** get_address_of_token_value_11() { return &___token_value_11; }
	inline void set_token_value_11(RuntimeObject * value)
	{
		___token_value_11 = value;
		Il2CppCodeGenWriteBarrier((&___token_value_11), value);
	}

	inline static int32_t get_offset_of_token_12() { return static_cast<int32_t>(offsetof(JsonReader_t354941621, ___token_12)); }
	inline int32_t get_token_12() const { return ___token_12; }
	inline int32_t* get_address_of_token_12() { return &___token_12; }
	inline void set_token_12(int32_t value)
	{
		___token_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_T354941621_H
#ifndef UIVIEW_T1452205135_H
#define UIVIEW_T1452205135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIView
struct  UIView_t1452205135  : public NSObject_t1518098886
{
public:

public:
};

struct UIView_t1452205135_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIView::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIView_t1452205135_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEW_T1452205135_H
#ifndef UIVIEWCONTROLLER_T1891120779_H
#define UIVIEWCONTROLLER_T1891120779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIViewController
struct  UIViewController_t1891120779  : public NSObject_t1518098886
{
public:

public:
};

struct UIViewController_t1891120779_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIViewController::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIViewController_t1891120779_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVIEWCONTROLLER_T1891120779_H
#ifndef UILOCALNOTIFICATION_T869364318_H
#define UILOCALNOTIFICATION_T869364318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UILocalNotification
struct  UILocalNotification_t869364318  : public NSObject_t1518098886
{
public:

public:
};

struct UILocalNotification_t869364318_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UILocalNotification::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UILocalNotification_t869364318_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILOCALNOTIFICATION_T869364318_H
#ifndef UISCREEN_T4048045402_H
#define UISCREEN_T4048045402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIScreen
struct  UIScreen_t4048045402  : public NSObject_t1518098886
{
public:

public:
};

struct UIScreen_t4048045402_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIScreen::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIScreen_t4048045402_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCREEN_T4048045402_H
#ifndef UIUSERNOTIFICATIONSETTINGS_T4227938467_H
#define UIUSERNOTIFICATIONSETTINGS_T4227938467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIUserNotificationSettings
struct  UIUserNotificationSettings_t4227938467  : public NSObject_t1518098886
{
public:

public:
};

struct UIUserNotificationSettings_t4227938467_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIUserNotificationSettings::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIUserNotificationSettings_t4227938467_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIUSERNOTIFICATIONSETTINGS_T4227938467_H
#ifndef JSONDATA_T4263252052_H
#define JSONDATA_T4263252052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.JsonData
struct  JsonData_t4263252052  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<ThirdParty.Json.LitJson.JsonData> ThirdParty.Json.LitJson.JsonData::inst_array
	RuntimeObject* ___inst_array_0;
	// System.Boolean ThirdParty.Json.LitJson.JsonData::inst_boolean
	bool ___inst_boolean_1;
	// System.Double ThirdParty.Json.LitJson.JsonData::inst_double
	double ___inst_double_2;
	// System.UInt64 ThirdParty.Json.LitJson.JsonData::inst_number
	uint64_t ___inst_number_3;
	// System.Collections.Generic.IDictionary`2<System.String,ThirdParty.Json.LitJson.JsonData> ThirdParty.Json.LitJson.JsonData::inst_object
	RuntimeObject* ___inst_object_4;
	// System.String ThirdParty.Json.LitJson.JsonData::inst_string
	String_t* ___inst_string_5;
	// System.String ThirdParty.Json.LitJson.JsonData::json
	String_t* ___json_6;
	// ThirdParty.Json.LitJson.JsonType ThirdParty.Json.LitJson.JsonData::type
	int32_t ___type_7;
	// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.String,ThirdParty.Json.LitJson.JsonData>> ThirdParty.Json.LitJson.JsonData::object_list
	RuntimeObject* ___object_list_8;

public:
	inline static int32_t get_offset_of_inst_array_0() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_array_0)); }
	inline RuntimeObject* get_inst_array_0() const { return ___inst_array_0; }
	inline RuntimeObject** get_address_of_inst_array_0() { return &___inst_array_0; }
	inline void set_inst_array_0(RuntimeObject* value)
	{
		___inst_array_0 = value;
		Il2CppCodeGenWriteBarrier((&___inst_array_0), value);
	}

	inline static int32_t get_offset_of_inst_boolean_1() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_boolean_1)); }
	inline bool get_inst_boolean_1() const { return ___inst_boolean_1; }
	inline bool* get_address_of_inst_boolean_1() { return &___inst_boolean_1; }
	inline void set_inst_boolean_1(bool value)
	{
		___inst_boolean_1 = value;
	}

	inline static int32_t get_offset_of_inst_double_2() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_double_2)); }
	inline double get_inst_double_2() const { return ___inst_double_2; }
	inline double* get_address_of_inst_double_2() { return &___inst_double_2; }
	inline void set_inst_double_2(double value)
	{
		___inst_double_2 = value;
	}

	inline static int32_t get_offset_of_inst_number_3() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_number_3)); }
	inline uint64_t get_inst_number_3() const { return ___inst_number_3; }
	inline uint64_t* get_address_of_inst_number_3() { return &___inst_number_3; }
	inline void set_inst_number_3(uint64_t value)
	{
		___inst_number_3 = value;
	}

	inline static int32_t get_offset_of_inst_object_4() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_object_4)); }
	inline RuntimeObject* get_inst_object_4() const { return ___inst_object_4; }
	inline RuntimeObject** get_address_of_inst_object_4() { return &___inst_object_4; }
	inline void set_inst_object_4(RuntimeObject* value)
	{
		___inst_object_4 = value;
		Il2CppCodeGenWriteBarrier((&___inst_object_4), value);
	}

	inline static int32_t get_offset_of_inst_string_5() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___inst_string_5)); }
	inline String_t* get_inst_string_5() const { return ___inst_string_5; }
	inline String_t** get_address_of_inst_string_5() { return &___inst_string_5; }
	inline void set_inst_string_5(String_t* value)
	{
		___inst_string_5 = value;
		Il2CppCodeGenWriteBarrier((&___inst_string_5), value);
	}

	inline static int32_t get_offset_of_json_6() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___json_6)); }
	inline String_t* get_json_6() const { return ___json_6; }
	inline String_t** get_address_of_json_6() { return &___json_6; }
	inline void set_json_6(String_t* value)
	{
		___json_6 = value;
		Il2CppCodeGenWriteBarrier((&___json_6), value);
	}

	inline static int32_t get_offset_of_type_7() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___type_7)); }
	inline int32_t get_type_7() const { return ___type_7; }
	inline int32_t* get_address_of_type_7() { return &___type_7; }
	inline void set_type_7(int32_t value)
	{
		___type_7 = value;
	}

	inline static int32_t get_offset_of_object_list_8() { return static_cast<int32_t>(offsetof(JsonData_t4263252052, ___object_list_8)); }
	inline RuntimeObject* get_object_list_8() const { return ___object_list_8; }
	inline RuntimeObject** get_address_of_object_list_8() { return &___object_list_8; }
	inline void set_object_list_8(RuntimeObject* value)
	{
		___object_list_8 = value;
		Il2CppCodeGenWriteBarrier((&___object_list_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDATA_T4263252052_H
#ifndef UISCREENMODE_T609995313_H
#define UISCREENMODE_T609995313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIScreenMode
struct  UIScreenMode_t609995313  : public NSObject_t1518098886
{
public:

public:
};

struct UIScreenMode_t609995313_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIScreenMode::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIScreenMode_t609995313_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISCREENMODE_T609995313_H
#ifndef UIIMAGE_T3580593017_H
#define UIIMAGE_T3580593017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIImage
struct  UIImage_t3580593017  : public NSObject_t1518098886
{
public:

public:
};

struct UIImage_t3580593017_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIImage::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIImage_t3580593017_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIIMAGE_T3580593017_H
#ifndef AWSCONFIGS_T909839638_H
#define AWSCONFIGS_T909839638_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.AWSConfigs
struct  AWSConfigs_t909839638  : public RuntimeObject
{
public:

public:
};

struct AWSConfigs_t909839638_StaticFields
{
public:
	// System.Char[] Amazon.AWSConfigs::validSeparators
	CharU5BU5D_t1328083999* ___validSeparators_0;
	// System.Func`1<System.DateTime> Amazon.AWSConfigs::utcNowSource
	Func_1_t2647598351 * ___utcNowSource_1;
	// System.String Amazon.AWSConfigs::_awsRegion
	String_t* ____awsRegion_2;
	// Amazon.LoggingOptions Amazon.AWSConfigs::_logging
	int32_t ____logging_3;
	// Amazon.ResponseLoggingOption Amazon.AWSConfigs::_responseLogging
	int32_t ____responseLogging_4;
	// System.Boolean Amazon.AWSConfigs::_logMetrics
	bool ____logMetrics_5;
	// System.String Amazon.AWSConfigs::_endpointDefinition
	String_t* ____endpointDefinition_6;
	// System.String Amazon.AWSConfigs::_awsProfileName
	String_t* ____awsProfileName_7;
	// System.String Amazon.AWSConfigs::_awsAccountsLocation
	String_t* ____awsAccountsLocation_8;
	// System.Boolean Amazon.AWSConfigs::_useSdkCache
	bool ____useSdkCache_9;
	// System.Object Amazon.AWSConfigs::_lock
	RuntimeObject * ____lock_10;
	// System.Collections.Generic.List`1<System.String> Amazon.AWSConfigs::standardConfigs
	List_1_t1398341365 * ___standardConfigs_11;
	// System.Boolean Amazon.AWSConfigs::configPresent
	bool ___configPresent_12;
	// Amazon.Util.Internal.RootConfig Amazon.AWSConfigs::_rootConfig
	RootConfig_t4046630774 * ____rootConfig_13;
	// System.TimeSpan Amazon.AWSConfigs::<ClockOffset>k__BackingField
	TimeSpan_t3430258949  ___U3CClockOffsetU3Ek__BackingField_14;
	// System.ComponentModel.PropertyChangedEventHandler Amazon.AWSConfigs::mPropertyChanged
	PropertyChangedEventHandler_t3042952059 * ___mPropertyChanged_15;
	// System.Object Amazon.AWSConfigs::propertyChangedLock
	RuntimeObject * ___propertyChangedLock_16;
	// Amazon.AWSConfigs/HttpClientOption Amazon.AWSConfigs::_httpClient
	int32_t ____httpClient_17;
	// System.Boolean Amazon.AWSConfigs::UnityWebRequestInitialized
	bool ___UnityWebRequestInitialized_18;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<System.Diagnostics.TraceListener>> Amazon.AWSConfigs::_traceListeners
	Dictionary_2_t403882377 * ____traceListeners_19;
	// System.Xml.Linq.XDocument Amazon.AWSConfigs::xmlDoc
	XDocument_t2733326047 * ___xmlDoc_20;

public:
	inline static int32_t get_offset_of_validSeparators_0() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___validSeparators_0)); }
	inline CharU5BU5D_t1328083999* get_validSeparators_0() const { return ___validSeparators_0; }
	inline CharU5BU5D_t1328083999** get_address_of_validSeparators_0() { return &___validSeparators_0; }
	inline void set_validSeparators_0(CharU5BU5D_t1328083999* value)
	{
		___validSeparators_0 = value;
		Il2CppCodeGenWriteBarrier((&___validSeparators_0), value);
	}

	inline static int32_t get_offset_of_utcNowSource_1() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___utcNowSource_1)); }
	inline Func_1_t2647598351 * get_utcNowSource_1() const { return ___utcNowSource_1; }
	inline Func_1_t2647598351 ** get_address_of_utcNowSource_1() { return &___utcNowSource_1; }
	inline void set_utcNowSource_1(Func_1_t2647598351 * value)
	{
		___utcNowSource_1 = value;
		Il2CppCodeGenWriteBarrier((&___utcNowSource_1), value);
	}

	inline static int32_t get_offset_of__awsRegion_2() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____awsRegion_2)); }
	inline String_t* get__awsRegion_2() const { return ____awsRegion_2; }
	inline String_t** get_address_of__awsRegion_2() { return &____awsRegion_2; }
	inline void set__awsRegion_2(String_t* value)
	{
		____awsRegion_2 = value;
		Il2CppCodeGenWriteBarrier((&____awsRegion_2), value);
	}

	inline static int32_t get_offset_of__logging_3() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____logging_3)); }
	inline int32_t get__logging_3() const { return ____logging_3; }
	inline int32_t* get_address_of__logging_3() { return &____logging_3; }
	inline void set__logging_3(int32_t value)
	{
		____logging_3 = value;
	}

	inline static int32_t get_offset_of__responseLogging_4() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____responseLogging_4)); }
	inline int32_t get__responseLogging_4() const { return ____responseLogging_4; }
	inline int32_t* get_address_of__responseLogging_4() { return &____responseLogging_4; }
	inline void set__responseLogging_4(int32_t value)
	{
		____responseLogging_4 = value;
	}

	inline static int32_t get_offset_of__logMetrics_5() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____logMetrics_5)); }
	inline bool get__logMetrics_5() const { return ____logMetrics_5; }
	inline bool* get_address_of__logMetrics_5() { return &____logMetrics_5; }
	inline void set__logMetrics_5(bool value)
	{
		____logMetrics_5 = value;
	}

	inline static int32_t get_offset_of__endpointDefinition_6() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____endpointDefinition_6)); }
	inline String_t* get__endpointDefinition_6() const { return ____endpointDefinition_6; }
	inline String_t** get_address_of__endpointDefinition_6() { return &____endpointDefinition_6; }
	inline void set__endpointDefinition_6(String_t* value)
	{
		____endpointDefinition_6 = value;
		Il2CppCodeGenWriteBarrier((&____endpointDefinition_6), value);
	}

	inline static int32_t get_offset_of__awsProfileName_7() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____awsProfileName_7)); }
	inline String_t* get__awsProfileName_7() const { return ____awsProfileName_7; }
	inline String_t** get_address_of__awsProfileName_7() { return &____awsProfileName_7; }
	inline void set__awsProfileName_7(String_t* value)
	{
		____awsProfileName_7 = value;
		Il2CppCodeGenWriteBarrier((&____awsProfileName_7), value);
	}

	inline static int32_t get_offset_of__awsAccountsLocation_8() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____awsAccountsLocation_8)); }
	inline String_t* get__awsAccountsLocation_8() const { return ____awsAccountsLocation_8; }
	inline String_t** get_address_of__awsAccountsLocation_8() { return &____awsAccountsLocation_8; }
	inline void set__awsAccountsLocation_8(String_t* value)
	{
		____awsAccountsLocation_8 = value;
		Il2CppCodeGenWriteBarrier((&____awsAccountsLocation_8), value);
	}

	inline static int32_t get_offset_of__useSdkCache_9() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____useSdkCache_9)); }
	inline bool get__useSdkCache_9() const { return ____useSdkCache_9; }
	inline bool* get_address_of__useSdkCache_9() { return &____useSdkCache_9; }
	inline void set__useSdkCache_9(bool value)
	{
		____useSdkCache_9 = value;
	}

	inline static int32_t get_offset_of__lock_10() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____lock_10)); }
	inline RuntimeObject * get__lock_10() const { return ____lock_10; }
	inline RuntimeObject ** get_address_of__lock_10() { return &____lock_10; }
	inline void set__lock_10(RuntimeObject * value)
	{
		____lock_10 = value;
		Il2CppCodeGenWriteBarrier((&____lock_10), value);
	}

	inline static int32_t get_offset_of_standardConfigs_11() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___standardConfigs_11)); }
	inline List_1_t1398341365 * get_standardConfigs_11() const { return ___standardConfigs_11; }
	inline List_1_t1398341365 ** get_address_of_standardConfigs_11() { return &___standardConfigs_11; }
	inline void set_standardConfigs_11(List_1_t1398341365 * value)
	{
		___standardConfigs_11 = value;
		Il2CppCodeGenWriteBarrier((&___standardConfigs_11), value);
	}

	inline static int32_t get_offset_of_configPresent_12() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___configPresent_12)); }
	inline bool get_configPresent_12() const { return ___configPresent_12; }
	inline bool* get_address_of_configPresent_12() { return &___configPresent_12; }
	inline void set_configPresent_12(bool value)
	{
		___configPresent_12 = value;
	}

	inline static int32_t get_offset_of__rootConfig_13() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____rootConfig_13)); }
	inline RootConfig_t4046630774 * get__rootConfig_13() const { return ____rootConfig_13; }
	inline RootConfig_t4046630774 ** get_address_of__rootConfig_13() { return &____rootConfig_13; }
	inline void set__rootConfig_13(RootConfig_t4046630774 * value)
	{
		____rootConfig_13 = value;
		Il2CppCodeGenWriteBarrier((&____rootConfig_13), value);
	}

	inline static int32_t get_offset_of_U3CClockOffsetU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___U3CClockOffsetU3Ek__BackingField_14)); }
	inline TimeSpan_t3430258949  get_U3CClockOffsetU3Ek__BackingField_14() const { return ___U3CClockOffsetU3Ek__BackingField_14; }
	inline TimeSpan_t3430258949 * get_address_of_U3CClockOffsetU3Ek__BackingField_14() { return &___U3CClockOffsetU3Ek__BackingField_14; }
	inline void set_U3CClockOffsetU3Ek__BackingField_14(TimeSpan_t3430258949  value)
	{
		___U3CClockOffsetU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_mPropertyChanged_15() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___mPropertyChanged_15)); }
	inline PropertyChangedEventHandler_t3042952059 * get_mPropertyChanged_15() const { return ___mPropertyChanged_15; }
	inline PropertyChangedEventHandler_t3042952059 ** get_address_of_mPropertyChanged_15() { return &___mPropertyChanged_15; }
	inline void set_mPropertyChanged_15(PropertyChangedEventHandler_t3042952059 * value)
	{
		___mPropertyChanged_15 = value;
		Il2CppCodeGenWriteBarrier((&___mPropertyChanged_15), value);
	}

	inline static int32_t get_offset_of_propertyChangedLock_16() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___propertyChangedLock_16)); }
	inline RuntimeObject * get_propertyChangedLock_16() const { return ___propertyChangedLock_16; }
	inline RuntimeObject ** get_address_of_propertyChangedLock_16() { return &___propertyChangedLock_16; }
	inline void set_propertyChangedLock_16(RuntimeObject * value)
	{
		___propertyChangedLock_16 = value;
		Il2CppCodeGenWriteBarrier((&___propertyChangedLock_16), value);
	}

	inline static int32_t get_offset_of__httpClient_17() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____httpClient_17)); }
	inline int32_t get__httpClient_17() const { return ____httpClient_17; }
	inline int32_t* get_address_of__httpClient_17() { return &____httpClient_17; }
	inline void set__httpClient_17(int32_t value)
	{
		____httpClient_17 = value;
	}

	inline static int32_t get_offset_of_UnityWebRequestInitialized_18() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___UnityWebRequestInitialized_18)); }
	inline bool get_UnityWebRequestInitialized_18() const { return ___UnityWebRequestInitialized_18; }
	inline bool* get_address_of_UnityWebRequestInitialized_18() { return &___UnityWebRequestInitialized_18; }
	inline void set_UnityWebRequestInitialized_18(bool value)
	{
		___UnityWebRequestInitialized_18 = value;
	}

	inline static int32_t get_offset_of__traceListeners_19() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ____traceListeners_19)); }
	inline Dictionary_2_t403882377 * get__traceListeners_19() const { return ____traceListeners_19; }
	inline Dictionary_2_t403882377 ** get_address_of__traceListeners_19() { return &____traceListeners_19; }
	inline void set__traceListeners_19(Dictionary_2_t403882377 * value)
	{
		____traceListeners_19 = value;
		Il2CppCodeGenWriteBarrier((&____traceListeners_19), value);
	}

	inline static int32_t get_offset_of_xmlDoc_20() { return static_cast<int32_t>(offsetof(AWSConfigs_t909839638_StaticFields, ___xmlDoc_20)); }
	inline XDocument_t2733326047 * get_xmlDoc_20() const { return ___xmlDoc_20; }
	inline XDocument_t2733326047 ** get_address_of_xmlDoc_20() { return &___xmlDoc_20; }
	inline void set_xmlDoc_20(XDocument_t2733326047 * value)
	{
		___xmlDoc_20 = value;
		Il2CppCodeGenWriteBarrier((&___xmlDoc_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AWSCONFIGS_T909839638_H
#ifndef LOGGINGCONFIG_T4162907495_H
#define LOGGINGCONFIG_T4162907495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.LoggingConfig
struct  LoggingConfig_t4162907495  : public RuntimeObject
{
public:
	// Amazon.LoggingOptions Amazon.Util.LoggingConfig::_logTo
	int32_t ____logTo_1;
	// Amazon.ResponseLoggingOption Amazon.Util.LoggingConfig::<LogResponses>k__BackingField
	int32_t ___U3CLogResponsesU3Ek__BackingField_2;
	// System.Int32 Amazon.Util.LoggingConfig::<LogResponsesSizeLimit>k__BackingField
	int32_t ___U3CLogResponsesSizeLimitU3Ek__BackingField_3;
	// System.Boolean Amazon.Util.LoggingConfig::<LogMetrics>k__BackingField
	bool ___U3CLogMetricsU3Ek__BackingField_4;
	// Amazon.LogMetricsFormatOption Amazon.Util.LoggingConfig::<LogMetricsFormat>k__BackingField
	int32_t ___U3CLogMetricsFormatU3Ek__BackingField_5;
	// Amazon.Runtime.IMetricsFormatter Amazon.Util.LoggingConfig::<LogMetricsCustomFormatter>k__BackingField
	RuntimeObject* ___U3CLogMetricsCustomFormatterU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of__logTo_1() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ____logTo_1)); }
	inline int32_t get__logTo_1() const { return ____logTo_1; }
	inline int32_t* get_address_of__logTo_1() { return &____logTo_1; }
	inline void set__logTo_1(int32_t value)
	{
		____logTo_1 = value;
	}

	inline static int32_t get_offset_of_U3CLogResponsesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogResponsesU3Ek__BackingField_2)); }
	inline int32_t get_U3CLogResponsesU3Ek__BackingField_2() const { return ___U3CLogResponsesU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLogResponsesU3Ek__BackingField_2() { return &___U3CLogResponsesU3Ek__BackingField_2; }
	inline void set_U3CLogResponsesU3Ek__BackingField_2(int32_t value)
	{
		___U3CLogResponsesU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLogResponsesSizeLimitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogResponsesSizeLimitU3Ek__BackingField_3)); }
	inline int32_t get_U3CLogResponsesSizeLimitU3Ek__BackingField_3() const { return ___U3CLogResponsesSizeLimitU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CLogResponsesSizeLimitU3Ek__BackingField_3() { return &___U3CLogResponsesSizeLimitU3Ek__BackingField_3; }
	inline void set_U3CLogResponsesSizeLimitU3Ek__BackingField_3(int32_t value)
	{
		___U3CLogResponsesSizeLimitU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogMetricsU3Ek__BackingField_4)); }
	inline bool get_U3CLogMetricsU3Ek__BackingField_4() const { return ___U3CLogMetricsU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CLogMetricsU3Ek__BackingField_4() { return &___U3CLogMetricsU3Ek__BackingField_4; }
	inline void set_U3CLogMetricsU3Ek__BackingField_4(bool value)
	{
		___U3CLogMetricsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsFormatU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogMetricsFormatU3Ek__BackingField_5)); }
	inline int32_t get_U3CLogMetricsFormatU3Ek__BackingField_5() const { return ___U3CLogMetricsFormatU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CLogMetricsFormatU3Ek__BackingField_5() { return &___U3CLogMetricsFormatU3Ek__BackingField_5; }
	inline void set_U3CLogMetricsFormatU3Ek__BackingField_5(int32_t value)
	{
		___U3CLogMetricsFormatU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495, ___U3CLogMetricsCustomFormatterU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CLogMetricsCustomFormatterU3Ek__BackingField_6() const { return ___U3CLogMetricsCustomFormatterU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_6() { return &___U3CLogMetricsCustomFormatterU3Ek__BackingField_6; }
	inline void set_U3CLogMetricsCustomFormatterU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CLogMetricsCustomFormatterU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLogMetricsCustomFormatterU3Ek__BackingField_6), value);
	}
};

struct LoggingConfig_t4162907495_StaticFields
{
public:
	// System.Int32 Amazon.Util.LoggingConfig::DefaultLogResponsesSizeLimit
	int32_t ___DefaultLogResponsesSizeLimit_0;

public:
	inline static int32_t get_offset_of_DefaultLogResponsesSizeLimit_0() { return static_cast<int32_t>(offsetof(LoggingConfig_t4162907495_StaticFields, ___DefaultLogResponsesSizeLimit_0)); }
	inline int32_t get_DefaultLogResponsesSizeLimit_0() const { return ___DefaultLogResponsesSizeLimit_0; }
	inline int32_t* get_address_of_DefaultLogResponsesSizeLimit_0() { return &___DefaultLogResponsesSizeLimit_0; }
	inline void set_DefaultLogResponsesSizeLimit_0(int32_t value)
	{
		___DefaultLogResponsesSizeLimit_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGINGCONFIG_T4162907495_H
#ifndef LOGGINGSECTION_T905443946_H
#define LOGGINGSECTION_T905443946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.LoggingSection
struct  LoggingSection_t905443946  : public RuntimeObject
{
public:
	// Amazon.LoggingOptions Amazon.LoggingSection::<LogTo>k__BackingField
	int32_t ___U3CLogToU3Ek__BackingField_0;
	// Amazon.ResponseLoggingOption Amazon.LoggingSection::<LogResponses>k__BackingField
	int32_t ___U3CLogResponsesU3Ek__BackingField_1;
	// System.Nullable`1<System.Int32> Amazon.LoggingSection::<LogResponsesSizeLimit>k__BackingField
	Nullable_1_t334943763  ___U3CLogResponsesSizeLimitU3Ek__BackingField_2;
	// System.Nullable`1<System.Boolean> Amazon.LoggingSection::<LogMetrics>k__BackingField
	Nullable_1_t2088641033  ___U3CLogMetricsU3Ek__BackingField_3;
	// Amazon.LogMetricsFormatOption Amazon.LoggingSection::<LogMetricsFormat>k__BackingField
	int32_t ___U3CLogMetricsFormatU3Ek__BackingField_4;
	// System.Type Amazon.LoggingSection::<LogMetricsCustomFormatter>k__BackingField
	Type_t * ___U3CLogMetricsCustomFormatterU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CLogToU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogToU3Ek__BackingField_0)); }
	inline int32_t get_U3CLogToU3Ek__BackingField_0() const { return ___U3CLogToU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CLogToU3Ek__BackingField_0() { return &___U3CLogToU3Ek__BackingField_0; }
	inline void set_U3CLogToU3Ek__BackingField_0(int32_t value)
	{
		___U3CLogToU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CLogResponsesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogResponsesU3Ek__BackingField_1)); }
	inline int32_t get_U3CLogResponsesU3Ek__BackingField_1() const { return ___U3CLogResponsesU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CLogResponsesU3Ek__BackingField_1() { return &___U3CLogResponsesU3Ek__BackingField_1; }
	inline void set_U3CLogResponsesU3Ek__BackingField_1(int32_t value)
	{
		___U3CLogResponsesU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLogResponsesSizeLimitU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogResponsesSizeLimitU3Ek__BackingField_2)); }
	inline Nullable_1_t334943763  get_U3CLogResponsesSizeLimitU3Ek__BackingField_2() const { return ___U3CLogResponsesSizeLimitU3Ek__BackingField_2; }
	inline Nullable_1_t334943763 * get_address_of_U3CLogResponsesSizeLimitU3Ek__BackingField_2() { return &___U3CLogResponsesSizeLimitU3Ek__BackingField_2; }
	inline void set_U3CLogResponsesSizeLimitU3Ek__BackingField_2(Nullable_1_t334943763  value)
	{
		___U3CLogResponsesSizeLimitU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogMetricsU3Ek__BackingField_3)); }
	inline Nullable_1_t2088641033  get_U3CLogMetricsU3Ek__BackingField_3() const { return ___U3CLogMetricsU3Ek__BackingField_3; }
	inline Nullable_1_t2088641033 * get_address_of_U3CLogMetricsU3Ek__BackingField_3() { return &___U3CLogMetricsU3Ek__BackingField_3; }
	inline void set_U3CLogMetricsU3Ek__BackingField_3(Nullable_1_t2088641033  value)
	{
		___U3CLogMetricsU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsFormatU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogMetricsFormatU3Ek__BackingField_4)); }
	inline int32_t get_U3CLogMetricsFormatU3Ek__BackingField_4() const { return ___U3CLogMetricsFormatU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CLogMetricsFormatU3Ek__BackingField_4() { return &___U3CLogMetricsFormatU3Ek__BackingField_4; }
	inline void set_U3CLogMetricsFormatU3Ek__BackingField_4(int32_t value)
	{
		___U3CLogMetricsFormatU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(LoggingSection_t905443946, ___U3CLogMetricsCustomFormatterU3Ek__BackingField_5)); }
	inline Type_t * get_U3CLogMetricsCustomFormatterU3Ek__BackingField_5() const { return ___U3CLogMetricsCustomFormatterU3Ek__BackingField_5; }
	inline Type_t ** get_address_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_5() { return &___U3CLogMetricsCustomFormatterU3Ek__BackingField_5; }
	inline void set_U3CLogMetricsCustomFormatterU3Ek__BackingField_5(Type_t * value)
	{
		___U3CLogMetricsCustomFormatterU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLogMetricsCustomFormatterU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGINGSECTION_T905443946_H
#ifndef UIDEVICE_T860038178_H
#define UIDEVICE_T860038178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIDevice
struct  UIDevice_t860038178  : public NSObject_t1518098886
{
public:

public:
};

struct UIDevice_t860038178_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIDevice::_classHandle
	intptr_t ____classHandle_3;
	// System.Int32 ThirdParty.iOS4Unity.UIDevice::_majorVersion
	int32_t ____majorVersion_4;
	// System.Int32 ThirdParty.iOS4Unity.UIDevice::_minorVersion
	int32_t ____minorVersion_5;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIDevice_t860038178_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}

	inline static int32_t get_offset_of__majorVersion_4() { return static_cast<int32_t>(offsetof(UIDevice_t860038178_StaticFields, ____majorVersion_4)); }
	inline int32_t get__majorVersion_4() const { return ____majorVersion_4; }
	inline int32_t* get_address_of__majorVersion_4() { return &____majorVersion_4; }
	inline void set__majorVersion_4(int32_t value)
	{
		____majorVersion_4 = value;
	}

	inline static int32_t get_offset_of__minorVersion_5() { return static_cast<int32_t>(offsetof(UIDevice_t860038178_StaticFields, ____minorVersion_5)); }
	inline int32_t get__minorVersion_5() const { return ____minorVersion_5; }
	inline int32_t* get_address_of__minorVersion_5() { return &____minorVersion_5; }
	inline void set__minorVersion_5(int32_t value)
	{
		____minorVersion_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIDEVICE_T860038178_H
#ifndef UIAPPLICATION_T1857964820_H
#define UIAPPLICATION_T1857964820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIApplication
struct  UIApplication_t1857964820  : public NSObject_t1518098886
{
public:

public:
};

struct UIApplication_t1857964820_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIApplication::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIApplication_t1857964820_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIAPPLICATION_T1857964820_H
#ifndef UIALERTVIEW_T2175232939_H
#define UIALERTVIEW_T2175232939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIAlertView
struct  UIAlertView_t2175232939  : public NSObject_t1518098886
{
public:

public:
};

struct UIAlertView_t2175232939_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIAlertView::_classHandle
	intptr_t ____classHandle_3;

public:
	inline static int32_t get_offset_of__classHandle_3() { return static_cast<int32_t>(offsetof(UIAlertView_t2175232939_StaticFields, ____classHandle_3)); }
	inline intptr_t get__classHandle_3() const { return ____classHandle_3; }
	inline intptr_t* get_address_of__classHandle_3() { return &____classHandle_3; }
	inline void set__classHandle_3(intptr_t value)
	{
		____classHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIALERTVIEW_T2175232939_H
#ifndef WRAPPERFACTORY_T327905379_H
#define WRAPPERFACTORY_T327905379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.WrapperFactory
struct  WrapperFactory_t327905379  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRAPPERFACTORY_T327905379_H
#ifndef IMPORTERFUNC_T850687278_H
#define IMPORTERFUNC_T850687278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.ImporterFunc
struct  ImporterFunc_t850687278  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMPORTERFUNC_T850687278_H
#ifndef AWSSDKUTILS_T2036360342_H
#define AWSSDKUTILS_T2036360342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.Util.AWSSDKUtils
struct  AWSSDKUtils_t2036360342  : public RuntimeObject
{
public:

public:
};

struct AWSSDKUtils_t2036360342_StaticFields
{
public:
	// System.DateTime Amazon.Util.AWSSDKUtils::EPOCH_START
	DateTime_t693205669  ___EPOCH_START_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> Amazon.Util.AWSSDKUtils::RFCEncodingSchemes
	Dictionary_2_t1037045868 * ___RFCEncodingSchemes_1;
	// System.String Amazon.Util.AWSSDKUtils::ValidPathCharacters
	String_t* ___ValidPathCharacters_2;
	// Amazon.Runtime.Internal.Util.BackgroundInvoker Amazon.Util.AWSSDKUtils::_dispatcher
	BackgroundInvoker_t1722929158 * ____dispatcher_3;
	// System.Object Amazon.Util.AWSSDKUtils::_preserveStackTraceLookupLock
	RuntimeObject * ____preserveStackTraceLookupLock_4;
	// System.Boolean Amazon.Util.AWSSDKUtils::_preserveStackTraceLookup
	bool ____preserveStackTraceLookup_5;

public:
	inline static int32_t get_offset_of_EPOCH_START_0() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ___EPOCH_START_0)); }
	inline DateTime_t693205669  get_EPOCH_START_0() const { return ___EPOCH_START_0; }
	inline DateTime_t693205669 * get_address_of_EPOCH_START_0() { return &___EPOCH_START_0; }
	inline void set_EPOCH_START_0(DateTime_t693205669  value)
	{
		___EPOCH_START_0 = value;
	}

	inline static int32_t get_offset_of_RFCEncodingSchemes_1() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ___RFCEncodingSchemes_1)); }
	inline Dictionary_2_t1037045868 * get_RFCEncodingSchemes_1() const { return ___RFCEncodingSchemes_1; }
	inline Dictionary_2_t1037045868 ** get_address_of_RFCEncodingSchemes_1() { return &___RFCEncodingSchemes_1; }
	inline void set_RFCEncodingSchemes_1(Dictionary_2_t1037045868 * value)
	{
		___RFCEncodingSchemes_1 = value;
		Il2CppCodeGenWriteBarrier((&___RFCEncodingSchemes_1), value);
	}

	inline static int32_t get_offset_of_ValidPathCharacters_2() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ___ValidPathCharacters_2)); }
	inline String_t* get_ValidPathCharacters_2() const { return ___ValidPathCharacters_2; }
	inline String_t** get_address_of_ValidPathCharacters_2() { return &___ValidPathCharacters_2; }
	inline void set_ValidPathCharacters_2(String_t* value)
	{
		___ValidPathCharacters_2 = value;
		Il2CppCodeGenWriteBarrier((&___ValidPathCharacters_2), value);
	}

	inline static int32_t get_offset_of__dispatcher_3() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ____dispatcher_3)); }
	inline BackgroundInvoker_t1722929158 * get__dispatcher_3() const { return ____dispatcher_3; }
	inline BackgroundInvoker_t1722929158 ** get_address_of__dispatcher_3() { return &____dispatcher_3; }
	inline void set__dispatcher_3(BackgroundInvoker_t1722929158 * value)
	{
		____dispatcher_3 = value;
		Il2CppCodeGenWriteBarrier((&____dispatcher_3), value);
	}

	inline static int32_t get_offset_of__preserveStackTraceLookupLock_4() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ____preserveStackTraceLookupLock_4)); }
	inline RuntimeObject * get__preserveStackTraceLookupLock_4() const { return ____preserveStackTraceLookupLock_4; }
	inline RuntimeObject ** get_address_of__preserveStackTraceLookupLock_4() { return &____preserveStackTraceLookupLock_4; }
	inline void set__preserveStackTraceLookupLock_4(RuntimeObject * value)
	{
		____preserveStackTraceLookupLock_4 = value;
		Il2CppCodeGenWriteBarrier((&____preserveStackTraceLookupLock_4), value);
	}

	inline static int32_t get_offset_of__preserveStackTraceLookup_5() { return static_cast<int32_t>(offsetof(AWSSDKUtils_t2036360342_StaticFields, ____preserveStackTraceLookup_5)); }
	inline bool get__preserveStackTraceLookup_5() const { return ____preserveStackTraceLookup_5; }
	inline bool* get_address_of__preserveStackTraceLookup_5() { return &____preserveStackTraceLookup_5; }
	inline void set__preserveStackTraceLookup_5(bool value)
	{
		____preserveStackTraceLookup_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AWSSDKUTILS_T2036360342_H
#ifndef EXPORTERFUNC_T173265409_H
#define EXPORTERFUNC_T173265409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.ExporterFunc
struct  ExporterFunc_t173265409  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPORTERFUNC_T173265409_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ADBANNERVIEW_T1238242894_H
#define ADBANNERVIEW_T1238242894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.AdBannerView
struct  AdBannerView_t1238242894  : public UIView_t1452205135
{
public:

public:
};

struct AdBannerView_t1238242894_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.AdBannerView::_classHandle
	intptr_t ____classHandle_4;

public:
	inline static int32_t get_offset_of__classHandle_4() { return static_cast<int32_t>(offsetof(AdBannerView_t1238242894_StaticFields, ____classHandle_4)); }
	inline intptr_t get__classHandle_4() const { return ____classHandle_4; }
	inline intptr_t* get_address_of__classHandle_4() { return &____classHandle_4; }
	inline void set__classHandle_4(intptr_t value)
	{
		____classHandle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADBANNERVIEW_T1238242894_H
#ifndef STATEHANDLER_T3489987002_H
#define STATEHANDLER_T3489987002_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.Json.LitJson.Lexer/StateHandler
struct  StateHandler_t3489987002  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEHANDLER_T3489987002_H
#ifndef UIACTIVITYVIEWCONTROLLER_T2230362714_H
#define UIACTIVITYVIEWCONTROLLER_T2230362714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIActivityViewController
struct  UIActivityViewController_t2230362714  : public UIViewController_t1891120779
{
public:

public:
};

struct UIActivityViewController_t2230362714_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIActivityViewController::_classHandle
	intptr_t ____classHandle_4;

public:
	inline static int32_t get_offset_of__classHandle_4() { return static_cast<int32_t>(offsetof(UIActivityViewController_t2230362714_StaticFields, ____classHandle_4)); }
	inline intptr_t get__classHandle_4() const { return ____classHandle_4; }
	inline intptr_t* get_address_of__classHandle_4() { return &____classHandle_4; }
	inline void set__classHandle_4(intptr_t value)
	{
		____classHandle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIACTIVITYVIEWCONTROLLER_T2230362714_H
#ifndef UIACTIONSHEET_T2483183669_H
#define UIACTIONSHEET_T2483183669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIActionSheet
struct  UIActionSheet_t2483183669  : public UIView_t1452205135
{
public:

public:
};

struct UIActionSheet_t2483183669_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIActionSheet::_classHandle
	intptr_t ____classHandle_4;

public:
	inline static int32_t get_offset_of__classHandle_4() { return static_cast<int32_t>(offsetof(UIActionSheet_t2483183669_StaticFields, ____classHandle_4)); }
	inline intptr_t get__classHandle_4() const { return ____classHandle_4; }
	inline intptr_t* get_address_of__classHandle_4() { return &____classHandle_4; }
	inline void set__classHandle_4(intptr_t value)
	{
		____classHandle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIACTIONSHEET_T2483183669_H
#ifndef UIWINDOW_T800018578_H
#define UIWINDOW_T800018578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.UIWindow
struct  UIWindow_t800018578  : public UIView_t1452205135
{
public:

public:
};

struct UIWindow_t800018578_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.UIWindow::_classHandle
	intptr_t ____classHandle_4;

public:
	inline static int32_t get_offset_of__classHandle_4() { return static_cast<int32_t>(offsetof(UIWindow_t800018578_StaticFields, ____classHandle_4)); }
	inline intptr_t get__classHandle_4() const { return ____classHandle_4; }
	inline intptr_t* get_address_of__classHandle_4() { return &____classHandle_4; }
	inline void set__classHandle_4(intptr_t value)
	{
		____classHandle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIWINDOW_T800018578_H
#ifndef NSMUTABLEDICTIONARY_T171997941_H
#define NSMUTABLEDICTIONARY_T171997941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ThirdParty.iOS4Unity.NSMutableDictionary
struct  NSMutableDictionary_t171997941  : public NSDictionary_t3598984691
{
public:

public:
};

struct NSMutableDictionary_t171997941_StaticFields
{
public:
	// System.IntPtr ThirdParty.iOS4Unity.NSMutableDictionary::_classHandle
	intptr_t ____classHandle_4;

public:
	inline static int32_t get_offset_of__classHandle_4() { return static_cast<int32_t>(offsetof(NSMutableDictionary_t171997941_StaticFields, ____classHandle_4)); }
	inline intptr_t get__classHandle_4() const { return ____classHandle_4; }
	inline intptr_t* get_address_of__classHandle_4() { return &____classHandle_4; }
	inline void set__classHandle_4(intptr_t value)
	{
		____classHandle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NSMUTABLEDICTIONARY_T171997941_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef UNITYINITIALIZER_T2778189483_H
#define UNITYINITIALIZER_T2778189483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Amazon.UnityInitializer
struct  UnityInitializer_t2778189483  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct UnityInitializer_t2778189483_StaticFields
{
public:
	// Amazon.UnityInitializer Amazon.UnityInitializer::_instance
	UnityInitializer_t2778189483 * ____instance_2;
	// System.Object Amazon.UnityInitializer::_lock
	RuntimeObject * ____lock_3;
	// System.Threading.Thread Amazon.UnityInitializer::_mainThread
	Thread_t241561612 * ____mainThread_4;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(UnityInitializer_t2778189483_StaticFields, ____instance_2)); }
	inline UnityInitializer_t2778189483 * get__instance_2() const { return ____instance_2; }
	inline UnityInitializer_t2778189483 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(UnityInitializer_t2778189483 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of__lock_3() { return static_cast<int32_t>(offsetof(UnityInitializer_t2778189483_StaticFields, ____lock_3)); }
	inline RuntimeObject * get__lock_3() const { return ____lock_3; }
	inline RuntimeObject ** get_address_of__lock_3() { return &____lock_3; }
	inline void set__lock_3(RuntimeObject * value)
	{
		____lock_3 = value;
		Il2CppCodeGenWriteBarrier((&____lock_3), value);
	}

	inline static int32_t get_offset_of__mainThread_4() { return static_cast<int32_t>(offsetof(UnityInitializer_t2778189483_StaticFields, ____mainThread_4)); }
	inline Thread_t241561612 * get__mainThread_4() const { return ____mainThread_4; }
	inline Thread_t241561612 ** get_address_of__mainThread_4() { return &____mainThread_4; }
	inline void set__mainThread_4(Thread_t241561612 * value)
	{
		____mainThread_4 = value;
		Il2CppCodeGenWriteBarrier((&____mainThread_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYINITIALIZER_T2778189483_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (U3CModuleU3E_t3783534234), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (U3CModuleU3E_t3783534235), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (ObjC_t468753822), -1, sizeof(ObjC_t468753822_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2102[12] = 
{
	ObjC_t468753822_StaticFields::get_offset_of_TimeIntervalSinceReferenceDateSelector_0(),
	ObjC_t468753822_StaticFields::get_offset_of_AbsoluteStringSelector_1(),
	ObjC_t468753822_StaticFields::get_offset_of_DoubleValueSelector_2(),
	ObjC_t468753822_StaticFields::get_offset_of_CountSelector_3(),
	ObjC_t468753822_StaticFields::get_offset_of_ObjectAtIndexSelector_4(),
	ObjC_t468753822_StaticFields::get_offset_of_AllObjectsSelector_5(),
	ObjC_t468753822_StaticFields::get_offset_of_ArrayWithObjects_CountSelector_6(),
	ObjC_t468753822_StaticFields::get_offset_of_SetWithArraySelector_7(),
	ObjC_t468753822_StaticFields::get_offset_of_InitWithCharacters_lengthSelector_8(),
	ObjC_t468753822_StaticFields::get_offset_of_URLWithStringSelector_9(),
	ObjC_t468753822_StaticFields::get_offset_of_DateWithTimeIntervalSinceReferenceDateSelector_10(),
	ObjC_t468753822_StaticFields::get_offset_of_InitWithDoubleSelector_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (Libraries_t4218932343), -1, sizeof(Libraries_t4218932343_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2103[2] = 
{
	Libraries_t4218932343_StaticFields::get_offset_of_Foundation_0(),
	Libraries_t4218932343_StaticFields::get_offset_of_UIKit_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (Runtime_t1145037850), -1, sizeof(Runtime_t1145037850_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2104[4] = 
{
	Runtime_t1145037850_StaticFields::get_offset_of__contructorLock_0(),
	Runtime_t1145037850_StaticFields::get_offset_of__objectLock_1(),
	Runtime_t1145037850_StaticFields::get_offset_of__constructors_2(),
	Runtime_t1145037850_StaticFields::get_offset_of__objects_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (U3CU3Ec_t88230275), -1, sizeof(U3CU3Ec_t88230275_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2105[1] = 
{
	U3CU3Ec_t88230275_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (Selector_t1939762325), -1, sizeof(Selector_t1939762325_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2106[16] = 
{
	Selector_t1939762325_StaticFields::get_offset_of_Init_0(),
	Selector_t1939762325_StaticFields::get_offset_of_InitWithCoder_1(),
	Selector_t1939762325_StaticFields::get_offset_of_InitWithName_2(),
	Selector_t1939762325_StaticFields::get_offset_of_InitWithFrame_3(),
	Selector_t1939762325_StaticFields::get_offset_of_MethodSignatureForSelector_4(),
	Selector_t1939762325_StaticFields::get_offset_of_FrameLength_5(),
	Selector_t1939762325_StaticFields::get_offset_of_RetainCount_6(),
	Selector_t1939762325_StaticFields::get_offset_of_AllocHandle_7(),
	Selector_t1939762325_StaticFields::get_offset_of_ReleaseHandle_8(),
	Selector_t1939762325_StaticFields::get_offset_of_RetainHandle_9(),
	Selector_t1939762325_StaticFields::get_offset_of_AutoreleaseHandle_10(),
	Selector_t1939762325_StaticFields::get_offset_of_DoesNotRecognizeSelectorHandle_11(),
	Selector_t1939762325_StaticFields::get_offset_of_PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle_12(),
	Selector_t1939762325_StaticFields::get_offset_of_PerformSelectorWithObjectAfterDelayHandle_13(),
	Selector_t1939762325_StaticFields::get_offset_of_UTF8StringHandle_14(),
	Selector_t1939762325::get_offset_of_handle_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (Callbacks_t3084417356), -1, sizeof(Callbacks_t3084417356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2107[2] = 
{
	Callbacks_t3084417356_StaticFields::get_offset_of__delegates_0(),
	Callbacks_t3084417356_StaticFields::get_offset_of__callbacks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (Methods_t1187897474), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (NSBundle_t3599697655), -1, sizeof(NSBundle_t3599697655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2109[1] = 
{
	NSBundle_t3599697655_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (NSData_t1166097363), -1, sizeof(NSData_t1166097363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2110[1] = 
{
	NSData_t1166097363_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (NSDictionary_t3598984691), -1, sizeof(NSDictionary_t3598984691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2111[1] = 
{
	NSDictionary_t3598984691_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (NSError_t1370871221), -1, sizeof(NSError_t1370871221_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2112[1] = 
{
	NSError_t1370871221_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (NSLocale_t2224424797), -1, sizeof(NSLocale_t2224424797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	NSLocale_t2224424797_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (NSMutableDictionary_t171997941), -1, sizeof(NSMutableDictionary_t171997941_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2114[1] = 
{
	NSMutableDictionary_t171997941_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (NSNotification_t1437913478), -1, sizeof(NSNotification_t1437913478_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2115[1] = 
{
	NSNotification_t1437913478_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (NSNotificationCenter_t3366908717), -1, sizeof(NSNotificationCenter_t3366908717_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2116[1] = 
{
	NSNotificationCenter_t3366908717_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (NSNumberFormatter_t3902210636), -1, sizeof(NSNumberFormatter_t3902210636_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2117[1] = 
{
	NSNumberFormatter_t3902210636_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (NSObject_t1518098886), -1, sizeof(NSObject_t1518098886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2118[3] = 
{
	NSObject_t1518098886_StaticFields::get_offset_of__classHandle_0(),
	NSObject_t1518098886::get_offset_of_Handle_1(),
	NSObject_t1518098886::get_offset_of__shouldRelease_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (NSTimeZone_t416177812), -1, sizeof(NSTimeZone_t416177812_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2119[1] = 
{
	NSTimeZone_t416177812_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (SKPayment_t4242751664), -1, sizeof(SKPayment_t4242751664_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2120[1] = 
{
	SKPayment_t4242751664_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (SKPaymentQueue_t538265951), -1, sizeof(SKPaymentQueue_t538265951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2121[1] = 
{
	SKPaymentQueue_t538265951_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (SKPaymentTransaction_t2918057744), -1, sizeof(SKPaymentTransaction_t2918057744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2122[1] = 
{
	SKPaymentTransaction_t2918057744_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (SKProduct_t3309436227), -1, sizeof(SKProduct_t3309436227_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2123[1] = 
{
	SKProduct_t3309436227_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (SKProductsRequest_t3969930329), -1, sizeof(SKProductsRequest_t3969930329_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2124[1] = 
{
	SKProductsRequest_t3969930329_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (SKProductsResponse_t3540529521), -1, sizeof(SKProductsResponse_t3540529521_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2125[1] = 
{
	SKProductsResponse_t3540529521_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (AdBannerView_t1238242894), -1, sizeof(AdBannerView_t1238242894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2126[1] = 
{
	AdBannerView_t1238242894_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (UIActionSheet_t2483183669), -1, sizeof(UIActionSheet_t2483183669_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2127[1] = 
{
	UIActionSheet_t2483183669_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (UIActivityViewController_t2230362714), -1, sizeof(UIActivityViewController_t2230362714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2128[1] = 
{
	UIActivityViewController_t2230362714_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (UIAlertView_t2175232939), -1, sizeof(UIAlertView_t2175232939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2129[1] = 
{
	UIAlertView_t2175232939_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (UIApplication_t1857964820), -1, sizeof(UIApplication_t1857964820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2130[1] = 
{
	UIApplication_t1857964820_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (UIDevice_t860038178), -1, sizeof(UIDevice_t860038178_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2131[3] = 
{
	UIDevice_t860038178_StaticFields::get_offset_of__classHandle_3(),
	UIDevice_t860038178_StaticFields::get_offset_of__majorVersion_4(),
	UIDevice_t860038178_StaticFields::get_offset_of__minorVersion_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (UIImage_t3580593017), -1, sizeof(UIImage_t3580593017_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2132[1] = 
{
	UIImage_t3580593017_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (UILocalNotification_t869364318), -1, sizeof(UILocalNotification_t869364318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2133[1] = 
{
	UILocalNotification_t869364318_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (UIScreen_t4048045402), -1, sizeof(UIScreen_t4048045402_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2134[1] = 
{
	UIScreen_t4048045402_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (UIScreenMode_t609995313), -1, sizeof(UIScreenMode_t609995313_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2135[1] = 
{
	UIScreenMode_t609995313_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (UIUserNotificationSettings_t4227938467), -1, sizeof(UIUserNotificationSettings_t4227938467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2136[1] = 
{
	UIUserNotificationSettings_t4227938467_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (UIView_t1452205135), -1, sizeof(UIView_t1452205135_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2137[1] = 
{
	UIView_t1452205135_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (UIViewController_t1891120779), -1, sizeof(UIViewController_t1891120779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2138[1] = 
{
	UIViewController_t1891120779_StaticFields::get_offset_of__classHandle_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (UIWindow_t800018578), -1, sizeof(UIWindow_t800018578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2139[1] = 
{
	UIWindow_t800018578_StaticFields::get_offset_of__classHandle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (ABCDStruct_t676331327)+ sizeof (RuntimeObject), sizeof(ABCDStruct_t676331327 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2140[4] = 
{
	ABCDStruct_t676331327::get_offset_of_A_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ABCDStruct_t676331327::get_offset_of_B_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ABCDStruct_t676331327::get_offset_of_C_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ABCDStruct_t676331327::get_offset_of_D_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (MD5Core_t1840877875), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (MD5Managed_t1233897219), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[4] = 
{
	MD5Managed_t1233897219::get_offset_of__data_4(),
	MD5Managed_t1233897219::get_offset_of__abcd_5(),
	MD5Managed_t1233897219::get_offset_of__totalLength_6(),
	MD5Managed_t1233897219::get_offset_of__dataSize_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (JsonType_t808352724)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2143[11] = 
{
	JsonType_t808352724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (JsonData_t4263252052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[9] = 
{
	JsonData_t4263252052::get_offset_of_inst_array_0(),
	JsonData_t4263252052::get_offset_of_inst_boolean_1(),
	JsonData_t4263252052::get_offset_of_inst_double_2(),
	JsonData_t4263252052::get_offset_of_inst_number_3(),
	JsonData_t4263252052::get_offset_of_inst_object_4(),
	JsonData_t4263252052::get_offset_of_inst_string_5(),
	JsonData_t4263252052::get_offset_of_json_6(),
	JsonData_t4263252052::get_offset_of_type_7(),
	JsonData_t4263252052::get_offset_of_object_list_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (OrderedDictionaryEnumerator_t1664192327), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[1] = 
{
	OrderedDictionaryEnumerator_t1664192327::get_offset_of_list_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (JsonException_t1457213491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (PropertyMetadata_t3287739986)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[3] = 
{
	PropertyMetadata_t3287739986::get_offset_of_Info_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropertyMetadata_t3287739986::get_offset_of_IsField_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PropertyMetadata_t3287739986::get_offset_of_Type_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (ArrayMetadata_t1135078014)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[3] = 
{
	ArrayMetadata_t1135078014::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ArrayMetadata_t1135078014::get_offset_of_is_array_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ArrayMetadata_t1135078014::get_offset_of_is_list_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (ObjectMetadata_t4058137740)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[3] = 
{
	ObjectMetadata_t4058137740::get_offset_of_element_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectMetadata_t4058137740::get_offset_of_is_dictionary_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObjectMetadata_t4058137740::get_offset_of_properties_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (ExporterFunc_t173265409), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (ImporterFunc_t850687278), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (WrapperFactory_t327905379), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (JsonMapper_t77474459), -1, sizeof(JsonMapper_t77474459_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2154[17] = 
{
	JsonMapper_t77474459_StaticFields::get_offset_of_max_nesting_depth_0(),
	JsonMapper_t77474459_StaticFields::get_offset_of_datetime_format_1(),
	JsonMapper_t77474459_StaticFields::get_offset_of_base_exporters_table_2(),
	JsonMapper_t77474459_StaticFields::get_offset_of_custom_exporters_table_3(),
	JsonMapper_t77474459_StaticFields::get_offset_of_base_importers_table_4(),
	JsonMapper_t77474459_StaticFields::get_offset_of_custom_importers_table_5(),
	JsonMapper_t77474459_StaticFields::get_offset_of_array_metadata_6(),
	JsonMapper_t77474459_StaticFields::get_offset_of_array_metadata_lock_7(),
	JsonMapper_t77474459_StaticFields::get_offset_of_conv_ops_8(),
	JsonMapper_t77474459_StaticFields::get_offset_of_conv_ops_lock_9(),
	JsonMapper_t77474459_StaticFields::get_offset_of_object_metadata_10(),
	JsonMapper_t77474459_StaticFields::get_offset_of_object_metadata_lock_11(),
	JsonMapper_t77474459_StaticFields::get_offset_of_type_properties_12(),
	JsonMapper_t77474459_StaticFields::get_offset_of_type_properties_lock_13(),
	JsonMapper_t77474459_StaticFields::get_offset_of_static_writer_14(),
	JsonMapper_t77474459_StaticFields::get_offset_of_static_writer_lock_15(),
	JsonMapper_t77474459_StaticFields::get_offset_of_dictionary_properties_to_ignore_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (U3CU3Ec_t1591091833), -1, sizeof(U3CU3Ec_t1591091833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2155[28] = 
{
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_0_1(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_1_2(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_2_3(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_3_4(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_4_5(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_5_6(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_6_7(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_7_8(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_8_9(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_9_10(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__24_10_11(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_0_12(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_1_13(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_2_14(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_3_15(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_4_16(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_5_17(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_6_18(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_7_19(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_8_20(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_9_21(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_10_22(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_11_23(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_12_24(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__25_13_25(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__31_0_26(),
	U3CU3Ec_t1591091833_StaticFields::get_offset_of_U3CU3E9__32_0_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (JsonToken_t2445581255)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2156[15] = 
{
	JsonToken_t2445581255::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (JsonReader_t354941621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[13] = 
{
	JsonReader_t354941621::get_offset_of_depth_0(),
	JsonReader_t354941621::get_offset_of_current_input_1(),
	JsonReader_t354941621::get_offset_of_current_symbol_2(),
	JsonReader_t354941621::get_offset_of_end_of_json_3(),
	JsonReader_t354941621::get_offset_of_end_of_input_4(),
	JsonReader_t354941621::get_offset_of_lexer_5(),
	JsonReader_t354941621::get_offset_of_parser_in_string_6(),
	JsonReader_t354941621::get_offset_of_parser_return_7(),
	JsonReader_t354941621::get_offset_of_read_started_8(),
	JsonReader_t354941621::get_offset_of_reader_9(),
	JsonReader_t354941621::get_offset_of_reader_is_owned_10(),
	JsonReader_t354941621::get_offset_of_token_value_11(),
	JsonReader_t354941621::get_offset_of_token_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (Condition_t2230619687)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2158[6] = 
{
	Condition_t2230619687::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (WriterContext_t1209007092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[5] = 
{
	WriterContext_t1209007092::get_offset_of_Count_0(),
	WriterContext_t1209007092::get_offset_of_InArray_1(),
	WriterContext_t1209007092::get_offset_of_InObject_2(),
	WriterContext_t1209007092::get_offset_of_ExpectingValue_3(),
	WriterContext_t1209007092::get_offset_of_Padding_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (JsonWriter_t3014444111), -1, sizeof(JsonWriter_t3014444111_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2160[11] = 
{
	JsonWriter_t3014444111_StaticFields::get_offset_of_number_format_0(),
	JsonWriter_t3014444111::get_offset_of_context_1(),
	JsonWriter_t3014444111::get_offset_of_ctx_stack_2(),
	JsonWriter_t3014444111::get_offset_of_has_reached_end_3(),
	JsonWriter_t3014444111::get_offset_of_hex_seq_4(),
	JsonWriter_t3014444111::get_offset_of_indentation_5(),
	JsonWriter_t3014444111::get_offset_of_indent_value_6(),
	JsonWriter_t3014444111::get_offset_of_inst_string_builder_7(),
	JsonWriter_t3014444111::get_offset_of_pretty_print_8(),
	JsonWriter_t3014444111::get_offset_of_validate_9(),
	JsonWriter_t3014444111::get_offset_of_writer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (FsmContext_t4275593467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[4] = 
{
	FsmContext_t4275593467::get_offset_of_Return_0(),
	FsmContext_t4275593467::get_offset_of_NextState_1(),
	FsmContext_t4275593467::get_offset_of_L_2(),
	FsmContext_t4275593467::get_offset_of_StateStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (Lexer_t954714164), -1, sizeof(Lexer_t954714164_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2162[14] = 
{
	Lexer_t954714164_StaticFields::get_offset_of_fsm_return_table_0(),
	Lexer_t954714164_StaticFields::get_offset_of_fsm_handler_table_1(),
	Lexer_t954714164::get_offset_of_allow_comments_2(),
	Lexer_t954714164::get_offset_of_allow_single_quoted_strings_3(),
	Lexer_t954714164::get_offset_of_end_of_input_4(),
	Lexer_t954714164::get_offset_of_fsm_context_5(),
	Lexer_t954714164::get_offset_of_input_buffer_6(),
	Lexer_t954714164::get_offset_of_input_char_7(),
	Lexer_t954714164::get_offset_of_reader_8(),
	Lexer_t954714164::get_offset_of_state_9(),
	Lexer_t954714164::get_offset_of_string_buffer_10(),
	Lexer_t954714164::get_offset_of_string_value_11(),
	Lexer_t954714164::get_offset_of_token_12(),
	Lexer_t954714164::get_offset_of_unichar_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (StateHandler_t3489987002), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (CRC32_t619824607), -1, sizeof(CRC32_t619824607_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2164[3] = 
{
	CRC32_t619824607::get_offset_of__TotalBytesRead_0(),
	CRC32_t619824607_StaticFields::get_offset_of_crc32Table_1(),
	CRC32_t619824607::get_offset_of__RunningCrc32Result_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (CrcCalculatorStream_t2228597532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[3] = 
{
	CrcCalculatorStream_t2228597532::get_offset_of__InnerStream_1(),
	CrcCalculatorStream_t2228597532::get_offset_of__Crc32_2(),
	CrcCalculatorStream_t2228597532::get_offset_of__length_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (InvalidDataException_t1057132120), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (AWSConfigs_t909839638), -1, sizeof(AWSConfigs_t909839638_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2167[21] = 
{
	AWSConfigs_t909839638_StaticFields::get_offset_of_validSeparators_0(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_utcNowSource_1(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__awsRegion_2(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__logging_3(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__responseLogging_4(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__logMetrics_5(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__endpointDefinition_6(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__awsProfileName_7(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__awsAccountsLocation_8(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__useSdkCache_9(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__lock_10(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_standardConfigs_11(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_configPresent_12(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__rootConfig_13(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_U3CClockOffsetU3Ek__BackingField_14(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_mPropertyChanged_15(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_propertyChangedLock_16(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__httpClient_17(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_UnityWebRequestInitialized_18(),
	AWSConfigs_t909839638_StaticFields::get_offset_of__traceListeners_19(),
	AWSConfigs_t909839638_StaticFields::get_offset_of_xmlDoc_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (HttpClientOption_t4250830711)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2168[3] = 
{
	HttpClientOption_t4250830711::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (U3CU3Ec__DisplayClass94_0_t1056790044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[3] = 
{
	U3CU3Ec__DisplayClass94_0_t1056790044::get_offset_of_awsConfig_0(),
	U3CU3Ec__DisplayClass94_0_t1056790044::get_offset_of_xDoc_1(),
	U3CU3Ec__DisplayClass94_0_t1056790044::get_offset_of_action_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (U3CU3Ec__DisplayClass94_1_t1056790043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[2] = 
{
	U3CU3Ec__DisplayClass94_1_t1056790043::get_offset_of_e_0(),
	U3CU3Ec__DisplayClass94_1_t1056790043::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (U3CU3Ec__DisplayClass96_0_t1056790110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[1] = 
{
	U3CU3Ec__DisplayClass96_0_t1056790110::get_offset_of_propertyInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (LoggingOptions_t2865640765)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2172[6] = 
{
	LoggingOptions_t2865640765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (ResponseLoggingOption_t3443611737)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2173[4] = 
{
	ResponseLoggingOption_t3443611737::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (LogMetricsFormatOption_t97749509)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2174[3] = 
{
	LogMetricsFormatOption_t97749509::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (AWSSection_t3096528870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[10] = 
{
	AWSSection_t3096528870::get_offset_of_U3CLoggingU3Ek__BackingField_0(),
	AWSSection_t3096528870::get_offset_of_U3CEndpointDefinitionU3Ek__BackingField_1(),
	AWSSection_t3096528870::get_offset_of_U3CRegionU3Ek__BackingField_2(),
	AWSSection_t3096528870::get_offset_of_U3CUseSdkCacheU3Ek__BackingField_3(),
	AWSSection_t3096528870::get_offset_of_U3CCorrectForClockSkewU3Ek__BackingField_4(),
	AWSSection_t3096528870::get_offset_of_U3CProxyU3Ek__BackingField_5(),
	AWSSection_t3096528870::get_offset_of_U3CProfileNameU3Ek__BackingField_6(),
	AWSSection_t3096528870::get_offset_of_U3CProfilesLocationU3Ek__BackingField_7(),
	AWSSection_t3096528870::get_offset_of_U3CApplicationNameU3Ek__BackingField_8(),
	AWSSection_t3096528870::get_offset_of__serviceSections_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (ProxySection_t4280332351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (LoggingSection_t905443946), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[6] = 
{
	LoggingSection_t905443946::get_offset_of_U3CLogToU3Ek__BackingField_0(),
	LoggingSection_t905443946::get_offset_of_U3CLogResponsesU3Ek__BackingField_1(),
	LoggingSection_t905443946::get_offset_of_U3CLogResponsesSizeLimitU3Ek__BackingField_2(),
	LoggingSection_t905443946::get_offset_of_U3CLogMetricsU3Ek__BackingField_3(),
	LoggingSection_t905443946::get_offset_of_U3CLogMetricsFormatU3Ek__BackingField_4(),
	LoggingSection_t905443946::get_offset_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (RegionEndpoint_t661522805), -1, sizeof(RegionEndpoint_t661522805_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2178[22] = 
{
	RegionEndpoint_t661522805_StaticFields::get_offset_of__hashBySystemName_0(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USEast1_1(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USEast2_2(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USWest1_3(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USWest2_4(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_EUWest1_5(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_EUWest2_6(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_EUWest3_7(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_EUCentral1_8(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APNortheast1_9(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APNortheast2_10(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APSouth1_11(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APSoutheast1_12(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_APSoutheast2_13(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_SAEast1_14(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_USGovCloudWest1_15(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_CNNorth1_16(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_CNNorthWest1_17(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of_CACentral1_18(),
	RegionEndpoint_t661522805_StaticFields::get_offset_of__regionEndpointProvider_19(),
	RegionEndpoint_t661522805::get_offset_of_U3CSystemNameU3Ek__BackingField_20(),
	RegionEndpoint_t661522805::get_offset_of_U3CDisplayNameU3Ek__BackingField_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (Endpoint_t3348692641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[3] = 
{
	Endpoint_t3348692641::get_offset_of_U3CHostnameU3Ek__BackingField_0(),
	Endpoint_t3348692641::get_offset_of_U3CAuthRegionU3Ek__BackingField_1(),
	Endpoint_t3348692641::get_offset_of_U3CSignatureVersionOverrideU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (UnityInitializer_t2778189483), -1, sizeof(UnityInitializer_t2778189483_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2180[3] = 
{
	UnityInitializer_t2778189483_StaticFields::get_offset_of__instance_2(),
	UnityInitializer_t2778189483_StaticFields::get_offset_of__lock_3(),
	UnityInitializer_t2778189483_StaticFields::get_offset_of__mainThread_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (RegionEndpointProviderV2_t1909011008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (RegionEndpoint_t1885241449), -1, sizeof(RegionEndpoint_t1885241449_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2184[6] = 
{
	RegionEndpoint_t1885241449_StaticFields::get_offset_of__documentEndpoints_0(),
	RegionEndpoint_t1885241449_StaticFields::get_offset_of_loaded_1(),
	RegionEndpoint_t1885241449_StaticFields::get_offset_of_LOCK_OBJECT_2(),
	RegionEndpoint_t1885241449_StaticFields::get_offset_of_hashBySystemName_3(),
	RegionEndpoint_t1885241449::get_offset_of_U3CSystemNameU3Ek__BackingField_4(),
	RegionEndpoint_t1885241449::get_offset_of_U3CDisplayNameU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (RegionEndpointV3_t2255552612), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[6] = 
{
	RegionEndpointV3_t2255552612::get_offset_of__serviceMap_0(),
	RegionEndpointV3_t2255552612::get_offset_of_U3CRegionNameU3Ek__BackingField_1(),
	RegionEndpointV3_t2255552612::get_offset_of_U3CDisplayNameU3Ek__BackingField_2(),
	RegionEndpointV3_t2255552612::get_offset_of__partitionJsonData_3(),
	RegionEndpointV3_t2255552612::get_offset_of__servicesJsonData_4(),
	RegionEndpointV3_t2255552612::get_offset_of__servicesLoaded_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (ServiceMap_t3203095671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[2] = 
{
	ServiceMap_t3203095671::get_offset_of__serviceMap_0(),
	ServiceMap_t3203095671::get_offset_of__dualServiceMap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (RegionEndpointProviderV3_t342927067), -1, sizeof(RegionEndpointProviderV3_t342927067_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2187[5] = 
{
	RegionEndpointProviderV3_t342927067::get_offset_of__root_0(),
	RegionEndpointProviderV3_t342927067::get_offset_of__regionEndpointMap_1(),
	RegionEndpointProviderV3_t342927067::get_offset_of__regionEndpointMapLock_2(),
	RegionEndpointProviderV3_t342927067::get_offset_of__allRegionEndpointsLock_3(),
	RegionEndpointProviderV3_t342927067_StaticFields::get_offset_of__emptyDictionaryJsonData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (ProxyConfig_t2693849256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (LoggingConfig_t4162907495), -1, sizeof(LoggingConfig_t4162907495_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2189[7] = 
{
	LoggingConfig_t4162907495_StaticFields::get_offset_of_DefaultLogResponsesSizeLimit_0(),
	LoggingConfig_t4162907495::get_offset_of__logTo_1(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogResponsesU3Ek__BackingField_2(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogResponsesSizeLimitU3Ek__BackingField_3(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogMetricsU3Ek__BackingField_4(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogMetricsFormatU3Ek__BackingField_5(),
	LoggingConfig_t4162907495::get_offset_of_U3CLogMetricsCustomFormatterU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (AWSSDKUtils_t2036360342), -1, sizeof(AWSSDKUtils_t2036360342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2190[6] = 
{
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of_EPOCH_START_0(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of_RFCEncodingSchemes_1(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of_ValidPathCharacters_2(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of__dispatcher_3(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of__preserveStackTraceLookupLock_4(),
	AWSSDKUtils_t2036360342_StaticFields::get_offset_of__preserveStackTraceLookup_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (U3CU3Ec_t3667845516), -1, sizeof(U3CU3Ec_t3667845516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2191[3] = 
{
	U3CU3Ec_t3667845516_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3667845516_StaticFields::get_offset_of_U3CU3E9__30_0_1(),
	U3CU3Ec_t3667845516_StaticFields::get_offset_of_U3CU3E9__30_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (CryptoUtilFactory_t2421970493), -1, sizeof(CryptoUtilFactory_t2421970493_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2194[3] = 
{
	CryptoUtilFactory_t2421970493_StaticFields::get_offset_of_util_0(),
	CryptoUtilFactory_t2421970493_StaticFields::get_offset_of__initializedAlgorithmNames_1(),
	CryptoUtilFactory_t2421970493_StaticFields::get_offset_of__keyedHashAlgorithmCreationLock_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (CryptoUtil_t1025015063), -1, 0, sizeof(CryptoUtil_t1025015063_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable2195[1] = 
{
	THREAD_STATIC_FIELD_OFFSET,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (NetworkInfo_t3319294148), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (U3CU3Ec__DisplayClass1_0_t2945196168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[2] = 
{
	U3CU3Ec__DisplayClass1_0_t2945196168::get_offset_of__networkReachability_0(),
	U3CU3Ec__DisplayClass1_0_t2945196168::get_offset_of_asyncEvent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (InternalSDKUtils_t3074264706), -1, sizeof(InternalSDKUtils_t3074264706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2199[4] = 
{
	InternalSDKUtils_t3074264706_StaticFields::get_offset_of__customSdkUserAgent_0(),
	InternalSDKUtils_t3074264706_StaticFields::get_offset_of__customData_1(),
	InternalSDKUtils_t3074264706_StaticFields::get_offset_of__userAgentBaseName_2(),
	InternalSDKUtils_t3074264706_StaticFields::get_offset_of__logger_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
